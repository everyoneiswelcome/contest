
## PORTS
# for i in {1..50} ; do
#   if ../generate-test-blob.py -x 255 -- -a bob -p `cat mutations/port/$i.mutation` -n 10.00 > /dev/null 2>&1; then
#     ../generate-test-blob.py -x 255 -- -a bob -p `cat mutations/port/$i.mutation` -n 10.00
#   else
#     ../generate-test-blob.py -x 255 -b -- -a bob -p `cat mutations/port/$i.mutation | base64` -n 10.00;
#   fi;
# done > $1.json

# ## CURRENCY
# for i in {1..50} ; do
#   if ../generate-test-blob.py -x 255 -- -a bob -w `cat mutations/currency/$i.mutation` > /dev/null 2>&1; then
#     ../generate-test-blob.py -x 255 -- -a bob -w `cat mutations/currency/$i.mutation`
#   else
#     ../generate-test-blob.py -x 255 -b -- -a bob -w `cat mutations/currency/$i.mutation | base64`;
#   fi;
# done > $1.json

## Filenames
for i in {1..50} ; do
  if ../generate-test-blob.py -x 255 -- -a bob -c `cat mutations/filename/$i.mutation` -n 10.00 > /dev/null 2>&1; then
    ../generate-test-blob.py -x 255 -- -a bob -c `cat mutations/filename/$i.mutation` -n 10.00
  else
    ../generate-test-blob.py -x 255 -b -- -a bob -c `cat mutations/filename/$i.mutation | base64` -n 10.00;
  fi;
done > $1.json
