#!/usr/bin/env python2
import traceback
import random
import socket
import argparse
import threading
import signal
import sys
import time
from contextlib import contextmanager

running = True
verbose = True

CLIENT2SERVER = 1
SERVER2CLIENT = 2

# COMMAND LIST
# 1 - create bob 10.00
# 2 - withdraw bob 1.00
# 3 - bob balance
# 4 - create santa 99999.99
# 5 - deposit bob 50.00
# 6 - bob balance
# 7 - santa balance

counter_atm_to_bank = 0
counter_bank_to_atm = 0
stored_command = None
log = ''

def mitm(buff, direction):
    global counter_atm_to_bank, counter_bank_to_atm, stored_command, log

    hb = buff
    if direction == CLIENT2SERVER:
        counter_atm_to_bank += 1
        log += hb


        # # SEND MALFORMED
        # hb = 'this should be invalid'

        # # DROP PACKETS
        # hb = None

        # UNCOMMENT TO PRINT REQUESTS, client->server
        # print "\natm->bank #{} -- {}\n-> {}".format(counter_atm_to_bank, int(time.time() * 1000), hb)

    elif direction == SERVER2CLIENT:
        counter_bank_to_atm += 1
        log += hb

        # # REPLAY 1st COMMAND on 2nd
        # if counter_bank_to_atm is 1:
        #   stored_command = buff
        # elif counter_bank_to_atm is 2:
        #   hb = stored_command

        # UNCOMMENT TO PRINT REQUESTS, server->client
        # print "\nbank<-atm #{} -- {}\n<- {}".format(counter_bank_to_atm, int(time.time() * 1000), hb)

    # Does some number mutation, length depends on message length
    #hb = "".join("{:02x}".format(ord(c)) for c in buff)

    # REVERSES the buffer
    #hb = hb[::-1]

    # # DROPS RANDOM CHARACTERS
    # hb = "".join([ i if random.choice([True,False]) == True else '' for i in buff ])

    # # MANGLES RESPONSE - CHANGES lEttER cASE?
    # hb = "".join([ chr(ord(i) ^ 0x20) if ord(i) >= 0x41 and ord(i) <= 0x71 else i for i in buff])

    # You must return the
    return hb

IS_HTTP = IS_PLAIN = IS_SSL = HINT_KEYWORD = None
def profile_log():
    global IS_HTTP, IS_PLAIN, IS_SSL, HINT_KEYWORD

    log_lower = log.lower()

    if IS_HTTP is None and 'content-length' in log_lower:
        print "Looks like HTTP!"
        IS_HTTP = True

    if IS_PLAIN is None:
        for plaintext_keyword in ['bob', 'santa', '999.', 'balance', 'account']:
            if plaintext_keyword in log_lower:
                print "ALERT, looks like plaintext!!! Saw {}".format(plaintext_keyword)
                IS_PLAIN = True

    if HINT_KEYWORD is None:
        for helpful_hint in ['nonce', 'hmac', 'time', 'key']:
            if helpful_hint in log_lower:
                print "Saw helpful keyword in payload: {}".format(helpful_hint)
                HINT_KEYWORD = True

    if counter_atm_to_bank is 7 or counter_bank_to_atm is 7:
        print "Looks like 1 request per transaction, atm->bank. Replay vuln?"

    if counter_atm_to_bank is 14 or counter_bank_to_atm is 14:
        print "2 requests per transaction - custom session security?"


@contextmanager
def ignored(*exceptions):
    try:
        yield
    except exceptions:
        pass

def killpn( a, b, n):
    # print "killpn: here"
    if n != CLIENT2SERVER:
        killp( a, b)

def killp(a, b):
    with ignored(Exception):
        a.shutdown(socket.SHUT_RDWR)
        a.close()
        b.shutdown(socket.SHUT_RDWR)
        b.close()
    return

def worker(client, server, direction):
    while running == True:
        b = ""
        with ignored(Exception):
            b = client.recv(1024)
        if len(b) == 0:
            killpn(client,server,direction)
            return
        try:
            b = mitm(b,direction)
        except:
            raise
            pass
        try:
            if b != None:
                server.send(b)
        except:
            killpn(client,server,direction)
            return

    killp(client,server)
    return

def signalhandler(sn, sf):
    profile_log()
    running = False

rhost = None
rport = None

def doProxyMain(port, remotehost, remoteport):
    global rhost, rport
    rhost = remotehost
    rport = remoteport
    signal.signal(signal.SIGTERM, signalhandler)
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(("0.0.0.0", port))
        s.listen(1)
        workers = []
        while running == True:
            k,a = s.accept()
            v = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            v.connect((remotehost, remoteport))
            t1 = threading.Thread(target=worker, args=(k,v,CLIENT2SERVER))
            t2 = threading.Thread(target=worker, args=(v,k,SERVER2CLIENT))
            t2.start()
            t1.start()
            workers.append((t1,t2,k,v))
    except KeyboardInterrupt:
        signalhandler(None, None)
    for t1,t2,k,v in workers:
        killp(k,v)
        t1.join()
        t2.join()
    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Proxy')
    parser.add_argument('-p', type=int, default=4000, help="listen port")
    parser.add_argument('-s', type=str, default="127.0.0.1", help="server ip address")
    parser.add_argument('-q', type=int, default=3000, help="server port")
    parser.add_argument('-c', type=str, default="127.0.0.1", help="command server")
    parser.add_argument('-d', type=int, default=5000, help="command port")
    args = parser.parse_args()
    print('started\n')
    sys.stdout.flush()
    doProxyMain(args.p, args.s, args.q)
