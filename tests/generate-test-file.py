#!/usr/bin/env python2
import optparse
import json
import os

def get_pretty_print(json_object):
    return json.dumps(json_object, sort_keys=True, indent=4, separators=(',', ': '))

def main():
    usage = "usage: %prog [-m <mitm script>] [output filename - stdout default]"
    p = optparse.OptionParser(usage=usage)

    p.add_option('-m', '--mitm', type="string", dest="mitm", metavar="<mitm script>", help="MITM attack script")

    options, arguments = p.parse_args()

    test = {
        'inputs': [ '<...ADD TEST BLOBS HERE...>' ],
    }

    if options.mitm is not None:
        test['mitm'] = options.mitm

    try:
        filename = arguments[0]
        # TODO - append .json if not present
    except:
        print get_pretty_print(test)
        return

    if os.path.exists(filename):
        print "file already exists"
        return

    f = open(filename, 'w')
    f.write(get_pretty_print(test))
    f.close()
    print "created file: {}".format(filename)

if __name__ == '__main__':
    main()
