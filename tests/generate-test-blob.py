#!/usr/bin/env python2
import optparse
import json

def main():
    usage = "usage: %prog [output options] -- [atm options]"
    p = optparse.OptionParser(usage=usage)

    p.add_option('-o', '--output', type="string", dest="output", metavar='\'{"json": "output"}\'', default="{}", help="the json output expected, provided as a string")
    p.add_option('-x', '--exit', type="int", dest="exit", metavar="<exit code>", default=0, help="the expected exit code")
    p.add_option('-b', action="store_true", dest="base64", help="encode into base64 - default: false")

    try:
        options, arg_list = p.parse_args()
    except:
        sys.exit(1)

    output = None
    try:
        output = json.loads(options.output)
    except:
        print "Invalid output payload"
        return

    if '-i' not in arg_list:
        arg_list.insert(0, '%IP%')
        arg_list.insert(0, '-i')

    if '-p' not in arg_list:
        arg_list.insert(0, '%PORT%')
        arg_list.insert(0, '-p')

    blob = {
        'input': {
            'input': arg_list
        },
        'output': {
            'output': output,
            'exit': options.exit
        }
    }
    if options.base64:
        blob['input']['base64'] = True

    print json.dumps(blob)


if __name__ == '__main__':
    main()
