#!/usr/bin/env python2
import sys
from Crypto.Hash import HMAC
from Crypto.Cipher import AES
import re
import optparse
import os

#https://en.wikipedia.org/wiki/Reserved_IP_addresses lists lots of 'reserved' IP addresses
reservedip = ["255.255.255.255"]
leadingzero = re.compile('0[0-9]') # detect leading zero
invalid_chars = re.compile('[^_\-\.0-9a-z]')
validip = re.compile("(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])")
validamt = re.compile('[0-9]+(\.[0-9][0-9]?)?')
valid_options = ['-a', '-s', '-i', '-p', '-c', '-n', '-d', '-w', '-g']

def general_error():
    sys.exit(255)

def make_hmac(message, key):
    """Creates an HMAC from the given message, using the given key. Uses HMAC-MD5.
       message - The message to create an HMAC of.
       key - The key to use for the HMAC (at least 16 bytes).

       returns A hex string of the HMAC.

    from https://bitbucket.org/brendanlong/python-encryption """
    h = HMAC.new(key)
    h.update(message)
    return h.hexdigest()

def check_hmac(provided, computed):
    result = 0
    if len(provided) != len(computed):
        protocol_error()
        #raise ValueError('HMAC length mismatch')
    for x,y in zip(provided, computed):
        result += int(ord(x)^ord(y))
    if result != 0:
        raise ValueError('Invalid HMAC')
    return

def generate_nonce():
    nonce = base64.b64encode(os.urandom(8))
    return nonce

def check_port(option, opt_str, value, parser):
    if getattr(parser.values, option.dest) is not None:
        return general_error()
    if leadingzero.match(value):
        return general_error()

    if value.count('.') != 0:
        return general_error()

    try:
        port = int(value)
    except:
        return general_error()

    if port < 1024 or port > 65535:
        return general_error()

    setattr(parser.values, option.dest, port)

def check_filename(option, opt_str, value, parser):
    if getattr(parser.values, option.dest) is not None:
        return general_error()
    if len(value) > 255:
        return general_error()
    if not value or invalid_chars.search(value):
        return general_error()
    if value == '.' or value == '..':
        return general_error()

    setattr(parser.values, option.dest, value)

class ModifiedOptionParser(optparse.OptionParser):
    def error(self, msg):
        return general_error()

def encrypt(message, key):
    """Encrypts a given message with the given key, using AES-CFB.
       message - The message to encrypt (byte string).
       key - The AES key (16 bytes).

       returns (ciphertext, iv). Both values are byte strings.
    """
    # The IV should always be random
    iv = os.urandom(AES.block_size)
    cipher = AES.new(key, AES.MODE_CFB, iv)
    ciphertext = cipher.encrypt(message)
    return (ciphertext, iv)

def decrypt(ciphertext, key, iv):
    """Decrypts a given ciphertext with the given key, using AES-CFB.
       message - The ciphertext to decrypt (byte string).
       key - The AES key (16 bytes).
       iv - The original IV used for encryption.

       returns The cleartext (byte string)
    """
    cipher = AES.new(key, AES.MODE_CFB, iv)
    msg = cipher.decrypt(ciphertext)
    return msg
