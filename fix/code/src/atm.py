#!/usr/bin/env python2
import httplib
import json
import sys
import os
import base64
import socket
import re

def protocol_error():
    sys.exit(63)

def verify_auth_file_existence(auth_file):
    if os.path.isfile(auth_file) == False:
        return general_error()

def read_auth_file(auth_file):
    f = open(auth_file)
    return [base64.b64decode(line) for line in f]


def test_mac(header_mac, payload, hmac_key):
    payload_hmac = make_hmac(payload, hmac_key)
    check_hmac(header_mac, payload_hmac)

def prepare_payload(payload, hmac_key, aes_key):
    payload, iv = encrypt(payload, aes_key)
    return payload, {
        'MAC': make_hmac(payload, hmac_key),
        'IV': base64.b64encode(iv)
    }

def request_nonce(ip_address, port, hmac_key, aes_key):

    payload = generate_nonce()

    conn = httplib.HTTPConnection(ip_address, port, timeout=10)
    conn.request("POST", "/nonce", payload, {'MAC': make_hmac(payload, hmac_key)})
    resp = conn.getresponse()

    ciphernonce = resp.read()
    test_mac(resp.getheader('MAC'), ciphernonce, hmac_key)
    return decrypt(ciphernonce, aes_key, base64.b64decode(resp.getheader('IV')))

def perform_request(ip_address, port, payload, hmac_key, aes_key):

    try:
        payload = json.dumps(payload)
    except:
        return general_error()

    try: # Timeout handling
        nonce = request_nonce(ip_address, port, hmac_key, aes_key)
        payload, headers = prepare_payload(payload, hmac_key, aes_key)
        headers['NONCE'] = nonce

        conn = httplib.HTTPConnection(ip_address, port, timeout=10)

        conn.request("POST", "/", payload, headers)
        resp = conn.getresponse()
    except:
        return protocol_error()

    if (resp.status == 500):
        return general_error()

    if (resp.status != 200):
        return protocol_error()

    try:
        cipherresult = resp.read() # Read body of response
        test_mac(resp.getheader('MAC'), cipherresult, hmac_key)
        result = decrypt(cipherresult, aes_key, base64.b64decode(resp.getheader('IV')))
    except:
        return general_error()

    return result.rstrip()

_currency_format = re.compile(r'\.\d{2}$') # MUST have 2 decimals at end
def check_currency(option, opt_str, value, parser):
    if getattr(parser.values, option.dest) is not None:
        return general_error()
    if leadingzero.match(value):
        return general_error()
    if not _currency_format.search(value):
        return general_error()
    if not validamt.match(value):
        return general_error()
    # Parse string into number
    try:
        value = float(value)
    except:
        return general_error()

    if (value >= 4294967296):
        return general_error()
    if (value <= 0):
        return general_error()

    setattr(parser.values, option.dest, value)

def check_ip(option, opt_str, value, parser):
    if getattr(parser.values, option.dest) is not None:
        return general_error()
    validate_ip(value)
    setattr(parser.values, option.dest, value)

def validate_ip(address):
    if leadingzero.match(address):
        return general_error()
    if address in reservedip:
        return general_error()
    if not validip.match(address):
        return general_error()
    try:
        # Try to parse IP with socket library
        socket.inet_aton(address)
    except socket.error:
        return general_error()

    if (address.count('.') != 3):
        # We need this because socket library allows "short" ip like "127.0"
        return general_error()

def check_account(option, opt_str, value, parser):
    if getattr(parser.values, option.dest) is not None:
        return general_error()
    if len(value) > 250:
        return general_error()
    if not value or invalid_chars.search(value):
        return general_error()

    setattr(parser.values, option.dest, value)

def test_arguments(mode_create, mode_deposit, mode_withdraw, mode_balance ):
    if (mode_create + mode_balance + mode_withdraw + mode_deposit) > 1:
        general_error()
    return

def test_card_file_existence(card_name):
    if os.path.isfile(card_name):
        general_error()

def create_card_file_contents(card_name, account, hmac_key):
    test_card_file_existence(card_name)
    account_hmac = make_hmac(account, hmac_key)
    return account_hmac

def write_card_file(card_name, contents):
    try:
        test_card_file_existence(card_name)
        card_file = open(card_name, 'w')
        card_file.write(contents)
        card_file.close()
    except:
        general_error()

def get_card_content(card_name):
    try:
        card_file = open(card_name, 'r')
    except:
        return general_error()
    content= card_file.read()
    return content

def test_min_balance(balance):
    if balance < 9.999:
        general_error()

def main():
    try:
        p = ModifiedOptionParser(add_help_option=False)
        # Require option
        p.add_option('-a', type="string", dest="account", action="callback", callback=check_account)

        # Options with defaults
        p.add_option('-s', type="string", dest="auth_file", action="callback", callback=check_filename)
        p.add_option('-i', type="string", dest="ip_address", action="callback", callback=check_ip)
        p.add_option('-p', type="string", dest="port", action="callback", callback=check_port)
        p.add_option('-c', type="string", dest="card_file", action="callback", callback=check_filename)

        # Operation mode options
        p.add_option('-n', type="string", dest="balance", action="callback", callback=check_currency)
        p.add_option('-d', type="string", dest="deposit_amount", action="callback", callback=check_currency)
        p.add_option('-w', type="string", dest="withdraw_amount", action="callback", callback=check_currency)
        p.add_option('-g', action="count", dest="get_balance")

        # TODO - perhaps POSIX compliance and arg length limits?

        options, arguments = p.parse_args()

        if len(arguments) > 0 or options.get_balance > 1:
            return general_error()

        if len(sys.argv) != len(set(sys.argv)):
            return general_error()

        # Check for invalid input parameter '--' at end of user input
        j = 0
        for i in sys.argv:
            remainder = j % 2
            if remainder != 0:
                if i not in valid_options:
                    return general_error()
            j = j + 1
        # End check for invalid input parameter '--' at end of user input

        # Boolean mode flags for clarity
        mode_create = options.balance is not None
        mode_deposit = options.deposit_amount is not None
        mode_withdraw = options.withdraw_amount is not None
        mode_balance = options.get_balance is not None

        test_arguments(mode_create, mode_deposit, mode_withdraw, mode_balance)

        if options.account is None:
            return general_error()
        if options.auth_file is None:
            options.auth_file = "bank.auth"
        if options.ip_address is None:
            options.ip_address = "127.0.0.1"
        if options.port is None:
            options.port = 3000
        if options.card_file is None:
            options.card_file = options.account
        if not options.card_file.endswith(".card") and len(options.card_file) < 250:
            options.card_file += '.card'

        verify_auth_file_existence(options.auth_file)

        # Keys ready for payload encryption (confidentiality) & MAC (integrity)
        aes_key, hmac_key = read_auth_file(options.auth_file)

        payload = {'account': options.account}

        # Handle modes
        if (mode_create == True):
            payload['card'] = create_card_file_contents(options.card_file, options.account, hmac_key)
            payload['mode'] = "create"
            test_min_balance(options.balance)
            payload['initial_balance'] = options.balance

        elif (mode_deposit == True):
            payload['mode'] = "deposit"
            payload['deposit'] = options.deposit_amount
            payload['card'] = get_card_content(options.card_file)

        elif (mode_withdraw == True):
            payload['mode'] = "withdraw"
            payload['withdraw'] = options.withdraw_amount
            payload['card'] = get_card_content(options.card_file)

        elif (mode_balance == True):
            payload['mode'] = "balance"
            payload['card'] = get_card_content(options.card_file)

        else:
            return general_error()

    except:
        general_error()

    # Send payload to bank
    resp = perform_request(options.ip_address, options.port, payload, hmac_key, aes_key)

    if mode_create is True:
        write_card_file(options.card_file, payload['card'])

    # Print JSON response
    print(resp)


if __name__ == '__main__':
    main()
