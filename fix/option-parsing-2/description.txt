The library we were using could not detect duplicate command line options, and we had some broken/naive checks in place. We had to remove default values from the option parsing, then we were able to detect when an option was given more than once. The fix requires us to manually set default values after the automated parsing takes place
Corrects a bug in the original fix submission (option-parsing): 77e69395968733229f30743750e68a5224581aa1

For example, our application failed break #3562 because our naive check gave a false positive when arguments were the same (account and card name in this example). The fix updates our logic to only check options and no longer incorrectly errors on duplicate values.
