This test verifies that a new card file is unique (has not been created yet) where the name of the card file is not the default value, and is provided. Account name = "tom" and card file = ".card"; atm should return code 0.  

Team:  21  
$ /tmp/tmp5KlWuD/atm -p 3000 -i 127.0.0.1 -a tom -c .card -n 10.00  
got exit: 255  
expected exit: 0  
