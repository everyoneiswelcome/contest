ATM exits with wrong code with missing card file option ("-c" specified)

$ /tmp/tmp7y6BAB/atm -p 3000 -i 127.0.0.1 -a bob -c -n 10.00
got exit: 0
expected exit: 255
