This test verifies that a new card file is unique (has not been created yet) where the name of the card file is not the default value. Account name = sadie and card file parameter is provided, but is blank ("-c" ""); atm should return error 255.  

Team:  12  
$ /tmp/tmpueeZmP/atm -p 3000 -i 127.0.0.1 -a sadie -c  -n 10.00  
got exit: 0  
expected exit: 255  
