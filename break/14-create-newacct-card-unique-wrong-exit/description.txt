This test verifies that a new card file is unique (has not been created yet) where the name of the card file is the default value, and is provided. Account name = fred and card file = fred.card; atm should return code 0.  

Team:  14  
$ /tmp/tmp_eCq0O/atm -p 3000 -i 127.0.0.1 -a fred -c fred.card -n 10.00  
got exit: 63  
expected exit: 0  
