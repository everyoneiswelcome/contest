This test verifies the card file name is limited to digits and lowercase alphabet characters; account name = 'fred', card file name = 'fred3.card'; atm should send code 0.  

Team:  21  
$ /tmp/tmpQ6uX5_/atm -p 3000 -i 127.0.0.1 -a fred -c fred3.card -n 10.00  
got exit: 255  
expected exit: 0  
