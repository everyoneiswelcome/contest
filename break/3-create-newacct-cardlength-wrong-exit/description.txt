This test verifies that a new card file name is restricted as 0 < card file length <= 255, the card file name is not the default value, and is provided. Account name = "a" and card file = "a"; atm should return code 0.  

Team:  3  
$ /tmp/tmpVQwn1l/atm -p 3000 -i 127.0.0.1 -a a -c a -n 10.00  
got exit: 255
expected exit: 0
