This demonstrates an integrity violation by replaying a "withdraw" command sent by the atm.
An account is created, then a withdrawal is made, and the data is replayed to the bank.
