#!/bin/bash

debug=0
if [[ "$1" = "-d" ]]; then
	debug=1
	shift
fi

app_name="$1"
shift

filename="$1"
while [[ ! -z "$filename" ]]; do
	awk '{
		if(substr($0, 0, 11) == "if __name__"){
			exit;
		} else {
			print $0;
		}
	}' "$filename" >> "${app_name}.tmp"
	shift
	filename="$1"
done

if [[ "$debug" = "1" ]]; then
	mv "${app_name}.tmp" "$app_name"
	echo -en "\napplication().run()" >> "$app_name"
else
	sed -r "
		s/[ ]*#.*$//g;
		/^[ \t]*$/d;
		s/[ \t]*$//g;
		s/(self|encsock_help).error.*,[ ]*([0-9]+)\)$/sys.exit(\2)/g
	" "${app_name}.tmp" >> "${app_name}.tmp2"

	grep "import " "${app_name}.tmp2" >> "${app_name}.tmp3"
	grep -v "import " "${app_name}.tmp2" >> "${app_name}.tmp4"
	cat "${app_name}.tmp3" "${app_name}.tmp4" >> "${app_name}.tmp5"

	awk 'BEGIN{
		print "#!/usr/bin/env python3";
	}{
		if(substr($0, 2, 9) == "def error"){
			getline; getline; getline;
			getline; getline; getline;
			getline;
		} else {
			print $0;
		}
	}' "${app_name}.tmp5" >> "${app_name}.tmp6"

	mv "${app_name}.tmp6" "$app_name"
	rm -f \
		"${app_name}.tmp" \
		"${app_name}.tmp2" \
		"${app_name}.tmp3" \
		"${app_name}.tmp4" \
		"${app_name}.tmp5"

	if [[ "$app_name" = "bank" ]]; then
		echo -ne "\napplication().run()\n" >> "$app_name"
	else
		echo -ne "\ntry:\n" >> "$app_name"
		echo -ne "\tapplication().run()\n" >> "$app_name"
		echo -ne "except Exception:\n" >> "$app_name"
		echo -ne "\tsys.exit(255)\n" >> "$app_name"
	fi
fi
