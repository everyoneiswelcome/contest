#!/usr/bin/env python3

from Crypto.Cipher import AES
from Crypto.Util import Counter
from Crypto import Random
import binascii, hashlib, hmac, time
from operator import xor

class encsock_help(object):

	_message_len = 2
	_nonce_len = 16
	_timestamp_len = 4
	_header_len = 54
	_iv_len = 16
	_mac_hash_algo = hashlib.sha256
	_mac_hash_len = 32 #int(256/8) # this value is the length of _mac_hash_algo in bytes
	_counter_len = 128
	_encryption_keysize = 16
	_cardfile_size = 10
	_encoding = "ASCII"

	# stderr writing
	def error(self, text, code=False, newline=True):
		# commented out for super speed
		if newline:
			sys.stderr.write("Error: {}\n".format(text))
		else:
			sys.stderr.write("Error: {}".format(text))
		
		sys.stderr.flush()

		if code is not False:
			sys.exit(code)

	@staticmethod
	def gen_rand(rand_len):
		random_obj = Random.new()
		rand_bytes = random_obj.read(rand_len)
		random_obj.close()
		return rand_bytes

	# integer to binary length
	@staticmethod
	def i2b_len(length, pad_len):
		# convert to hex
		hex_length = hex(length)[2:]

		# pad with single extra zero on left if necessary
		if len(hex_length)%2 == 1:
			hex_length = "0" + hex_length

		# return hex as binary, and padd with null bytes on left
		binret = binascii.unhexlify(hex_length)
		len_binret = len(binret)
		if len_binret < pad_len:
			binret = b'\x00'*(encsock_help._message_len-len_binret) + binret

		return binret

	# binary to integer length
	# (much easier than i2b...)
	@staticmethod
	def b2i_len(binlength, expected_len):
		return int(binascii.hexlify(binlength), 16)

	# easy mac method wrapper (HMAC-SHA256)
	@staticmethod
	def easy_mac(message, binary=False):
		if type(message) == list:
			message = b"".join(message)

		if binary is False:
			message = message.encode(encsock_help._encoding)
		return hmac.new(
			encsock_help.encryption_key, message, encsock_help._mac_hash_algo
		).digest()

	# recv data from socket
	@staticmethod
	def socket_recv(sock):
		header = sock.recv(encsock_help._header_len, socket.MSG_WAITALL)
		if header is False or len(header) != encsock_help._header_len:
			encsock_help.error("Bad length", 63)

		read_length = header[0:2]
		nonce = header[2:18]
		timestamp = header[18:22]
		header_mac = header[22:54]

		mac_compare = encsock_help.easy_mac([read_length, nonce, timestamp], binary=True)
		if not mac_compare == header_mac:
			raise socket.error

		# decode ints
		read_length = encsock_help.b2i_len(read_length, encsock_help._message_len)
		timestamp = encsock_help.b2i_len(timestamp, encsock_help._timestamp_len)

		# backup length
		orig_read_length = read_length

		# read data until we can't
		ret_data = b""
		while read_length > 4096:
			ret_data += sock.recv(4096)
			read_length -= 4096

		# read the rest of it
		if read_length > 0:
			ret_data += sock.recv(read_length, socket.MSG_WAITALL)

		# grab encrypted data and mac
		enc_data = ret_data[0:orig_read_length-encsock_help._mac_hash_len]
		mac_data = ret_data[-encsock_help._mac_hash_len:]

		# compare
		mac_compare = encsock_help.easy_mac([nonce, enc_data], binary=True)
		if not mac_compare == mac_data:
			raise socket.error

		# return
		data = encsock_help.aes_decrypt(enc_data, nonce)
		return (nonce, timestamp, data)

	@staticmethod
	def aes_decrypt(data, nonce):
		#iv_data = data[0:encsock_help._iv_len]
		#data = data[encsock_help._iv_len:]

		ephemeral_key = []
		for i in range(0, encsock_help._nonce_len):
			ephemeral_key.append(xor(nonce[i], encsock_help.encryption_key[i]))
		ephemeral_key = bytes(ephemeral_key)

		aes_obj = AES.new(
			key=ephemeral_key,
			mode=AES.MODE_CTR,
			counter=Counter.new(encsock_help._counter_len)
			#mode=AES.MODE_CBC, IV=iv_data
		)

		return encsock_help.unpad(aes_obj.decrypt(data).decode())

	@staticmethod
	def aes_encrypt(data, nonce):
		#iv_data = encsock_help.gen_rand(encsock_help._iv_len)

		ephemeral_key = []
		for i in range(0, encsock_help._nonce_len):
			ephemeral_key.append(xor(nonce[i], encsock_help.encryption_key[i]))
		ephemeral_key = bytes(ephemeral_key)

		aes_obj = AES.new(
			key=ephemeral_key,
			#mode=AES.MODE_CBC, IV=iv_data
			mode=AES.MODE_CTR,
			counter=Counter.new(encsock_help._counter_len)
		)

		#enc_data = b"".join([iv_data, aes_obj.encrypt(encsock_help.pad(data))])
		enc_data = aes_obj.encrypt(encsock_help.pad(data))
		return enc_data

	# pad data before encryption
	@staticmethod
	def pad(data):
		padder = 16 - len(data)%16
		if padder == 0: padder = 16
		pad_chr = bytes([padder])
		data += pad_chr*padder
		return data

	# unpad data before encryption
	@staticmethod
	def unpad(data):
		data = data[:-ord(data[-1])]
		return data

	# send data to socket
	@staticmethod
	def socket_send(sock, data):
		bin_data = data.encode(encsock_help._encoding)

		# generate nonce
		nonce = encsock_help.gen_rand(encsock_help._nonce_len)

		# encrypt-then-mac
		enc_data = encsock_help.aes_encrypt(bin_data, nonce)
		mac_data = encsock_help.easy_mac([nonce, enc_data], binary=True)

		# protocol headers
		timestamp = encsock_help.i2b_len(int(time.time()), encsock_help._timestamp_len)
		length = encsock_help.i2b_len(
			len(enc_data) + encsock_help._mac_hash_len, encsock_help._message_len
		)
		header_mac = encsock_help.easy_mac([length, nonce, timestamp], binary=True)

		# send it away
		bin_stream = b"".join([length, nonce, timestamp, header_mac, enc_data, mac_data])
		sock.send(bin_stream)
