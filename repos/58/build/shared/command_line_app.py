#!/usr/bin/env python3

import sys

class cmdline_app(object):
	_delimiter = "|"
	_argvalid = {}
	_argrequired = []
	arguments = {}
	encryption_key = None

	def __init__(self):
		pass

	# implements the forced stdout flushing
	def print(self, text, newline=True):
		if newline:
			sys.stdout.write(text + "\n")
		else:
			sys.stdout.write(text)

		sys.stdout.flush()

	# stderr writing
	def error(self, text, code=False, newline=True):
		# commented out for super speed
		if newline:
			sys.stderr.write("Error: {}\n".format(text))
		else:
			sys.stderr.write("Error: {}".format(text))
		
		sys.stderr.flush()

		if code is not False:
			sys.exit(code)

	# registers an argument as something to be expected
	def register_arg(self, argswitch, validator, required=False):
		self._argvalid[argswitch] = validator
		if required:
			self._argrequired.append(argswitch)

	# parse the input
	def parse(self):
		# prepopulate boolean values with False
		for argswitch,validator in self._argvalid.items():
			if validator == bool:
				self.arguments[argswitch] = False

		# prepare to loop input
		argswitch = False
		value_expected = False
		defined_args = {}

		# LOOP
		for i in range(1, len(sys.argv)):
			# POSIX validation requirement, no argument greater than
			# 4096 characters
			len_sys_argv_i = len(sys.argv[i])
			if len_sys_argv_i > 4096:
				self.error("An argument greater than 4096.", 255)

			# previous argument was a switch that expects input
			if value_expected is not False:
				valid,value = self._argvalid[argswitch](sys.argv[i])
				if not valid:
					self.error("Validation of command value failed", 255)

				defined_args[argswitch] = value
				value_expected = False
				continue

			# this is a switch
			if sys.argv[i][0:1] == "-":
				value_expected = False

				# if it's just a dash, then how is that valid?
				if len_sys_argv_i <= 1:
					self.error("Just a dash, invalid POSIX", 255)

				# loop each argument switch (there can be multiple)
				for j in range(1, len_sys_argv_i):
					argswitch = sys.argv[i][j]

					# if its not a registered argument, it's not valid
					if argswitch not in self._argvalid:
						self.error("Unknown Argument", 255)

					# check to see if everything is valid
					validator = self._argvalid[argswitch]
					if validator == bool:
						if argswitch in defined_args:
							self.error("Boolean switch specified twice", 255)

						defined_args[argswitch] = True

					# else, quit this loop and record where we are in the arg
					else:
						value_expected = j+1
						break

				# if we are inside an argument and expect a value, let's see
				# if we can grab it right now ("-i4000")
				if (
					value_expected is not False and
					value_expected < len_sys_argv_i
				):
					value = sys.argv[i][value_expected:]

					# see if the remaining value that we found is valid
					valid,value = validator(value)
					if not valid:
						self.error("Validation of command value failed", 255)
					else:
						defined_args[argswitch] = value
						value_expected = False

			else:
				self.error("Extra unknown argument", 255)

		# check if all required arguments are present
		for req in self._argrequired:
			if req not in defined_args:
				self.error("Did not specify a required argument", 255)

		# update arguments and we are ready to go!
		self.arguments.update(defined_args)
