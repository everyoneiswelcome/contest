#!/usr/bin/env python3

import re

class validators(object):

	@staticmethod
	def number(value):
		if value == "0":
			return (True, 0)

		len_value = len(value)
		if len_value == 0 or not value.isdigit() or value[0] == "0":
			return (False, False)

		return (True, int(value))

	@staticmethod
	def amount(value):
		ret = value.split(".")
		if len(ret) != 2:
			return (False, False)

		dollars, cents = ret
		valid, dollars = validators.number(dollars)
		if not valid or dollars < 0 or dollars > 4294967295:
			return (False, False)

		if len(cents) != 2 or not cents.isdigit():
			return (False, False)

		cents = int(cents)
		if cents < 0 or cents > 99:
			return (False, False)

		return (True, (value, dollars, cents))

	@staticmethod
	def port(value):
		valid, value = validators.number(value)
		if not valid or value < 1024 or value > 65535:
			return (False, False)

		return (True, int(value))

	@staticmethod
	def ip(value):
		###re_octet = "[0-9]+"
		###if not re.match(
		###	"^({octet}\.){{3}}{octet}$".format(octet=re_octet), value
		###):
		###	return (False, False)

		octets = value.split(".")
		if len(octets) != 4:
			return (False, False)

		for octet in octets:
			valid, octet = validators.number(octet)
			if not valid or octet < 0 or octet > 255:
				return (False, False)

		return (True, value)

	@staticmethod
	def file(value):
		len_value = len(value)
		if (
			len_value < 1 or len_value > 255 or
			value == "." or value == ".." or
			re.search("[^_\-\.0-9a-z]", value)
		):
			return (False, False)

		return (True, value)

	@staticmethod
	def account(value):
		len_value = len(value)
		if (
			len_value < 1 or len_value > 250 or
			re.search("[^_\-\.0-9a-z]", value)
		):
			return (False, False)

		return (True, value)

	@staticmethod
	def valid_chars(value):
		re.search("[^_\-\.0-9a-z]", value)

if __name__ == "__main__":
	# number
	assert(validators.number("0")[0])
	assert(validators.number("42")[0])
	assert(validators.number("052")[0] is False)
	assert(validators.number("0x2a")[0] is False)

	# amount
	assert(validators.amount("0.00")[0])
	assert(validators.amount("00.00")[0] is False)
	assert(validators.amount("0.000")[0] is False)
	assert(validators.amount("4294967295.99")[0])
	assert(validators.amount("4294967296.99")[0] is False)
	assert(validators.amount("1024")[0] is False)

	# port
	assert(validators.port("100")[0] is False)
	assert(validators.port("1024")[0])
	assert(validators.port("01024")[0] is False)
	assert(validators.port("65535")[0])
	assert(validators.port("65536")[0] is False)

	# ip
	assert(validators.ip("0.0.0.0")[0])
	assert(validators.ip("10.15.19.80")[0])
	assert(validators.ip("199.199.199.199")[0])
	assert(validators.ip("255.255.255.255")[0])
	assert(validators.ip("256.0.0.0")[0] is False)
	assert(validators.ip("265.0.0.0")[0] is False)
	assert(validators.ip("300.0.0.0")[0] is False)
	assert(validators.ip("010.15.19.80")[0] is False)
	assert(validators.ip("1000.15.19.80")[0] is False)

	# file
	assert(validators.file("file_name-2015.01")[0])
	assert(validators.file("file_name-2015.01=")[0] is False)
	assert(validators.file("/etc/passwd")[0] is False)
	assert(validators.file(".")[0] is False)
	assert(validators.file("..")[0] is False)
	assert(validators.file("a"*255)[0])
	assert(validators.file("a"*256)[0] is False)
	assert(validators.file("")[0] is False)

	# account
	assert(validators.account("account_name-12345.6789")[0])
	assert(validators.account("account_name-12345.6789=")[0] is False)
	assert(validators.account(".")[0])
	assert(validators.account("..")[0])
	assert(validators.account("")[0] is False)
	assert(validators.account("a"*250)[0])
	assert(validators.account("a"*251)[0] is False)

	# done, YAY
	print("All asserts passed.")
