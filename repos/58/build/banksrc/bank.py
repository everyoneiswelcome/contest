#!/usr/bin/env python3

import math, os, signal, socket
###import base64
###import multiprocessing

class application(cmdline_app):
	bind_host = "127.0.0.1" # could also be 0.0.0.0 for all interfaces...
	replay_check_nonce = []
	replay_check_ts = []
	replay_check_len = 0

	def __init__(self):
		super(application, self).__init__()
		self.register_arg("p", validators.port)
		self.register_arg("s", validators.file)
		self.parse()

		if "p" in self.arguments:
			self.port = self.arguments["p"]
		else:
			self.port = 3000
		if "s" in self.arguments:
			self.auth_file = self.arguments["s"]
		else:
			self.auth_file = "bank.auth"

		self.print("created")

	def createAuthFile(self):
		self.encryption_key = encsock_help.gen_rand(encsock_help._encryption_keysize)
		encsock_help.encryption_key = self.encryption_key

		authFile = open(self.auth_file, mode='wb')
		authFile.write(self.encryption_key)
		authFile.close()

		return authFile

	def receive(self, data_dict, address):

		if "a" not in data_dict:
			return '1|||'

		account_valid, account_name = validators.account(data_dict["a"])

		if "c" in data_dict:
			card_value = data_dict["c"]
		else:
			return '2|||'

		if "n" in data_dict:
			bal_valid, initial_balance = validators.amount(data_dict["n"])
			if not bal_valid or initial_balance[1] < 10:
				return '23|||'

			# account already exists
			try:
				acct_index = self.acct_names.index(account_name)
				return '99|||'
			except ValueError:
				pass

			# using someone else's card for your account, a core1 test
			if card_value in self.acct_cards:
				return '49|||'

			# create account
			self.acct_names.append(account_name)
			self.acct_balances.append(initial_balance)
			self.acct_cards.append(card_value)

			result = self._delimiter.join([
				"0", account_name, "initial_balance", initial_balance[0]
			])
			self.print("".join([
				'{"account":"', account_name,
				'","initial_balance":', initial_balance[0],
				'}'
			]))

		else:
			try:
				acct_index = self.acct_names.index(account_name)
			except ValueError:
				return '3|||'

			acct_balance = self.acct_balances[acct_index]
			acct_card_value = self.acct_cards[acct_index]

			if acct_card_value != card_value:
				return '42|||'

		if "d" in data_dict:
			dep_valid, deposit_amt = validators.amount(data_dict["d"])
			if not dep_valid or (deposit_amt[1] == 0 and deposit_amt[2] == 0):
				return '17|||'

			self.acct_balances[acct_index] = self.add_amounts(acct_balance, deposit_amt)

			result = self._delimiter.join([
				"0", account_name, "deposit", deposit_amt[0]
			])
			self.print("".join([
				'{"account":"', account_name,
				'","deposit":', deposit_amt[0],
				'}'
			]))

		elif "w" in data_dict:
			wd_valid, withdraw_amt = validators.amount(data_dict["w"])
			if not wd_valid or (withdraw_amt[1] == 0 and withdraw_amt[2] == 0):
				return '93|||'

			###if (
			###	withdraw_amt[1] > account_obj.balance[1] or (
			###		withdraw_amt[1] == account_obj.balance[1] and
			###		withdraw_amt[2] > account_obj.balance[2]
			###	)
			###):
			###	return {"error": 64} # whatever error you want

			if (
				withdraw_amt[1] > acct_balance[1] or (
					withdraw_amt[1] == acct_balance[1] and
					withdraw_amt[2] > acct_balance[2]
				)
			):
				return '64|||'

			self.acct_balances[acct_index] = self.sub_amounts(acct_balance, withdraw_amt)

			result = self._delimiter.join([
				"0", account_name, "withdraw", withdraw_amt[0]
			])
			self.print("".join([
				'{"account":"', account_name,
				'","withdraw":', withdraw_amt[0],
				'}'
			]))

		elif "g" in data_dict:
			result = self._delimiter.join([
				"0", account_name, "balance", acct_balance[0]
			])

			self.print("".join([
				'{"account":"', account_name,
				'","balance":', acct_balance[0],
				'}'
			]))

		return result

	def add_amounts(self, amount1, amount2):
		dollars = amount1[1] + amount2[1]
		cents = amount1[2] + amount2[2]

		if cents >= 100:
			dollars += int(cents/100)
			cents %= 100

		return self.create_amount(dollars, cents)

	def sub_amounts(self, amount1, amount2):
		dollars = amount1[1] - amount2[1]
		cents = amount1[2] - amount2[2]

		if cents < 0:
			dollars -= int(math.ceil(-cents/100.0))
			cents %= 100

		return self.create_amount(dollars, cents)

	def create_amount(self, dollars, cents):
		join_list = [str(dollars), "."]

		if cents < 10:
			join_list.append("0")

		join_list.append(str(cents))

		return ("".join(join_list), dollars, cents)

	def spawn_thread(self, socket_conn, address):
		try:
			self.socket_conn = socket_conn
			(nonce, timestamp, data) = encsock_help.socket_recv(socket_conn)

			current_timestamp = int(time.time())

			while (
				self.replay_check_len > 0 and
				self.replay_check_ts[0] < current_timestamp-15
			):
				self.replay_check_nonce.pop(0)
				self.replay_check_ts.pop(0)
				self.replay_check_len -= 1

			if nonce in self.replay_check_nonce or current_timestamp-15 > timestamp:
				raise SystemExit

			self.replay_check_ts.append(timestamp)
			self.replay_check_nonce.append(nonce)
			self.replay_check_len += 1

			acct_name, card_value, mode, val = data.split(self._delimiter)
			rcvd_dict = {"a": acct_name, "c": card_value, mode: val}

			result = self.receive(rcvd_dict, address)
			encsock_help.socket_send(socket_conn, result)

		except (socket.timeout, socket.error, SystemExit):
		#except (socket.timeout, SystemExit): # debug
			self.print("protocol_error")

		# close connection
		socket_conn.close()
		###sys.exit(0)

	def run(self):
		# create auth file
		self.createAuthFile()

		# manager
		###self.manager = multiprocessing.Manager()
		###self.accounts = self.manager.dict()
		self.acct_names = []
		self.acct_balances = []
		self.acct_cards = []

		# build socket then listen on given port
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.socket.setblocking(True)
		self.socket.bind((self.bind_host, self.port))
		self.socket.listen(5)

		# handle sigterm
		def sigterm_handler(signum, frame):
			###self.manager.shutdown()
			self.socket.shutdown(socket.SHUT_RDWR)
			self.socket.close()
			sys.exit(0)
		signal.signal(signal.SIGINT, signal.SIG_IGN)
		signal.signal(signal.SIGTERM, sigterm_handler)

		# fork new socket connections, parent starts accepting new connections
		# forever, child moves on
		while True:
			(socket_conn, address) = self.socket.accept()
			socket_conn.settimeout(10)

			#### child please
			###p = multiprocessing.Process(
			###	target=self.spawn_thread, args=(socket_conn, address)
			###)
			###p.start()

			self.spawn_thread(socket_conn, address)
