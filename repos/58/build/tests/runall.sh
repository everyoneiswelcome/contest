#!/bin/zsh
find tests -name "*.json" | sort | while read line; do
	time ./tests/run_test.py "$line"
done
