#!/usr/bin/env python3

import sys, getopt, socket
import simplejson


class application(cmdline_app):

	def __init__(self):
		super(application, self).__init__()
		self.register_arg("s", validators.file)
		self.register_arg("i", validators.ip)
		self.register_arg("p", validators.port)
		self.register_arg("c", validators.file)
		self.register_arg("a", validators.account)
		self.register_arg("n", validators.amount)
		self.register_arg("g", bool, required=True)
		self.parse()
		
	def run(self):
		#self.socket = socket(AF_INET, SOCK_STREAM)
		#self.socket.connect(self.ip_address, self.port) 
		## The while loop for rcv data from the bank - TBD		

		## somewhere, sometime, we will receive the data in dict format
		## then, just print the result
		## ordering seem to change
		#print(simplejson.JSONEncoder().encode(rcvd_data))
		print("Program Output: {}".format(self.arguments))
