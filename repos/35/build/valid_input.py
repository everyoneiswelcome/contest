# -*- coding: utf-8 -*-
import argparse
import sys
import re
import decimal
from utils import log

__author__ = 'dror asaf'

file_str_pattern = re.compile("^[_\-\.0-9a-z]{1,255}$")
str_pattern = re.compile("^[_\-\.0-9a-z]{1,250}$")
ip_pattern = re.compile("^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$")
INCORRECT_FILE_STR = [".", ".."]


class AtmStoreTrue(argparse.Action):
    def __init__(self, option_strings, dest, const=True, default=None,
                 required=False, help=None, metavar=None, **kwargs):
        super(AtmStoreTrue, self).__init__(option_strings, dest, nargs=0,
            const=const, default=default, required=required,
            help=help, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        log ("this is the namespace:", namespace)
        if len(values) != 0:
            raise argparse.ArgumentTypeError("value is not supported")
	value = getattr(namespace, self.dest)
	if value is True:
            raise argparse.ArgumentTypeError("argument already specified")
	    return
        setattr(namespace, self.dest, self.const)


class AtmStore(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, const=None,
                 default=None, type=None, choices=None, required=False,
                 help=None, metavar=None):
        if nargs == 0:
            raise ValueError("nargs for store actions must be greater than 0")
        super(AtmStore, self).__init__(option_strings, dest, nargs,
            const=const, type=type, default=default, required=required,
            choices=choices, help=help, metavar=metavar)

    def __call__(self, parser, namespace, values, option_string=None):
	value = getattr(namespace, self.dest)
	if value is True:
            raise argparse.ArgumentTypeError("argument already specified")
	    return

        if self.type is None:
            raise argparse.ArgumentTypeError("No type is specified")
	    return

        setattr(namespace, self.dest, values)


def valid_file_str(string):
    pattern = file_str_pattern.match(string)
    specials = string in INCORRECT_FILE_STR
    return pattern and not specials


def valid_str(string):
    log("str ", string)
    return str_pattern.match(string)


def valid_ip(string):
    match = ip_pattern.match(string)
    if match:
	array = string.split('.')
	for i in range(len(array)):
	    if int(array[i]) > 255 or int(array[i]) < 0:
		return Flase
	return True
    return False


def valid_port(port_num):
    max_port = 65535
    min_port = 1024
    if port_num < min_port or port_num > max_port:
        return False
    return True


def float_2precision(string):
    max_int = 4294967295
    max_cents = 99
    valid_float = re.compile("^(0|[1-9]([0-9])*)\.[0-9]{2}$")
    if not valid_float.match(string):
	log("string is incorrect")
    	msg = "string is not a valid float" % string
	raise argparse.ArgumentTypeError(msg)
    array = string.split('.')
    if float(array[0]) > max_int or float(array[0]) < 0:
    	msg = "string is not a valid float" % string
	raise argparse.ArgumentTypeError(msg)
    if float(array[1]) > max_cents or float(array[1]) < 0:
    	msg = "string is not a valid float" % string
	raise argparse.ArgumentTypeError(msg)

    return decimal.Decimal(float(array[0])) + decimal.Decimal(float(array[1]) * (10 ** -2))


def number(string):
    """
    does not allow leading zeroes, verify according to regular expression
    """
    valid_float = re.compile("^([1-9]([0-9])*)")
    if not valid_float.match(string):
	log("string is incorrect")
    	msg = "string is not a valid port number" % string
	raise argparse.ArgumentTypeError(msg)
    return int(string)
 

class ArgumentParser(argparse.ArgumentParser):
    def _get_action_from_name(self, name):
    	"""
	Get Action instance registered with this parser.
	"""
	container = self._actions
        if name is None:
            return None
        for action in container:
   	    if '/'.join(action.option_strings) == name:
	        return action
   	    elif action.metavar == name:
	        return action
	    elif action.dest == name:
	        return action

    def error(self,message):
	exc = sys.exc_info()[1]
	if exc:
	    exc.argument = self._get_action_from_name(exc.argument_name)
	    raise exc
        sys.exit(255)

