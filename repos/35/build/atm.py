# -*- coding: utf-8 -*-
import argparse
import ssl
import socket
import sys
import re
import decimal
import math

import msg_pb2
from utils import log
from utils import printer
from utils import SERVERNAME
from utils import COMMON_PATH
import valid_input
import proto_helper

__author__ = 'dror asaf'

PROTOCOL_ERROR = 63


def wait_response(sslsocket):
    try:
        response = proto_helper.read_message(sslsocket, msg_pb2.TransactionResult)
        if response.res == True:
            if response.response != "":
                printer (response.response)
            else:
                sys.exit(255)
        return response
    except:
	log ("wait response: protocol error")
        sys.exit(PROTOCOL_ERROR)


def get_parts(dec):
    """
    return two parts of a decimal number
    """
    b,a = math.modf(dec)
    return int(round(a)), int(round(b * 100))


def create_new_account_buffer(account_name, card_file, initial_value):
    trans = msg_pb2.Transaction()
    
    trans.account_name = account_name
    trans.card_file = card_file
    trans.operation_type = msg_pb2.CREATE_ACCOUNT
    trans.dollars, trans.cents = get_parts(initial_value)
    return trans


def create_deposit_buffer(account_name, card_file, deposit):
    trans = msg_pb2.Transaction()

    trans.account_name = account_name
    trans.card_file = card_file
    trans.operation_type = msg_pb2.DEPOSIT
    trans.dollars, trans.cents = get_parts(deposit)
    return trans


def create_withdrawal_buffer(account_name, card_file, withdraw):
    trans = msg_pb2.Transaction()

    trans.account_name = account_name
    trans.card_file = card_file
    trans.operation_type = msg_pb2.WITHDRAW
    trans.dollars, trans.cents = get_parts(withdraw)
    return trans


def create_get_status_buffer(account_name, card_file):
    trans = msg_pb2.Transaction()

    trans.account_name = account_name
    trans.card_file = card_file
    trans.operation_type = msg_pb2.GET_STATUS
    return trans


def send_request(ssl_sock, ip, port, buf):
    ssl_sock.settimeout(10)
    try:
        ssl_sock.connect((ip, port))
        proto_helper.write_message(ssl_sock, buf)
    except:
        log("send request: protocol error")
        sys.exit(PROTOCOL_ERROR)


def handle_new_account(sslsocket, ip, port, account_name, card_file, initial_value):
    buf = create_new_account_buffer(account_name, card_file, initial_value)
    send_request(sslsocket, ip, port, buf)

    # handle pin code transaction
    try:
        response = proto_helper.read_message(sslsocket, msg_pb2.TransactionResult)
    except:
        log ("handle new account: first protocol error")
        sys.exit(PROTOCOL_ERROR)

    if response.res != True:
       log ("incorrect result")
       sys.exit(255)

    try:       
	with open(COMMON_PATH + card_file, 'wb') as f:
            f.write(response.response)
        log ("finished writing ", COMMON_PATH + card_file)
    except:
        log ("handle new account: protocol error")
        sys.exit(PROTOCOL_ERROR)

    response = wait_response(sslsocket)
    if response.res == True:
        return 0
    else:
        return 255


def handle_deposit(sslsocket, ip, port, account_name, card_file, deposit):
    buf = create_deposit_buffer(account_name, card_file, deposit)
    send_request(sslsocket, ip, port, buf)
    response = wait_response(sslsocket)
    if response.res == True:
        return 0
    else:
        return 255


def handle_withdraw(sslsocket, ip, port, account_name, card_file, withdrawal):
    buf = create_withdrawal_buffer(account_name, card_file, withdrawal)
    send_request(sslsocket, ip, port, buf)
    response = wait_response(sslsocket)
    if response.res == True:
        return 0
    else:
        return 255


def handle_get_status(sslsocket, ip, port, account_name, card_file):
    buf = create_get_status_buffer(account_name, card_file)
    send_request(sslsocket, ip, port, buf)
    response = wait_response(sslsocket)
    if response.res == True:
        return 0
    else:
        return 255


# main function
parser = valid_input.ArgumentParser(description='Use an atm for sending requests for the bank',
                                    formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('-s', metavar='auth-file', type=str, nargs='?',
                    default="bank.auth", action=valid_input.AtmStore,
                    help='The name of the auth file.')
parser.add_argument('-i', metavar='ip-address', type=str, nargs='?',
                    default="127.0.0.1", action=valid_input.AtmStore,
                    help='The IP address that bank is running on')
parser.add_argument('-p', metavar='port', type=valid_input.number, nargs='?',
                    default=3000, action=valid_input.AtmStore,
                    help='The port that bank should listen on.')
parser.add_argument('-c', metavar='card-file', type=str, nargs='?',
                    action=valid_input.AtmStore,
                    help="The customer's atm card file.(default=<account name>.card)")

parser.add_argument('-a', metavar='account name', type=str,
                     action=valid_input.AtmStore, help='The account name')
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('-n', metavar='balance', type=valid_input.float_2precision,
                    action=valid_input.AtmStore,
                    help='Create a new account with the given balance. The account must be unique.')
group.add_argument('-d', metavar='amount', type=valid_input.float_2precision,
                    action=valid_input.AtmStore,
                    help='Deposit the amount of money specified.')
group.add_argument('-w', metavar='amount', type=valid_input.float_2precision,
                    action=valid_input.AtmStore,
                    help='Withdraw the amount of money specified.')
group.add_argument('-g', help='Get the current balance of the account.',
                   action=valid_input.AtmStoreTrue)

try:
    args = parser.parse_args()
except:
    sys.exit(255)

if not valid_input.valid_port(args.p):
    log ("Incorrect port number", args.p)
    sys.exit(255)

if not valid_input.valid_file_str(args.s):
    log ("Incorrect authentication file name", args.s)
    sys.exit(255)

if not valid_input.valid_str(args.a):
    log ("Incorrect account name", args.a)
    sys.exit(255)

if not valid_input.valid_ip(args.i):
    log ("Incorrect ip address", args.i)
    sys.exit(255)

# handle missing card file name - use default <account_name>.card
if args.c is None:
    args.c = args.a + ".card"

if not valid_input.valid_file_str(args.c):
    log ("Incorrect card file name", args.c)
    sys.exit(255)

decimal.getcontext().prec = 2
sslcontext = ssl.create_default_context(cafile=args.s)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

sslsocket = sslcontext.wrap_socket(s, server_hostname=SERVERNAME)

if args.n is not None:
    log ("handle new account")
    result = handle_new_account(sslsocket, args.i, args.p, args.a, args.c, args.n)

if args.d is not None:
    log ("handle deposit")
    result = handle_deposit(sslsocket, args.i, args.p, args.a, args.c, args.d)

if args.w is not None:
    log ("handle withdraw")
    result = handle_withdraw(sslsocket, args.i, args.p, args.a, args.c, args.w)

if args.g:
    log ("handle get status")
    result = handle_get_status(sslsocket, args.i, args.p, args.a, args.c)

sslsocket.close()
sys.exit(result)

