# -*- coding: utf-8 -*-
import struct

__author__ = 'dror asaf'

def socket_read_n(connstream, n):
    """
    read exactly n bytes from SSL socket
    Raises exception otherwise
    """
    buf = ''
    while n > 0:
        data = connstream.read(n)
	#TODO: define fine tuned excetion
	if data == '':
	    raise RuntimeError('unexpected connection')
 	buf += data
	n -= len(data)
    return buf


def write_message(connstream, buf):
    msg = buf.SerializeToString()
    packed_len = struct.pack('>L', len(msg))
    # TODO: what do if write fails?
    connstream.write(packed_len + msg)


def read_message(connstream, msgtype):
    """
    Read a message from SSL socket  
    """
    len_buf = socket_read_n(connstream, 4)
    msg_len = struct.unpack('>L', len_buf)[0]
    msg_buf = socket_read_n(connstream, msg_len)

    msg = msgtype()
    msg.ParseFromString(msg_buf)
    return msg

