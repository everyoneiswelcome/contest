# -*- coding: utf-8 -*-
import argparse
import ssl
import socket
import sys
import simplejson as json
import decimal
import random
import os
import signal
from OpenSSL import crypto

import msg_pb2
from utils import log
from utils import printer
from utils import SERVERNAME
from utils import COMMON_PATH
import valid_input
import proto_helper


__author__ = 'dror asaf'

TWOPLACES = decimal.Decimal(10) ** -2
PROTOCOL_ERROR_STR = "protocol_error"
KEYFILE = "bank.key"
KEYSIZE = 2048
filename = None
bindsocket = None
accounts = {}
# concurrency is not supported only a single atm is valid at a time
active_users = []

def valid_account(account_name):
    return account_name in accounts.keys()


def valid_card_file(account_name, card_file):
    log ('valid card file: entered')
    try:
        with open(COMMON_PATH + card_file, 'rb') as f:
            buff = f.read()
            return accounts[account_name]['pin_code'] == buff
    except:
        log ('unable to open file: ', card_file)
        return False


def create_self_signed_cert(cert_file, key_file):
    log ('create self signed cert: entered')
    k = crypto.PKey() 
    k.generate_key(crypto.TYPE_RSA, KEYSIZE)

    cert = crypto.X509()
    cert.get_subject().C = "US"
    cert.get_subject().ST = "aaaaaa"
    cert.get_subject().L = "bbbbbb"
    cert.get_subject().O = "cccccc"
    cert.get_subject().OU = "OrbitShield"
    cert.get_subject().CN = SERVERNAME

    # TODO: randomize serial number?
    cert.set_serial_number(1000)
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(60*60*24*365)
    cert.set_issuer(cert.get_subject())
    cert.set_issuer(cert.get_subject())
    cert.set_pubkey(k)
    cert.sign(k, 'sha1')

    with open(key_file, "wt") as f:
        f.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, k))
    with open(cert_file, "wt") as f:
        f.write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert))
    log ('create self signed cert: Done')


def send_response(connstream, result):
    """
    encapsulates the result into a protobuf and send it over the connstream
    """
    error = 0
    trans = msg_pb2.TransactionResult()
    trans.res = result[0] 
    if result[0] == True:
        if len(result) > 1:
            printer (result[1])
            trans.response = result[1]

    try:
        proto_helper.write_message(connstream, trans)
    except:
        log("failed writing message")
        error = 1
    if error:
        printer (PROTOCOL_ERROR_STR)
        return False

    return True


def create_card_file(card_file_name):
    num = random.randint(1, 2 ** 31)
    return str(num)


def create_account(connstream, account_name, card_file, value):
    if value < 10.0:
        return False, 

    if account_name in accounts.keys():
        return False,

    for acc in accounts.keys():
        if accounts[acc]['card'] == card_file:
            return False,

    pin_code = create_card_file(card_file)
    trans = msg_pb2.TransactionResult()
    trans.res = True 
    trans.response = pin_code

    try:
        proto_helper.write_message(connstream, trans)
    except:
        printer (PROTOCOL_ERROR_STR)
        return False

    accounts[account_name] = {'amount' : value, 'pin_code': pin_code, 'card': card_file}
    data= {}
    data['account'] = account_name
    data['initial_balance'] = value.quantize(TWOPLACES)
    return True, json.dumps(data)


def get_status(connstream, account_name, card_file, value):
    if not valid_account(account_name):
        log ("get_status: invalid account", account_name)
        return False, 

    if not valid_card_file(account_name, card_file):
        log ("get_status: invalid card file", account_name, card_file)
        return False, 

    data= {}
    data['account'] = account_name
    data['balance'] = accounts[account_name]['amount']
    return True, json.dumps(data)


def withdraw(connstream, account_name, card_file, value):
    if value <= 0.0:
        log ("withdraw: invalid amount", value)
        return False, 

    if not valid_account(account_name):
        log ("withdraw: invalid account", account_name)
        return False, 

    if not valid_card_file(account_name, card_file):
        log ("withdraw: invalid card file", account_name, card_file)
        return False, 


    if value > accounts[account_name]['amount']:
        log ("withdraw more than account has", value, accounts[account_name]['amount'])
        return False, 

    new_value = accounts[account_name]['amount'] - value

    accounts[account_name]['amount'] = new_value
    data= {}
    data['account'] = account_name
    data['withdraw'] = value.quantize(TWOPLACES)
    return True, json.dumps(data)


def deposit(connstream, account_name, card_file, value):
    if value <= 0.0:
        return False, 

    if not valid_account(account_name):
        log ("deposit: invalid account", account_name)
        return False, 

    if not valid_card_file(account_name, card_file):
        log ("deposit: invalid card file", account_name, card_file)
        return False, 

    new_value = accounts[account_name]['amount'] + value
    accounts[account_name]['amount'] = new_value
    data= {}
    data['account'] = account_name
    data['deposit'] = value.quantize(TWOPLACES)
    return True, json.dumps(data)


def delete_account(connstream, account_name, card_file, value):
    if account_name not in accounts.keys():
        return False,

    del accounts[account_name]
    return False,


def noop(connstream, account_name, card_file, value):
    return True,


def handle_operation(connstream, account_name, card_file, typ, value):
    operations = {
        msg_pb2.CREATE_ACCOUNT: create_account,
        msg_pb2.DEPOSIT : deposit,
        msg_pb2.WITHDRAW : withdraw,
        msg_pb2.GET_STATUS : get_status
        }
    rollbacks = {
        msg_pb2.CREATE_ACCOUNT: delete_account,
        msg_pb2.DEPOSIT : withdraw,
        msg_pb2.WITHDRAW : deposit,
        msg_pb2.GET_STATUS : noop
        }

    if not valid_input.valid_file_str(card_file):
        send_response(connstream, (False, ))
        return

    if not valid_input.valid_str(account_name):
        send_response(connstream, (False, ))
        return

    if account_name in active_users:
        send_response(connstream, (False, ))
        return

    active_users.append(account_name)

    log("handle_operation: ", str(typ), value)
    result = operations[typ](connstream, account_name, card_file, value)
    res = send_response(connstream, result)
    if not res:
        rollbacks[typ](connstream, account_name, card_file, value)

    active_users.remove(account_name)


def deal_with_client(connstream):
    connstream.settimeout(10)
    try:
        msg = proto_helper.read_message(connstream, msg_pb2.Transaction)
    except:
	log ("failed reading messages")
        printer (PROTOCOL_ERROR_STR)
        return

    log("Dollars:", msg.dollars, msg.cents)
    handle_operation(connstream, msg.account_name, msg.card_file, msg.operation_type,
                     decimal.Decimal(msg.dollars) + decimal.Decimal(msg.cents) * TWOPLACES)


def exit_gracefully(signum, frame):
    try:
        bindsocket.shutdown(socket.SHUT_RDWR)
        bindsocket.close()
    except:
        pass
    try:
        os.remove(KEYFILE)
    except:
        pass
    try:
        os.remove(filename)
    except:
        pass
    sys.exit(0)


# main function
parser = valid_input.ArgumentParser(description='Create a server to process atm requests(a.k.a. bank)',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('-p', metavar='port', type=valid_input.number, nargs='?', default=3000,
                    action=valid_input.AtmStore,
                    help='The port that bank should listen on.')
parser.add_argument('-s', metavar='auth-file', type=str, nargs='?', default="bank.auth",
                    action=valid_input.AtmStore,
                    help=' The name of the auth file.')
try:
    args = parser.parse_args()
except (argparse.ArgumentError) as exc:
    log(exc.message, '\n', exc.argument)
    sys.exit(255)
except:
    sys.exit(255)

if not valid_input.valid_port(args.p):
    log ("Incorrect port number", args.p)
    sys.exit(255)

if not valid_input.valid_file_str(args.s):
    log ("Incorrect authentication file name", args.s)
    sys.exit(255)

filename = args.s
try:
    bindsocket = socket.socket()
    bindsocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    bindsocket.bind(('', args.p))
except:
    log ("unable to bind port")
    sys.exit(255)
bindsocket.listen(5)

signal.signal(signal.SIGINT, exit_gracefully)
signal.signal(signal.SIGTERM, exit_gracefully)


# start server and listen on the specified port
sslcontext = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
try:
    create_self_signed_cert(args.s, KEYFILE)
except:
    sys.exit(255)
sslcontext.load_cert_chain(certfile=args.s, keyfile=KEYFILE)

printer ("created")
while True:
    newsocket, fromaddr = bindsocket.accept()
    newsocket.settimeout(10)
    try:
        connstream = sslcontext.wrap_socket(newsocket, server_side=True)
    except:
        log ("wrap socket failed")
        printer (PROTOCOL_ERROR_STR)
        continue
    try:
        deal_with_client(connstream)
    finally:
        try:
            connstream.shutdown(socket.SHUT_RDWR)
            connstream.close()
        except:
            pass
