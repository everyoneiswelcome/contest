#!/usr/bin/env python

# standard
import os
import socket
import sys

import time

s = None

def setup_network_com(host="127.0.0.1", port=3000):
    global s
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((host,port))
    except:
        sys.stderr.write("Could not connect to bank\n")
        os._exit(255)


#OPTIMIZATION: reduce calls to json.dumps/loads
def main():
  
    sys.stderr.write("Setup network\n")
    setup_network_com()
    
    time.sleep(15)
    
    s.close()
    sys.exit(0)

if __name__ == "__main__":
    main()
