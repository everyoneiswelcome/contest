#!/usr/bin/env python

# standard
import os
import re
import signal
import socket
import select
import sys
from decimal import *
import bisect

# en-decodings
import argparse
import base64
import json

# crypto
import libnacl.secret

s = None

MAX_BALANCE = 42949672960000

class crypto_utils:
    '''
    Initialize (cache) objects for the crypto_utils class here.
    '''
    secretbox = libnacl.secret.SecretBox()
    
    @staticmethod
    def create_auth_file(path):
        '''
        Creates the ATM-Bank cross-authentication file with a key for salsa20
        and a key for HMAC-SHA256 and saves at path. Additionally for
        user-message encryption a bank key-pair is generated and saved.
        
        Used only by bank once at each startup.
        '''
        
        to_ret = {}
        to_ret['secret_key'] = crypto_utils.secretbox.sk
        #sys.stderr.write("private secret key: " + to_ret['secret_key'] + "\n")
        
        to_save = {}
        to_save['secret_key'] = base64.b64encode(to_ret['secret_key'])
        
        with open(path, 'wb') as auth_file:
            auth_file.write(json.dumps(to_save, sort_keys=True))
            
        return to_ret


    @staticmethod
    def encrypt_then_mac(secret_key, payload_as_string):
        '''
        Encrypts the payload with the secret_key with salsa20 and then macs with
        the ciphertext with mac_key as HMAC-SHA256, concatenates the two with
        base64 encoded output with . as separator
        
        Used by the ATM when sending a request to the Bank.
        Used by the Bank when sending a response to the ATM.
        '''
        
        ciphertext = base64.b64encode(
          crypto_utils.secretbox.encrypt(payload_as_string)
        )
        
        return ciphertext


    @staticmethod
    def verify_then_decrypt(secret_key, ciphertext_as_base64_string):
        '''
        Decrypt the ciphertext with the secret_key with salsa20 after verifying
        the MAC of the ciphertext with mac_key as HMAC-SHA256, ciphertext and
        MAC should be concatenated the two as base64 encoded with . as separator
        
        Used by the ATM when receiving a response from the Bank.
        Used by the Bank when receiving a request from the ATM.
        '''
        
        plaintext = crypto_utils.secretbox.decrypt(base64.b64decode(ciphertext_as_base64_string))
        
        return plaintext

        
        
def signal_term_handler(signal, frame):
    global s
    #sys.stderr.write('got SIGTERM, shutting down\n')
    s.shutdown(socket.SHUT_RDWR)
    s.close()
    
    sys.exit(0)

signal.signal(signal.SIGTERM, signal_term_handler)

base64_regexp = '(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?'
valid_input = re.compile('^'+ base64_regexp + '$')

getcontext().prec = 16  # this should be enough for 10000 max value deposits for
                        # every account, as 2**32*10000 == 4.29... * 10**13
                        # i.e., per account this allows up to 42 trillion USD/EUR
                        # which would be enough in the real-world
                        # cf. http://gizmodo.com/5995301/how-much-money-is-there-on-earth
two_digits = Decimal('1.00')

keys = {}
ledger = {}

def index(a, x):
    i = bisect.bisect_left(a, x)
    if i != len(a) and a[i] == x:
        return True
    return False
    
    
used_tids = []  # this may be suboptimal, but currently we don't have a better
                # method to discover replay attacks

                
def handle_cmd_arg():
    '''
    Parses command line parameters and validates them.
    '''
    global s
    parser = argparse.ArgumentParser(
      description='This is a very secure bank server.'
    )
    parser.add_argument("-p",
                        dest='port',
                        type=int,
                        default=3000,
                        help=("The port that bank should listen on. The " +
                              "default is 3000.")
                       )
    parser.add_argument("-s",
                        dest='authfile',
                        type=str,
                        default="bank.auth",
                        help=("The name of the auth file. If not supplied, " +
                             "defaults to \"bank.auth\".")
                       )
    args = parser.parse_args() 
    #sys.stderr.write("Parsed arguments\n")
    #sys.stderr.write("\tport: " + str(args.port) + '\n')
    #sys.stderr.write("\tauth file: " + args.authfile + '\n')
    authfile_abspath = os.path.abspath(args.authfile)
    if os.path.isfile(authfile_abspath) \
      and os.access(authfile_abspath, os.R_OK):
        #sys.stderr.write("File exists and is readable, exit with 255 " +
        #                "- don\'t create the same auth file\n")
        if s != None:
            s.shutdown(socket.SHUT_RDWR)
            s.close()
        os._exit(255);
    else:
        #sys.stderr.write("Either file is missing or is not readable, " +
        #                 "create new\n")
        authfile = args.authfile
    
    port = args.port
    # port must be number between 1024 and 65535 inclusively
    if (port < 1024) or (port > 65535):
        #sys.stderr.write("Port must be a number between 1024 " +
        #                 " and 65535, exit with 255\n")
        s.shutdown(socket.SHUT_RDWR)
        s.close()
        os._exit(255);
    
    return port, authfile


def check_cmd_arg_duplicate():

    global s
	
    arg_values = dict() 
    for arg in sys.argv[1:]:
        if arg.startswith('-'):
            if arg not in arg_values:
                arg_values[arg] = 1
            else:                
                if arg_values[arg] > 0:
                    #sys.stderr.write("Duplicated argument: " + arg + "\n")
                    s.shutdown(socket.SHUT_RDWR)
                    s.close()
                    os._exit(255)


def create_auth_file(path):
    '''
    Creates the Bank-ATM cross-authentication file at the given path. If the
    file already exists, it raises an exception. If the file cannot be opened
    (e.g., file name is '.') the standard Python exception is thrown internally
    by `open`.
    
    I.e., this function should be encapsulated into a try-except block and the
    except branch should return with `sys.exit(255)` at the end.
    
    Return of this is a dictionary which contains the following keys:
        keys['secret_key'] as encryption key for communication with ATMs
        keys['mac_key'] as MAC key for communication with ATMs
        keys['bank_public_key'] the bank's public key,
                                currently used only by ATMs
        keys['bank_private_key'] the bank's private key, used for encrypting
                                 message for users
    '''
    global s
    
    if os.path.isfile(path) \
      and os.access("./" + path, os.R_OK):
        #sys.stderr.write("File " + path + " already exists\n")
        s.shutdown(socket.SHUT_RDWR)
        s.close()
        os._exit(255)

    keys = crypto_utils.create_auth_file(path)
    
    return keys


def setup_network_com(host, port):
    global s
    
    # setup socket binding and start listening
    # this is the  while True: loop for the rest of the program
    #  create socket
    #  bind socket to hostname and port
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((host,port))
    
    #  listen on socket with a queue of length 2
    s.listen(2)
    
    return


#OPTIMIZATION(?): is this even necessary?
#  If not, than we can save a regexp matching
def verify_atm_request_integrity(data):
    # verify that request is valid 'base64'.'base64'    
    if valid_input.match(data) is None:
        raise Exception('Not valid input from ATM')
    
    return


def decrypt_atm_request(data, keys):
    # decrypt valid encrypted requests
    # send decrypted ATM request to be validated,
    #   to validate_atm_request(account, action, amount)
    
    request_received = crypto_utils.verify_then_decrypt(keys['secret_key'], data)
    
    return request_received

    
def check_field_exist(check_dict, field_name):
    if field_name in check_dict.keys() and check_dict[field_name] is not None:
        return True
    else:
        return False

        
def validate_atm_request(request_as_dict):
    #sys.stderr.write(json.dumps(request_as_dict) + '\n')

    if check_field_exist(request_as_dict, 'mode') is False:
            raise Exception('Not valid request from ATM, mode field is absent')
    
    action = request_as_dict['mode']
    
    is_pass = True
    
    # validate create
    if action == "create":
        if check_field_exist(request_as_dict, 'account_name') is False:
            is_pass = False
        elif check_field_exist(request_as_dict, 'transaction_id') is False: 
            is_pass = False
        elif check_field_exist(request_as_dict, 'amount') is False:
            is_pass = False
        elif check_field_exist(request_as_dict, 'user_pin') is False:
            is_pass = False
        
        if is_pass is False:
            raise Exception('Not valid create request from ATM')
        
    # validate deposit/withdraw
    if action == "deposit" or action == "withdraw":
        if check_field_exist(request_as_dict, 'account_name') is False:
            is_pass = False
        elif check_field_exist(request_as_dict, 'transaction_id') is False: 
            is_pass = False
        elif check_field_exist(request_as_dict, 'amount') is False:
            is_pass = False
        
        if is_pass is False:
            raise Exception('Not valid deposit/withdraw request from ATM')

    # validate get
    if action == "get":
        if check_field_exist(request_as_dict, 'account_name') is False:
            is_pass = False
        elif check_field_exist(request_as_dict, 'transaction_id') is False: 
            is_pass = False
        
        if is_pass is False:
            raise Exception('Not valid get request from ATM')
            
    return


def create_account(account_name,
                   amount,
                   user_pin):
    # add entry in the memory for this account
    global ledger

    #sys.stderr.write("Printing ledger keys:\n\t")
    #for k in ledger.keys():
        #sys.stderr.write(k + ',')
    #sys.stderr.write('\n')
    if account_name in ledger:
        #sys.stderr.write("Account already exists\n")
        ret = {}
        ret['error'] = "Account already exists"
        ret['account'] = 'ERROR'
        return ret, False
    
    #sys.stderr.write("New account, because |" + \
    #  account_name + "| does not exist yet\n")
    ledger[account_name] = {}
    ledger[account_name]['balance'] = Decimal(amount).quantize(two_digits)
    ledger[account_name]['user_pin'] = user_pin
    
    ret = {}
    ret['account'] = account_name
    ret['initial_balance'] =  "%.2f" % amount
    
    return ret, True


def deposit(account_name, amount):
    # add amount to the account
    global ledger
    
    quantized = Decimal(amount).quantize(two_digits)
    if (ledger[account_name]['balance'] + quantized > MAX_BALANCE):
        #sys.stderr.write("Max account balance reached\n")
        ret = {}
        ret['account'] = 'ERROR'
        ret['error'] = "Max account balance reached"
        return ret, False
    
    ledger[account_name]['balance'] += Decimal(amount)
    ledger[account_name]['balance'] = ledger[account_name]['balance'].quantize(
      two_digits
    )
    
    ret = {}
    ret['account'] = account_name
    ret['amount'] =  "%.2f" % amount
    
    return ret, True


def withdraw(account_name , amount):
    global ledger
    
    quantized = Decimal(amount).quantize(two_digits)
    if (ledger[account_name]['balance'] < quantized):
        #sys.stderr.write("Not enough funds\n")
        ret = {}
        ret['account'] = 'ERROR'
        ret['error'] = "Not enough funds"
        return ret, False

    
    ledger[account_name]['balance'] -= quantized
    ledger[account_name]['balance'] = ledger[account_name]['balance'].quantize(
      two_digits
    )
    
    ret = {}
    ret['account'] = account_name
    ret['amount'] = "%.2f" % amount
    
    return ret, True


def get_balance(account_name):
    global ledger
    
    ret = {}
    ret['account'] = account_name
    ret['balance'] = "%.2f" % Decimal(ledger[account_name]['balance'])
    
    return ret


def encrypt_atm_reply(reply, keys, account_name):
    # get return value from bank action (create/deposit/withdraw/balance)
    # create the encrypted return message and its hash
    
    response_to_send = crypto_utils.encrypt_then_mac(
      keys['secret_key'],
      json.dumps(reply)
    )
    
    return response_to_send


def main():
    global s, ledger
    host = "127.0.0.1"
    check_cmd_arg_duplicate()
    port, authfile = handle_cmd_arg()
    #sys.stderr.write("Creating authentication file\n")
    keys = create_auth_file(authfile)
    #sys.stderr.write("Setup network\n")
    setup_network_com(host, port)
    print "created"
    sys.stdout.flush()
    
    #sys.stderr.write("Every setup OK, starting to listen for requests\n")

    while True:
        connection, client_address = s.accept()

        connection.setblocking(0)

        #sys.stderr.write("connection from: " + str(client_address) + '\n')
        ready = select.select([connection], [], [], 10)
        if ready[0]:
            data = connection.recv(1024)
        else:
            print 'protocol_error'
            sys.stdout.flush()
            continue
        
        if data:
            success = True
            try:
                verify_atm_request_integrity(data)
            except:
                #sys.stderr.write("Incorrect request")
                action = "protocol_error"
                reply = {}
                reply['error'] = "ATM request not in correct form"
                reply['account'] = 'ERROR'
                reply['protocol_error'] = True
                success = False
            
            try:
                if success is True:
                    request_received = json.loads(decrypt_atm_request(data, keys))
            except:
                #sys.stderr.write("Could not verifiably decrypt request")
                action = "protocol_error"
                reply = {}
                reply['error'] = "ATM request could not be verifiably decrypted"
                reply['account'] = 'ERROR'
                reply['protocol_error'] = True
                success = False
            
            try:
                if success is True:
                    validate_atm_request(request_received)
            except:
                #sys.stderr.write("Incorrect request")
                action = "protocol_error"
                reply = {}
                reply['error'] = "ATM request components are not in correct form"
                reply['account'] = 'ERROR'
                reply['protocol_error'] = True
                success = False    
            
            tid = None
            if success is True:
                tid = int(request_received['transaction_id'])
                action = request_received['mode']
                if index(used_tids, tid):
                    #sys.stderr.write("Replaying transaction ID detected")
                    action = "protocol_error"
                    reply = {}
                    reply['error'] = "Transaction ID already used"
                    reply['account'] = 'ERROR'
                    reply['protocol_error'] = True
                    success = False

            if action == "protocol_error":
                #sys.stderr.write("action is " + action + "\n")
                pass
            elif action == "create":
                reply, success = create_account(
                  request_received['account_name'],
                  request_received['amount'],
                  request_received['user_pin']
                )
                if success:
                    print '{"initial_balance": ' + \
                      str(reply['initial_balance']) + ', "account": "' + \
                      reply['account'] + '"}'
                    sys.stdout.flush()
            else: # non-create action
                if request_received['account_name'] not in ledger:
                    #sys.stderr.write("Unknown account, returning error\n")
                    reply = {}
                    reply['account'] = 'ERROR'
                    reply['error'] = "Account does not exist"
                else: # account is existing
                    #sys.stderr.write(
                    #  "current balance is " + str(
                    #    ledger[request_received['account_name']]['balance']
                    #  ) + '\n'
                    #)
                    if ledger[request_received['account_name']]['user_pin'] != request_received['user_pin']:
                        #sys.stderr.write("Incorrect PIN (stored: " + str(ledger[request_received['account_name']]['user_pin']) + " vs. received " + str(request_received['user_pin']) + ")\n")
                        action = "fail"
                        reply = {}
                        reply['account'] = 'ERROR'
                        reply['error'] = "PIN error"
                    
                    if action == "deposit":
                        reply, success = deposit(request_received['account_name'],
                                        request_received['amount']
                                       )
                        if success:
                            print '{"account": "' + reply['account'] + '", ' + \
                              '"deposit": ' + str(reply['amount']) + '}'
                            sys.stdout.flush()
                    elif action == "withdraw":
                        reply, success = withdraw(
                          request_received['account_name'],
                          request_received['amount']
                        )
                        if success:
                            print '{"account": "' + reply['account'] + '", ' + \
                              '"withdraw": ' + str(reply['amount']) + '}'
                            sys.stdout.flush()
                    elif action == "get":
                        reply = get_balance(request_received['account_name'])
                        print '{"account": "' + reply['account'] + '", ' + \
                          '"balance": ' + str(reply['balance']) + '}'
                        sys.stdout.flush()
                        success = True
                    elif action != "fail":
                        #sys.stderr.write("Unknown action: " + action + '\n')
                        print 'protocol_error'
                        sys.stdout.flush()
                        success = False
            if tid is not None:
                reply['transaction_id'] = request_received['transaction_id']
                bisect.insort(used_tids, tid)
            
            reply_to_send = encrypt_atm_reply(reply,
                                              keys,
                                              reply['account'])
            
            #sys.stderr.write(reply_to_send + '\n')
                                              
            if 'protocol_error' in reply:
                print 'protocol_error'
                sys.stdout.flush()
                                              
            try:
                connection.send(reply_to_send)
            except socket.timeout:
                #sys.stderr.write("Timeout occured")
                print 'protocol_error'
                sys.stdout.flush()
        #else:
            #sys.stderr.write("Empty request received")


if __name__ == '__main__':
    
    try:
        main()
    except KeyboardInterrupt:
        pass
    finally:
        if s != None:
            s.shutdown(socket.SHUT_RDWR)
            s.close()