#!/usr/bin/env python

# standard
import os
import re
import socket
import sys

# en-decodings
import argparse
import base64
import json

# crypto
import libnacl.secret

class crypto_utils:

    secretbox = None

    @staticmethod
    def load_auth_file(path):
        '''
        Loads the ATM-Bank cross-authentication file with a key for salsa20 and
        a key for HMAC-SHA256 from file at path. Additionally for user-message
        encryption a bank key-pair is loaded.
        
        Used only by ATM once at each startup.
        '''
        
        with open(path, 'rb') as auth_file:
            to_load = json.loads(auth_file.read())
        
        keys = {}
        keys['secret_key'] = base64.b64decode(to_load['secret_key'])
        
        crypto_utils.secretbox = libnacl.secret.SecretBox(keys['secret_key'])
        
        return keys


    @staticmethod
    def create_user_card(path):
        '''
        Creates the user's card file, which contains two key-pairs, one for
        encryption/decryption over Curve25519 and one for signing/verification
        over Ed25519, saves to file at path
        
        Used by ATM once when creating a new user.
        '''
        
        ret_user = {}
        ret_user['user_pin'] = libnacl.randombytes_random()

        #sys.stderr.write("Creating user card at " + path + "\n")
        with open(path, 'wb') as card_file:
            card_file.write(json.dumps(ret_user, sort_keys=True))
        
        return ret_user


    @staticmethod
    def load_user_card(path):
        '''
        Loads the user's card file, which contains two key-pairs, one for
        encryption/decryption over Curve25519 and one for signing/verification
        over Ed25519 from file at path
        
        Used by ATM once when doing something for an existing user.
        '''
        
        with open(path, 'r') as card_file:
            raw_user = json.loads(card_file.read())
            
        user = {}
        user['user_pin'] = raw_user['user_pin']
        
        return user


    @staticmethod
    def encrypt_then_mac(secret_key, payload_as_string):
        '''
        Encrypts the payload with the secret_key with salsa20 and then macs with
        the ciphertext with mac_key as HMAC-SHA256, concatenates the two with
        base64 encoded output with . as separator
        
        Used by the ATM when sending a request to the Bank.
        Used by the Bank when sending a response to the ATM.
        '''
        
        ciphertext = base64.b64encode(
          crypto_utils.secretbox.encrypt(payload_as_string)
        )
        
        return ciphertext


    @staticmethod
    def verify_then_decrypt(secret_key, ciphertext_as_base64_string):
        '''
        Decrypt the ciphertext with the secret_key with salsa20 after verifying
        the MAC of the ciphertext with mac_key as HMAC-SHA256, ciphertext and
        MAC should be concatenated the two as base64 encoded with . as separator
        
        Used by the ATM when receiving a response from the Bank.
        Used by the Bank when receiving a request from the ATM.
        '''
        
        plaintext = crypto_utils.secretbox.decrypt(base64.b64decode(ciphertext_as_base64_string))

        return plaintext



valid_number = re.compile('^(0|[1-9][0-9]*)\.[0-9]{2}$')
valid_account = re.compile('^[_\-\.0-9a-z]+$')
valid_ip = re.compile('^((0|[1-9][0-9]{0,2})\.){3}(0|[1-9][0-9]{0,2})$')
valid_port = re.compile('^[1-9][0-9]{3,4}$')
socket.setdefaulttimeout(10.0)

def handle_cmd_arg():
    '''
    Parses command line parameters and validates them.
    '''
    parser=argparse.ArgumentParser(
      description='This is a very secure ATM client.'
    )
    
    # Optional arguments
    parser.add_argument("-p",
                        dest='port',
                        type=str,
                        default='3000',
                        help=("The port that bank should listen on. The " +
                              "default is 3000.")
                       )
    parser.add_argument("-s",
                        dest='authfile',
                        type=check_file,
                        default="bank.auth",
                        help=("The name of the auth file. If not supplied, " +
                              "defaults to \"bank.auth\".")
                       )
    parser.add_argument("-i",
                        dest='ip_address',
                        type=str,
                        default="127.0.0.1",
                        help=("The IP address that bank is running on. The " +
                              "default value is \"127.0.0.1\".")
                       )
    parser.add_argument("-c",
                        dest='cardfile',
                        type=check_file,
                        help=("The customer's atm card file. The default " +
                              "value is the account name prepended to " +
                              "\".card\" (\"<account>.card\").")
                       )
    
    # Mandatory arguments
    parser.add_argument("-a",
                        dest='account',
                        type=validate_account,
                        help="The customer's account name."
                       )
    
    # Modes of operation    
    parser.add_argument("-n",
                        dest='balance',
                        type=validate_balance,
                        help=("Create a new account with the given balance. " +
                              "The amount must be greater than 10.00")
                       )
    parser.add_argument("-d",
                        dest='deposit_amount', 
                        type=str,
                        help=("Deposit the amount of money specified. The " +
                              "amount must be greater than 0.00")
                       )
    parser.add_argument("-w",
                        dest='withdraw_amount',
                        type=str,
                        help=("Withdraw the amount of money specified. The " +
                              "amount must be greater than 0.00")
                       )
    parser.add_argument("-g",
                        dest='get',
                        action='store_true',
                        help=" Get the current balance of the account."
                       )
    
    try:
        args=parser.parse_args() 
    except:
        #sys.stderr.write("Problem during parsing the args\n")
        os._exit(255);
    
    #check if exactly one mode from -n, -d, -w, -g is selected
    if not one_true(isinstance(args.balance, float),
                    isinstance(args.deposit_amount, str),
                    isinstance(args.withdraw_amount, str),
                    args.get):
        #sys.stderr.write("Not exactly one mode given\n")
        os._exit(255);
    
    mode = 0
    amount = 0
    if args.balance:
        mode = 1 # create new account 
        amount = args.balance
    elif args.deposit_amount :
        mode = 2 # deposite amount 
        amount = validate_amount(args.deposit_amount)
    elif args.withdraw_amount :
        mode = 3 # withdraw amount 
        amount = validate_amount(args.withdraw_amount)
    elif args.get :
        mode = 4 # get balance 
    else:
        os._exit(255);
    
    port = validate_port(args.port)
    host = validate_ip(args.ip_address)
    authfile = args.authfile
    account = args.account
    
    cardfile = account + '.card' if args.cardfile == None else args.cardfile
    
    #sys.stderr.write("Parsed arguments\n")
    #sys.stderr.write("\tport: " + str(args.port) + '\n')
    #sys.stderr.write("\tauth file: " + str(args.authfile) + '\n')
    #sys.stderr.write("\tip_address: " + str(args.ip_address) + '\n')
    #sys.stderr.write("\tcard file: " + str(args.cardfile) + '\n')
    #sys.stderr.write("\taccount name:"  + str(args.account) + '\n')
    #sys.stderr.write(("\tMode of Operation #1 create, #2 deposit, #3 withdraw,"+
    #                  " #4 check balance : ##"  +str(mode) + '\n')
    #                )
    
    return host, port, authfile, account, cardfile, mode, amount 


def validate_account(value):
    if valid_account.match(value) is None:
        #sys.stderr.write("Incorrect account name: |" + value + '|\n')
        os._exit(255)
    return value


def validate_balance(value):
    if valid_number.match(value) is None:
        #sys.stderr.write("Incorrectly formatted number input: " + value + '\n')
        os._exit(255)
    
    try:
        balance = float(value)
        if balance < 10.00:
            #sys.stderr.write("Balance can't be less than 10.00\n")
            os._exit(255);
    except ValueError:
        #sys.stderr.write("Balance value is non-float: " + value + '\n')
        os._exit(255);
    return balance 


def validate_amount(value):
    if valid_number.match(value) is None:
        #sys.stderr.write("Incorrectly formatted number input: " + value + '\n')
        os._exit(255)
    
    try:
        amount = int(float(value))
        if amount < 0 or amount >= 4294967296:
            #sys.stderr.write("Amount must be [0.00, 4294967295.99]\n")
            os._exit(255);
        if float(value) == 0.00:
            #sys.stderr.write("Amount must be non-zero\n")
            os._exit(255);
    except ValueError:
        #sys.stderr.write("Amount value is non-float: " + value + '\n')
        os._exit(255);
    return float(value)
    
def validate_ip(value):
    #sys.stderr.write("Validating IP\n")
    if valid_ip.match(value) is None:
        #sys.stderr.write("Incorrectly formatted IP input: " + value + '\n')
        os._exit(255)
    try:
        for s in value.split('.'):
            as_int = int(s)
            if as_int < 0 or as_int > 255:
                #sys.stderr.write("Invalid IP address: " + value + '\n')
                os._exit(255)
    except:
        #sys.stderr.write("Could not parse IP address: " + value + '\n')
        os._exit(255)
    return value
    
        
def validate_port(value):
    if valid_port.match(value) is None:
        #sys.stderr.write("Incorrectly formatted port: " + value + '\n')
        os._exit(255)
        
    try:
        as_int = int(value)
        if as_int <= 1024 or as_int >= 65535:
            #sys.stderr.write("Invalid port number:" + value + "\n")
            os._exit(255)
        return as_int
    except:
        #sys.stderr.write("Could not parse port number:" + value + "\n")
        os._exit(255)
    

    
        
def one_true(*args):
    '''
    Function to check that the passed paratmers conatian only one ture and reset
    are false.
    
    Mainly used to check modes of operation 
    '''
    
    return sum(args) == 1


def check_cmd_arg_duplicate():
    arg_values = dict() 
    for arg in sys.argv[1:]:
        if arg.startswith('-'):
            if arg not in arg_values:
                arg_values[arg] = 1
            else:                
                if arg_values[arg] > 0:
                    #sys.stderr.write("Duplicated argument: " + arg + "\n")
                    os._exit(255)


def check_file(file_to_check):
    '''
    Check if the authentication file is esixt and readable
    '''
    
    if not os.path.isfile(file_to_check) \
      and os.access("./" + file_to_check, os.R_OK):
        #sys.stderr.write(("Either file is missing or is not readable: " +
        #                  file_to_check + "\n")
        #                )
        os._exit(255);
    return file_to_check


def load_authfile(authfile):
    '''
    Call the cypto_utils to load the file and get the keys 
    '''
    
    keys = crypto_utils.load_auth_file(authfile)
    return keys


def send_request_receive_response(request, auth):
    '''
    Sends the request after pre-processing with crypto_utils.sign_user_request
    and  crypto_utils.encrypt_then_mac
    
    Bank's return is passed to receive_response
    '''        
    
    request_to_send = crypto_utils.encrypt_then_mac(auth['secret_key'], json.dumps(request))
    
    #these can raise socket.timeout exception, has to be catched by caller
    s.sendall(request_to_send)
    data = s.recv(1024)
    
    response_received = crypto_utils.verify_then_decrypt(auth['secret_key'], data)

    resp_dict = json.loads(response_received)
    if 'error' in resp_dict:
        #sys.stderr.write("Error at bank '" +
        #  json.loads(response_received)['error'] + "\n")
        if 'protocol_error' in resp_dict:
            #sys.stderr.write("Exit with 63\n")
            os._exit(63)
        else:
            #sys.stderr.write("Exit with 255\n")
            os._exit(255)
            
    #TEST: replay attack towards atm
    if request['transaction_id'] != resp_dict['transaction_id']:
        #sys.stderr.write("Incorrect transaction_id in response\n")
        os._exit(63)
            
    return resp_dict


def create_user(name, amount, auth, cardfile):
    if os.path.isfile(cardfile) \
      and os.access("./" + cardfile, os.R_OK):
        #sys.stderr.write("File " + cardfile + " already exists\n")
        os._exit(255)
    
    #sys.stderr.write("Creat user file\n")
    try:
        user_pin = crypto_utils.create_user_card(cardfile)
    except:
        #sys.stderr.write("Could not create cardfile\n")
        os._exit(255)
    
    request = {
        'mode':'create',
        'account_name':name,
        'transaction_id':str(libnacl.randombytes_random()),
        'amount':amount,
        'user_pin':user_pin
    }
    
    #sys.stderr.write("Request is " + str(request))
    decrypted_response = send_request_receive_response(request,
                                                       auth
                                                      )
    return decrypted_response


def process_deposit(name, amount, auth, cardfile):
    if os.path.isfile(cardfile) \
      and os.access("./" + cardfile, os.R_OK):
        user_pin = crypto_utils.load_user_card(cardfile)
    else:
        #sys.stderr.write("Cardfile non-existent/non-readable\n")
        os._exit(255)
    
    request = {
        'mode':'deposit',
        'account_name':name,
        'transaction_id':str(libnacl.randombytes_random()),
        'amount':amount,
        'user_pin':user_pin
    }
    
    decrypted_response = send_request_receive_response(request,
                                                       auth
                                                      )
    return decrypted_response


def process_withdraw(name, amount, auth, cardfile):
    if os.path.isfile(cardfile) \
      and os.access("./" + cardfile, os.R_OK):
        user_pin = crypto_utils.load_user_card(cardfile)
    else:
        #sys.stderr.write("Cardfile non-existent/non-readable\n")
        os._exit(255)
    
    request = {
        'mode':'withdraw',
        'account_name':name,
        'transaction_id':str(libnacl.randombytes_random()),
        'amount':amount,
        'user_pin':user_pin
    }
      
    decrypted_response = send_request_receive_response(request,
                                                       auth
                                                      )
    return decrypted_response


def process_get(name, auth, cardfile):
    if os.path.isfile(cardfile) \
      and os.access("./" + cardfile, os.R_OK):
        user_pin = crypto_utils.load_user_card(cardfile)
    else:
        #sys.stderr.write("Cardfile non-existent/non-readable\n")
        os._exit(255)
    
    request = {
        'mode':'get',
        'account_name':name,
        'transaction_id':str(libnacl.randombytes_random()),
        'user_pin':user_pin
    }

    decrypted_response = send_request_receive_response(request,
                                                       auth
                                                      )
    return decrypted_response


def setup_network_com(host, port):
    global s
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((host,port))
    except:
        #sys.stderr.write("Could not connect to bank\n")
        os._exit(255)


def main():
    check_cmd_arg_duplicate()
    
    host, port, authfile, account, cardfile, mode, amount = handle_cmd_arg()
    
    #sys.stderr.write("Setup network\n")
    setup_network_com(host, port)
    
    #sys.stderr.write("Read auth file\n")
    auth = crypto_utils.load_auth_file(authfile)
    
    if mode == 1:
        try:
            #sys.stderr.write("Create new user\n")
            response = create_user(account, amount, auth, cardfile)
            #sys.stderr.write(json.dumps(response) + '\n')
            print '{"initial_balance": ' + str(response['initial_balance']) + \
              ', "account": "' + response['account'] + '"}'
            sys.stdout.flush()
        except socket.timeout:
            #sys.stderr.write("Timeout occured, deleting user card\n")
            os.remove(cardfile)
            sys.exit(63)
    else:
        try:
            if mode == 2:
                #sys.stderr.write("Deposit amount\n")
                response = process_deposit(account, amount, auth, cardfile)
                #sys.stderr.write(json.dumps(response) + '\n')
                print '{"account": "' + response['account'] + \
                  '", "deposit": ' + str(response['amount']) + '}'
                sys.stdout.flush()
            elif mode == 3:
                #sys.stderr.write("Withdraw amount\n")
                response = process_withdraw(account, amount, auth, cardfile)
                #sys.stderr.write(json.dumps(response) + '\n')
                print '{"account": "' + response['account'] + \
                  '", "withdraw": ' + str(response['amount']) + '}'
                sys.stdout.flush()
            elif mode == 4:
                #sys.stderr.write("Get balance starts\n")
                response = process_get(account, auth, cardfile)
                #sys.stderr.write(json.dumps(response) + '\n')
                print '{"account": "' + response['account'] + \
                  '", "balance": ' + str(response['balance']) + '}'
                sys.stdout.flush()
        except socket.timeout:
            #sys.stderr.write("Timeout occured\n")
            sys.exit(63)
    
    s.close()
    sys.exit(0)

if __name__ == "__main__":
    main()