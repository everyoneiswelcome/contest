import uuid
import libnacl
import time
import sys


start = time.time()
for i in range(1, 10000):
  sys.stderr.write(str(uuid.uuid4()))
end = time.time()
print end - start

start = time.time()
for i in range(1, 10000):
  sys.stderr.write(str(libnacl.randombytes_random()))
end = time.time()
print end - start

import bisect
L = [0, 100]

bisect.insort(L, 50)
bisect.insort(L, 20)
bisect.insort(L, 21)

print L
## [0, 20, 21, 50, 100]

i = bisect.bisect(L, 20)
print L[i-1], L[i]
## 20, 21

def index(a, x):
    'Locate the leftmost value exactly equal to x'
    i = bisect.bisect_left(a, x)
    if i != len(a) and a[i] == x:
        return True
    return False

print index(L, 22)

