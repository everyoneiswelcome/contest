import os

# file manipulation
import tempfile

# en-decodings
import base64
import binascii
import json
import uuid

# crypto
import hashlib
import hmac
import libnacl
import libnacl.dual
import libnacl.encode
import libnacl.public
import libnacl.secret
import libnacl.sign
import libnacl.utils

def self_test():
    # Creating/Sharing secrets
    temp_auth = tempfile.NamedTemporaryFile(delete=False)
    
    # does contain bank private key
    bank_keys = create_auth_file(temp_auth.name)
    # does NOT contain bank private key
    atm_keys = load_auth_file(temp_auth.name)
    
    print "Bank keys are (from memory)", bank_keys
    print "ATM keys are (from file)", atm_keys 
    
    temp_user = tempfile.NamedTemporaryFile(delete=False)
    
    create_user_card(temp_user.name) # also returns user_keys, but we test
                                     # reading back here
    alice = load_user_card(temp_user.name)
    
    print "User card read (from file, same as in memory)", alice
    
    # Create protocol run to share user keys with the bank
    request = {
        'mode':'create',
        'account_name':'alice',
        'account_id':alice['account_id'],
        'transaction_id':str(uuid.uuid4()),
        'amount':100.00,
        'verification_key':base64.b64encode(alice['verification_key']),
        'encryption_key':base64.b64encode(alice['encryption_key'])
    }
    
    request_to_send = encrypt_then_mac(atm_keys['secret_key'],
      atm_keys['mac_key'], json.dumps(request))
    
    print "Request to send is", request_to_send
    
    request_received = verify_then_decrypt(bank_keys['secret_key'],
      bank_keys['mac_key'], request_to_send)
      
    print "Request received is", request_received
    request_received_dict = json.loads(request_received)
    alice_vk = base64.b64decode(request_received_dict['verification_key'])
    alice_pk = base64.b64decode(request_received_dict['encryption_key'])
    alice_name = request_received_dict['account_name']
    alice_id = request_received_dict['account_id']
    
    response = {
        'mode':request_received_dict['mode'],
        'account_name':alice_name,
        'account_id':alice_id,
        'transaction_id':request_received_dict['transaction_id'],
        'amount':request_received_dict['amount']
    }
    
    encrypted_response = encrypt_for_user(bank_keys['bank_private_key'], 
      alice_pk, json.dumps(response))
    response_to_send = encrypt_then_mac(bank_keys['secret_key'],
      bank_keys['mac_key'], encrypted_response)
    
    print "Response to send is", response_to_send
    
    response_received = verify_then_decrypt(atm_keys['secret_key'],
      atm_keys['mac_key'], response_to_send)
    decrypted_response = decrypt_for_user(atm_keys['bank_public_key'],
      alice['decryption_key'], response_received)
    
    print "Decrypted response is", decrypted_response
    
    # Deposit protocol round to test sign/verify
    request = {
        'mode':'deposit',
        'account_name':'alice',
        'account_id':alice['account_id'],
        'transaction_id':str(uuid.uuid4()),
        'amount':100.00,
    }
    
    signed_request = sign_user_request(alice['signing_key'], request)
    request_to_send = encrypt_then_mac(atm_keys['secret_key'],
      atm_keys['mac_key'], json.dumps(signed_request))
    
    print "Request to send is", request_to_send
    
    request_received = verify_then_decrypt(bank_keys['secret_key'],
      bank_keys['mac_key'], request_to_send)
    request_received_dict = json.loads(request_received)
    verified_request = verify_user_request(alice_vk, request_received_dict)
    
    print "Verified user request is", verified_request
    
    response = {
        'mode':request_received_dict['mode'],
        'account_name':alice_name,
        'account_id':alice_id,
        'transaction_id':request_received_dict['transaction_id'],
        'amount':request_received_dict['amount']
    }
    
    encrypted_response = encrypt_for_user(bank_keys['bank_private_key'], 
      alice_pk, json.dumps(response))
    response_to_send = encrypt_then_mac(bank_keys['secret_key'],
      bank_keys['mac_key'], encrypted_response)
    
    print "Response to send is", response_to_send
    
    response_received = verify_then_decrypt(atm_keys['secret_key'],
      atm_keys['mac_key'], response_to_send)
    decrypted_response = decrypt_for_user(atm_keys['bank_public_key'],
      alice['decryption_key'], response_received)
    
    print "Decrypted response is", decrypted_response
    
    # Cleanup
    os.remove(temp_auth.name)
    os.remove(temp_user.name)


def create_auth_file(path):
    '''
    Creates the ATM-Bank cross-authentication file with a key for salsa20 and a
    key for HMAC-SHA256 and saves at path. Additionally for user-message
    encryption a bank key-pair is generated and saved.
    
    Used only by bank once at each startup.
    '''
    
    to_ret = {}
    to_ret['mac_key'] = libnacl.randombytes(32)
    to_ret['secret_key'] = libnacl.secret.SecretBox().sk
    
    bank_keys = libnacl.public.SecretKey()
    to_ret['bank_public_key'] = bank_keys.pk
    to_ret['bank_private_key'] = bank_keys.sk
    
    to_save = {}
    to_save['mac_key'] = base64.b64encode(to_ret['mac_key'])
    to_save['secret_key'] = base64.b64encode(to_ret['secret_key'])
    to_save.update(bank_keys.for_json())
    to_save.pop('priv')
    
    with open(path, 'wb') as auth_file:
        auth_file.write(json.dumps(to_save, sort_keys=True))
        
    return to_ret


def load_auth_file(path):
    '''
    Loads the ATM-Bank cross-authentication file with a key for salsa20 and a
    key for HMAC-SHA256 from file at path. Additionally for user-message
    encryption a bank key-pair is loaded.
    
    Used only by ATM once at each startup.
    '''
    
    with open(path, 'rb') as auth_file:
        to_load = json.loads(auth_file.read())
    #FIXME: verify file, that it ocntains one line, which is JSON, contains the
    #correct keys, etc.
    
    keys = {}
    keys['mac_key'] = base64.b64decode(to_load['mac_key'])
    keys['secret_key'] = base64.b64decode(to_load['secret_key'])
    keys['bank_public_key'] = libnacl.encode.hex_decode(
      to_load['pub']
    )
    return keys


def create_user_card(path):
    '''
    Creates the user's card file, which contains two key-pairs, one for
    encryption/decryption over Curve25519 and one for signing/verification over
    Ed25519, saves to file at path
    
    Used by ATM once when creating a new user.
    '''
    
    #TODO: if file already exists... that should be handled by the caller of
    #this function
    
    keys = libnacl.dual.DualSecret()
    user = keys.for_json()
    user['account_id'] = str(uuid.uuid4())
    
    print path
    with open(path, 'wb') as card_file:
        card_file.write(json.dumps(user, sort_keys=True))
    
    ret_user = {}
    ret_user['account_id'] = user['account_id']
    ret_user['encryption_key'] = keys.pk
    ret_user['decryption_key'] = keys.sk
    ret_user['signing_key'] = keys.seed
    ret_user['verification_key'] = keys.hex_vk()
        
    return ret_user


def load_user_card(path):
    '''
    Loads the user's card file, which contains two key-pairs, one for
    encryption/decryption over Curve25519 and one for signing/verification over
    Ed25519 from file at path
    
    Used by ATM once when doing something for an existing user.
    '''
    
    raw_user = {}
    with open(path, 'r') as card_file:
        raw_user = json.loads(card_file.read())
        
    user = {}
    user['account_id'] = raw_user['account_id']
    user['encryption_key'] = libnacl.encode.hex_decode(
      raw_user['pub']
    )
    user['decryption_key'] = libnacl.encode.hex_decode(
      raw_user['priv']
    )
    user['signing_key'] = libnacl.encode.hex_decode(
      raw_user['sign']
    )
    user['verification_key'] = libnacl.encode.hex_encode(
      libnacl.encode.hex_decode(raw_user['verify'])
    )
    
    #FIXME: verify file, that it ocntains one line, which is JSON, contains the
    #correct keys, etc.
    
    return user


def encrypt_then_mac(secret_key, mac_key, payload_as_string):
    '''
    Encrypts the payload with the secret_key with salsa20 and then macs with the
    ciphertext with mac_key as HMAC-SHA256, concatenates the two with base64
    encoded output with . as separator
    
    Used by the ATM when sending a request to the Bank.
    Used by the Bank when sending a response to the ATM.
    '''
    
    #TODO: optimization opportunity on bank-side: cache SecretBox object
    box = libnacl.secret.SecretBox(secret_key)
    ciphertext = base64.b64encode(box.encrypt(payload_as_string))
    
    mac = hmac.new(mac_key, msg=ciphertext, digestmod=hashlib.sha256).digest()
    
    return ciphertext + '.' + base64.b64encode(mac)


def verify_then_decrypt(secret_key, mac_key, ciphertext_as_base64_string):
    '''
    Decrypt the ciphertext with the secret_key with salsa20 after verifying the
    mac of the ciphertext with mac_key as HMAC-SHA256, ciphertext and mac should
    be concatenated the two as base64 encoded with . as separator
    
    Used by the ATM when receiving a response from the Bank.
    Used by the Bank when receiving a request from the ATM.
    '''
    
    split_index = ciphertext_as_base64_string.index('.')
    ciphertext = ciphertext_as_base64_string[:split_index]
    mac = ciphertext_as_base64_string[split_index+1:]
    
    my_mac = hmac.new(mac_key, msg=ciphertext, digestmod=hashlib.sha256) \
      .digest()
    
    if my_mac != base64.b64decode(mac):
        raise Exception('MAC mismatch')
    
    #TODO: optimization opportunity on bank-side: cache SecretBox object
    box = libnacl.secret.SecretBox(secret_key)
    plaintext = box.decrypt(base64.b64decode(ciphertext))

    return plaintext


def encrypt_for_user(bank_key, user_key, payload_as_string):
    '''
    Encrypt a message for a user with the given bank private and user public key
    
    Used by the Bank when sending a response to the ATM (on the raw response,
    i.e., before encrypt_then_mac)
    '''
    
    #TODO: optimization opportunity on bank-side: cache Box objects for users
    box = libnacl.public.Box(bank_key, user_key)
    ciphertext = box.encrypt(payload_as_string)
    
    return base64.b64encode(ciphertext)


def decrypt_for_user(bank_key, user_key, ciphertext_as_base64_string):
    '''
    Decrypt a message for a user with the given bank public and user private key
    
    Used by the ATM when receiving a response from the Bank (on the raw
    response, i.e., after verify_then_decrypt)
    '''
    
    box = libnacl.public.Box(user_key, bank_key)
    plaintext = box.decrypt(base64.b64decode(ciphertext_as_base64_string))
    
    return plaintext


def sign_user_request(user_key, payload_as_dict):
    '''
    Signs a request map (it will be transferred as JSON) with the given user
    signing key
    
    Used by the ATM when sending a request to the Bank (on the raw request,
    i.e., before encrypt_then_mac)
    '''
    
    #TODO: optimization opportunity: without dumping to JSON?
    signer = libnacl.sign.Signer(user_key)
    
    signature = signer.signature(json.dumps(payload_as_dict, sort_keys=True))
    
    #FIXME: if signaure is already a field, raise Exception
    payload_as_dict['signature'] = base64.b64encode(signature)
    
    return payload_as_dict


def verify_user_request(user_key, payload_as_dict):
    '''
    Verify a request map (transferred via JSON) with the given user verification
    key
    
    Used by the Bank when receiving a request from the ATM (on the raw request,
    i.e., after verify_then_decrypt)
    '''
    
    verifier = libnacl.sign.Verifier(user_key)
    #FIXME: missing signature
    signature = base64.b64decode(payload_as_dict.pop('signature'))
    
    #TODO: optimization opportunity: without dumping to JSON? see line 194
    verified = verifier.verify(signature + json.dumps(payload_as_dict,
                                                      sort_keys=True
                                                     )
                              )
    
    return verified
