import socket

host = "127.0.0.1"
port = 3000
auth = "bank.auth"

#create socket
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#bind socket to hostname and port
s.bind((host,port))

#listen on socket
s.listen(1)

while True:
    connection, client_address = s.accept()

    print ("connection from: ", client_address)
    data = connection.recv(16)
    if data:
        print(data)
    else:
        break

    connection.close()
