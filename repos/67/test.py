import sys
import os
import argparse
import gzip
import libnacl
import libnacl.public
import tempfile
import base64
import binascii
import hmac
import hashlib
import json
import array

'''
This file is meant to be a test for imports whether every import works as it should on the target BIBIVM
sys = OK
os = OK
argparse = OK
gzip = OK
libnacl = OK (installation needed with `sudo apt-get install -y python-pip` and then `sudo pip install libnacl`)
tempfile = OK
'''

if __name__=="__main__":
    # Testing argparse
    parser = argparse.ArgumentParser(description='Cybersecurity Capstone project ATM/Bank protocol')
    
    parser.add_argument('-g',
                       help='Get balance')
    parser.add_argument('-w',
                       help='Withdraw amount')
    parser.add_argument('-d',
                       help='Deposit amount')
    parser.add_argument('-n',
                       help='Create new account')
    
    args = parser.parse_args()        
    print args
    
    #Testing libnacl
    # Define a message to send
    msg = b'You\'ve got two empty halves of coconut and you\'re bangin\' \'em together.'

    # Generate the key pairs for Alice and bob, if secret keys already exist
    # they can be passed in, otherwise new keys will be automatically generated
    bob = libnacl.public.SecretKey()
    alice = libnacl.public.SecretKey()
    
    # Create the boxes, this is an object which represents the combination of the
    # sender's secret key and the receiver's public key
    bob_box = libnacl.public.Box(bob.sk, alice.pk)
    alice_box = libnacl.public.Box(alice.sk, bob.pk)
    
    # Bob's box encrypts messages for Alice
    bob_ctxt = bob_box.encrypt(msg)
    # Alice's box decrypts messages from Bob
    bclear = alice_box.decrypt(bob_ctxt)
    # Alice can send encrypted messages which only Bob can decrypt
    alice_ctxt = alice_box.encrypt(msg)
    aclear = alice_box.decrypt(alice_ctxt)
    print aclear    
    
    #Testing gzip"
    content = "Lots of content here"
    with gzip.open('file.txt.gz', 'wb') as f:
        f.write(content)
    with gzip.open('file.txt.gz', 'rb') as f:
        file_content = f.read()
        print file_content
    os.remove('file.txt.gz')
    
    #Testing tempfile
    tf = tempfile.NamedTemporaryFile(delete=False)
    with open(tf.name, 'wb') as f:
        f.write(content)
    with open(tf.name, 'rb') as f:
        print f.read()
    print tf.name    
    os.remove(tf.name)
    
    #Testing HMAC, hashlib, binascii, base64
    dig = hmac.new(b'key', msg="The quick brown fox jumps over the lazy dog", digestmod=hashlib.sha256).digest()
    # this should be 0xf7bc83f430538424b13298e6aa6fb143ef4d59a14946175997479dbc2d1a3cd8
    print binascii.hexlify(bytearray(dig))
    print base64.b64encode(dig).decode()
    
    #Testing JSON
    tester = {}
    tester['a'] = 'x'
    tester['b'] = 2.5
    print json.dumps(tester)
    print json.loads(json.dumps(tester))
    
    #Testing array
    print array.array('B', "ABCD")
    
    sys.exit(63)
