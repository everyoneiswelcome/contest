#!/usr/bin/python
from Crypto.Cipher import AES
from Crypto import Random
from thread import *
from binascii import hexlify, unhexlify
from hashlib import sha256
from os import path
from re import search
from StringIO import StringIO
import cPickle
import getopt
import hmac
import signal
import socket
import sys
import zlib


debug = 0 


compress = 1 
compressLevel = 1 
connBuffer = 65536
connHashSize = 256 
connTimeout = 10
keyNames = ["km","kh","kc","ks"]
hasHandshake = 1
nodelay = 0 
reuseAddress = 1
TCP_IP = '127.0.0.1'


authFile = "bank.auth"
clients = {}
clientsAcknonceControl = {}
clientsNonceControl = {}
keys={}
portNumber = 3000
server = 0



def main(argv):
	checkInputs(argv)
	checkFile()
	createAuthFile()
	startServer()



def checkFile():
	
	if path.isfile(authFile):
		
		sys.exit(255)



def checkInputs(argv):
	
	
	
	
	try:
		opts, args = getopt.getopt(argv, "p:s:")
	except getopt.GetoptError as error:
		
		sys.exit(255)
	

	
	
	oList = [o for o, a in opts]
		
	if oList.count("-p") > 1 or oList.count("-s") > 1:
		
		sys.exit(255)
	
	
	for o, a in opts:
		if len(a) > 4096:
			
			sys.exit(255)

		if o == "-p": 
			if not search("^(0|[1-9][0-9]*$)",a):
				
				sys.exit(255)
				
			if int(a) < 1024 or int(a) > 65535:
				
				sys.exit(255)
				
			
			global portNumber
			portNumber = int(a)
				
				
		elif o == "-s": 
			if not search("^[_\-\.0-9a-z]+$",a) or len(a) > 255 or a == "." or a == "..":
				
				sys.exit(255)
				
			
			global authFile
			authFile = a
		
		

def createAuthFile():
	
	for k in keyNames:
		keys[k]=Random.new().read(32)
	
	
	f = open(authFile,"w")

	for k in keys:
		f.write(k + ":" + keys[k].encode('base64'))

	f.close()
	print "created"
	sys.stdout.flush() 



def createClient(account, initial_balance):
	
	if account in clients:
		return False

	clients[account] = initial_balance
	return True



def sigterm_handler(_signo, _stack_frame):
	
	if server <> 0:
		closeServer()
	sys.exit(0)



def srDecrypt(data):
	

	
	if connHashSize == 256:
		dataHash = data[-32:]
		data = data[:-32]
		connHmac = hmac.new(keys["kh"],data,sha256).digest()
	else:
		dataHash = data[-16:]
		data = data[:-16]
		connHmac = hmac.new(keys["kh"],data).digest()

	
	if dataHash <> connHmac:
		
		
		
		
		return cPickle.dumps({"conn_error" : "hmac_error"})

	
	c = AESCipher(keys["km"])
	data = c.decrypt(data)

	
	e = PKCS7Encoder()
	data = e.decode(data)

	
	if compress:
		data = zlib.decompress(data)

	return data



def srEncrypt(data):
	
	if compress:
		data = zlib.compress(data, compressLevel)

	
	e = PKCS7Encoder()
	data = e.encode(data)

	
	c = AESCipher(keys["km"])
	data = c.encrypt(data)

	if connHashSize == 256:
		
		dataHash = hmac.new(keys["kh"],data,sha256).digest()
	else:
		
		dataHash = hmac.new(keys["kh"],data).digest()

	data = "".join([data,dataHash])

	
	

	return data




def startServer():
	
	global server

	server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	if nodelay:
		server.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY,1)
	if reuseAddress:
		server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	try:
		server.bind(("127.0.0.1", portNumber))
	except socket.error as msg:
		print "Server cannot bind to port: " + str(portNumber) + "."
		sys.stdout.flush()
		sys.exit(255)

	server.listen(10)

	while 1:
		conn, addr = server.accept()
		start_new_thread(clientthread,(conn,))

	server.close()

	
	
def closeServer():
	
	global server
	
	server.close()
	














def clientthread(conn):
	global clientsAcknonceControl
	global clientsNonceControl
	conn.settimeout(connTimeout)

	try:
		
		if hasHandshake:
			recv = conn.recv(connBuffer)
			recv = recv[:connBuffer]
			data = srDecrypt(recv)
			try:
				rmessage = cPickle.loads(data)
			except:
				raise NameError("Connection Error.")

			

			
			if "conn_error" in rmessage:
				raise NameError("Connection Error.")

			clientsNonceControl[rmessage["a"]] = Random.new().read(3).encode('base64')[:-1]
			clientsAcknonceControl[rmessage["a"]] = ""

			message = rmessage.copy()
			message["n"] = clientsNonceControl[rmessage["a"]]
			message["k"] = rmessage["n"]
			data = cPickle.dumps(message)
			data = srEncrypt(data)
			conn.sendall(data)
			
			

		
		recv = conn.recv(connBuffer)
		recv = recv[:connBuffer]
		data = srDecrypt(recv)
		try:
			rmessage = cPickle.loads(data)
		except:
			raise NameError("Connection Error.")


		
		if "conn_error" in rmessage:
			raise NameError("Connection Error.")

		
		if rmessage["c"] == "c":
			isOk = 1
			nonceError = False
			clientSuccess = False

			
			if hasHandshake:
				if rmessage["k"] <> clientsNonceControl[rmessage["a"]]:
					
					nonceError = True
				else:
					clientsAcknonceControl[rmessage["a"]] = rmessage["k"]
			
			
			if rmessage["a"] not in clients:
				clientSuccess = True
			
			
			if nonceError == True or clientSuccess == False:
				isOk = 0

			
			

			
			clientsNonceControl[rmessage["a"]] = Random.new().read(3).encode('base64')[:-1]

			message = rmessage.copy()
			message["n"] = clientsNonceControl[rmessage["a"]]
			message["k"] = rmessage["n"]
			message["s"] = isOk

			data = cPickle.dumps(message)
			data = srEncrypt(data)
			conn.sendall(data)
			
			

		elif rmessage["c"] == "b":
			balance = "0.00"
			clientSuccess = False
			nonceError = False
			isOk = 1

			
			if hasHandshake:
				if rmessage["k"] <> clientsNonceControl[rmessage["a"]]:
					nonceError = True
				else:
					clientsAcknonceControl[rmessage["a"]] = rmessage["k"]
				
			if str(rmessage["a"]) in clients:
				clientSuccess = True

			
			if nonceError == True or clientSuccess == False:
				isOk = 0

			
			if str(rmessage["a"]) in clients:
				balance = "{0:.2f}".format(clients[rmessage["a"]])
			
			

			
			clientsNonceControl[rmessage["a"]] = Random.new().read(3).encode('base64')[:-1]

			message = rmessage.copy()
			message["n"] = clientsNonceControl[rmessage["a"]]
			message["k"] = rmessage["n"]
			message["s"] = isOk
			message["b"] = balance
			
			data = cPickle.dumps(message)
			data = srEncrypt(data)
			conn.sendall(data)
				

		elif rmessage["c"] == "d":
			isOk = 1
			clientSuccess = False
			nonceError = False

			
			if hasHandshake:
				if rmessage["k"] <> clientsNonceControl[rmessage["a"]]:
					nonceError = True
				else:
					clientsAcknonceControl[rmessage["a"]] = rmessage["k"]

			if rmessage["a"] in clients and not nonceError:
				clientSuccess = True

			
			if sys.float_info.max - round(clients[rmessage["a"]] + rmessage["d"],2) < 0:
				clientSuccess = False

			
			if nonceError == True or clientSuccess == False:
				isOk = 0

			

			
			clientsNonceControl[rmessage["a"]] = Random.new().read(3).encode('base64')[:-1]

			message = rmessage.copy()
			message["n"] = clientsNonceControl[rmessage["a"]]
			message["k"] = rmessage["n"]
			message["s"] = isOk

			data = cPickle.dumps(message)
			data = srEncrypt(data)
			conn.sendall(data)
				

		elif rmessage["c"] == "w":
			isOk = 1
			clientSuccess = False
			nonceError = False
			
			if hasHandshake:
				if rmessage["k"] <> clientsNonceControl[rmessage["a"]]:
					nonceError = True
				else:
					clientsAcknonceControl[rmessage["a"]] = rmessage["k"]
			
			if rmessage["a"] in clients and not nonceError:
				clientSuccess = True

			
			if round(clients[rmessage["a"]] - rmessage["w"],2) < 0.00:
				clientSuccess = False

			
			if nonceError == True or clientSuccess == False:
				isOk = 0

			

			
			clientsNonceControl[rmessage["a"]] = Random.new().read(3).encode('base64')[:-1]

			message = rmessage.copy()
			message["n"] = clientsNonceControl[rmessage["a"]]
			message["k"] = rmessage["n"]
			message["s"] = isOk

			data = cPickle.dumps(message)
			data = srEncrypt(data)
			conn.sendall(data)
				

		
		recv = conn.recv(connBuffer)
		recv = recv[:connBuffer]
		data = srDecrypt(recv)
		
		try:
			rmessage = cPickle.loads(data)
		except:
			raise NameError("Connection Error.")

		

		
		if "conn_error" in rmessage:
			raise NameError("Connection Error.")

		
		
		if rmessage["k"] <> clientsNonceControl[rmessage["a"]]:
			t = 1 
			
		else:
			clientsAcknonceControl[rmessage["a"]] = rmessage["k"]

		
		
		if rmessage["c"] == "c" and rmessage["s"] == 1:
			if createClient(rmessage["a"], rmessage["i"]):
				print "".join(["{\"account\":\"",rmessage["a"],"\",\"initial_balance\":","{0:.2f}".format(rmessage["i"]),"}"])
				sys.stdout.flush() 
		
		elif rmessage["c"] == "b" and rmessage["s"] == 1:
			print "".join(["{\"account\":\"",rmessage["a"],"\",\"balance\":","{0:.2f}".format(clients[rmessage["a"]]),"}"])
			sys.stdout.flush() 

		elif rmessage["c"] == "d" and rmessage["s"] == 1:
			clients[rmessage["a"]] = round(clients[rmessage["a"]] + rmessage["d"],2)
			print "".join(["{\"account\":\"",rmessage["a"],"\",\"deposit\":","{0:.2f}".format(rmessage["d"]),"}"])
			sys.stdout.flush() 

		elif rmessage["c"] == "w" and rmessage["s"] == 1:
			clients[rmessage["a"]] = round(clients[rmessage["a"]] - rmessage["w"],2)
			print "".join(["{\"account\":\"",rmessage["a"],"\",\"withdraw\":","{0:.2f}".format(rmessage["w"]),"}"])
			sys.stdout.flush() 

		
		del clientsAcknonceControl[rmessage["a"]]
		del clientsNonceControl[rmessage["a"]]

	except socket.timeout:
		print "protocol_error"
		sys.stdout.flush()
	except NameError:
		print "protocol_error"
		sys.stdout.flush()

	conn.close()

		

class AESCipher:
	def __init__( self, key):
		self.key = key

	def encrypt( self, raw):
		e = PKCS7Encoder()
		raw = e.encode(raw)
		iv = Random.new().read(AES.block_size)
		
		cipher = AES.new( self.key, AES.MODE_CBC, iv)
		encryptedData = cipher.encrypt(raw)
		
		return "".join([iv,encryptedData])

	def decrypt( self, enc):
		e = PKCS7Encoder()
		iv = enc[:16]
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return e.decode(cipher.decrypt(enc[16:]))



class PKCS7Encoder(object):
	def __init__(self, k=16):
		self.k = k

	def decode(self, text):
		nl = len(text)
		val = int(hexlify(text[-1]), 16)
		if val > self.k:
			raise ValueError('Input is not padded or padding is corrupt')

		l = nl - val
		return text[:l]

	def encode(self, text):
		l = len(text)
		output = StringIO()
		val = self.k - (l % self.k)
		for _ in xrange(val):
			output.write('%02x' % val)
		return "".join([text,unhexlify(output.getvalue())])





if __name__== "__main__":
	signal.signal(signal.SIGTERM, sigterm_handler)
	main(sys.argv[1:])
