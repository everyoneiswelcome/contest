#!/usr/bin/python
from Crypto.Cipher import AES
from Crypto import Random
from binascii import hexlify, unhexlify
from hashlib import sha256
from os import path
from re import search, match
from StringIO import StringIO
import cPickle
import getopt
import hmac
import signal
import socket
import sys
import zlib


debug = 0 


arguments = ["-s","-i","-p","-c","-a","-n","-d","-w","-g"]
cardHashSize = 256 
compress = 1
compressLevel = 1
connBuffer = 65536
connHashSize = 256 
connTimeout = 10
fileCompress = 1
fileCompressLevel = 1 
hasHandshake = 1
nodelay = 0 
sufixCard = ".card"


account= ""
authFile = "bank.auth"
card = ""
cmd = ""
cmdValue = 0.00
keys={}
portNumber = 3000
serverIP = "127.0.0.1"




def main(argv):
	checkInputs(argv)
	getKeys()
	runCmd()



def checkFile():
	
	if path.isfile(authFile):
		
		sys.exit(255)



def checkInputs(argv):
	
	global account
	global authFile
	global card
	global cmd
	global cmdValue
	global deposit
	global portNumber
	global serverIP

	
	
	
	try:
		opts, args = getopt.getopt(argv, "s:i:p:c:a:n:d:w:g")
	except getopt.GetoptError as error:
		
		sys.exit(255)

	
	
	
	
	oList = [o for o, a in opts]
	
	
	
	
	for i in xrange(len(argv) - 1):
		if argv[i] == "-g" and i < (len(argv) - 1):
			if argv[i+1] not in ["-s","-i","-p","-c","-a"]:
				
				sys.exit(255)

	
	if oList.count("-s") > 1 or oList.count("-i") > 1 or oList.count("-p") > 1 or oList.count("-c") > 1 or oList.count("-a") > 1 or oList.count("-n") > 1 or oList.count("-d") > 1 or oList.count("-w") > 1 or oList.count("-g") > 1:
		
		sys.exit(255)
		
	
	if oList.count("-a") <> 1:
		
		sys.exit(255)
		
	
	cmdTotal = oList.count("-n") + oList.count("-d") + oList.count("-w") + oList.count("-g")
	if cmdTotal <> 1:
		
		sys.exit(255)


	
	for o, a in opts:
		if len(a) > 4096:
			
			sys.exit(255)

		if o == "-s": 
			if not search("^[_\-\.0-9a-z]+$",a) or len(a) > 255 or a == "." or a == "..":
				
				sys.exit(255)
				
			
			authFile = a

		elif  o == "-i": 
			if not match("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$",a): 
				
				sys.exit(255)
			if not isValidIP(a):
				
				sys.exit(255)
				
			
			serverIP = a
		
		elif o == "-p": 
			if not search("^(0|[1-9][0-9]*)$",a):
				
				sys.exit(255)
				
			if int(a) < 1024 or int(a) > 65535:
				
				sys.exit(255)
				
			
			portNumber = int(a)

		elif o == "-a": 
			if not search("^[_\-\.0-9a-z]+$",a) or len(a) > 250:
				
				sys.exit(255)
				
			
			account = a
			if len(card) == 0:
				card = "".join([account,sufixCard])


		elif o == "-c": 
			if not search("^[_\-\.0-9a-z]+$",a) or len(a) > 255 or a == "." or a == "..":
				
				sys.exit(255)
				
			
			card = a

		elif o == "-n" or o == "-d" or o == "-w": 
			if not search("^(0|[1-9][0-9]*)(\.[0-9]{2})$",a):
				
				sys.exit(255)

			
			n = round(float(a),2)
			if n < 0 or n > 4294967295.99:
				
				sys.exit(255)

			
			cmdValue = n

		
		if o in ["-n","-d","-w","-g"]:
			cmd = o[1:]
		

def createCard():
	
	e = PKCS7Encoder()
	e.k = 256
	data = e.encode(account)

	
	if fileCompress:
		data = zlib.compress(data, fileCompressLevel)

	
	c = AESCipher(keys["kc"])
	data = c.encrypt(data)

	if cardHashSize == 256:
		
		dataHash = hmac.new(keys["ks"],data,sha256).digest()
	else:
		
		dataHash = hmac.new(keys["ks"],data).digest()

	

	data = "".join([data,dataHash])

	
	f = open(card, "w")
	f.write(data.encode("hex"))
	f.close()



def createClient():
	
	
	

	
	if cmdValue < 10:
		
		sys.exit(255)

	
	if path.isfile(card):
		
		sys.exit(255)

	
	message= {
		"a" : account,
		"i": cmdValue,
		"c": "c"
	}
	rmessage = srData(message)

	
	if rmessage["s"] == 1:
		createCard()
	else:
		
		sys.exit(255)

	
	print "".join(["{\"account\":\"",str(account),"\",\"initial_balance\":","{0:.2f}".format(cmdValue),"}"])
	sys.stdout.flush()



def execDeposit():
	
	
	

	
	if account <> getCardHolder():
		
		sys.exit(255)

	
	if cmdValue <= 0:
		
		sys.exit(255)

	
	message= {
		"a" : account,
		"d" : cmdValue,
		"c" : "d"
	}
	rmessage = srData(message)

	
	if rmessage["s"] == 0:
		
		sys.exit(255)

	
	print "".join(["{\"account\":\"",str(rmessage["a"]),"\",\"deposit\":","{0:.2f}".format(cmdValue),"}"])
	sys.stdout.flush()



def execWithdraw():
	
	
	

	
	if account <> getCardHolder():
		
		sys.exit(255)

	
	if cmdValue <= 0:
		
		sys.exit(255)

	
	message= {
		"a" : account,
		"w" : cmdValue,
		"c" : "w"
	}
	rmessage = srData(message)

	
	if rmessage["s"] == 0:
		
		sys.exit(255)

	
	print "".join(["{\"account\":\"",str(account),"\",\"withdraw\":","{0:.2f}".format(cmdValue),"}"])
	sys.stdout.flush()



def getBalance():
	
	

	
	if account <> getCardHolder():
		
		sys.exit(255)

	
	message= {
		"a" : account,
		"c": "b"
	}
	rmessage = srData(message)

	
	if rmessage["s"] == 0:
		
		sys.exit(255)

	
	print "".join(["{\"account\":\"",str(rmessage["a"]),"\",\"balance\":",str(rmessage["b"]),"}"])
	sys.stdout.flush()



def getCardHolder():
	
	if not path.isfile(card):
		
		sys.exit(255)

	try:
		with open(card, 'r') as f:
			allcard = f.readlines()[0]
	except:
		
		sys.exit(255)

	allcard = allcard.decode("hex")
	

	
	if cardHashSize == 256:
		dataHash = allcard[-32:]
		data = allcard[:-32]
		cardHmac = hmac.new(keys["ks"],data,sha256).digest()
	else:
		dataHash = allcard[-16:]
		data = allcard[:-16]
		cardHmac = hmac.new(keys["ks"],data).digest()

	

	if dataHash <> cardHmac:
		
		sys.exit(255)

	
	
	c = AESCipher(keys["kc"])
	data = c.decrypt(data)

	
	if fileCompress:
		data = zlib.decompress(data)

	
	e = PKCS7Encoder()
	e.k = 256
	data = e.decode(data)

	

	return data



def getKeys():
	try:
		with open(authFile,'r') as f:
			for l in f:
				keys[l.rstrip().split(":")[0]] = l.rstrip().split(":")[1].decode('base64')

			
	except:
		
		sys.exit(255)
		
	

def isValidIP(address):
	try:
		socket.inet_aton(address)
	except socket.error:
		return False
	
	return True




def runCmd():
	if cmd == "n":
		createClient()
	elif cmd == "d":
		execDeposit()
	elif cmd == "w":
		execWithdraw()
	elif cmd == "g":
		getBalance()
	else:
		
		sys.exit(255)















def srData(message):
	
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	if nodelay:
		s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY,1)
	s.settimeout(connTimeout)

	try:
		s.connect((serverIP,portNumber))

		
		if hasHandshake:
			
			message["n"] = nonce = Random.new().read(3).encode('base64')[:-1]
			data = cPickle.dumps(message)
			data = srEncrypt(data)

			
			s.sendall(data)
			

			
			recv = s.recv(connBuffer)
			recv = recv[:connBuffer]
			rmessage = srDecrypt(recv)

			try:
				rmessage = cPickle.loads(rmessage)
			except:
				raise NameError("Connection Error.")

			
			
			if rmessage["k"] <> nonce:
				
				sys.exit(255) 
			

		
		message["n"] = Random.new().read(3).encode('base64')[:-1]
		if hasHandshake:
			message["k"] = rmessage["n"]
		data = cPickle.dumps(message)
		data = srEncrypt(data)

		
		s.sendall(data)
	

		
		recv = s.recv(connBuffer)
		recv = recv[:connBuffer]
		rmessage = srDecrypt(recv)
		try:
			rmessage = cPickle.loads(rmessage)
		except:
			raise NameError("Connection Error.")

		


		if rmessage["k"] <> message["n"]:
			
			sys.exit(255) 

		
		del message["n"]
		message["k"] = rmessage["n"]
		message["s"] = rmessage["s"]
		data = cPickle.dumps(message)
		data = srEncrypt(data)
		
		
		s.sendall(data)

		

	except:
		
		sys.exit(63)
		s.close()

	finally:
		s.close()

	return rmessage



def srDecrypt(data):
	

	
	if connHashSize == 256:
		dataHash = data[-32:]
		data = data[:-32]
		connHmac = hmac.new(keys["kh"],data,sha256).digest()
	else:
		dataHash = data[-16:]
		data = data[:-16]
		connHmac = hmac.new(keys["kh"],data).digest()

	
	if dataHash <> connHmac:
		
		
		
		
		exit(255) 

	
	c = AESCipher(keys["km"])
	data = c.decrypt(data)

	
	e = PKCS7Encoder()
	data = e.decode(data)

	
	if compress:
		data = zlib.decompress(data)

	return data



def srEncrypt(data):
	
	if compress:
		data = zlib.compress(data, compressLevel)

	
	e = PKCS7Encoder()
	data = e.encode(data)

	
	c = AESCipher(keys["km"])
	data = c.encrypt(data)

	if connHashSize == 256:
		
		dataHash = hmac.new(keys["kh"],data,sha256).digest()
	else:
		
		dataHash = hmac.new(keys["kh"],data).digest()

	data = "".join([data, dataHash])

	
	

	return data





class AESCipher:
	def __init__( self, key):
		self.key = key

	def encrypt( self, raw):
		e = PKCS7Encoder()
		raw = e.encode(raw)
		iv = Random.new().read(AES.block_size)
		
		cipher = AES.new( self.key, AES.MODE_CBC, iv)
		encryptedData = cipher.encrypt(raw)
		
		return "".join([iv, encryptedData])

	def decrypt( self, enc):
		e = PKCS7Encoder()
		iv = enc[:16]
		cipher = AES.new(self.key, AES.MODE_CBC, iv)
		return e.decode(cipher.decrypt(enc[16:]))




class PKCS7Encoder(object):
	def __init__(self, k=16):
		self.k = k

	def decode(self, text):
		nl = len(text)
		val = int(hexlify(text[-1]), 16)
		if val > self.k:
			raise ValueError('Input is not padded or padding is corrupt')

		l = nl - val
		return text[:l]

	def encode(self, text):
		l = len(text)
		output = StringIO()
		val = self.k - (l % self.k)
		for _ in xrange(val):
			output.write('%02x' % val)
		return "".join([text,unhexlify(output.getvalue())])


if __name__=="__main__":
	main(sys.argv[1:])
