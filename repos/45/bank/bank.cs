﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections.Generic;
using CommandLine;
using CommandLine.Text;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using utils;

namespace bank
{
	sealed partial class MyServer
	{
		private static TcpListener listener;
		private static Thread thread;
		private static string authFile = "bank.auth";
		private static Dictionary<String, Account> cuentasDelBanco = new Dictionary<string, Account>();

		static int Main(string[] args)
		{

			var options = new ServerOptions();
			var parser = new CommandLine.Parser();

			if (parser.ParseArgumentsStrict(args, options, () => Utils.ExitWithError(255)))
			{
				if (options.port < 1024 || options.port > 65535) {
					Utils.ExitWithError (255);
				}

				if (!Utils.isValidFile(options.authFile)) {						
					Utils.ExitWithError (255);
				}


				if (File.Exists (options.authFile)) {
					Utils.ExitWithError (255);
				} else {
					byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
					byte[] key = Guid.NewGuid().ToByteArray();
					IEnumerable<byte> rv = time.Concat (key);
					File.WriteAllBytes (options.authFile,rv.ToArray());
					authFile = options.authFile;
					Console.WriteLine ("created");
				}

				listener = new TcpListener(new IPAddress(new byte[] {127,0,0,1}),options.port);
				thread = new Thread(new ThreadStart(Listen));
				thread.Start();
			}
			return 0;
		}


		private static void Listen()
		{
			listener.Start();

			while(true)
			{
				TcpClient client = listener.AcceptTcpClient();
				Thread listenThread = new Thread(new ParameterizedThreadStart(ListenThread));
				listenThread.Start(client);
			}
		}

		private static void ListenThread(Object client)
		{

			NetworkStream netstream = ((TcpClient)client).GetStream();
			Byte[] response = new Byte[(int)((TcpClient)client).ReceiveBufferSize];
			netstream.Read (response, 0, (int)((TcpClient)client).ReceiveBufferSize);
			var trimResponse = response.TakeWhile((v, index) => response.Skip(index).Any(w => w != 0x00)).ToArray();
			AtmAction atmActionFromClient = (AtmAction)Utils.ByteArrayToObject (Utils.Decrypt(trimResponse,authFile));

			ManageTransaction (ref atmActionFromClient);

			if (atmActionFromClient.successfulTransaction) {
				Console.WriteLine(atmActionFromClient.ToJson());
			}				

			byte[] resMessage = Utils.Encrypt(Utils.ObjectToByteArray(atmActionFromClient),authFile);
			netstream.Write(resMessage, 0, resMessage.Length);
			netstream.Flush ();
			netstream.Close();

		}

		private static void ManageTransaction(ref AtmAction transaction){
						
			if (transaction.operationMode == (int)Utils.ModesOperations.NEW) {
				if (!cuentasDelBanco.ContainsKey (transaction.account)) {
					Account nuevaCuenta = new Account ();
					nuevaCuenta.AccUser = transaction.account;
					nuevaCuenta.AccCard = transaction.accountCard;
					nuevaCuenta.Balance = transaction.initial_balance;
					cuentasDelBanco.Add (transaction.account, nuevaCuenta);
					transaction.successfulTransaction = true;
				} else {
					transaction.successfulTransaction = false;
				}
			}

			if (transaction.operationMode == (int)Utils.ModesOperations.DEPOSIT) {
				Account cuenta = new Account ();
				if (cuentasDelBanco.TryGetValue(transaction.account, out cuenta)) {
					if (transaction.accountCard.Equals (cuenta.AccCard)) {						
						cuenta.Balance = cuenta.Balance + transaction.deposit;
						cuentasDelBanco[transaction.account] = cuenta;
						transaction.successfulTransaction = true;
					} else {
						transaction.successfulTransaction = false;
					}
				} else {
					transaction.successfulTransaction = false;
				}
			}

			if (transaction.operationMode == (int)Utils.ModesOperations.WITHDRAW) {
				Account cuenta = new Account ();
				if (cuentasDelBanco.TryGetValue(transaction.account, out cuenta)) {
					if (transaction.accountCard.Equals (cuenta.AccCard)) {
						if ((cuenta.Balance - transaction.withdraw) >= 0) {
							cuenta.Balance = cuenta.Balance - transaction.withdraw;
							cuentasDelBanco [transaction.account] = cuenta;
							transaction.successfulTransaction = true;
						} else {
							transaction.successfulTransaction = false;
						}
					} else {
						transaction.successfulTransaction = false;
					}
				} else {
					transaction.successfulTransaction = false;
				}
			}

			if (transaction.operationMode == (int)Utils.ModesOperations.BALANCE) {				
				Account cuenta = new Account ();
				if (cuentasDelBanco.TryGetValue(transaction.account, out cuenta)) {
					if (transaction.accountCard.Equals (cuenta.AccCard)) {						
						transaction.serverBalance = cuenta.Balance;
						transaction.successfulTransaction = true;
					} else {
						transaction.successfulTransaction = false;
					}
				} else {
					transaction.successfulTransaction = false;
				}
			}
		}
	}
}
