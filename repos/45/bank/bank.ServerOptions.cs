﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using CommandLine;
using CommandLine.Text;

namespace bank
{
	partial class MyServer
	{
		public sealed class ServerOptions
		{

			[Option('s', null, Required = false, DefaultValue = "bank.auth", HelpText = "Input file with data to process.")]
			public string authFile {get; set;}

			[Option('p', null ,MetaValue = "INT", DefaultValue = 3000, HelpText = "Port where the banks server will run.")]
			public int port { get; set; }

			[ParserState]
			public IParserState LastParserState { get; set; }

			[HelpOption]
			public string GetUsage()
			{
				return HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
			}
		}

	}
}
