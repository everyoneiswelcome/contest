﻿using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace utils
{
	public static class Utils
	{
		public enum ModesOperations 
		{
			MODE_ERROR = 0,
			NEW = 1,
			DEPOSIT = 2,
			WITHDRAW = 3,
			BALANCE = 4
		}

		static readonly string VIKey = "HR$2pIjHR$2pIj12";
		public static byte[] ObjectToByteArray(Object obj)
		{
			if(obj == null)
				return null;
			BinaryFormatter bf = new BinaryFormatter();
			MemoryStream ms = new MemoryStream();
			bf.Serialize(ms, obj);
			return ms.ToArray();
		}

		public static Object ByteArrayToObject(byte[] arrBytes)
		{
			MemoryStream memStream = new MemoryStream();
			BinaryFormatter binForm = new BinaryFormatter();
			memStream.Write(arrBytes, 0, arrBytes.Length);
			memStream.Seek(0, SeekOrigin.Begin);
			Object obj = (Object) binForm.Deserialize(memStream);
			return obj;
		}

		public static bool isValidNumber(string number){
			Match numericMatch = Regex.Match (number, @"^(0|[1-9][0-9]*)$");
			return numericMatch.Success;
		}

		public static bool isValidUsername(string username){
			Match usernameMatch = Regex.Match (username, @"^[0-9a-z\-._]+$");
			if (!(usernameMatch.Success && (username.Length >= 1 && username.Length <= 255))) {
				return false;
			}
			return true;
		}

		public static bool isValidFile(string fileName){
			Match fileMatch = Regex.Match (fileName, @"^[0-9a-z\-._]+$");
			if (fileMatch.Success && (fileName.Length > 1 && fileName.Length <= 255) && !fileName.Equals(".") && !fileName.Equals("..")) {
				return true;
			}
			return false;
		}

		public static bool isValidCurrencyAmount(string amount){
			try{
				string[] separators = {"."};
				string[] amountParts = amount.Split(separators, StringSplitOptions.RemoveEmptyEntries);
				if (amountParts.Length > 2)
					return false;

				Match decimalMatch = Regex.Match (amountParts[1], @"^[0-9]{2}$");

				if(Utils.isValidNumber(amountParts[0]) && decimalMatch.Success && (Double.Parse(amountParts[0]) >= 0 && Double.Parse(amountParts[0]) <= 4294967295 ))
					return true;
				else
					return false;

			}
			catch{
				return false;
			}
		}

		public static void ExitWithError(int error){
			//Console.WriteLine ("255");
			Environment.Exit (error);
		}

		public static byte[] Encrypt(byte[] plainText,string authFile)
		{
			//byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
			byte[] plainTextBytes = plainText;


			byte[] keyBytes = File.ReadAllBytes(authFile);
			var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
			var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

			byte[] cipherTextBytes;

			using (var memoryStream = new MemoryStream())
			{
				using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
				{
					cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
					cryptoStream.FlushFinalBlock();
					cipherTextBytes = memoryStream.ToArray();
					cryptoStream.Close();
				}
				memoryStream.Close();
			}
			return cipherTextBytes;
			//return Convert.ToBase64String(cipherTextBytes);
		}

		public static byte[] Decrypt(byte[] encryptedText,string authFile)
		{
			//byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
			byte[] cipherTextBytes =encryptedText;

			byte[] keyBytes = File.ReadAllBytes(authFile);
			var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

			var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
			var memoryStream = new MemoryStream(cipherTextBytes);
			var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Write);
			cryptoStream.Write (encryptedText, 0, encryptedText.Length);
			cryptoStream.Close();

			byte[] plainTextBytes =  memoryStream.ToArray ();
			//int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
			memoryStream.Close();
			//return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
			return plainTextBytes;
		}
	}
}

