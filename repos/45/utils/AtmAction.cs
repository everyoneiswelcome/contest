﻿using System;

namespace utils
{
	[Serializable]
	public class AtmAction
	{
		public String account { get; set; }
		public String accountCard { get; set; }
		public int operationMode { get; set; }
		public double initial_balance { get; set; } 
		public double deposit { get; set; }
		public double withdraw { get; set; }
		public double serverBalance { get; set; }
		public bool balance { get; set; }
		public bool successfulTransaction { get; set; }

		public string ToJson(){
			String jsonAction = "{\"account\":\"<username>\",\"<action>\"<amount>}";

			jsonAction = jsonAction.Replace("<username>", this.account);
			if (this.operationMode == (int)Utils.ModesOperations.NEW) {
				jsonAction = jsonAction.Replace ("<action>", "initial_balance");
				jsonAction = jsonAction.Replace ("<amount>", ":" + this.initial_balance.ToString ());
			}

			if (this.operationMode == (int)Utils.ModesOperations.DEPOSIT) {
				jsonAction = jsonAction.Replace ("<action>", "deposit");
				jsonAction = jsonAction.Replace ("<amount>", ":" + this.deposit.ToString ());
			}

			if (this.operationMode == (int)Utils.ModesOperations.WITHDRAW) {
				jsonAction = jsonAction.Replace ("<action>", "withdraw");
				jsonAction = jsonAction.Replace ("<amount>", ":" +this.withdraw.ToString ());
			}

			if (this.operationMode == (int)Utils.ModesOperations.BALANCE) {
				jsonAction = jsonAction.Replace ("<action>", "balance");
				jsonAction = jsonAction.Replace ("<amount>", ": " + this.serverBalance.ToString());

			}

			return jsonAction;
		}
	}
}

