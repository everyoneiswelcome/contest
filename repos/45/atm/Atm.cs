﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using utils;

namespace atm
{
	sealed partial class MyClient
	{		
		private static int operationMode = (int)Utils.ModesOperations.MODE_ERROR;
		private static double initialBalance = 0.00;
		private static double deposit = 0.00;
		private static double withdraw = 0.00;
		private static string authFile= "bank.auth";

		private static string[] parametros = { "-s", "-i", "-p", "-c", "-a", "-n", "-d", "-w", "-g" };
		private static int[] cantidad = new int[parametros.Length];

		static int Main(string[] args)
		{
			AtmAction atmAction = new AtmAction ();

			for (int j = 0; j < args.Length; j++) {

				if (args [j] == "-g") {
					if ((j + 1) < args.Length) {
						if (!args [j + 1].Contains ("-"))
							Utils.ExitWithError (255);
					}
				}

				for (int i = 0; i < parametros.Length; i++) {
					if (args[j].Contains (parametros [i])) {
						cantidad [i]++;
						if (cantidad [i] > 1)
							Utils.ExitWithError (255);


					}
				}
			}

			var options = new ClientOptions();
			var parser = new CommandLine.Parser();

			if (parser.ParseArgumentsStrict (args, options, () => Utils.ExitWithError(255))) {
				if (!Utils.isValidUsername(options.username)) {
					Utils.ExitWithError (255);	
				}

				if (options.cardFile == null) {
					options.cardFile = options.username + ".card";
				}

				if (!Utils.isValidFile (options.cardFile)) {						
					Utils.ExitWithError (255);
				}

				if (options.port < 1024 || options.port > 65535) {
					Utils.ExitWithError (255);
				}

				if (!Utils.isValidFile (options.authFile)) {						
					Utils.ExitWithError (255);
				} else {
					authFile = options.authFile;
				}

				if (!File.Exists (authFile)) {
					Utils.ExitWithError (255);
				}

				String validIpAddressRegex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
				Match ipMatch = Regex.Match (options.serverAddress, validIpAddressRegex);

				if (!ipMatch.Success) {
					Utils.ExitWithError (255);
				}

				if (options.initialBalance != null) {					
					operationMode = (int)Utils.ModesOperations.NEW;

					if (Utils.isValidCurrencyAmount(options.initialBalance)) {
						try{
							initialBalance = double.Parse(options.initialBalance, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
							if(initialBalance < 10.00)
								throw new FormatException("Balance must be gratter or equal to 10.00");
						}
						catch(FormatException){
							Utils.ExitWithError (255);
						}
					} else {
						Utils.ExitWithError (255);
					}
				}

				if (options.deposit != null) {	
					
					if (operationMode.Equals ((int)Utils.ModesOperations.MODE_ERROR)) {
						operationMode = (int)Utils.ModesOperations.DEPOSIT;
					} else {
						Utils.ExitWithError (255);
					}	

					if (Utils.isValidCurrencyAmount(options.deposit)) {
						try{
							deposit = double.Parse (options.deposit, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
							if(deposit <= 0.00)
								throw new FormatException("Balance must be gratter or equal to 10.00");
						}
						catch(FormatException){
							Utils.ExitWithError (255);
						}
					} else {
						Utils.ExitWithError (255);
					}
				}

				if (options.withdraw != null) {	

					if (operationMode.Equals ((int)Utils.ModesOperations.MODE_ERROR)) {
						operationMode = (int)Utils.ModesOperations.WITHDRAW;
					} else {
						Utils.ExitWithError (255);
					}	

					if (Utils.isValidCurrencyAmount(options.withdraw)) {
						try{
							withdraw = double.Parse (options.withdraw, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
							if(withdraw <= 0.00)
								throw new FormatException("Balance must be gratter or equal to 10.00");
						}
						catch(FormatException){
							Utils.ExitWithError (255);
						}
					} else {
						Utils.ExitWithError (255);
					}
				}

				if (options.showBalance) {	
					if (operationMode.Equals ((int)Utils.ModesOperations.MODE_ERROR)) {
						operationMode = (int)Utils.ModesOperations.BALANCE;
					} else {
						Utils.ExitWithError (255);
					}	
				}


				if (operationMode.Equals ((int)Utils.ModesOperations.MODE_ERROR)) {
					Utils.ExitWithError (255);
				}
			}

			atmAction.account = options.username;
			atmAction.accountCard = options.cardFile;
			switch (operationMode) {
			case (int)Utils.ModesOperations.NEW:
				if (File.Exists (options.cardFile)) {					
					Utils.ExitWithError (255);
				}
				atmAction.operationMode = (int)Utils.ModesOperations.NEW;
				atmAction.initial_balance = initialBalance;
				break;
			case (int)Utils.ModesOperations.DEPOSIT:
				atmAction.operationMode = (int)Utils.ModesOperations.DEPOSIT;
				atmAction.deposit = deposit;
				break;
			case (int)Utils.ModesOperations.WITHDRAW:
				atmAction.operationMode = (int)Utils.ModesOperations.WITHDRAW;
				atmAction.withdraw = withdraw;
				break;
			case (int)Utils.ModesOperations.BALANCE:
				atmAction.operationMode = (int)Utils.ModesOperations.BALANCE;
				atmAction.balance = true;
				break;
			default:
				break;
			}

			TcpClient tcpClient;
			NetworkStream stream = null;
			tcpClient = new TcpClient(options.serverAddress, options.port);
			stream = tcpClient.GetStream();
			byte[] message = Utils.Encrypt(Utils.ObjectToByteArray(atmAction),authFile);
			stream.Write ( message, 0, message.Length);

			Byte[] response = new Byte[(int)tcpClient.ReceiveBufferSize];
			stream.Read (response, 0, (int)tcpClient.ReceiveBufferSize);
			var trimResponse = response.TakeWhile((v, index) => response.Skip(index).Any(w => w != 0x00)).ToArray();

			AtmAction atmActionFromServer = (AtmAction)Utils.ByteArrayToObject (Utils.Decrypt(trimResponse,authFile));
			if (atmActionFromServer.successfulTransaction) {
				if (!File.Exists (atmActionFromServer.accountCard)) {					
					File.Create (atmActionFromServer.accountCard);
				}
				Console.WriteLine (atmActionFromServer.ToJson ());
			} else {
				tcpClient.Close();
				stream.Close();
				Utils.ExitWithError (255);
			}

			tcpClient.Close();
			stream.Close();
			return 0;
		}
	}
}
