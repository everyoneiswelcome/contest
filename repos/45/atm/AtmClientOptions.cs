﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using CommandLine;
using CommandLine.Text;

namespace atm
{
	partial class MyClient
	{
		public sealed class ClientOptions
		{

			[Option('s', null, Required = false, DefaultValue = "bank.auth", HelpText = "Input file with data to process.")]
			public string authFile {get; set;}

			[Option('p', null ,MetaValue = "INT", DefaultValue = 2999, HelpText = "Port where the banks server will run.")]
			public int port { get; set; }

			[Option('a', null, Required = true, HelpText = "Account username.")]
			public string username {get; set;}

			[Option('i', null, Required = false,  DefaultValue = "127.0.0.1", HelpText = "Bank ip address")]
			public string serverAddress {get; set;}

			[Option('c', null, Required = false, HelpText = "User unique card file.")]
			public string cardFile {get; set;}

			[Option('n', null , HelpText = "Parameter to create a new accont followed by the balance gratter than 10.00")]
			public string initialBalance { get; set; }

			[Option('d', null , HelpText = "Parameter to deposit an amount gratter than 0.00")]
			public string deposit { get; set; }

			[Option('w', null , HelpText = "Parameter to withdraw an amount gratter than 0.00")]
			public string withdraw { get; set; }

			[Option("g", HelpText = "Show balance of the user's account")]
			public bool showBalance { get; set; }

			[ParserState]
			public IParserState LastParserState { get; set; }

			[HelpOption]
			public string GetUsage()
			{
				return HelpText.AutoBuild(this, current => HelpText.DefaultParsingErrorsHandler(this, current));
			}
		}

	}
}
