﻿using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
namespace utils
{
	public static class Utils
	{
		static Regex numericRegex;
		static Regex usernameFileRegex;
		static Regex decimalRegex;
		static BinaryFormatter bf;
		static byte[] keyBytes;

		private static byte[] getKeyBytes(string keyfiles){
			if(keyBytes == null)
				keyBytes = File.ReadAllBytes(keyfiles);

			return keyBytes;
		}

		public enum ModesOperations 
		{
			MODE_ERROR = 0,
			NEW = 1,
			DEPOSIT = 2,
			WITHDRAW = 3,
			BALANCE = 4
		}

		static readonly string VIKey = "HR$2pIjHASdasdas";
		public static byte[] ObjectToByteArray(Object obj)
		{
			if(obj == null)
				return null;
			BinaryFormatter bf = new BinaryFormatter();
			MemoryStream ms = new MemoryStream();
			bf.Serialize(ms, obj);
			return ms.ToArray();
		}

		public static Object ByteArrayToObject(byte[] arrBytes)
		{
			MemoryStream memStream = new MemoryStream();
			BinaryFormatter binForm = new BinaryFormatter();
			memStream.Write(arrBytes, 0, arrBytes.Length);
			memStream.Seek(0, SeekOrigin.Begin);
			Object obj = (Object) binForm.Deserialize(memStream);
			return obj;
		}

		public static bool isValidNumber(string number){
			if(numericRegex == null)
				numericRegex = new Regex (@"^(0|[1-9][0-9]*)$");
			
			Match numericMatch = numericRegex.Match (number);
			return numericMatch.Success;
		}

		public static bool isValidUsername(string username){
			if (usernameFileRegex == null)
				usernameFileRegex = new Regex (@"^[0-9a-z\-._]+$");

			Match usernameMatch = usernameFileRegex.Match (username);
			if (!(usernameMatch.Success && (username.Length >= 1 && username.Length <= 255))) {
				return false;
			}
			return true;
		}

		public static bool isValidFile(string fileName){
			if (usernameFileRegex == null)
				usernameFileRegex = new Regex (@"^[0-9a-z\-._]+$");

			Match fileMatch = usernameFileRegex.Match (fileName);
			if (fileMatch.Success && (fileName.Length > 1 && fileName.Length <= 255) && !fileName.Equals(".") && !fileName.Equals("..")) {
				return true;
			}
			return false;
		}

		public static bool isValidCurrencyAmount(string amount){
			if (decimalRegex == null)
				decimalRegex = new Regex (@"^[0-9]{2}$");

			try{
				string[] separators = {"."};
				string[] amountParts = amount.Split(separators, StringSplitOptions.RemoveEmptyEntries);
				if (amountParts.Length > 2)
					return false;

				Match decimalMatch = decimalRegex.Match (amountParts[1]);

				if(Utils.isValidNumber(amountParts[0]) && decimalMatch.Success && (Double.Parse(amountParts[0]) >= 0 && Double.Parse(amountParts[0]) <= 4294967295 ))
					return true;
				else
					return false;

			}
			catch{
				return false;
			}
		}

		public static void ExitWithError(int error){
			//Console.WriteLine ("255");
			Environment.Exit (error);
		}

		public static byte[] Encrypt(byte[] plainText,string authFile)
		{
			//byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
			byte[] plainTextBytes = plainText;

			byte[] keyBytes = getKeyBytes (authFile);

			var symmetricKey = new AesManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
			var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

			byte[] cipherTextBytes;

			using (var memoryStream = new MemoryStream())
			{
				using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
				{
					cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
					cryptoStream.FlushFinalBlock();
					cipherTextBytes = memoryStream.ToArray();
					cryptoStream.Close();
				}
				memoryStream.Close();
			}
			return cipherTextBytes;
			//return Convert.ToBase64String(cipherTextBytes);
		}

		public static byte[] Decrypt(byte[] encryptedText,string authFile)
		{
			//byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
			byte[] cipherTextBytes =encryptedText;

			byte[] keyBytes = getKeyBytes (authFile);
			var symmetricKey = new AesManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None};

			var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
			var memoryStream = new MemoryStream(cipherTextBytes);
			var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Write);
			cryptoStream.Write (encryptedText, 0, encryptedText.Length);
			cryptoStream.Close();

			byte[] plainTextBytes =  memoryStream.ToArray ();
			//int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
			memoryStream.Close();
			//return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
			return plainTextBytes;
		}

		public static byte[] Serialize<T>(this T m)
		{
			if (bf == null)
				bf = new BinaryFormatter ();
			using (var ms = new MemoryStream())
			{
				bf.Serialize(ms, m);
				return ms.ToArray();
			}
		}

		public static T Deserialize<T>(this byte[] byteArray)
		{
			if (bf == null)
				bf = new BinaryFormatter ();
			using (var ms = new MemoryStream(byteArray))
			{
				return (T)bf.Deserialize(ms);
			}
		}

		public static byte[] EncryptMyData(byte[] plainData, string authFile)
		{
			var provider = new AesCryptoServiceProvider();      
			byte[] Key = File.ReadAllBytes(authFile);
			provider.Key = Key;
			provider.IV =  Encoding.ASCII.GetBytes(VIKey);
			byte[] encrypted;
			var encryptor = provider.CreateEncryptor(provider.Key, provider.IV);
			using (MemoryStream msEncrypt = new MemoryStream ()) {
				using (var cryptoStream = new CryptoStream (msEncrypt, encryptor, CryptoStreamMode.Write)) {					
					cryptoStream.Write (plainData, 0, plainData.Length);
					encrypted = msEncrypt.ToArray ();
				}     
			}

			return encrypted;		      
		}

		public static byte[] DecryptMyData(byte[] encryptedData, string authFile)
		{
			var provider = new AesCryptoServiceProvider();
			byte[] Key = File.ReadAllBytes(authFile);
			provider.Key = Key;
			provider.IV =  Encoding.ASCII.GetBytes(VIKey);
			var encryptor = provider.CreateEncryptor(provider.Key, provider.IV);
			byte[] decrypted;
			using (var destination = new MemoryStream())
			{
				using (var cryptoStream = new CryptoStream(destination, encryptor, CryptoStreamMode.Write))
				{
					cryptoStream.Write (encryptedData, 0, encryptedData.Length);
					decrypted = destination.ToArray ();
				}
			}

			return decrypted;
		}




	}
}

