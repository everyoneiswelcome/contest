﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using utils;

namespace atm
{
	public class MyClient
	{		
		private static int operationMode = (int)Utils.ModesOperations.MODE_ERROR;
		private static double initialBalance = 0.00;
		private static double deposit = 0.00;
		private static double withdraw = 0.00;
		private static string authFile= "bank.auth";
		static DateTime start;
		static DateTime finish;
		static TimeSpan span;

		private static string[] parametros = { "-s", "-i", "-p", "-c", "-a", "-n", "-d", "-w", "-g" };
		private static int[] cantidad = new int[parametros.Length];

		static int Main(string[] args)
		{
			start = DateTime.Now;
			AtmAction atmAction = new AtmAction ();

			for (int j = 0; j < args.Length; j++) {

				if (args [j] == "-g") {
					if ((j + 1) < args.Length) {
						if (!args [j + 1].Contains ("-"))
							Utils.ExitWithError (255);
					}
				}

				for (int i = 0; i < parametros.Length; i++) {
					if (args[j].Contains (parametros [i])) {
						cantidad [i]++;
						if (cantidad [i] > 1)
							Utils.ExitWithError (255);


					}
				}
			}

//			finish = DateTime.Now;
//			span = finish - start;
//			Console.WriteLine ("Before parse time: " + (int)span.TotalMilliseconds);
			CMDLineParser parserCMD = new CMDLineParser();
			parserCMD.throwInvalidOptionsException = true;

			//add Options to parse
			CMDLineParser.Option authFileO = parserCMD.AddStringParameter("-s", "Input file with data to process.", false);
			CMDLineParser.Option port = parserCMD.AddStringParameter("-p", "Input file with data to process.", false);
			CMDLineParser.Option username = parserCMD.AddStringParameter("-a", "Input file with data to process.", true);
			CMDLineParser.Option serverAddress = parserCMD.AddStringParameter("-i", "Input file with data to process.", false);
			CMDLineParser.Option cardFile = parserCMD.AddStringParameter("-c", "Input file with data to process.", false);
			CMDLineParser.Option initialBalanceO = parserCMD.AddStringParameter("-n", "Input file with data to process.", false);
			CMDLineParser.Option depositO = parserCMD.AddStringParameter("-d", "Input file with data to process.", false);
			CMDLineParser.Option withdrawO = parserCMD.AddStringParameter("-w", "Input file with data to process.", false);
			CMDLineParser.Option showBalance = parserCMD.AddBoolSwitch("-g", "Input file with data to process.");


			try
			{
				parserCMD.Parse(args);
//				finish = DateTime.Now;
//				span = finish - start;
//				Console.WriteLine ("Parse time: " + (int)span.TotalMilliseconds);
				authFileO.Value = authFileO.Value == null ? "bank.auth" : authFileO.Value;
				port.Value = port.Value == null ? "3000" : port.Value;
				serverAddress.Value = serverAddress.Value == null ? "127.0.0.1" : serverAddress.Value;

				if (Int32.Parse(port.Value.ToString()) < 1024 || Int32.Parse(port.Value.ToString()) > 65535 || !Utils.isValidNumber(port.Value.ToString())) {
					Utils.ExitWithError (255);
				}

				if (!Utils.isValidUsername(username.Value.ToString())) {
					Utils.ExitWithError (255);	
				}

				if (cardFile.Value == null) {
					cardFile.Value = username.Value + ".card";
				}

				if (!Utils.isValidFile (cardFile.Value.ToString())) {						
					Utils.ExitWithError (255);
				}

				if (!Utils.isValidFile (authFileO.Value.ToString())) {						
					Utils.ExitWithError (255);
				} else {
					authFile = authFileO.Value.ToString();
				}

				if (!File.Exists (authFile)) {
					Utils.ExitWithError (255);
				}

				String validIpAddressRegex = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";
				Match ipMatch = Regex.Match (serverAddress.Value.ToString(), validIpAddressRegex);

				if (!ipMatch.Success) {
					Utils.ExitWithError (255);
				}

				if (initialBalanceO.Value != null) {					
					operationMode = (int)Utils.ModesOperations.NEW;

					if (Utils.isValidCurrencyAmount(initialBalanceO.Value.ToString())) {
						try{
							initialBalance = double.Parse(initialBalanceO.Value.ToString(), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
							if(initialBalance < 10.00)
								Utils.ExitWithError (255);
						}
						catch(FormatException){
							Utils.ExitWithError (255);
						}
					} else {
						Utils.ExitWithError (255);
					}
				}

				if (depositO.Value != null) {	
					
					if (operationMode.Equals ((int)Utils.ModesOperations.MODE_ERROR)) {
						operationMode = (int)Utils.ModesOperations.DEPOSIT;
					} else {
						Utils.ExitWithError (255);
					}	

					if (Utils.isValidCurrencyAmount(depositO.Value.ToString())) {
						try{
							deposit = double.Parse (depositO.Value.ToString(), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
							if(deposit <= 0.00)
								Utils.ExitWithError (255);
						}
						catch(FormatException){
							Utils.ExitWithError (255);
						}
					} else {
						Utils.ExitWithError (255);
					}
				}

				if (withdrawO.Value != null) {	

					if (operationMode.Equals ((int)Utils.ModesOperations.MODE_ERROR)) {
						operationMode = (int)Utils.ModesOperations.WITHDRAW;
					} else {
						Utils.ExitWithError (255);
					}	

					if (Utils.isValidCurrencyAmount(withdrawO.Value.ToString())) {
						try{
							withdraw = double.Parse (withdrawO.Value.ToString(), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
							if(withdraw <= 0.00)
								Utils.ExitWithError (255);
						}
						catch(FormatException){
							Utils.ExitWithError (255);
						}
					} else {
						Utils.ExitWithError (255);
					}
				}

				if (showBalance.Value != null) {	
					if((bool)showBalance.Value){
						if (operationMode.Equals ((int)Utils.ModesOperations.MODE_ERROR)) {
							operationMode = (int)Utils.ModesOperations.BALANCE;
						} else {
							Utils.ExitWithError (255);
						}	
					}
				}


				if (operationMode.Equals ((int)Utils.ModesOperations.MODE_ERROR)) {
					Utils.ExitWithError (255);
				}

			}
			catch (Exception)
			{
				Utils.ExitWithError (255);
			}

			atmAction.account = username.Value.ToString();
			atmAction.accountCard = cardFile.Value.ToString();
			switch (operationMode) {
			case (int)Utils.ModesOperations.NEW:
				if (File.Exists (cardFile.Value.ToString())) {					
					Utils.ExitWithError (255);
				}
				atmAction.operationMode = (int)Utils.ModesOperations.NEW;
				atmAction.initial_balance = initialBalance;
				break;
			case (int)Utils.ModesOperations.DEPOSIT:
				atmAction.operationMode = (int)Utils.ModesOperations.DEPOSIT;
				atmAction.deposit = deposit;
				break;
			case (int)Utils.ModesOperations.WITHDRAW:
				atmAction.operationMode = (int)Utils.ModesOperations.WITHDRAW;
				atmAction.withdraw = withdraw;
				break;
			case (int)Utils.ModesOperations.BALANCE:
				atmAction.operationMode = (int)Utils.ModesOperations.BALANCE;
				atmAction.balance = true;
				break;
			default:
				break;
			}

			// Data buffer for incoming data.
			byte[] bytes = new byte[2048];

			// Connect to a remote device.
			try {
				// Establish the remote endpoint for the socket.
				// This example uses port 11000 on the local computer.

				//IPHostEntry ipHostInfo = Dns.Resolve(serverAddress.Value.ToString());
//				finish = DateTime.Now;
//				span = finish - start;
//				Console.WriteLine ("Before Resolve time: " + (int)span.TotalMilliseconds);
				IPAddress ipAddress = IPAddress.Parse(serverAddress.Value.ToString());
//				finish = DateTime.Now;
//				span = finish - start;
//				Console.WriteLine ("Resolve time: " + (int)span.TotalMilliseconds);
				IPEndPoint remoteEP = new IPEndPoint(ipAddress,Int32.Parse(port.Value.ToString()));
//				finish = DateTime.Now;
//				span = finish - start;
//				Console.WriteLine ("RemoteEP time: " + (int)span.TotalMilliseconds);

				// Create a TCP/IP  socket.
				Socket sender = new Socket(AddressFamily.InterNetwork, 
					SocketType.Stream, ProtocolType.Tcp );
				sender.ReceiveTimeout = 10000;
				sender.SendTimeout = 10000;
//				finish = DateTime.Now;
//				span = finish - start;
//				Console.WriteLine ("Create Socket time: " + (int)span.TotalMilliseconds);


				// Connect the socket to the remote endpoint. Catch any errors.
				try {
					sender.Connect(remoteEP);

//					finish = DateTime.Now;
//					span = finish - start;
//					Console.WriteLine ("Connect time: " + (int)span.TotalMilliseconds);


					byte[] message = Utils.Encrypt(Utils.Serialize<AtmAction>(atmAction),authFile);
//					finish = DateTime.Now;
//					span = finish - start;
//					Console.WriteLine ("Encrypt time: " + (int)span.TotalMilliseconds);

					int bytesSent = sender.Send(message);
//					finish = DateTime.Now;
//					span = finish - start;
//					Console.WriteLine ("Send time: " + (int)span.TotalMilliseconds);

					int bytesRec = sender.Receive(bytes);
//					finish = DateTime.Now;
//					span = finish - start;
//					Console.WriteLine ("Receive time: " + (int)span.TotalMilliseconds);

					byte[] _bytes = new List<byte>(bytes).GetRange(0,bytesRec ).ToArray();
					AtmAction atmActionFromServer = Utils.Deserialize<AtmAction>(Utils.Decrypt(_bytes,authFile));
//					finish = DateTime.Now;
//					span = finish - start;
//					Console.WriteLine ("Decrypt time: " + (int)span.TotalMilliseconds);

					if (atmActionFromServer.successfulTransaction) {
						if (!File.Exists (atmActionFromServer.accountCard)) {					
							File.Create (atmActionFromServer.accountCard);
						}
						Console.WriteLine (atmActionFromServer.ToJson ());
					} else {						
						Utils.ExitWithError (255);
					}
					// Release the socket.
					sender.Shutdown(SocketShutdown.Both);
					sender.Close();

				} catch (ArgumentNullException ) {
					Utils.ExitWithError (255);
				} catch (SocketException ) {
					Utils.ExitWithError (63);
				} catch (Exception ) {
					Utils.ExitWithError (255);
				}

			} catch (Exception ) {
				Utils.ExitWithError (255);
			}

//			finish = DateTime.Now;
//			span = finish - start;
//			Console.WriteLine ("Total time: " + (int)span.TotalMilliseconds);
			return 0;
		}

		public static void StartClient() {
	       
	    }


	}
}
