﻿using System;

namespace bank
{
	public class Account
	{
		public String AccUser { get; set; }
		public String AccCard { get; set; }
		public double Balance { get; set; }
	}
}

