﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using utils;

namespace bank
{
	public class MyServer
	{
//		private static TcpListener listener;
		public static string data = null;
//		private static Thread thread;
		private static string authFile = "bank.auth";
		private static Dictionary<String, Account> cuentasDelBanco = new Dictionary<string, Account>();
		private static string[] parametros = { "-s", "-p" };
		private static int[] cantidad = new int[parametros.Length];

		static int Main(string[] args)
		{
			foreach (var item in args) {
				for (int i = 0; i < parametros.Length; i++) {
					if (item.Contains (parametros [i])) {
						cantidad [i]++;
						if (cantidad [i] > 1)
							Utils.ExitWithError (255);
					}
				}
			}

			CMDLineParser parserCMD = new CMDLineParser();
			parserCMD.throwInvalidOptionsException = true;

			//add Options to parse
			CMDLineParser.Option authFileO = parserCMD.AddStringParameter("-s", "Input file with data to process.", false);
			CMDLineParser.Option port = parserCMD.AddIntParameter("-p", "Input file with data to process.", false);


			try
			{
				parserCMD.Parse(args);
				authFileO.Value = authFileO.Value == null ? "bank.auth" : authFileO.Value;
				port.Value = port.Value == null ? 3000 : port.Value;
				if ((int)port.Value < 1024 || (int)port.Value > 65535) {
					Utils.ExitWithError (255);
				}

				if (!Utils.isValidFile(authFileO.Value.ToString())) {						
					Utils.ExitWithError (255);
				}

				if (File.Exists (authFileO.Value.ToString())) {
					Utils.ExitWithError (255);
				} else {
					byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
					byte[] key = Guid.NewGuid().ToByteArray();
					IEnumerable<byte> rv = time.Concat (key);
					File.WriteAllBytes (authFileO.Value.ToString(),rv.ToArray());
					authFile = authFileO.Value.ToString();
					Console.WriteLine ("created");
				}

				IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
				IPEndPoint localEndPoint = new IPEndPoint(ipAddress, (int)port.Value);

				// Create a TCP/IP socket.
				Socket listener = new Socket(AddressFamily.InterNetwork,
					SocketType.Stream, ProtocolType.Tcp );

				// Bind the socket to the local endpoint and 
				// listen for incoming connections.
				try {
					listener.Bind(localEndPoint);
					listener.Listen(10);

					// Start listening for connections.
					while (true) {
						try{
							// Program is suspended while waiting for an incoming connection.
							Socket handler = listener.Accept();
							handler.ReceiveTimeout = 10000;
							handler.SendTimeout = 10000;
							byte[] bytes = new byte[2048];
							data = null;
							int bytesRec = handler.Receive(bytes);
							byte[] _bytes = new List<byte>(bytes).GetRange(0,bytesRec ).ToArray();

							//		                while (true) {
							//		                    bytes = new byte[1024];
							//		                    int bytesRec = handler.Receive(bytes);
							//							//AtmAction atmActionFromClient = (AtmAction)Utils.ByteArrayToObject (Utils.Decrypt(bytes,authFile));
							//		                    //data += Encoding.ASCII.GetString(bytes,0,bytesRec);
							//							i++;
							//							if (i >= bytes.Length) {
							//		                        break;
							//		                    }
							//		                }
							//var trimResponse = bytes.TakeWhile((v, index) => bytes.Skip(index).Any(w => w != 0x00)).ToArray();
							AtmAction atmActionFromClient = Utils.Deserialize<AtmAction>(Utils.Decrypt(_bytes,authFile));

							ManageTransaction (ref atmActionFromClient);

							if (atmActionFromClient.successfulTransaction) {
								Console.WriteLine(atmActionFromClient.ToJson());
							}	

							byte[] resMessage = Utils.Encrypt(Utils.Serialize<AtmAction>(atmActionFromClient),authFile);

							handler.Send(resMessage);
							handler.Shutdown(SocketShutdown.Both);
							handler.Close();
						}
						catch(Exception){
							Console.WriteLine ("protocol_error");
						}
					}

				} catch (Exception) {
					Console.WriteLine ("protocol_error");
				}
			}
			catch{
				Utils.ExitWithError (255);
			}


//			var options = new ServerOptions();
//			var parser = new CommandLine.Parser();
//
//			if (parser.ParseArgumentsStrict(args, options, () => Utils.ExitWithError(255)))
//			{
//				if (options.port < 1024 || options.port > 65535) {
//					Utils.ExitWithError (255);
//				}
//
//				if (!Utils.isValidFile(options.authFile)) {						
//					Utils.ExitWithError (255);
//				}
//
//
//				if (File.Exists (options.authFile)) {
//					Utils.ExitWithError (255);
//				} else {
//					byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
//					byte[] key = Guid.NewGuid().ToByteArray();
//					IEnumerable<byte> rv = time.Concat (key);
//					File.WriteAllBytes (options.authFile,rv.ToArray());
//					authFile = options.authFile;
//					Console.WriteLine ("created");
//				}
//
//
//		 // Data buffer for incoming data.
//		        ///byte[] bytes = new Byte[1024];
//
//		        // Establish the local endpoint for the socket.
//		        // Dns.GetHostName returns the name of the 
//		        // host running the application.
//
//
////				try{
////					listener = new TcpListener(new IPAddress(new byte[] {127,0,0,1}),options.port);
////					thread = new Thread(new ParameterizedThreadStart(Listen));
////					thread.Start(listener);
////				}
////				catch(Exception){
////					Console.WriteLine ("protocol_error");
////				}
//			}
			return 0;
		}


		private static void Listen(Object obj)
		{
			TcpListener listeners = (TcpListener)obj;

			try{
				listeners.Start();
				while (true) {
					TcpClient client = listeners.AcceptTcpClient();
					while (!ThreadPool.QueueUserWorkItem( new WaitCallback(ListenThread), client ));
				}

			}
			catch(ThreadAbortException){
				Console.WriteLine ("protocol_error");
			}
			catch(SocketException){
				Console.WriteLine ("protocol_error");
			}


		}

		private static void ListenThread(Object obj)
		{
			TcpClient client = (TcpClient)obj;

			try{
				NetworkStream netstream = client.GetStream();
				netstream.ReadTimeout = 10000;
				netstream.WriteTimeout = 10000;
				Byte[] response = new Byte[(int)((TcpClient)client).ReceiveBufferSize];
				netstream.Read (response, 0, (int)((TcpClient)client).ReceiveBufferSize);
				var trimResponse = response.TakeWhile((v, index) => response.Skip(index).Any(w => w != 0x00)).ToArray();
				AtmAction atmActionFromClient = (AtmAction)Utils.ByteArrayToObject (Utils.Decrypt(trimResponse,authFile));

				ManageTransaction (ref atmActionFromClient);

				if (atmActionFromClient.successfulTransaction) {
					Console.WriteLine(atmActionFromClient.ToJson());
				}				

				byte[] resMessage = Utils.Encrypt(Utils.ObjectToByteArray(atmActionFromClient),authFile);
				netstream.Write(resMessage, 0, resMessage.Length);
				netstream.Flush ();
				netstream.Close();
			}
			catch(Exception){
				Console.WriteLine ("protocol_error");
			}
			finally{
				client.Close ();
			}
		}

		private static void ManageTransaction(ref AtmAction transaction){
						
			if (transaction.operationMode == (int)Utils.ModesOperations.NEW) {
				if (!cuentasDelBanco.ContainsKey (transaction.account)) {
					Account nuevaCuenta = new Account ();
					nuevaCuenta.AccUser = transaction.account;
					nuevaCuenta.AccCard = transaction.accountCard;
					nuevaCuenta.Balance = transaction.initial_balance;
					cuentasDelBanco.Add (transaction.account, nuevaCuenta);
					transaction.successfulTransaction = true;
				} else {
					transaction.successfulTransaction = false;
				}
			}

			if (transaction.operationMode == (int)Utils.ModesOperations.DEPOSIT) {
				Account cuenta = new Account ();
				if (cuentasDelBanco.TryGetValue(transaction.account, out cuenta)) {
					if (transaction.accountCard.Equals (cuenta.AccCard)) {						
						cuenta.Balance = cuenta.Balance + transaction.deposit;
						cuentasDelBanco[transaction.account] = cuenta;
						transaction.successfulTransaction = true;
					} else {
						transaction.successfulTransaction = false;
					}
				} else {
					transaction.successfulTransaction = false;
				}
			}

			if (transaction.operationMode == (int)Utils.ModesOperations.WITHDRAW) {
				Account cuenta = new Account ();
				if (cuentasDelBanco.TryGetValue(transaction.account, out cuenta)) {
					if (transaction.accountCard.Equals (cuenta.AccCard)) {
						if (Math.Round(Math.Round(cuenta.Balance, 2) - Math.Round(transaction.withdraw, 2), 2) >= 0) {
							cuenta.Balance = Math.Round(Math.Round(cuenta.Balance, 2) - Math.Round(transaction.withdraw, 2), 2);
							cuentasDelBanco [transaction.account] = cuenta;
							transaction.successfulTransaction = true;
						} else {
							transaction.successfulTransaction = false;
						}
					} else {
						transaction.successfulTransaction = false;
					}
				} else {
					transaction.successfulTransaction = false;
				}
			}

			if (transaction.operationMode == (int)Utils.ModesOperations.BALANCE) {				
				Account cuenta = new Account ();
				if (cuentasDelBanco.TryGetValue(transaction.account, out cuenta)) {
					if (transaction.accountCard.Equals (cuenta.AccCard)) {						
						transaction.serverBalance = cuenta.Balance;
						transaction.successfulTransaction = true;
					} else {
						transaction.successfulTransaction = false;
					}
				} else {
					transaction.successfulTransaction = false;
				}
			}
		}
	}
}
