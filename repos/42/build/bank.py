#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import argparse
import json
import os.path
import SocketServer
import signal
from lib.server_handler import BankHTTPRequestHandler
from lib.common import *
from lib import atm_proto

parser = argparse.ArgumentParser(add_help=False)
parser.print_usage = parser_usage
parser.error = parser_error 

parser.add_argument('-s', default="bank.auth", type=str, help="The authentication file that bank creates for the atm", dest="auth_file")
parser.add_argument('-p', default=3000, type=int, help="The TCP port that bank is listening on", dest="server_port")

debug("<<< running bank: " + ' '.join(sys.argv))

try:
    args = parser.parse_args()
except Exception as e:
    halt(255, e)

# auth file не должно быть!
if os.path.isfile(args.auth_file):
    halt(255, "auth file %s exists!" % args.auth_file)

# создает auth file
auth_data = atm_proto.generate_bank_auth()
try:
    # unbuffered
    f = open(args.auth_file, 'w', 0)
    f.write(auth_data)
    f.close
except Exception as e:
    halt(255, "error creating auth file %s: %s" % (args.auth_file, e))

# print 
output('created')

# создаем HTTP сервер
handler = BankHTTPRequestHandler
handler.key = atm_proto.str_to_bstr(auth_data)

httpd = SocketServer.TCPServer(("", args.server_port), handler, bind_and_activate=False)
httpd.allow_reuse_address = True
httpd.server_bind()
httpd.server_activate()

# создаем обработчик сигнала SIGTERM и SIGINT
def signal_handler(signum, frame):
    debug('<<< stopping server...')
    httpd.server_close()
    debug('<<< aborted on signal %s' % signum)
    sys.exit(0)

signal.signal(signal.SIGTERM, signal_handler)
signal.signal(signal.SIGINT, signal_handler)

debug("<<< running server at port %d..." % args.server_port)
try:
    while True:
        httpd.handle_request()
finally:
    debug('<<< stopping server on exception...')
    httpd.server_close()
