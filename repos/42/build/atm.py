#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import argparse
#import requests
import httplib
import socket
import json
import os.path
from lib.common import *
from lib import atm_proto

server_host = None
server_port = None
key = None

# делаем запрос на сервер через httplib
def query_server(params):
    token = atm_proto.generate_token()
    
    # add token to payload (will be encrypted)
    params["token"] = token
    data = atm_proto.encode(params, key)
        
    debug(">>> querying server %s:%d with message len %d" % (server_host, server_port, len(data)))
    headers = {
        "Content-type": "application/x-www-form-urlencoded", 
        "Accept": "text/plain", 
        "X-ATM-Token": token, 
        "Content-Length": len(data)
    }
    conn = httplib.HTTPConnection(server_host, server_port, False, atm_proto.TIMEOUT)
    try:
        conn.request("POST", "", data, headers)
        r = conn.getresponse()
    except socket.timeout as e:
        halt(63, e)
    except socket.error as e:
        halt(255, e)
        
    status = r.status
    content = r.read()
    
    debug(">>> response code: %d, raw content len: %d" % (status, len(content)))
    
    res_struct = atm_proto.decode(content, key)
    
    return (status, res_struct)

def mode_get_balance(args):
    try:
        (code, res) = query_server({"mode": "get_balance", "account": args.account, "pin": card_pin(args.card)})    
    except Exception as e:
        halt(63, e)
    
    if code == 200:
        output({"account": res["account"], "balance": res["balance"]})
    else:
        halt(255, 'unable to read account balance')

def mode_deposit(args):
    if not check_decimal(args.deposit):
        halt(255, "invalid number: %s" % args.deposit)
    
    try:
        (code, res) = query_server({"mode": "deposit", "account": args.account, "amount": args.deposit, "pin": card_pin(args.card)})    
    except Exception as e:
        halt(63, e)
        
    if code == 200:
        output({"account": res["account"], "deposit": res["deposit"]})
    else:
        halt(255, 'unable to deposit')
    
def mode_withdraw(args):
    if not check_decimal(args.withdraw):
        halt(255, "invalid number: %s" % args.withdraw)
    
    try:
        (code, res) = query_server({"mode": "withdraw", "account": args.account, "amount": args.withdraw, "pin": card_pin(args.card)})    
    except Exception as e:
        halt(63, e)
        
    if code == 200:
        output({"account": res["account"], "withdraw": res["withdraw"]})
    else:
        halt(255, 'unable to withdraw')
    
def mode_new_account(args):
    if not check_decimal(args.new_account):
        halt(255, "invalid number: %s" % args.new_account)
    
    if check_card(args.card):
        halt(255, "card file %s exists!" % args.card)
        
#    try:
    (code, res) = query_server({"mode": "new_account", "account": args.account, "initial_balance": float(args.new_account)})
#    except Exception as e:
#        halt(63, e)
        
    if code == 200:
        try:
            create_card(args.card, str(res["pin"]))
        except Exception as e:
            halt(255, "unable to create card file: %s" % e)
            
        output({"account": res["account"], "initial_balance": res["initial_balance"]})
    else:
        halt(255, 'unable to create new account')
    
def card_pin(card_name):
    pin = None
    try:
        file = open(card_name, "r")
        pin = file.read(atm_proto.PIN_LENGTH * 2)
        file.close()
    except Exception as e:
        halt(255, "error reading pin from card file %s: %s" % (card_name, e)) 
    
    return pin

def check_card(card_name):
    return os.path.isfile(card_name)

def create_card(card_name, pin):
    try:
        file = open(card_name, "w", 0)
        file.write(pin)
        file.close()
    except Exception as e:
        halt(255, "error creating card file %s: %s" % (card_name, e))

debug(">>> running atm: " + ' '.join(sys.argv))

parser = argparse.ArgumentParser(add_help=False)
parser.print_usage = parser_usage
parser.error = parser_error 

parser.add_argument('-s', default="bank.auth", type=argparse.FileType('r'), help="The authentication file that bank creates for the atm", dest="auth_file")
parser.add_argument('-i', default="127.0.0.1", type=str, help="The IP address that bank is running on", dest="server_host")
parser.add_argument('-p', default=3000, type=str, help="The TCP port that bank is listening on", dest="server_port")
parser.add_argument('-a', required=True, type=str, help="The customer's account name", dest="account")
parser.add_argument('-c', default=None, type=str, help="The customer's atm card file", dest="card")
parser.add_argument('-n', default=None, type=str, help="Create a new account with the given balance", dest="new_account")
parser.add_argument('-d', default=None, type=str, help="Deposit the amount of money specified", dest="deposit")
parser.add_argument('-w', default=None, type=str, help="Withdraw the amount of money specified", dest="withdraw")
parser.add_argument('-g', default=None, action="count", help="Get the current balance of the account", dest="get_balance")

try:
    args = parser.parse_args()
except Exception as e:
    halt(255, e)
    
try:
    str_key = args.auth_file.read(atm_proto.KEY_LENGTH * 2)
    debug("str_key: %s" % str_key)
    key = atm_proto.str_to_bstr(str_key)
    debug(">>> got key of len %d" % len(key))
except Exception as e:
    halt(255, "error reading key: %s" % e)
    
if not check_ipaddress(args.server_host):
    halt(255, "invalid server host: %s" % args.server_host)
if not check_port(args.server_port):
    halt(255, "invalid server port: %s" % args.server_port)
if (args.card == None) or (len(args.card) == 0):
    args.card = args.account + ".card"
if (args.card in ('.', '..')) or (len(args.card) > 255):
    halt(255, "invalid card name: %s" % args.card)
    
server_url = "http://%s:%s" % (args.server_host, args.server_port)
server_host = args.server_host
server_port = int(args.server_port)

modes = 0
if args.new_account:
    modes += 1
if args.deposit:
    modes += 1
if args.withdraw:
    modes += 1
if args.get_balance == 1:
    modes += 1

if modes > 1:
    halt(255, "more than one mode specified")
    
if args.new_account:
    mode_new_account(args)
elif args.deposit:
    mode_deposit(args)
elif args.withdraw:
    mode_withdraw(args)
elif args.get_balance == 1:
    mode_get_balance(args)
else:
    halt(255, "no mode of operation specified!")
