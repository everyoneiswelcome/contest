# -*- coding: utf-8 -*-

import re
from lib.common import *

class Account:
    MIN_INITIAL_BALANCE = 10.0
    MAX_AMOUNT = 4294967296.0
    MAX_NAME_LEN = 250
    
    @classmethod
    def check_name(cls, name):
        p = re.compile('^[_\-\.0-9a-z]+$') # allowed name pattern
        return (type(name) == str) and (len(name) <= Account.MAX_NAME_LEN) and (p.match(name) != None)
    
    @classmethod
    def check_amount(cls, amount):
        return (type(amount) == float) and (amount > 0.0) and (amount < Account.MAX_AMOUNT)

    def __init__(self, name, balance, pin):
        if not Account.check_name(name):
            raise ValueError("account name '%s' is invalid" % name)
        self._name = name
        if not Account.check_amount(balance):
            raise ValueError("initial balance %f is invalid" % balance)
        if balance < Account.MIN_INITIAL_BALANCE:
            raise ValueError("initial balance must be greater than %f" % Account.MIN_INITIAL_BALANCE)
        self._balance = balance
        
        self._pin = pin

    def deposit(self, amount):
        if not Account.check_amount(amount):
            raise ValueError("invalid amount to deposit: %f" % amount)
        self._balance += amount

    def withdraw(self, amount):
        if not Account.check_amount(amount):
            raise ValueError("invalid amount to withdraw: %f" % amount)
        if float_le(self._balance, amount):
            raise ValueError("insufficient funds")
        self._balance -= amount
        
    def check_pin(self, pin_challenge):
        return self._pin == pin_challenge

    def get_name(self):
        return self._name

    def get_balance(self):
        return float("%0.2f" % self._balance)
