# -*- coding: utf-8 -*-

import sys
import simplejson as json

DEBUG = True
EPSILON = 1e-03

# выводим дебаг-информацию в STDERR
def debug(msg):
    if DEBUG: sys.stderr.write(str(msg) + "\n")

def halt(code = 255, msg = ""):
    debug(msg)
    debug("exiting with code %s" % code)
    sys.exit(code)

def parser_usage():
    ""

def parser_error(msg):
    halt(255, msg)
    
def output(msg):
    if type(msg) == dict:
        print(json.dumps(msg, separators=(",", ":"), sort_keys=True))
    else:
        print(msg)
    sys.stdout.flush()

# проверяем, что число имеет правильный формат: ровно два знака после нуля, нет лишнего нуля в начале
def check_decimal(x):
    try:
        (a, b) = x.split('.')
    except Exception:
        return False
    return (len(a) >= 1) and (len(a) == 1 or a[0] != 0) and (str(int(a)) == a) and (len(b) == 2) and ("%02d" % int(b) == b)

def check_ipaddress(x):
    groups = x.split('.')
    if len(groups) == 4:
        res = True
        for num in groups:
            res = res and (int(num) >= 0) and (int(num) <= 255) and (str(int(num)) == num)
        return res
    else:
        return False
        
def check_port(p):
    return (p != "") and (str(p) == str(int(p))) and (int(p) >= 1024) and (int(p) <= 65535)
    
def float_eq(a, b):
    return abs(a - b) < EPSILON

def float_le(a, b):
    return a - b < - EPSILON
