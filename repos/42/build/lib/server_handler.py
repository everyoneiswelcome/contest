# -*- coding: utf-8 -*-

import sys
import BaseHTTPServer
import socket
from lib.common import *
from lib import atm_proto
from lib.account import Account

_bank_accounts = {}
_tokens = set()
class BankHTTPRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    timeout = atm_proto.TIMEOUT
    key = None
    
    def handle(self):
        try:
            BaseHTTPServer.BaseHTTPRequestHandler.handle(self)
        except socket.timeout as e:
            output('protocol_error')
    
    def handle_one_request(self):
        """Handle a single HTTP request.

        You normally don't need to override this method; see the class
        __doc__ string for information on how to handle specific HTTP
        commands such as GET and POST.

        """
        self.raw_requestline = self.rfile.readline(65537)
        if len(self.raw_requestline) > 65536:
            self.requestline = ''
            self.request_version = ''
            self.command = ''
            self.send_error(414)
            return
        if not self.raw_requestline:
            self.close_connection = True
            return
        if not self.parse_request():
            # An error code has been sent, just exit
            return
        mname = 'do_' + self.command
        if not hasattr(self, mname):
            self.send_error(501, "Unsupported method (%r)" % self.command)
            return
        method = getattr(self, mname)
        method()
        self.wfile.flush() #actually send the response if not already done.
    
    def do_POST(s):
        try:
            length = int(s.headers["Content-Length"])
            raw = s.rfile.read(length)
            debug("<<< raw input len: %d" % len(raw))
        except Exception as e:
            debug("<<< error parsing request: %s" % e)

        (res, out) = (500, None)
        try:
            data = atm_proto.decode(raw, BankHTTPRequestHandler.key)
            debug("<<< decoded input: %s" % data)
            if BankHTTPRequestHandler.validate_token(data["token"], s.headers["X-ATM-Token"]):
                # store token
                _tokens.add(data["token"])
                try:
                    (res, out) = BankHTTPRequestHandler.process(data)
                except Exception as e:
                    debug("<<< error processing request: %s" % e)
        except Exception as e:
            debug("<<< error decoding request: %s" % e)
                
        s.send_response(res)
        s.end_headers()
        if out:
            s.wfile.write(atm_proto.encode(out, BankHTTPRequestHandler.key))
            
    # log nothing
    def log_message(*args):
        pass

    @staticmethod
    def validate_token(token, challenge):
        if len(token) == 0:
            debug("<<< empty token")
            return False
        
        if token != challenge:
            debug("<<< token mismatch: token=%s, challenge=%s" % (token, challenge))
            return False
            
        if token in _tokens:
            debug("<<< token %s already used!" % token)

        return True
            
    @staticmethod
    def process(data):
        (res, out) = (500, None)
        if data and (type(data) == dict) and ('mode' in data):
            if data['mode'] == 'new_account':
                (res, out) = BankHTTPRequestHandler.mode_new_account(data)
            elif data['mode'] == 'get_balance':
                (res, out) = BankHTTPRequestHandler.mode_get_balance(data)
            elif data['mode'] == 'deposit':
                (res, out) = BankHTTPRequestHandler.mode_deposit(data)
            elif data['mode'] == 'withdraw':
                (res, out) = BankHTTPRequestHandler.mode_withdraw(data)
            else:
                debug("invalid mode")
                res = 500
        else:
            res = 500
        
        return (res, out)
        
    @staticmethod
    def mode_new_account(data):
        try:
            if str(data["account"]) in _bank_accounts:
                debug("account already exists")
                return (500, None)
            
            pin = atm_proto.generate_pin()
            account = Account(str(data["account"]), float(data["initial_balance"]), pin)
            _bank_accounts[account.get_name()] = account
            output({"account": account.get_name(), "initial_balance": account.get_balance()})
            return (200, {"account": account.get_name(), "initial_balance": account.get_balance(), "pin": pin})
        except Exception as e:
            debug("error creating account: %s" % e)
            return (500, None)
        
    @staticmethod
    def mode_get_balance(data):
        try:
            if not str(data["account"]) in _bank_accounts:
                debug("account does not exist")
                return (500, None)

            account = _bank_accounts[str(data["account"])]
            if account.check_pin(str(data["pin"])):
                output({"account": account.get_name(), "balance": account.get_balance()})
                return (200, {"account": account.get_name(), "balance": account.get_balance()})
            else:
                debug("pin mismatch")
                return (500, None)
        except Exception as e:
            debug("unable to check balance: %s" % e)
            return (500, None)

    @staticmethod
    def mode_deposit(data):
        try:
            if not str(data["account"]) in _bank_accounts:
                debug("account does not exist")
                return (500, None)

            account = _bank_accounts[str(data["account"])]
            if account.check_pin(str(data["pin"])):
                amount = float(data["amount"])
                account.deposit(amount)
                output({"account": account.get_name(), "deposit": amount})
                return (200, {"account": account.get_name(), "deposit": amount})
            else:
                debug("pin mismatch")
                return (500, None)
        except Exception as e:
            debug("unable to deposit: %s" % e)
            return (500, None)

    @staticmethod
    def mode_withdraw(data):
        try:
            if not str(data["account"]) in _bank_accounts:
                debug("account does not exist")
                return (500, None)

            account = _bank_accounts[str(data["account"])]
            if account.check_pin(str(data["pin"])):
                amount = float(data["amount"])
                account.withdraw(amount)
                output({"account": account.get_name(), "withdraw": amount})
                return (200, {"account": account.get_name(), "withdraw": amount})
            else:
                debug("pin mismatch")
                return (500, None)
        except Exception as e:
            debug("unable to withdraw: %s" % e)
            return (500, None)
