# -*- coding: utf-8 -*-

import simplejson as json
import random
from Crypto.Cipher import AES
import math
import struct
from . import common

TIMEOUT = 10
BLOCK_SIZE = 16
KEY_LENGTH = 16 # bytes
PIN_LENGTH = 16 # bytes
TOKEN_LENGTH = 16 # bytes

def encrypt(plain, key):
    iv = '\x00' * 16
    encryptor = AES.new(key, AES.MODE_CBC, IV=iv)
    return encryptor.encrypt(plain)
    
def decrypt(cipher, key):
    iv = '\x00' * 16
    decryptor = AES.new(key, AES.MODE_CBC, IV=iv)
    return decryptor.decrypt(cipher)

# structure -> string
def encode(struct, key):
    plain = json.dumps(struct, encoding="latin1")
    # pad
    new_len = int(math.ceil(len(plain) * 1.0 / BLOCK_SIZE) * BLOCK_SIZE)
    return encrypt(plain.ljust(new_len, ' '), key)
    
# string -> structure
def decode(raw_cipher, key):
    if raw_cipher == "":
        return ""
        
    raw_plain = decrypt(raw_cipher, key)
        
    struct = None
    try:
        struct = json.loads(raw_plain, encoding="latin1")
    except Exception as e:
        raise Exception("error decoding atm proto message: %s" % raw_plain)
        
    return struct
    
# generate auth file - this is the key
def generate_bank_auth():
    fmt = '%0' + str(KEY_LENGTH * 2) + 'x'
    return fmt % random.getrandbits(KEY_LENGTH * 8)

# generate pin - long of 128 bits
def generate_pin():
    fmt = '%0' + str(PIN_LENGTH * 2) + 'x'
    return fmt % random.getrandbits(PIN_LENGTH * 8)

# generate one-time token for server-client communication
def generate_token():
    fmt = '%0' + str(TOKEN_LENGTH * 2) + 'x'
    return fmt % random.getrandbits(TOKEN_LENGTH * 8)
    
# convert string to binary string
# '9ee366d46110f3fbeb782ccd2f72b5df' -> '\x9e\xe3f\xd4a\x10\xf3\xfb\xebx,\xcd/r\xb5\xdf'
def str_to_bstr(s):
    l = len(s) / 2
    return struct.pack("B" * l, *[int(s[i:i+2], l) for i in range(0, len(s), 2)])