#!/usr/bin/env python2
from gevent.pywsgi import WSGIServer, WSGIHandler
from gevent import socket
import sys
import json
import os
import base64
from datetime import datetime
from threading import Lock

# 10 second timeouts
socket.setdefaulttimeout(10)

# Dict for in-memory storage of account balances
__bankroll = {}
__bankroll_lock = Lock()

# nonce stuff
__nonces = {}
__nonces_lock = Lock()
__nonce_duration_sec = 2

# Globals for holding keys in memory.
__aes_key = __hmac_key = None

def protocol_error():
    print("protocol_error")
    sys.stdout.flush()

def request_error(start_response):
    # Send error to atm - must provide start_response
    start_response('500', [])
    return ['']

def get_nonce():
    nonce = generate_nonce()
    __nonces[nonce] = datetime.now()
    return nonce

def test_nonce(nonce):
    created = get_nonce_created(nonce)
    if not created:
        raise ValueError('Invalid nonce')

    elapsed_sec = (datetime.now() - created).total_seconds() if created else 0

    if elapsed_sec > __nonce_duration_sec:
        raise ValueError('Expired nonce')

def get_nonce_created(nonce):
    __nonces_lock.acquire()
    try:
        return __nonces.pop(nonce) if nonce in __nonces else None
    finally:
        __nonces_lock.release()

def test_mac(env, payload):
    payload_hmac = make_hmac(payload, __hmac_key)
    check_hmac(env['HTTP_MAC'], payload_hmac)

def test_account_existence(account):
    """
    assumes lock has already been acquired
    """
    if not __bankroll_lock.locked():
        raise RuntimeError('Must acquire lock before accessing bankroll')
    if account not in __bankroll:
        raise ValueError('Non-existent account entered')
    return

def test_negative_funds(account, amount):
    """
    assumes lock has already been acquired
    """
    if not __bankroll_lock.locked():
        raise RuntimeError('Must acquire lock before accessing bankroll')
    if __bankroll[account] - amount < -0.0001:
        raise ValueError('Not enough funds')
    return

def do_create(account, amount):
    __bankroll_lock.acquire()
    try:
        if (account in __bankroll):
            raise ValueError('account already exists')
        else:
            __bankroll[account] = amount
            return amount
    finally:
        __bankroll_lock.release()

def do_deposit(account, amount):
    __bankroll_lock.acquire()
    try:
        test_account_existence(account)
        __bankroll[account] += amount
        return amount
    finally:
        __bankroll_lock.release()

def do_withdraw(account, amount):
    __bankroll_lock.acquire()
    try:
        test_account_existence(account)
        test_negative_funds(account, amount)
        __bankroll[account] -= amount
        return amount
    finally:
        __bankroll_lock.release()

def get_balance(account):
    __bankroll_lock.acquire()
    try:
        test_account_existence(account)
        return round(__bankroll[account], 2)
    finally:
        __bankroll_lock.release()

def test_auth_file_existence(name):
    if os.path.exists(name):
        return general_error()

def validate_card(payload):
    account_hmac = make_hmac(payload['account'],__hmac_key)
    check_hmac(payload['card'], account_hmac)

def perform_transaction(payload):
    result = {'account': payload['account']}
    account = payload['account']

    mode = payload['mode']
    if mode != 'create':
        validate_card(payload)

    if (mode == 'create'):
        result['initial_balance'] = do_create(account, payload['initial_balance'])
    elif (mode == 'deposit'):
        result['deposit'] = do_deposit(account, payload['deposit'])
    elif (mode == 'withdraw'):
        result['withdraw'] = do_withdraw(account, payload['withdraw'])
    elif (mode == 'balance'):
        result['balance'] = get_balance(account)
    else:
        raise ValueError('invalid mode')

    # Turn result into string
    result = json.dumps(result)
    # Print result to console (bank spec)
    print(result)
    sys.stdout.flush()

    return result

def prepare_payload(payload, start_response):
    cipherpayload, iv = encrypt(payload, __aes_key)
    start_response('200 OK', [
        ('MAC', make_hmac(cipherpayload, __hmac_key)), # sets HTTP 'MAC' header (custom)
        ('IV', base64.b64encode(iv))
    ])
    return [cipherpayload]

def decrypt_payload(env, payload):
    test_mac(env, payload)
    payload = decrypt(payload, __aes_key,  base64.b64decode(env['HTTP_IV']))
    return payload

def handle_transaction(env, start_response):
    try:
        if env['PATH_INFO'] == '/':
            test_nonce(env['HTTP_NONCE'])
            payload = env['wsgi.input'].read()
            payload = decrypt_payload(env, payload)


            command = json.loads(payload)

            result = perform_transaction(command)
            result = prepare_payload(result, start_response)

            return result

        elif env['PATH_INFO'] == '/nonce':
            payload = env['wsgi.input'].read()
            test_mac(env, payload)
            nonce = get_nonce()
            result = prepare_payload(nonce, start_response)

            return result

        else:
            return request_error(start_response)
    except:
        return request_error(start_response)

class ErrorCapturingWSGIHandler(WSGIHandler):
    def read_requestline(self):
        result = None
        try:
            result = WSGIHandler.read_requestline(self)
        except:
            protocol_error()
            raise # re-raise error, to not change WSGIHandler functionality
        return result

class ErrorCapturingWSGIServer(WSGIServer):
    handler_class = ErrorCapturingWSGIHandler

def start_server(port):
    server = ErrorCapturingWSGIServer(
        ('', port), handle_transaction, log=None)

    server.serve_forever()

def make_keys():
    keys = os.urandom(32)
    # Generate 2 random keys, for AES + HMAC
    return keys[:16], keys[16:]

def generate_auth_file(name):
    # Modify global key variables
    global __aes_key, __hmac_key

    k1, k2 = make_keys()

    __aes_key = k1
    __hmac_key = k2

    # TODO last call for name validation
    # Open auth file for writing
    f = open(name, 'w')
    # write generated keys, separated by newline
    f.write(base64.b64encode(k1) + '\n' + base64.b64encode(k2))
    f.close()
    print('created')
    sys.stdout.flush() # Flush stdout (test runner hangs without this)

def main():
    try:
        # Parse options
        p = ModifiedOptionParser()

        p.add_option('-p', type="string", dest="port", default=3000, action="callback", callback=check_port)
        p.add_option('-s', type="string", dest="auth_file", default="bank.auth", action="callback", callback=check_filename)

        # TODO - perhaps POSIX compliance and arg length limits?

        options, arguments = p.parse_args()

        test_auth_file_existence(options.auth_file)

        # Run generation of auth file
        generate_auth_file(options.auth_file)
        # Start listening on specified port
        start_server(options.port)
    except:
        general_error()

if __name__ == '__main__':
    main()
