# gevent-http branch

## Requirements

* `python-gevent` is required. It's not (yet) included in the VM.
* run `sudo apt-get install python-gevent` for now.

## Design Notes

### TODOs
I had a lot of time on my hands, but I also wanted to make sure that there was still
a lot of fun code to write, so I focused on the network exchange and basic payload formats.
This version will have tons of errors, which we can test for & incrementally implement
the **TODO**s. No auth/card file creation or reading has been implemented yet, but
there are TODOs related to this in the code.

### Gevent

Since concurrency is a required feature of the bank server, I started researching
event-driven / "asynchronous" python implementations. I had heard of [Tornado](http://www.tornadoweb.org/en/stable/)
and came across some benchmarks and articles, and decided to give **gevent** a go.

* [Benchmarks](http://blog.kgriffs.com/2012/12/12/gevent-vs-tornado-benchmarks.html)
* [Great article / tutorial](http://blog.pythonisito.com/2012/08/building-tcp-servers-with-gevent.html)

They're a little old - feel free to bring up newer materials & patterns if you see them!

I went with a basic TCP server to start with, but we can easily refactor to HTTP
using gevent or another solution. Also, looks like setting up SSL is rather simple,
so we could end up using that for message integrity.

Update: Refactored to HTTP, needed the request/response mechanic!
