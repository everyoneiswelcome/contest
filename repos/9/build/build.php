<?php

$files = $argv;
array_shift($files);

foreach ($files as $file) {
    $full_path = __DIR__ . DIRECTORY_SEPARATOR . $file;
    file_put_contents($full_path, str_replace('{path}', __DIR__ . DIRECTORY_SEPARATOR, file_get_contents($full_path)));
}