<?php
require_once(__DIR__ . '/lib/core.php');

// parse input parameters
$input_args = array();

for ($i = 1; $i < count($argv); $i++) {

    if (substr($argv[$i], 0, 1) !== '-') {
        error_log('wrong parameter');
        exit(255);
    }

    $key = substr($argv[$i], 1);

    if (strlen($key) > 1) {
        $value = substr($key, 1);
        $key = substr($key, 0, 1);
    } else {
        $value = true;
        if (isset($argv[$i+1]) && substr($argv[$i+1], 0, 1) !== '-') {
            $value = $argv[$i+1];
            $i++;
        }
    }

    if(isset($input_args[$key])) {
        error_log('duplicated parameter');
        exit(255);
    }
    $input_args[$key] = $value;
}
// fix case when g parameter has value provided
if (isset($input_args['g']) && $input_args['g'] !== true) {
    error_log('argument g should not have value');
    exit(255);
}

// setup default values
$port = '3000';
if (isset($input_args['p'])) {
    $port = $input_args['p'];
}
$ip = '127.0.0.1';
if (isset($input_args['i'])) {
    $ip = $input_args['i'];
}
if (isset($input_args['a'])) {
    $account_name = $input_args['a'];
} else {
    error_log('account name is required parameter');
    exit(255);
}
$auth_file_name = 'bank.auth';
if (isset($input_args['s'])) {
    $auth_file_name = $input_args['s'];
}
$card_file_name = $account_name . '.card';
if (isset($input_args['c'])) {
    $card_file_name = $input_args['c'];
}

// validate input parameters
if (!preg_match('/^[_\-\.0-9a-z]*$/', $account_name) || strlen($account_name) > 250) {
    error_log('invalid account name');
    exit(255);
}

if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
    error_log('invalid ip');
    exit(255);
}

if (!preg_match('/^[_\-\.0-9a-z]*$/', $auth_file_name) || $auth_file_name == '.' || $auth_file_name == '..' || strlen($auth_file_name) > 255) {
    error_log('invalid auth file name');
    exit(255);
}
if (!preg_match('/^[_\-\.0-9a-z]*$/', $card_file_name) || $card_file_name == '.' || $card_file_name == '..' || strlen($card_file_name) > 255) {
    error_log('invalid card file name');
    exit(255);
}
if ((isset($input_args['n']) + isset($input_args['d']) + isset($input_args['w']) + isset($input_args['g'])) != 1) {
    error_log('action is not specified or more then one action is requested');
    exit(255);
}

$actions = array('n' => 'new', 'd' => 'deposit', 'w' => 'withdraw', 'g' => 'balance');
$action_key = isset($input_args['n']) ? 'n' : (isset($input_args['d']) ? 'd' : (isset($input_args['w']) ? 'w' : 'g'));
$action = $actions[$action_key];

if ($action != 'balance') {
    $amount = $input_args[$action_key];
    $number = number_format(doubleval($amount), 2, '.', '');

    if ($number !== $amount || $amount <= 0 || $amount > 4294967295.99) {
        error_log('wrong amount value');
        exit(255);
    }
}

if (number_format(intval($port), 0, '.', '') !== $port || $port < 1024 || $port > 65535) {
    error_log('wrong format of port parameter');
    exit(255);
}

if (!file_exists($auth_file_name)) {
    error_log('auth file does not exist');
    exit(255);
}

if (isset($input_args['n']) && file_exists($card_file_name)) {
    error_log('card file already exist');
    exit(255);
}

if (!isset($input_args['n']) && !file_exists($card_file_name)) {
    error_log('card file does not exists');
    exit(255);
}
if ($action_key == 'n' && $input_args['n'] < 10) {
    error_log('Initial balance is too small');
    exit(255);
}
if (($action_key == 'd' || $action_key == 'w') && $input_args[$action_key] <= 0) {
    error_log('balance is too small');
    exit(255);
}

// read key
$auth_key = file_get_contents($auth_file_name);

// check card file
if (!isset($input_args['n'])) {
    if (file_get_contents($card_file_name) !== get_card($account_name, $auth_key)) {
        error_log('card file does not correspond to the account');
        exit(255);
    }
}



// create message to be sent to bank
$json_request = array();
$json_request['action'] = $action;
if ($action != 'balance') {
    $json_request['amount'] = number_format($input_args[$action_key], 2, '.', '');
}

$json_request['account'] = $account_name;
$request_str = json_encode($json_request);

// setup network communication
try {
    $sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

    if (!socket_connect($sock, $ip, $port)) {throw new Exception('cannot connect');};

    // request nonce for secure connection
    if (socket_send($sock, 'nonce', 5, 0) === false) {
        throw new Exception('atm iv request error: ' . socket_strerror(socket_last_error($sock)));
    }

    socket_set_option($sock,SOL_SOCKET,SO_RCVTIMEO,array("sec"=>10,"usec"=>0));

    // receive nonce
    if (!socket_recv($sock, $iv, 1000, 0)) {
        throw new Exception('atm iv response error: ' . socket_strerror(socket_last_error($sock)));
    }

    if ($iv === 'protocol_error' || strlen($iv) !== 32) {
        throw new Exception('bank returned protocol error');
    }

    $request_str = encode_string($request_str, substr($iv, 0, 16), $auth_key);

    // send request
    socket_send($sock, $request_str, strlen($request_str), 0);

    // get response
    if (!socket_recv($sock, $response, 1000, 0)) {
        throw new Exception('atm response error:' . socket_strerror(socket_last_error($sock)));
    }
    $response = decode_string($response, substr($iv, 16), $auth_key);

    socket_close($sock);
} catch (Exception $e) {
    if (isset($sock) && $sock) socket_close($sock);
    error_log($e->getMessage());
    exit(63); // protocol error
}


try {
    $json_response = json_decode($response, 1);
} catch (Exception $e) {
    error_log('response contains invalid json data');
    exit(255);
}


// process response with an error
// valid response should contain "initial_balance", "deposit", "withdraw" or "balance" key depending of $action_key
$allowed_keys = array('n' => 'initial_balance', 'd' => 'deposit', 'w' => 'withdraw', 'g' => 'balance');
if (empty($response) || isset($json_response['error']) || empty($json_response) || !isset($json_response[$allowed_keys[$action_key]])) {
    error_log(isset($json_response['error']) ? $json_response['error'] : 'wrong format of bank response');
    exit(255);
}
// process successful response
if (isset($input_args['n'])) {
    // create card file
    file_put_contents($card_file_name, get_card($account_name, $auth_key));
}

print $response . "\n";