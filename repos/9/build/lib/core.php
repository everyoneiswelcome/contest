<?php

function get_card($account_name, $key) {
    return md5($account_name . base64_encode($key));
}

function encode_string($sValue, $iv, $sSecretKey) {
    $method = 'aes-128-cbc';
    return rtrim(base64_encode(openssl_encrypt($sValue, $method, $sSecretKey, true, $iv)), "\0\3");
}

function decode_string($sValue, $iv, $sSecretKey) {
    $method = 'aes-128-cbc';
    return rtrim(openssl_decrypt(base64_decode($sValue), $method, $sSecretKey, true, $iv), "\0\3");
}

function generate_iv() {
    return openssl_random_pseudo_bytes(32);
}

function generate_key() {
    return openssl_random_pseudo_bytes(256);
}