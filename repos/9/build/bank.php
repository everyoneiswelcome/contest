<?php
require_once(__DIR__ . '/lib/core.php');

// parse input parameters
$input_args = array();

for ($i = 1; $i < count($argv); $i++) {

    if (substr($argv[$i], 0, 1) !== '-') {
        error_log('wrong parameter');
        exit(255);
    }

    $key = substr($argv[$i], 1);

    if (strlen($key) > 1) {
        $value = substr($key, 1);
        $key = substr($key, 0, 1);
    } else {
        $value = true;
        if (isset($argv[$i+1]) && substr($argv[$i+1], 0, 1) !== '-') {
            $value = $argv[$i+1];
            $i++;
        }
    }

    if(isset($input_args[$key])) {
        error_log('duplicated parameter');
        exit(255);
    }
    $input_args[$key] = $value;
}
// setup default values
$port = 3000;
if (isset($input_args['p'])) {
    $port = $input_args['p'];
}
$auth_file_name = 'bank.auth';
if (isset($input_args['s'])) {
    $auth_file_name = $input_args['s'];
}

// validate parameters
if (!preg_match('/^[_\-\.0-9a-z]*$/', $auth_file_name) || $auth_file_name == '.' || $auth_file_name == '..' || strlen($auth_file_name) > 255) {
    error_log('invalid auth file name');
    exit(255);
}
if (file_exists($auth_file_name)) {
    error_log('auth file exists');
    exit(255);
}

// setup account storage
$bank_accounts = array();

// create auth file
$auth_key = generate_key();
file_put_contents($auth_file_name, $auth_key);
print "created" . "\n";


// open connection
$sock = socket_create_listen($port);

if (!$sock) {
    error_log('cannot open initiate network connection');

    exit(255);
}
// socket_getsockname($sock, $addr, $port);
$arrOpt = array('l_onoff' => 1, 'l_linger' => 0);
socket_set_block($sock);
socket_set_option($sock, SOL_SOCKET, SO_LINGER, $arrOpt);

// init connection
while($c = socket_accept($sock)) {
    try {
        socket_set_option($c,SOL_SOCKET,SO_RCVTIMEO,array("sec"=>10,"usec"=>0));
        socket_set_option($c, SOL_SOCKET, SO_LINGER, $arrOpt);

        // receive request for nonce
        if (!socket_recv($c, $nonce_request, 5, 0)) {
            throw new Exception(socket_strerror(socket_last_error($c)));
        }

        if ($nonce_request !== 'nonce') {
            throw new Exception('nonce is not requested');
        }
        $iv = generate_iv();

        // send nonce
        if (socket_send($c, $iv, strlen($iv), 0) === false) {
            throw new Exception(socket_strerror(socket_last_error($c)));
        }

        // receive request
        if (!socket_recv($c, $request, 1000, 0)) {
            throw new Exception(socket_strerror(socket_last_error($c)));
        }

        $request = decode_string($request, substr($iv, 0, 16), $auth_key);

        // process request
        $response_json = array();

        try {
            $request_json = json_decode($request, 1);

            if (!empty($request_json['action']) && !empty($request_json['account'])) {

                $account_name = $request_json['account'];

                if ($request_json['action'] !== 'balance' && empty($request_json['amount'])) {
                    throw new Exception('empty amount parameter');
                }

                $response_json['account'] = $account_name;
                switch ($request_json['action']) {
                    case 'new':
                        if (isset($bank_accounts[$account_name])) {
                            throw new Exception('account already exists');
                        }

                        $amount = floatval($request_json['amount']);
                        $bank_accounts[$account_name] = round($amount, 2);
                        $response_json['initial_balance'] = $amount;
                        break;

                    case 'withdraw':
                        if (!isset($bank_accounts[$account_name])) {
                            throw new Exception('account does not exist');
                        }
                        if ($bank_accounts[$account_name] - $request_json['amount'] < 0) {
                            throw new Exception('negative balance');
                        }
                        $amount = floatval($request_json['amount']);
                        $bank_accounts[$account_name] = round($bank_accounts[$account_name] - $request_json['amount'], 2);
                        $response_json['withdraw'] = $amount;
                        break;

                    case 'deposit':
                        if (!isset($bank_accounts[$account_name])) {
                            throw new Exception('account does not exist');
                        }
                        $amount = floatval($request_json['amount']);
                        /*if (($bank_accounts[$account_name] + $amount) > 4294967296.00) {
                            error_log('Current balance: ' . $bank_accounts[$account_name]);
                            error_log('Add amount: ' . $amount);
                            throw new Exception('maximum balance value exceed');
                        }*/

                        $bank_accounts[$account_name] = round($bank_accounts[$account_name] + $request_json['amount'], 2);
                        $response_json['deposit'] = $amount;
                        break;

                    case 'balance':
                        if (!isset($bank_accounts[$account_name])) {
                            throw new Exception('account does not exist');
                        }
                        $response_json['balance'] = $bank_accounts[$account_name];
                        break;

                    default:
                        throw new Exception('unsupported action ' . $request_json['action']);
                }

            } else {
                throw new Exception('action or account is not defined');
            }

        } catch (Exception $e) {
            error_log($e->getMessage());
            $response_json['error'] = $e->getMessage();
        }

        // send response
        if (empty($response_json)) {
            $response_json['error'] = 'empty response';
        }

        $response_str = json_encode($response_json);

        if (empty($response_json['error'])) {
            print($response_str . "\n");
        }

        $response_str = encode_string($response_str, substr($iv, 16), $auth_key);
        socket_send($c, $response_str, strlen($response_str), 0);
    } catch (Exception $e){
        error_log('bank protocol error:' . $e->getMessage());
        print "protocol_error\n";
        socket_send($c, "protocol_error", strlen("protocol_error"), 0);
    }
    socket_close($c);
}
// maybe this code will newer be executed:
socket_close($sock);
