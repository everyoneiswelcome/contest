#!/bin/bash

sourcefiles=""

for filename in ./src/*.java; do
        sourcefiles=$sourcefiles" "$filename
done

javac $sourcefiles

for filename in ./src/*.class; do
        mv $filename ./bin/
done

sed -i "s@.*theclasspath=.*@theclasspath=\"$PWD/bin/\"   #modified by makefile@" atm
sed -i "s@.*theclasspath=.*@theclasspath=\"$PWD/bin/\"   #modified by makefile@" bank
