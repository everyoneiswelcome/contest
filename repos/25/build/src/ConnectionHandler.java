

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import javax.swing.Timer;

/**
 * This is the class that gets spawned per connection, the handling of client requests are done in this class
 *
 */
public class ConnectionHandler implements Runnable {
	
	final SocketChannel socketChannel;
    final SelectionKey selectionKey;
    ByteBuffer input = ByteBuffer.allocate(1024);
    static final int READING = 0, SENDING = 1, DONE = 2;
    int state = READING;
    String requestmessage = "";
    String responsemessage = "";
    
    byte[] _requestBytes, _responseBytes;
    
    String authfile, cardfile, account, theop = "";
	double amount = 0.0;
	
	static int TIMEOUT_DURATION = 10000; // Default 10000 (10 seconds)
	Timer timeoutTimer;
	ActionListener taskPerformer;
	
	private static int threadCounter = 0;
	private int threadId;
	private static Object counterLock = new Object();
    
	/**
	 * This is the constructor for the Connection Handler
	 * @param selector the selector object associated with the bank rector
	 * @param c the socketchannel for this request
	 * @throws IOException
	 */
    public ConnectionHandler(Selector selector, SocketChannel c) throws IOException {
    	
    	synchronized(counterLock) {
    		
    		threadId = threadCounter++;
    		
    	}
    	
        socketChannel = c;
        c.configureBlocking(false);
        selectionKey = socketChannel.register(selector, 0);
        selectionKey.attach(this);
        selectionKey.interestOps(SelectionKey.OP_READ);
        selector.wakeup();
        
        taskPerformer = new ActionListener() {
        	@Override
			public void actionPerformed(ActionEvent event) {
        		
				PrintOutput("Timeout period expired. Closing connection.");
				System.out.println("protocol_error");

				try {
					
					PrintOutput("[BANK] Shutting down socket.");
				
					timeoutTimer.removeActionListener(taskPerformer);
	            	timeoutTimer.stop();
					
			        selectionKey.cancel();
			        state = DONE;
			        socketChannel.close();
			        
				} catch(Exception e){}
				
			}        	
        };
        timeoutTimer = new Timer(TIMEOUT_DURATION, taskPerformer);
        timeoutTimer.start();
        
    }

	@Override
	public void run() {
		try {
            if (state == READING) {
                read();
                doSecurityChecks();
                processTransaction();
            } else if (state == SENDING) {
            	//TODO need to figure out if this add a level of asynchronicity, as opposed to directly just sending the response after processTransaction
                send();
            } else if(state == DONE){

				PrintOutput("[BANK] State changed to done, shutting down socket.");
            	
            	timeoutTimer.removeActionListener(taskPerformer);
            	timeoutTimer.stop();
    	        selectionKey.cancel();
    	        socketChannel.close();    	        
            }
        } catch (IOException ex) {
        	System.out.println("protocol_error");
            PrintOutput("[ConnectionHandler] IO issue");
            dropConnection();
        } catch (BankProtocolException e) {
        	System.out.println("protocol_error");
        	PrintOutput("[ConnectionHandler]  bank protocol problem");
        	dropConnection("-2");
		} catch (BankUnauthorizedException e) {
			PrintOutput("[ConnectionHandler] the request is not authorized");
			dropConnection();
		} catch (BankInsuffientFundsException e) {
			PrintOutput("[ConnectionHandler] insufficient funds");
			dropConnection();
		} catch (BankAccountNotExistException e) {
			PrintOutput("[ConnectionHandler] account does not exist");
			dropConnection();
		} catch (BankAccountAlreadyExistsException e) {
			PrintOutput("[ConnectionHandler] account already exists");
			dropConnection();
		}
	}
	
	/**
	 * The function for reading from the socketchannel 
	 * @throws IOException
	 */
	private void read() throws IOException {
		PrintOutput("[ConnectionHandler] reading from the socket");
		_requestBytes = new byte[0];
        int readCount = socketChannel.read(input);
        if (readCount > 0) {
            readProcess(readCount);
        }
    }
	
	/**
	 * Reading from the socketchannel
	 * @param readCount
	 * @throws IOException 
	 */
	private synchronized void readProcess(int readCount) throws IOException {
		
		byte[] plainText;
		
        StringBuilder sb = new StringBuilder();
        input.flip();
        byte[] subStringBytes = new byte[readCount];
        byte[] array = input.array();
        System.arraycopy(array, 0, subStringBytes, 0, readCount);
    	_requestBytes = subStringBytes;
        
        try {
        	
        	Logger.LogBankMessage("Got request message: %s", ProtocolSecurity.ByteArrayToHex(_requestBytes));
            plainText = ProtocolSecurity.Decrypt(_requestBytes, ProtocolSecurity.GetAuthKey());
        	
        } catch(Exception e){
        	
        	Logger.LogBankMessageWithThreadId("Could not decrypt incoming AES stream.", threadId);
        	throw new IOException("Could not decrypt incoming AES stream.");
        	
        }
        
        /** This is assuming ASCII  **/
        sb.append(new String(plainText));
        input.clear();
        requestmessage = sb.toString().trim();
    }
	
	/**
	 * The method for undertaking some of the required security checks
	 */
	private void doSecurityChecks(){
		PrintOutput("[ConnectionHandler] doing security checks");
		if(!requestmessage.isEmpty()){//&& other security checks pass
			
		}else{
			
		}
	}
	
	/**
	 * This method processes the atm message to perform the actual transaction as requested by the atm
	 * @throws BankProtocolException
	 * @throws BankUnauthorizedException
	 * @throws BankInsuffientFundsException
	 * @throws BankAccountNotExistException
	 * @throws BankAccountAlreadyExistsException
	 */
	private void processTransaction() throws BankProtocolException, BankUnauthorizedException, BankInsuffientFundsException, BankAccountNotExistException, BankAccountAlreadyExistsException{
		PrintOutput("[ConnectionHandler] processing the transaction");
		BankDB db = BankDB.getInstance();
		
		/**
		 * Currently the message is just send as the token separated by a dash, so just tokenize and get the invididuals items, will need to be updated
		 * once the full communication protocol has been decided on
		 */
		String[] args = requestmessage.split(" - ");		
		if (args.length == 5){//expected message from atm TODO this will be handled as part of decrypting the message
			PrintOutput("[ConnectionHandler] got the right length request");
			authfile = args[0];
    		cardfile = args[1];
    		account = args[2];
    		theop = args[3];
    		amount = Double.parseDouble(args[4]);
    		
    		switch(theop){
    		case "n": 
    			responsemessage = db.createAccount(account, amount, cardfile);
    			break;
    		case "d":
    			responsemessage = db.depositAmount(account, amount, cardfile);
    			break;
    		case "g":
    			responsemessage = db.accountInfo(account, cardfile);
    			break;
    		case "w":
    			responsemessage = db.withdrawAmount(account, amount, cardfile);
    			break;
    		default:
    			throw new BankProtocolException();
    		}    		
    		
    		/**
    		 * Once we have finished reading and processing the request message, now should have handle the writing/sending
    		 */
    		state = SENDING;
            selectionKey.interestOps(SelectionKey.OP_WRITE);
		}else{
			throw new BankProtocolException();
		}		
	}
	
	/**
	 * The method for sending a response back to the client, afterward close the socket connection, if doing multiple interactions (a la handshake of some sort)
	 * then we will need to keep the socket connection open until after the last interaction
	 * @throws IOException
	 */
	private void send() throws IOException {
		
		byte[] cipherText;
		
		try{
	        
			cipherText = ProtocolSecurity.Encrypt(responsemessage.getBytes(), ProtocolSecurity.GetAuthKey());
        
        } catch(Exception e) {
        	
        	Logger.LogAtmMessage("Could not encrypt outgoing AES stream.");
        	throw new IOException("Could not encrypt outgoing AES stream.");
        	
        }
		
		System.out.println(responsemessage);		
        ByteBuffer output = ByteBuffer.wrap(cipherText);
        socketChannel.write(output);
        selectionKey.cancel();
        state = DONE;
        socketChannel.close();
        PrintOutput("[ConnectionHandler] finished sending the response message");
        ShutdownTimer();
    }
	
	/**
	 * This method is specifically when there's been an exception and we need to explicitly drop a socket connection, 
	 * without waiting to go through the SENDING state cycle. By default it will return -1 so that the atm can just exit with 255
	 */
	private void dropConnection(){
		dropConnection("-1");
	}
	
	/**
	 * This method is specifically when there's been an exception and we need to explicitly drop a socket connection, 
	 * without waiting to go through the SENDING state cycle.
	 * @param message this is the internal error code that we send back "-1" for exit(255) and "-2" for exit(63)
	 */
	private void dropConnection(String message){//an exception occured, kill the connection, sort of        
        try {
			
			byte[] cipherText;
			cipherText = ProtocolSecurity.Encrypt(message.getBytes(), ProtocolSecurity.GetAuthKey());			
        	ByteBuffer output = ByteBuffer.wrap(cipherText);
			
			socketChannel.write(output);
	        selectionKey.cancel();
	        state = DONE;
	        socketChannel.close();
	        ShutdownTimer();
		} catch (Exception e) {
			System.out.println("protocol_error");
			PrintOutput("[ConnectionHandler] had issues dropping the connection");
		}        
    }
	
	private void ShutdownTimer(){
		
		PrintOutput("Shutting down timer.");
		timeoutTimer.removeActionListener(taskPerformer);
		timeoutTimer.stop();
		
	}
	
	private void PrintOutput(String format, Object... args){
		
		String inputString = String.format(format, args);
		Logger.LogBankMessageWithThreadId("%s", threadId, inputString);
		
	}	
}
