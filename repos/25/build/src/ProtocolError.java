public enum ProtocolError {

	NoError       ((byte) 0x00),
	GenericError  ((byte) 0x3F), //63
	UnknownError  ((byte) 0xFF);
	
	private byte errorId;
	
	ProtocolError (byte errorId) {
		
        this.errorId = errorId;
    
	}
	
	public byte ErrorIdAsByte() {
		
		return this.errorId;
		
	}
	
	public static ProtocolError ProtocolErrorFromValue(byte value){
		
		try{
            return ProtocolError.values()[value];
       }catch( ArrayIndexOutOfBoundsException e ) {
            return ProtocolError.UnknownError;
       }
		
	}
	
}