
public enum ProtocolSequence {
	
	Uninitialised  ((byte) 0x00),
	Hello          ((byte) 0x01),
	SrvHello       ((byte) 0x02),
	Command        ((byte) 0x03),
	Response       ((byte) 0x04),
	Complete       ((byte) 0x05),
	NotImplemented ((byte) 0xFE),
	Error          ((byte) 0xFF);
	
	private byte sequenceId;
	
	ProtocolSequence(byte sequenceId) {
		
        this.sequenceId = sequenceId;
    
	}
	
	public byte SequenceIdAsByte(){
		
		return this.sequenceId;
		
	}
	
	public ProtocolSequence NextSequenceId(){
		
		int currentSequenceId = (int) this.sequenceId;
		int nextSequenceId = 0;
		
		if(currentSequenceId >= 0 && currentSequenceId < 5)
			nextSequenceId = currentSequenceId + 1;
		
		return ProtocolSequence.ProtocolSequenceFromValue((byte) nextSequenceId);
		
	}
	
	public static ProtocolSequence ProtocolSequenceFromValue(byte value){
	
		try{
            return ProtocolSequence.values()[value];
       }catch( ArrayIndexOutOfBoundsException e ) {
            return ProtocolSequence.NotImplemented;
       }
		
	}
	
}
