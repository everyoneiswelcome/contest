public class ProtocolException extends Exception {

	public ProtocolException() {
		super();
	}

	public ProtocolException(String message) {
		super(message);
	}	
	
	private static final long serialVersionUID = 1L;

}