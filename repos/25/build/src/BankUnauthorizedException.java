

/**
 * This exception gets thrown when there is an authorized connection made to the bank
 *
 */
public class BankUnauthorizedException extends Exception {

	public BankUnauthorizedException() {
		super();
	}

	public BankUnauthorizedException(String message) {
		super(message);
	}	
}
