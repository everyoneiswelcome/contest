import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class ProtocolMessage {

	public ProtocolSequence SequenceId = ProtocolSequence.Hello;
	public ProtocolCommand CommandId = ProtocolCommand.Initialise;
	public ProtocolError ErrorId = ProtocolError.NoError;	
	
	private byte[] _clientNonce = new byte[16];
	private byte[] _serverNonce = new byte[16];
	private byte[] _accountName = new byte[250];
	private byte[] _authToken = new byte[16];
	private byte[] _payload = ProtocolPayload.HELLO_PAYLOAD;
	
	private byte[] _message;
	private byte[] _messageHash;
	
	private long _wholeAmount = 0;
	private byte _fractionAmount = 0;
	
	public ProtocolMessage(){
		
		Logger.LogMessage("Initialising message object.");
		_clientNonce = ProtocolSecurity.GenerateNonce();		
		
	}
	
	public void SetClientNonce(byte[] clientNonce) { _clientNonce = clientNonce; }
	public void SetServerNonce(byte[] serverNonce) { _serverNonce = serverNonce; }
	
	public void SetAccountName(String accountName) { _accountName = accountName.getBytes(StandardCharsets.US_ASCII); }
	public void SetAccountName(byte[] accountName) { _accountName = accountName; }
	public void SetAuthToken(byte[] authToken) { _authToken = authToken; }
	
	public void SetAmount(long wholeAmount, byte fractionAmount){ _wholeAmount = wholeAmount; _fractionAmount = fractionAmount; }
	
	public void SetAmount(BigDecimal amount){
		
		if(amount.scale() > 2 || amount.compareTo(ProtocolPayload.SMALLEST_AMOUNT) < 0 || amount.compareTo(ProtocolPayload.LARGEST_AMOUNT) > 0)
			throw new IllegalArgumentException("Payload be between SMALLEST_AMOUNT and LARGEST_AMOUNT, with scale 2.");	 
		
		long wholeAmount = amount.setScale(0, BigDecimal.ROUND_DOWN).longValueExact();
		byte fractionAmount = amount.abs().remainder(BigDecimal.ONE).unscaledValue().byteValue();
		
		_wholeAmount = wholeAmount;
		_fractionAmount = fractionAmount;
		
	}
	
	public byte[] GetPackedMessage() throws ProtocolMessageException{
		
		ByteBuffer buffer = ByteBuffer.allocate(326);
		
		try {
		
			buffer.put(ProtocolSecurity.GenerateNonce());
			buffer.put(SequenceId.SequenceIdAsByte());
			buffer.put(_clientNonce);
			buffer.put(_serverNonce);
			buffer.put(CommandId.CommandIdAsByte());
			buffer.put(_accountName);
			buffer.put(_authToken);
			buffer.put(ErrorId.ErrorIdAsByte());
			buffer.put(_payload);
			
		} catch (Exception e){
			
			Logger.LogMessage("Could not pack message.");
			System.err.flush();
			
			return new byte[1];
			
		}
		
		_message = buffer.array();
		
		if(_message.length != 326){
			
			Logger.LogMessage(String.format("Expected message length of 326 bytes, got %d.", _message.length));
			throw new ProtocolMessageException("Unexpected message size.");
			
		}
		
		_messageHash = ProtocolSecurity.GenerateCrc32(_message);
		
		buffer = ByteBuffer.allocate(334);
		
		buffer.put(_message);
		buffer.put(_messageHash);
		
		byte[] packedMessage = buffer.array();
		
		return packedMessage;
		
	}
	
	public void LoadProtocolMessageFromBytes(byte[] packedMessage) throws ProtocolMessageException {
		
		if(packedMessage.length != 334){
			
			Logger.LogMessage(String.format("Expected packed message of length of 334 bytes, got %d.", packedMessage.length));
			throw new ProtocolMessageException("Unexpected packed message size.");
		}
		
		_message = Arrays.copyOfRange(packedMessage, 0, 326);
		_messageHash = ProtocolSecurity.GenerateCrc32(_message);
		
		byte[] checksum = Arrays.copyOfRange(packedMessage, 326, 334);				
		
		if(!Arrays.equals(checksum, _messageHash)){
			
			Logger.LogMessage(String.format("Message hash %s did not match supplied checksum %s.",
						ProtocolSecurity.ByteArrayToHex(_messageHash),
						ProtocolSecurity.ByteArrayToHex(checksum)));
			
			throw new ProtocolMessageException("Message hash didn't match supplied checksum; unpacking failed.");
			
		}
		
		//byte[] messageNonce = Arrays.copyOfRange(packedMessage, 0, 16);
		
		SequenceId = ProtocolSequence.ProtocolSequenceFromValue(Arrays.copyOfRange(packedMessage, 16, 17)[0]);
		CommandId = ProtocolCommand.ProtocolCommandFromValue(Arrays.copyOfRange(packedMessage, 49, 50)[0]);
		ErrorId = ProtocolError.ProtocolErrorFromValue(Arrays.copyOfRange(packedMessage, 316, 317)[0]);
		
		_clientNonce = Arrays.copyOfRange(packedMessage, 17, 33);
		_serverNonce = Arrays.copyOfRange(packedMessage, 33, 49);
		_accountName = Arrays.copyOfRange(packedMessage, 50, 300);
		_authToken = Arrays.copyOfRange(packedMessage, 300, 316);
		_payload = Arrays.copyOfRange(packedMessage, 317, 326);
		
		_wholeAmount = ProtocolPayload.GetWholeAmountFromPayload(_payload);
		_fractionAmount = ProtocolPayload.GetFractionAmountFromPayload(_payload);
		
	}
	
	public byte[] GetMessageBytes(){ return _message; }	
	public byte[] GetMessageHash(){ return _messageHash; }
	
	public byte[] GetClientNonce() { return _clientNonce; }
	public byte[] GetServerNonce() { return _serverNonce; }
	
	public byte[] GetAccountName() { return _accountName; }
	public String GetAccountNameString(){ return new String(_accountName, StandardCharsets.US_ASCII); }
	public byte[] GetAuthToken() { return _authToken; }
	
	public long GetAmountWholePart() { return _wholeAmount; }
	public byte GetAmountFractionPart() { return _fractionAmount; }
	public BigDecimal GetDecimalAmount(){ return ProtocolPayload.GetAmountFromComponents(_wholeAmount, _fractionAmount); }	
	public double GetDoubleAmount(){ return ProtocolPayload.GetAmountFromComponents(_wholeAmount, _fractionAmount).doubleValue(); }
}