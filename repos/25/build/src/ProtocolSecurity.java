import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class ProtocolSecurity {
	
	private static byte[] _authKey = new byte[] { 0x00 };
	
	private static byte[] NULL_IV = new byte[] {
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00,
		0x00, 0x00, 0x00, 0x00,
	};

	public static byte[] GenerateNonce(){
		
		byte[] nonce = new byte[16];
		
		SecureRandom sr = new SecureRandom();
		sr.nextBytes(nonce);
		
		return nonce;
		
	}
	
	public static byte[] Encrypt(byte[] plainText, byte[] keyBytes){
		
		
		byte[] cipherText = new byte[1];
		
		try {
			
			SecretKey secretKey = new SecretKeySpec(keyBytes, 0, keyBytes.length, "AES");
			IvParameterSpec initialisationVector = new IvParameterSpec(NULL_IV);
			
			Cipher encryptionCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			encryptionCipher.init(Cipher.ENCRYPT_MODE, secretKey, initialisationVector);
			cipherText = encryptionCipher.doFinal(plainText);
			
		} catch (Exception e){
			
			
			
		}
		
		return cipherText;		
		
	}
	
	public static byte[] Decrypt(byte[] cipherText, byte[] key){
		
		
		byte[] plainText = new byte[1];
		
		try {
			
			SecretKey secretKey = new SecretKeySpec(key, 0, key.length, "AES");
			IvParameterSpec initialisationVector = new IvParameterSpec(NULL_IV);
			
			Cipher encryptionCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			encryptionCipher.init(Cipher.DECRYPT_MODE, secretKey, initialisationVector);
			plainText = encryptionCipher.doFinal(cipherText);
			
		} catch (Exception e){
			
			e.printStackTrace();
			
		}
		
		return plainText;		
		
	}
	
	public static byte[] GenerateAesKey() throws NoSuchAlgorithmException{
		
        KeyGenerator keyGenerator;

        keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(128);        
        
        SecretKey secretKey = keyGenerator.generateKey();
        
        return secretKey.getEncoded();
		
	}
	
	public static String ByteArrayToHex(byte[] byteArray) {
		
	   StringBuilder hexString = new StringBuilder(byteArray.length * 2);
	   
	   for(byte b : byteArray)
		   hexString.append(String.format("%02X", b));
	   
	   return hexString.toString();
	   
	}
	
	public static byte[] HexToByteArray(String hexString){
		
		if((hexString.length() % 2) > 0)
			throw new IllegalArgumentException("Hex string must have an even number of characters.");
		
		byte[] byteArray = new byte[hexString.length() / 2];
		
	    for (int i = 0; i < byteArray.length * 2; i += 2) {
	    	
	    	byteArray[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4)
	    								+ Character.digit(hexString.charAt(i+1), 16));
	        
	    }
		
	    return byteArray;
		
	}
	
	public static byte[] GenerateCrc32(byte[] byteArray){
		
		Checksum checksum = new CRC32();
		
		checksum.reset();
		checksum.update(byteArray, 0, byteArray.length);
		
		long hashValue = checksum.getValue();
		
		ByteBuffer buffer = ByteBuffer.allocate((Long.SIZE / Byte.SIZE));	    
		buffer.putLong(hashValue);
	    
	    byte[] hashBytes = buffer.array();	    
		return hashBytes;
		
	}
	
	public static byte[] GetAuthKey() throws NoSuchAlgorithmException{
		
		if(Arrays.equals(_authKey, new byte[] { 0x00 })){
			
			Logger.LogMessage("Generating new AES key.");
			_authKey = GenerateAesKey();
			
		}
		
		return _authKey;
		
	}
	
	public static void SetAuthKey(byte[] authKey){
		
		Logger.LogMessage("Loading AES key.");
		_authKey = authKey;
		
	}
	
}