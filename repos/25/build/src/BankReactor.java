

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

/**
 * This implements the Reactor pattern to provide a non-blocking thread per client implementation, if performance suffers, could explore a thread polling strategy
 * The implementation is inspired by the following blog entry on the Reactor pattern http://jeewanthad.blogspot.com.au/2013/02/reactor-pattern-explained-part-1.html
 *
 */
public class BankReactor implements Runnable {
	
	final Selector selector;
    final ServerSocketChannel serverSocketChannel;
 
    /**
     * The default constructor for the bank, this also prints the required "created" message
     * @param port
     * @throws IOException
     */
    BankReactor(int port) throws IOException {
 
        selector = Selector.open();
        serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.socket().bind(new InetSocketAddress(port));
        serverSocketChannel.configureBlocking(false);
        SelectionKey selectionKey0 = serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        selectionKey0.attach(new Acceptor());
        
        System.out.println("created");
    }

	@Override
	public void run() {
        try {
            while (!Thread.interrupted()) {
                selector.select();
                Set selected = selector.selectedKeys();
                Iterator it = selected.iterator();
                while (it.hasNext()) {
                    dispatch((SelectionKey) (it.next()));
                }
                selected.clear();
            }
        } catch (IOException ex) {
        	/**
        	 * protocol error from any IOExceptions
        	 */
        	System.out.println("protocol_error");
        }
	}
	
	void dispatch(SelectionKey k) {
        Runnable r = (Runnable) (k.attachment());
        if (r != null) {
            r.run();
        } 
    }
	
	/**
	 * This handles incoming connections
	 */
	class Acceptor implements Runnable {
        public void run() {
            try {
                SocketChannel socketChannel = serverSocketChannel.accept();
                if (socketChannel != null) {
                        new ConnectionHandler(selector, socketChannel);
                }
            } catch (IOException ex) {
            	/**
            	 * protocol error if there is an exception in here
            	 */
            	System.out.println("protocol_error");
            }
        }
    }

}
