
public class Logger {
	
	private static final boolean LOGGING_ENABLED = false; 
	
	static void LogMessage(String format, Object... args){
		
		try {
		
			if(LOGGING_ENABLED)
				System.err.println(String.format(format, args));
			
		} catch(Exception e){
			
			if(LOGGING_ENABLED)
				throw e;
			
		}
		
	}
	
	static void LogAtmMessage(String format, Object... args){
		
		try {
			
			LogMessage(String.format("[ATM] %s", String.format(format, args)));
			
		} catch(Exception e) {
		
			LogMessage("Logger threw and caught an exception.");
			
		}
		
	}
	
	static void LogBankMessage(String format, Object... args){
		
		try {
			
			LogMessage(String.format("[BANK] %s", String.format(format, args)));
			
		} catch(Exception e) {
			
			LogMessage("Logger threw and caught an exception.");
			
		}
		
	}
	
	static void LogBankMessageWithThreadId(String format, int threadId, Object... args){
		
		try {
		
			LogMessage(String.format("[BANK] (Thread #%d) %s", threadId, String.format(format, args)));
			
		} catch(Exception e) {
		
			LogMessage("Logger threw and caught an exception.");
			
		}
		
	}

}