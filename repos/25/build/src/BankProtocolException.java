

/**
 * This exception get thrown when there is a procotol issue in handling the requests
 *
 */
public class BankProtocolException extends Exception {

	public BankProtocolException() {
		super();
	}

	public BankProtocolException(String message) {
		super(message);
	}

}
