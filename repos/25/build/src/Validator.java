import java.util.regex.Pattern;

public class Validator {
	
	public static boolean FilenameIsValid(String filename){
		
		if(filename == null || filename.isEmpty()){
			
			Logger.LogMessage("Filename is invalid: null or empty.");
			return false;
			
		}
		
		if(filename.length() > 255){
			
			Logger.LogMessage("Filename %s is too long.", filename);
			return false;
			
		}
		
		if(!Pattern.matches("^[_\\-\\.0-9A-Za-z]{1,255}$", filename)){
			
			Logger.LogMessage("Filename %s is invalid.", filename);
			return false;
						
		}
		
		return true;
		
	}

}
