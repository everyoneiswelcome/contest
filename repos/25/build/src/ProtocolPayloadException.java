public class ProtocolPayloadException extends Exception {

	public ProtocolPayloadException() {
		super();
	}

	public ProtocolPayloadException(String message) {
		super(message);
	}	
	
	private static final long serialVersionUID = 1L;

}