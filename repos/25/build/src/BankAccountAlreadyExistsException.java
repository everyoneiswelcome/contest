

/**
 *	This is the exception that gets thrown when the bank account already exists i.e. when a request to create a new account is made
 */
public class BankAccountAlreadyExistsException extends Exception {

	public BankAccountAlreadyExistsException() {
		super();
	}

	public BankAccountAlreadyExistsException(String message) {
		super(message);
	}
	
}
