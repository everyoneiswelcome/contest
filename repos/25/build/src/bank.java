import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class bank {
	
	public static void main(String[] args) throws IOException{
		
		/**
		 * At the moment just grabbing the port and listening on the localhost. args[1] is the bank.auth file and we should grab that to create the bank.auth file
		 * so args[0] - port
		 * args[1] - auth filename, we should probably pass the present working directory as well
		 */
		createAuthFile(args[1]);
		
	    BankReactor reactor  = new BankReactor(Integer.parseInt(args[0]));
	    new Thread(reactor).start();		
	}
	
	/**
	 * Create the bank.auth file with all the relevant keys
	 * @param filename the filename of bank.auth file, not the full path
	 */
	private static void createAuthFile(String filename){
		
		if(filename == null || filename.isEmpty())
			filename = "bank.auth";
		
		if(!Validator.FilenameIsValid(filename)){
			
			Logger.LogBankMessage("Filename for auth-file is invalid.");
			System.exit(255);
			
		}
		
		File authFile = new File(filename);
		
		if(authFile.exists()){
			
			Logger.LogBankMessage("Auth file exists.");
			System.exit(255);
			
		}
		
		try {
		
			byte[] authKey = ProtocolSecurity.GetAuthKey();
			
			FileWriter authFileWriter = new FileWriter(filename);
			authFileWriter.write(ProtocolSecurity.ByteArrayToHex(authKey));
			authFileWriter.flush();
			authFileWriter.close();
			
		} catch(Exception e){
			
			Logger.LogBankMessage("Error creating auth file: %s", e.getMessage());
			System.exit(255);
			
		}
		
	}
}
