

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.nio.file.Files;

public class atm {

	String hostIp;
    int hostPort;
	static String _theop;
	static String _cardfilePath;
 
    /**
     * A constructor for the atm application
     * @param hostIp the ip address
     * @param hostPort the port
     */
    public atm(String hostIp, int hostPort) {
        this.hostIp = hostIp;
        this.hostPort = hostPort;
    }
 
    /**
     * The method that runs core functionality of the atm
     * @param message this is the message to the bank, formatted according to the specified communication protocol.
     * @throws IOException
     */
    public void runClient(String message) throws IOException {
        Socket clientSocket = null;
        OutputStream out = null;
        InputStream in = null;
 
        try {
            clientSocket = new Socket(hostIp, hostPort);
            clientSocket.setSoTimeout(10000);
            out = clientSocket.getOutputStream();
            in = clientSocket.getInputStream();
        } catch (UnknownHostException e) {
            Logger.LogAtmMessage("Unknown host: " + hostIp);
            System.exit(255);
        } catch (IOException e) {
            Logger.LogAtmMessage("Couldn't connect to: " + hostIp);
            System.exit(255);
        }
            
        /**
         * Send the message out to the bank
         */
        
        byte[] request;
        
        try{
        
        	request = ProtocolSecurity.Encrypt(message.getBytes(), ProtocolSecurity.GetAuthKey());
        
        } catch(Exception e) {
        	
        	Logger.LogAtmMessage("Could not encrypt outgoing AES stream.");
        	throw new IOException("Could not encrypt outgoing AES stream.");
        	
        }        
        
        Logger.LogBankMessage("Sending message: %s", ProtocolSecurity.ByteArrayToHex(request));
        out.write(request);
        out.flush();
        
        Logger.LogAtmMessage("[ATM] Just send the message to the bank");
        
        ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
        
		if(in == null){
        	Logger.LogAtmMessage("Input stream is null.");
        	System.exit(63);
        }
        
        int b;
        while((b = in.read()) != -1)
        	responseStream.write((byte)b);
        
        byte[] cipherText = responseStream.toByteArray();
        byte[] plainText;
        
        try {
        	
        	Logger.LogBankMessage("Got request message: %s", ProtocolSecurity.ByteArrayToHex(cipherText));
            plainText = ProtocolSecurity.Decrypt(cipherText, ProtocolSecurity.GetAuthKey());
        	
        } catch(Exception e){
        	
        	Logger.LogAtmMessage("Could not decrypt incoming AES stream.");
        	throw new IOException("Could not decrypt incoming AES stream.");
        	
        }
        
        String response = new String(plainText).trim();
        
        Logger.LogAtmMessage("[ATM] The response from the bank : " + response);
        
        out.close();
        in.close();
        clientSocket.close();
		
		
        
        /**
         * The bank send "-2" for a BankProtocolException and "-1" for other Exceptions
         */
        if(response == null || response.isEmpty()){
			
			Logger.LogAtmMessage("[ATM] Empty or null response from bank. Terminating.");
			
			if(_theop.equals("n")){
				
				Logger.LogAtmMessage(String.format("[ATM] Removing card file: %s", _cardfilePath));
				
				try {
					
					File cardFile = new File(_cardfilePath);
					cardFile.delete();
					
				} catch(Exception ce) {
					
					Logger.LogAtmMessage("[ATM] Could not remove card file.");
					
				}
				
			}
			
			Logger.LogAtmMessage("[ATM] Empty or null response from bank. Terminating.");
			System.exit(63);
			
		}
		
		if(response.equals("-1")){
        	System.exit(255);        	
        }else if (response.equals("-2")){          
        	System.exit(63);
        }else{
        	System.out.println(response);
        }   
    }
    
    public static void main(String[] args) {
    	atm client; 
    	String authfile, hostip, cardfilePath, account, theop, thepwd = ""; /* theop is the operation that should be performed (g,d,w,n) and thepwd is the current working directory */
    	int port = 0;
    	double amount = 0.0;
    	
    	if(args.length != 8){
    		
    		Logger.LogAtmMessage(String.format("[ATM] Not enough arguments recieved from the bash script. Got: %d", args.length));
    		
    		for(String arg : args)
    			Logger.LogAtmMessage(String.format("Arg: %s", arg));
    		
    		System.exit(255);
    		return;
    		
    	}else{
    		
        	/**
        	 * This is expecting 8 arguments from the bash script, this is where the parser should kick in and the arguments assigned to the above variables
        	 */
    		
    		authfile = args[0];
    		hostip = args[1];
    		port = Integer.parseInt(args[2]);
    		cardfilePath = args[3];
    		account = args[4];
    		theop = args[5];
    		amount = Double.parseDouble(args[6]);
    		thepwd = args[7];
			
			_theop = theop;
			_cardfilePath = cardfilePath;
    		
    		Logger.LogAtmMessage("[ATM] Arguments received on the Java atm : " + authfile  + " - " + cardfilePath + " - " + account + " - " + theop + " - " + amount);
    		
    		readAuthFile(authfile);
    		
    	}
    	
        try {
        	      	
        		client = new atm(hostip, port);
        		client.runClient(authfile  + " - " + cardfilePath + " - " + account + " - " + theop + " - " + amount);

		} catch (SocketTimeoutException e) {
			
			Logger.LogAtmMessage("[ATM] a timeout happened");
			
			if(theop.equals("n")){
				
				Logger.LogAtmMessage(String.format("[ATM] Removing card file: %s", cardfilePath));
				
				try {
					
					File cardFile = new File(cardfilePath);
					cardFile.delete();
					
				} catch(Exception ce) {
					
					Logger.LogAtmMessage("[ATM] Could not remove card file.");
					
				}
				
			}
			
			System.exit(63);
			
		} catch (IOException e) {
			Logger.LogAtmMessage("issues in the atm");
			System.exit(255);
		}
	}
    
    private static void readAuthFile(String filename){
		
		if(filename == null || filename.isEmpty())
			filename = "bank.auth";
		
		if(!Validator.FilenameIsValid(filename)){
			
			Logger.LogAtmMessage("Filename for auth-file is invalid.");
			System.exit(255);
			
		}
		
		File authFile = new File(filename);
		
		if(!authFile.exists()){
			
			Logger.LogAtmMessage("Auth file does not exist.");
			System.exit(255);
			
		}
		
		try {
			
			String authKey = new String(Files.readAllBytes(authFile.toPath()));
			Logger.LogAtmMessage("Got auth key: %s", authKey.trim());
			
			ProtocolSecurity.SetAuthKey(ProtocolSecurity.HexToByteArray(authKey));
			
		} catch(Exception e){
			
			Logger.LogAtmMessage("Error reading auth file: %s", e.getMessage());
			System.exit(255);
			
		}
		
	}
}
