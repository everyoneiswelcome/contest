
public enum ProtocolCommand {

	Initialise     ((byte) 0x01),
	Create         ((byte) 0x02),
	Deposit        ((byte) 0x03),
	Withdraw       ((byte) 0x04),
	Balance        ((byte) 0x05),
	Close          ((byte) 0x06),
	NotImplemented ((byte) 0xFE),
	Error          ((byte) 0xFF);
	
	private byte commandId;
	
	ProtocolCommand (byte commandId) {
		
        this.commandId = commandId;
    
	}
	
	public byte CommandIdAsByte() {
		
		return this.commandId;
		
	}
	
	public static ProtocolCommand ProtocolCommandFromValue(byte value){
		
		try{
            return ProtocolCommand.values()[value];
       }catch( ArrayIndexOutOfBoundsException e ) {
            return ProtocolCommand.NotImplemented;
       }
		
	}
	
}
