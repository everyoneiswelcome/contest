public class ProtocolMessageException extends Exception {

	public ProtocolMessageException() {
		super();
	}

	public ProtocolMessageException(String message) {
		super(message);
	}	
	
	private static final long serialVersionUID = 1L;

}