

/**
 * This exception gets thrown when there aren't insufficient funds in the bank account to honour the requested operation
 *
 */
public class BankInsuffientFundsException extends Exception {

	public BankInsuffientFundsException() {
		super();
	}

	public BankInsuffientFundsException(String message) {
		super(message);
	}

}
