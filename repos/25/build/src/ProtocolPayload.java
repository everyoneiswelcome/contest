import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class ProtocolPayload {

	public static BigDecimal SMALLEST_AMOUNT = new BigDecimal("-9223372036854775808.99");
	public static BigDecimal LARGEST_AMOUNT = new BigDecimal("9223372036854775807.99");
	
	public static byte[] HELLO_PAYLOAD = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x68, 0x65, 0x6C, 0x6C, 0x6F };
	public static byte[] SRVHELLO_PAYLOAD = new byte[] { 0x73, 0x72, 0x76, 0x5F, 0x68, 0x65, 0x6C, 0x6C, 0x6F };
	public static byte[] BYE_PAYLOAD = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x62, 0x79, 0x65 };
	public static byte[] SRVBYE_PAYLOAD = new byte[] { 0x00, 0x00, 0x73, 0x72, 0x76, 0x5F, 0x62, 0x79, 0x65 };
	public static byte[] ERROR_PAYLOAD = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x65, 0x72, 0x72, 0x6F, 0x72 };
	
	public static BigDecimal GetAmountFromComponents(long wholeAmount, byte fractionAmount){
		
		String amount = String.format("%d.%d", wholeAmount, fractionAmount);
		
		return new BigDecimal(amount);	
		
	}
	
	public static BigDecimal GetAmountFromPayload(byte[] payload){
		
		if(payload.length != 9)
			throw new IllegalArgumentException("Payload must be 9 bytes.");
		
		long wholeAmount = GetWholeAmountFromPayload(payload);
		byte fractionAmount = GetFractionAmountFromPayload(payload);
		
		return GetAmountFromComponents(wholeAmount, fractionAmount);
		
	}
	
	public static long GetWholeAmountFromPayload(byte[] payload){
		
		if(payload.length != 9)
			throw new IllegalArgumentException("Payload must be 9 bytes.");	    
	    
		byte[] wholeAmountBytes = Arrays.copyOfRange(payload, 0, 8);
		
		ByteBuffer buffer = ByteBuffer.allocate((Long.SIZE / Byte.SIZE));
	    buffer.put(wholeAmountBytes);
	    
	    long wholeValue = buffer.getLong(0);
	    
	    return wholeValue;	    
		
	}
	
	public static byte GetFractionAmountFromPayload(byte[] payload){
		
		if(payload.length != 9)
			throw new IllegalArgumentException("Payload must be 9 bytes.");	    
	    
		return Arrays.copyOfRange(payload, 8, 9)[0];
		
	}
	
	public static byte[] GetPayloadFromAmount(BigDecimal amount) throws ProtocolPayloadException{
		
		if(amount.scale() > 2 || amount.compareTo(SMALLEST_AMOUNT) < 0 || amount.compareTo(LARGEST_AMOUNT) > 0)
			throw new IllegalArgumentException("Payload be between SMALLEST_AMOUNT and LARGEST_AMOUNT, with scale 2.");	 
		
		long wholeAmount = amount.setScale(0, BigDecimal.ROUND_DOWN).longValueExact();
		byte fractionAmount = amount.abs().remainder(BigDecimal.ONE).unscaledValue().byteValue();

		return GetPayloadFromAmount(wholeAmount, fractionAmount);
		
	}
	
	public static byte[] GetPayloadFromAmount(long wholeAmount, byte fractionAmount) throws ProtocolPayloadException{
			
		ByteBuffer buffer = ByteBuffer.allocate((Long.SIZE / Byte.SIZE) + 1);
	    
		buffer.putLong(wholeAmount);
		buffer.put(fractionAmount);
	    
	    byte[] payload = buffer.array();
	    
	    if(payload.length != 9){	    

	    	Logger.LogMessage(String.format("Expected payload length of 9 bytes, got %d", payload.length));
	    	throw new ProtocolPayloadException("Error converting amount to payload.");	    	
	    
	    }
		
		return payload;
		
	}
	
}
