

/**
 *	This is the exception that gets thrown when the bank account does not exists for the deposit, withdraw and account info requests
 */
public class BankAccountNotExistException extends Exception {

	public BankAccountNotExistException() {
		super();
	}

	public BankAccountNotExistException(String message) {
		super(message);
	}

}
