

import java.util.ArrayList;

/**
 * This is the main bank backend that stores all the account information. Its implemented as a singleton and for thread safety the specific bank account variables are set
 * to be volatile. Each account is stored as an ArrayList of BankAccount objects i.e. ArrayList<BankAccount> which is defined as a private inner class
 */

public class BankDB {
	private ArrayList<BankAccount> accounts;
	
	private BankDB(){
		accounts = new ArrayList<BankAccount>();
	}
	
	/**
	 *  This is a lazy loading and thread safe singleton implementation
	 */	
	private static class BankDBHolder {
		public static final BankDB instance = new BankDB();
	}
	
	public static BankDB getInstance(){
		return BankDBHolder.instance;		
	}
	
	/**
	 * This method creates a new account with the provided details
	 * @param account is the account name 
	 * @param amount is the default amount associated with the account
	 * @param key is some string security token that is associated with each bank account
	 * @return the JSON message in the specified format
	 * @throws BankUnauthorizedException
	 * @throws BankAccountAlreadyExistsException
	 */
	public synchronized String createAccount(String account, double amount, String key) throws BankUnauthorizedException, BankAccountAlreadyExistsException{
		Logger.LogBankMessage("[BankDB] Creating a new account");
		BankAccount tmp = new BankAccount(account, amount, key);
		if(accounts.contains(tmp)){
			throw new BankAccountAlreadyExistsException();
		}
		accounts.add(tmp);
		return tmp.getInfo("initial_balance", key);
	}
	
	/**
	 * This method deposits the specified amount into the specific account
	 * @param account is the account name
	 * @param amount is the amount to be deposited
	 * @param key is some string security token that is associated with each bank account
	 * @return the JSON message in the specified format
	 * @throws BankUnauthorizedException
	 * @throws BankAccountNotExistException
	 */
	public synchronized String depositAmount(String account, double amount, String key) throws BankUnauthorizedException, BankAccountNotExistException{
		Logger.LogBankMessage("[BankDB] depositing amount " + account + " R" + amount + "size " + accounts.size());
		int index = accounts.indexOf(new BankAccount(account, 0.0, ""));
		if(index != -1){
			return (accounts.get(index)).deposit(amount, key) ;
		}else{
			Logger.LogBankMessage("[BankDB] account not there with index " + index);
			for(BankAccount a : accounts){
				//System.err.println(a);
			}
			//System.err.println(new BankAccount(account, 0.0, ""));
			throw new BankAccountNotExistException();
		}		
	}
	
	/**
	 * This method withdraws the specified amount from the specific account
	 * @param account is the account name
	 * @param amount is the amount to be withdrawn
	 * @param key is some string security token that is associated with each bank account
	 * @return the JSON message in the specified format
	 * @throws BankUnauthorizedException
	 * @throws BankInsuffientFundsException
	 * @throws BankAccountNotExistException
	 */
	public synchronized String withdrawAmount(String account, double amount, String key) throws BankUnauthorizedException, BankInsuffientFundsException, BankAccountNotExistException{
		Logger.LogBankMessage("[BankDB] withdrawing dosh");
		int index = accounts.indexOf(new BankAccount(account, 0.0, ""));
		if(index != -1){
			return (accounts.get(index)).withdraw(amount, key);
		}else{
			throw new BankAccountNotExistException();
		}
	}
	
	/**
	 * This method gets the account information
	 * @param account the name of the account
	 * @param key is some string security token that is associated with each bank account
	 * @return the JSON message in the specified format
	 * @throws BankUnauthorizedException
	 * @throws BankAccountNotExistException
	 */
	public synchronized String accountInfo(String account, String key) throws BankUnauthorizedException, BankAccountNotExistException{
		Logger.LogBankMessage("[BankDB] getting account info");
		int index = accounts.indexOf(new BankAccount(account, 0.0, ""));
		if(index != -1){
			return (accounts.get(index)).getInfo("balance", key);
		}else{
			throw new BankAccountNotExistException();
		}
	}
	
	/**
	 * This is the actual bank account object
	 *
	 */
	private class BankAccount{
		
		/**
		 * The actual variables have been made volatile to provide some thread safety. Don't know if this is the best strategy, versus making
		 * the accessor methods synchronized. 
		 */
		private volatile double amount = 0.0;
		private volatile String accountname = ""; 
		private volatile String validator = ""; /** At the moment the key is just the name of the cardfile, it will have to be a key or some sort **/
		
		/**
		 * The bank account constructor
		 * @param accountname the name of the account
		 * @param amount the initial balance in the account
		 * @param validator is some string security token that is associated with each bank account
		 */
		public BankAccount(String accountname, double amount, String validator) {
			super();
			this.amount = amount;
			this.amount = roundUp(this.amount);
			this.accountname = accountname;
			this.validator = validator;
			Logger.LogBankMessage("creating the money: " + amount);
		}
		
		/**
		 * Deposit money into an account, confirm that that request is valid
		 * @param howmuch the amount to be deposited
		 * @param key some key to confirm access to this particular account
		 * @return the JSON formatted indication of the operation taken
		 * @throws BankUnauthorizedException
		 */
		public String deposit(double howmuch, String key) throws BankUnauthorizedException{
			Logger.LogBankMessage("[BankAccount] depositing the money: " + howmuch + " from: " + amount);
			if (validate(key)){
				this.amount += howmuch;
				this.amount = roundUp(this.amount);
				return getInfo("deposit", howmuch);
			}else{
				throw new BankUnauthorizedException();
			}			
		}
		
		/**
		 * Withdraw money from an account, confirming that the request is valid
		 * @param howmuch the money to be withdrawn
		 * @param key some key to confirm access to this particular account
		 * @return the JSON formatted indication of the operation taken
		 * @throws BankUnauthorizedException
		 * @throws BankInsuffientFundsException
		 */
		public String withdraw(double howmuch, String key) throws BankUnauthorizedException, BankInsuffientFundsException{
			Logger.LogBankMessage("[BankAccount] withdrawing the money: " + howmuch + " from: " + amount);
			if (validate(key)){
				if (howmuch <= this.amount){
					this.amount -= howmuch;
					this.amount = roundUp(this.amount);
					return getInfo("withdraw", howmuch);
				}else{					
					throw new BankInsuffientFundsException();
				}				
			}else{
				throw new BankUnauthorizedException();
			}
		}
		
		/**
		 * This is to round up the double amounts to two decimal places
		 * @param number
		 * @return
		 */
		private double roundUp(Double number){
			return (Math.round(number * 100)/100.0);
		}
		
		/**
		 * This function validates that the request has access to a specific account, this will grow to be a lot more extensive and implement per bank account security features.
		 * @param key
		 * @return
		 */
		private boolean validate(String key){
			return this.validator.equals(key);
		}
		
		/**
		 * Overriding the equals method so that the ArrayList plays nicely with the BankAccount object
		 */
		public boolean equals(Object accountobject){
			return this.accountname.equals(((BankAccount)accountobject).accountname);			
		}
		
		/**
		 * Overriding the toString() method
		 */
		public String toString(){
			return getInfo("balance");
		}		
		
		/**
		 * The method prints the JSON response for each of the operations, the only different is the string key associated with the amounts e.g.
		 * "initial_balance" for new account, "deposit" for a deposit ...
		 * @param stringforamount the string that should go before the amount value in the JSON response
		 * @return the JSON formatted information about the operation {"account":"name","balance":23.23}
		 */
		private String getInfo(String stringforamount){
			return "{\"account\":\"" + this.accountname + "\",\"" + stringforamount + "\":" + String.format( "%.2f", this.amount) + "}";			
		}
		
		/**
		 * The method prints the JSON response for each of the operations, the only different is the string key associated with the amounts e.g.
		 * "initial_balance" for new account, "deposit" for a deposit ...
		 * @param stringforamount the string that should go before the amount value in the JSON response
		 * @param theamount this allows for specifying the amount to be reported on, default is to display the balance, otherwise the deposit/withdraw amount
		 * @return the JSON formatted information about the operation {"account":"name","balance":23.23}
		 */
		private String getInfo(String stringforamount, double theamount){
			return "{\"account\":\"" + this.accountname + "\",\"" + stringforamount + "\":" + String.format( "%.2f",theamount) + "}";			
		}
		
		/**
		 * The method prints the JSON response for each of the operations, the only different is the string key associated with the amounts e.g.
		 * "initial_balance" for new account, "deposit" for a deposit ... this one requires a key to validate access to the information ..i.e. 
		 * being public its not used internally
		 * @param stringforamount the string that should go before the amount value in the JSON response
		 * @param key some key to confirm access to this particular account
		 * @return the JSON formatted information about the operation {"account":"name","balance":23.23}
		 * @throws BankUnauthorizedException
		 */
		public String getInfo(String stringforamount, String key) throws BankUnauthorizedException{
			if (validate(key)){
				return "{\"account\":\"" + this.accountname + "\",\"" + stringforamount + "\":" + String.format( "%.2f", this.amount) + "}";
			}else{
				throw new BankUnauthorizedException();
			}			
		}
	}
}
