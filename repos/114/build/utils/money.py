from errors import InvalidMoney
from validation import *

class Money(object):
    def __init__(self, dollars, cents = 0):
        self._dollars = dollars
        self._cents = cents

    @staticmethod
    def new(amt):
        try:
            dollars, cents = val_dollar_string(amt, False)
            return Money(dollars, cents)
        except:
            raise InvalidMoney("bad string format")

    def __str__(self):
        return "{0}.{1:02d}".format(self._dollars, self._cents)

    def __eq__(self, other):
        return self._dollars == other._dollars and \
               self._cents == other._cents

    def __add__(self, other):
        tmp_cents = self._cents + other._cents
        tmp_dollars = self._dollars + other._dollars
        if(tmp_cents >= 100):
            tmp_dollars += 1
            tmp_cents -= 100
        return Money(tmp_dollars, tmp_cents)

    def __sub__(self, other):
        tmp_cents = self._cents - other._cents
        tmp_dollars = self._dollars - other._dollars
        if(tmp_cents < 0):
            tmp_dollars -= 1
            tmp_cents += 100
        return Money(tmp_dollars, tmp_cents)

    def __lt__(self, other):
        if self._dollars == other._dollars:
            return self._cents < other._cents
        return self._dollars < other._dollars

    def __gt__(self, other):
        if self._dollars == other._dollars:
            return self._cents > other._cents
        return self._dollars > other._dollars

    def __le__(self, other):
        if self._dollars == other._dollars:
            return self._cents <= other._cents
        return self._dollars <= other._dollars

    def __ge__(self, other):
        if self._dollars == other._dollars:
            return self._cents >= other._cents
        return self._dollars >= other._dollars

