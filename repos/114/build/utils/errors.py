import sys
import argparse

class ArgumentParser(argparse.ArgumentParser):

    def error(self, message):
        self.exit(255)

class ProtocolErrorException(Exception):
    pass

class InvalidMoney(Exception):
    pass

class InvalidBalance(Exception):
    pass

class AuthfileExistsException(Exception):
    pass

class CardfileExistsException(Exception):
    pass

class AccountExistsException(Exception):
    pass

class AtmAuthException(ProtocolErrorException):
    pass

class BankAuthException(ProtocolErrorException):
    pass

class MACException(ProtocolErrorException):
    pass

class InvalidPacketException(ProtocolErrorException):
    pass

class ConnectionClosedException(ProtocolErrorException):
    pass

class OperationFailed(Exception):
    pass

class InvalidArgumentException(Exception):
    pass
