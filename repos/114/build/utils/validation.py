import argparse
from string import ascii_lowercase, digits
from argparse import ArgumentTypeError
from re import match

def val_account(money):
    max_dollars = 2**32 - 1
    max_cents = 99
    if money._dollars < 0 or \
       money._cents > max_cents or \
       money._cents < 0:
        return False
    return True

def val_acct_name(value):
    allowed = '._' + ascii_lowercase + digits
    if len(value) < 1 or len(value) > 250:
        raise ArgumentTypeError("Invalid Account Name")
    for c in value:
        if c not in allowed:
            raise ArgumentTypeError("Invalid Account Name")
    return value


def val_file_name(value):
    allowed = '._' + ascii_lowercase + digits
    if value == '.' or value == '..' or len(value) > 255 or len(value) < 1:
        raise ArgumentTypeError("Invalid File Name")
    for c in value:
        if c not in allowed:
            raise ArgumentTypeError("Invalid File Name")
    return value


def val_port(value):
    val = int(value)
    if val < 1024 or val > 65535 or not val_number(str(value)):
        raise ArgumentTypeError("Invalid Port")
    return val


def val_ip(value):
    try:
        a,b,c,d = map(int,map(val_number, value.split('.')))

        if a > 255 or a < 0 or \
            b > 255 or b < 0 or \
            c > 255 or c < 0 or \
            d > 255 or d < 0:
                raise ArgumentTypeError("Invalid IP")

    except ValueError:
        raise ArgumentTypeError("Invalid IP")

    return value


def val_number(value):
    if not match("^(?:[1-9][0-9]*|0)$", value):
        raise ArgumentTypeError("Invalid Number")
    return value


def val_dollar_string(value, to_string=True):
    try:
        d = value.split('.')
        if len(d) != 2 \
            or not (val_number(d[0]) and int(d[0]) < 2**32 \
                and len(d[1]) == 2 and int(d[1]) > -1 and int(d[1]) < 100):
            raise ArgumentTypeError("Invalid String format for money")
        if to_string:
            return value
        else:
            return int(d[0]), int(d[1])

    except ValueError:
        raise ArgumentTypeError("Invalid String format for money")

def initial_value(value):
    try:
        if val_dollar_string(value) and float(value) >= float(10.0):
            return value
        else:
            raise ArgumentTypeError("Invalid String format for money")
    except:
        raise ArgumentTypeError("Invalid String format for money")
