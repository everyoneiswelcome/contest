from threading import Lock
from errors import InvalidBalance
from money import Money
from validation import *

from Crypto.Hash import SHA512
from Crypto.Cipher import PKCS1_OAEP


class Account(object):
    def __str__(self):
        return "{0:s}.{1:s}".format(self._id, self._balance)

    #Uniqueness is the bank's problem bitches
    #However, checking the balance is the Account's problem
    def __init__(self, ident, money, salt, hashpass):
        self._id = ident
        self._balance = money
        self._lock = Lock()
        if not val_account(self._balance):
            raise InvalidBalance("invalid balance on creation")

        self._salt = salt
        self._hashpass = hashpass

    def lock(self):
        self._lock.acquire()

    def unlock(self):
        self._lock.release()

    def check_withdraw(self, amt):
        tmp = self._balance - amt
        if tmp < Money(0, 0):
            return False
        return True

    def withdraw(self, amt):
        self._balance = self._balance - amt

    def deposit(self, amt):
        self._balance = self._balance + amt

    def balance(self):
        return self._balance

    def authenticate(self, card, key):
        sha = SHA512.new()
        sha.update(self._salt)
        sha.update(card)
        sha.update(self._id)

        return sha.digest() == self._hashpass
