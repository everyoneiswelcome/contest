import error
import util

# Modes of operation
MOP_NEW = 0
MOP_DEP = 1
MOP_WIT = 2
MOP_GET = 3
MOP_ERR = 4 # Error
MOP_UNK = 5 # Unknown

modes_operation = {
    MOP_NEW: "initial_balance",
    MOP_DEP: "deposit",
    MOP_WIT: "withdraw",
    MOP_GET: "balance",
    MOP_ERR: "error",
    MOP_UNK: "unknown"
}

MSG_JSON_TMPL = """{"account": "%s", "%s": %s}"""

def fields_from_txt(msg_str):
    """Raw message format: "msg_id|mop|acct|value"
    """

    fields = msg_str.split('|')
    if len(fields) != 4:
        raise error.ProtocolException("Unexpected message format %s" % msg_str)
    msg_id, mop, acct, value = fields
    return mop, acct, value, msg_id

def msg_str_to_json(msg_str):
    """Build a JSON message from a TXT message."""

    mop, acct, value, msg_id = fields_from_txt(msg_str)
    mop = modes_operation[int(mop)]
    return MSG_JSON_TMPL % (acct, mop, value)

def msg_from_txt(msg_str):
    """Build a Message object from a TXT message."""

    mop, acct, value, msg_id = fields_from_txt(msg_str)
    return Message(int(mop), acct, value, msg_id)

class Message(object):
    """Base class for all transaction messages.
    """

    TYPE = None
    ATTRS = None

    def __init__(self, mop, acct, value, msg_id=None):
        self.mop = mop
        self.acct = acct
        self.value = value
        if msg_id:
            self.msg_id = msg_id
        else:
            self.msg_id = util.get_uuid()

    def __str__(self):
        return "%s|%d|%s|%s" % (self.msg_id, self.mop, self.acct, self.value)

    def __repr__(self):
        return self.__str__()

    def to_json(self):
        return MSG_JSON_TMPL % (self.acct, modes_operation[self.mop], self.value)
