import unittest
import decimal
import re

import util

class TestUtil(unittest.TestCase):

    def test_true_currency_pattern(self):
        p = util.P_CURRENCY
        currencies = ['22.00', '0.00', '9990.02']
        for c in currencies:
            self.assertTrue(p.match(c))

    def test_false_currency_pattern(self):
        p = util.P_CURRENCY
        currencies = ['2', '02.00', '.00', '20.', '20.1', 'aa.bb']
        for c in currencies:
            self.assertFalse(p.match(c))

    def test_get_uuid(self):
        guid = util.get_uuid()
        self.assertEquals(len(guid), 32)

    def test_min_blance(self):
        self.assertTrue(util.min_balance(decimal.Decimal('0.00')))

    def test_is_uuid(self):
        self.assertTrue(util.is_uuid('F527C32B39CE4ACBA100CD72D1284606'))

    def test_is_bad_uuid(self):
        self.assertFalse(util.is_uuid('toto'))

    def test_is_currency(self):
        self.assertTrue(util.is_currency('10.20'))

    def test_is_currency_one_dec(self):
        self.assertFalse(util.is_currency('10.2'))

    def test_is_currency_three_dec(self):
        self.assertFalse(util.is_currency('10.222'))

    def test_is_currency_max(self):
        self.assertTrue(util.is_currency('4294967295.99'))

    def test_is_currency_min(self):
        self.assertTrue(util.is_currency('0.00'))

    def test_is_currency_greater_then_max(self):
        self.assertFalse(util.is_currency('4294967296'))

    def test_is_currency_negative(self):
        self.assertFalse(util.is_currency('-20.00'))

if __name__ == '__main__':
    unittest.main()