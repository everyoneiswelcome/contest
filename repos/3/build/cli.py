"""Command line parsing and validation.
"""

import argparse
import os
import re
import sys

import conf
import util

# PATTERNS

# Pattern for files and account
P_FILE = re.compile(r'^(?:[_\-\.0-9a-z]+)$')

# Pattern for IP address
P_IP = re.compile(r'^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$')

# Balance and currency: pattern for whole amount

# Balance and currency: pattern for factional amount
P_FRACTONAL = re.compile(r'^[0-9]{2}$')

# CUSTOM ARGUMENT PARSER
class ArgsParser(argparse.ArgumentParser):
    """Custom ArgumentParser that overrides the error method.
    No default message printed to stdout.
    """
    def error(self, msg):
        util.log(msg)
        self.exit(255)

# CUSTOM VALIDATORS
def account_type(value):
    """Validate the format of the bank account.
    """
    value = value.strip()
    if value and P_FILE.match(value) and len(value) < 251:
        return value
    msg = "%s: invalid account" % value
    raise argparse.ArgumentTypeError(msg)

def file_type(value):
    """Validate the format of the file name.
    """
    value = value.strip()
    if value and P_FILE.match(value) \
             and len(value) < 256 \
             and value not in ('.', '..'):
        return value
    msg = "%s: invalid file" % value
    raise argparse.ArgumentTypeError(msg)

def ip_type(value):
    """Validate the format of an IP address.
    """
    value = value.strip()
    if value and P_IP.match(value):
        return value
    msg = "%s: invalid ip address" % value
    raise argparse.ArgumentTypeError(msg)

def port_type(value):
    """Validate the format of an TCP port: numeric, does not start with '0',
    between 1024 and 65536 inclusively
    """
    value = value.strip()
    if value and value.isdigit() and not value.startswith('0'):
        if int(value) in range(1024, 65536):
            return int(value)
    msg = "%s: invalid TCP port" % value
    raise argparse.ArgumentTypeError(msg)

def currency_new_type(value):
    """New account."""
    value = value.strip()
    if util.is_currency(value) and float(value) >= 10:
        return value
    msg = "%s: invalid currency/balance for new account" % str(value)
    raise argparse.ArgumentTypeError(msg)

def currency_type(value):
    value = value.strip()
    if util.is_currency(value) and float(value) > 0:
        return value
    msg = "%s: invalid currency or balance" % value
    raise argparse.ArgumentTypeError(msg)

def val_mult_args(args):
    """Identify multiple occurences of the same option. argparse does not do
    it natively,"""
    if len(args) > len(set(args)):
        util.log("Multiple occurrences of same option")
        sys.exit(255)

# HELPERS
def update_card(args):
    """Custom method to set the card file. If not passed  at the command line,
    set default card: <account>.card.
    """
    if not args.c:
        args.c = "%s.card" % args.a
    return args

def validate_card(parser, args):
    """Validate existence of card file for new account. Card file should not
    exist for new account."""
    card = args.c
    account = args.a
    if args.n and os.path.isfile(card):
        parser.error("Operation rejected for account (%s): card file (%s) \
already exists." % (account, card))

# CLI PARSER
def atm_parse():
    """Create an ATM command line options and arguments parser. Parse the
    command line validating and sanitizing all parameters. All default values
    are also initialized. If not errors, returns a Namespace object (args) to the
    client. The namespace contains all valid values expected by the client.
    """
    parser = ArgsParser(prog=conf.ATM_NAME, add_help=False)
    parser.add_argument('-a', help='<account>', type=account_type,
                        required=True)
    parser.add_argument('-s', help='<auth-file>', type=file_type,
                        default=conf.AUTH_FILE)
    parser.add_argument('-i', help='<ip-address>', type=ip_type,
                        default=conf.LOCALHOST)
    parser.add_argument('-p', help='<port>', type=port_type,
                        default=conf.PORT)
    parser.add_argument('-c', help='<card-file>', type=file_type)

    # modes
    group_mode = parser.add_mutually_exclusive_group(required=True)
    group_mode.add_argument('-n', help='<new_account>', type=currency_new_type)
    group_mode.add_argument('-d', help='<deposit_amount>', type=currency_type)
    group_mode.add_argument('-w', help='<withdraw_amount>', type=currency_type)
    group_mode.add_argument('-g', help='<get_balance>', action='store_true')

    args = parser.parse_args()
    args = update_card(args)
    validate_card(parser, args)
    return args

def bank_parse():
    """Create a Bank command line options and arguments parser. Parse the
    command line validating and sanitizing all parameters. All default values
    are also initialized. If not errors, returns a Namespace object (args) to the
    server. The namespace contains all valid values expected by the server.
    """
    parser = ArgsParser(prog=conf.BANK_NAME, add_help=False)
    parser.add_argument('-s', help='<auth-file>', type=file_type,
                        default=conf.AUTH_FILE)
    parser.add_argument('-p', help='<port>', type=port_type,
                        default=conf.PORT)
    args = parser.parse_args()
    return args
