"""Core Bank module.
"""

import os
import signal
import socket
import sys

import error
import cli
import conf
import crypto
#import msg
import simplemsg as smsg
import util

from decimal import Decimal

NAME = "Bank"
REPR_TMPL = "BankServer(auth='%s', port='%s')"

class BankServer(object):
    """Represents the bank server. Holds the configuration. Initiates Bank
    actions.
    """

    def __init__(self, args):
        self.auth = args.s
        self.port = args.p
        self.client_sock = None
        self.msg_ids = {} # stores msg ids already received (k = id, v = msg)
        self.accounts = {} # stores account + balance (k = account, v = balance)
        self.check_auth()
        self.key, self.bank_key, self.atm_key, self.iv = self.create_auth()
        self.init_net()
        self.run()

    def __repr__(self):
        return REPR_TMPL % (self.auth, self.port)

    def init_net(self):
        self.listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.listener.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        host = '' # bind to all interfaces
        self.listener.bind((host, self.port))
        self.listener.listen(1) # number of pending connections
        signal.signal(signal.SIGTERM, self.signal_handler)
        #signal.signal(signal.SIGINT, self.signal_handler)

    def account_exists(self, account):
        return account in self.accounts.keys()

    def balance_handler(self, message):
        if not self.account_exists(message.acct):
            return self.build_error_msg(message, "Account does not exist")

        message.value = str(self.accounts[message.acct])
        return message

    def initial_balance_handler(self, message):
        if self.account_exists(message.acct):
            return self.build_error_msg(message, "Account already exists")

        balance = Decimal(message.value)
        if balance < 10:
            return self.build_error_msg(message, "Insufficient balance to \
create account")

        self.accounts[message.acct] = balance
        return message

    def deposit_handler(self, message):
        if not self.account_exists(message.acct):
            return self.build_error_msg(message, "Account does not exist")

        balance = self.accounts[message.acct]
        new_balance = balance + Decimal(message.value)
        self.accounts[message.acct] = new_balance
        return message

    def withdraw_handler(self, message):
        if not self.account_exists(message.acct):
            return self.build_error_msg(message, "Account does not exist")

        withdraw = Decimal(message.value)
        balance = self.accounts[message.acct]
        if withdraw > balance:
            return self.build_error_msg(message, "Insufficient balance")
        else:
            self.accounts[message.acct] = balance - withdraw
            return message

    def build_error_msg(self, message, err):
        message.mop = smsg.MOP_ERR
        message.value = err
        return message

    def msg_handler(self, message):
        """Dispatch message upon reception: call specific handler based on type
        of message received.
        """

        if message.msg_id in self.msg_ids.keys(): # msg already received?
            raise error.ProtocolException(error.SRV_E_REPLAY_MSG)

        self.msg_ids[message.msg_id] = message

        call = {
            smsg.MOP_NEW: self.initial_balance_handler,
            smsg.MOP_DEP: self.deposit_handler,
            smsg.MOP_WIT: self.withdraw_handler,
            smsg.MOP_GET: self.balance_handler,
        }

        return call[message.mop](message) # Dispatch to custom handler

    def encrypt(self, data):
        return crypto.encrypt(data, self.key, self.iv, self.bank_key)

    def decrypt(self, data):
        return crypto.decrypt(data, self.key, self.iv, self.atm_key)

    def run(self):
        while True:
            try:
                self.client_sock, client_addr = self.listener.accept()
            except socket.error:
                util.log("Caught socket error.")
                continue
            except KeyboardInterrupt:
                util.log("Caught keyboard interruptions.")
                sys.exit(255) # Test bundle controls server with kb interrupt

            self.client_sock.settimeout(conf.TIMEOUT)
            try:
                data = self.client_sock.recv(conf.BUF_SIZE)
                if data:
                    data = self.decrypt(data)
                    message = smsg.msg_from_txt(data)
                    msg_resp = self.msg_handler(message)
                    if msg_resp:
                        if msg_resp.mop != smsg.MOP_ERR: # don't display error
                            util.println(msg_resp.to_json())
                        msg_pt = str(msg_resp)
                        msg_ct = self.encrypt(msg_pt)
                        self.client_sock.sendall(msg_ct)
                else:
                    util.log("done receiving data received - empty msg?")
            except(KeyboardInterrupt, SystemExit):
                util.log("Caught interruptions while receiving.")
                sys.exit(255)
            except error.ProtocolException, e:
                util.println(conf.PROTOCOL_ERROR)
                util.log(e.message)
                self.client_sock.close()
                continue
            except error.SecurityException, e:
                util.println(conf.PROTOCOL_ERROR)
                util.log(e.message)
                continue
            except socket.timeout:
                util.println(conf.PROTOCOL_ERROR)
                util.log("timeout")
                continue

    # AUTH FILE MANAGEMENT
    def check_auth(self):
        """On startup if auth file exists, exit with code 255."""
        if os.path.isfile(self.auth):
            util.log("auth file exists. exits.")
            sys.exit(255)

    def create_auth(self):
        key, bank_sign_k, atm_sign_k, iv = crypto.create_auth(self.auth)
        util.println("created")
        return key, bank_sign_k, atm_sign_k, iv

    def delete_auth(self):
        if os.path.isfile(self.auth):
            os.remove(self.auth)

    def quit(self, signum):
        """Ends the bank process: close open socket, delete auth file."""

        if self.client_sock:
            self.client_sock.close()
        self.listener.close()
        self.delete_auth()
        util.log("received signal: %d" % signum)
        sys.exit(0)

    def signal_handler(self, signum, frame):
        """Dispatch to appropriate signal handler."""
        if signum == signal.SIGTERM:
            self.quit(signum)

def main():
    cli.val_mult_args(sys.argv)
    args = cli.bank_parse()
    bank = BankServer(args)
    util.log(str(bank))

if __name__ == '__main__':
    main()
