import unittest
import os

from Crypto.Cipher import AES

import crypto
import util
import error

class TestCrypto(unittest.TestCase):

    def test_encode_decode(self):
        data = "TEST"
        self.assertEquals(crypto.decode(crypto.encode(data)), data)

    def test_decode(self):
        encoded = 'dG90bw=='
        self.assertEquals(crypto.decode('dG90bw=='), 'toto')

    def test_encode(self):
        self.assertEquals(crypto.encode('toto'), 'dG90bw==')

    def test_new_key(self):
        key = crypto.new_key()
        self.assertEquals(len(key), crypto.KEY_SIZE)

    def test_new_iv(self):
        iv = crypto.new_iv()
        self.assertEquals(len(iv), AES.block_size)

    def test_create_card(self):
        key = crypto.new_key()
        acct = 'ted'
        card = 'ted.card'
        crypto.create_card_file(acct, card, key)
        self.assertTrue(os.path.isfile(card))
        # Clean-up
        os.remove(card)

    def test_card(self):
        key = crypto.new_key()
        acct = 'ted'
        card = 'ted.card'
        crypto.create_card_file(acct, card, key)
        self.assertTrue(os.path.isfile(card))
        self.assertTrue(crypto.is_card_valid(acct, card, key))

        # Clean-up
        os.remove(card)

    def test_create_auth_file(self):
        auth = 'bank.auth'
        iv, key = crypto.create_auth(auth)
        self.assertTrue(os.path.isfile(auth))

        # Clean-up
        os.remove(auth)

    def test_create_auth_file(self):
        auth = 'bank.auth'
        key1, bank_key1, atm_key1, iv1 = crypto.create_auth(auth)
        self.assertTrue(os.path.isfile(auth))
        self.assertEquals(len(key1), crypto.KEY_SIZE)
        self.assertEquals(len(bank_key1), crypto.KEY_SIZE)
        self.assertEquals(len(atm_key1), crypto.KEY_SIZE)
        self.assertEquals(len(iv1), AES.block_size)

        # Clean-up
        os.remove(auth)

    def test_read_auth_file(self):
        auth = 'bank.auth'
        key1, bank_key1, atm_key1, iv1 = crypto.create_auth(auth)
        key2, bank_key2, atm_key2, iv2 = crypto.read_auth(auth)
        self.assertEquals(key1, key2)
        self.assertEquals(bank_key1, bank_key2)
        self.assertEquals(atm_key1, atm_key2)
        self.assertEquals(iv1, iv2)

        # Clean-up
        os.remove(auth)

    def test_encrypt_decrypt_ctr(self):
        key = crypto.new_key()
        ctr_enc = crypto.new_ctr()
        plain_txt = "Toute la musique que j'aime"
        cipher_txt = crypto._encrypt_ctr(plain_txt, key, ctr_enc)
        ctr_dec = crypto.new_ctr()
        data = crypto._decrypt_ctr(cipher_txt, key, ctr_dec)
        self.assertEquals(plain_txt, data)

    def test_encrypt_decrypt_iv(self):
        key = crypto.new_key()
        iv = crypto.new_iv()
        plain_txt = "Toute la musique que j'aime"
        cipher_txt = crypto._encrypt(plain_txt, key, iv)
        data = crypto._decrypt(cipher_txt, key, iv)
        self.assertEquals(plain_txt, data)

    def test_sign_verify(self):
        cipher_txt = "someencrypteddata"
        key = crypto.new_key()
        signed_cipher = crypto.sign(cipher_txt, key)
        self.assertTrue(crypto.verify(signed_cipher, key))

    def test_bank_sign_atm_verify(self):
        cipher_txt = "someencrypteddata"
        key = crypto.new_key()
        bank_key = crypto.new_bank_key(key)
        # Bank signs
        signed_cipher = crypto.sign(cipher_txt, bank_key)
        # ATM verifies
        self.assertTrue(crypto.verify(signed_cipher, bank_key))

    def test_atm_sign_bank_verify(self):
        cipher_txt = "someencrypteddata"
        key = crypto.new_key()
        atm_key = crypto.new_atm_key(key)
        # ATM signs
        signed_cipher = crypto.sign(cipher_txt, atm_key)
        # Bank verifies
        self.assertTrue(crypto.verify(signed_cipher, atm_key))

    def test_atm_sign_bank_verity_vail(self):
        cipher_txt = "someencrypteddata"
        key = crypto.new_key()
        atm_key = crypto.new_atm_key(key)
        bank_key = crypto.new_bank_key(key)
        # ATM signs
        signed_cipher = crypto.sign(cipher_txt, atm_key)
        # Bank verifies (fails)
        self.assertFalse(crypto.verify(signed_cipher, bank_key))

    def test_bank_sign_atm_verify_fail(self):
        cipher_txt = "someencrypteddata"
        key = crypto.new_key()
        atm_key = crypto.new_atm_key(key)
        bank_key = crypto.new_bank_key(key)
        # Bank sign
        signed_cipher = crypto.sign(cipher_txt, bank_key)
        # ATM verifies (fails)
        self.assertFalse(crypto.verify(signed_cipher, atm_key))

    def test_bank_encrypt_atm_decrypt(self):
        key = crypto.new_key()
        iv = crypto.new_iv()
        bank_key = crypto.new_bank_key(key)
        plain_txt = "Toute la musique que j'aime"
        # Bank encrypts
        enc_msg = crypto.encrypt(plain_txt, key, iv, bank_key)
        # Atm decrypts
        dec_msg = crypto.decrypt(enc_msg, key, iv, bank_key)
        self.assertTrue(plain_txt, dec_msg)

    def test_atm_encrypt_bank_decrypt(self):
        key = crypto.new_key()
        iv = crypto.new_iv()
        atm_key = crypto.new_atm_key(key)
        plain_txt = "Toute la musique que j'aime"
        # ATM encrypts
        enc_msg = crypto.encrypt(plain_txt, key, iv, atm_key)
        # Bank decrypts
        dec_msg = crypto.decrypt(enc_msg, key, iv, atm_key)
        self.assertTrue(plain_txt, dec_msg)

    def test_bank_decrypt_empty_msg(self):
        key = crypto.new_key()
        iv = crypto.new_iv()
        atm_key = crypto.new_bank_key(key)
        enc_msg = ""
        with self.assertRaises(error.SecurityException):
            dec_msg = crypto.decrypt(enc_msg, key, iv, atm_key)

    def test_atm_encrypt_bank_decrypt_wrong_key(self):
        key = crypto.new_key()
        iv = crypto.new_iv()
        atm_key = crypto.new_atm_key(key)
        bank_key = crypto.new_bank_key(key)
        plain_txt = "Toute la musique que j'aime"
        enc_msg = crypto.encrypt(plain_txt, key, iv, atm_key)
        with self.assertRaises(error.SecurityException):
            dec_msg = crypto.decrypt(enc_msg, key, iv, bank_key)

if __name__ == '__main__':
    unittest.main()