import unittest
from decimal import Decimal

import simplejson as json

import simplemsg as smsg
import error

class TestMsg(unittest.TestCase):

    def test_build_msg_balance(self):
        m = smsg.Message(3, "ted", "10.00")
        self.assertIsNotNone(m.msg_id)
        m_txt = str(m) # "msg_id|mop|acct|value"
        self.assertTrue(m_txt.endswith("|3|ted|10.00"))
        m_json = smsg.msg_str_to_json(m_txt)
        self.assertEquals(m_json, '{"account": "ted", "balance": 10.00}')
        data = json.loads(m_json, use_decimal=True)
        self.assertEquals(data["account"], "ted")
        self.assertEquals(data["balance"], Decimal("10.00"))

    def test_build_msg_new(self):
        m = smsg.Message(0, "ted", "10.00")
        self.assertIsNotNone(m.msg_id)
        m_txt = str(m) # "msg_id|mop|acct|value"
        self.assertTrue(m_txt.endswith("|0|ted|10.00"))
        m_json = smsg.msg_str_to_json(m_txt)
        self.assertEquals(m_json, '{"account": "ted", "initial_balance": 10.00}')
        data = json.loads(m_json, use_decimal=True)
        self.assertEquals(data["account"], "ted")
        self.assertEquals(data["initial_balance"], Decimal("10.00"))

    def test_build_msg_deposit(self):
        m = smsg.Message(1, "ted", "10.00")
        self.assertIsNotNone(m.msg_id)
        m_txt = str(m) # "msg_id|mop|acct|value"
        self.assertTrue(m_txt.endswith("|1|ted|10.00"))
        m_json = smsg.msg_str_to_json(m_txt)
        self.assertEquals(m_json, '{"account": "ted", "deposit": 10.00}')
        data = json.loads(m_json, use_decimal=True)
        self.assertEquals(data["account"], "ted")
        self.assertEquals(data["deposit"], Decimal("10.00"))

    def test_build_msg_withdraw(self):
        m = smsg.Message(2, "ted", "10.00")
        self.assertIsNotNone(m.msg_id)
        m_txt = str(m) # "msg_id|mop|acct|value"
        self.assertTrue(m_txt.endswith("|2|ted|10.00"))
        m_json = smsg.msg_str_to_json(m_txt)
        self.assertEquals(m_json, '{"account": "ted", "withdraw": 10.00}')
        data = json.loads(m_json, use_decimal=True)
        self.assertEquals(data["account"], "ted")
        self.assertEquals(data["withdraw"], Decimal("10.00"))

    def test_build_msg_error(self):
        m = smsg.Message(4, "ted", "some_error")
        self.assertIsNotNone(m.msg_id)
        m_txt = str(m) # "msg_id|mop|acct|value"
        self.assertTrue(m_txt.endswith("|4|ted|some_error"))
        m_json = smsg.msg_str_to_json(m_txt)

    def test_raw_mesg_error(self):
        data = 'something'
        with self.assertRaises(error.ProtocolException):
            message = smsg.msg_str_to_json(data)

    def test_fields_from_text(self):
        msg_txt = "FD36B9E703D54BA79276114FFDF8200E|2|ted|10.00"
        mop, acct, value, msg_id = smsg.fields_from_txt(msg_txt)
        self.assertEquals(msg_id, "FD36B9E703D54BA79276114FFDF8200E")
        self.assertEquals(mop, "2")
        self.assertEquals(acct, "ted")
        self.assertEquals(value, "10.00")

    def test_msg_str_to_json(self):
        msg_txt = "FD36B9E703D54BA79276114FFDF8200E|2|ted|10.00"
        msg_json = smsg.msg_str_to_json(msg_txt)
        self.assertEquals(msg_json, '{"account": "ted", "withdraw": 10.00}')

    def test_msg_from_txt(self):
        msg_txt = "FD36B9E703D54BA79276114FFDF8200E|2|ted|10.00"
        o = smsg.msg_from_txt(msg_txt)
        self.assertEquals(o.msg_id, "FD36B9E703D54BA79276114FFDF8200E")
        self.assertEquals(o.mop, 2)
        self.assertEquals(o.acct, "ted")
        self.assertEquals(o.value, "10.00")

if __name__ == '__main__':
    unittest.main()