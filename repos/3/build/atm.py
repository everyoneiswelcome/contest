"""Core ATM module.
"""

import socket
import sys

import cli
import conf
import crypto
import error
import simplemsg as smsg
import util

NAME = "ATM Client"
ATM_TMPL = "AtmClient(auth='%s', ip_address='%s', port='%s')"
TRANS_TMPL = "AtmTransaction(account='%s', card='%s', action='%s', \
amount='%s')"

class AtmTransaction(object):
    def __init__(self, args, key):
        self.account = args.a
        self.card = args.c
        self.key = key
        self.mop, self.value = self.get_mop_value(args)
        self.tr_id = 0

    def __repr__(self):
        return TRANS_TMPL % (self.account, self.card, self.mop, self.value)

    def get_mop_value(self, args):
        if args.n:
            return smsg.MOP_NEW, args.n
        if args.d:
            return smsg.MOP_DEP, args.d
        if args.w:
            return smsg.MOP_WIT, args.w
        if args.g:
            return smsg.MOP_GET, '0.00'

    def account_exists(self):
        if not crypto.is_card_valid(self.account, self.card, self.key):
            util.log("Account does not exist")
            return False
        return True

    def validate_new_account(self):
        if crypto.is_card_valid(self.account, self.card, self.key):
            util.log("Account already exists")
            return False
        return True

    def create_new_account(self):
        crypto.create_card_file(self.account, self.card, self.key)

    def validate_deposit(self):
        # Core tests fail with deposit = 0.00
        return self.account_exists() and float(self.value) > 0

    def validate_withdraw(self):
        # Core tests fail with withdraw = 0.00
        return self.account_exists() and float(self.value) > 0

    def validate_balance(self):
        return self.account_exists()

    def validate(self):
        call = {
            smsg.MOP_NEW: self.validate_new_account,
            smsg.MOP_DEP: self.validate_deposit,
            smsg.MOP_WIT: self.validate_withdraw,
            smsg.MOP_GET: self.validate_balance
        }
        return call[self.mop]()

    def create_message(self):
        """Create JSON message to be sent to Bank."""
        message = smsg.Message(self.mop, self.account, self.value)
        self.tr_id = message.msg_id
        msg_txt = str(message)
        return msg_txt

class AtmClient(object):
    def __init__(self, args):
        """Init properties used by the ATM. All properties have been sanitized
        at this point.
        """
        self.auth = args.s

        try:
            self.key, self.bank_key, self.atm_key, self.iv = crypto.read_auth(self.auth)
        except IOError, msg:
            util.log(msg) # Auth file missing
            sys.exit(255)

        if any([not self.key, not self.bank_key, not self.atm_key, not self.iv]):
            util.log("Share key, Bank verif key, ATM sign key or IV is missing")
            raise error.SecurityException("Missing security keys")

        self.ip_address = args.i
        self.port = args.p
        self.transaction = AtmTransaction(args, self.key)

    def __repr__(self):
        return ATM_TMPL % (self.auth, self.ip_address, self.port)

    def account_exists(self):
        return crypto.is_card_valid(self.transaction.account,
                                    self.transaction.card,
                                    self.key)

    def create_account(self):
        crypto.create_card_file(self.transaction.account,
                                self.transaction.card,
                                self.key)

    def encrypt(self, data):
        return crypto.encrypt(data, self.key, self.iv, self.atm_key)

    def decrypt(self, data):
        return crypto.decrypt(data, self.key, self.iv, self.bank_key)

    def send_transaction(self):
        if not self.transaction.validate():
            util.log("Invalid transaction")
            sys.exit(255)

        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error, e:
            util.log("Socket error: %s" % e)
            sys.exit(63)

        try:
            s.connect((self.ip_address, self.port))
        except socket.gaierror, e:
            util.log("Error with server address: %s" % e)
            sys.exit(63)
        except socket.error, e:
            util.log("Connection Error: %s" % e)
            sys.exit(63)

        message = self.transaction.create_message()
        util.log("Message: %s" % message)

        try:
            enc_msg = self.encrypt(message)
            if not enc_msg:
                util.log("message empty")
                sys.exit(255)

            s.sendall(enc_msg)
        except socket.error, e:
            util.log("Error sending msg to bank: %s" % e)
            sys.exit(63)

        s.settimeout(conf.TIMEOUT)
        try:
            data = s.recv(conf.BUF_SIZE)

            if data:
                data = self.decrypt(data)
                message = smsg.msg_from_txt(data)

                if message.msg_id != self.transaction.tr_id:
                    # Replay attack?
                    raise error.ProtocolException("Unexpected message ID")

                if self.transaction.mop == smsg.MOP_NEW:
                    # Create card only after successful result from the bank
                    self.create_account()

                if message.mop == smsg.MOP_ERR:
                    # An error occurred on the server side
                    util.log("Error message: %s" % message)
                    sys.exit(255)
                else:
                    util.println(message.to_json())
            else:
                raise error.ProtocolException("Empty message")

        except socket.error, e:
            util.log("Error in receive: %s" % e)
            sys.exit(63)

        except socket.timeout:
            util.log("timeout")
            sys.exit(63)

        except error.ProtocolException, e:
            util.log(e.message)
            sys.exit(63)

        except error.SecurityException, e:
            util.log(e.message)
            sys.exit(63)

        finally:
            s.close()

def main():
    cli.val_mult_args(sys.argv)
    args = cli.atm_parse()
    atm = AtmClient(args)
    atm.send_transaction()
    sys.exit(0)

if __name__ == '__main__':
    main()

