"""Custom exception, in particular Protocol Exception.
"""

# Msg validation errors
MSG_E_NO_DATA = "No data in JSON message"
MSG_E_MISSING_FIELDS = "Missing fields in JSON message"
MSG_E_MISSING_TYPE = "Missing fields in JSON message"
MSG_E_MISSING_VALUE = "Attribute(s) with no value in JSON message"
MSG_E_INVALID_JSON = "Invalid JSON message"
MSG_E_INVALID_ATTR = "Invalid attribute(s) in message"
MSG_E_INVALID_CURRENCY = "Attribute expected decimal"
MSG_E_NO_ACCOUNT = "Missing account in JSON message"
MSG_E_NO_ID = "Missing id in JSON message"
MSG_E_BAD_ID = "Invalid id in JSON message"
MSG_E_TOO_MANY_TYPES = "More than one type in the JSON message"

# Server validation errors
SRV_E_REPLAY_MSG = "Received transaction more than once"

# PROTOCOL EXCEPTION
class ProtocolException(Exception):
    """Exception generated when a protocol error is identify by the atm or bank.
    """
    pass

# SECURITY EXCEPTION
class SecurityException(Exception):
    """Exception generated when a crypto routine or possible security violation
    isdetected.
    """
    pass
