import unittest
import argparse

import cli
import conf

class TestCli(unittest.TestCase):

    # TEST ACCOUNT
    def test_alpha_account(self):
        value = "toto"
        self.assertEquals(cli.account_type(value), value)

    def test_numeric_account(self):
        value = "123"
        self.assertEquals(cli.account_type(value), value)

    def test_alnum_account(self):
        value = "123"
        self.assertEquals(cli.account_type(value), value)

    def test_all_char_account(self):
        value = "-123_._abc-"
        self.assertEquals(cli.account_type(value), value)

    def test_dot_account(self):
        value = "."
        self.assertEquals(cli.account_type(value), value)

    def test_dot_dot_account(self):
        value = ".."
        self.assertEquals(cli.account_type(value), value)

    def test_long_account(self):
        value = "a" * 250
        self.assertEquals(cli.account_type(value), value)

    def test_uppercase_account(self):
        value = "TOTO"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.account_type(value)

    def test_too_long_account(self):
        value = "a" * 251
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.account_type(value)

    def test_empty_account(self):
        value = ""
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.account_type(value)

    def test_special_char_account(self):
        value = "$"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.account_type(value)

    # TEST FILE
    def test_alpha_file(self):
        value = "toto"
        self.assertEquals(cli.file_type(value), value)

    def test_numeric_file(self):
        value = "123"
        self.assertEquals(cli.file_type(value), value)

    def test_alnum_file(self):
        value = "123"
        self.assertEquals(cli.file_type(value), value)

    def test_all_char_file(self):
        value = "-123_._abc-"
        self.assertEquals(cli.file_type(value), value)

    def test_dot_file(self):
        value = "."
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.file_type(value)

    def test_dot_dot_file(self):
        value = ".."
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.file_type(value)

    def test_long_file(self):
        value = "a" * 255
        self.assertEquals(cli.file_type(value), value)

    def test_uppercase_file(self):
        value = "TOTO"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.file_type(value)

    def test_too_long_file(self):
        value = "a" * 256
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.file_type(value)

    def test_empty_file(self):
        value = ""
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.file_type(value)

    def test_special_char_file(self):
        value = "$"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.file_type(value)

    def test_dollar_file(self):
        value = "toto%"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.file_type(value)

    def test_percent_file(self):
        value = "to$to"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.file_type(value)

    # IP ADDRESS
    def test_correct_ip(self):
        value = "123.123.123.134"
        self.assertEquals(cli.ip_type(value), value)

    def test_localhost_ip(self):
        value = "127.0.0.1"
        self.assertEquals(cli.ip_type(value), value)

    def test_al_zero_ip(self):
        value = "0.0.0.0"
        self.assertEquals(cli.ip_type(value), value)

    def test_empty_ip(self):
        value = ""
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.ip_type(value)

    def test_no_dot_ip(self):
        value = "123"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.ip_type(value)

    def test_alpha_ip(self):
        value = "abc.abc.abc.abc"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.ip_type(value)

    def test_too_large_number_ip(self):
        value = "256.123.123.123" # first block > 255
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.ip_type(value)

    def test_two_blocks_ip(self):
        value = "123.123"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.ip_type(value)

    # TCP PORT
    def test_correct_port(self):
        value = "55555"
        self.assertEquals(cli.port_type(value), int(value))

    def test_min_port(self):
        value = "1024"
        self.assertEquals(cli.port_type(value), int(value))

    def test_max_port(self):
        value = "65535"
        self.assertEquals(cli.port_type(value), int(value))

    def test_empty_port(self):
        value = ""
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.ip_type(value)

    def test_zero_port(self):
        value = "0"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.port_type(value)

    def test_port_starts_with_zero(self):
        value = "0123"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.port_type(value)

    def test_alnum_port(self):
        value = "123abc"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.port_type(value)

    def test_alpha_port(self):
        value = "abc"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.port_type(value)

    def test_zero_port(self):
        value = "0"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.port_type(value)

    def test_too_large_port(self):
        value = "65536"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.port_type(value)

    def test_too_large_argument_port(self):
        value = "2" * 4097
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.port_type(value)

    def test_too_small_port(self):
        value = "1000"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.port_type(value)

    # CURRENCY - BALANCE
    def test_decimal_balance(self):
        value = "10.02"
        self.assertEquals(cli.currency_type(value), "10.02")

    def test_max_balance(self):
        value = "4294967295.99"
        self.assertEquals(cli.currency_type(value), "4294967295.99")

    def test_min_balance(self):
        value = "0.00"
        self.assertEquals(cli.currency_type(value), "0.00")

    def test_alpha_currency(self):
        value = "aaa"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.currency_type(value)

    def test_alpha_dot_currency(self):
        value = "aaa.bb"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.currency_type(value)

    def test_alpnum_currency(self):
        value = "aaa.99"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.currency_type(value)

    def test_three_decimal_currency(self):
        value = "10.999"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.currency_type(value)

    def test_greater_than_max_currency(self):
        value = "4294967296.99"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.currency_type(value)

    def test_negative_currency(self):
        value = "-10"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.currency_type(value)

    def test_two_dots_currency(self):
        value = "10.2.2"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.currency_type(value)

    def test_currency_starts_with_zero(self):
        value = "010.10"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.currency_type(value)

    def test_min_balance(self):
        value = "0"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.currency_type(value)

    def test_min_decimal_balance(self):
        value = "0.0"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.currency_type(value)

    def test_simple_balance(self):
        value = "10"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.currency_type(value)

    def test_dot_balance(self):
        value = "10."
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.currency_type(value)

    # BALANCE NEW ACCOUNT
    def test_new_account_balance(self):
        value = "10.00"
        self.assertEquals(cli.currency_new_type(value), "10.00")

    def test_insufficient_new_account_balance(self):
        value = "9.2"
        with self.assertRaises(argparse.ArgumentTypeError):
            cli.currency_new_type(value)

if __name__ == '__main__':
    unittest.main()