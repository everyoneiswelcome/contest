"""Crypto functions for securing ATM-Bank communication and data."""

import base64
import sys
import os

from Crypto.Cipher import AES
from Crypto.Util import Counter
from Crypto.Hash import HMAC
from Crypto.Hash import SHA256
import Crypto.Random.OSRNG.posix as RNG

import conf
import error
import util

KEY_SIZE = 32
BS = AES.block_size

# Lambda functions for padding and unpadding
pad = lambda buf, bs: buf + (bs - len(buf) % bs) * chr(bs - len(buf) % bs)
unpad = lambda buf: buf[:-ord(buf[len(buf)-1:])]

def create_card_file(account, card_file, key):
    if not card_file:
        card_file = "%s.card"
    with open(card_file, 'w') as f:
        f.write(encode(_hmac(account, key)))

def is_card_valid(account, card_file, key):
    if os.path.isfile(card_file):
        with open(card_file) as f:
            signed_account = decode(f.read().strip())
            return _hmac(account, key) == signed_account
    return False

def encode(buf):
    """Base64 encode chunk of data."""
    return base64.b64encode(buf)

def decode(buf):
    """Base64 decode some text."""
    try:
        data = base64.b64decode(buf)
    except TypeError, e:
        raise error.ProtocolException(e.message)
    return data

def new_key():
    """Generate a random key of size KEY_SIZE."""
    return RNG.new().read(KEY_SIZE)

def new_ctr():
    """Generate counter for AES counter mode."""
    return Counter.new(128)

def new_iv():
    """Generate IV."""
    return RNG.new().read(AES.block_size)

def create_auth(auth_file):
    """Create a key and save it in auth file."""
    key = new_key()
    iv = new_iv()
    bank_sign_k = new_bank_key(key)
    atm_sign_k = new_atm_key(key)
    with open(auth_file, 'w') as f:
        f.write(encode(key + bank_sign_k + atm_sign_k + iv))
    return key, bank_sign_k, atm_sign_k, iv

def read_auth(auth_file):
    """Read auth file and return iv and key."""
    iv, key = None, None

    with open(auth_file) as f:
        buf = decode(f.read())
    if buf:
        key = buf[:KEY_SIZE]
        bank_sign_k = buf[KEY_SIZE:KEY_SIZE*2]
        atm_sign_k = buf[KEY_SIZE*2:KEY_SIZE*3]
        iv = buf[KEY_SIZE*3:]
    return key, bank_sign_k, atm_sign_k, iv

def _encrypt(data, key, iv):
    padded_data = pad(data, AES.block_size)
    enc = AES.new(key, AES.MODE_CBC, iv)
    return enc.encrypt(padded_data)

def _decrypt(buf, key, iv):
    dec = AES.new(key, AES.MODE_CBC, iv)
    data = dec.decrypt(buf)
    return unpad(data)

def _encrypt_ctr(data, key, ctr):
    padded_data = pad(data, AES.block_size)
    enc = AES.new(key, AES.MODE_CTR, counter=ctr)
    return enc.encrypt(padded_data)

def _decrypt_ctr(buf, key, ctr):
    dec = AES.new(key, AES.MODE_CTR, counter=ctr)
    data = dec.decrypt(buf)
    return unpad(data)

def _hmac(buf, key):
    return HMAC.new(key, msg=buf, digestmod=SHA256).digest()

def sign(cipher_txt, key):
    return cipher_txt + _hmac(cipher_txt, key)

def verify(cipher_txt, key):
    tag = cipher_txt[-SHA256.digest_size:]
    buf = cipher_txt[:-SHA256.digest_size]
    return (tag == _hmac(buf, key))

def extract_ct(enc_data):
    """Extract the cipher_txt off the payload, leaving off the tag."""
    return enc_data[:-SHA256.digest_size]

def new_atm_key(key):
    return _hmac(conf.ATM_CODE, key)

def new_bank_key(key):
    return _hmac(conf.BANK_CODE, key)

def encrypt(message, key, iv, key_sign):
    cipher_txt = _encrypt(message, key, iv)
    signed_msg = sign(cipher_txt, key_sign)
    return encode(signed_msg)

def decrypt(message, key, iv, key_sign):
    if not message:
        raise error.SecurityException("No message to decrypt")

    data = decode(message)

    if not verify(data, key_sign):
        raise error.SecurityException("Message not comming from known sender")

    cipher_txt = extract_ct(data)

    if not cipher_txt:
        raise error.SecurityException("Empty cipher text")

    return _decrypt(cipher_txt, key, iv)
