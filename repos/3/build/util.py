import binascii
import decimal
import os
import re
import sys
import uuid

import conf

P_CURRENCY = re.compile(r'^(:?0|[1-9][0-9]*)\.\d{2}$')

def log(msg):
    if conf.DEBUG:
        sys.stderr.write("DEBUG >>> " + str(msg) + '\n')
        sys.stderr.flush()

def println(msg):
    sys.stdout.write(msg)
    sys.stdout.write('\n')
    sys.stdout.flush()

def get_script_dir():
    return os.path.dirname(os.path.realpath(sys.argv[0]))

def hex(buf):
    return binascii.hexlify(buf)

def short_hex(buf):
    return hex(buf)[:10] + '...'

def get_uuid():
    return str(uuid.uuid4()).upper().replace('-', '')

def min_balance(value):
    return value == conf.MIN_BALANCE

def is_currency(value):
    """Validate balance and currency format.
    - Whole and fractional part separated with a '.'
    - Fractional is always 2 digits
    - Numeric
    -
    """

    value = value.strip()

    if not P_CURRENCY.match(value):
        return False

    try:
        d = decimal.Decimal(value)
    except decimal.InvalidOperation, e:
        return False

    if d > conf.MAX_BALANCE: # Balances are bounded to 4294967295.99
        return False

    if d < 0:
        return False

    return True

def is_uuid(guid):
    """Validate GUID."""
    guid = guid.replace('-', '').upper()
    return re.match(re.compile(r'^[0-9A-F]{32}$'), guid)
