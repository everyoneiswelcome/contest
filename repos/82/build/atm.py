#!/usr/bin/env python2.7

from __future__ import print_function
from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Random import random
from decimal import Decimal

import argparse
import base64
import datetime
import decimal
import hashlib
import json
import re
import string
import sys
import urllib
import urllib2

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        return super(DecimalEncoder, self).default(o)


_BANK_IP = "127.0.0.1"
_PORT = "3000"
_AUTH_FILE = "bank.auth"
BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s: s[:-ord(s[len(s)-1:])]


class AESCipher:
    def __init__(self, key):
        self.key = key

    def encrypt(self, raw):
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(pad(raw)))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return unpad(cipher.decrypt(enc[16:]))

class ArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        self.exit(255)


class Atm(object):
    def __init__(self, auth_file="auth-file.xml", card_file="card-file.xml"):
        self.auth_file = auth_file
        self.card_file = card_file

    def check_args(self, auth_file, ip, port, card_file, account, balance,
                   deposit, withdraw):
        # auth_file
        if not re.match(r'[a-z_\-\.0-9]{1,255}$', auth_file) or auth_file == "." or auth_file == "..":
            sys.exit(255)
        # ip: not necesary check if len<4096
        if not re.match(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$', ip):
            sys.exit(255)
        # port
        if port is not None:
            if not re.match(r'[1-9]\d{3,4}$', port) or int(port) < 1024 or int(port) > 65535:
                sys.exit(255)
        # card_file
        if not re.match(r'[a-z_\-\.0-9]{1,255}$', card_file) or card_file == "." or card_file == "..":
            sys.exit(255)
        # account name
        if account is not None:
            if not re.match(r'[a-z_\-\.0-9]{1,250}$', account):
                sys.exit(255)
        # balance
            if balance is not None:
                if not re.match(r'[1-9]\d{0,9}\.\d{2}$', balance) or float(balance) < 10 or float(balance) > 4294967295.99:
                    sys.exit(255)
        # deposit
            elif deposit is not None:
                if not re.match(r'0|[1-9]\d{0,9}\.\d{2}$', deposit) or float(deposit) < 0 or float(deposit) > 4294967295.99:
                    sys.exit(255)
            elif withdraw is not None:
                if not re.match(r'0|[1-9]\d{0,9}\.\d{2}$', withdraw) or float(withdraw) < 0 or float(withdraw) > 4294967295.99:
                    sys.exit(255)
        return 1

    def generate_pin(self, length):
        return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length))

    def create_account(self, auth_file, bank_ip, port, card_file, account,
                       balance):
        try:
            f_key = open(auth_file, "r")
        except IOError:
            sys.exit(255)
        else:
            with f_key:
                key = f_key.read(16)
        url = "http://" + bank_ip + ":" + str(port) + "/"
        # Encrypt + hash data
        cy = AESCipher(key)
        pin = self.generate_pin(16)
        nonce = self.generate_pin(10)
        t_stamp = str(datetime.datetime.utcnow())
        data = "action:initial_balance;" + "account:" + account + ";balance:" + balance + ";cardname:" + card_file + ";pin:" + pin + ";nonce:" + nonce + ";timestamp:" + t_stamp
        hash_data = hashlib.sha256(data).hexdigest()
        data = data + ";hash:" + hash_data
        enc_data = cy.encrypt(data)
        # Send post
        form = {'transaction':enc_data}
        u_form = urllib.urlencode(form)
        req = urllib2.Request(url, u_form)
        try:
            response = urllib2.urlopen(req, timeout=10)
        except Exception:
            sys.exit(63)
        the_page = response.read()
        dec_str = cy.decrypt(the_page)
        dec_list = dec_str.split(";")
        n_list = dec_list[1].split(":")
        if dec_list[0] == "1" and n_list[1] == nonce:
            try:
                f_card = open(card_file, "w")
            except IOError:
                sys.exit(255)
            else:
                with f_card:
                    f_card.write(pin)
            print (json.dumps({"initial_balance":Decimal(balance.strip(' "')), "account":account}, cls=DecimalEncoder))
            sys.stdout.flush()
            sys.exit(0)
        else:
            sys.exit(int(dec_list[0]))

    def deposit(self, auth_file, bank_ip, port, card_file, account, amount):
        if amount == None:
            sys.exit(255)
        try:
            f_key = open(auth_file, "r")
        except IOError:
            sys.exit(255)
        else:
            with f_key:
                key = f_key.read(16)
        try:
            f_pin = open(card_file, "r")
        except IOError:
            sys.exit(255)
        else:
            with f_pin:
                pin = f_pin.read(16)
        url = "http://" + bank_ip + ":" + str(port) + "/"
        # Encrypt + hash data
        cy = AESCipher(key)
        nonce = self.generate_pin(10)
        t_stamp = str(datetime.datetime.utcnow())
        data = "action:deposit;" + "account:" + account + ";amount:" +amount + ";cardname:" + card_file + ";pin:" + pin + ";nonce:" + nonce + ";timestamp:" + t_stamp
        hash_data = hashlib.sha256(data).hexdigest()
        data = data + ";hash:" + hash_data
        enc_data = cy.encrypt(data)
        # Send post
        form = {'transaction':enc_data}
        u_form = urllib.urlencode(form)
        req = urllib2.Request(url, u_form)
        try:
            response = urllib2.urlopen(req, timeout=10)
        except Exception:
            sys.exit(63)
        the_page = response.read()
        dec_str = cy.decrypt(the_page)
        dec_list = dec_str.split(";")
        n_list = dec_list[1].split(":")
        if dec_list[0] == "1" and n_list[1] == nonce:
            print (json.dumps({"deposit":Decimal(amount.strip(' "')), "account":account}, cls=DecimalEncoder))
            sys.stdout.flush()
            sys.exit(0)
        # Error -> show 255
        else:
            sys.exit(int(dec_list[0]))

    def withdraw(self, auth_file, bank_ip, port, card_file, account, amount):
        try:
            f_key = open(auth_file, "r")
        except IOError:
            sys.exit(255)
        else:
            with f_key:
                key = f_key.read(16)
        try:
            f_pin = open(card_file, "r")
        except IOError:
            sys.exit(255)
        else:
            with f_pin:
                pin = f_pin.read(16)
        url = "http://" + bank_ip + ":" + str(port) + "/"
        cy = AESCipher(key)
        nonce = self.generate_pin(10)
        t_stamp = str(datetime.datetime.utcnow())
        data = "action:withdraw;" + "account:" + account + ";amount:" + amount + ";cardname:" + card_file + ";pin:" + pin + ";nonce:" + nonce + ";timestamp:" + t_stamp
        hash_data = hashlib.sha256(data).hexdigest()
        data = data + ";hash:" + hash_data
        enc_data = cy.encrypt(data)
        # Send post
        form = {'transaction':enc_data}
        u_form = urllib.urlencode(form)
        req = urllib2.Request(url, u_form)
        try:
            response = urllib2.urlopen(req, timeout=10)
        except Exception:
            sys.exit(63)
        the_page = response.read()
        dec_str = cy.decrypt(the_page)
        dec_list = dec_str.split(";")
        n_list = dec_list[1].split(":")
        if dec_list[0] == "1" and n_list[1] == nonce:
            print (json.dumps({"withdraw":Decimal(amount.strip(' "')), "account":account}, cls=DecimalEncoder))

            sys.stdout.flush()
            sys.exit(0)
        else:
            sys.exit(int(dec_list[0]))

    def get_balance(self, auth_file, bank_ip, port, card_file, account):
        try:
            f_key = open(auth_file, "r")
        except IOError:
            sys.exit(255)
        else:
            with f_key:
                key = f_key.read(16)
        try:
            f_pin = open(card_file, "r")
        except IOError:
            sys.exit(255)
        else:
            with f_pin:
                pin = f_pin.read(16)
        url = "http://" + bank_ip + ":" + str(port) + "/"
        cy = AESCipher(key)
        nonce = self.generate_pin(10)
        t_stamp = str(datetime.datetime.utcnow())
        data = "action:balance;" + "account:" + account + ";cardname:" + card_file + ";pin:" + pin + ";nonce:" + nonce + ";timestamp:" + t_stamp
        hash_data = hashlib.sha256(data).hexdigest()
        data = data + ";hash:" + hash_data
        enc_data = cy.encrypt(data)
        # Send post
        form = {'transaction':enc_data}
        u_form = urllib.urlencode(form)
        req = urllib2.Request(url, u_form)
        try:
            response = urllib2.urlopen(req, timeout=10)
        except Exception:
            sys.exit(63)
        the_page = response.read()
        dec_str = cy.decrypt(the_page)
        dec_list = dec_str.split(";")
        n_list = dec_list[1].split(":")
        if dec_list[0] == "1" and n_list[1] == nonce:
            amount_list = dec_list[2].split(":")
            print (json.dumps({"balance":Decimal(amount_list[1].strip(' "')), "account":account}, cls=DecimalEncoder))
            sys.exit(0)
        else:
            sys.exit(int(dec_list[0]))


if __name__== '__main__':
    parser = ArgumentParser()
    parser.add_argument("-s", dest="auth_file", type=str, help="auth file")
    parser.add_argument("-i", dest="ip", type=str,
                        help="ip address where the server is listening")
    parser.add_argument("-p", dest="port", type=str,
                        help="port where the server is listening")
    parser.add_argument("-c", dest="card_file", type=str, help="card file")
    parser.add_argument("-a", dest="account", type=str, help="account",
                        required=True)
    group = parser.add_mutually_exclusive_group()
    group.add_argument("-n", dest="balance", type=str, help="Create a new account")
    group.add_argument("-d", dest="deposit", type=str,
                        help="deposit amount of money")
    group.add_argument("-w", dest="withdraw", type=str,
                        help="withdraw amount of money")
    group.add_argument("-g", dest="get_balance", help="get the balance",
                       action="store_true")
    atm = Atm()
    check_dup = set(sys.argv)
    if len(check_dup) != len(sys.argv):
        sys.exit(255)
    try:
        args = parser.parse_args()
    except argparse.ArgumentError:
        sys.exit(255)

    if args.auth_file:
        _AUTH_FILE = args.auth_file
    if args.ip:
        _BANK_IP = args.ip
    if args.port:
        _PORT = args.port
    if args.card_file:
        _CARD_FILE = args.card_file
    else:
        _CARD_FILE = args.account + ".card"
    if atm.check_args(_AUTH_FILE, _BANK_IP, _PORT, _CARD_FILE, args.account,
                      args.balance, args.deposit, args.withdraw):
        if args.balance:
            atm.create_account(_AUTH_FILE, _BANK_IP, int(_PORT), _CARD_FILE,
                               args.account, args.balance)
        elif args.deposit:
            atm.deposit(_AUTH_FILE, _BANK_IP, int(_PORT), _CARD_FILE,
                           args.account, args.deposit)
        elif args.withdraw:
            atm.withdraw(_AUTH_FILE, _BANK_IP, int(_PORT), _CARD_FILE,
                            args.account, args.withdraw)
        else:
            atm.get_balance(_AUTH_FILE, _BANK_IP, int(_PORT), _CARD_FILE,
                            args.account)
        sys.exit(0)
    else:
        sys.exit(255)
