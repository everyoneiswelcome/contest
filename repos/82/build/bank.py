#!/usr/bin/env python2.7
from __future__ import print_function
from decimal import Decimal
from Crypto.Cipher import AES
from Crypto import Random
from Crypto.Random import random
from SocketServer import ThreadingMixIn
from BaseHTTPServer import HTTPServer
from datetime import datetime
import argparse
import sys
import re
import json
import os
import cgi,hmac,base64
import threading,thread

import hashlib
import decimal
import BaseHTTPServer
import socket
import string



MAX_DELAY=20000
WAIT_TIME=10


def generate_pin(length):
        return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length))


def createAuthFile(filename):
        pin = generate_pin(16)
        try:
            f_card = open(filename, "w")
        except IOError:
            sys.exit(255)
        else:
            with f_card:
                f_card.write(pin)

def print_error(error_str, code):
        print(error_str + "\n", file=sys.stderr)
        #sys.exit(code)


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        return super(DecimalEncoder, self).default(o)


def checkAccount(account):
    if not re.match(r'[a-z_\-\.0-9]{1,250}$', account):
        raise Exception("ERROR:Not valid account format")
    else:
        return True

def checkInitialCard(account,cardName):
    global bank
    if cardName in bank.accountCardNameList.values():
        raise ApplicationException("ERROR:Already used account")
    else:
        return True
def checkCard(account,pin,cardName):
    global bank
    if bank.accountCardList[account] != pin:
        raise ApplicationException("ERROR:Not valid PIN")
    if bank.accountCardNameList[account]!=cardName:
        raise ApplicationException("ERROR:Not valid cardname")
    else:
        return True

def checkBalance(balance):
    if (balance and not re.match(r'[1-9]\d{0,9}\.\d{2}$', balance)):
       raise ApplicationException("ERROR:Not valid balance")
    if (Decimal(balance) < 10.00):
       raise ApplicationException("ERROR:Not enough money balance")
    return True

def checkAmount(amount):
    if amount and not re.match(r'0|[1-9]\d{0,9}\.\d{2}$', amount):
        raise ApplicationException("ERROR:Not valid amount")
    if Decimal(amount) <= 0.00:
        raise ApplicationException("ERROR:Negative or zero amount not allowed")
    if Decimal(amount) > 4294967296:
        raise ApplicationException("Too big amount")
    else:
        return True

def checkOperation(operation):
    if operation not in ["balance","initial_balance","deposit","withdraw"]:
        raise Exception("ERROR:Not valid balance/amount format")
    else:
        return True

def loadKey(authfile):
    try:
        f_key = open(authfile, "r")
    except IOError:
        pass
    else:
        with f_key:
            key = f_key.read(16)
    return key


def extractFields(bodyData):
    fieldValueList = {}
    fieldList = bodyData.split(";")
    for field in fieldList:
        if field.startswith("timestamp"):
            fieldValueList["timestamp"]=field[10:]
        else:
            nameValue = field.split(":")
            fieldValueList[nameValue[0]]= nameValue[1]
    return fieldValueList


def checkReplay(nonce,timestamp):
    #First we check for tiemstamp delta
    dateTimeStamp = datetime.strptime(timestamp,'%Y-%m-%d %H:%M:%S.%f')
    deltaTime = datetime.utcnow() - dateTimeStamp
    if deltaTime.seconds > MAX_DELAY:
        raise Exception("ERROR:Expired nonce ")
    #The we check if it is in the table
    global bank
    if (nonce in bank.nonceData):
        raise Exception("ERROR:Reinyected package")

def checkHash(data,hash):
    #We get rid of the trailing ;hash field
    index = data.index(";hash:")
    subString=data[0:index]
    h_data = hashlib.sha256(subString).hexdigest()
    if (h_data != hash):
        raise Exception("ERROR:Modified Message")





BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s : s[:-ord(s[len(s)-1:])]


BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
unpad = lambda s : s[:-ord(s[len(s)-1:])]

class ApplicationException(Exception):
    def __init__(self, message):
        self.message = message

class AESCipher:
    def __init__( self, key ):
        self.key = key

    def encrypt( self, raw ):
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode( iv + cipher.encrypt(pad(raw)) )

    def decrypt( self, enc ):
        enc = base64.b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return unpad(cipher.decrypt(enc[16:]))


class ThreadingSimpleServer(ThreadingMixIn, HTTPServer):
     pass



#HTTP Handler
class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def handle_one_request(self):
        try:
            self.raw_requestline = self.rfile.readline(65537)
            if len(self.raw_requestline) > 65536:
                self.requestline = ''
                self.request_version = ''
                self.command = ''
                self.send_error(414)
                return
            if not self.raw_requestline:
                self.close_connection = 1
                return
            if not self.parse_request():
                # An error code has been sent, just exit
                return
            mname = 'do_' + self.command
            if not hasattr(self, mname):
                self.send_error(501, "Unsupported method (%r)" % self.command)
                return
            method = getattr(self, mname)
            method()
            self.wfile.flush() #actually send the response if not already done.
        except socket.timeout, e:
            #a read or a write timed out.  Discard this connection
            print ("protocol_error")
            sys.stdout.flush()
            self.close_connection = 1
            return
    def setup(self):
        BaseHTTPServer.BaseHTTPRequestHandler.setup(self)
        self.request.settimeout(10)
    def do_POST(self):
        global bank
        try:
            # Parse the form data posted
            form = cgi.FieldStorage(
                fp=self.rfile,
                headers=self.headers,
                environ={'REQUEST_METHOD':'POST',
                         'CONTENT_TYPE':self.headers['Content-Type'],
                         })
            for field in form.keys():
                field_item = form[field]
            #Transaction specified in field Transaction
            cypheredData = form["transaction"].value
            aesCypher = AESCipher(bank.key)
            decypheredData = aesCypher.decrypt(cypheredData)
            fieldList = extractFields(decypheredData)
            #First we check operation and account which should always be present
            account = fieldList["account"]
            checkAccount(account)
            operation = fieldList["action"]
            checkOperation(operation)
            pin = fieldList["pin"]
            nonce = fieldList["nonce"]
            cardName = fieldList["cardname"]
            checkReplay(nonce,fieldList["timestamp"])
            checkHash(decypheredData,fieldList["hash"])
            #Check for addtional parameters
            if operation =="initial_balance":
                balance = fieldList["balance"]
                checkBalance(balance)
                checkInitialCard(account,cardName)
                decBalance = Decimal(balance.strip(' "'))
                #Check for correct initial balance format and bounds done
                #Check for already created account
                try:
                    bank.lock.acquire()
                    if bank.accountList[account] is not None:
                        bank.lock.release()
                        raise ApplicationException("Already created account")
                except KeyError:
                    #If exception is raised, we create the account
                    bank.accountList[account]= decBalance
                    bank.accountCardList[account]= pin
                    bank.accountCardNameList[account] = cardName
                    bank.lock.release()
                    print (json.dumps({"initial_balance":Decimal(balance.strip(' "')), "account":account}, cls=DecimalEncoder))
                    sys.stdout.flush()
                    self.wfile.write(aesCypher.encrypt("1"+";nonce:"+nonce))
            if operation =="deposit":
                amount = fieldList["amount"]
                checkAmount(amount)
                decAmount = Decimal(amount.strip(' "'))
                bank.lock.acquire()
                if bank.accountList[account] is not None:
                    checkCard(account,pin,cardName)
                    bank.accountList[account] = bank.accountList[account] + Decimal(amount.strip(' "'))
                    bank.lock.release()
                    print (json.dumps({"deposit":Decimal(amount.strip(' "')), "account":account}, cls=DecimalEncoder))
                    sys.stdout.flush()
                    self.wfile.write(aesCypher.encrypt("1"+";nonce:"+nonce))
                else:
                    bank.lock.release()
                    raise ApplicationException("ERROR:Tried to deposit in a nonexistent account")
            if operation =="balance":
                if bank.accountList[account] is not None:
                    checkCard(account,pin,cardName)
                    print (json.dumps({"account":account,"balance":bank.accountList[account]},cls=DecimalEncoder))
                    sys.stdout.flush()
                    resp = aesCypher.encrypt("1"+";nonce:" + nonce + ";balance:" + str(bank.accountList[account]))
                    self.wfile.write(resp)
                else:
                    raise ApplicationException("ERROR:Asked for the balance of a nonexistent account")
            if operation =="withdraw":
                amount = fieldList["amount"]
                checkAmount(amount)
                #Check for already created account
                bank.lock.acquire()
                if bank.accountList[account] is not None:
                    checkCard(account,pin,cardName)
                    decAmount = Decimal(amount.strip(' "'))
                    accountReal= bank.accountList[account]
                    #Check for funds
                    if accountReal >= decAmount:
                        bank.accountList[account] = accountReal - decAmount
                        bank.lock.release()
                        print (json.dumps({"account":account,"withdraw":decAmount},cls=DecimalEncoder))
                        sys.stdout.flush()
                        self.wfile.write(aesCypher.encrypt("1"+";nonce:"+nonce))
                    else:
                        bank.lock.release()
                        raise ApplicationException("ERROR:Not enough funds for withdrawal")
                else:
                    bank.lock.release()
                    raise ApplicationException("ERROR:Tried to deposit in a nonexistent account")
        except ApplicationException:
            if bank.lock.locked():
                bank.lock.release()
            self.wfile.write(aesCypher.encrypt("255"+";nonce:"+nonce))
        except Exception:
            if bank.lock.locked():
                bank.lock.release()
            print ("protocol_error")
            self.wfile.write(aesCypher.encrypt("63"+";nonce:"+nonce))
            sys.exit(63)
        return

class ArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        sys.exit(255)

class Bank(object):
    def __init__(self):
        self.accountList = {}
        self.accountCardList = {}
        self.accountCardNameList ={}
        self.key = ""
        self.lock = threading.Lock()
        self.nonceData={}
        self.isActive=True
    def check_args(self, auth_file, port):
        # auth_file
        if not re.match(r'[a-z_\-\.0-9]{1,255}$', auth_file) or auth_file == "." or auth_file == ".." or len(auth_file)>4096:
            sys.exit(255)

        if not os.path.isfile(auth_file):
            createAuthFile(auth_file)

        # port
        if port <1024 or port > 65535:
            sys.exit(255)

    def flushNonceData(self):
        for item in self.nonceData:
            currentDate=datetime.utcnow()
            timestamp = self.nonceData["item"]
            if datetime.timedelta(timestamp,currentDate) > MAX_DELAY:
                del self.nonceData["item"]

class TimerCleaner(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.event = threading.Event()

    def run(self):
        global bank
        while(bank.isActive):
                bank.flushNonceData()
                self.event.wait(WAIT_TIME)

    def stop(self):
        self.event.set()
        self.stop()


if __name__== '__main__':

    parser = ArgumentParser()
    parser.add_argument("-s", "--auth_file", type=str,help="Auth file", nargs='?', default="bank.auth")
    parser.add_argument("-p", "--port", type=int,help="port where the server is listening", nargs='?', default=3000)
    global bank
    bank = Bank()
    check_dup = set(sys.argv)
    if len(check_dup) != len(sys.argv):
        sys.exit(255)
    try:
        args = parser.parse_args()
    except argparse.ArgumentError:
        sys.exit(255)
    try:
        bank.check_args(args.auth_file,args.port)
        bank.key = loadKey(args.auth_file)
        server = ThreadingSimpleServer(('', args.port), MyHandler)
        #Thread to clean the nonce data table
        tmr = TimerCleaner()
        tmr.start()
        print("created")
        sys.stdout.flush()
    except:
        sys.exit(255)
    try:
        while 1:
                server.handle_request()
    except KeyboardInterrupt:
        bank.isActive=False

