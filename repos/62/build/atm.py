#!/usr/bin/env python
import getopt
import sys
import os.path
import random
import string
import simplejson
import SocketServer
import signal
import thread
import threading
import socket
import re
import base64
import hashlib
from Crypto import Random
from Crypto.Cipher import AES

FAIL_RET=255
PROTO_ERR_RET=63
DEBUG=False

##################################
#Utilities
##################################
def id_generator(size=128, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def terminateAtm(err="Terminating atm with error",code=FAIL_RET):
    #print >> sys.stderr, err
    sys.exit(code)

def printJson(data):
    print(simplejson.dumps(data,separators=(',',':')))
def getJson(data):
    return simplejson.dumps(data,separators=(',',':'))


class AESCipher(object):

    def __init__(self, key): 
        self.bs = 32
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
 
    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]


##################################
#Account operation
###################################

#Dictinary containing acconts data
#Key value is the account name
#Data is the account cardId and balance
GDataBase= {}

def accountCreate(accountName,balance):
    #TODO input check
    try:
        balance=round(float(balance),2)
    except ValueError:
        raise ValueError('Invalid balance')

#    print >> sys.stderr, "Creating account '%s' with balance '%.2f'" % (accountName, balance)
    
    #Test if account exists
    if accountName in GDataBase:
       raise RuntimeError('Account already exists', 34)

    #Create account
    accountData = {"cardId":id_generator(16),"balance":balance}
    #accountData = (id_generator(16),balance)
    GDataBase[accountName] = accountData
    
    print "{\"account\":\"%s\",\"initial_balance\":%.2f}" % (accountName,balance)
    sys.stdout.flush()


def accountDeposit(accountName,cardId,amount):
    #TODO input check
    try:
        balance=round(float(amount),2)
    except ValueError:
        raise ValueError('Invalid balance')

  #  print >> sys.stderr, "Deposition '%d' in account %s" % (accountName, balance)
    
    #Test if account exists
    if not accountName in GDataBase:
       raise RuntimeError('Account dont exists', 34)

    GDataBase[accountName]["balance"] += amount

    print "{\"account\":\"%s\",\"deposit\":%.2f}" % (accountName,amount) 
    sys.stdout.flush()


def accountWithdraw(accountName,cardId,amount):
    #TODO input check
    try:
        balance=round(float(amount),2)
    except ValueError:
        raise ValueError('Invalid balance')

  #  print >> sys.stderr, "Deposition '%d' in account %s" % (accountName, balance)
    
    #Test if account exists
    if not accountName in GDataBase:
       raise RuntimeError('Account dont exists', 34)

    #TODO test balance
    GDataBase[accountName]["balance"] -= amount

    print "{\"account\":\"%s\",\"withdraw\":%.2f}" % (accountName,amount) 
    sys.stdout.flush()


def accountBalance(accountName,cardId):
    #TODO teste card
    #Test if account exists
    if not accountName in GDataBase:
       raise RuntimeError('Account dont exists', 34)
    balance = GDataBase[accountName]["balance"]
    print "{\"account\":\"%s\",\"balance\":%.2f}" % (accountName,balance) 
    sys.stdout.flush()
    

#Bank initialization

def parseParameters():
    #modes of operation, required
    modeSet=False
    #mode can be n|d|w|g
    mode=""
    modeArg=0.00
    #required parameter
    accountSet=False
    accountName=""
    #optional parameters
    portSet=False
    ipSet=False
    authFileSet=False
    cardFileSet=False
    port=3000
    ip="127.0.0.1"
    authFile="bank.auth"
    cardFile=""

    #Test number of args
    if len(sys.argv) > 12 or len(sys.argv) < 3:
        terminateAtm()
   
    try:
        opts, args = getopt.getopt(sys.argv[1:], "s:i:p:c:a:n:d:w:g")
    except getopt.GetoptError as err:
        #print >> sys.stderr, err
        terminateAtm()

    #Test is there is remain args
    if len(args) > 0:
        terminateAtm()
    
    #Validate program parameters
    for o, a in opts:
        if o == "-p":
            #test if parameter is repeted 
            if portSet:
                terminateAtm()
            match = re.search(r'^([1-9][0-9]*)$',a)
            if not match:
                terminateAtm("Wrong sintax for port")
            port = int(a)
            if port < 1024 or port > 65535:
                terminateAtm("Port must be between 1024 and 65535")
            portSet = True
        elif o == "-s":
            #test if parameter is repeted 
            if authFileSet:
                terminateAtm()
            match = re.search(r'(^[_\-\.0-9a-z]*$)',a)
            if not match:
                terminateAtm("Wrong sintax for filename")
            match = re.search(r'^\.+$',a)
            if  match:
                terminateAtm("Wrong sintax for filename")
            if len(a) > 255:
                terminateAtm("Wrong sintax for filename, to long")
            authFile=a
            authFileSet = True
        elif o == "-i":
            #test if parameter is repeted 
            if ipSet:
                terminateAtm()
            match = re.search(r'^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$',a)
            if not match:
                terminateAtm("Wrong sintax for ip addr")
            ip=a
            ipSet = True
        elif o == "-c":
            #test if parameter is repeted 
            if cardFileSet:
                terminateAtm()
            match = re.search(r'(^[_\-\.0-9a-z]*$)',a)
            if not match:
                terminateAtm("Wrong sintax for filename")
            match = re.search(r'^\.+$',a)
            if  match:
                terminateAtm("Wrong sintax for filename")
            if len(a) > 255:
                terminateAtm("Wrong sintax for filename, to long")
            cardFile=a
            cardFileSet = True
        elif o == "-a":
            #test if parameter is repeted 
            if accountSet:
                terminateAtm()
            match = re.search(r'(^[_\-\.0-9a-z]*$)',a)
            if not match:
                terminateAtm("Wrong sintax for account")
            if len(a) > 250:
                terminateAtm("Wrong sintax for account, to long")
            accountName=a
            accountSet = True
        elif o in ("-n","-d","-w","-g") :
            #test if parameter is repeted 
            if modeSet:
                terminateAtm()
            mode=o[1]
            if not o == "-g":
                #check sintax
                match = re.search(r'^(0|[1-9][0-9]*)\.([0-9]{2})$',a)
                if not match:
                    terminateAtm("Wrong sintax for ammount")
                modeArg=float(a)
                if modeArg < 0.01 or modeArg > 4294967295.99:
                    terminateAtm("Wrong sintax for ammount, value")
            modeSet = True
        else:
            assert False, "unhandled option"
    #Test for required parameters
    if not accountSet or not modeSet:
        terminateAtm("Account and/or operation not specified")
    if not cardFileSet:
        cardFile = accountName + ".card"
    
    return (ip,port,authFile,cardFile,{"account":accountName,"mode":mode,"arg":modeArg})

def readFile(fileName):
    #check if file exists
    if not os.path.isfile(fileName):
        terminateAtm("File %s dont exists" % fileName) 
    #open and read file
    try:
        f = open(fileName,'r')
        data = f.read()
        f.close()
    except :
        terminateAtm("Problem reading file %s" % fileName)
    
    return data


def writeFile(fileName,data):
    #check if file exists
    if os.path.isfile(fileName):
        terminateAtm("File %s already exists" % fileName) 
    #open and read file
    try:
        f = open(fileName,'w')
        f.write(data)
        f.close()
    except :
        terminateAtm("Problem writing to file %s" % fileName)
   
Gauth=""

def startAtm(ip, port,operation,cardFile):
    global Gauth
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    cripto = AESCipher(Gauth) 
    try:
        #set timeout to 10sec
        sock.settimeout(10.0)
        sock.connect((ip, port))
        #sent task
        data = getJson(operation)
        superData = {}
        superData["data"]=data.encode("base64")
        superData["sec"]=hashlib.sha256(superData["data"]).hexdigest()
        data = getJson(superData)
        data = cripto.encrypt(data)
        #sock.sendall(getJson(operation) + "\n") 
        sock.sendall(data + "\n") 
        
        #receive response
        #TODO Timeout connection
        data = sock.recv(1024)
        if data == '':
            sock.close()
            terminateAtm("Server closed connection, proto error",PROTO_ERR_RET)
    except socket.timeout:
        sock.close()
        terminateAtm("Atm closed connection, timeout",PROTO_ERR_RET)
    except Exception as err:
        #print >> sys.stderr, err
        terminateAtm("Bank comunication problem");
    finally:
        sock.close()
    
    #Parse response
    data = cripto.decrypt(data)
    #print >> sys.stderr, data
    try:
        response=simplejson.loads(data)
        if not "code" in response.keys():
            terminateAtm("Error parsion json, no code, proto error",PROTO_ERR_RET)
        code=response["code"]
        if not code:
            if operation["mode"] == "n":
                if not "card" in response.keys():
                    terminateAtm("Error parsion json, no card proto error",PROTO_ERR_RET)
                writeFile(cardFile,response["card"])
                print "{\"account\":\"%s\",\"initial_balance\":%.2f}" % (operation["account"],operation["arg"])
                sys.stdout.flush()
            elif operation["mode"] == "g":
                if not "balance" in response.keys():
                    terminateAtm("Error parsion json, no balance proto error",PROTO_ERR_RET)
                print "{\"account\":\"%s\",\"balance\":%.2f}" % (operation["account"],response["balance"])
                sys.stdout.flush()
            elif operation["mode"] == "w":
                print "{\"account\":\"%s\",\"withdraw\":%.2f}" % (operation["account"],operation["arg"])
                sys.stdout.flush()
            elif operation["mode"] == "d":
                print "{\"account\":\"%s\",\"deposit\":%.2f}" % (operation["account"],operation["arg"])
                sys.stdout.flush()
            else:
                assert False, "unhandled operation"
                
        #not code
        else:
            terminateAtm("Operation error")
    except Exception as err:
        #print >> sys.stderr, err
        terminateAtm("Error parsion json, proto error",PROTO_ERR_RET)



def main():
    global Gauth
    (ip,port,authFile,cardFile,operation) = parseParameters()
    operation["id"] = id_generator(64) 
    Gauth = readFile(authFile)
    if operation["mode"] == "n":
        #check if file exists
        if os.path.isfile(cardFile):
            terminateAtm("Card %s already exists" % cardFile) 
    else:
        operation["card"] = readFile(cardFile)
    startAtm(ip,port,operation,cardFile)

if __name__ == "__main__":
    main()   
