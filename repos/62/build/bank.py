#!/usr/bin/env python
import getopt
import sys
import os.path
import random
import string
import simplejson
import SocketServer
import signal
import thread
import threading
import re
import base64
import hashlib
from Crypto import Random
from Crypto.Cipher import AES


FAIL_RET=255
DEBUG=False
server = 0

##################################
#Utilities
##################################
def id_generator(size=128, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def terminateBank(err="Teminating bank with error", code=FAIL_RET):
    #print >> sys.stderr, err
    sys.exit(code)

def printJson(data):
    print(simplejson.dumps(data,separators=(',',':')))
def getJson(data):
    return simplejson.dumps(data,separators=(',',':'))

class AESCipher(object):

    def __init__(self, key): 
        self.bs = 32
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')
 
    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]

##################################
#Account operation
###################################

#Dictinary containing acconts data
#Key value is the account name
#Data is the account cardId and balance
GDataBase= {}

def accountCreate(accountName,balance):
    #TODO input check
    try:
        balance=round(float(balance),2)
    except ValueError:
        raise ValueError('Invalid balance')

    if balance < 10.00 or balance > 4294967295.99:
       raise RuntimeError('Balance < 10.00 or > 4294967295.99', 34)

    #print >> sys.stderr, "Creating account '%s' with balance '%.2f'" % (accountName, balance)
    
    #Test if account exists
    if accountName in GDataBase:
       raise RuntimeError('Account already exists', 34)

    #Create account
    accountData = {"cardId":id_generator(16),"balance":balance}
    #accountData = (id_generator(16),balance)
    GDataBase[accountName] = accountData
    
    print "{\"account\":\"%s\",\"initial_balance\":%.2f}" % (accountName,balance) 
    sys.stdout.flush()
    return accountData["cardId"]

def accountDeposit(accountName,cardId,amount):
    #TODO input check
    try:
        amount=round(float(amount),2)
    except ValueError:
        raise ValueError('Invalid amount')

    if amount < 0.01 or amount > 4294967295.99:
       raise RuntimeError('amount < 0.01 or > 4294967295.99', 34)
  #  print >> sys.stderr, "Deposition '%d' in account %s" % (accountName, balance)
    
    #Test if account exists
    if not accountName in GDataBase:
       raise RuntimeError('Account dont exists', 34)

    # test card id
    if not cardId == GDataBase[accountName]["cardId"]:
        #print >> sys.stderr, "cardId dont match %s and %s" % (cardId,GDataBase[accountName]["cardId"])
        raise RuntimeError('Card dont match', 35)
    
    #TODO test balance
    GDataBase[accountName]["balance"] += amount

    print "{\"account\":\"%s\",\"deposit\":%.2f}" % (accountName,amount) 
    sys.stdout.flush()


def accountWithdraw(accountName,cardId,amount):
    #TODO input check
    try:
        amount=round(float(amount),2)
    except ValueError:
        raise ValueError('Invalid amount')

  #  print >> sys.stderr, "Deposition '%d' in account %s" % (accountName, balance)
    
    if amount < 0.01 or amount > 4294967295.99:
       raise RuntimeError('amount < 0.01 or > 4294967295.99', 34)

    #Test if account exists
    if not accountName in GDataBase:
       raise RuntimeError('Account dont exists', 34)

    # test card id
    if not cardId == GDataBase[accountName]["cardId"]:
        #print >> sys.stderr, "cardId dont match %s and %s" % (cardId,GDataBase[accountName]["cardId"])
        raise RuntimeError('Card dont match', 35)
    
    #TODO test balance
    if (GDataBase[accountName]["balance"] - amount) <= -0.01:
        raise RuntimeError("Account %s dont have founds (%f) for %f"%(accountName,GDataBase[accountName]["balance"],amount) , 34)

    GDataBase[accountName]["balance"] -= amount

    print "{\"account\":\"%s\",\"withdraw\":%.2f}" % (accountName,amount) 
    sys.stdout.flush()


def accountBalance(accountName,cardId):
    #Test if account exists
    if not accountName in GDataBase:
        raise RuntimeError('Account dont exists', 34)
    # test card id
    if not cardId == GDataBase[accountName]["cardId"]:
        #print >> sys.stderr, "cardId dont match %s and %s" % (cardId,GDataBase[accountName]["cardId"])
        raise RuntimeError('Card dont match', 35)
    balance = GDataBase[accountName]["balance"]
    print "{\"account\":\"%s\",\"balance\":%.2f}" % (accountName,balance) 
    sys.stdout.flush()
    return balance
    

OpHist=[]

def processOperation(data):
    global OpHist
    response={}


    try:
        superData = simplejson.loads(data)
    except Exception as err:
        raise RuntimeError("Error decoding json",1)
    
    #Check operation md5 and decode operation
    digest = hashlib.sha256(superData["data"]).hexdigest()
    #print "DEBUG,DIGEST:" + digest
    if not superData["sec"] == digest: 
        raise RuntimeError("Checksum dont match",3)
   
    data = superData["data"].decode("base64")

    #print "DEBUG,data" + data

    try:
        operation = simplejson.loads(data)
    except Exception as err:
        raise RuntimeError("Error decoding json",1)

    #Autenticate atm
    #if not operation["auth"] == Gauth:
    #    raise RuntimeError("Auth dont match",3)
   
    #Test for replay atack
    if operation["id"] in OpHist:
        raise RuntimeError("Invalid OpID",3)
    OpHist.append(operation["id"])

    try:
        if operation["mode"] == "n":
            response["card"] = accountCreate(operation["account"],operation["arg"])
            response["code"]=0
        elif operation["mode"] == "d":
            accountDeposit(operation["account"],operation["card"],operation["arg"])
            response["code"]=0
        elif operation["mode"] == "w":
            accountWithdraw(operation["account"],operation["card"],operation["arg"])
            response["code"]=0
        elif operation["mode"] == "g":
            response["balance"] = accountBalance(operation["account"],operation["card"])
            response["code"]=0
        else:
            raise RuntimeError("invalide operation",2)
    except Exception as err:
        #print >> sys.stderr, err
        response["code"]=1

    return getJson(response)


#Bank rotines
Gauth=""
cripto=object()

def parseParameters():
    portSet=False
    authFileSet=False
    port=3000
    auth_file="bank.auth"

    #Test number of args, must be <= 5
    if len(sys.argv) > 5:
        terminateBank()
   
    try:
        opts, args = getopt.getopt(sys.argv[1:], "p:s:")
    except getopt.GetoptError as err:
        terminateBank()

    #Test is there is remain args
    if len(args) > 0:
        terminateBank()
    
    #Validate program parameters
    for o, a in opts:
        if o == "-p":
            #test if parameter is repeted 
            if portSet:
                terminateBank()
            match = re.search(r'^([1-9][0-9]*)$',a)
            if not match:
                terminateBank("Wrong  sintax for port")
            port = int(a)
            if port < 1024 or port > 65535:
                terminateBank("Port must be between 1024 and 65535")
            portmet = True
        elif o == "-s":
            #test if parameter is repeted 
            if authFileSet:
                terminateBank()
            match = re.search(r'(^[_\-\.0-9a-z]*$)',a)
            if not match:
                terminateBank("Wrong sintax for filename")
            match = re.search(r'^\.+$',a)
            if  match:
                terminateBank("Wrong sintax for filename")
            if len(a) > 255:
                terminateBank("Wrong sintax for filename, to long")
            auth_file=a
            authFileSet = True
        else:
            assert False, "unhandled option"
    return (port,auth_file)

def createAuthFile(fileName):
    auth=""
    #print >> sys.stderr, "Creating auth file %s" % fileName
    #check if file exists
    if os.path.isfile(fileName):
        #print >> sys.stderr, "File %s already exists" % fileName
        terminateBank() 
    #create file
    try:
        f = open(fileName,'w')
        auth=id_generator()
        f.write(auth)
        f.close()
    except :
        #print >> sys.stderr, "Problem creating file %s" % fileName
        terminateBank()

    print "created"
    sys.stdout.flush()
    return auth


class ServerHandler(SocketServer.StreamRequestHandler):
    """
    Request Handler class for bank
    """
    timeout = 10
    def handle(self):
        response="{}"
        #do work
        try:
            self.data = self.rfile.readline().strip()
            msg = cripto.decrypt(self.data)
            #print "DEBUG:" + msg
            response = processOperation(msg)
        except Exception as err:
            #print >> sys.stderr, err
            print "protocol_error"
            sys.stdout.flush()
            self.finish()
            return

        #respond to client
        msg = cripto.encrypt(response)
        self.wfile.write(msg + "\n")


def terminate(signal,frame):
    #print "terminate.... "
    global server
    server.socket.close()
    #print "terminate22.... "
    sys.exit(0)

def startBankServer(port):
    #print >> sys.stderr, "Starting server at port %s" % port
    global server 
    global cripto

    try:
       # signal.signal(signal.SIGTERM, terminate)
       # signal.signal(signal.SIGINT, terminate)
        cripto = AESCipher(Gauth)
        SocketServer.TCPServer.allow_reuse_address = True
        server = SocketServer.TCPServer(("localhost",port), ServerHandler)
        signal.signal(signal.SIGINT, terminate)
        server.serve_forever()
        #print >> sys.stderr, "Shuting down server"
        server.server_close()
    except Exception as e:
        #print >> sys.stderr, e
        server.socket.close()
        terminateBank()


def main():
    global Gauth
    (port,authFile) = parseParameters()
    Gauth = createAuthFile(authFile)
    startBankServer(port)
#    accountTest()


if __name__ == "__main__":
    main()
