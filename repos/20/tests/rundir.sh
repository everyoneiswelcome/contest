#!/bin/bash


allfiles=`find $1 -name "*.json" -print | sort -n`

for file in $allfiles; do
    echo $file
    ./run_test.py $file
done
