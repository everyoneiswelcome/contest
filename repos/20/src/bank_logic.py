#!/usr/bin/env python2

from __future__ import print_function

import sys
import json
from bank_common import *


class Bank(object):

    """Provides persistent store for accounts."""

    def __init__(self):
        """default constructor.

        Attributes:
            accounts (dict): Dictionary structure for saving account info
        """
        self.accounts = {}

    def has_account(self, account_name):
        """returns True if an account with account_name exits, else False"""
        return account_name in self.accounts

    def add_account(self, account_name):
        """Adds a new account with the name account_name and a zero balance.

        Args:
            account_name (str): a string containting only /[_\-\.0-9a-z]/ of
            no more that 150 chars

        Attributes:
            accounts[account_name] (dict): Container for individual account info

        Raises:
            BadInputError: if account for account_name already exists
        """
        if account_name in self.accounts:
            raise BadInputError('duplicate account creation attempted')

        self.accounts[account_name] = 0

    def get_balance(self, account_name):
        """Gets the balance in account for account_name

        Args:
            account_name (str): a string containting only /[_\-\.0-9a-z]/ of
                no more that 250 chars

        Attributes:
            accounts[account_name] (BankAccount): account for account_name
        Raises:
            BadInputError: if account for account_name does not exist
        """
        if account_name not in self.accounts:
            raise BadInputError('Account does not exist')
        return round(self.accounts[account_name] / 100.0, 2)

    def deposit(self, account_name, amount):
        """Deposits the amount into the account for account_name.

        Args:
            account_name (str): a string containting only /[_\-\.0-9a-z]/ of
                no more that 250 chars
            amount (int): a quantity to deposit in to the account

        Attributes:
            accounts[account_name] (BankAccount): account for account_name

        Raises:
            BadInputError: if account for account_name does not exist,
                or amount < 0
        """
        if account_name not in self.accounts:
            raise BadInputError('Account does not exist')
        if amount < 0:
            raise BadInputError('Attempted to deposit a negative amount')
        self.accounts[account_name] += int(round(amount * 100.0, 0))

    def withdraw(self, account_name, amount):
        """Withdraws the amount in account for account_name.

        Args:
            account_name (str): a string containting only /[_\-\.0-9a-z]/ of
                no more that 250 chars
            amount (int): a quantity to deposit in to the account

        Attributes:
            accounts[account_name] (BankAccount): account for account_name

        Raises:
            BadInputError: if account for account_name does not exist,
                or if amount < 0
        """
        if account_name not in self.accounts:
            raise BadInputError('Account does not exist')
        if amount < 0:
            raise BadInputError('Attempted to withdraw a positive amount')
        if self.accounts[account_name] - (amount * 100.0) < 0:
            raise BadInputError('Attempted to withdraw more than balance')
        self.accounts[account_name] -= int(round(amount * 100.0, 0))


class BankCommandInterpreter():

    """Interprets and acts on JSON-encoded strings according to the procol"""

    VALID_KEYS = ['account', 'initial_balance', 'balance', 'deposit', 'withdraw']

    def __init__(self, bank=None):
        """ Initializes the BankCommandInterpreter, using a bank if one is
        provided.

        Args:
             bank (Bank): a store for account information

        Attributes:
             bank (Bank): a store for account information
        """
        if bank is None:
            self.bank = Bank()
        else:
            self.bank = bank

    def create_error_response(self, error_code, client_nonce, server_nonce):
        """convenience method to create a valid error response object

        Args:
            error_code (int): an error code to be communicated to the client

        Returns:
            a json-encoded string containing a valid response object to
            communicate the error code to the client
        """
        return json.dumps({'permission': client_nonce, 'granted': server_nonce,
                           'error': error_code})

    def handle_request(self, request_str, expected_client_nonce,
                       expected_server_nonce):
        """interprets remote command.

        Args:
            request_str (str):  A JSON-encoded string containing the command

        Attributes:
             bank (Bank): a store for account information

        Returns:
            a JSON-encoded string containing the response, or an empty string
            for invalid or bad input

        Raises:
            BadInputError: if the request was improperly formatted
            ProtocolError: if a timeout was observed from the time in the
                response object

        Outputs:
            a JSON-encoded string representing the response, if the
            transaction was successful
        """
        (client_nonce, server_nonce, request) = self.parse_request(request_str)

        if client_nonce != expected_client_nonce:
            raise BadInputError('attempted replay')

        if server_nonce != expected_server_nonce:
            raise BadInputError('attempted replay')

        response = {'response': request,
                    'permission': client_nonce,
                    'granted': server_nonce}

        if 'initial_balance' in request:
            self.bank.add_account(request['account'])
            self.bank.deposit(request['account'], request['initial_balance'])
        elif 'balance' in request:
            response['response']['balance'] = self.bank.get_balance(request['account'])
        elif 'deposit' in request:
            self.bank.deposit(request['account'], request['deposit'])
        elif 'withdraw' in request:
            self.bank.withdraw(request['account'], request['withdraw'])
        print(json.dumps(response['response']))
        sys.stdout.flush()

        return json.dumps(response)

    def parse_request(self, request_str):
        """Parses a string request into a JSON object and verifies that
           it contains a possible command.

        Args:
            request_str (str):  A JSON-encoded string containing the command

        Returns:
            a dictionary containing two attributes: 'account' and the valid
            command

        Raises:
            BadInputError: if string cannot be decoded, does not contain an
                account key, does not contain an appropriate command,
                contains other than exactly one valid command, or
                contains more than two keys
        """
        try:
            obj = json.loads(request_str)
        except ValueError:
            raise BadInputError('unable to decode response')

        if len(obj.keys()) != 3:
            raise BadInputError('badly formatted request')

        if 'permission' not in obj:
            raise BadInputError('badly formatted request')

        if 'granted' not in obj:
            raise BadInputError('badly formatted request')

        if 'request' not in obj:
            raise BadInputError('badly formatted request')

        if type(obj['request']) is not dict:
            raise BadInputError('badly formatted request')

        if 'account' not in obj['request']:
            raise BadInputError('account not found in object')

        if len(obj['request'].keys()) != 2:
            raise BadInputError('badly formatted request')

        for key in obj['request']:
            if key not in self.VALID_KEYS:
                raise BadInputError('badly formatted request')

        return (obj['permission'], obj['granted'], obj['request'])


if __name__ == "__main__":
    pass
