from __future__ import print_function
import sys
from re import compile as rec
import constants

##################################################################################################
## terminateApp
## Desc: Centralized exist point
## arg1: error code
## arg2: error message
##################################################################################################
def terminateApp(errorCode, message):
    print(message, file=sys.stderr)
    sys.exit(errorCode)

comp_amount_re = rec(constants.ALLOWED_AMOUNTS)
def checkAmount(opt,arg,parsed_com, oorg_param_processed):
    re_matched = comp_amount_re.match(arg.encode('string-escape'))
    if oorg_param_processed or not re_matched or not ( constants.ALLOWED_AMOUNTS_LIMITS[0] < float(arg) <= constants.ALLOWED_AMOUNTS_LIMITS[1]):
        sys.exit(constants.EXIT_BAD_INPUT)
    else:
        parsed_com[opt]=arg
        return True

comp_file_re = rec(constants.ALLOWED_FILES)
def checkFile(opt,arg,parsed_com, oorg_param_processed):
    re_matched = comp_file_re.match(arg.encode('string-escape'))
    if (arg in constants.DISALLOWED_FILES or not re_matched):
        sys.exit(constants.EXIT_BAD_INPUT)
    else:
        parsed_com[opt]=arg

comp_port_re = rec(constants.ALLOWED_PORTS)
def checkPort(opt,arg,parsed_com, oorg_param_processed):
    re_matched = comp_port_re.match(arg.encode('string-escape'))
    if not re_matched or not ( constants.ALLOWED_PORTS_LIMITS[0] <= int(arg) <= constants.ALLOWED_PORTS_LIMITS[1] ):
        sys.exit(constants.EXIT_BAD_INPUT)
    else:
        parsed_com[opt] = arg

comp_account_re = rec(constants.ALLOWED_ACCOUNTS)
def checkAccount(opt,arg,parsed_com, oorg_param_processed):
    re_matched = comp_account_re.match(arg.encode('string-escape'))
    if not re_matched:
        sys.exit(constants.EXIT_BAD_INPUT)
    else:
        parsed_com[opt]=arg

comp_ip_re = rec(constants.ALLOWED_IPS)
def checkIP(opt,arg,parsed_com, oorg_param_processed):
    re_matched = comp_ip_re.match(arg.encode('string-escape'))
    if not re_matched or arg=="255.255.255.255":
        sys.exit(constants.EXIT_BAD_INPUT)
    else:
        parsed_com[opt] = arg

def checkCurBalance(opt,arg,parsed_com, oorg_param_processed):
    if oorg_param_processed:
        sys.exit(constants.EXIT_BAD_INPUT)
    else:
        parsed_com[opt]=arg
        return True

comp_allchar_re = rec(constants.ALLOWED_CHARACTERS)
def checkSanitary(arg):
    re_matched = comp_allchar_re.match(arg.encode('string-escape'))
    if not re_matched:
        sys.exit(constants.EXIT_BAD_INPUT)
    else:
        return True

def buildChecksDict():
    checksDict = {}
    for param in constants.AMOUNT_GROUP:
        checksDict[param]= checkAmount

    for param in constants.FILE_GROUP:
        checksDict[param]=checkFile

    checksDict[constants.PORT_PARAM]= checkPort
    checksDict[constants.ACCOUNT_PARAM]= checkAccount
    checksDict[constants.IP_PARAM]= checkIP

    return checksDict


def validateCmdLine(argv, profile = False):
    len_argv = len(argv)
    if  len_argv > 12:
        sys.exit(constants.EXIT_BAD_INPUT)

    parsed_com = { }

    checksDict = buildChecksDict()
    oorg_param_processed = False

    i=0

    while i < len_argv:
        checked_param = argv[i]
        param_elem = None
        value_elem = None
        checkSanitary(checked_param)
        try:
            param_elem = checked_param[:2]
            if not param_elem in constants.ATM_KNOWN_PARAMS:
                sys.exit(constants.EXIT_BAD_INPUT)
        except Exception:
             sys.exit(constants.EXIT_BAD_INPUT)

        if param_elem == checked_param:
            if checked_param in constants.ATM_PARAMS_WITH_VALUE:
                if not (i+1 < len_argv):
                    sys.exit(constants.EXIT_BAD_INPUT)
                else:
                    value_elem = argv[i+1]

                    i+=2
            else:
                oorg_param_processed = (checkCurBalance(param_elem,None,parsed_com,oorg_param_processed) or oorg_param_processed)
                i+=1
                continue
        else:
            value_elem = checked_param[2:]
            if param_elem in constants.ATM_PARAMS_WITH_VALUE:
                i+=1
            else:
                sys.exit(constants.EXIT_BAD_INPUT)

        if not param_elem in constants.AMOUNT_GROUP and param_elem in parsed_com.keys():
            sys.exit(constants.EXIT_BAD_INPUT)
        oorg_param_processed = (checksDict[param_elem](param_elem,value_elem,parsed_com,oorg_param_processed) or oorg_param_processed)

    if not oorg_param_processed or not constants.ACCOUNT_PARAM in parsed_com.keys():
        sys.exit(constants.EXIT_BAD_INPUT)

    return parsed_com
