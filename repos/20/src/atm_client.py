#!/usr/bin/env python2

from __future__ import print_function

import socket
from bank_common import *
from constants import *
from authenticated_crypter import AuthenticatedCrypter, InvalidToken
import os
import json

NONCE_SIZE_BYTES = 16


class ATMClient(object):

    """Handles communication with bank servers."""

    def __init__(self, key, ip=DEFAULT_IP_VALUE, port=DEFAULT_PORT_VALUE):
        """Initializes the client.

        Args:
            key (byetstring): a pseudorandom shared key used to encrypt and
                authenticate communications, of length 44.
            ip (str): a pre-validated IP address where the bank server can be
                addressed
            port (int): a port in the range of ALLOWED_PORTS on which
                the bank server is running

        Attributes:
            _crypter (AuthenticatedCrypter): an interface for generating
                authenticated and encrypted communications, using
                AES192/PCKS7/HMAC/SHA-256, and initialized with key
            _ip (str): the saved ip address
            _port (int): the saved port
        Raises:
            ValueError: if key is not of appropriate length
        """
        self._port = port
        self._ip = ip
        self._crypter = AuthenticatedCrypter(key)

    def send(self, payload):
        """Communicates with server.

        Args:
            payload(str):

        Returns:
            a dictionary object containing the response contents, including
            the keys 'account' and one of 'balance', 'deposit', or 'withdraw'

        Raises:
            ProtocolError: if a protocol error was detected by the server, if
                the response was improperly formatted, if decryption failed,
                if the response took more than TIMEOUT_SECS to arrive
                as reckoned by the response object, if a socket timeout
                occurred, or if a socket error occurred
            BadInputError: if a BadInputError was detected by the server.
         """
        response = {}
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.settimeout(TIMEOUT_SECONDS)
            self.sock.connect((self._ip, self._port))

            atm_nonce = os.urandom(NONCE_SIZE_BYTES).encode('base64').strip()
            request = {'request': {'permission': atm_nonce}}
            request = json.dumps(request)
            self.sock.sendall(self._crypter.encrypt(request)[:MAX_PAYLOAD_SIZE])
            response = self._crypter.decrypt(self.sock.recv(MAX_PAYLOAD_SIZE))
            response = json.loads(response)
            if atm_nonce != response['response']['permission']:
                raise ProtocolError('attempted replay')
            bank_nonce = response['response']['granted']

            final_request = {'request': payload,
                             'permission': atm_nonce,
                             'granted': bank_nonce}

            encrypted_data = self._crypter.encrypt(json.dumps(final_request))
            self.sock.sendall(encrypted_data[:MAX_PAYLOAD_SIZE])
            final_response = self._crypter.decrypt(self.sock.recv(MAX_PAYLOAD_SIZE))
            final_response = json.loads(final_response)

            if final_response['granted'] != bank_nonce:
                raise ProtocolError('attempted replay')
            if final_response['permission'].strip() != atm_nonce:
                raise ProtocolError('attempted replay')
            if 'error' in final_response:
                if final_response['error'] == EXIT_BAD_INPUT:
                    raise BadInputError('error detected on server')
                if final_response['error'] == EXIT_PROTOCOL_ERROR:
                    raise ProtocolError('protocol error detected on server')

        except KeyError:
            raise BadInputError('badly formatted response')
        except socket.timeout:
            raise ProtocolError('timeout')
        except socket.error:
            raise ProtocolError('could not communicate with server')
        finally:
            self.sock.close()
        return final_response['response']
