#!/usr/bin/env python2

import sys
import atm_command_validator

def main(argv):
    atm_command_validator.validateCmdLine(argv)

if __name__ == "__main__":
   main(sys.argv[1:])
