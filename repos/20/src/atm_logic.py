#!/usr/bin/env python2

from __future__ import print_function

import os.path
import json
import sys
from constants import *
from bank_common import BadInputError, ProtocolError, debug
from atm_client import ATMClient
from authenticated_crypter import AuthenticatedCrypter, InvalidToken

NEW_ACCOUNT_MINIMUM_AMOUNT = 10.0


class RequestInterpreter(object):

    """Interprets configuration options and sends commands to the bank"""

    VALID_ACTIONS = ['new_balance', 'get_balance', 'deposit', 'withdraw']

    def __init__(self, config):
        """Initializes the RequestInterpeter instance

        Args:
            config (dict): a prevalidated dictionary containing the following:
               required keys:
                   account (str): the name associated with the account

               exclusive keys:
                   new_balance (float): an initial account balance
                   get_balance (None)
                   deposit (float): an amount to deposit
                   withdraw (float): an amount to withdraw

               optional keys:
                   auth_file (str): name of file containing pre-shared keys
                   card (str): name of file to be created to validate account
                       in future calls
                   port (int): a port in the range of ALLOWED_PORTS
                   ip (int): a valid ip address for the server

            Attributes:
                _config: the saved configuration
                _connection: a new ATMClient instance
                _crypter: a new instance of the AuthenticatedCrypter initialised
                    with key from auth_file
            Raises:
                BadInputError: if file named auth_file does not exist
        """
        self._config = config
        self._validate_config()

        if 'auth_file' not in config:
            self._config['auth_file'] = DEFAULT_AUTH_VALUE
        if 'port' not in config:
            self._config['port'] = int(DEFAULT_PORT_VALUE)
        if 'ip' not in config:
            self._config['ip'] = DEFAULT_IP_VALUE
        if 'card' not in config:
            self._config['card'] = self._config['account'] + DEFAULT_CARD_SUFFIX

        self._load_key()
        self._crypter = AuthenticatedCrypter(self._key)
        self._connection = ATMClient(self._key, self._config['ip'],
                                     self._config['port'])

    def _validate_config(self):
        """Ensures that the configuration contains appropriate values.

        Raises:
            BadInputError: if bad configuration is found
        """
        if type(self._config) is not dict:
            raise BadInputError('bad arguments')
        if 'account' not in self._config:
            raise BadInputError('bad arguments')

        actions_found = 0
        for action in self.VALID_ACTIONS:
            if action in self._config:
                actions_found += 1
        if actions_found != 1:
            raise BadInputError('bad arguments')

        if 'new_balance' in self._config and \
                (self._config['new_balance'] < NEW_ACCOUNT_MINIMUM_AMOUNT or
                 self._config['new_balance'] > ALLOWED_AMOUNTS_LIMITS[1]):
            raise BadInputError('bad balance')
        elif 'deposit' in self._config and \
                (self._config['deposit'] < ALLOWED_AMOUNTS_LIMITS[0] or
                 self._config['deposit'] > ALLOWED_AMOUNTS_LIMITS[1]):
            raise BadInputError('bad balance')
        elif 'withdraw' in self._config and \
                (self._config['withdraw'] < ALLOWED_AMOUNTS_LIMITS[0] or
                 self._config['withdraw'] > ALLOWED_AMOUNTS_LIMITS[1]):
            raise BadInputError('bad balance')


    def _check_user_credentials(self):
        """verifies that user account corresponds to provided card.

        Raises:
            BadInputError: if card file does not exist, if card could not be
                decrypted, or if the account name stored in card does not
                match the value self._config.['account']
        """
        if not os.path.exists(self._config['card']):
            raise BadInputError('no card')

        cardfile = open(self._config['card'], 'r')
        try:
            decrypted_card = self._crypter.decrypt(cardfile.read())
        except InvalidToken:
            raise BadInputError('bad card')
        cardfile.close()
        if decrypted_card != self._config['account']:
            raise BadInputError('bad card')

    def _create_card(self):
        """Creates a new card for the given user.

        Raises:
            BadInputError: if a file by the given name already exists.
        """
        if os.path.exists(self._config['card']):
            raise BadInputError('card file already exists')
        cardfile = open(self._config['card'], 'w')
        cardfile.write(self._crypter.encrypt(self._config['account']))
        cardfile.close()

    def _remove_card(self):
        """removes the card from the filesystem.

        Preconditions:
            the card has been checked by either self._create_card() or by
            self._check_user_credentials() to verify that it belongs to
            the account holder

        Raises:
            BadInputError if the card file does not exist.
        """
        if not os.path.exists(self._config['card']):
            raise BadInputError('card file does not exist')
        os.remove(self._config['card'])

    def _load_key(self):
        """Loads shared symmetric encryption key from auth_file.

        Raises:
            BadInputError: if the auth file does not exist.
        """
        if not os.path.exists(self._config['auth_file']):
            raise BadInputError('auth file does not exist')
        fh = open(self._config['auth_file'], 'r')
        self._key = fh.read().strip()
        fh.close()

    def process_request(self):
        """Generates a request object from the configuration and sends it
           to the bank.

        Determines the appropriate action based on what values are present in
        the configuration, and creates a request object to send to the server.
        Instructs the server to send the request to the bank, and then prints
        out the response.

        Outputs:
            a JSON-encoded string corresponding to the repsonse from the bank,
            if successful.

        Raises:
            ProtocolError: if a protocol error was detected by the server, if
                the response was improperly formatted, if decryption failed,
                if the response took more than TIMEOUT_SECS to arrive
                as reckoned by the response object, if a socket timeout
                occurred, or if a socket error occurred
            BadInputError: if a BadInputError was detected by the server.
        """
        request = {'account': self._config['account']}
        if 'new_balance' in self._config:
            self._create_card()
            request['initial_balance'] = self._config['new_balance']
        else:
            self._check_user_credentials()
            if 'deposit' in self._config:
                request['deposit'] = self._config['deposit']
            elif 'withdraw' in self._config:
                request['withdraw'] = self._config['withdraw']
            elif 'get_balance' in self._config:
                request['balance'] = 0
        try:
            response = self._connection.send(request)
        except BadInputError as e:
            if 'new_balance' in self._config:
                self._remove_card()
            raise BadInputError(e.value)
        except ProtocolError as e:
            if 'new_balance' in self._config:
                self._remove_card()
            raise ProtocolError(e.value)

        print(json.dumps(response))
        sys.stdout.flush()


if __name__ == "__main__":
    try:
        config = {'account': 'bob', 'new_balance': 10.3}
        RequestInterpreter(config).process_request()
        config = {'account': 'bob', 'deposit': 7.55}
        RequestInterpreter(config).process_request()
        config = {'account': 'bob', 'withdraw': 8.81}
        RequestInterpreter(config).process_request()
        config = {'account': 'bob', 'get_balance': 0}
        RequestInterpreter(config).process_request()
    except BadInputError as e:
        debug(e.value)
        sys.exit(EXIT_BAD_INPUT)
    except ProtocolError as e:
        debug(e.value)
        sys.exit(EXIT_PROTOCOL_ERROR)
