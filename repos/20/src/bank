#!/usr/bin/env python2

from __future__ import print_function
import sys, re, hashlib, hmac, socket, json, os
import signal
from decimal import Decimal
from Crypto.Cipher import AES


EXIT_BAD_INPUT = 255
EXIT_KILLED_CLEANLY = 0
EXIT_PROTOCOL_ERROR = 63

AUTH_PARAM="-s"
PORT_PARAM="-p"

BANK_KNOWN_PARAMS=[AUTH_PARAM,PORT_PARAM]

DEFAULT_AUTH_VALUE="bank.auth"
DEFAULT_PORT_VALUE="3000"

ALLOWED_CHARACTERS="^([_\-\.0-9a-z]{1,257})$"
ALLOWED_FILE_RANGE=[1,255]
ALLOWED_FILES="^([_\-\.0-9a-z]{%s,%s})$" %(str(ALLOWED_FILE_RANGE[0]),str(ALLOWED_FILE_RANGE[1]))
DISALLOWED_FILES=[ ".", ".."]
ALLOWED_PORTS_LIMITS=[1024,65535]
ALLOWED_PORTS="^([1-9][0-9]{3,4})$"


MAX_BALANCE = sys.maxint
TIMEOUT_SECONDS = 10
MAX_PAYLOAD_SIZE = 4096
NONCE_SIZE_BYTES = 16

AES_BLOCK_SIZE = 16
AES_KEY_SIZE = 128
SIG_SIZE = hashlib.sha256().digest_size



def debug(message):
    # print("DEBUG: ", message, file=sys.stderr)
    pass


class BadInputError(Exception):

    def __init__(self, value=''):
        self.value = value

    def __str__(self):
        return repr(self.value)


class ProtocolError(Exception):

    def __init__(self, value=''):
        self.value = value

    def __str__(self):
        return repr(self.value)


comp_file_re = re.compile(ALLOWED_FILES)
def checkFile(opt,arg,parsed_com):
    re_matched = comp_file_re.match(arg.encode('string-escape'))
    if (not re_matched or arg in DISALLOWED_FILES):
        sys.exit(EXIT_BAD_INPUT)
    else:
        parsed_com[opt]=arg

comp_port_re = re.compile(ALLOWED_PORTS)
def checkPort(opt,arg,parsed_com):
    re_matched = comp_port_re.match(arg.encode('string-escape'))
    if not re_matched or not ( ALLOWED_PORTS_LIMITS[0] <= int(arg) <= ALLOWED_PORTS_LIMITS[1] ):
        sys.exit(EXIT_BAD_INPUT)
    else:
        parsed_com[opt] = arg

comp_allchar_re = re.compile(ALLOWED_CHARACTERS)
def checkSanitary(arg):
    re_matched = comp_allchar_re.match(arg.encode('string-escape'))
    if arg and not re_matched:
        sys.exit(EXIT_BAD_INPUT)
    else:
        return True

def checkDuplicates(checked_param,parsed_com):
    if checked_param in parsed_com.keys():
        sys.exit(EXIT_BAD_INPUT)

def validateCmdLine(argv):
    if len(argv) > 4:
        sys.exit(EXIT_BAD_INPUT)

    parsed_com = { }

    i=0
    while i < len(argv):
        checked_param = argv[i]
        param_elem = None
        value_elem = None
        checkSanitary(checked_param)
        try:
            param_elem = checked_param[:2]
            if not param_elem in BANK_KNOWN_PARAMS:
                sys.exit(EXIT_BAD_INPUT)
        except Exception:
             sys.exit(EXIT_BAD_INPUT)

        if param_elem == checked_param:
            if not i+1 < len(argv):
                    sys.exit(EXIT_BAD_INPUT)
            else:
                value_elem = argv[i+1]
                i+=2
        else:
            value_elem = checked_param[2:]
            i+=1

        checkDuplicates(checked_param,parsed_com)
        if param_elem == PORT_PARAM:
            checkPort(param_elem,value_elem,parsed_com)
        else:
            checkFile(param_elem,value_elem,parsed_com)

    param_keys = parsed_com.keys()
    if not AUTH_PARAM in param_keys:
        parsed_com[AUTH_PARAM]= DEFAULT_AUTH_VALUE

    if not PORT_PARAM in param_keys:
        parsed_com[PORT_PARAM]=DEFAULT_PORT_VALUE

    return parsed_com


class InvalidToken(Exception):

    def __init__(self, value=''):
        self.value = value

    def __str__(self):
        return repr(self.value)


class AuthenticatedCrypter():

    def __init__(self, key):
        if len(key) != (AES_KEY_SIZE / 8 + SIG_SIZE):
            raise InvalidToken('bad key!')
        self._aes_key = key[:AES_KEY_SIZE / 8]
        self._hmac_key = key[AES_KEY_SIZE / 8:]

    @staticmethod
    def generate_key():
        return os.urandom(AES_KEY_SIZE / 8 + SIG_SIZE)

    def encrypt(self, data):
        pad = AES_BLOCK_SIZE - len(data) % AES_BLOCK_SIZE
        padded_data = padded_data = data +  pad *chr(pad)
        iv = os.urandom(AES_BLOCK_SIZE)
        cipher = AES.new(self._aes_key, AES.MODE_CBC, iv)
        encrypted_data = iv + cipher.encrypt(padded_data)
        sig = hmac.new(self._hmac_key, encrypted_data, hashlib.sha256).digest()
        return encrypted_data + sig

    def decrypt(self, signed_encrypted_data):
        data_len = len(signed_encrypted_data) - SIG_SIZE
        encrypted_data = signed_encrypted_data[:data_len]
        signature = signed_encrypted_data[data_len:]
        computed_sig = hmac.new(self._hmac_key, encrypted_data,
                                hashlib.sha256).digest()
        if not self.safe_equals(computed_sig, signature):
            raise InvalidToken('Bad signature')
        iv = encrypted_data[:AES_BLOCK_SIZE]
        encrypted_data = encrypted_data[AES_BLOCK_SIZE:]
        data = AES.new(self._aes_key, AES.MODE_CBC, iv).decrypt(encrypted_data)
        return data[:-ord(data[-1])]

    def safe_equals(self, a, b):
        if len(a) != len(b):
            return False

        result = 0
        for x, y in zip(a, b):
            result |= ord(x) ^ ord(y)
        return result == 0


class Bank(object):

    def __init__(self):
        self.accounts = {}
        self.d100 = Decimal('100')

    def has_account(self, account_name):
        return account_name in self.accounts

    def add_account(self, account_name, amount):
        if account_name in self.accounts:
            raise BadInputError('duplicate account creation attempted')

        self.accounts[account_name] = Decimal(amount)

    def get_balance(self, account_name):
        if account_name not in self.accounts:
            raise BadInputError('Account does not exist')
        return round(self.accounts[account_name],2)

    def deposit(self, account_name, amount):
        if account_name not in self.accounts:
             raise BadInputError('Account does not exist')
        if amount < 0:
             raise BadInputError('Attempted to deposit a negative amount')
        self.accounts[account_name] += Decimal(amount)

    def withdraw(self, account_name, amount):
        if account_name not in self.accounts:
            raise BadInputError('Account does not exist')
        if amount < 0:
            raise BadInputError('Attempted to withdraw a positive amount')
        b = self.accounts[account_name] - Decimal(amount)
        if b < 0:
            raise BadInputError('Attempted to withdraw more than balance')
        self.accounts[account_name] = b


class BankCommandInterpreter():

    VALID_KEYS = ['account', 'initial_balance', 'balance', 'deposit', 'withdraw']

    def __init__(self, bank=None):
        if bank is None:
            self.bank = Bank()
        else:
            self.bank = bank

    def create_error_response(self, error_code, client_nonce, server_nonce):
        return json.dumps({'permission': client_nonce, 'granted': server_nonce,
                           'error': error_code})

    def handle_request(self, request_str, expected_client_nonce,
                       expected_server_nonce):
        (client_nonce, server_nonce, request) = self.parse_request(request_str)

        if client_nonce != expected_client_nonce:
            raise BadInputError('attempted replay')

        if server_nonce != expected_server_nonce:
            raise BadInputError('attempted replay')

        response = {'response': request,
                    'permission': client_nonce,
                    'granted': server_nonce}

        if 'initial_balance' in request:
            self.bank.add_account(request['account'], request['initial_balance'])
            response['response']['initial_balance'] = float(response['response']['initial_balance'])

        elif 'balance' in request:
            response['response']['balance'] = self.bank.get_balance(request['account'])

        elif 'deposit' in request:
            self.bank.deposit(request['account'], request['deposit'])
            response['response']['deposit'] = float(response['response']['deposit'])

        elif 'withdraw' in request:
            self.bank.withdraw(request['account'], request['withdraw'])
            response['response']['withdraw'] = float(response['response']['withdraw'])

        print(json.dumps(response['response']))
        sys.stdout.flush()

        return json.dumps(response)


    def parse_request(self, request_str):
        try:
            obj = json.loads(request_str)
        except ValueError:
            raise BadInputError('unable to decode response')

        if len(obj.keys()) != 3:
            raise BadInputError('badly formatted request')

        if 'permission' not in obj:
            raise BadInputError('badly formatted request')

        if 'granted' not in obj:
            raise BadInputError('badly formatted request')

        if 'request' not in obj:
            raise BadInputError('badly formatted request')

        if type(obj['request']) is not dict:
            raise BadInputError('badly formatted request')

        if 'account' not in obj['request']:
            raise BadInputError('account not found in object')

        if len(obj['request'].keys()) != 2:
            raise BadInputError('badly formatted request')

        for key in obj['request']:
            if key not in self.VALID_KEYS:
                raise BadInputError('badly formatted request')

        return (obj['permission'], obj['granted'], obj['request'])


class BankServer():

    def __init__(self, auth_file, port):
        self._auth_file = auth_file
        self._port = port
        self._interpreter = BankCommandInterpreter()

        self._key = AuthenticatedCrypter.generate_key()
        self._crypter = AuthenticatedCrypter(self._key)
        self.create_auth_file()

    def create_auth_file(self):
        if os.path.exists(self._auth_file):
            raise BadInputError('Authorization file already exists')
        afh = open(self._auth_file, 'w')
        afh.write(self._key)
        afh.close()
        print('created')
        sys.stdout.flush()

    def encrypt(self, data):
        return self._crypter.encrypt(data)

    def decrypt(self, data):
        return self._crypter.decrypt(data)

    def run(self):
        self._s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        try:
            self._s.bind(('127.0.0.1', self._port))
        except socket.error:
            debug('server already running on this port.')
            sys.exit(1)

        self._s.listen(1)

        try:
            while 1:
                try:
                    (self._conn, self._addr) = self._s.accept()
                    self._conn.settimeout(TIMEOUT_SECONDS)
                    data = self._conn.recv(MAX_PAYLOAD_SIZE)
                    response = self.data_received(data)
                    self._conn.send(response)
                except socket.timeout:
                    print('protocol_error')
                    sys.stdout.flush()
        except socket.error:
            pass

        self.stop()

    def data_received(self, data):
        atm_nonce = ''
        bank_nonce = ''

        try:
            if not data:
                raise ProtocolError('no data')

            request = json.loads(self._crypter.decrypt(data))
            response = {'response': request['request']}
            bank_nonce = os.urandom(NONCE_SIZE_BYTES).encode('base64').strip()
            atm_nonce = request['request']['permission']
            response['response']['granted'] = bank_nonce
            self._conn.send(self._crypter.encrypt(json.dumps(response)))

            data = self._conn.recv(MAX_PAYLOAD_SIZE)

            if not data:
                raise ProtocolError('no data')

            decrypted_data = self.decrypt(data)
            response = self._interpreter.handle_request(decrypted_data,
                                                        atm_nonce,
                                                        bank_nonce)
        except BadInputError as e:
            response = self._interpreter.create_error_response(255,
                                                               atm_nonce,
                                                               bank_nonce)
        except ProtocolError as e:
            print('protocol_error')
            sys.stdout.flush()
            response = self._interpreter.create_error_response(63,
                                                               atm_nonce,
                                                               bank_nonce)
        except InvalidToken:
            print('protocol_error')
            sys.stdout.flush()
            response = self._interpreter.create_error_response(63,
                                                               atm_nonce,
                                                               bank_nonce)
        except KeyError:
            print('protocol_error')
            sys.stdout.flush()
            response = self._interpreter.create_error_response(63,
                                                               atm_nonce,
                                                               bank_nonce)


        return self.encrypt(response)[0:MAX_PAYLOAD_SIZE]

    def stop(self):
        try:
            os.remove(self._auth_file)
        except OSError:
            pass
        if hasattr(self, '_conn'):
            self._conn.close()
        if hasattr(self, '_s'):
            self._s.close()

def cleanup(signum, frame):
    global server
    if server is not None:
        server.stop()


if __name__ == "__main__":
    global server
    server = None
    try:
        options = validateCmdLine(sys.argv[1:])
        server = BankServer(options['-s'], int(options['-p']))
        signal.signal(signal.SIGHUP, cleanup)
        signal.signal(signal.SIGTERM, cleanup)
        signal.signal(signal.SIGINT, cleanup)
        server.run()
    except BadInputError as e:
        if server is not None:
            server.stop()
        sys.exit(EXIT_BAD_INPUT)
    except KeyboardInterrupt as e:
        server.stop()
