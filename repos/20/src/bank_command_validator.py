from __future__ import print_function
import sys, re

import constants

##################################################################################################
## terminateApp
## Desc: Centralized exist point
## arg1: error code
## arg2: error message
##################################################################################################
def terminateApp(errorCode, message):
    print(message, file=sys.stderr)
    sys.exit(errorCode)

##################################################################################################
## intNum
## Desc: returns the value of s or false if it isn' an int. Extra sec point to avoid crash.
## arg1: string s to be tested
##################################################################################################
def intNum(s):
    try:
        return int(s)
    except ValueError:
        return False

def addParam(list_container,param,value):
    if any(param for item in list_container if item[0]==param):
        terminateApp(constants.EXIT_BAD_INPUT,"Duplicated parameter.")
    else:
        list_container.append((param,value))

##################################################################################################
## parseComLine
## Desc: Checks the validity of the command. Returns the command as dict or exits with error.
## arg1: argv command line
##################################################################################################

def validateCmdLine(argv):
    if len(argv) > 4:
        terminateApp(constants.EXIT_BAD_INPUT,"Too many arguments.")

    parsed_com = { constants.AUTH_PARAM:constants.DEFAULT_AUTH_VALUE,constants.PORT_PARAM:constants.DEFAULT_PORT_VALUE}

    parsedParams = []
    i=0
    while i < len(argv):
        trimmed_param = argv[i].strip()

        if not re.match(constants.ALLOWED_CHARACTERS, trimmed_param.encode('string-escape')) or re.match(constants.ALLOWED_CHARACTERS, trimmed_param.encode('string-escape')).group()!=trimmed_param or not re.match(constants.BANK_ALLOWED_PARAMS, trimmed_param):
            terminateApp(constants.EXIT_BAD_INPUT,"Unknown parameter or character.")

        if len(trimmed_param) == 2:
            if not (i+1 < len(argv)) or not len(argv[i+1].strip()) or not re.match(constants.ALLOWED_CHARACTERS,(argv[i+1].strip())):
                terminateApp(constants.EXIT_BAD_INPUT,"Parameter requires value.")
            else:
                addParam(parsedParams,trimmed_param,argv[i+1].strip())
                i+=2
                continue
        else:
            addParam(parsedParams,trimmed_param[:2],trimmed_param[2:])
            i+=1
            continue

    params = dict(parsedParams)

    # Token indicating if more than one OneOf required parameters is found.
    for opt, arg in params.iteritems():
        if not arg or (arg and (not re.match(constants.ALLOWED_CHARACTERS, arg.encode('string-escape')) or re.match(constants.ALLOWED_CHARACTERS, arg.encode('string-escape')).group()!=arg)):
            terminateApp(constants.EXIT_BAD_INPUT,"Illegal character in value.")

        # Checks that provided port is an int, is superior to lower limit and inferior to upper limit.
        if opt == constants.PORT_PARAM:
            if (not intNum(arg.strip()) or intNum(arg.strip()) < constants.ALLOWED_PORTS_LIMITS[0] or intNum(arg.strip()) > constants.ALLOWED_PORTS_LIMITS[1] ):
                terminateApp(constants.EXIT_BAD_INPUT,"Bad port error.")
            else:
                parsed_com[opt] = arg.strip()
                continue

        # Checks that provided file name is not a disallowed one and matches allowed the pattern.
        else:
            if (arg in constants.DISALLOWED_FILES or not re.match(constants.ALLOWED_FILES,arg.strip()) or re.match(constants.ALLOWED_FILES,arg.strip()).group() != arg.strip()):
                terminateApp(constants.EXIT_BAD_INPUT,"Bad file name.")
            else:
                parsed_com[opt]=arg.strip()
                continue

    print("parsed_com",file=sys.stderr)
    for elem in parsed_com.iteritems():
        print(elem,file=sys.stderr)
    print("done",file=sys.stderr)

    return parsed_com
