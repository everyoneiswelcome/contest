#!/usr/bin/env python2

from __future__ import print_function

import sys

MAX_BALANCE = sys.maxint

TIMEOUT_SECONDS = 10
MAX_PAYLOAD_SIZE = 4096


def debug(message):
    # print("DEBUG: ", message, file=sys.stderr)
    pass


class BadInputError(Exception):

    def __init__(self, value=''):
        self.value = value

    def __str__(self):
        return repr(self.value)


class ProtocolError(Exception):

    def __init__(self, value=''):
        self.value = value

    def __str__(self):
        return repr(self.value)
