#!/usr/bin/env python2

from __future__ import print_function

import subprocess
import time
import sys


PROG = './bank'

SERVICE_KILLED_CLEANLY = 0
SERVICE_BAD_INPUT = 255
SERVICE_PROTOCOL_ERROR = 63

DEFAULT_AUTH = 'bank.auth'
DEFAULT_PORT = "3000"


def setup_func():
    "set up test fixtures"
    pass


def teardown_func():
    "tear down test fixtures"
    pass


def run_cmd(command):
    output = ''
    exit_code = 0
    try:
        output = subprocess.check_output(command, shell=True)
    except subprocess.CalledProcessError as c:
        exit_code = c.returncode
    return (exit_code, output)


def debug(message):
    print("DEBUG: ", message, file=sys.stderr)


def run_service(command):
    if type(command) == str:
        command = command.split()
    output = ''
    pipe = subprocess.Popen(command)
    time.sleep(0.5)
    try:
        if not pipe.poll():
            pipe.terminate()
            time.sleep(2)
            if not pipe.poll():
                pipe.terminate()
    except OSError:
        pass
    return (pipe.returncode, output)


def test_no_args():
    args = [PROG]
    (exit_code, output) = run_service(args)
    debug(exit_code)
    assert(exit_code == SERVICE_KILLED_CLEANLY)


def test_port_valid():
    args = [PROG, '-s', DEFAULT_AUTH, '-p', '4000']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)


def test_port_1024():
    args = [PROG, '-s', DEFAULT_AUTH, '-p', '1024']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)


def test_port_65535():
    args = [PROG, '-s', DEFAULT_AUTH, '-p', '65535']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)


def test_no_default_port():
    args = [PROG, '-s', DEFAULT_AUTH]
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)


def test_port_1023():
    args = [PROG, '-s', DEFAULT_AUTH, '-p', '1023']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)


def test_port_65536():
    args = [PROG, '-s', DEFAULT_AUTH, '-p', '65536']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)


def test_port_leading_zero():
    args = [PROG, '-s', DEFAULT_AUTH, '-p', '03000']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)


def test_port_valid_but_in_hex():
    args = [PROG, '-s', DEFAULT_AUTH, '-p', '0xbb8']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)


def test_port_invalid_float():
    args = [PROG, '-s', DEFAULT_AUTH, '-p', '3000.0']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)


def test_port_invalid_chars():
    args = [PROG, '-s', DEFAULT_AUTH, '-p', '3000a']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)


def test_duplicate_parameters():
    args = [PROG, '-p', '4000', '-p', '4000']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)

    args = [PROG, '-p', '4000', '-p3000']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)

    args = [PROG, '-p4000', '-p']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)


def test_duplicate_parameters2():
    args = [PROG, '-p', '4000', '-p3000']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)

def test_unspecified_parameters_flag():
    args = [PROG, '-s', DEFAULT_AUTH, '-p', DEFAULT_PORT, '-o']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)


def test_unspecified_parameters_option_argument():
    args = [PROG, '-s', DEFAULT_AUTH, '-p', DEFAULT_PORT, '-r', 'extra']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)


def test_port_flag_only():
    args = [PROG, '-s', DEFAULT_AUTH, '-p']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)


def test_auth_file_flag_only():
    args = [PROG, '-p', DEFAULT_PORT, '-s']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)


def test_auth_file_port_flag_only():
    args = [PROG, '-p', '-s']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)


def test_auth_file_port_flag_only():
    args = [PROG, '-p', '-s']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)

def test_auth_file_p():
    args = [PROG, '-s-p', '-p3000']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)

def test_bad_auth_file():
    args = [PROG, '-s.']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)

    args = [PROG, '-s..']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)

    args = [PROG, '-s']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_BAD_INPUT)

def test_special_auth_file():

    args = [PROG, '-s-']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)

    args = [PROG, '-s', '-']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)

    args = [PROG, '-s--']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)

    args = [PROG, '-s', '--']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)

    args = [PROG, '-s-s']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)

    args = [PROG, '-s', '-s']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)

    args = [PROG, '-s_']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)

    args = [PROG, '-s', '_']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)

    args = [PROG, '-s-p']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)

    args = [PROG, '-s', '-p']
    (exit_code, output) = run_service(args)
    assert(exit_code == SERVICE_KILLED_CLEANLY)

#def test_atm_bank_auth_file():
#
#    args = [PROG, '-s', 'atm']
#    (exit_code, output) = run_service(args)
#    assert(exit_code == SERVICE_KILLED_CLEANLY)
#
#    args = [PROG, '-s', 'bank']
#    (exit_code, output) = run_service(args)
#    assert(exit_code == SERVICE_KILLED_CLEANLY)


