#!/usr/bin/env python2

from __future__ import print_function

from Crypto.Cipher import AES
import hashlib
import os
import hmac
import sys

AES_BLOCK_SIZE = 16
AES_KEY_SIZE = 128
SIG_SIZE = hashlib.sha256().digest_size


def debug(message):
    print("DEBUG: ", message, file=sys.stderr)


class InvalidToken(Exception):

    def __init__(self, value=''):
        self.value = value

    def __str__(self):
        return repr(self.value)


class AuthenticatedCrypter():

    def __init__(self, key):
        if len(key) != (AES_KEY_SIZE / 8 + SIG_SIZE):
            raise InvalidToken('bad key!')
        self._aes_key = key[:AES_KEY_SIZE / 8]
        self._hmac_key = key[AES_KEY_SIZE / 8:]

    @staticmethod
    def generate_key():
        return os.urandom(AES_KEY_SIZE / 8 + SIG_SIZE)

    def encrypt(self, data):
        pad = AES_BLOCK_SIZE - len(data) % AES_BLOCK_SIZE
        padded_data = data + pad * chr(pad)
        iv = os.urandom(AES_BLOCK_SIZE)
        cipher = AES.new(self._aes_key, AES.MODE_CBC, iv)
        encrypted_data = iv + cipher.encrypt(padded_data)
        sig = hmac.new(self._hmac_key, encrypted_data, hashlib.sha256).digest()
        return encrypted_data + sig

    def decrypt(self, signed_encrypted_data):
        data_len = len(signed_encrypted_data) - SIG_SIZE
        encrypted_data = signed_encrypted_data[:data_len]
        signature = signed_encrypted_data[data_len:]
        computed_sig = hmac.new(self._hmac_key, encrypted_data,
                                hashlib.sha256).digest()
        if not self.safe_equals(computed_sig, signature):
            raise InvalidToken('Bad signature')
        iv = encrypted_data[:AES_BLOCK_SIZE]
        encrypted_data = encrypted_data[AES_BLOCK_SIZE:]
        data = AES.new(self._aes_key, AES.MODE_CBC, iv).decrypt(encrypted_data)
        return data[:-ord(data[-1])]

    def safe_equals(self, a, b):
        if len(a) != len(b):
            return False

        result = 0
        for x, y in zip(a, b):
            result |= ord(x) ^ ord(y)
        return result == 0


if __name__ == "__main__":
    data = 'secret data!'
    if len(sys.argv) > 1:
        data = sys.argv[1]
    key = AuthenticatedCrypter.generate_key()
    crypter = AuthenticatedCrypter(key)
    encrypted_data = crypter.encrypt(data)

    print(encrypted_data.encode('base64'))

    decrypted_data = crypter.decrypt(encrypted_data)
    if data != decrypted_data:
        print('Encryption / decryption failed.')
    else:
        print('Encryption / decryption succeeded! contents were ' +
              decrypted_data)
