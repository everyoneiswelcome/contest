#!/usr/bin/env python2

from __future__ import print_function

from unittest import TestCase
from authenticated_crypter import AuthenticatedCrypter
import json
import sys
import time
import deep_eq
import os
import socket
from subprocess import Popen, PIPE, check_output, CalledProcessError


PROG = './bank'
AUTH_FILE = 'bank.auth'
DEFAULT_PORT = 3000
TIMEOUT_SECONDS = 10
MAX_PAYLOAD_SIZE = 4096
NONCE_SIZE_BYTES = 16

def setup_func():
    "set up test fixtures"
    pass


def teardown_func():
    "tear down test fixtures"
    pass


def debug(message):
    print("DEBUG: ", message, file=sys.stderr)


def run_service(command):
    if type(command) == str:
        command = command.split()
    output = ''
    pipe = Popen(command)
    time.sleep(2)
    try:
        if not pipe.poll():
            pipe.terminate()
            time.sleep(1)
            if not pipe.poll():
                pipe.kill()
    except OSError:
        pass
    return (pipe.returncode, output)


def start_service(command):
    if type(command) == str:
        command = command.split()
    pipe = Popen(command, stdout=PIPE, stdin=PIPE)
    pipe.stdin.close()
    time.sleep(1)
    return pipe


def stop_service(pipe):
    try:
        if not pipe.poll():
            pipe.terminate()
            time.sleep(2)
            if not pipe.poll():
                pipe.terminate()
    except OSError:
        pass
    return (pipe.returncode)


def run_cmd(command):
    output = ''
    exit_code = 0
    try:
        if type(command) == str:

            output = check_output(command, shell=True)
        else:
            output = check_output(command)
    except CalledProcessError as c:
        exit_code = c.returncode
    return (exit_code, output)


def send_data(data, port=DEFAULT_PORT, data_chunk_size=4096, timeout=10):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(timeout)
    sock.connect(('127.0.0.1', DEFAULT_PORT))
    response = ''

    try:
        sock.sendall(data)
        response = sock.recv(data_chunk_size)
    except socket.timeout:
        response = 'timeout'
    finally:
        sock.close()

    return response


def get_file_contents(filename):
    fh = open(filename, 'r')
    contents = fh.read()
    fh.close()
    return contents


def send_object_encrypted(data, auth_file=AUTH_FILE, port=DEFAULT_PORT,
                          data_chunk_size=4096, timeout=10):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(timeout)
    sock.connect(('127.0.0.1', DEFAULT_PORT))
    response = ''

    try:
        key = get_file_contents(auth_file)
        crypter = AuthenticatedCrypter(key)
        atm_nonce = os.urandom(NONCE_SIZE_BYTES).encode('base64').strip()
        request = {'request': {'permission': atm_nonce}}
        sock.sendall(crypter.encrypt(json.dumps(request)))
        response = sock.recv(data_chunk_size)
        response = json.loads(crypter.decrypt(response))

        if response['response']['permission'] == atm_nonce:
            server_nonce = response['response']['granted']
            request = data
            request['permission'] = atm_nonce
            request['granted'] = server_nonce

            sock.sendall(crypter.encrypt(json.dumps(request)))
            response = sock.recv(data_chunk_size)
            response = json.loads(crypter.decrypt(response))
            if response['permission'] != atm_nonce or \
                    response['granted'] != server_nonce:
                response = {'error': 63}
        else:
            response = {'error': 63}

    except socket.timeout:
        response = 'timeout'
    finally:
        sock.close()

    return response


class TestSendData(TestCase):

    @classmethod
    def setup_class(self):
        self.pipe = start_service([PROG])

    @classmethod
    def teardown_class(self):
        os.remove(AUTH_FILE)
        stop_service(self.pipe)

    def check_expected_output(self, input_str, output_str):
        input_object = json.loads(input_str)
        output_object = json.loads(output_str)
        response_object = send_object_encrypted(input_object)

        if 'error' not in response_object or response_object['error'] == 255:
            self.pipe.stdout.readline().strip()

        del response_object['granted']
        del response_object['permission']
        assert(deep_eq.deep_eq(output_object, response_object))

    def test_auth_file_statement(self):
        assert(self.pipe.stdout.readline() == "created\n")

    def test_auth_file_created(self):
        import os.path
        assert(os.path.exists(AUTH_FILE))

    def test_send_bad_communication(self):
        raw_data_obj = {"request": None}
        response = send_object_encrypted(raw_data_obj)
        assert(response['error'] == 255)

    def test_send_data_chunk_limit(self):
        send_data(4096 * 'a')
        assert(self.pipe.stdout.readline() == "protocol_error\n")

    def test_send_good_communication(self):
        account = 'bob_good'
        raw_data_obj = {'request': {'account': account, 'initial_balance': 10.1}}
        response = send_object_encrypted(raw_data_obj)

        assert('error' not in response)
        assert(response['response']['account'] == account)
        output = self.pipe.stdout.readline()
        assert(json.loads(output)['account'] == account)
        assert(json.loads(output)['initial_balance'] == 10.1)

    def test_replay_attack_against_server(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(TIMEOUT_SECONDS)
        sock.connect(('127.0.0.1', DEFAULT_PORT))

        try:
            crypter = AuthenticatedCrypter(get_file_contents(AUTH_FILE))
            client_nonce = os.urandom(NONCE_SIZE_BYTES).encode('base64').strip()
            request1 = {'request': {'permission': client_nonce}}
            sock.sendall(crypter.encrypt(json.dumps(request1)))
            response1 = sock.recv(MAX_PAYLOAD_SIZE)
            response1 = json.loads(crypter.decrypt(response1))

            request2 = {'request': {'account': 'bob', 'initial_balance': 10.1}}
            request2['permission'] = client_nonce
            request2['granted'] = response1['response']['granted']

            sock.sendall(crypter.encrypt(json.dumps(request2)))
            response2 = sock.recv(MAX_PAYLOAD_SIZE)
            response2 = json.loads(crypter.decrypt(response2))

            # ensure that the original transaction succeeded
            assert('error' not in response2)
            assert(response2['response']['account'] == 'bob')
            output = self.pipe.stdout.readline().strip()
            assert(json.loads(output)['account'] == 'bob')
            assert(json.loads(output)['initial_balance'] == 10.1)

            # start the replay
            sock.close()
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(TIMEOUT_SECONDS)
            sock.connect(('127.0.0.1', DEFAULT_PORT))

            # send the original nonce request
            sock.sendall(crypter.encrypt(json.dumps(request1)))
            sock.recv(MAX_PAYLOAD_SIZE)
            sock.sendall(crypter.encrypt(json.dumps(request2)))
            response4 = sock.recv(MAX_PAYLOAD_SIZE)
            response4 = json.loads(crypter.decrypt(response4))
            assert('error' in response4)

        except socket.timeout:
            debug('timeout')
        finally:
            sock.close()

    def test_send_large_data(self):
        send_data(4097 * 'a')
        assert(self.pipe.stdout.readline() == "protocol_error\n")

    def test_send_impossibly_large_data(self):
        send_data(10000000 * 'a')
        assert(self.pipe.stdout.readline() == "protocol_error\n")

    def test_send_no_data(self):
        assert(send_data('') == 'timeout')
        assert(self.pipe.stdout.readline() == "protocol_error\n")

    def test_send_valid_data_size_bad_contents(self):
        send_data(1000 * 'a')
        assert(self.pipe.stdout.readline() == "protocol_error\n")

    def test_new_account_creation(self):
        input_obj = {"request": {"account": "newbie", "initial_balance": 10.30}}
        input_str = json.dumps(input_obj)

        output_obj = {"response": {"account": "newbie", "initial_balance": 10.30}}
        output_str = json.dumps(output_obj)

        self.check_expected_output(input_str, output_str)
