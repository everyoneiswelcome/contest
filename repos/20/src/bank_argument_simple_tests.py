#!/usr/bin/env python2

from __future__ import print_function
import sys, string, random, constants, re, bank_command_validator
from subprocess import check_output, CalledProcessError

TEST_LEVEL_LOW=10
TEST_LEVEL_MEDIUM=50
TEST_LEVEL_HIGH=100
TEST_LEVEL_INSANE=10000
CURRENT_TEST_LEVEL=TEST_LEVEL_INSANE



def setup_func():
    "set up test fixtures"
    pass


def teardown_func():
    "tear down test fixtures"
    pass


def debug(message):
    print("DEBUG: ", message, file=sys.stderr)


def run_cmd(command):
    output = ''
    exit_code = 0
    try:
        output = check_output(command, shell=True)
    except CalledProcessError as c:
        exit_code = c.returncode
    return (exit_code, output)

#################################################################
## Helper functions and generators
#################################################################
def generateFileNames(valid):
    return generateNames(valid,constants.ALLOWED_FILE_RANGE,constants.ALLOWED_FILES, constants.DISALLOWED_FILES)

def generateAccountNames(valid):
    return generateNames(valid,constants.ALLOWED_ACCOUNT_RANGE, constants.ALLOWED_ACCOUNTS, None)

def generateNames(valid, limits, allowed, disallowed):
    name = ""
    if valid:
        available_characters = string.ascii_lowercase + string.digits +"._-"

        rndlen = random.randint(limits[0],limits[1])

        for it in range(0, rndlen):
            name += random.choice(available_characters)

        if (disallowed and name in disallowed):
            return generateNames(valid,limits, allowed, disallowed)
    else:
        available_characters = string.printable
        rndlen = random.randint(1,1024)
        for it in range(0, rndlen):
            name += random.choice(available_characters)

        if not ((disallowed and (name.strip() in disallowed)) or (allowed and (not re.match(allowed,name.strip()) or re.match(allowed,name.strip()).group() != name.strip()))):
            return generateNames(valid,limits, allowed, disallowed)

    return name.strip()

def isSameParam(opt1, opt2):
    if isinstance(opt1, basestring):
        if isinstance(opt2, basestring):
            return opt1[:2]==opt2[:2]
        else:
            return opt1[:2]==opt2[0]
    else:
        if isinstance(opt2, basestring):
            return opt1[0]==opt2[:2]
        else:
            return opt1[0]==opt2[0]

def flatten(arg_list):
    flat_list = []
    for tu in arg_list:
        if isinstance(tu, basestring):
            flat_list.append(tu)
        else:
            flat_list.extend(flatten(tu))

    return flat_list

def selectNotYetSelected(choices,already_selected,count):
    if len(already_selected)>=len(choices):
        return already_selected;
    i=0
    avail= list(choices)
    selected = list(already_selected)
    while (i < count) and (len(selected)<len(choices)) and (len(avail)>0):
        choice = random.choice(avail)
        if not((choice in selected) or any(k for k, v in enumerate(selected) if isSameParam(v,choice))):
            selected.append(choice)
            i+=1

    return selected

def generatePorts(valid):
    port = random.randint(1,99999)
    if(valid and port >= constants.ALLOWED_PORTS_LIMITS[0] and port <= constants.ALLOWED_PORTS_LIMITS[1]) or ((not valid) and ((port < constants.ALLOWED_PORTS_LIMITS[0]) or (port > constants.ALLOWED_PORTS_LIMITS[1]) )):
        return str(port)
    else:
        return generatePorts(valid)

def generateRndFormOpt(opt,arg):
    param = [(opt,arg),(opt+arg)]
    return random.choice(param)

#################################################################
## Invalid input tests
#################################################################

# Tests one unknown argument at a time, CURRENT_TEST_LEVEL times.
def test_single_unknown_args():
    i = 0
    tested = []+constants.BANK_KNOWN_PARAMS
    while i < CURRENT_TEST_LEVEL and not all(l in tested for l in string.letters):
        rndChar = random.choice(string.letters)
        rndArg = "-"+ rndChar
        if not rndChar in tested:
            yield check_invalid_arg, [rndArg]
            tested.append(rndChar)
            i+=1

# Tests a set of at least CURRENT_TEST_LEVEL mixed (known and unknown) arguments at a time, CURRENT_TEST_LEVEL times.
def test_multiple_mixed_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        i2 = 0
        rndChar = []
        known = False
        unknown = False
        rnd_limit = random.randint(0,CURRENT_TEST_LEVEL)
        while (i2 < rnd_limit) or not known or not unknown:
            tmpChar = "-"+random.choice(string.letters)
            rndChar.append(tmpChar)
            i2+=1
            if tmpChar in constants.BANK_KNOWN_PARAMS:
                known = True
            else:
                unknown = True
        yield check_invalid_arg, rndChar

# Tests one known argument (regardless of needed value) at a time, CURRENT_TEST_LEVEL times.
def test_missing_value_known_args():
    yield check_invalid_arg, constants.BANK_KNOWN_PARAMS[0]
    yield check_invalid_arg, constants.BANK_KNOWN_PARAMS[1]

# Tests provided port out of range.
def test_option_badport_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        yield check_invalid_arg, flatten([generateRndFormOpt(constants.PORT_PARAM,generatePorts(False))])

# Tests provided port not a number.
def test_option_badport2_required_single_oneof_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        bad_port = "4554"
        try:
            while(int(bad_port)):
                bad_port=generateFileNames(True)
        except Exception:
            None

        yield check_invalid_arg, flatten([generateRndFormOpt(constants.PORT_PARAM,bad_port)])

# Tests provided illegal file name for auth argument.
def test_option_badauth_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        yield check_invalid_arg, flatten([generateRndFormOpt(constants.AUTH_PARAM,generateFileNames(False))])

def test_duplicate_port_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        yield check_invalid_arg, flatten([generateRndFormOpt(constants.PORT_PARAM,generatePorts(True)),generateRndFormOpt(constants.PORT_PARAM,generatePorts(True))])

def test_duplicate_auth_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        yield check_invalid_arg, flatten([generateRndFormOpt(constants.AUTH_PARAM,generateFileNames(True)),generateRndFormOpt(constants.AUTH_PARAM,generateFileNames(True))])

def check_invalid_arg(arg):
    try:
        bank_command_validator.validateCmdLine(arg)
        assert(False)
    except SystemExit as se_ex:
        assert(True)
    except Exception:
        assert(False)

#################################################################
## Valid input tests
#################################################################
def test_valid_format_no_args():
    check_valid_arg([])

def test_valid_format_with_port():
    for i in range(0, CURRENT_TEST_LEVEL):
        yield check_valid_arg, flatten([generateRndFormOpt(constants.PORT_PARAM,generatePorts(True))])

def test_valid_format_with_auth():
    for i in range(0, CURRENT_TEST_LEVEL):
        yield check_valid_arg, flatten([generateRndFormOpt(constants.AUTH_PARAM,generateFileNames(True))])

def test_valid_format_with_auth_and_port():
    rnd_avail_values = [generateRndFormOpt(constants.AUTH_PARAM,generateFileNames(True)),generateRndFormOpt(constants.PORT_PARAM,generatePorts(True))]
    selected_values = selectNotYetSelected(rnd_avail_values,[],len(rnd_avail_values))
    args = flatten(selected_values)

    yield check_valid_arg, args

def check_valid_arg(arg):
    try:
        bank_command_validator.validateCmdLine(arg)
        assert(True)
    except Exception as ex:
        debug(ex.message)
        assert(False)