#!/usr/bin/env python2

from __future__ import print_function
import sys
import json
import os.path
import os
import glob
import time
from subprocess import check_output, CalledProcessError, Popen, PIPE

PROG = './atm'
BANK_SERVICE = './bank'

EXIT_CLEAN = 0
EXIT_BAD_INPUT = 255
EXIT_PROTOCOL_ERROR = 63

LENIENT_COMPARISON = True

TEST_PORT = '12003'
TEST_IP = '127.0.0.1'
TEST_AUTHFILE = 'bank.default'


def run_cmd(command):
    output = ''
    exit_code = 0
    try:
        if type(command) == str:
            output = check_output(command, shell=True)
        else:
            debug(' '.join(command))
            output = check_output(command)
    except CalledProcessError as c:
        exit_code = c.returncode
    return (exit_code, output)


def load_files(metatestfile):
    fh = open(metatestfile)
    test_filenames = fh.read().split('\n')
    fh.close()

    testfiles = []
    for line in test_filenames:
        for fn in glob.glob(line):
            if os.path.isdir(fn):
                for subdir, dirs, files in os.walk(fn):
                    for f in files:
                        testfiles += [os.path.join(subdir, f)]

            else:
                testfiles += [fn]
    return testfiles


def test_generator():
    all_files = load_files('oracle_test_list')
    for fn in all_files:
        fh = open(fn, 'r')
        test_text = fh.read()
        fh.close()

        test_text = test_text.replace('%PORT%', TEST_PORT).replace('%IP%', TEST_IP)
        testobj = json.loads(test_text)
        fh.close()

        server_pipe = Popen([BANK_SERVICE, '-p', TEST_PORT, '-s', TEST_AUTHFILE], stdout=PIPE)
        debug('server has been launched')
        time.sleep(2)

        old_cards = glob.glob('*.card')
        for single_command in testobj['inputs']:
            yield run_commands, single_command['input'], single_command['output']

        for card in glob.glob('*.card') + glob.glob('.*.card'):
            if card not in old_cards:
                os.remove(card)
        server_pipe.terminate()


def run_commands(input, expected_output):
    (exit_code, output) = run_cmd([PROG] + input['input'])
    output = output.strip()
    if exit_code != expected_output['exit']:
        if len(output) == 0:
            raise AssertionError('Wrong exit code [%d]' % exit_code)
        else:
            raise AssertionError('Wrong exit code [%d], full output was %s'
                                 % (exit_code, output))

    assert(exit_code == expected_output['exit'])

    if len(expected_output['output']) == 0:
        if not len(output) == 0:
            raise AssertionError('Output generated, but none was expected')
    else:
        if len(output) == 0:
            raise AssertionError('Expected output, but got none!')

        if LENIENT_COMPARISON:
            try:
                output = json.loads(output)
            except ValueError:
                raise AssertionError('Unable to parse output [' + output + ']')
            try:
                expected = json.loads(expected_output['output'])
            except TypeError:
                expected = expected_output['output']
            for key, val in output.iteritems():
                assert(key in expected)
                assert(val == expected[key])

            for key, val in expected.iteritems():
                assert(key) in output
        else:
            if not output == expected_output['output']:
                raise AssertionError('Expected [%s], got [%s]' % (expected_output['output'], output))


def debug(message):
    print("DEBUG: ", message, file=sys.stderr)
