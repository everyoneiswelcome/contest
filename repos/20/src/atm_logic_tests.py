#!/usr/bin/env python2

from __future__ import print_function

from unittest import TestCase
import sys
import time
import os
import socket
import json
from contextlib import contextmanager
from StringIO import StringIO
from subprocess import Popen, PIPE, check_output, CalledProcessError
from atm_logic import RequestInterpreter
from bank_common import BadInputError, ProtocolError
from constants import *


PROG = './bank_server.py'

AUTH_FILE = 'bank.auth'
DEFAULT_PORT = 3000


@contextmanager
def captured_output():
    new_out, new_err = StringIO(), StringIO()
    old_out, old_err = sys.stdout, sys.stderr
    try:
        sys.stdout, sys.stderr = new_out, new_err
        yield sys.stdout, sys.stderr
    finally:
        sys.stdout, sys.stderr = old_out, old_err


def debug(message):
    print("DEBUG: ", message, file=sys.stderr)


def start_service(command):
    if type(command) == str:
        command = command.split()
    pipe = Popen(command, stdout=PIPE, stdin=PIPE)
    pipe.stdin.close()
    time.sleep(1)
    return pipe


def stop_service(pipe):
    try:
        if not pipe.poll():
            pipe.terminate()
            time.sleep(1)
            if not pipe.poll():
                pipe.kill()
    except OSError:
        pass
    return (pipe.returncode)


class TestATMLogic(TestCase):

    @classmethod
    def setup_class(self):
        self.pipe = start_service([PROG])

    @classmethod
    def teardown_class(self):
        os.remove(AUTH_FILE)
        stop_service(self.pipe)

    def test_request_interpreter_bad_config_none(self):
        try:
            RequestInterpreter(None)
            raise AssertionError('emtpy config option should not be allowed')
        except BadInputError:
            pass

    def test_request_interpreter_bad_config_not_dict(self):
        try:
            RequestInterpreter(2)
            raise AssertionError('Non-dictionary config should not be allowed')
        except BadInputError:
            pass

    def test_request_interpreter_bad_config_very_bad_dict(self):
        try:
            RequestInterpreter({'one': 'two'})
            raise AssertionError('Invalid config should not be allowed')
        except BadInputError:
            pass

    def test_request_interpreter_bad_no_valid_request(self):
        try:
            RequestInterpreter({'one': 'two'})
            raise AssertionError('Invalid config should not be allowed')
        except BadInputError:
            pass

    def test_request_interpreter_bad_no_account(self):
        try:
            RequestInterpreter({'balance': None})
            raise AssertionError('Invalid config should not be allowed')
        except BadInputError:
            pass

    def test_request_interpreter_bad_card_from_different_account(self):
        ### TODO
        pass

    def test_request_interpreter_overwrite_card_from_different_account(self):
        ### TODO
        pass

    def test_request_interpeter_bad_small_balance(self):
        balance = ALLOWED_AMOUNTS_LIMITS[0]
        ### TODO
        pass

    def test_request_interpeter_bad_large_balance(self):
        ### TODO
        balance = ALLOWED_AMOUNTS_LIMITS[1] + 0.01
        pass

    def test_request_interpreter_bad_card_exists(self):
        account = 'bob_card_exists'
        card = account + '.card'

        fh = open(card, 'w')
        fh.write('')
        fh.close()
        try:
            interpreter = RequestInterpreter({'account': account, 'new_balance': 10})
            interpreter.process_request()
            raise AssertionError('Invalid config should not be allowed')
        except BadInputError:
            pass
        finally:
            os.remove(card)

    def test_request_interpreter_good_create_new_account(self):
        account = 'bob_new_account'
        card = account + '.card'
        if os.path.exists(card):
            os.remove(card)
        with captured_output() as (out, err):
            interpreter = RequestInterpreter({'account': account, 'new_balance': 30.2})
            interpreter.process_request()

        output = out.getvalue().strip().split('\n')
        try:
            response = json.loads(output[0])
        except ValueError:
            raise AssertionError('Unexpected output when creating new account.')
        assert('account' in response)
        assert('initial_balance' in response)
        assert(response['initial_balance'] == 30.2)
        os.remove(card)

    def test_request_interpreter_good_deposit(self):
        ### TODO
        pass

    def test_request_interpreter_good_deposit_balance_limit(self):
        balance = ALLOWED_AMOUNTS_LIMITS[1]
        account = 'bob_deposit_limit'
        card = account + '.card'
        if os.path.exists(card):
            os.remove(card)

        with captured_output() as (out, err):
            interpreter = RequestInterpreter({'account': account, 'new_balance': balance})
            interpreter.process_request()
            interpreter = RequestInterpreter({'account': account, 'deposit': balance})
            interpreter.process_request()
            interpreter = RequestInterpreter({'account': account, 'get_balance': 0})
            interpreter.process_request()

        output = out.getvalue().strip().split('\n')
        try:
            response = json.loads(output[2])
        except ValueError:
            raise AssertionError('Unexpected output when creating new account.')
        assert('account' in response)
        assert('balance' in response)
        assert(response['balance'] == (2.0 * balance))
        os.remove(card)

    def test_request_interpreter_good_get_balance(self):
        balance = 15.00
        account = 'bob_balance'
        card = account + '.card'
        if os.path.exists(card):
            os.remove(card)

        with captured_output() as (out, err):
            interpreter = RequestInterpreter({'account': account, 'new_balance': balance})
            interpreter.process_request()
            interpreter = RequestInterpreter({'account': account, 'get_balance': balance})
            interpreter.process_request()

        output = out.getvalue().strip().split('\n')
        try:
            response = json.loads(output[1])
        except ValueError:
            debug(output)
            raise AssertionError('Unexpected output when creating new account.')
        assert('account' in response)
        assert('balance' in response)
        assert(response['balance'] == balance)
        os.remove(card)

    def test_request_interpreter_good_withdraw(self):
        ### TODO
        pass
