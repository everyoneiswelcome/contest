#!/usr/bin/env python2

import sys
import bank_command_validator

def main(argv):
    bank_command_validator.validateCmdLine(argv)

if __name__ == "__main__":
   main(sys.argv[1:])
