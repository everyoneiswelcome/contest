#!/usr/bin/env python2

from __future__ import print_function

from authenticated_crypter import AuthenticatedCrypter, InvalidToken
import socket
import sys
import os
import json
import signal
from bank_common import *
from bank_logic import BankCommandInterpreter
from constants import *

NONCE_SIZE_BYTES = 16


class BankServer():

    """ Responds to incomming requests from atms """

    def __init__(self, auth_file, port):
        """ initializes the bank server.

        Args:
            auth_file (str): the name of a file to which the shared secret will
                be written
            port (int): a port number to run the server on

        Attributes:
            _crypter (AuthenticatedCrypter): a utility for generating symmetric
                 keys using AES CBC 192 with PKCS7 / HMAC with SHA-256
            _interpreter (BankCommandInterpreter): a parser and handler for
                incoming data
            _key (bytes): a symmetric key to be used for encrypting / decrypting
        """
        self._auth_file = auth_file
        self._port = port
        self._interpreter = BankCommandInterpreter()

        self._key = AuthenticatedCrypter.generate_key()
        self._crypter = AuthenticatedCrypter(self._key)
        self.create_auth_file()

    def create_auth_file(self):
        """Creates an authorization file containing a shared secret.

        Creates a file named by self._auth_file, overwriting any file already
        existing.

        Output:
            prints 'created' to stdout and flushes stdout.
        """
        afh = open(self._auth_file, 'w')
        afh.write(self._key)
        afh.close()
        print('created')
        sys.stdout.flush()

    def encrypt(self, data):
        """encrypts given data using pre-shared key.

        Returns:
            encrypted data
        """
        return self._crypter.encrypt(data)

    def decrypt(self, data):
        """decrypts given data using pre-shared key.

        Returns:
            decrypted data
        """

        return self._crypter.decrypt(data)

    def run(self):
        """Runs the bank server.

        Binds to the configured port and listens for a single incoming
        connection. Exits silently if port is already in use.

        Outputs:
            prints 'protocol_error' and flushes stdout if a protocol error
            is observed.
        """
        self._s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        try:
            self._s.bind(('127.0.0.1', self._port))
        except socket.error:
            debug('server already running on this port.')
            sys.exit(1)

        self._s.listen(1)

        while 1:
            debug('awaiting connections')
            try:
                (self._conn, self._addr) = self._s.accept()
                self._conn.settimeout(TIMEOUT_SECONDS)
                data = self._conn.recv(MAX_PAYLOAD_SIZE)
                response = self.data_received(data)
                self._conn.send(response)
            except socket.timeout:
                print('protocol_error')
                sys.stdout.flush()

        self.stop()

    def data_received(self, data):
        """Passes off received data and returns a response to the client.

        Args:
            data (str): an encrypted bytestring

        Returns:
            a bytestring of length < MAX_PAYLOAD_SIZE, containing an encrypted
            and authenticated JSON-encoded response

        Raises:
            BadInputError: if the request was improperly formatted
            ProtocolError: if a timeout was observed from the time in the
                response object
        """
        atm_nonce = ''
        bank_nonce = ''

        try:
            if not data:
                raise ProtocolError('no data')

            request = json.loads(self._crypter.decrypt(data))
            response = {'response': request['request']}
            bank_nonce = os.urandom(NONCE_SIZE_BYTES).encode('base64').strip()
            atm_nonce = request['request']['permission']
            response['response']['granted'] = bank_nonce
            self._conn.send(self._crypter.encrypt(json.dumps(response)))

            data = self._conn.recv(MAX_PAYLOAD_SIZE)

            if not data:
                raise ProtocolError('no data')

            decrypted_data = self.decrypt(data)
            response = self._interpreter.handle_request(decrypted_data,
                                                        atm_nonce,
                                                        bank_nonce)
        except BadInputError as e:
            debug(e.value)
            response = self._interpreter.create_error_response(255,
                                                               atm_nonce,
                                                               bank_nonce)
        except ProtocolError as e:
            print('protocol_error')
            debug(e.value)
            sys.stdout.flush()
            response = self._interpreter.create_error_response(63,
                                                               atm_nonce,
                                                               bank_nonce)
        except InvalidToken:
            print('protocol_error')
            sys.stdout.flush()
            response = self._interpreter.create_error_response(63,
                                                               atm_nonce,
                                                               bank_nonce)
        except KeyError:
            debug(request)
            print('protocol_error')
            sys.stdout.flush()
            response = self._interpreter.create_error_response(63,
                                                               atm_nonce,
                                                               bank_nonce)


        return self.encrypt(response)[0:MAX_PAYLOAD_SIZE]

    def stop(self):
        """Cleanly shuts down the server."""
        debug('stopping bank')
        if hasattr(self, '_conn'):
            self._conn.shutdown(socket.SHUT_RDWR)
            self._conn.close()
        self._s.close()


if __name__ == "__main__":
    try:
        debug('bank is running.')
        server = BankServer(DEFAULT_AUTH_VALUE, int(DEFAULT_PORT_VALUE))
        server.run()
        signal.signal(signal.SIGHUP, server.stop)
        signal.signal(signal.SIGTERM, server.stop)
        signal.signal(signal.SIGINT, server.stop)
    except KeyboardInterrupt as e:
        debug('shutting down from keyboard')
        server.stop()
