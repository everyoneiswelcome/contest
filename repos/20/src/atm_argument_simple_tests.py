#!/usr/bin/env python2

from __future__ import print_function
import sys, string, random, constants, re, atm_command_validator
from subprocess import check_output, CalledProcessError

PROG = './atm_command_parser.py'

TEST_LEVEL_LOW=10
TEST_LEVEL_MEDIUM=50
TEST_LEVEL_HIGH=100
TEST_LEVEL_INSANE=1000
CURRENT_TEST_LEVEL=TEST_LEVEL_HIGH



def setup_func():
    "set up test fixtures"
    pass


def teardown_func():
    "tear down test fixtures"
    pass


def debug(message):
    print("DEBUG: ", message, file=sys.stderr)


def run_cmd(command):
    #$''
    output = ''
    exit_code = 0
    try:
        output = check_output(command, shell=True)
    except CalledProcessError as c:
        exit_code = c.returncode
    return (exit_code, output)

#################################################################
## Helper functions and generators
#################################################################
def generateFileNames(valid):
    return generateNames(valid,constants.ALLOWED_FILE_RANGE,constants.ALLOWED_FILES, constants.DISALLOWED_FILES)

def generateAccountNames(valid):
    return generateNames(valid,constants.ALLOWED_ACCOUNT_RANGE, constants.ALLOWED_ACCOUNTS, None)

def generateNames(valid, limits, allowed, disallowed):
    name = ""
    if valid:
        available_characters = string.ascii_lowercase + string.digits +"._-"

        rndlen = random.randint(limits[0],limits[1])

        for it in range(0, rndlen):
            name += random.choice(available_characters)

        if (disallowed and name in disallowed):
            return generateNames(valid,limits, allowed, disallowed)
    else:
        available_characters = string.printable
        rndlen = random.randint(1,1024)
        for it in range(0, rndlen):
            name += random.choice(available_characters)

        if not ((disallowed and (name.strip() in disallowed)) or (allowed and (not re.match(allowed,name.strip()) or re.match(allowed,name.strip()).group() != name.strip()))):
            return generateNames(valid,limits, allowed, disallowed)

    return name.strip()

def isSameParam(opt1, opt2):
    if isinstance(opt1, basestring):
        if isinstance(opt2, basestring):
            return opt1[:2]==opt2[:2]
        else:
            return opt1[:2]==opt2[0]
    else:
        if isinstance(opt2, basestring):
            return opt1[0]==opt2[:2]
        else:
            return opt1[0]==opt2[0]

def flatten(arg_list):
    flat_list = []
    for tu in arg_list:
        if isinstance(tu, basestring):
            flat_list.append(tu)
        else:
            flat_list.extend(flatten(tu))

    return flat_list

def selectNotYetSelected(choices,already_selected,count):
    if len(already_selected)>=len(choices):
        return already_selected;
    i=0
    avail= list(choices)
    selected = list(already_selected)
    while (i < count) and (len(selected)<len(choices)) and (len(avail)>0):
        choice = random.choice(avail)
        if not((choice in selected) or any(k for k, v in enumerate(selected) if isSameParam(v,choice))):
            selected.append(choice)
            i+=1

    return selected

def generateIP(valid):
    ip_addr = str(random.randint(0,500))+"."+str(random.randint(0,500))+"."+str(random.randint(0,500))+"."+str(random.randint(0,500))
    if (valid and re.match(constants.ALLOWED_IPS,ip_addr) and re.match(constants.ALLOWED_IPS,ip_addr).group() == ip_addr) or ((not valid) and (not (re.match(constants.ALLOWED_IPS,ip_addr)) or re.match(constants.ALLOWED_IPS,ip_addr).group() != ip_addr)):
        return ip_addr
    else:
        return generateIP(valid)

def generatePorts(valid):
    port = random.randint(1,99999)
    if(valid and port >= constants.ALLOWED_PORTS_LIMITS[0] and port <= constants.ALLOWED_PORTS_LIMITS[1]) or ((not valid) and ((port < constants.ALLOWED_PORTS_LIMITS[0]) or (port > constants.ALLOWED_PORTS_LIMITS[1]) )):
        return str(port)
    else:
        return generatePorts(valid)

def generateAmounts(valid):
    amount = random.uniform(0.0,9.9)
    multiplier = random.randint(0,10)
    for i in range(0,multiplier):
        amount = amount * 10

    if valid:
        if re.match(constants.ALLOWED_AMOUNTS,str(amount)) and re.match(constants.ALLOWED_AMOUNTS,str(amount)).group() == str(amount) and (amount > constants.ALLOWED_AMOUNTS_LIMITS[0]) and (amount <= constants.ALLOWED_AMOUNTS_LIMITS[1]):
            return re.match(constants.ALLOWED_AMOUNTS,str(amount)).group()
        else:
            return generateAmounts(valid)
    else:
        if not (re.match(constants.ALLOWED_AMOUNTS,str(amount)) and re.match(constants.ALLOWED_AMOUNTS,str(amount)).group() == str(amount)) or amount < constants.ALLOWED_AMOUNTS_LIMITS[0] or amount > constants.ALLOWED_AMOUNTS_LIMITS[1]:
            return str(amount)
        else:
            return generateAmounts(valid)

def generateRandomOptions():
    auth_param = [(constants.AUTH_PARAM,generateFileNames(True)),(constants.AUTH_PARAM+generateFileNames(True))]
    card_param = [(constants.CARD_PARAM,generateFileNames(True)),(constants.CARD_PARAM+generateFileNames(True))]
    ip_param = [(constants.IP_PARAM,generateIP(True)),(constants.IP_PARAM+generateIP(True))]
    port_param = [(constants.PORT_PARAM,generatePorts(True)),(constants.PORT_PARAM+generatePorts(True))]
    return [random.choice(auth_param),random.choice(card_param),random.choice(ip_param),random.choice(port_param)]

def generateRndFormOpt(opt,arg):
    param = [(opt,arg),(opt+arg)]
    return random.choice(param)

#################################################################
## Invalid input tests
#################################################################
def test_no_args():
    check_invalid_arg([])

# Tests one unknown argument at a time, CURRENT_TEST_LEVEL times.
def test_single_unknown_args():
    i = 0
    tested = []+constants.ATM_KNOWN_PARAMS
    while i < CURRENT_TEST_LEVEL and not all(l in tested for l in string.letters):
        rndChar = random.choice(string.letters)
        rndArg = "-"+ rndChar

        if not rndChar in tested:
            yield check_invalid_arg, [rndArg]
            tested.append(rndChar)
            i+=1

# Tests up to CURRENT_TEST_LEVEL unknown arguments at a time, CURRENT_TEST_LEVEL times.
def test_multiple_unknown_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rndChar = []
        rnd_limit = random.randint(0,CURRENT_TEST_LEVEL)
        for i2 in range(0,rnd_limit):
            tmpChar = "-"+random.choice(string.letters)
            if not tmpChar in constants.ATM_KNOWN_PARAMS:
                rndChar.append(tmpChar)

        yield check_invalid_arg, rndChar

# Tests a set of at least CURRENT_TEST_LEVEL mixed (known and unknown) arguments at a time, CURRENT_TEST_LEVEL times.
def test_multiple_mixed_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        i2 = 0
        rndChar = []
        known = False
        unknown = False
        rnd_limit = random.randint(0,CURRENT_TEST_LEVEL)
        while (i2 < rnd_limit) or not known or not unknown:
            tmpChar = "-"+random.choice(string.letters)
            rndChar.append(tmpChar)
            i2+=1
            if tmpChar in constants.ATM_KNOWN_PARAMS:
                known = True
            else:
                unknown = True
        yield check_invalid_arg, rndChar

# Tests one known argument (regardless of needed value) at a time, CURRENT_TEST_LEVEL times.
def test_insufficient_known_args():
    tested = []
    i=0
    while i < CURRENT_TEST_LEVEL and not all(l in tested for l in constants.ATM_KNOWN_PARAMS):
        rnd_param = random.choice(constants.ATM_KNOWN_PARAMS)
        if not rnd_param in tested:
            tested.append(rnd_param)
            i+=1
            yield check_invalid_arg, [rnd_param]

# Tests at least 2 known arguments with proper values, from the same group, at a time, CURRENT_TEST_LEVEL times.
def test_samegroup_known_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(2,len(rnd_avail_values)))
        args = flatten(selected_values)
        yield check_invalid_arg, args

# Tests at least 1 random duplicate optional arguments.
def test_samegroup_duplicate_known_args():
    for i in range(0, CURRENT_TEST_LEVEL):

        rnd_avail_values= generateRandomOptions()
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(3,len(rnd_avail_values)))
        selected_values = selected_values + selectNotYetSelected(rnd_avail_values,[],random.randint(2,len(rnd_avail_values)))
        args = flatten(selected_values)

        yield check_invalid_arg, args

# Tests missing OneOf argument.
def test_option_and_required_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) +[generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True))]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_invalid_arg, args

# Tests extra value added to OneOf argument which doesn't take arguments.
def test_option_required_oneof_valuenotneeded_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        bad_value = "-"
        while len(bad_value) and bad_value[0]=="-":
            bad_value = generateFileNames(True)

        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),generateRndFormOpt(constants.CURRENTBALANCE_PARAM,bad_value)]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_invalid_arg, args

# Tests provided amount with illegal format.
def test_option_required_single_oneof_badamount_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),generateRndFormOpt(random.choice(constants.AMOUNT_GROUP),generateAmounts(False))]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_invalid_arg, args

# Tests provided amount not a number.
def test_option_required_single_oneof_badamount2_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        bad_amount = "0.00"
        while(re.match(constants.ALLOWED_AMOUNTS,bad_amount) and re.match(constants.ALLOWED_AMOUNTS,bad_amount).group()==bad_amount):
            bad_amount=generateFileNames(True)
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),generateRndFormOpt(random.choice(constants.AMOUNT_GROUP),bad_amount)]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_invalid_arg, args

# Tests provided amount out of range.
def test_option_required_single_oneof_badamount3_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),generateRndFormOpt(random.choice(constants.AMOUNT_GROUP),str(random.randint(1,999999)+constants.ALLOWED_AMOUNTS_LIMITS[1]))]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_invalid_arg, args

# Tests provided illegal account name.
def test_option_required_badvalue_single_oneof_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(False)),generateRndFormOpt(random.choice(constants.AMOUNT_GROUP),generateAmounts(True))]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_invalid_arg, args

# Tests provided illegal ip.
def test_option_badip_required_single_oneof_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        for t in rnd_avail_values:
            if isSameParam(t,constants.IP_PARAM):
                rnd_avail_values.remove(t)

        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.IP_PARAM,generateIP(False)),generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),generateRndFormOpt(random.choice(constants.AMOUNT_GROUP),generateAmounts(True))]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_invalid_arg, args

# Tests provided port out of range.
def test_option_badport_required_single_oneof_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        for t in rnd_avail_values:
            if isSameParam(t,constants.PORT_PARAM):
                rnd_avail_values.remove(t)

        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.PORT_PARAM,generatePorts(False)),generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),generateRndFormOpt(random.choice(constants.AMOUNT_GROUP),generateAmounts(True))]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_invalid_arg, args

# Tests provided port not a number.
def test_option_badport2_required_single_oneof_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        for t in rnd_avail_values:
            if isSameParam(t,constants.PORT_PARAM):
                rnd_avail_values.remove(t)

        bad_port = "4554"
        try:
            while(int(bad_port)):
                bad_port=generateFileNames(True)
        except Exception:
            None

        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.PORT_PARAM,bad_port),generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),generateRndFormOpt(random.choice(constants.AMOUNT_GROUP),generateAmounts(True))]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_invalid_arg, args

# Tests provided illegal file name for auth argument.
def test_option_badauth_required_single_oneof_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        for t in rnd_avail_values:
            if isSameParam(t,constants.AUTH_PARAM):
                rnd_avail_values.remove(t)

        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.AUTH_PARAM,generateFileNames(False)),generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),generateRndFormOpt(random.choice(constants.AMOUNT_GROUP),generateAmounts(True))]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_invalid_arg, args

# Tests provided illegal file name for card argument.
def test_option_badcard_required_single_oneof_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        for t in rnd_avail_values:
            if isSameParam(t,constants.CARD_PARAM):
                rnd_avail_values.remove(t)

        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.CARD_PARAM,generateFileNames(False)),generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),generateRndFormOpt(random.choice(constants.AMOUNT_GROUP),generateAmounts(True))]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_invalid_arg, args

# Tests provided multiple OneOf arguments requiring amount.
def test_option_required_multiple_oneof_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(random.choice(constants.AMOUNT_GROUP),generateAmounts(True)),constants.CURRENTBALANCE_PARAM]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_invalid_arg, args

# Tests provided multiple OneOf arguments, one not requiring amount.
def test_option_required_multiple_oneof_noamount_args():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + selectNotYetSelected([generateRndFormOpt(constants.DEPOSIT_PARAM,generateAmounts(True)),generateRndFormOpt(constants.WITHDRAW_PARAM,generateAmounts(True)),generateRndFormOpt(constants.BALANCE_PARAM,generateAmounts(True))],[],random.randint(2,3))
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)
        #debug(args)
        yield check_invalid_arg, args

def test_duplicated_valid_balance_option():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),(constants.CURRENTBALANCE_PARAM,)]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))+[(constants.CURRENTBALANCE_PARAM,)]
        args = flatten(final_opts)

        yield check_invalid_arg, args

def test_double_oneof_option():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),generateRndFormOpt(random.choice(constants.AMOUNT_GROUP),generateAmounts(True))]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))+[(constants.CURRENTBALANCE_PARAM,)]
        args = flatten(final_opts)

        yield check_invalid_arg, args

def test_duplicated_valid_options_option():
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(3,len(rnd_avail_values))) + [generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),(constants.CURRENTBALANCE_PARAM,)]
        selected_values = selected_values + selectNotYetSelected(rnd_avail_values,[],random.randint(2,len(rnd_avail_values)))
        args = flatten(selected_values)

        yield check_invalid_arg, args

def check_invalid_arg(arg):
    try:
        atm_command_validator.validateCmdLine(arg)
        assert(False)
    except SystemExit as se_ex:
        assert(True)
    except Exception:
        assert(False)

#################################################################
## Valid input tests
#################################################################
import profile_settings
profile_settings.init()

def test_valid_format_with_amount():

    profile_settings.init()
    #pr.enable()
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),generateRndFormOpt(random.choice(constants.AMOUNT_GROUP),generateAmounts(True))]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_valid_arg, args, True

    profile_settings.proc_dump()

def test_valid_format_without_amount():
    profile_settings.init()
    #pr.enable()
    for i in range(0, CURRENT_TEST_LEVEL):
        rnd_avail_values= generateRandomOptions()
        selected_values = selectNotYetSelected(rnd_avail_values,[],random.randint(0,len(rnd_avail_values))) + [generateRndFormOpt(constants.ACCOUNT_PARAM,generateAccountNames(True)),(constants.CURRENTBALANCE_PARAM,)]
        final_opts = selectNotYetSelected(selected_values,[],len(selected_values))
        args = flatten(final_opts)

        yield check_valid_arg, args, True

    profile_settings.proc_dump()

def check_valid_arg(arg, profile=False):
    try:
        atm_command_validator.validateCmdLine(arg, profile)
        assert(True)
    except Exception as ex:
        debug(ex.message)
        import time
        time.sleep(30)
        assert(False)