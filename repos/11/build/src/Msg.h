/*
 * Msg.h
 *
 *  Created on: Oct 1, 2015
 *      Author: rajiv
 */

#ifndef SRC_MSG_H_
#define SRC_MSG_H_

#include <string>
#include <memory.h>

using namespace std;

#include "Common.h"

struct Atm;
struct Msg;
struct TcpConnection;


struct Msg {
	Msg();

	MsgType _msgType;
	unsigned char _sessionKey[SESSION_KEY_SIZE];
	char _accountId[ACCOUNT_ID_SIZE+1];
	unsigned char _cardKey[CARD_KEY_SIZE];
	AccountAction _accountAction;
	unsigned long _actionAmount;
	bool _success;
	FailureReason _failureReason;

	void clear();
};

#define HMAC_SIZE 20
#define AES_BLOCK_SIZE 16
#define MSG_SIZE sizeof(Msg)
#define HASHMSG_SIZE (HMAC_SIZE + MSG_SIZE)
#define PADDED_HASHMSG_SIZE (HASHMSG_SIZE + (AES_BLOCK_SIZE - HASHMSG_SIZE % AES_BLOCK_SIZE))
#define SECURE_MSG_SIZE (AES_BLOCK_SIZE + PADDED_HASHMSG_SIZE)

ostream& operator<<(ostream& os, Msg& msg);

struct IoRecord {
	void clear() {
		memset(this, 0, sizeof(IoRecord));
	}
	void resetIo() {
		_state = IoState_Null;
		_bytesTotal = 0;
		_bytesDone = 0;
		_endTime = 0;
	}

	unsigned char _secureBytes[SECURE_MSG_SIZE]; // includes padding
	Msg _msg;
	IoState _state;
	size_t _bytesTotal;
	size_t _bytesDone;
	time_t _endTime;
};
ostream& operator<<(ostream& os, const IoRecord& ior);

struct ConnectionProcessor {
	virtual bool processConnection(TcpConnection* tcpConnection) = 0;
	virtual ~ConnectionProcessor() {}
};



#endif /* SRC_MSG_H_ */
