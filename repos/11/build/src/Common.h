/*
 * Common.h
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#ifndef SRC_COMMON_H_
#define SRC_COMMON_H_

#include <string>
#include <iostream>
#include <iomanip>
#include <time.h>
#include <unistd.h>
#include <stdint.h>

#define GRACEFUL_BUT_INEFFICIENT 1
#define SESSION_KEY_SIZE 16
#define ACCOUNT_ID_SIZE 250
#define AUTH_KEY_SIZE 16
#define CARD_KEY_SIZE 16

using namespace std;

enum MsgType {
	MsgType_Null = 0,
	MsgType_SessionKeyRequest,
	MsgType_SessionKeyResponse,
	MsgType_AccountActionRequest,
	MsgType_AccountActionResponse
};
const char* getName(MsgType mt);

enum AccountAction {
	AccountAction_Null = 0,
	AccountAction_New,
	AccountAction_Deposit,
	AccountAction_Withdrawal,
	AccountAction_Balance
};
const char* getName(AccountAction aa);

enum FailureReason {
	FR_Null,
	FR_AccountIdNotUnique,
	FR_MinInitialBalanceViolated,
	FR_MaxCurrencyAmountViolated,
	FR_AccountDoesNotExist,
	FR_CardFileAuthenticationFailed,
	FR_DepositAmountNotPositive,
	FR_InsufficientBalance,
	FR_InvalidAccountAction,
	FR_UnexpectedAccountInsertFailure
};
const char* getName(FailureReason fr);

enum IoState {
	IoState_Null = 0,

	IoState_Complete = 1,
	IoState_EOF = 2,

	IOSTATE_INCOMPLETE_GT = 10,

	IoState_InComplete = 11,
	IoState_InCompleteHeader = 12,
	IoState_InCompleteBody = 13,
	IoState_InCompleteRecordRead = 14,

	IOSTATE_ERROR_LT = -1,

	IoState_MaxRecordSizeExceeded = -2,
	IoState_AbruptEOF = -3,
	IoState_TimedOut = -5,
	IoState_SocketError = -6,
	IoState_SecurityError = -7,
	IoState_InvalidSessionKey = -8,
	IoState_LogicError = -9
};
const char* getName(IoState is);


struct Buffer2Hex {
	Buffer2Hex(unsigned char* b, size_t size, size_t beginBytes = 0, size_t endBytes = 0)
			:
			_b(b),
			_size(size),
			_beginBytes((beginBytes == 0) ? size : beginBytes),
			_endBytes((endBytes == 0) ? size : endBytes),
			_middleBytes((int) size - _beginBytes - _endBytes) {
	}
	unsigned char* _b;
	size_t _size;
	size_t _beginBytes;
	size_t _endBytes;
	int _middleBytes;
};

ostream& operator<<(ostream& os, const Buffer2Hex& s2h);


string string_to_hex(const string& in);
struct String2Hex {
	String2Hex(const string& s, size_t beginBytes = 0, size_t endBytes = 0)
			:
			_s(s),
			_beginBytes((beginBytes == 0) ? s.size() : beginBytes),
			_endBytes((endBytes == 0) ? s.size() : endBytes),
			_middleBytes((int) s.size() - _beginBytes - _endBytes) {
	}
	const string& _s;
	const size_t _beginBytes;
	const size_t _endBytes;
	int _middleBytes;
};
ostream& operator<<(ostream& os, const String2Hex& s2h);
string hex_to_string(const string& in);

#define MESSAGE_READ_TIMEOUT_IN_SECONDS 10
#define MESSAGE_WRITE_TIMEOUT_IN_SECONDS 10

/*
struct IoRecord2Hex {
	IoRecord2Hex(const IoRecord& r, uint16_t beginBytes = 0, uint16_t endBytes = 0)
			:
			_r(r),
			_beginBytes((beginBytes == 0) ? r._byteCount : beginBytes),
			_endBytes((endBytes == 0) ? r._byteCount : endBytes),
			_middleBytes(r._byteCount - _beginBytes - _endBytes) {
	}
	const IoRecord& _r;
	const uint16_t _beginBytes;
	const uint16_t _endBytes;
	int _middleBytes;
};
ostream& operator<<(ostream& os, const IoRecord2Hex& r2h);
*/

typedef unsigned long CurrencyAmount;

const CurrencyAmount MaxCurrencyAmount = 429496729599UL;
const CurrencyAmount MinCurrencyAmount = 0UL;
const CurrencyAmount MinInitialBalance = 1000UL;

struct CurrencyAmountWrapper
{
	CurrencyAmountWrapper(unsigned long value)
		:
		_value(value)
	{
	}

	unsigned long _value;
};

inline ostream& operator<<(ostream& os, const CurrencyAmountWrapper& v) {
	return os << v._value/100 << "." << setw(2) << setfill('0') << v._value%100;
}

inline bool operator<(const CurrencyAmountWrapper& lv, const CurrencyAmountWrapper& rv) {
	return lv._value < rv._value;
}

struct Sleep {
	Sleep(int seconds) : _seconds(seconds) {
	}
	const int _seconds;
};

inline ostream& operator<<(ostream& os, const Sleep& s) {
	os << "Sleep(" << s._seconds << ")";
	::sleep(s._seconds);
	return os;
}

struct Clock {
	Clock()
	:_timespec(){
		_timespec.tv_sec = 0;
		_timespec.tv_nsec = 0;
		clock_gettime(CLOCK_REALTIME, &_timespec);
	}
	Clock(const timespec& timespec)
	:_timespec(timespec){
	}
	timespec _timespec;
};

inline Clock operator-(const Clock& l, const Clock& r) {
	timespec dt = l._timespec;
	if (dt.tv_nsec < r._timespec.tv_nsec) {
		dt.tv_nsec += 10000000000L;
		dt.tv_nsec -= 1;
	}
	dt.tv_sec -= r._timespec.tv_sec;
	dt.tv_nsec -= r._timespec.tv_nsec;
	return Clock(dt);
}

inline ostream& operator<<(ostream& os, const Clock& c) {
	return os << c._timespec.tv_sec << "." << setw(9) << setfill('0') << c._timespec.tv_nsec;
}

struct Elapsed {
	Elapsed() : _startClock() {
	}
	Clock elapsed() const {
		return (Clock() - _startClock);
	}
	Clock startClock() const {
		return _startClock;
	}
private:
	Clock _startClock;
};

inline ostream& operator<<(ostream& os, const Elapsed& e) {
	Clock ce = e.elapsed();
	return os << ce;
}

extern Elapsed elapsed;

#endif /* SRC_COMMON_H_ */
