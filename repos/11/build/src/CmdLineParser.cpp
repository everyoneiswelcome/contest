/*
 * ParametersParser.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <cstdlib>
#include <iterator>
#include <algorithm>
#include <fstream>
#include "Bank.h"
#include "Atm.h"
#include "CmdLineParser.h"
#include "Parameters.h"

using namespace std;

CmdLineParser::CmdLineParser(const vector<string>& args)
:
_args(args),
_argCount(args.size()),
_argIndex(0),
_error(false)
{
}

bool CmdLineParser::parseParam(Param* pParam, const string& arg) {
	Param& param = *pParam;

	if (arg.compare(0, param._flag.size(), param._flag) != 0)
		return false;

	//cerr << "Cmdline : Processing flag " << param._flag << endl << flush;

	_error = true;

	if (param._hasValue) {
		cerr << "\tERROR: Duplicate Parameter " << param._flag << endl << flush;
		return false;
	}

	string argValue = arg.substr(param._flag.size());

	if (param._flagOnly) {
		if (argValue.empty()) {
			_error = false;
			param._hasValue = true;
			return true;
		} else {
			cerr << "\tERROR: Flag only Parameter has value " << param._flag << "=" << argValue << endl << flush;
			return false;
		}
	}

	if (argValue.empty()) {
		if (_argIndex < _argCount) {
			argValue = _args[_argIndex++];
		} else {
			cerr << "\tERROR: Missing Parameter Value for " << param._flag << endl << flush;
			return false;
		}
	}

	//cerr << "Cmdline : Processing flag value " << param._flag << "=" << argValue << endl << flush;

	if (param._pRegex != 0 && !param._pRegex->match(argValue)) {
		cerr << "\tERROR: Regex mismatch for " << param._flag << "=" << argValue << endl << flush;
		return false;
	}
	if (!param.parseValue(argValue)) {
		cerr << "\tERROR: Value parsing error for " << param._flag << "=" << argValue << endl << flush;
		return false;
	}

	if (!param.isValid()) {
		cerr << "\tERROR: Value invalid for " << param._flag << "=" << argValue << endl << flush;
		return false;
	}

	_error = false;
	param._hasValue = true;
	return true;
}

Bank* CmdLineParser::createBank() {

	ParamPort paramPort;
	ParamAuthFile paramAuthFile;
	Param* params[2] = {&paramPort, &paramAuthFile};
	int paramCount = sizeof(params)/sizeof(Param*);

	// process named parameters
	while (!_error && _argIndex < _argCount) {
		//cerr << "BankCmdline : Processing arg " << _argIndex <<  " = " << _args[_argIndex] << endl << flush;

		string arg = (_args[_argIndex++]);

		for (int pi=0; pi<paramCount; pi++) {
			if (parseParam(params[pi], arg))
				goto continue_while_loop;
			if (_error)
				break;
		}

		if (_error)
			return 0; // unrecognized parameter

		_error = true;
		cerr << "\tERROR: Unknown parameter " << arg << endl << flush;
		return 0;

		continue_while_loop: ; /* no op */
	}

	// use defaults if param value not provided
	for (int pi=0; pi<paramCount; pi++) {
		Param& param = *params[pi];
		if (!param._hasValue && param._hasDefault)
			param._hasValue = true;
	}

	// check if all params have value
	for (int pi=0; pi<paramCount; pi++) {
		Param& param = *params[pi];
		if (!param._hasValue) {
			cerr << "\tERROR: Missing parameter " << param._flag << endl << flush;
			_error = true;
			return 0;
		}
	}

	//cerr << "\tINFO: Bank parameters = " << paramPort._flag << " " << paramPort._value << " " << paramAuthFile._flag << " " << paramAuthFile._value << endl << flush;

	return new Bank((unsigned short) paramPort._value, paramAuthFile._value);
}

Atm* CmdLineParser::createAtm() {

	if (_args.empty() || _argIndex != 0)
		return 0;

	ParamAuthFile paramAuthFile;
	ParamIpAddress paramIpAddress;
	ParamPort paramPort;
	ParamCardFile paramCardFile;
	ParamAccountId paramAccountId;
	ParamNewAccount paramNewAccount;
	ParamAccountDeposit paramAccountDeposit;
	ParamAccountWithdrawal paramAccountWithdrawal;
	ParamAccountBalance paramAccountBalance;
	Param* params[] = {&paramAuthFile, &paramIpAddress, &paramPort, &paramCardFile, &paramAccountId,
			&paramNewAccount, &paramAccountDeposit, &paramAccountWithdrawal, &paramAccountBalance};
	int paramCount = sizeof(params)/sizeof(Param*);

	// process named parameters
	while (!_error && _argIndex < _argCount) {
		//cerr << "AtmCmdline : Processing arg " << _argIndex <<  " = " << _args[_argIndex] << endl;

		string arg = (_args[_argIndex++]);

		for (int pi=0; pi<paramCount; pi++) {
			if (parseParam(params[pi], arg))
				goto continue_while_loop;
			if (_error)
				break;
		}

		if (_error)
			return 0; // unrecognized parameter

		_error = true;
		cerr << "\tERROR: Unexpected parameter " << arg << endl << flush;
		return 0;


		continue_while_loop: ; /* no op */
	}

	// default card file name based on account id
	if (!paramCardFile._hasValue) {
		paramCardFile._value = paramAccountId._value + ".card";
		paramCardFile._hasValue = true;
	}

	// use defaults if param value not provided
	for (int pi=0; pi<(paramCount-4); pi++) {
		Param& param = *params[pi];
		if (!param._hasValue && param._hasDefault)
			param._hasValue = true;
	}

	// check if all params have value
	for (int pi=0; pi<(paramCount-4); pi++) {
		Param& param = *params[pi];
		if (!param._hasValue) {
			cerr << "\tERROR: Missing parameter " << param._flag << endl << flush;
			_error = true;
			return 0;
		}
	}

	// check if exactly one of last 4 params have value

	AccountAction accountAction = AccountAction_Null;
	unsigned long actionAmount = 0UL;

	for (int pi=(paramCount-4); pi<paramCount; pi++) {
		Param& param = *params[pi];

		if (accountAction != AccountAction_Null && param._hasValue) {
			cerr << "\tERROR: Multiple account action parameters " << param._flag << endl << flush;
			_error = true;
			return 0;
		}

		if (accountAction == AccountAction_Null && param._hasValue) {
			if (param._flag == paramNewAccount._flag) {
				accountAction = AccountAction_New;
				actionAmount = paramNewAccount._value;
			} else if (param._flag == paramAccountDeposit._flag) {
				accountAction = AccountAction_Deposit;
				actionAmount = paramAccountDeposit._value;
			} else if (param._flag == paramAccountWithdrawal._flag) {
				accountAction = AccountAction_Withdrawal;
				actionAmount = paramAccountWithdrawal._value;
			} else if (param._flag == paramAccountBalance._flag) {
				accountAction = AccountAction_Balance;
			}
		}
	}
	if (accountAction == AccountAction_Null) {
		cerr << "\tERROR: No account action parameter" << endl << flush;
		_error = true;
		return 0;
	}
/*
	// cerr << "\tINFO: Atm parameters = " << paramAuthFile._flag << " " << paramAuthFile._value << " " << paramIpAddress._flag << " " << paramIpAddress._dotValue << " " << paramPort._flag << " " << paramPort._value << " " << paramCardFile._flag << " " << paramCardFile._value << " " << paramAccountId._flag << " " << paramAccountId._value << " ";

	if (paramNewAccount._hasValue)
			cerr << paramNewAccount._flag << " " << CurrencyAmountWrapper(paramNewAccount._value);
	if (paramAccountDeposit._hasValue)
			cerr << paramAccountDeposit._flag << " " << CurrencyAmountWrapper(paramAccountDeposit._value);
	if (paramAccountWithdrawal._hasValue)
			cerr << paramAccountWithdrawal._flag << " " << CurrencyAmountWrapper(paramAccountWithdrawal._value);
	if (paramAccountBalance._hasValue)
			cerr << paramAccountBalance._flag;
	cerr << endl << flush;
*/
	return new Atm(
			paramAuthFile._value,
			paramIpAddress._dotValue,
			(unsigned short) paramPort._value,
			paramCardFile._value,
			paramAccountId._value,
			accountAction,
			actionAmount);

}


