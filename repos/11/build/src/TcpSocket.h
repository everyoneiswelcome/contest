/*
 * TcpSocket.h
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#ifndef SRC_TCPSOCKET_H_
#define SRC_TCPSOCKET_H_

#include <string>

using namespace std;

enum SocketType { SocketType_Null, SocketType_Listener, SocketType_Connection };

struct TcpSocket {
	TcpSocket(const char* name);
	TcpSocket(const char* name, bool nonBlocking, int fdSocket, SocketType socketType);

	bool nonBlocking(bool nonBlocking);

	//int processSocket() = 0;

	bool closeSocket();

	const char* _name;
	bool _onHeap;
	bool _nonBlocking;
	int _fdSocket;
	SocketType _socketType;
};

#endif /* SRC_TCPSOCKET_H_ */
