/*
 * Regex.h
 *
 *  Created on: May 19, 2015
 *      Author: singh.rajiv
 */

#ifndef REGEX_H_
#define REGEX_H_

#include <string>
#include <sys/types.h>
#include <regex.h>

using namespace std;

struct Regex {
	Regex(const string& strRegex);
	~Regex();

	bool match(const string& strTarget);

	::regex_t _regex;
	::regmatch_t _matches[5];
	string _strRegex;
};




#endif /* REGEX_H_ */
