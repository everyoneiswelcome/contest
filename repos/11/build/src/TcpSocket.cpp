/*
 * TcpSocket.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#include "TcpSocket.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <iostream>

TcpSocket::TcpSocket(const char* name)
	: _name(name), _nonBlocking(true), _fdSocket(0), _socketType(SocketType_Null) {
}

TcpSocket::TcpSocket(const char* name, bool nonBlocking, int fdSocket, SocketType socketType)
	: _name(name), _nonBlocking(nonBlocking), _fdSocket(fdSocket), _socketType(socketType) {
}

bool TcpSocket::nonBlocking(bool nonBlocking) {
	if (_fdSocket <= 0)
		return false;

	_nonBlocking = nonBlocking;

    // non-blocking
    int fcntlFlags = ::fcntl(_fdSocket, F_GETFL, 0);
    if (fcntlFlags == -1)
    	fcntlFlags = 0;

    fcntlFlags = _nonBlocking ? (fcntlFlags | O_NONBLOCK) : (fcntlFlags & ~O_NONBLOCK);
    int retFcntl = ::fcntl(_fdSocket, F_SETFL, fcntlFlags);
    if (retFcntl == -1) {
    	cerr << "\tERR nonBlocking failure on client socket" << endl << flush;
    	return false;
    }

    return true;
}

bool TcpSocket::closeSocket() {
    if (_fdSocket > 0) {
    	//cerr << "\tINFO: " << _name << " closeSocket fd=" << _fdSocket << endl << flush;
    	int retClose = ::close(_fdSocket);
    	if (retClose != 0) {
    		cerr << "\tERROR: " << _name << " close() Failed. errno=" << errno << endl << flush;
    	}
    	_fdSocket = 0;
     }
	return true;
}


