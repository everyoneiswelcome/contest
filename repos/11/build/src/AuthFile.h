/*
 * AuthFile.h
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#ifndef AUTHFILE_H_
#define AUTHFILE_H_

#include <string>

using namespace std;

struct AuthFile {
	AuthFile(const string& fileName);

	bool getFileContent();
	bool putFileContent();
	bool deleteFile();

	string _fileName;
	string _fileContent;
	bool _created;
};

#endif /* AUTHFILE_H_ */
