/*
 * Common.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#include "Common.h"

#include <sstream>
#include <stdexcept>
#include <stdint.h>

using namespace std;

ostream& operator<<(ostream& os, const Buffer2Hex& b2h) {
	os << b2h._size << ":";
	if (b2h._middleBytes <= 0) {
		for (uint16_t i = 0; i < b2h._size; ++i) {
			os << hex << setw(2) << setfill('0') << static_cast<unsigned int>(static_cast<unsigned char>(b2h._b[i]));
		}
	} else {
		for (uint16_t i = 0; i < b2h._beginBytes; ++i) {
			os << hex << setw(2) << setfill('0') << static_cast<unsigned int>(static_cast<unsigned char>(b2h._b[i]));
		}
    	os << "-";
		for (uint16_t i = b2h._size - b2h._endBytes; i < b2h._size; ++i) {
			os << hex << setw(2) << setfill('0') << static_cast<unsigned int>(static_cast<unsigned char>(b2h._b[i]));
		}
	}
    os << dec;
    return os;

}

const char* getName(IoState is) {
	switch(is) {
	case IoState_Null: return "Null";
	case IoState_Complete: return "Complete";
	case IoState_EOF: return "EOF";
	case IoState_InComplete: return "InComplete";
	case IoState_InCompleteHeader: return "InCompleteHeader";
	case IoState_InCompleteBody: return "InCompleteBody";
	case IoState_InCompleteRecordRead: return "InCompleteRecordRead";
	case IoState_MaxRecordSizeExceeded: return "MaxRecordSizeExceeded";
	case IoState_AbruptEOF: return "AbruptEOF";
	case IoState_TimedOut: return "TimedOut";
	case IoState_SocketError: return "SocketError";
	case IoState_SecurityError: return "SecurityError";
	case IoState_InvalidSessionKey: return "InvalidSessionKey";
	case IoState_LogicError: return "LogicError";
	default: return "???";
	}
}

Elapsed elapsed;

string string_to_hex(const string& in) {
    stringstream ss;

    ss << hex << setw(2) << setfill('0');
    for (size_t i = 0; in.size() > i; ++i) {
        ss << static_cast<unsigned int>(static_cast<unsigned char>(in[i]));
    }

    return ss.str();
}

ostream& operator<<(ostream& os, const String2Hex& s2h) {
	if (s2h._middleBytes <= 0) {
		for (size_t i = 0; i < s2h._s.size(); ++i) {
			os << hex << setw(2) << setfill('0') << static_cast<unsigned int>(static_cast<unsigned char>(s2h._s[i]));
		}
	} else {
		for (size_t i = 0; i < s2h._beginBytes; ++i) {
			os << hex << setw(2) << setfill('0') << static_cast<unsigned int>(static_cast<unsigned char>(s2h._s[i]));
		}
    	os << "-";
		for (size_t i = s2h._s.size() - s2h._endBytes; i < s2h._s.size(); ++i) {
			os << hex << setw(2) << setfill('0') << static_cast<unsigned int>(static_cast<unsigned char>(s2h._s[i]));
		}
	}
    os << dec;
    return os;
}

/*

ostream& operator<<(ostream& os, const IoRecord2Hex& r2h) {
	if (r2h._middleBytes <= 0) {
		for (uint16_t i = 0; i < r2h._r._byteCount; ++i) {
			os << hex << setw(2) << setfill('0') << static_cast<unsigned int>(static_cast<unsigned char>(r2h._r._bytes[i]));
		}
	} else {
		for (uint16_t i = 0; i < r2h._beginBytes; ++i) {
			os << hex << setw(2) << setfill('0') << static_cast<unsigned int>(static_cast<unsigned char>(r2h._r._bytes[i]));
		}
    	os << "-";
		for (uint16_t i = r2h._r._byteCount - r2h._endBytes; i < r2h._r._byteCount; ++i) {
			os << hex << setw(2) << setfill('0') << static_cast<unsigned int>(static_cast<unsigned char>(r2h._r._bytes[i]));
		}
	}
    os << dec;
    return os;
}

string hex_to_string(const string& in) {
    string output;

    if ((in.size() % 2) != 0) {
        throw runtime_error("String is not valid length ...");
    }

    size_t cnt = in.size() / 2;

    for (size_t i = 0; cnt > i; ++i) {
        uint32_t s = 0;
        stringstream ss;
        ss << hex << in.substr(i * 2, 2);
        ss >> s;

        output.push_back(static_cast<unsigned char>(s));
    }

    return output;
}
*/
