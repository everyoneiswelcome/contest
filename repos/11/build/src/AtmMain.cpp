/*
 * AtmMain.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

//#define _EXCLUDE_ATMMAIN
#ifndef _EXCLUDE_ATMMAIN

#include <iostream>
#include <exception>
#include <string>
#include <vector>
#include <signal.h>
#include <unistd.h>

using namespace std;

#include "CmdLineParser.h"
#include "Atm.h"
#include "Common.h"

Atm* atm = 0;

int main(int argc, char* argv[]);

void signalHandler(int signalNumber)
{
  if (signalNumber == SIGTERM) {
	  //cerr << endl << endl << "\tINFO: Shutdown requested with SIGTERM ..." << endl << flush;
	  if (atm != 0)
		  atm->_shutdownRequested = true;
  } else if (signalNumber == SIGINT) {
	  //cerr << "\tINFO: Shutdown requested with SIGINT..." << endl << flush;
	  if (atm != 0)
		  atm->_shutdownRequested = true;
  }
}

int main(int argc, char* argv[]) {
	int retAtm = 255;
	try {
		//cerr << endl << "    [ Starting " <<  Clock() << " ( "; for (int i=0; i<argc; i++) cerr << " " << argv[i] << " "; cerr << " )" << endl << flush;

		// install signal handlers
		if (::signal(SIGTERM, signalHandler) == SIG_ERR)
			cerr << "\tERROR: Failed to add SIGTERM signal handler" << endl << flush;
		if (::signal(SIGINT, signalHandler) == SIG_ERR)
			cerr << "\tERROR: Failed to add SIGINT signal handler" << endl << flush;
		if (::signal(SIGPIPE, SIG_IGN) == SIG_ERR)
			cerr << "\tERROR: Failed to ignore SIGPIPE signal" << endl << flush;

		vector<string> parameters(argv+1, argv+argc);
		CmdLineParser cmdLineParser(parameters);

		atm = cmdLineParser.createAtm();
		if (atm == 0) {
			cerr << "\tERROR: Parameters error" << endl << flush;
			//cerr << "    ] " << argv[0] << " Returning " << retAtm << endl << flush;
			return retAtm;
		}

		if (!atm->start()) {
			cerr << "\tERROR: Start failure" << endl << flush;
			//cerr << "    ] " << argv[0] << " Returning " << retAtm << endl << flush;
			return retAtm;
		}

		retAtm = atm->run();

	} catch (const exception& e) {
		cerr << "  ERROR: " << "Exiting with Exception = " << e.what() << endl << endl << endl << flush;
	} catch (...) {
		cerr << "  ERROR: " << "Exiting with Exception = ..." << endl << endl << endl << flush;
	}

	//cerr << "    ] " << Clock() << " " << argv[0] << " Returning " << retAtm << endl << endl << flush;
	//cerr << "\tINFO: " << "Exit(" << retAtm << ")" << endl << flush;
	return retAtm;
}

#endif //_EXCLUDE_ATMMAIN

