/*
 * AuthFile.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#include "AuthFile.h"

#include <iostream>
#include <cstdio>
#include <fstream>

using namespace std;

AuthFile::AuthFile(const string& fileName)
:
	_fileName(fileName),
	_fileContent(),
	_created(false)
{

}

bool AuthFile::getFileContent() {
	ifstream file(_fileName.c_str());
	if (!file.is_open())
	  return false;
	string content((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
	file.close();
	_fileContent = content;
	return true;
}

bool AuthFile::putFileContent() {
	FILE * pFile = ::fopen (_fileName.c_str(), "rb");
	if (pFile != 0) {
		::fclose(pFile);
		cerr << "\tERROR: AuthFile write failure. " << _fileName << " already exists." << endl << flush;
		return false;
	}

	pFile = ::fopen (_fileName.c_str(), "wb");
	if (pFile != 0) {
		::fwrite (_fileContent.c_str() , sizeof(char), _fileContent.size(), pFile);
		::fclose (pFile);
		_created = true;
		return true;
	} else {
		cerr << "\tERROR: AuthFile write failure. " << _fileName << " could not be opened for writing." << endl << flush;
	}

	return false;
}

bool AuthFile::deleteFile() {
	bool bRet = true;
	if (_created) {
		int retDelete = ::remove(_fileName.c_str());
		bRet = (retDelete == 0);
	}
	return bRet;
}
