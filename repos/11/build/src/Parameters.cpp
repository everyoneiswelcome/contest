/*
 * Parameters.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#include "Parameters.h"
#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <cstdlib>
#include <iterator>
#include <algorithm>
#include <fstream>

using namespace std;

Regex AllRegex::_reNumber("^(0|[1-9][0-9]*)$");
Regex AllRegex::_reAmount("^(0|[1-9][0-9]*)\\.([0-9][0-9])$");
Regex AllRegex::_reName("^([0-9a-z_]|\\-|\\.)+$");
Regex AllRegex::_reIpAddress("^(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)\\.(0|[1-9][0-9]*)$");

bool AllRegex::parseAmount(const string& arg, unsigned long& amount) {
	if (AllRegex::_reAmount.match(arg)) {
		regmatch_t* m = AllRegex::_reAmount._matches;
		string textValue = string(arg.c_str() + m[0].rm_so, arg.c_str() + m[0].rm_eo);

		unsigned long parts[2];
		for (int i=1; i<3; i++) {
			string part = string(arg.c_str() + m[i].rm_so, arg.c_str() + m[i].rm_eo);
			parts[i-1] = (unsigned long) ::strtoul(part.c_str(), 0, 10);
			//cerr << "Amount part " << i << " = " << part << " = " << parts[i-1] << endl << flush;
		}
		amount = parts[0] * 100 + parts[1];
		return true;
	} else
		return false;
};
