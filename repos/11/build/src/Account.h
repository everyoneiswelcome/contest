/*
 * Account.h
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#ifndef ACCOUNT_H_
#define ACCOUNT_H_

#include <string.h>
#include <iostream>

using namespace std;

#include "Common.h"

struct Account
{
	Account(char* accountId, unsigned char* cardKey, CurrencyAmount balance)
		:
			_accountId(),
			_cardKey(),
			_balance(balance)
	{
		strncpy(_accountId, accountId, ACCOUNT_ID_SIZE);
		memcpy(_cardKey, cardKey, CARD_KEY_SIZE);
	}

	char _accountId[ACCOUNT_ID_SIZE+1];
	unsigned char _cardKey[CARD_KEY_SIZE];
	CurrencyAmount _balance;
};

#endif /* ACCOUNT_H_ */
