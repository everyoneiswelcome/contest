/*
 * Parameters.h
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#ifndef PARAMETERS_H_
#define PARAMETERS_H_

#include <string>
#include <vector>
#include <cstdlib>
#include "Account.h"

using namespace std;

#include "Regex.h"

struct AllRegex {
	static Regex _reNumber;
	static Regex _reAmount;
	static Regex _reName;
	static Regex _reIpAddress;
	static bool parseAmount(const string& arg, unsigned long& amount);
};


struct Param {
	Param(const char* flag, Regex* pRegex, bool flagOnly, bool hasValue, bool hasDefault)
		: _flag(flag), _pRegex(pRegex), _flagOnly(flagOnly), _hasValue(hasValue), _hasDefault(hasDefault) {}

	string _flag;
	Regex* _pRegex;
	bool _flagOnly;
	bool _hasValue;
	bool _hasDefault;

	virtual bool parseValue(const string& arg) = 0;
	virtual bool isValid() = 0;
	virtual ~Param() {};
};

struct ParamPort : public Param {
	ParamPort() : Param("-p", &AllRegex::_reNumber, false, false, true), _value(3000) {}
	unsigned long _value;
	virtual bool parseValue(const string& arg) { _value = atoi(arg.c_str()); return true; };
	virtual bool isValid() { return _value >= 1024 && _value <= 65535; };
};

struct ParamAuthFile : public Param {
	ParamAuthFile() : Param("-s", &AllRegex::_reName, false, false, true), _value("bank.auth") {}
	string _value;
	virtual bool parseValue(const string& arg) { _value = arg; return true; };
	virtual bool isValid() { return _value.size() >= 1 && _value.size() <= 255
			&& _value.compare(".") != 0 && _value.compare("..") != 0; };
};

struct ParamIpAddress : public Param {
	ParamIpAddress() : Param("-i", &AllRegex::_reIpAddress, false, false, true),
			_dotValue("127.0.0.1"), _intValue(2130706433U) {
		_ipOctets[0] = 127;
		_ipOctets[1] = 0;
		_ipOctets[2] = 0;
		_ipOctets[3] = 1;
	}
	string _dotValue;
	unsigned int _ipOctets[4];
	unsigned int _intValue;
	virtual bool parseValue(const string& arg) {
		if (AllRegex::_reIpAddress.match(arg)) {
			regmatch_t* m = AllRegex::_reIpAddress._matches;
			_dotValue = string(arg.c_str() + m[0].rm_so, arg.c_str() + m[0].rm_eo);

			for (int i=1; i<5; i++) {
				string octet = string(arg.c_str() + m[i].rm_so, arg.c_str() + m[i].rm_eo);
				_ipOctets[i-1] = (unsigned int) ::atoi(octet.c_str());
			}
			_intValue = ((((((_ipOctets[0] << 8) | _ipOctets[1]) << 8) | _ipOctets[2]) << 8) | _ipOctets[3]);
			return true;
		} else
			return false;
	};
	bool isValid() { return
			_ipOctets[0] >= 0 && _ipOctets[0] <= 255 &&
			_ipOctets[1] >= 0 && _ipOctets[1] <= 255 &&
			_ipOctets[2] >= 0 && _ipOctets[2] <= 255 &&
			_ipOctets[3] >= 0 && _ipOctets[3] <= 255; };
};

struct ParamCardFile : public Param {
	ParamCardFile() : Param("-c", &AllRegex::_reName, false, false, true), _value(".card") {}
	string _value;
	virtual bool parseValue(const string& arg) { _value = arg; return true; };
	virtual bool isValid() { return _value.size() >= 1 && _value.size() <= 255
			&& _value.compare(".") != 0 && _value.compare("..") != 0; };
};

struct ParamAccountId : public Param {
	ParamAccountId() : Param("-a", &AllRegex::_reName, false, false, false), _value() {}
	string _value;
	virtual bool parseValue(const string& arg) { _value = arg; return true; };
	virtual bool isValid() { return _value.size() >= 1 && _value.size() <= 250; };
};

struct ParamNewAccount : public Param {
	ParamNewAccount() : Param("-n", &AllRegex::_reAmount, false, false, false), _value(0UL) {}
	unsigned long _value;
	virtual bool parseValue(const string& arg) { return AllRegex::parseAmount(arg, _value); };
	virtual bool isValid() { return _value >= MinInitialBalance && _value <= MaxCurrencyAmount; };
};

struct ParamAccountDeposit : public Param {
	ParamAccountDeposit() : Param("-d", &AllRegex::_reAmount, false, false, false), _value(0UL) {}
	unsigned long _value;
	virtual bool parseValue(const string& arg) { return AllRegex::parseAmount(arg, _value); };
	virtual bool isValid() { return _value > MinCurrencyAmount && _value <= MaxCurrencyAmount; };
};

struct ParamAccountWithdrawal : public Param {
	ParamAccountWithdrawal() : Param("-w", &AllRegex::_reAmount, false, false, false), _value(0UL) {}
	unsigned long _value;
	virtual bool parseValue(const string& arg) { return AllRegex::parseAmount(arg, _value); };
	virtual bool isValid() { return _value > MinCurrencyAmount && _value <= MaxCurrencyAmount; };
};

struct ParamAccountBalance : public Param {
	ParamAccountBalance() : Param("-g", &AllRegex::_reAmount, true, false, false) {}
	virtual bool parseValue(const string& arg) { return true; };
	virtual bool isValid() { return true; };
};




#endif /* PARAMETERS_H_ */
