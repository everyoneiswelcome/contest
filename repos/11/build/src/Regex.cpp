/*
 * Regex.cpp
 *
 *  Created on: May 20, 2015
 *      Author: singh.rajiv
 */

#include "Regex.h"

#include <stdexcept>
#include <iostream>
using namespace std;

Regex::Regex(const string& strRegex) {
	int reti = ::regcomp(&_regex, strRegex.c_str(), REG_EXTENDED);
	if (reti != 0)
		throw runtime_error("Regex regcomp failed");
	_strRegex = strRegex;
}

Regex::~Regex() {
	::regfree(&_regex);
}

bool Regex::match(const string& strTarget) {
	int reti = ::regexec(&_regex, strTarget.c_str(), 5, _matches, 0);
	if (reti == 0) {
		//cerr << strTarget << " matched RE " << _strRegex << endl << flush;
		return true;
	}
	else if (reti == REG_NOMATCH) {
		//cerr << strTarget << " NOT matched RE " << _strRegex << endl << flush;
		return false;
	}
	else {
		//cerr << strTarget << " FAILED matched RE " << _strRegex << endl << flush;
		throw runtime_error("Regex match failed");
	}
}

