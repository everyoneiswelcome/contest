/*
 * TcpPoller.h
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#ifndef SRC_TCPPOLLER_H_
#define SRC_TCPPOLLER_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <errno.h>

#include <string>
#include <vector>

using namespace std;

struct TcpSocket;
struct ConnectionProcessor;

struct Bank;

struct TcpPoller {

	TcpPoller(const char* name);

	bool processSockets(ConnectionProcessor* connectionProcessor);

	bool addTcpSocket(TcpSocket* tcpSocket);

	void exit();

	const char* _name;

	//TcpConnection _tcpConnection[4];
	vector<TcpSocket*> _tcpSocketsToPoll;


	struct pollfd _fdToPoll[100];
	int _fdCountMax;
	int _fdCount;

	int _pollTimeout;
};



#endif /* SRC_TCPPOLLER_H_ */
