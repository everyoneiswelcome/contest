/*
 * Msg.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: rajiv
 */

#include "Msg.h"
#include "Common.h"
#include "Atm.h"
#include "AuthFile.h"
#include "CardFile.h"

#include <iostream>
#include <iomanip>
#include <ostream>

using namespace std;

const char* getName(MsgType mt) {
	switch(mt) {
	case MsgType_Null: return "Null";
	case MsgType_SessionKeyRequest: return "SessionKeyRequest";
	case MsgType_SessionKeyResponse: return "SessionKeyResponse";
	case MsgType_AccountActionRequest: return "AccountActionRequest";
	case MsgType_AccountActionResponse: return "AccountActionResponse";
	default: return "???";
	}
}

const char* getName(AccountAction aa) {
	switch(aa) {
	case AccountAction_Null: return "Null";
	case AccountAction_New: return "New";
	case AccountAction_Deposit: return "Deposit";
	case AccountAction_Withdrawal: return "Withdrawal";
	case AccountAction_Balance: return "Balance";
	default: return "???";
	}
}

const char* getName(FailureReason fr) {
	switch(fr) {
	case FR_Null: return "Null";
	case FR_AccountIdNotUnique: return "AccountIdNotUnique";
	case FR_MinInitialBalanceViolated: return "MinInitialBalanceViolated";
	case FR_MaxCurrencyAmountViolated: return "MaxCurrencyAmountViolated";
	case FR_AccountDoesNotExist: return "AccountDoesNotExist";
	case FR_CardFileAuthenticationFailed: return "CardFileAuthenticationFailed";
	case FR_DepositAmountNotPositive: return "DepositAmountNotPositive";
	case FR_InsufficientBalance: return "InsufficientBalance";
	case FR_InvalidAccountAction: return "InvalidAccountAction";
	case FR_UnexpectedAccountInsertFailure: return "UnexpectedAccountInsertFailure";
	default: return "???";
	}
}

Msg::Msg()
{
	clear();
}

void Msg::clear() {
	::memset(this, 0, sizeof(Msg));
}

ostream& operator<<(ostream& os, Msg& msg) {
	return os << "Msg ("
			<< " mt=" << msg._msgType << ":" << getName(msg._msgType)
			<< " sk=" << Buffer2Hex(msg._sessionKey, SESSION_KEY_SIZE, 2, 2)
			<< " ai=" << msg._accountId
			<< " ck=" << Buffer2Hex(msg._cardKey, CARD_KEY_SIZE, 2, 2)
			<< " aa=" << msg._accountAction << ":" << getName(msg._accountAction)
			<< " $=" << CurrencyAmountWrapper(msg._actionAmount)
			<< " s=" << msg._success
			<< " fr=" << msg._failureReason << ":" << getName(msg._failureReason)
			<< " )";
}

ostream& operator<<(ostream& os, const IoRecord& ior) {
	return os << "IoRecord( "
			<< " b=" << "TBD" // IoRecord2Hex(ior, 2, 2)
			<< " s=" << getName(ior._state)
			<< " #=" << ior._bytesDone << "/" << ior._bytesTotal
			<< " et=" << ior._endTime
			<< " )";
}

