/*
 * TcpConnection.h
 *
 *  Created on: Oct 4, 2015
 *      Author: rajiv
 */

#ifndef SRC_TCPCONNECTION_H_
#define SRC_TCPCONNECTION_H_

#include "TcpSocket.h"
#include "Common.h"
#include "Msg.h"

#include <string>
#include <time.h>

using namespace std;

struct Security;
struct Account;


enum ReadWriteState {
	RWS_Null,
	RWS_Reading,
	RWS_Read,
	RWS_Writing,
	RWS_Written,
	RWS_ExpectingEof,
	RWS_Eof,
	RWS_Error
};
const char* getName(ReadWriteState rws);

enum TransactionState {
	TS_Null,
	TS_Begin,
	TS_SessionKeyRequest,
	TS_SessionKeyResponse,
	TS_AccountActionRequest,
	TS_AccountActionResponse,
	TS_Commit,
	TS_End,
	TS_Error
};
const char* getName(TransactionState ts);

struct TcpConnection : public TcpSocket {
	TcpConnection(const char* name, Security* security);
	TcpConnection(const char* name, Security* security, int fdConnectionSocket);

	bool connect(const char* ipAddress, unsigned short port);
	void shutdownConnection(int how);

	ReadWriteState _rwState;

	TransactionState _transactionState;

	IoState readMsg();
	IoState writeMsg();

	Security* _security;

	// session key
	unsigned char _sessionKey[SESSION_KEY_SIZE];

	// non-blocking read
	IoRecord _rRecord;

	// non-blocking write
	IoRecord _wRecord;
};

#endif /* SRC_TCPCONNECTION_H_ */



