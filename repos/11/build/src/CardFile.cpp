/*
 * CardFile.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#include "CardFile.h"

#include <iostream>
#include <cstdio>
#include <fstream>

using namespace std;

CardFile::CardFile(const string& fileName)
:
	_fileName(fileName),
	_fileContent()
{
}

CardFile::CardFile(const string& fileName, const string& fileContent)
:
	_fileName(fileName),
	_fileContent(fileContent)
{
}

bool CardFile::fileExists() {
	FILE* pFile = fopen (_fileName.c_str(), "rb");
	if (pFile != 0) {
		fclose(pFile);
		//cerr << "\tINFO: CardFile " << _fileName << " exists." << endl << flush;
		return true;
	} else {
		//cerr << "\tINFO: CardFile " << _fileName << " does not exist." << endl << flush;
		return false;
	}
}

bool CardFile::getFileContent() {
	ifstream file(_fileName.c_str());
	if (!file.is_open())
	  return false;
	string content((istreambuf_iterator<char>(file)), istreambuf_iterator<char>());
	file.close();
	_fileContent = content;
	return true;
}

bool CardFile::putFileContent() {
	if (fileExists()) {
		cerr << "\tERROR: CardFile Write failure. " << _fileName << " already exists." << endl << flush;
		return false;
	}

	FILE* pFile = fopen (_fileName.c_str(), "wb");
	if (pFile != 0) {
		fwrite (_fileContent.c_str() , sizeof(char), _fileContent.size(), pFile);
		fclose (pFile);
		return true;
	} else {
		cerr << "\tERROR: CardFile write failure. " << _fileName << " could not be opened for writing." << endl << flush;
	}

	return false;
}

