/*
 * Bank.h
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#ifndef BANK_H_
#define BANK_H_

#include <string>
#include <map>

using namespace std;

#include "Common.h"
#include "Account.h"
#include "AuthFile.h"
#include "TcpPoller.h"
#include "TcpListener.h"
#include "Msg.h"
#include "Security.h"

struct Bank : public ConnectionProcessor
{
	Bank(unsigned short port, const string& authFileName);

	bool start();
	bool stop();
	void run();

	virtual bool processConnection(TcpConnection* tcpConnection);
	void processAccountAction(Msg& msgIn, Msg& msgOut);

	static void outputAccountAction(const char* accountId, const char* amountKey, CurrencyAmount amountValue);

	void dump();

	const char* _name;
	Security _security;

	unsigned short _port;
	TcpListener* _tcpListener;
	TcpPoller _tcpPoller;
	bool _shutdownRequested;


	map<string, Account*> _accounts;
};




#endif /* BANK_H_ */
