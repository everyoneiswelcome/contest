/*
 * Atm.h
 *
 *  Created on: Oct 2, 2015
 *      Author: singh.rajiv
 */

#include "Atm.h"

#include <string>
#include <iostream>
#include <unistd.h>

using namespace std;

#include "Common.h"
#include "Msg.h"
#include "AuthFile.h"
#include "CardFile.h"
#include "Bank.h"
#include "TcpConnection.h"

Atm::Atm(
		const string& authFileName,
		const string& ipAddress,
		unsigned short port,
		const string& cardFileName,
		const string& accountId,
		AccountAction accountAction,
		unsigned long actionAmount)
:
	_name("AtmSocket"),
	_ipAddress(ipAddress),
	_port(port),
	_cardFile(cardFileName),
	_security(authFileName),
	_tcpConnection(0),
	_tcpPoller("AtmPoller"),
	_shutdownRequested(false),
	_atmReturnCode(10)

{
	_msg._msgType = MsgType_AccountActionRequest;
	strncpy(_msg._accountId, accountId.c_str(), ACCOUNT_ID_SIZE);
	_msg._accountAction = accountAction;
	_msg._actionAmount = actionAmount;
}

bool Atm::start() {

	if (_security._authFile.getFileContent()) {
		memcpy(_security._aesKey, _security._authFile._fileContent.c_str(), AUTH_KEY_SIZE);
		//cerr << "\tINFO: Read auth file " << _authFile._fileName << endl << flush;
	}
	else {
		cerr << "\tERROR: Failed to read auth file " << _security._authFile._fileName << endl << flush;
		return false;
	}

	if (_msg._accountAction == AccountAction_New) {
		if (_cardFile.fileExists()) {
			cerr << "\tERROR: CardFile " << _cardFile._fileName << " already exists." << endl << flush;
			return false;
		} else {
			//cerr << "\tINFO: CardFile " << _cardFile._fileName << " does not exists." << endl << flush;
		}

	} else {
		if (_cardFile.getFileContent()) {
			//cerr << "\tINFO: Read card file " << _cardFile._fileName << endl << flush;
		}
		else {
			cerr << "\tERROR: Failed to read card file " << _cardFile._fileName << endl << flush;
			return false;
		}
	}

	_tcpConnection = new TcpConnection(_name, &_security);

	if (_tcpConnection->connect(_ipAddress.c_str(), _port)) {
		_tcpConnection->_transactionState = TS_Begin;
		_tcpPoller.addTcpSocket(_tcpConnection);
	}
	else {
		cerr << "\tERROR: Failed to connect to bank" << endl << flush;
		return false;
	}

	return true;
}

bool Atm::processConnection(TcpConnection* tcpConnection) {
	bool disposeSocket = false;

	ReadWriteState& rwState = tcpConnection->_rwState;
	TransactionState& tState = tcpConnection->_transactionState;
	Msg& msgIn = tcpConnection->_rRecord._msg;
	Msg& msgOut = tcpConnection->_wRecord._msg;

	//cerr << "\tINFO " << Clock() << ": Enter sm " << tcpConnection->_name << " ts=" << getName(tState) << " rws=" << getName(rwState) << endl << flush;

	if (rwState == RWS_Error) {
		tState = TS_Error;
	}

	switch(tState)
	{
	case TS_Null:
		tState = TS_Begin;
		break;
	case TS_Begin:
		tState = TS_SessionKeyRequest;
		break;
	case TS_SessionKeyRequest:
		switch(rwState) {
		case RWS_Null:
			msgOut._msgType = MsgType_SessionKeyRequest;
			memcpy(msgOut._accountId, _msg._accountId, ACCOUNT_ID_SIZE+1);
			msgOut._success = true;
			rwState = RWS_Writing;
			break;
		case RWS_Writing:
			break;
		case RWS_Written:
			rwState = RWS_Null;
			tState = TS_SessionKeyResponse;
			break;
		default:
			tState = TS_Error;
		}
		break;
	case TS_SessionKeyResponse:
		switch(rwState) {
		case RWS_Null:
			rwState = RWS_Reading;
			break;
		case RWS_Reading:
			break;
		case RWS_Read:
			rwState = RWS_Null;
			if (msgIn._msgType == MsgType_SessionKeyResponse) {
				memcpy(tcpConnection->_sessionKey, msgIn._sessionKey, SESSION_KEY_SIZE);
				tState = TS_AccountActionRequest;
			} else {
				tState = TS_Error;
			}
			break;
		default:
			tState = TS_Error;
		}
		break;
	case TS_AccountActionRequest:
		switch(rwState) {
		case RWS_Null:
			toAccountActionMsg(msgOut);
			rwState = RWS_Writing;
			break;
		case RWS_Writing:
			break;
		case RWS_Written:
			rwState = RWS_Null;
			tState = TS_AccountActionResponse;
			break;
		default:
			tState = TS_Error;
		}
		break;
	case TS_AccountActionResponse:
		switch(rwState) {
		case RWS_Null:
			rwState = RWS_Reading;
			break;
		case RWS_Reading:
			break;
		case RWS_Read:
			rwState = RWS_Null;
			if (msgIn._msgType == MsgType_AccountActionResponse) {
				_atmReturnCode = processAccountActionResponse(msgIn);
				//cerr << "\tINFO: " << "Exit(" << _atmReturnCode << ")" << endl << flush;
				exit(_atmReturnCode); // just efficient ! no cleanup needed.
			} else {
				tState = TS_Error;
			}
			break;
		default:
			tState = TS_Error;
		}
		break;
	case TS_Commit:
		_atmReturnCode = processAccountActionResponse(msgIn);
		//cerr << "\tINFO: " << "Exit(" << _atmReturnCode << ")" << endl << flush;
		exit(_atmReturnCode); // just efficient ! no cleanup needed.
		break;
	case TS_End:
		rwState = RWS_Null;
		disposeSocket = true;
		break;
	case TS_Error:
		rwState = RWS_Null;
		_atmReturnCode = 63;
		//cerr << "\tINFO: " << "Exit(" << _atmReturnCode << ")" << endl << flush;
		exit(_atmReturnCode); // just efficient ! no cleanup needed.
		disposeSocket = true;
		break;
	}

	//cerr << "\tINFO " << Clock() << ":  Exit sm " << tcpConnection->_name << " ts=" << getName(tState) << " rws=" << getName(rwState) << (disposeSocket ? " --- DISPOSE ---" : "") << endl << flush;

	return disposeSocket;
}

int Atm::run() {
	while (_tcpPoller.processSockets(this)) {

		if (_shutdownRequested)
			break;

	}
	_tcpPoller.exit();

	return _atmReturnCode;
}

int Atm::processAccountActionResponse(const Msg& rcvMsg) {
	if (!rcvMsg._success) {
		//cerr << "\tINFO: FailureReason = " << rcvMsg._failureReason << endl << flush;
		return 255;
	} else {
		//cerr << "\tBANK: RequestSuccessful" << endl << flush;
	}

	if (rcvMsg._accountAction == AccountAction_New) {
		_cardFile._fileContent = string((char *) rcvMsg._cardKey, CARD_KEY_SIZE);
		if (_cardFile.putFileContent()) {
			//cerr << "\tINFO: Wrote card file " << _cardFile._fileName << endl << flush;
		}
		else {
			cerr << "\tERROR: Failed to write card file " << _cardFile._fileName << endl << flush;
			return 255;
		}
	}

	switch (rcvMsg._accountAction) {
	case AccountAction_New:
		outputAccountAction("initial_balance", rcvMsg._actionAmount);
		break;
	case AccountAction_Deposit:
		outputAccountAction("deposit", rcvMsg._actionAmount);
		break;
	case AccountAction_Withdrawal:
		outputAccountAction("withdraw", rcvMsg._actionAmount);
		break;
	case AccountAction_Balance:
		outputAccountAction("balance", rcvMsg._actionAmount);
		break;
	default:
		cerr << "\tERROR: Invalid accountAction " << _msg._accountAction << endl << flush;
		return 255;
		break;
	}

	return 0;
}

void Atm::dump() {
	cerr << "AtmParams: authFile=" << _security._authFile._fileName
			<< ", ipAddress=" << _ipAddress
			<< ", port=" << _port
			<< ", cardFile=" << _cardFile._fileName
			<< ", accountId=" << _msg._accountId
			<< ", accountAction=" << _msg._accountAction
			<< ", actionAmount=" << _msg._actionAmount
			<< endl << flush;
}

void Atm::outputAccountAction(const char* amountKey, CurrencyAmount amountValue) const {
	printf("{\"account\":\"%s\",\"%s\":%ld.%02ld}\n", _msg._accountId, amountKey, amountValue/100, amountValue%100);
	fflush(stdout);
}

void Atm::toAccountActionMsg(Msg& msg){
	memcpy(_msg._cardKey, _cardFile._fileContent.c_str(), CARD_KEY_SIZE);
	memcpy(&msg, &_msg, MSG_SIZE);
}

