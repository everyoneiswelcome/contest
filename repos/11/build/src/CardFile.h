/*
 * CardFile.h
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#ifndef CARDFILE_H_
#define CARDFILE_H_

#include <string>

using namespace std;

struct CardFile
{
	CardFile(const string& fileName);
	CardFile(const string& fileName, const string& fileContent);

	bool fileExists();
	bool getFileContent();
	bool putFileContent();

	string _fileName;
	string _fileContent;
};



#endif /* CARDFILE_H_ */
