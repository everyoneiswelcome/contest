/*
 * TcpPoll.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#include "TcpPoller.h"
#include "Msg.h"
#include "TcpSocket.h"
#include "TcpListener.h"
#include "TcpConnection.h"
#include "Bank.h"
#include "Common.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <errno.h>
#include <time.h>
#include <memory.h>
#include <iostream>


using namespace std;

TcpPoller::TcpPoller(const char* name)
	:
	_name(name),
	_tcpSocketsToPoll(),
	_fdToPoll(),
	_fdCountMax(0),
	_fdCount(0),
	_pollTimeout(100)

{
	// initialize pollfd structures
	::memset((void*) _fdToPoll, 0 , sizeof(_fdToPoll));
	_fdCountMax = sizeof(_fdToPoll) / sizeof(struct pollfd);
}

bool TcpPoller::processSockets(ConnectionProcessor* connectionProcessor) {

	TcpSocket* tcpSocket = 0;
	TcpConnection* tcpConnection = 0;
	TcpListener* tcpListener = 0;

	/////////////////////////////////////////////////////////////////////////////////////
	// process connections

	for (int i=0; i<(int)_tcpSocketsToPoll.size(); i++) {

		tcpSocket = _tcpSocketsToPoll[i];
		if (tcpSocket->_socketType == SocketType_Listener) {
			continue;
		}

		tcpConnection = (TcpConnection*) _tcpSocketsToPoll[i];
		bool disposeSocket = false;
		// if disposeSocket false and non-io state keep calling
		while (true) {
			disposeSocket = connectionProcessor->processConnection(tcpConnection);
			if (disposeSocket)
				break;
			if (tcpConnection->_rwState == RWS_Reading ||
					tcpConnection->_rwState == RWS_Writing ||
					tcpConnection->_rwState == RWS_ExpectingEof)
				break;
		}

		if (disposeSocket) {
			//cerr << "\tINFO:  Disposing socket " << tcpConnection->_name << " ts=" << getName(tcpConnection->_transactionState) << " rws=" << getName(tcpConnection->_rwState) << endl << flush;
			tcpConnection->closeSocket();
			delete tcpConnection;
			_tcpSocketsToPoll.erase(_tcpSocketsToPoll.begin() + i);
			i--;
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////
	// prepare for polling

	_fdCount = 0;
	for (int i=0; i<(int)_tcpSocketsToPoll.size(); i++) {
		if (_fdCount < _fdCountMax) {
			int events = 0;
			tcpSocket = _tcpSocketsToPoll[i];
			switch(tcpSocket->_socketType) {
			case SocketType_Listener:
				events = POLLIN;
				_fdToPoll[_fdCount].fd = tcpSocket->_fdSocket;
				_fdToPoll[_fdCount].events = events;
				break;
			case SocketType_Connection:
				tcpConnection = (TcpConnection*) tcpSocket;
				switch (tcpConnection->_rwState) {
				case RWS_Reading: events = POLLIN; break;
				case RWS_Writing: events = POLLOUT; break;
				case RWS_ExpectingEof: events = POLLIN; break;
				default: break;
				}
				_fdToPoll[_fdCount].fd = tcpSocket->_fdSocket;
				_fdToPoll[_fdCount].events = events;
				break;
			case SocketType_Null:
				break;
			}
			_fdCount += 1;
		} else {
			_fdCount = -1;
			cerr << "\tERROR: " << _name << " TcpPoll : _fdToPoll overflowed" << endl << flush;
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////
	// poll sockets

	int retPoll = 0;

	if (_fdCount > 0) {
		//cerr << "\tINFO " << Clock() << " : TcpPoller poll _fdCount=" << _fdCount << endl << flush;
		retPoll = ::poll(_fdToPoll, _fdCount, _pollTimeout);
		//cerr << "\tINFO " << Clock() << " : TcpPoller poll returned. retPoll=" << retPoll << endl << flush;

		if (retPoll < 0) { // poll failed
			if (errno != EINTR)
				cerr << "\tERROR: TcpPoller poll SocketError. errno=" << errno << endl << flush;
		} else if (retPoll == 0) {
			//cerr << "\tINFO " << Clock() << " : TcpPoller poll timed-out. retPoll=" << retPoll << endl << flush;
		} else {
			//cerr << "\tINFO " << Clock() << " : TcpPoller poll success. retPoll=" << retPoll << endl << flush;
		}
	}

	for (int i=0; i<(int)_tcpSocketsToPoll.size(); i++) {

		tcpSocket = _tcpSocketsToPoll[i];
		if (tcpSocket == 0) {
			continue;
		}
		if (tcpSocket == 0)
			continue;

		IoState ioState = IoState_Null;
		switch (tcpSocket->_socketType) {
		case SocketType_Connection:
			tcpConnection = (TcpConnection*) tcpSocket;
			switch (tcpConnection->_rwState) {
			case RWS_Reading:
				ioState = tcpConnection->readMsg();
				if (ioState == IoState_Complete) {
					tcpConnection->_rwState = RWS_Read;
				} else if (ioState > IOSTATE_INCOMPLETE_GT) {
					// keep spinning
				} else {
					tcpConnection->_rwState = RWS_Error;
				}
				break;
			case RWS_Writing:
				ioState = tcpConnection->writeMsg();
				if (ioState == IoState_Complete) {
					tcpConnection->_rwState = RWS_Written;
				} else if (ioState > IOSTATE_INCOMPLETE_GT) {
					// keep spinning
				} else {
					tcpConnection->_rwState = RWS_Error;
				}
				break;
			case RWS_ExpectingEof:
				ioState = tcpConnection->readMsg();
				//cerr << "\tINFO: RWS_ExpectingEof ioState=" << getName(ioState) << endl << flush;
				if (ioState > IOSTATE_INCOMPLETE_GT) {
					// keep spinning
				} else if (ioState == IoState_EOF) {
					tcpConnection->_rwState = RWS_Eof;
				} else {
					tcpConnection->_rwState = RWS_Error;
				}
				break;
			default:
				break;
			}
			break;
		case SocketType_Listener:
			tcpListener = (TcpListener*) tcpSocket;
			tcpListener->acceptAllConnections(_tcpSocketsToPoll);
			//cerr << "   " << "TcpPoll : activity on SocketType_Listener, New connections = " << connectionsAccepted << endl << flush;
			break;
		case SocketType_Null:
			break;
		}
	}

	return !_tcpSocketsToPoll.empty();
}

void TcpPoller::exit() {
	for (int i=0; i<(int)_tcpSocketsToPoll.size(); i++) {
		TcpSocket* tcpSocket = _tcpSocketsToPoll[i];
		if (tcpSocket == 0) {
			continue;
		}
		tcpSocket->closeSocket();
		delete tcpSocket;
	}
	_tcpSocketsToPoll.clear();
}

bool TcpPoller::addTcpSocket(TcpSocket* tcpSocket) {
	if (_fdCount < _fdCountMax) {
		_fdToPoll[_fdCount].fd = tcpSocket->_fdSocket;
		_fdToPoll[_fdCount].events = POLLIN | POLLOUT;
		_fdCount += 1;
		_tcpSocketsToPoll.push_back(tcpSocket);
		return true;
	} else {
		tcpSocket->closeSocket();
		delete tcpSocket;
		return false;
	}
}

