/*
 * BankMain.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

//#define _EXCLUDE_BANKMAIN
#ifndef _EXCLUDE_BANKMAIN

#include <iostream>
#include <exception>
#include <string>
#include <vector>
#include <signal.h>
#include <unistd.h>

using namespace std;

#include "Bank.h"
#include "Common.h"
#include "CmdLineParser.h"
#include "Security.h"

Bank* bank = 0;

int main(int argc, char* argv[]);

void signalHandler(int signalNumber)
{
  if (signalNumber == SIGTERM) {
	  //cerr << endl << endl << "\tINFO: Shutdown requested with SIGTERM ..." << endl << flush;
	  if (bank != 0)
		  bank->_shutdownRequested = true;
  } else if (signalNumber == SIGINT) {
	  //cerr << "\tINFO: Shutdown requested with SIGINT..." << endl << flush;
	  if (bank != 0)
		  bank->_shutdownRequested = true;
  }

}

int main(int argc, char* argv[]) {
	try {
		//cerr << endl << endl << "  [ Starting " <<  Clock() << " ( "; for (int i=0; i<argc; i++) cerr << " " << argv[i] << " "; cerr << " )" << endl << flush;

		// install signal handlers
		if (::signal(SIGTERM, signalHandler) == SIG_ERR)
			cerr << "\tERROR: Failed to add SIGTERM signal handler" << endl << flush;
		if (::signal(SIGINT, signalHandler) == SIG_ERR)
			cerr << "\tERROR: Failed to add SIGINT signal handler" << endl << flush;
		if (::signal(SIGPIPE, SIG_IGN) == SIG_ERR)
			cerr << "\tERROR: Failed to ignore SIGPIPE signal" << endl << flush;


		vector<string> parameters(argv+1, argv+argc);
		CmdLineParser cmdLineParser(parameters);
		bank = cmdLineParser.createBank();
		if (bank == 0) {
			cerr << "\tERROR: Bank CmdLine error" << endl << flush;
			cerr << "  ] " << argv[0] << " Returning 255" << endl << flush;
			return 255;
		}

		if (!bank->start()) {
			cerr << "\tERROR: Bank start error" << endl << flush;
			cerr << "  ] " << argv[0] << " Returning 255" << endl << flush;
			return 255;
		}

		bank->run();

		bank->stop();

	} catch (const exception& e) {
		cerr << "  ERROR: " << "Exiting with Exception = " << e.what() << endl << endl << endl << flush;
	} catch (...) {
		cerr << "  ERROR: " << "Exiting with Exception = ..." << endl << endl << endl << flush;
	}

	//cerr << "  ] " << Clock() << " " << argv[0] << " Returning 0" << endl << endl << endl << flush;
	return 0;
}

#endif // _EXCLUDE_BANKMAIN


