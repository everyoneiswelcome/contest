/*
 * TcpListener.h
 *
 *  Created on: Oct 4, 2015
 *      Author: rajiv
 */

#ifndef SRC_TCPLISTENER_H_
#define SRC_TCPLISTENER_H_

#include "TcpSocket.h"

struct TcpConnection;
struct Security;

#include <vector>
using namespace std;

struct TcpListener : public TcpSocket {
	TcpListener(const char* name, Security* security, unsigned short port);

	bool startListening();
	TcpConnection* acceptConnection();
	int acceptAllConnections(vector<TcpSocket*>& tcpSockets);
	int processSocket();
	bool stopListening();

	Security* _security;
	unsigned short _port;
};



#endif /* SRC_TCPLISTENER_H_ */
