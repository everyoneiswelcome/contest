/*
 * CmdLineParser.h
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#ifndef CMDLINEPARSER_H_
#define CMDLINEPARSER_H_

#include <string>
#include <vector>

using namespace std;

#include "Regex.h"

struct Bank;
struct Atm;
struct Param;

struct CmdLineParser {
	CmdLineParser(const vector<string>& args);

	bool parseParam(Param* pParam, const string& arg);

	Bank* createBank() ;
	Atm* createAtm() ;

	const vector<string>& _args;
	int _argCount;
	int _argIndex;
	bool _error;
};




#endif /* CMDLINEPARSER_H_ */
