/*
 * Bank.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */


#include "Bank.h"
#include "AuthFile.h"
#include "CardFile.h"
#include "TcpConnection.h"
#include <time.h>

#include "Msg.h"
#include "Security.h"

Bank::Bank(unsigned short port, const string& authFileName)
	:
	_name("Bank"),
	_security(authFileName),
	_port(port),
	_tcpListener(new TcpListener("BankSocket", &_security, port)),
	_tcpPoller("Bank"),
	_shutdownRequested(false),
	_accounts()
{
}

bool Bank::start() {

	// start listening for Atm connections
	_tcpListener->nonBlocking(true);
	if (_tcpListener->startListening()) {
		//cerr << "Bank: listening on port " << _tcpListener._port << endl << flush;
		bool retTcpPollAdd = _tcpPoller.addTcpSocket(_tcpListener);
		if (!retTcpPollAdd) {
			cerr << "\tERROR: TcpPoll overflowed" << endl << flush;
		}
	}
	else {
		cerr << "\tERROR: Failed to listen on port " << _tcpListener->_port << endl << flush;
		return false;
	}

	// create and save auth file
	_security.randomBytes(_security._aesKey, AES_BLOCK_SIZE);
	_security._authFile._fileContent = string((char*)_security._aesKey, AUTH_KEY_SIZE);
	if (_security._authFile.putFileContent()) {
		//cerr << "\tINFO: Created auth file " << _authFile._fileName << endl << flush;
		cout << "created" << endl << flush;
	}
	else {
		cerr << "\tERROR: Failed to create auth file " << _security._authFile._fileName << endl << flush;
		return false;
	}

	return true;
}

bool Bank::stop() {
	_tcpListener->stopListening();

	// delete the auth file
	// KEEP THIS COMMENTED AS TESTS FAIL IF THIS FILE IS DELETED
	//_authFile.deleteFile();

	return true;
}

void Bank::run() {
	while (_tcpPoller.processSockets(this)) {

		if (_shutdownRequested)
			break;

	}
	_tcpPoller.exit();
	return;
}

bool Bank::processConnection(TcpConnection* tcpConnection) {
	bool disposeSocket = false;

	ReadWriteState& rwState = tcpConnection->_rwState;
	TransactionState& tState = tcpConnection->_transactionState;
	Msg& msgIn = tcpConnection->_rRecord._msg;
	Msg& msgOut = tcpConnection->_wRecord._msg;

	//cerr << "\tINFO " << Clock() << ": Enter sm " << tcpConnection->_name << " ts=" << getName(tState) << " rws=" << getName(rwState) << endl << flush;

	if (rwState == RWS_Error) {
		tState = TS_Error;
	}

	switch(tState)
	{
	case TS_Null:
		tState = TS_Begin;
		break;
	case TS_Begin:
		tState = TS_SessionKeyRequest;
		break;
	case TS_SessionKeyRequest:
		switch(rwState) {
		case RWS_Null:
			rwState = RWS_Reading;
			break;
		case RWS_Reading:
			break;
		case RWS_Read:
			rwState = RWS_Null;
			if (msgIn._msgType == MsgType_SessionKeyRequest) {
				tState = TS_SessionKeyResponse;
			} else {
				tState = TS_Error;
			}
			break;
		default:
			tState = TS_Error;
		}
		break;
	case TS_SessionKeyResponse:
		switch(rwState) {
		case RWS_Null:
			msgOut._msgType = MsgType_SessionKeyResponse;
			_security.randomBytes(msgOut._sessionKey, SESSION_KEY_SIZE);
			memcpy(tcpConnection->_sessionKey, msgOut._sessionKey, SESSION_KEY_SIZE);
			msgOut._success = true;
			rwState = RWS_Writing;
			break;
		case RWS_Writing:
			break;
		case RWS_Written:
			rwState = RWS_Null;
			tState = TS_AccountActionRequest;
			break;
		default:
			tState = TS_Error;
		}
		break;
	case TS_AccountActionRequest:
		switch(rwState) {
		case RWS_Null:
			rwState = RWS_Reading;
			break;
		case RWS_Reading:
			break;
		case RWS_Read:
			rwState = RWS_Null;
			if (msgIn._msgType == MsgType_AccountActionRequest) {
				tState = TS_AccountActionResponse;
			} else {
				tState = TS_Error;
			}
			break;
		default:
			tState = TS_Error;
		}
		break;
	case TS_AccountActionResponse:
		switch(rwState) {
		case RWS_Null:
			msgOut._msgType = MsgType_AccountActionResponse;
			processAccountAction(msgIn, msgOut);
			rwState = RWS_Writing;
			break;
		case RWS_Writing:
			break;
		case RWS_Written:
			tState = TS_End;
			tcpConnection->closeSocket();

			/*
			rwState = RWS_Null;
			tState = TS_Commit;
			*/
			break;
		default:
			tState = TS_Error;
		}
		break;
	case TS_Commit:
		switch(rwState) {
		case RWS_Null:
			rwState = RWS_ExpectingEof;
			break;
		case RWS_ExpectingEof:
			break;
		case RWS_Eof:
		case RWS_Error:
			tState = TS_End;
			break;
		default:
			tState = TS_Error;
		}
		break;
	case TS_End:
		rwState = RWS_Null;
		disposeSocket = true;
		break;
	case TS_Error:
		rwState = RWS_Null;
		cout << "protocol_error" << endl << flush;
		disposeSocket = true;
		break;
	}

	// cerr << "\tINFO " << Clock() << ":  Exit sm " << tcpConnection->_name << " ts=" << getName(tState) << " rws=" << getName(rwState) << (disposeSocket ? " --- DISPOSE ---" : "") << endl << flush;

	return disposeSocket;
}

void Bank::processAccountAction(Msg& msgIn, Msg& msgOut) {
	msgOut = msgIn;
	msgOut._success = false;

	map<string, Account*>::iterator iAccount = _accounts.find(msgIn._accountId);
	Account* pAccount = (iAccount == _accounts.end()) ? 0 : iAccount->second;

	msgOut._msgType = MsgType_AccountActionResponse;

	if (msgIn._accountAction == AccountAction_New) {
		if (pAccount != 0) {
			msgOut._failureReason = FR_AccountIdNotUnique;
			pAccount = 0;
			return;
		}
		if (msgIn._actionAmount < MinInitialBalance) {
			msgOut._failureReason = FR_MinInitialBalanceViolated;
			return;
		}
		if (msgIn._actionAmount > MaxCurrencyAmount) {
			msgOut._failureReason = FR_MaxCurrencyAmountViolated;
			return;
		}

		_security.randomBytes(msgOut._cardKey, AES_BLOCK_SIZE); // generate card key
		pAccount = new Account(msgIn._accountId, msgOut._cardKey, msgIn._actionAmount);
		pair<map<string, Account*>::iterator,bool> retInsert = _accounts.insert( pair<string, Account*>(msgIn._accountId, pAccount));
		msgOut._success = retInsert.second;
		if (msgOut._success) {
			outputAccountAction(msgIn._accountId, "initial_balance", msgIn._actionAmount);
			msgOut._failureReason = FR_Null;
		} else {
			msgOut._failureReason = FR_UnexpectedAccountInsertFailure;
			cerr << "\tERROR: Unexpected account insert failure" << endl << flush;
		}

		return;
	}

	// account should exist for other actions
	if (pAccount == 0) {
		msgOut._failureReason = FR_AccountDoesNotExist;
		return;
	}

	// check card file
	if (memcmp(pAccount->_cardKey, msgIn._cardKey, CARD_KEY_SIZE) != 0) {
		msgOut._failureReason = FR_CardFileAuthenticationFailed;
		return;
	}

	switch (msgIn._accountAction) {
	case AccountAction_Deposit:
		if (msgIn._actionAmount == MinCurrencyAmount) {
			msgOut._failureReason = FR_DepositAmountNotPositive;
			return;
		}
		pAccount->_balance += msgIn._actionAmount;
		outputAccountAction(msgIn._accountId, "deposit", msgIn._actionAmount);
		break;
	case AccountAction_Withdrawal:
		if (pAccount->_balance < msgIn._actionAmount) {
			msgOut._failureReason = FR_InsufficientBalance;
			return;
		}
		pAccount->_balance -= msgIn._actionAmount;
		outputAccountAction(msgIn._accountId, "withdraw", msgIn._actionAmount);
		break;
	case AccountAction_Balance:
		msgOut._actionAmount = pAccount->_balance;
		outputAccountAction(msgIn._accountId, "balance", pAccount->_balance);
		break;
	default:
		msgOut._failureReason = FR_InvalidAccountAction;
		cerr << "\tERROR: Rcvd msg with invalid account action : " << msgIn._msgType << endl << flush;
		return;
	}

	msgOut._success = true;
	msgOut._failureReason = FR_Null;
}

void Bank::outputAccountAction(const char* accountId, const char* amountKey, unsigned long amountValue) {
	printf("{\"account\":\"%s\",\"%s\":%ld.%02ld}\n", accountId, amountKey, amountValue/100, amountValue%100);
	fflush(stdout);
}



