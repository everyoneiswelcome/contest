/*
 * Atm.h
 *
 *  Created on: Oct 2, 2015
 *      Author: singh.rajiv
 */

#ifndef ATM_H_
#define ATM_H_

#include <string>

using namespace std;

#include "Common.h"
#include "Msg.h"
#include "AuthFile.h"
#include "CardFile.h"
#include "Security.h"
#include "TcpPoller.h"

struct Bank;
struct TcpConnection;

struct Atm : public ConnectionProcessor{
	Atm(
			const string& authFileName,
			const string& ipAddress,
			unsigned short port,
			const string& cardFileName,
			const string& accountId,
			AccountAction accountAction,
			unsigned long actionAmount);

	bool start();
	bool stop();
	int run();

	bool processConnection(TcpConnection* tcpConnection);
	int transactWithBank();
	int processAccountActionResponse(const Msg& rcvMsg);

	void dump();

	void outputAccountAction(const char* amountKey, CurrencyAmount amountValue) const;

	void toAccountActionMsg(Msg& msg);

	const char* _name;

	string _ipAddress;
	unsigned short _port;

	Msg _msg;

	CardFile _cardFile;
	Security _security;

	TcpConnection* _tcpConnection;
	TcpPoller _tcpPoller;
	bool _shutdownRequested;

	int _atmReturnCode;
};


#endif /* ATM_H_ */
