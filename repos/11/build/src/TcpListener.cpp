/*
 * TcpListener.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#include "TcpListener.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <iostream>
#include <sstream>

using namespace std;

#include "TcpConnection.h"
#include "Security.h"


TcpListener::TcpListener(const char* name, Security* security, unsigned short port)
:
TcpSocket(name, true, 0, SocketType_Listener),
_security(security),
_port(port)
{
}

bool TcpListener::startListening() {

	// create an AF_INET stream socket to receive incoming connections on
    _fdSocket = ::socket(AF_INET, SOCK_STREAM, 0);
    if (_fdSocket < 0) {
    	cerr << "\tERR " << "TcpListener socket() failed" << endl << flush;
    	return false;
    }

    // allow socket descriptor to be reuseable
    int on = 1;
    int retSetSockOpt = ::setsockopt(_fdSocket, SOL_SOCKET,  SO_REUSEADDR, (char *)&on, sizeof(on));
    if (retSetSockOpt < 0) {
    	::close(_fdSocket);
    	cerr << "   " << "TcpListener setsockopt() SO_REUSEADDR failed" << endl << flush;
    	return false;
    }


    struct sockaddr_in serv_addr;
    ::memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = ::htonl(INADDR_ANY);
    serv_addr.sin_port = htons(_port);

    int bindRet = ::bind(_fdSocket, (struct sockaddr*)&serv_addr, sizeof(serv_addr));
    if (bindRet == -1) {
    	::close(_fdSocket);
    	cerr << "   " << "TcpListener bind() failed" << endl << flush;
    	return false;
    }

    int listenRet = ::listen(_fdSocket, 100);
    if (listenRet == -1) {
    	::close(_fdSocket);
    	cerr << "   " << "TcpListener listen() failed" << endl << flush;
    	return false;
    }

    // non-blocking
    nonBlocking(_nonBlocking);

    return true;
}

TcpConnection* TcpListener::acceptConnection() {
    if (_fdSocket == -1)
    	return 0;

    int fdConnectionSocket = ::accept(_fdSocket, (struct sockaddr*)NULL, NULL);
    if (fdConnectionSocket < 0) {
    	if (errno == EAGAIN || errno == EWOULDBLOCK) {
    		return 0;
    	} else {
	    	cerr << "\tERROR: " << _name << " TcpListener accept SocketError. errno=" << errno << endl << flush;
    		return 0;
    	}
    }

    unsigned int flag = 1;
    ::setsockopt(fdConnectionSocket, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(int));

    // non-blocking
    int fcntlFlags = ::fcntl(fdConnectionSocket, F_GETFL, 0);
    if (fcntlFlags == -1)
    	fcntlFlags = 0;
    int retFcntl = ::fcntl(fdConnectionSocket, F_SETFL, fcntlFlags | O_NONBLOCK);
    if (retFcntl == -1) {
    	cerr << "NONBLOCK failure on accepted socket" << endl << flush;
    }

    return new TcpConnection("BankConnection", _security, fdConnectionSocket);
 }

int TcpListener::acceptAllConnections(vector<TcpSocket*>& tcpSockets) {
	int newConnectionsCount = 0;
	while(true) {
		TcpConnection* tcpConnection = acceptConnection();
		if (tcpConnection == 0)
			return newConnectionsCount;
		else {
			newConnectionsCount++;
			tcpConnection->_transactionState = TS_Begin;
			tcpSockets.push_back(tcpConnection);
		}
	}
	return newConnectionsCount;
 }

bool TcpListener::stopListening() {
	return closeSocket();
}

