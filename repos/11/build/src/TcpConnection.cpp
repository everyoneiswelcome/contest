/*
 * TcpConnection.cpp
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#include "TcpConnection.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <limits.h>
#include <time.h>
#include <iostream>

using namespace std;

#include "Security.h"
#include "Msg.h"

const char* getName(ReadWriteState rws) {
	switch(rws) {
	case RWS_Null: return "Null";
	case RWS_Reading: return "Reading";
	case RWS_Read: return "Read";
	case RWS_Writing: return "Writing";
	case RWS_Written: return "Written";
	case RWS_ExpectingEof: return "ExpectingEof";
	case RWS_Eof: return "Eof";
	case RWS_Error: return "Error";
	default: return "???";
	}
}

const char* getName(TransactionState ts) {
	switch(ts) {
	case TS_Null: return "Null";
	case TS_Begin: return "Begin";
	case TS_SessionKeyRequest: return "SessionKeyRequest";
	case TS_SessionKeyResponse: return "SessionKeyResponse";
	case TS_AccountActionRequest: return "AccountActionRequest";
	case TS_AccountActionResponse: return "AccountActionResponse";
	case TS_Commit: return "Commit";
	case TS_End: return "End";
	case TS_Error: return "Error";
	default: return "???";
	}
}

TcpConnection::TcpConnection(const char* name, Security* security)
	:
	TcpSocket(name, true, 0, SocketType_Connection),
	_rwState(RWS_Null),
	_transactionState(TS_Null),
	_security(security),
	_sessionKey(),

	_rRecord(),
	_wRecord()
{
	memset(_sessionKey, 0, SESSION_KEY_SIZE);
}

TcpConnection::TcpConnection(const char* name, Security* security, int fdSocket)
	:
	TcpSocket(name, true, fdSocket, SocketType_Connection),
	_rwState(RWS_Null),
	_transactionState(TS_Null),
	_security(security),
	_sessionKey(),

	_rRecord(),
	_wRecord()
{
	memset(_sessionKey, 0, SESSION_KEY_SIZE);
}

bool TcpConnection::connect(const char* ipAddress, unsigned short port)
{
	_fdSocket = ::socket(AF_INET, SOCK_STREAM, 0);
	if(_fdSocket == -1)
		return false;

	struct sockaddr_in serv_addr;
	::memset(&serv_addr, '0', sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port);

	int retInetPton = ::inet_pton(AF_INET, ipAddress, &serv_addr.sin_addr);
	if(retInetPton != 1)
		return false;

	int retConnect = ::connect(_fdSocket, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
	if(retConnect == -1)
		return false;

    unsigned int flag = 1;
    ::setsockopt(_fdSocket, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(int));

    nonBlocking(_nonBlocking);

	return true;
}

void TcpConnection::shutdownConnection(int how) {
	//cerr << "\tINFO " << Clock() << ": " << _name << " shutdownConnection how=" << how << endl << flush;
	int retShutdown = ::shutdown(_fdSocket, how);
	if (retShutdown != 0) {
		cerr << "\tERROR " << Clock() << ": " << _name << " shutdown() Failed. errno=" << errno << endl << flush;
	}
}

IoState TcpConnection::readMsg() {

	if (_rRecord._state == IoState_Null) {
		_rRecord._endTime = time(0) + MESSAGE_READ_TIMEOUT_IN_SECONDS;
		_rRecord._bytesTotal = SECURE_MSG_SIZE;
		_rRecord._bytesDone = 0;
		_rRecord._state = IoState_InComplete;
		//cerr << "\tINFO: " << _name << " READ " << _rRecord << " initiated" << endl << flush;
	} else {
		time_t currentTime = time(0);
		if (currentTime > _rRecord._endTime) {
			cerr << "\tERROR: " << _name << " READ " << _rRecord << " -> " << "IoState_TimedOut" << endl << flush;
			return IoState_TimedOut;
		}
	}

	if (_rRecord._state == IoState_InComplete) {

		int readRet = ::read(_fdSocket,
				(char*) _rRecord._secureBytes + _rRecord._bytesDone,
				_rRecord._bytesTotal - _rRecord._bytesDone);

		if (readRet < 0) {
			if (errno == EAGAIN || errno == EWOULDBLOCK || errno == 0)
				return IoState_InComplete;
			else {
				cerr << "\tERROR: " << _name << " IoState_SocketError. errno=" << errno << endl << flush;
				return IoState_SocketError;
			}
		} else if (readRet == 0) {
			return ((_rRecord._bytesDone == 0) ? IoState_EOF : IoState_AbruptEOF);
		} else {
			_rRecord._bytesDone += readRet;
		}

		if (_rRecord._bytesDone < _rRecord._bytesTotal) {
			return _rRecord._state;
		} else {
			_rRecord._state = IoState_InCompleteRecordRead;
    	}
	}

	if (_rRecord._state == IoState_InCompleteRecordRead) {
		//cerr << "\tINFO " << Clock() << ": " << _name << " bytes read - " << Buffer2Hex(_rRecord._secureBytes, SECURE_MSG_SIZE, 2, 2) << endl << flush;

		if (_security->unlock(_rRecord._secureBytes, (unsigned char*) &_rRecord._msg)) {

			// cerr << "\tINFO: " << _name << " READ " << _msgIn << endl << flush;
			Msg& msgIn = _rRecord._msg;

			if (msgIn._msgType == MsgType_SessionKeyResponse)
				memcpy(_sessionKey, msgIn._sessionKey, SESSION_KEY_SIZE);

			if (memcmp(msgIn._sessionKey, _sessionKey, SESSION_KEY_SIZE) != 0) {
				cerr << "\tERROR: " << _name << " IoState_InvalidSessionKey. got="
						<< Buffer2Hex(msgIn._sessionKey, SESSION_KEY_SIZE, 2, 2) << ", insteadOf=" << Buffer2Hex(_sessionKey, SESSION_KEY_SIZE, 2, 2) << endl << flush;
				return _rRecord._state = IoState_InvalidSessionKey;
			}

			_rRecord._state = IoState_Complete;
		} else {
			// corrupted stream on connection
			cerr << "\tERROR " << Clock() << ": " << _name << " READ ReadResult_SecurityError" << endl << flush;
			return _rRecord._state = IoState_SecurityError;
		}
	}

	// reach here only when IoState_Complete
	_rRecord.resetIo();

	//cerr << "\tINFO " << Clock() << ": " << _name << " readMsg - " << _rRecord._msg << endl << flush;

	return IoState_Complete;
}

IoState TcpConnection::writeMsg() {
	Msg& msgOut = _wRecord._msg;
	if (_wRecord._state == IoState_Null) {

		memcpy(msgOut._sessionKey, _sessionKey, SESSION_KEY_SIZE);

		_wRecord._endTime = time(0) + MESSAGE_WRITE_TIMEOUT_IN_SECONDS;

		if (_security == 0)
			memcpy(_wRecord._secureBytes, (unsigned char*) &msgOut, MSG_SIZE);
		else
			_security->lock((unsigned char*) &msgOut, _wRecord._secureBytes);

		_wRecord._bytesTotal = SECURE_MSG_SIZE;
		_wRecord._bytesDone = 0;
		_wRecord._state = IoState_InComplete;
	} else {
		time_t currentTime = time(0);
		if (currentTime > _wRecord._endTime) {
			cerr << "\tERROR: " << _name << " WRITE " << _wRecord << " -> " << "IoState_TimedOut" << endl << flush;
			return IoState_TimedOut;
		}
	}

	if (_wRecord._state == IoState_InComplete) {
    	int writeRet = ::write(_fdSocket,
    			(char *) _wRecord._secureBytes + _wRecord._bytesDone,
				_wRecord._bytesTotal - _wRecord._bytesDone);

        if (writeRet < 0) {
        	if (errno == EAGAIN || errno == EWOULDBLOCK || errno == 0)
        		return _wRecord._state;
        	else {
    	    	cerr << "\tERROR: " << _name << " WriteResult_SocketError. errno=" << errno << endl << flush;
    	    	return _wRecord._state = IoState_SocketError;
        	}
        } else {
        	_wRecord._bytesDone += writeRet;
        }

        if (_wRecord._bytesDone < _wRecord._bytesTotal) {
        	return _wRecord._state;
        } else
        	_wRecord._state = IoState_Complete;
	}

	_wRecord.resetIo();
	// reach here only when IoState_Complete

	//cerr << "\tINFO " << Clock() << ": " << _name << " writeMsg - " << _wRecord._msg << endl << flush;


	return IoState_Complete;
}
