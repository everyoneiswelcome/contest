/*
 * Security.h
 *
 *  Created on: Oct 1, 2015
 *      Author: singh.rajiv
 */

#ifndef SECURITY_H_
#define SECURITY_H_

#include <string>
using namespace std;

#include "Common.h"
#include "AuthFile.h"
#include <openssl/aes.h>

struct Security {

	Security(const string& authFileName);
	void createKeys();

	void randomBytes(unsigned char* rb, unsigned int count);
	void hmacDigest(unsigned char* msg, unsigned char* msgDigest);
	void lockHmacCbcWithAesKey(unsigned char* rawData, unsigned char* secureData);
	void lock(unsigned char* rawData, unsigned char* secureData);
	bool unlockHmacCbcWithAesKey(unsigned char* secureData, unsigned char* rawData);
	bool unlock(unsigned char* secureData, unsigned char* rawData);
	unsigned char _aesKey[AUTH_KEY_SIZE];
	AuthFile _authFile;

    AES_KEY _decKey;
    AES_KEY _encKey;
	bool _createKeys;
};


#endif /* SECURITY_H_ */
