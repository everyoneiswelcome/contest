/*
 * Security.cpp
 *
 *  Created on: May 16, 2015
 *      Author: singh.rajiv
 */

#include "Security.h"
#include "Common.h"
#include "Msg.h"

#include <string>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <iomanip>
#include <stdint.h>

using namespace std;

#include <openssl/rand.h>
#include <openssl/hmac.h>
#include <openssl/sha.h>

using namespace std;

#define SECURITY_ON

unsigned int padData(unsigned char* data, unsigned int dataLen) {
	 // calculate pad character
	unsigned char padChar = (unsigned char) (AES_BLOCK_SIZE - (dataLen % AES_BLOCK_SIZE));

	// create padded data
	::memset((void *) (data + dataLen), padChar, padChar);

	return dataLen + padChar;
}

unsigned int unpadData(unsigned char* paddedData, unsigned int paddedDataLen) {
	 // calculate pad character
	unsigned char padChar = (unsigned char) paddedData[paddedDataLen - 1];
	return  paddedDataLen - padChar;
}

Security::Security(const string& authFileName)
:
_authFile(authFileName),
_createKeys(true)
{
}

void Security::createKeys() {
	if (_createKeys) {
	    AES_set_encrypt_key((const unsigned char*) _aesKey, AES_BLOCK_SIZE*8, &_encKey);
	    AES_set_decrypt_key(_aesKey, AES_BLOCK_SIZE*8, &_decKey);
	    _createKeys = false;
	}
}


void Security::randomBytes(unsigned char* rb, unsigned int count) {
	int ret = RAND_bytes(rb, count);
	if (ret == 0)
		cerr << "SECURITY_ERROR : RAND_bytes() failed" << endl << flush;
}

void Security::hmacDigest(unsigned char* msg, unsigned char* msgDigest) {
#ifdef SECURITY_ON
	HMAC_CTX ctx;
	HMAC_CTX_init(&ctx);

	unsigned char *md = (unsigned char *) msgDigest;//[EVP_MAX_MD_SIZE];
	unsigned int md_len = 0;
	const EVP_MD *evp_md = EVP_sha1();
	unsigned char* key = _aesKey;
	int key_len = 16;
	int msg_len = MSG_SIZE;

	HMAC(evp_md,
		(const void *) key, key_len,
		(const unsigned char *) msg, msg_len,
		 md, &md_len);

	HMAC_CTX_cleanup(&ctx);



	HMAC_CTX_cleanup(&ctx);
#endif
}
#define HMAC_SIZE 20
#define AES_BLOCK_SIZE 16
#define MSG_SIZE sizeof(Msg)
#define HASHMSG_SIZE (HMAC_SIZE + MSG_SIZE)
#define PADDED_HASHMSG_SIZE (HASHMSG_SIZE + (AES_BLOCK_SIZE - HASHMSG_SIZE % AES_BLOCK_SIZE))
#define SECURE_MSG_SIZE (AES_BLOCK_SIZE + PADDED_HASHMSG_SIZE)

void Security::lockHmacCbcWithAesKey(unsigned char* rawData, unsigned char* secureData) {
#ifdef SECURITY_ON
	unsigned char iv[AES_BLOCK_SIZE];
	randomBytes(iv, AES_BLOCK_SIZE);
	memcpy(secureData, iv, AES_BLOCK_SIZE);

	unsigned char plainText[PADDED_HASHMSG_SIZE];
	hmacDigest(rawData, plainText);
	memcpy(plainText + HMAC_SIZE, rawData, MSG_SIZE);
	unsigned int dataLen = padData(plainText, HMAC_SIZE + MSG_SIZE);

	if (_createKeys)
		createKeys();

    AES_cbc_encrypt(
    		plainText,
			secureData + AES_BLOCK_SIZE,
			(unsigned long int) dataLen,
			&_encKey,
			iv,
			AES_ENCRYPT);
#endif
}

void Security::lock(unsigned char* rawData, unsigned char* secureData) {
#ifdef SECURITY_ON
	lockHmacCbcWithAesKey(rawData, secureData);
#else
	memcpy(secureData, rawData, MSG_SIZE);
#endif
}

bool Security::unlockHmacCbcWithAesKey(unsigned char* secureData, unsigned char* rawData) {
#ifdef SECURITY_ON
	unsigned char* iv = secureData;
	unsigned char* cipherText = secureData + AES_BLOCK_SIZE;

	unsigned char plainText[PADDED_HASHMSG_SIZE];

	if (_createKeys)
		createKeys();

    AES_cbc_encrypt(
    		(const unsigned char *) cipherText,
    		(unsigned char*) plainText,
			PADDED_HASHMSG_SIZE,
			&_decKey,
			iv,
			AES_DECRYPT);

	unsigned char computedDigest[HMAC_SIZE];
	hmacDigest(plainText + HMAC_SIZE, computedDigest);

    bool ok = (memcmp(plainText, computedDigest, HMAC_SIZE) == 0);
    if (ok) {
    	memcpy(rawData, plainText + HMAC_SIZE, MSG_SIZE);
    }
    return ok;
#else
    return true;
#endif
}

bool Security::unlock(unsigned char* secureData, unsigned char* rawData) {
#ifdef SECURITY_ON
	return unlockHmacCbcWithAesKey(secureData, rawData);
#else
	memcpy(rawData, secureData, MSG_SIZE);
	return true;
#endif
}
