package main

import (
	//	"fmt"
	"os"

	"atm/client"
	"atm/config"
	"bank/utils"
)

func main() {
	if err := config.Configure(); err != nil {
		os.Exit(255)
	}

	if err := client.Process(); err != nil {
		if err == utils.ProtocolError {
			//fmt.Println(err.Error())
			os.Exit(63)
		} else {
			os.Exit(255)
		}
	}
}
