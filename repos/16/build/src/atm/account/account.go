package account

import (
	"math/big"
)

type Account struct {
	ErrCode uint8
	CardId  string
	Name    string
	Balance *big.Int
}
