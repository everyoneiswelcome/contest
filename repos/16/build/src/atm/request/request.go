package request

import ()

type Operation uint8

type Request struct {
	Timestamp int64
	CardId    string
	Name      string
	Operation Operation
	Amount    int64
}

const (
	OPERATION_NEW      Operation = 1
	OPERATION_GET      Operation = 2
	OPERATION_DEPOSIT  Operation = 3
	OPERATION_WITHDRAW Operation = 4
)
