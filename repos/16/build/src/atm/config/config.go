package config

import (
	"errors"
	"fmt"
	"github.com/pborman/getopt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strconv"
)

var Config struct {
	AccountName string
	AuthBytes   []byte
	AuthFile    string
	IPAddress   string
	Port        int
	CardFile    string
	NewAccount  *int64
	Deposit     *int64
	Withdraw    *int64
	Get         bool
}

func Configure() (err error) {
	a := getopt.String('a', "", "Account")
	s := getopt.String('s', "bank.auth", "Auth file")
	i := getopt.String('i', "127.0.0.1", "IP address")
	p := getopt.Int('p', 3000, "Port")
	c := getopt.String('c', "", "Card file")
	n := getopt.String('n', "", "New account")
	d := getopt.String('d', "", "Deposit")
	w := getopt.String('w', "", "Withdraw")
	g := getopt.Bool('g', "Get account")

	opts := getopt.CommandLine

	if err := opts.Getopt(os.Args, nil); err != nil {
		log.Printf("opts.Getopt() error = %+v", err)
		return err
	}

	operationsCount := opts.GetCount('n') + opts.GetCount('g') + opts.GetCount('d') + opts.GetCount('w')
	if operationsCount != 1 || opts.NArgs() > 0 {
		err := errors.New("Require one operation")
		log.Print(err)
		return err
	}

	if a != nil && len(*a) > 0 {
		accountName, err := parseAccountName(*a)
		if err != nil {
			return err
		}
		Config.AccountName = accountName
	}

	if i != nil && len(*i) > 0 {
		ipAddress, err := parseIPAddress(*i)
		if err != nil {
			return err
		}
		Config.IPAddress = ipAddress
	}

	if p != nil {
		port, err := parsePortNumber(*p)
		if err != nil {
			return err
		}
		Config.Port = port
	}

	if c != nil && len(*c) > 0 {
		fileName, err := parseFileName(*c)
		if err != nil {
			return err
		}
		Config.CardFile = fileName
	}

	if s != nil && len(*s) > 0 {
		fileName, err := parseFileName(*s)
		if err != nil {
			return err
		}
		Config.AuthFile = fileName
	}

	if n != nil && len(*n) > 0 {
		amount, err := parseMoneyArg(*n)
		if err != nil {
			return err
		}
		Config.NewAccount = amount
	}

	if d != nil && len(*d) > 0 {
		amount, err := parseMoneyArg(*d)
		if err != nil {
			return err
		}
		Config.Deposit = amount
	}

	if w != nil && len(*w) > 0 {
		amount, err := parseMoneyArg(*w)
		if err != nil {
			return err
		}
		Config.Withdraw = amount
	}

	Config.Get = *g

	if Config.AuthBytes, err = ioutil.ReadFile(Config.AuthFile); err != nil {
		log.Printf("ioutil.ReadFile() error: %+v", err)
		return err
	}

	return nil
}

func parseMoneyArg(strArg string) (*int64, error) {
	reg := regexp.MustCompile(`\A(0|[1-9][0-9]*)\.([0-9]{2})\z`)
	matches := reg.FindAllStringSubmatch(strArg, -1)
	if len(matches) != 1 || len(matches[0]) != 3 {
		err := errors.New(fmt.Sprintf("Invalid number: %v", strArg))
		log.Print(err)
		return nil, err
	}

	cents, _ := strconv.ParseInt(matches[0][2], 10, 64)

	amount, err := strconv.ParseInt(matches[0][1], 10, 64)
	if err != nil {
		log.Printf("strconv.ParseInt() error: %+v", err)
		return nil, err
	}

	amount = (amount * 100) + cents

	if amount > 429496729599 {
		err := errors.New(fmt.Sprintf("Invalid number: %v", strArg))
		log.Print(err)
		return nil, err
	}

	return &amount, nil
}

func parseFileName(strArg string) (string, error) {
	reg := regexp.MustCompile(`\A[_\-\.0-9a-z]{1,255}\z`)
	if !reg.Match([]byte(strArg)) || strArg == "." || strArg == ".." {
		err := errors.New(fmt.Sprintf("Invalid file name: %v", strArg))
		log.Print(err)
		return "", err
	}
	return strArg, nil
}

func parseAccountName(strArg string) (string, error) {
	reg := regexp.MustCompile(`\A[_\-\.0-9a-z]{1,250}\z`)
	if !reg.Match([]byte(strArg)) {
		err := errors.New(fmt.Sprintf("Invalid account name: %v", strArg))
		log.Print(err)
		return "", err
	}
	return strArg, nil
}

func parsePortNumber(intArg int) (int, error) {
	if intArg < 1024 || intArg > 65535 {
		err := errors.New(fmt.Sprintf("Invalid port number: %d", intArg))
		log.Print(err)
		return 0, err
	}
	return intArg, nil
}

func parseIPAddress(strArg string) (string, error) {
	reg := regexp.MustCompile(`\A(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\z`)
	if !reg.Match([]byte(strArg)) {
		err := errors.New(fmt.Sprintf("Invalid IP address: %v", strArg))
		log.Print(err)
		return "", err
	}
	return strArg, nil
}
