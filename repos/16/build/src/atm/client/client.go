package client

import (
	"bytes"
	"encoding/gob"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/big"
	"net"
	"os"
	"time"
	"github.com/kisom/aescrypt/secretbox"

	"atm/account"
	"atm/config"
	"atm/request"
	"bank/utils"
)

func Process() error {
	if config.Config.NewAccount != nil {
		acc, err := processNew()
		if err != nil {
			return err
		}
		//log.Printf("DEBUG success account = %+v", acc)
		return utils.PrintReport(acc.Name, "initial_balance", acc.Balance)
	} else if config.Config.Get {
		acc, err := processGet()
		if err != nil {
			return err
		}
		//log.Printf("DEBUG success account = %+v", acc)
		return utils.PrintReport(acc.Name, "balance", acc.Balance)
	} else if config.Config.Deposit != nil {
		acc, err := processDeposit()
		if err != nil {
			return err
		}
		//log.Printf("DEBUG success account = %+v", acc)
		return utils.PrintReport(acc.Name, "deposit", big.NewInt(*config.Config.Deposit))
	} else if config.Config.Withdraw != nil {
		acc, err := processWithdraw()
		if err != nil {
			return err
		}
		//log.Printf("DEBUG success account = %+v", acc)
		return utils.PrintReport(acc.Name, "withdraw", big.NewInt(*config.Config.Withdraw))
	} else {
		return errors.New("Unknown operation")
	}

	return nil
}

func processNew() (*account.Account, error) {
	req := &request.Request{
		Name:      config.Config.AccountName,
		Operation: request.OPERATION_NEW,
		Amount:    *config.Config.NewAccount}

	if _, err := os.Stat(cardFile()); !os.IsNotExist(err) {
		err := errors.New(fmt.Sprintf("Card file already exists: %s", cardFile()))
		log.Print(err.Error())
		return nil, err
	}

	acc, err := send(req)
	if err != nil {
		return nil, err
	}

	f, err := os.Create(cardFile())
	if err != nil {
		log.Printf("os.Create() error: %+v", err)
		return nil, err
	}
	defer f.Close()

	if _, err := f.WriteString(acc.CardId); err != nil {
		log.Printf("f.WriteString() error: %+v", err)
		return nil, err
	}

	return acc, nil
}

func processGet() (*account.Account, error) {
	cardId, err := readCardFile()
	if err != nil {
		return nil, err
	}

	req := &request.Request{
		Name:      config.Config.AccountName,
		CardId:    cardId,
		Operation: request.OPERATION_GET}

	acc, err := send(req)
	if err != nil {
		return nil, err
	}

	return acc, nil
}

func processDeposit() (*account.Account, error) {
	cardId, err := readCardFile()
	if err != nil {
		return nil, err
	}

	req := &request.Request{
		Name:      config.Config.AccountName,
		CardId:    cardId,
		Operation: request.OPERATION_DEPOSIT,
		Amount:    *config.Config.Deposit,
		Timestamp: time.Now().UnixNano()}

	acc, err := send(req)
	if err != nil {
		return nil, err
	}

	return acc, nil
}

func processWithdraw() (*account.Account, error) {
	cardId, err := readCardFile()
	if err != nil {
		return nil, err
	}

	req := &request.Request{
		Name:      config.Config.AccountName,
		CardId:    cardId,
		Operation: request.OPERATION_WITHDRAW,
		Amount:    *config.Config.Withdraw,
		Timestamp: time.Now().UnixNano()}

	acc, err := send(req)
	if err != nil {
		return nil, err
	}

	return acc, nil
}

func send(req *request.Request) (*account.Account, error) {
	conn, err := connect()
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	if err := write(conn, req); err != nil {
		return nil, err
	}

	if err := conn.CloseWrite(); err != nil {
		log.Printf("conn.CloseWrite() error: %+v", err)
		return nil, utils.ProtocolError
	}

	acc, err := receive(conn)
	if err != nil {
		return nil, err
	}

	if acc.ErrCode != 0 {
		return nil, errors.New("account error")
	}

	return acc, err
}

func connect() (*net.TCPConn, error) {
	tcpAddr, err := net.ResolveTCPAddr("tcp", fmt.Sprintf("%s:%d", config.Config.IPAddress, config.Config.Port))
	if err != nil {
		log.Printf("net.ResolveTCPAddr() error: %+v", err)
		return nil, utils.ProtocolError
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		log.Printf("net.DialTCP() error: %+v", err)
		return nil, err
	}

	return conn, nil
}

func write(conn *net.TCPConn, req *request.Request) error {
	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)

	err := enc.Encode(req)
	if err != nil {
		log.Printf("enc.Encode() error: %+v", err)
		return err
	}

	encrypted, ok := secretbox.Seal(buf.Bytes(), config.Config.AuthBytes)
	if !ok {
		err := errors.New("secretbox.Seal() error")
		log.Print(err.Error())
		return err
	}

	conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
	_, err = conn.Write(encrypted)
	if err != nil {
		log.Printf("conn.Write() error: %+v", err)
		return utils.ProtocolError
	}

	return nil
}

func receive(conn *net.TCPConn) (*account.Account, error) {
	raw := make([]byte, 102400)

	conn.SetReadDeadline(time.Now().Add(10 * time.Second))
	readLen, err := conn.Read(raw)
	if err != nil {
		log.Printf("conn.Read() error: %+v", err)
		return nil, utils.ProtocolError
	}

	raw = raw[:readLen]

	decrypted, ok := secretbox.Open(raw, config.Config.AuthBytes)
	if !ok {
		log.Print("secretbox.Seal() error")
		return nil, utils.ProtocolError
	}

	dec := gob.NewDecoder(bytes.NewReader(decrypted))

	acc := &account.Account{}

	if err = dec.Decode(acc); err != nil {
		log.Printf("dec.Decode() error: %+v", err)
		return nil, utils.ProtocolError
	}

	return acc, nil
}

func readCardFile() (string, error) {
	cardBytes, err := ioutil.ReadFile(cardFile())
	if err != nil {
		log.Printf("ioutil.ReadFile() error: %+v", err)
		return "", err
	}

	return string(cardBytes), nil
}

func cardFile() string {
	result := config.Config.CardFile
	if len(result) == 0 {
		result = config.Config.AccountName + ".card"
	}
	return result
}
