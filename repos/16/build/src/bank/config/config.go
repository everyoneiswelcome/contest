package config

import (
	"errors"
	"fmt"
	"github.com/pborman/getopt"
	"log"
	"os"
	"regexp"
	"github.com/kisom/aescrypt/secretbox"
)

var Config struct {
	Port       int
	AuthBytes []byte
	AuthFile   string
}

func Configure() (err error) {
	p := getopt.Int('p', 3000, "Port")
	s := getopt.String('s', "bank.auth", "Auth file")

	opts := getopt.CommandLine

	if err := opts.Getopt(os.Args, nil); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(255)
	}

	if p != nil {
		port, err := parsePortNumber(*p)
		if err != nil {
			return err
		}
		Config.Port = port
	}

	if s != nil && len(*s) > 0 {
		fileName, err := parseFileName(*s)
		if err != nil {
			return err
		}
		Config.AuthFile = fileName
	}

	if _, err := os.Stat(Config.AuthFile); !os.IsNotExist(err) {
		err := errors.New(fmt.Sprintf("Auth file already exists: %s", Config.AuthFile))
		log.Print(err.Error())
		return err
	}

	var ok bool
	if Config.AuthBytes, ok = secretbox.GenerateKey(); !ok {
		log.Print("secretbox.GenerateKey() error")
		return err
	}

	f, err := os.Create(Config.AuthFile)
	if err != nil {
		log.Printf("os.Create() error: %+v", err)
		return err
	}

	defer f.Close()

	if _, err := f.Write(Config.AuthBytes); err != nil {
		log.Printf("f.Write() error: %+v", err)
		return err
	}

	f.Sync()

	fmt.Println("created")

	return nil
}

func parseFileName(strArg string) (string, error) {
	reg := regexp.MustCompile(`\A[_\-\.0-9a-z]{1,255}\z`)
	if !reg.Match([]byte(strArg)) || strArg == "." || strArg == ".." {
		err := errors.New(fmt.Sprintf("Invalid file name: %v", strArg))
		log.Print(err)
		return "", err
	}
	return strArg, nil
}

func parsePortNumber(intArg int) (int, error) {
	if intArg < 1024 || intArg > 65535 {
		err := errors.New(fmt.Sprintf("Invalid port number: %d", intArg))
		log.Print(err)
		return 0, err
	}
	return intArg, nil
}
