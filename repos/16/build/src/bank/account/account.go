package account

import (
	"errors"
	"fmt"
	"math/big"
	"regexp"
	"time"

	"bank/utils"
)

type Account struct {
	ErrCode       uint8
	lastUpdatedAt int64
	CardId        string
	Name          string
	Balance       *big.Int
}

var allAccount map[string]*Account = map[string]*Account{}

func NewAccount(name string, balance *big.Int) (*Account, error) {
	reg := regexp.MustCompile(`\A[_\-\.0-9a-z]{1,250}\z`)
	if !reg.Match([]byte(name)) {
		return nil, errors.New(fmt.Sprintf("Account name is not valid: %s", name))
	}
	if balance.Cmp(big.NewInt(1000)) == -1 {
		return nil, errors.New(fmt.Sprintf("Account balance must be >= 1000: %v", balance))
	}
	if account, ok := allAccount[name]; ok && account != nil {
		return nil, errors.New(fmt.Sprintf("Account already exists: %s", name))
	}

	cardId, err := utils.RandomBase64(12)
	if err != nil {
		return nil, err
	}

	newAccount := &Account{
		lastUpdatedAt: time.Now().UnixNano(),
		CardId:        cardId,
		Name:          name,
		Balance:       balance}

	allAccount[name] = newAccount

	return newAccount, nil
}

func GetAccount(name, cardId string) (acc *Account, err error) {
	acc, ok := allAccount[name]
	if ok && acc != nil {
		if acc.CardId != cardId {
			acc = nil
		}
	}

	if acc == nil {
		return nil, errors.New(fmt.Sprintf("Account not found: %s", name))
	}

	return acc, nil
}

func (a *Account) Deposit(amount *big.Int, timestamp int64) error {
	if timestamp <= a.lastUpdatedAt {
		return errors.New("Ilegal withdraw")
	}
	if amount.Cmp(big.NewInt(0)) <= 0 {
		return errors.New(fmt.Sprintf("Deposit amount must be > 0: %v", amount))
	}
	a.Balance.Add(a.Balance, amount)
	a.lastUpdatedAt = time.Now().UnixNano()
	return nil
}

func (a *Account) Withdraw(amount *big.Int, timestamp int64) error {
	if timestamp <= a.lastUpdatedAt {
		return errors.New("Ilegal withdraw")
	}
	if amount.Cmp(big.NewInt(0)) <= 0 {
		return errors.New(fmt.Sprintf("Withdraw amount must be > 0: %v", amount))
	}
	if a.Balance.Cmp(amount) == -1 {
		return errors.New(fmt.Sprintf("Withdraw amount must be <= balance: %v", amount))
	}
	a.Balance.Sub(a.Balance, amount)
	a.lastUpdatedAt = time.Now().UnixNano()
	return nil
}
