package main

import (
	"os"
	"os/signal"
	"syscall"

	"bank/config"
	"bank/server"
)

func main() {
	if err := config.Configure(); err != nil {
		os.Exit(255)
	}

	//	go func() {
	//		for {
	//			ch := make(chan os.Signal)
	//			signal.Notify(ch, syscall.SIGINT)
	//			<-ch
	//		}
	//	}()

	go func() {
		ch := make(chan os.Signal)
		signal.Notify(ch, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
		<-ch
		os.Exit(0)
	}()

	if err := server.Listen(); err != nil {
		os.Exit(255)
	}
}
