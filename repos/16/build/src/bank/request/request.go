package request

import (
	"errors"
	"log"
	"math/big"

	"bank/account"
	"bank/utils"
)

type Operation uint8

type Request struct {
	Timestamp int64
	CardId    string
	Name      string
	Operation Operation
	Amount    int64
}

const (
	OPERATION_NEW      Operation = 1
	OPERATION_GET      Operation = 2
	OPERATION_DEPOSIT  Operation = 3
	OPERATION_WITHDRAW Operation = 4
)

func (req *Request) Execute() (*account.Account, error) {
	if req.Operation == OPERATION_NEW {
		return req.newAccount()
	} else if req.Operation == OPERATION_GET {
		return req.getAccount()
	} else if req.Operation == OPERATION_DEPOSIT {
		return req.deposit()
	} else if req.Operation == OPERATION_WITHDRAW {
		return req.withdraw()
	} else {
		err := errors.New("Unknown operation")
		log.Printf("Execute() error: %+v", err)
		return nil, err
	}
}

func (req *Request) newAccount() (*account.Account, error) {
	acc, err := account.NewAccount(req.Name, big.NewInt(req.Amount))
	if err != nil {
		log.Printf("account.NewAccount() error: %+v", err)
		return nil, err
	}

	//if err := acc.UpdateOnce(); err != nil {
	//	log.Printf("acc.UpdateOnce() error: %+v", err)
	//	return nil, err
	//}

	utils.PrintReport(acc.Name, "initial_balance", acc.Balance)

	return acc, nil
}

func (req *Request) getAccount() (*account.Account, error) {
	acc, err := account.GetAccount(req.Name, req.CardId)
	if err != nil {
		log.Printf("account.GetAccount() error: %+v", err)
		return nil, err
	}

	utils.PrintReport(acc.Name, "balance", acc.Balance)

	return acc, nil
}

func (req *Request) deposit() (*account.Account, error) {
	acc, err := account.GetAccount(req.Name, req.CardId)
	if err != nil {
		log.Printf("account.GetAccount() error: %+v", err)
		return nil, err
	}

	amount := big.NewInt(req.Amount)

	if err := acc.Deposit(amount, req.Timestamp); err != nil {
		log.Printf("acc.Deposit() error: %+v", err)
		return nil, err
	}

	utils.PrintReport(acc.Name, "deposit", amount)

	return acc, nil
}

func (req *Request) withdraw() (*account.Account, error) {
	acc, err := account.GetAccount(req.Name, req.CardId)
	if err != nil {
		log.Printf("account.GetAccount() error: %+v", err)
		return nil, err
	}

	amount := big.NewInt(req.Amount)

	if err := acc.Withdraw(amount, req.Timestamp); err != nil {
		log.Printf("acc.Withdraw() error: %+v", err)
		return nil, err
	}

	utils.PrintReport(acc.Name, "withdraw", amount)

	return acc, nil
}
