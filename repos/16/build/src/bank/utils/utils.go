package utils

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"log"
)

var ProtocolError error = errors.New("protocol_error")

func RandomBase64(size int) (string, error) {
	randByte := make([]byte, size)
	if _, err := rand.Read(randByte); err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(randByte), nil
}

func RandomHex(size int) (string, error) {
	randByte := make([]byte, size)
	if _, err := rand.Read(randByte); err != nil {
		return "", err
	}
	return hex.EncodeToString(randByte), nil
}

func RandomUInt64() (uint64, error) {
	num := uint64(0)

	for num == uint64(0) {
		randByte := make([]byte, 8)
		if _, err := rand.Read(randByte); err != nil {
			log.Printf("rand.Read() error: %+v", err)
			return 0, err
		}
		num = binary.LittleEndian.Uint64(randByte)
	}

	return num, nil
}
