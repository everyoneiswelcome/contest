package utils

import (
	"encoding/json"
	"fmt"
	"log"
	"math/big"
	"os"
)

func PrintReport(name string, label string, amount *big.Int) error {
	dollars := big.NewInt(0)
	cents := big.NewInt(0)
	dollars.DivMod(amount, big.NewInt(100), cents)

	nameBytes, err := json.Marshal(name)
	if err != nil {
		log.Printf("json.Marshal() error: %+v", err)
		return err
	}

	fmt.Printf("{\"account\":%s,\"%s\":%s.%02d}\n", string(nameBytes), label, dollars.String(), cents.Int64())

	if err := os.Stdout.Sync(); err != nil {
		//log.Printf("DEBUG os.Stdout.Sync() error: %+v", err)
	}

	return nil
}
