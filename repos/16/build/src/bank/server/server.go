package server

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"log"
	"net"
	"time"
	"github.com/kisom/aescrypt/secretbox"

	"bank/account"
	"bank/config"
	"bank/request"
	"bank/utils"
)

func Listen() error {
	addr := fmt.Sprintf(":%d", config.Config.Port)
	l, err := net.Listen("tcp", addr)
	if err != nil {
		log.Printf("net.Listen() error: %+v", err)
		return err
	}
	defer l.Close()

	log.Printf("Listening on %s\n", addr)

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Printf("l.Accept() error: %+v", err)
		} else {
			handleRequest(conn)
		}
	}
}

func handleRequest(conn net.Conn) {
	raw := make([]byte, 10240)

	conn.SetReadDeadline(time.Now().Add(10 * time.Second))
	readLen, err := conn.Read(raw)
	if err != nil {
		log.Printf("conn.Read() error: %+v", err)
		fmt.Println(utils.ProtocolError.Error())
		return
	}

	raw = raw[:readLen]

	decrypted, ok := secretbox.Open(raw, config.Config.AuthBytes)
	if !ok {
		log.Print("secretbox.Seal() error")
		fmt.Println(utils.ProtocolError.Error())
		return
	}

	dec := gob.NewDecoder(bytes.NewReader(decrypted))

	req := &request.Request{}

	if err = dec.Decode(req); err != nil {
		log.Printf("dec.Decode() error: %+v", err)
		fmt.Println(utils.ProtocolError.Error())
		return
	}

	acc, err := req.Execute()
	if err != nil {
		acc = &account.Account{ErrCode: 255}
	}

	var buf bytes.Buffer
	enc := gob.NewEncoder(&buf)

	err = enc.Encode(acc)
	if err != nil {
		log.Printf("enc.Encode() error: %+v", err)
		return
	}

	encrypted, ok := secretbox.Seal(buf.Bytes(), config.Config.AuthBytes)
	if !ok {
		log.Print("secretbox.Seal() error")
		return
	}

	conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
	_, err = conn.Write(encrypted)
	if err != nil {
		log.Printf("conn.Write() error: %+v", err)
		fmt.Println(utils.ProtocolError.Error())
		return
	}

	conn.Close()
}
