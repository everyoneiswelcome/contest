#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <signal.h>
#include <sys/signalfd.h>

#include <pthread.h>

#include <openssl/pem.h>
#include <openssl/conf.h>
#include <openssl/x509v3.h>
#include <openssl/ssl.h>
#include <openssl/x509_vfy.h>
#include <openssl/sha.h>
#include <openssl/pkcs12.h>
#include <openssl/err.h>
#include <openssl/crypto.h>

#include "common/cJSON.h"
#include "common/rbt.h"

#define LOCAL_ADDR "127.0.0.1"
//#define ERROR_STRING "\033[22;31mERROR\033[0m "
#define ERROR_STRING "ERROR "

//#define ATM_THREADS

// To debug the application, uncomment the below line
//#define ATM_DEBUG

#ifdef ATM_DEBUG
#define LOG(fmt, ...) fprintf(stderr, "[%s:%s:%d] " fmt"\n", __FILE__,__func__, __LINE__, ##__VA_ARGS__)
//#define LOG_ERROR(fmt, ...) fprintf(stderr, "[%s:%s:%d] "ERROR_STRING fmt"\n", __FILE__,__func__, __LINE__, ##__VA_ARGS__)
#define LOG_ERROR(fmt, ...) fprintf(stderr, "[%s:%s:%d] port %d, "ERROR_STRING fmt"\n", __FILE__,__func__, __LINE__, getThreadSpecificData(portKey), ##__VA_ARGS__)
#else
#define LOG(fmt, ...)
#define LOG_ERROR(fmt, ...)
#endif

extern int client_port;
extern pthread_key_t portKey;
int getThreadSpecificData(pthread_key_t key);

#define LOG_ENTRY(fmt, ...) LOG("ENTRY " fmt, ##__VA_ARGS__)
#define LOG_EXIT(fmt, ...) LOG("EXIT " fmt, ##__VA_ARGS__)

#define ERROR_CHECK(cond, str, err, jump) \
  if(cond) { \
    LOG_ERROR(str); \
    ret = err; \
    goto jump; \
  }

#define FREE_STRING(str) \
  if(str) {\
    free(str); \
    str = NULL; \
  }

#define DEFAULT_AUTH "bank.auth"
#define MAX_INTEGER_STORAGE 33 /*sizeof(int)*8+1*/
#define MAX_DOUBLE_STORAGE 65 /*sizeof(double)*8+1*/

#define OK 0
#define ERROR 255
#define PROTOCOL_ERROR 63

void thread_setup(void);
void thread_cleanup(void);

/* common/tcpHelpers.c */
int createSignalFD(int *signal_fd, sigset_t *mask);
int checkForSignal(int sfd, int *p_endFlag);
int tcpAccept(int *p_client_soc, int srvr_soc, struct sockaddr_in *clnt_addr, int signal_fd, int *p_isExitFlag);
int tcpConnect(char *ip, int port, int *p_soc, int signal_fd, int *p_isExitFlag);
int tcpListen(const char *ip, int port, int *p_soc);

/* common/sslHelpers.c */
int sslServerInitializeContext(SSL_CTX **p_ctx, X509 *x509, EVP_PKEY *pkey);
int sslClientInitializeContext(SSL_CTX **p_ctx, char *CAfile, char *CApath);
int isRetryNeeded(int soc, SSL* ssl, int err, int signal_fd, int *p_isExitFlag);
int sslAccept(int soc, SSL_CTX *ctx, SSL **p_ssl, int signal_fd, int *p_isExitFlag);
int sslConnect(int soc, SSL_CTX *ctx, SSL **p_ssl, int signal_fd, int *p_isExitFlag);
char const* crt_strerror(int err);

/* common/boundCheckers.c */
int isFileNameValid(const char *name);
int isAccountNameValid(const char *name);
int isPortValid(int port);
int isIPValid(const char *ip);
int isAmountValid(double amount);
int isNumericValid(const char *num, int isDotsAllowed);

int mkcert(X509 **x509p, EVP_PKEY **pkeyp, int bits, int serial, int days);
int add_ext(X509 *cert, int nid, char *value);

#define MAX_FILE_NAME 255
#define MAX_ACCOUNT_NAME 250
#define MAX_ARGUMENT_NAME 4096

#define INITIAL_AMOUNT 10.00

enum PERFORM_ACTION {
  NONE=0,
  CREATE=1,
  DEPOSIT=2,
  WITHDRAW=3,
  BALANCE=4
};

#define CARD_TAG "cardDetails"
#define ACCOUNT_TAG "acntName"
#define ACTION_TAG "action"
#define AMOUNT_TAG "amount"
#define STATUS_TAG "status"
#define RESPONSE_TAG "response"

// indexed by enum PERFORM_ACTION
static char *responseNames[] = {
  "account", /*ACCOUNT_NAME*/
  "initial_balance", /*CREATE*/
  "deposit", /*DEPOSIT*/
  "withdraw", /*WITHDRAW*/
  "balance" /*BALANCE*/
};

#define RESPONSE_TO_PRINT_FORMAT "{\\\"%s\\\":\\\"%s\\\",\\\"%s\\\":%.2f}\n"
#define RESPONSE_TO_PRINT2_FORMAT "{\"%s\":\"%s\",\"%s\":%.2f}\n"
#define RESPONSE_TO_SEND_FORMAT "{\"%s\":%d,\"%s\":\"%s\"}"
#define RESPONSE_CREATE_TO_SEND_FORMAT "{\"%s\":%d,\"%s\":\"%s\",\"%s\":%d}"

