#include "../defs.h"

int getThreadSpecificData(pthread_key_t key) {
#ifdef ATM_THREADS
  int ret = (int) pthread_getspecific(key);
  return ret;
#else
  return client_port;
#endif
}
