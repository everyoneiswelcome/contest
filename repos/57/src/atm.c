#include "defs.h"

int do_getopt(int argc, char *argv[], char **p_authFile, char **p_ip, int *p_port, char *cardFile, char **p_acntName, int *p_action, double *p_amount);
int createJSON(char **JSONstring, int cardDetails, char *acntName, int action, double amount);
int parseJSON(char *JSONstring, int *p_ret, char *responseToPrint, int maxLen, int getCardDetails, int *p_cardDetails);

#ifdef ATM_THREADS
void freeThreadSpecificData(void *value) {
  return;
}
#endif

pthread_key_t portKey = 0;
int client_port = 0;

int main(int argc, char *argv[]) {
  int ret = OK, port = 3000, soc = 0, status = OK;
  socklen_t soc_len;
  struct sockaddr_in clnt_addr;
  SSL_CTX *ctx = NULL;
  SSL *ssl = NULL;

  char *authFile = DEFAULT_AUTH, *ip = LOCAL_ADDR;
  char cardFile[MAX_FILE_NAME+1], *acntName = NULL;
  int action = NONE;
  double amount = -1;

  char *responseToPrint = NULL;
  int responseToPrintMaxLen = sizeof(char)*(MAX_FILE_NAME + MAX_INTEGER_STORAGE + MAX_DOUBLE_STORAGE + 100);

  int cardDetails = 0;
  FILE *fp=NULL;

  char *JSONstring = NULL;
  char buffer[1024] = "test";
  int nbytes;

  int signal_fd = 0;
  sigset_t mask;
  int isExitFlag = 0;

  LOG_ENTRY();

#ifdef ATM_THREADS
  ret = pthread_key_create(&portKey, freeThreadSpecificData);
  if(ret != 0) {
    LOG("pthread_key_create() failed");
    ret = ERROR;
    goto end;
  }
  pthread_setspecific(portKey, NULL);
#endif

  cardFile[MAX_FILE_NAME] = '\0';

  ret = createSignalFD(&signal_fd, &mask);
  ERROR_CHECK(ret == ERROR, "createSignalFD() failed", ERROR, end);

  ret = do_getopt(argc, argv, &authFile, &ip, &port, cardFile, &acntName, &action, &amount);
  ERROR_CHECK(ret == ERROR, "do_getopt() failed", ERROR, end);

  if(action == CREATE) {
#if 1
    fp = fopen(cardFile, "r");
    if(fp) {
      LOG_ERROR("%s exists",cardFile);
      ret = ERROR;
      goto end;
    }
#endif
    ERROR_CHECK(amount < INITIAL_AMOUNT, "initial ammount cannot be less than INITIAL_AMOUNT", ERROR, end);
    cardDetails = -1;
  } else {
    fp = fopen(cardFile, "r");
    ERROR_CHECK(!fp, "fopen() failed", ERROR, end);
    ret = fscanf(fp, "%d", &cardDetails);
    ERROR_CHECK(ret != 1, "fscanf() failed", ERROR, end);
  }

  ret = sslClientInitializeContext(&ctx, authFile, NULL);
  ERROR_CHECK(ret == ERROR, "sslClientInitializeContext() failed", ERROR, end);

  ret = tcpConnect(ip, port, &soc, signal_fd, &isExitFlag);
  ERROR_CHECK(isExitFlag, "Exit flag set", ERROR, end);
  ERROR_CHECK(ret == ERROR, "tcpConnect() failed", PROTOCOL_ERROR, end);
#ifdef ATM_THREADS
  pthread_setspecific(portKey, (void *)getPortFromSocket(soc));
#else
  client_port = getPortFromSocket(soc);
#endif

  ret = sslConnect(soc, ctx, &ssl, signal_fd, &isExitFlag);
  ERROR_CHECK(isExitFlag, "Exit flag set", ERROR, end);
  ERROR_CHECK(ret == ERROR, "sslConnect() failed", PROTOCOL_ERROR, end);

  fcntl(soc, F_SETFL, O_NONBLOCK);

#if 0
  ret = createJSON(&JSONstring, cardDetails, acntName, action, amount);
#else
  ret = createRequest(&JSONstring, cardDetails, acntName, action, amount);
#endif
  ERROR_CHECK(ret == ERROR, "createJSON() failed", ERROR, end);

  LOG("%s",JSONstring);

  do{
    nbytes = SSL_write(ssl, JSONstring, strlen(JSONstring));
    if(nbytes <=0 && isRetryNeeded(soc, ssl, nbytes, signal_fd, &isExitFlag) != OK) {
      ERROR_CHECK(isExitFlag, "Exit flag set", ERROR, end);
      break;
    }
    ERROR_CHECK(isExitFlag, "Exit flag set", ERROR, end);
  } while(nbytes <= 0);
  free(JSONstring);
  ERROR_CHECK(nbytes <= 0, "SSL_write() failed", PROTOCOL_ERROR, end);

  do{
    nbytes = SSL_read(ssl, buffer, 1024);
    if(nbytes <=0 && isRetryNeeded(soc, ssl, nbytes, signal_fd, &isExitFlag) != OK) {
      ERROR_CHECK(isExitFlag, "Exit flag set", ERROR, end);
      break;
    }
    ERROR_CHECK(isExitFlag, "Exit flag set", ERROR, end);
  } while(nbytes <= 0);
  ERROR_CHECK(nbytes <= 0, "SSL_read() failed", PROTOCOL_ERROR, end);

  LOG("%s",buffer);

#if 0
  responseToPrint = (char *)malloc(responseToPrintMaxLen);
  if(action != CREATE) {
    ret = parseJSON(buffer, &nbytes, responseToPrint, responseToPrintMaxLen, 0, NULL);
    ERROR_CHECK(ret == ERROR, "parseJSON() failed", ERROR, end);
  } else {
    ret = parseJSON(buffer, &nbytes, responseToPrint, responseToPrintMaxLen, 1, &cardDetails);
    ERROR_CHECK(ret == ERROR, "parseJSON() failed", ERROR, end);
    fp = fopen(cardFile, "w");
    if(!fp) {
      LOG_ERROR("%s unable to create, %d",cardFile, errno);
      ret = ERROR;
      goto end;
    }
    fprintf(fp, "%d", cardDetails);
  }

  printf("%s",responseToPrint);
  fflush(stdout);
#else
  ret = parseResponse(buffer, &status, &amount, &cardDetails);
  ERROR_CHECK(ret == ERROR, "parseResponse() failed", ERROR, end);
  ret = status;
  if(ret == OK && action == CREATE){
    fp = fopen(cardFile, "w");
    if(!fp) {
      LOG_ERROR("%s unable to create, %d",cardFile, errno);
      ret = ERROR;
      goto end;
    }
    fprintf(fp, "%d", cardDetails);
  } else if(ret != OK)
    goto end;
  printf(RESPONSE_TO_PRINT2_FORMAT, responseNames[NONE], acntName, responseNames[action], amount);
  fflush(stdout);
#endif

end:
#ifdef ATM_THREADS
  pthread_key_delete(portKey);
#endif
  if(signal_fd) close(signal_fd);
  if(fp) fclose(fp);
  FREE_STRING(responseToPrint);
  if(ssl) {
    SSL_shutdown(ssl);
    SSL_free(ssl);
  }
  if(ctx) SSL_CTX_free(ctx);
  if(soc) close(soc);
  ERR_free_strings();
  LOG_EXIT();
  return ret;
}

int do_getopt(int argc, char *argv[], char **p_authFile, char **p_ip, int *p_port, char *cardFile, char **p_acntName, int *p_action, double *p_amount) {
  int ret = OK, c;
  int isFilledAuthFile = 0, isFilledIP = 0, isFilledPort = 0, isFilledCardFile = 0, isFilledAcntName = 0, isFilledAction = 0, isFilledAmount = 0;
  LOG_ENTRY();

  // *cardFile is an array
  while((c = getopt(argc, argv, "s:i:p:c:a:n:d:w:g::")) != -1) {
    switch(c) {
      case 's':
        *p_authFile = optarg;
        isFilledAuthFile++;
        break;
      case 'i':
        ret = isIPValid(optarg);
        ERROR_CHECK(ret == ERROR, "isIPValid() failed", ERROR, end);
        *p_ip = optarg;
        isFilledIP++;
        break;
      case 'p':
        ret = isNumericValid(optarg, 0);
        ERROR_CHECK(ret == ERROR, "isNumericValid() failed", ERROR, end);
        *p_port = atoi(optarg);
        isFilledPort++;
        break;
      case 'c':
        ERROR_CHECK(!optarg, "name of card file is empty", ERROR, end);
        ret = isFileNameValid(optarg);
        ERROR_CHECK(ret == ERROR, "isFileNameValid() failed", ERROR, end);
        strncpy(cardFile, optarg, MAX_FILE_NAME);
        isFilledCardFile++;
        break;
      case 'a':
        *p_acntName = optarg;
        isFilledAcntName++;
        break;
      case 'n':
        *p_action = CREATE;
        ret = isNumericValid(optarg, 1);
        ERROR_CHECK(ret == ERROR, "isNumericValid() failed", ERROR, end);
        *p_amount = atof(optarg);
        isFilledAction++;
        break;
      case 'd':
        *p_action = DEPOSIT;
        ret = isNumericValid(optarg, 1);
        ERROR_CHECK(ret == ERROR, "isNumericValid() failed", ERROR, end);
        *p_amount = atof(optarg);
        isFilledAction++;
        break;
      case 'w':
        *p_action = WITHDRAW;
        ret = isNumericValid(optarg, 1);
        ERROR_CHECK(ret == ERROR, "isNumericValid() failed", ERROR, end);
        *p_amount = atof(optarg);
        isFilledAction++;
        break;
      case 'g':
        *p_action = BALANCE;
        if(!optarg) {
          if(argv[optind] != NULL)
            if(argv[optind][0] != '-') {
              ret = ERROR;
              goto end;
            }
        }
        *p_amount = 1;
        isFilledAction++;
        break;
      case '?':
        LOG_ERROR("Unknown option -%c",optopt);
        ret = ERROR;
        goto end;
    }
  }

  // Check if Account Nmae is received
  ERROR_CHECK(isFilledAuthFile > 1 || isFilledIP > 1 || isFilledPort > 1 || isFilledCardFile > 1, "Duplicate parameters", ERROR, end);
  ERROR_CHECK(isFilledAcntName != 1, "Account name missing or duplicate", ERROR, end);
  ERROR_CHECK(isFilledAction != 1, "Action missing or duplicate", ERROR, end);

  ret = isFileNameValid(*p_authFile);
  ERROR_CHECK(ret == ERROR, "isFileNameValid() failed", ERROR, end);
  ret = isAccountNameValid(*p_acntName);
  ERROR_CHECK(ret == ERROR, "isAccountNameValid() failed", ERROR, end);
  ret = isPortValid(*p_port);
  ERROR_CHECK(ret == ERROR, "isPortValid() failed", ERROR, end);
  ret = isAmountValid(*p_amount);
  ERROR_CHECK(ret == ERROR, "isAmountValid() failed", ERROR, end);

  // Check if cardFile is received
  if(isFilledCardFile == 0) {
    snprintf(cardFile, MAX_FILE_NAME+1, "%s.card",*p_acntName);
  }

  // Check if amount is received, need for all action except withdrawl
  LOG("Arguments:\nauth file\t%s\nserver address\t%s:%d\naccount name\t%s\ncard file\t%s\naction\t%d\namount\t%f", *p_authFile, *p_ip, *p_port, *p_acntName, cardFile, *p_action, *p_amount);

  LOG_EXIT();

end:
  return ret;
}

int createJSON(char **JSONstring, int cardDetails, char *acntName, int action, double amount) {
  int ret = OK;
  cJSON *root;

  root = cJSON_CreateObject();
  cJSON_AddNumberToObject(root, CARD_TAG, cardDetails);
  cJSON_AddStringToObject(root, ACCOUNT_TAG, acntName);
  cJSON_AddNumberToObject(root, ACTION_TAG, action);
  cJSON_AddNumberToObject(root, AMOUNT_TAG, amount);

  *JSONstring = cJSON_Print(root);

  cJSON_Delete(root);
end:
  return ret;
}

int parseJSON(char *JSONstring, int *p_ret, char *responseToPrint, int maxLen, int getCardDetails, int *p_cardDetails) {
  int ret = OK;
  cJSON *json = NULL;

  json = cJSON_Parse(JSONstring);
  ERROR_CHECK(!json, "cJSON_Parse() failed", ERROR, end);

  *p_ret = cJSON_GetObjectItem(json, STATUS_TAG)->valueint;
  ERROR_CHECK(*p_ret == ERROR, "Request failed", ERROR, end);
  snprintf(responseToPrint, maxLen, "%s", cJSON_GetObjectItem(json, RESPONSE_TAG)->valuestring);

  if(getCardDetails) {
    *p_cardDetails = cJSON_GetObjectItem(json, CARD_TAG)->valueint;
  }

end:
  if(json) cJSON_Delete(json);
  return ret;
}
