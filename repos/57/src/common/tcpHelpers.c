#include "../defs.h"

int getPortFromSocket(int soc) {
#if 1
  struct sockaddr_in server;
  int len = sizeof(server);
  if(!soc) return 0;
  getsockname(soc, (struct sockaddr *)&server, &len);
  return ntohs(server.sin_port);
#else
  return 0;
#endif
}

int createSignalFD(int *signal_fd, sigset_t *mask) {
  int ret = OK;

  sigemptyset(mask);
  sigaddset(mask, SIGTERM);
  sigaddset(mask, SIGINT);

  ret = sigprocmask(SIG_BLOCK, mask, NULL);
  ERROR_CHECK(ret < 0, "sigprocmask() failed", ERROR, end);

  *signal_fd = signalfd(-1, mask, 0);
  ERROR_CHECK(*signal_fd < 0, "signalfd() failed", ERROR, end);

  fcntl(*signal_fd, F_SETFL, O_NONBLOCK);

end:
  return ret;
}

int checkForSignal(int sfd, int *p_endFlag) {
  int ret = OK;
  struct signalfd_siginfo si;
  ssize_t res;

  *p_endFlag = 0;
  res = read (sfd, &si, sizeof(si));

  if(res < 0 && errno == EAGAIN)
    return OK;

  ERROR_CHECK(res < 0 || res != sizeof(si), "read() failed", ERROR, end);

  if (si.ssi_signo == SIGTERM) {
    LOG("Got SIGTERM");
    *p_endFlag = 1;
  }
  //TODO : Comment this
#if 1
  else if (si.ssi_signo == SIGINT) {
    LOG("Got SIGINT");
    *p_endFlag = 1;
  }
#endif
  else {
    LOG("Got some unhandled signal, %d\n", si.ssi_signo);
  }

end:
  return ret;
}

int tcpAccept(int *p_client_soc, int srvr_soc, struct sockaddr_in *clnt_addr, int signal_fd, int *isExitFlag) {
  int ret = OK, len = 0, result;
  socklen_t soc_len;
  fd_set read_fds;

  FD_ZERO(&read_fds);

  soc_len = sizeof(struct sockaddr);
  ret = accept(srvr_soc, (struct sockaddr *)clnt_addr, &soc_len);
  ERROR_CHECK(ret == -1 && errno != EAGAIN, "accept() failed", ERROR, end);
  if(ret > 0) {
    LOG("accept() success");
    *p_client_soc = ret;
    ret = OK;
    goto end;
  }

  FD_SET(srvr_soc, &read_fds);
  if(signal_fd) FD_SET(signal_fd, &read_fds);

  len = (srvr_soc > signal_fd)?srvr_soc:signal_fd;
  ret = select(len+1, &read_fds, NULL, NULL, NULL);
  ERROR_CHECK(ret == 0, "select() timed-out", ERROR, end);

  if(FD_ISSET(signal_fd, &read_fds)) {
    ret = checkForSignal(signal_fd, &result);
    ERROR_CHECK(ret == ERROR, "checkForSignal() failed", ERROR, end);
    *isExitFlag = result;
    *p_client_soc = result-1;
    ERROR_CHECK(result, "SIGTERM", ERROR, end);
  }
  ERROR_CHECK(!FD_ISSET(srvr_soc, &read_fds), "FD_ISSET() not true", ERROR, end);
  len = sizeof(ret);
  result = getsockopt(srvr_soc, SOL_SOCKET, SO_ERROR, &ret, &len);
  ERROR_CHECK(result != 0 || ret, "getsockopt() or socket failed", ERROR, end);

  soc_len = sizeof(struct sockaddr);
  ret = accept(srvr_soc, (struct sockaddr *)clnt_addr, &soc_len);
  ERROR_CHECK(ret == -1, "accept() failed", ERROR, end);
  if(ret > 0) {
    LOG("accept() success");
    *p_client_soc = ret;
    ret = OK;
  } else
    ret = ERROR;

end:
  return ret;
}

int tcpConnect(char *ip, int port, int *p_soc, int signal_fd, int *p_isExitFlag) {
  int ret = OK, soc = 0, len = 0, result = 0;
  struct sockaddr_in server;
  struct hostent *hp;

  fd_set write_fds, read_fds;

  soc = socket(AF_INET, SOCK_STREAM, 0);
  ERROR_CHECK(soc < 0, "socket() failed", ERROR, end);

  hp = gethostbyname(ip);
  ERROR_CHECK(!hp, "gethostbyname() failed", ERROR, end);

  memcpy((void *)&server.sin_addr, hp->h_addr_list[0], hp->h_length);
  server.sin_family = AF_INET;
  server.sin_port = htons(port);

  fcntl(soc, F_SETFL, O_NONBLOCK);

  ret = connect(soc, (struct sockaddr *)&server, sizeof(server));
  ERROR_CHECK(ret == -1 && errno != EINPROGRESS, "connect() failed", ERROR, end);
  if(ret > 0) {
    *p_soc = ret;
    return OK;
  }

  FD_ZERO(&read_fds);
  FD_ZERO(&write_fds);
  FD_SET(soc, &write_fds);
  if(signal_fd) FD_SET(signal_fd, &read_fds);

  len = (soc > signal_fd)?soc:signal_fd;
  ret = select(len+1, &read_fds, &write_fds, NULL, NULL);
  ERROR_CHECK(ret == 0, "select() timed-out", ERROR, end);

  if(signal_fd && FD_ISSET(signal_fd, &read_fds)) {
    ret = checkForSignal(signal_fd, &result);
    ERROR_CHECK(ret == ERROR, "checkForSignal() failed", ERROR, end);
    *p_isExitFlag = result;
    ERROR_CHECK(result, "SIGTERM", ERROR, end);
  }

  ERROR_CHECK(!FD_ISSET(soc, &write_fds), "FD_ISSET() not true", ERROR, end);
  len = sizeof(ret);
  result = getsockopt(soc, SOL_SOCKET, SO_ERROR, &ret, &len);
  ERROR_CHECK(result != 0 || ret, "getsockopt() or socket failed", ERROR, end);

  *p_soc = soc;

end:
  if(ret == ERROR && soc) close(soc);
  return ret;
}

int tcpListen(const char *ip, int port, int *p_soc) {
  int ret = OK;
  int soc = OK;
  struct sockaddr_in myaddr;

  soc = socket(AF_INET, SOCK_STREAM, 0);
  ERROR_CHECK(soc < 0, "socket() failed", ERROR, end);

  memset((void *)&myaddr, 0, sizeof(myaddr));
  myaddr.sin_family = AF_INET;
  myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  myaddr.sin_port = htons(port);

  ret = bind(soc, (struct sockaddr *)&myaddr, sizeof(myaddr));
  ERROR_CHECK(ret < 0, "bind() failed", ERROR, end);

  ret = listen(soc, 1);
  ERROR_CHECK(ret < 0, "listen() failed", ERROR, end);

  *p_soc = soc;

end:
  return ret;
}
