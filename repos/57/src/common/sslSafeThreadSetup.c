#include "../defs.h"

static pthread_mutex_t *lock_cs;
static long *lock_count;

void pthreads_thread_id(CRYPTO_THREADID *tid)
{
  CRYPTO_THREADID_set_numeric(tid, (unsigned long)pthread_self());
}

unsigned long pthreads_thread_id_func(void)
{
  return ((unsigned long) pthread_self());
  //CRYPTO_THREADID_set_numeric(tid, (unsigned long)pthread_self());
}

void pthreads_locking_callback(int mode, int type, const char *file, int line)
{
  if (mode & CRYPTO_LOCK) {
    pthread_mutex_lock(&(lock_cs[type]));
    lock_count[type]++;
  } else {
    pthread_mutex_unlock(&(lock_cs[type]));
  }
}

void thread_setup(void)
{
  int i;

  lock_cs = OPENSSL_malloc(CRYPTO_num_locks() * sizeof(pthread_mutex_t));
  lock_count = OPENSSL_malloc(CRYPTO_num_locks() * sizeof(long));
  for (i = 0; i < CRYPTO_num_locks(); i++) {
    lock_count[i] = 0;
    pthread_mutex_init(&(lock_cs[i]), NULL);
  }

  //CRYPTO_THREADID_set_callback(pthreads_thread_id);
  CRYPTO_set_id_callback(pthreads_thread_id_func);
  CRYPTO_set_locking_callback(pthreads_locking_callback);
}

void thread_cleanup(void)
{
  int i;

  CRYPTO_set_locking_callback(NULL);
  CRYPTO_set_id_callback(NULL);
  for (i = 0; i < CRYPTO_num_locks(); i++) {
    pthread_mutex_destroy(&(lock_cs[i]));
  }
  OPENSSL_free(lock_cs);
  OPENSSL_free(lock_count);
}
