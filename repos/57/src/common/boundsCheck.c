#include "../defs.h"

int isFileNameValid(const char *name) {
  int ret = OK;
  int len = strlen(name), i, hasDots = 0;

  ERROR_CHECK(len > MAX_FILE_NAME, "File name is more than MAX_FILE_NAME chars", ERROR, end);

  for(i=0;i<len;i++) {
    // Check lowercase alphabetical characters
    if(name[i] >= 'a' && name[i] <= 'z')
      continue;
    // Check digits
    else if(name[i] >= '0' && name[i] <= '9')
      continue;
    // Check for dots
    else if(name[i] == '.') {
      hasDots++;
      continue;
    }
    // Check for underscore
    else if(name[i] == '_')
      continue;
    else {
      ret = ERROR;
      break;
    }
  }

  ERROR_CHECK(hasDots == len, "File name cannot be . or ..", ERROR, end);
end:
  return ret;
}

int isAccountNameValid(const char *name) {
  int ret = OK;
  int len = strlen(name), i;

  ERROR_CHECK(len > MAX_ACCOUNT_NAME, "Account name is more than MAX_FILE_NAME chars", ERROR, end);

  for(i=0;i<len;i++) {
    // Check lowercase alphabetical characters
    if(name[i] >= 'a' && name[i] <= 'z')
      continue;
    // Check digits
    else if(name[i] >= '0' && name[i] <= '9')
      continue;
    // Check for dots
    else if(name[i] == '.')
      continue;
    // Check for underscore
    else if(name[i] == '_')
      continue;
    else {
      ret = ERROR;
      break;
    }
  }

end:
  return ret;
}

int isPortValid(int port) {
  if(port < 1024 || port > 65535)
    return ERROR;
  else
    return OK;
}

int isIPValid(const char *ip) {
  int ret = OK;
  int len = strlen(ip), i = 0, dotCount = 0, zeroCount = 0;
  char *buffer=NULL;
  char *pch;

  buffer = malloc(strlen(ip)+10);
  strcpy(buffer, ip);
  ERROR_CHECK(len > MAX_ARGUMENT_NAME, "argument is more than MAX_ARGUMENT_NAME chars", ERROR, end);

  pch = strtok(buffer, ".");
  while(pch != NULL) {
    len = strlen(pch);
    for(i=0;i<len;i++) {
      if(pch[i] < '0' && pch[i] > '9') {
        ret = ERROR;
        goto end;
      }
    }
    if(len != 1 && pch[0] == '0') {
        ret = ERROR;
        goto end;
    }
    pch = strtok(NULL, ".");
  }
#if 0
  for(i=0;i<len;i++) {
    if(ip[i] < '0' || ip[i] > '9') {
      if(ip[i] == '.') {
        dotCount++;
        continue;
      }
      ret = ERROR;
      goto end;
    }
  }

  if(dotCount != 3)
    ret = ERROR;
#endif
end:
  free(buffer);
  return ret;
}

int isAmountValid(double amount) {
  if(amount <= 0.00 || amount > 4294967295.99)
    return ERROR;
  else
    return OK;
}

int isNumericValid(const char *num, int isDotsAllowed) {
  int ret = OK;
  int len = strlen(num), i;
  int dotCount = 0, dotLocation = len;

  ERROR_CHECK(len > MAX_ARGUMENT_NAME, "argument is more than MAX_ARGUMENT_NAME chars", ERROR, end);

  for(i=0;i<len;i++) {
    if(num[i] < '0' || num[i] > '9') {
      if(num[i] == '.') {
        if(!isDotsAllowed)
          return ERROR;
        dotCount++;
        dotLocation = i;
        continue;
      } else {
        return ERROR;
      }
    } else {
      continue;
    }
  }

  if(dotLocation == 0 || (dotLocation != 1 && num[0] == '0') || (dotCount == 1 && len-dotLocation != 3) || (isDotsAllowed && dotCount !=1))
    ret = ERROR;

  ERROR_CHECK(dotCount > 1 || len - dotLocation > 3, "Only two digits after decimal is allowed", ERROR, end );

end:
  return ret;
}
