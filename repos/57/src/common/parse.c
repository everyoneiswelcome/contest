#include "../defs.h"

int createRequest(char **_buffer, int cardDetails, char *acntName, int action, double amount) {
  char *buffer;
  int len;

  len = strlen(acntName) + 2*MAX_INTEGER_STORAGE + MAX_DOUBLE_STORAGE + 20;
  buffer = (char *)malloc(sizeof(char)*len);

  snprintf(buffer, len, "#%s#%d#%d#%f#", acntName, cardDetails, action, amount);

  *_buffer = buffer;
  LOG("Request : %s", buffer);

  return OK;
}

int parseRequest(char *buffer, int *p_cardDetails, char *p_acntName, int *p_action, double *p_amount) {
  int ret = OK, i = 0;
  char *pch;

  LOG("Request : %s", buffer);
  pch = strtok(buffer, "#");
  while(pch != NULL) {
    switch(i) {
      case 0:
        strcpy(p_acntName, pch);
        break;
      case 1:
        *p_cardDetails = atoi(pch);
        break;
      case 2:
        *p_action = atoi(pch);
        break;
      case 3:
        *p_amount = atof(pch);
        break;
      default:
        ERROR_CHECK(1, "parseResponse() failed", ERROR, end);
    }
    i++;
    pch = strtok(NULL, "#");
  }
end:
  return ret;
}

int createResponse(char **_buffer, int status, double amount, int cardDetails) {
  // TODO: Doesnt work with multithreads
  static char buffer[2*MAX_INTEGER_STORAGE + MAX_DOUBLE_STORAGE+20];

  snprintf(buffer, 2*MAX_INTEGER_STORAGE + MAX_DOUBLE_STORAGE+20, "#%d#%f#%d#", status, amount, cardDetails);

  *_buffer = buffer;

  LOG("Response : %s", buffer);

  return OK;
}

int parseResponse(char *buffer, int *p_status, double *p_amount, int *p_cardDetails) {
  int ret = OK, i = 0;
  char *pch;

  LOG("Response : %s", buffer);

  pch = strtok(buffer, "#");
  while(pch != NULL) {
    switch(i) {
      case 0:
        *p_status = atoi(pch);
        break;
      case 1:
        *p_amount = atof(pch);
        break;
      case 2:
        if(p_cardDetails) *p_cardDetails = atoi(pch);
        break;
      default:
        ERROR_CHECK(1, "parseResponse() failed", ERROR, end);
    }
    i++;
    pch = strtok(NULL, "#");
  }
end:
  return ret;
}

int main2() {
  char *buffer, name[255];
  int card, action;
  double amount;

  createRequest(&buffer, 1, "tarun", 2, 10.10);
  printf("Request: %s\n", buffer);
  parseRequest(buffer, &card, name, &action, &amount);
  printf("%s %d %d %f\n", name, card, action, amount);

  free(buffer);

  createResponse(&buffer, 1, 10.00, 1234);
  printf("Response: %s\n", buffer);
  parseResponse(buffer, &action, &amount, &card);
  printf("%d %f %d\n", action, amount, card);

  return OK;
}
