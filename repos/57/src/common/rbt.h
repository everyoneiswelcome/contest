
//////////////////////
// supplied by user //
//////////////////////

typedef int KeyType;            // type of key

typedef struct {                // value related to key
    char name[256];
    double amount;
} ValType;

/////////////////////////////////////
// implementation independent code //
/////////////////////////////////////
typedef enum { BLACK, RED } nodeColor;

typedef enum {
    RBT_STATUS_OK,
    RBT_STATUS_MEM_EXHAUSTED,
    RBT_STATUS_DUPLICATE_KEY,
    RBT_STATUS_KEY_NOT_FOUND
} RbtStatus;

typedef struct NodeTag {
    struct NodeTag *left;       // left child
    struct NodeTag *right;      // right child
    struct NodeTag *parent;     // parent
    nodeColor color;            // node color (BLACK, RED)
    KeyType key;                // key used for searching
    ValType val;                // data related to key
} NodeType;

NodeType *rbtFind(KeyType key);
RbtStatus rbtInsert(KeyType key, ValType val, NodeType **node);

typedef struct {
  char name[256];
}strKeyType;            // type of key

typedef struct {                // value related to key
  NodeType *node;
} strValType;

typedef struct strNodeTag {
    struct strNodeTag *left;       // left child
    struct strNodeTag *right;      // right child
    struct strNodeTag *parent;     // parent
    nodeColor color;            // node color (BLACK, RED)
    strKeyType key;                // key used for searching
    strValType val;                // data related to key
} strNodeType;

strNodeType *strrbtFind(strKeyType key);
RbtStatus strrbtInsert(strKeyType key, strValType val, strNodeType **node);

