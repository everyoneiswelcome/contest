#include "../defs.h"

char const* crt_strerror(int err);

int sslServerInitializeContext(SSL_CTX **p_ctx, X509 *x509, EVP_PKEY *pkey) {
  int ret = OK;
  const SSL_METHOD *method;
  SSL_CTX *ctx = NULL;

  SSL_library_init();
  OpenSSL_add_all_algorithms();
  SSL_load_error_strings();

  method = SSLv3_server_method();
  ctx = SSL_CTX_new(method);
  ERROR_CHECK(!ctx, "SSL_CTX_new() failed", ERROR, end);

  SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);

  SSL_CTX_set_mode(ctx, SSL_MODE_ACCEPT_MOVING_WRITE_BUFFER | SSL_MODE_ENABLE_PARTIAL_WRITE);

  // TODO: In error cases, ctx has to be freed
  ret = SSL_CTX_use_certificate(ctx, x509);
  ERROR_CHECK(ret != 1, "SSL_CTX_use_certificate() failed", ERROR, end);

  // TODO: In error cases, ctx has to be freed
  ret = SSL_CTX_use_PrivateKey(ctx, pkey);
  ERROR_CHECK(ret != 1, "SSL_CTX_use_PrivateKey() failed", ERROR, end);

  ret = X509_STORE_add_cert(SSL_CTX_get_cert_store(ctx), x509);
  ERROR_CHECK(ret != 1, "X509_STORE_add_cert() failed", ERROR, end);

  ret = OK;
end:
  *p_ctx = ctx;
  return ret;
}

int sslClientInitializeContext(SSL_CTX **p_ctx, char *CAfile, char *CApath) {
  int ret = OK;
  const SSL_METHOD *method;
  SSL_CTX *ctx = NULL;
  FILE *fp = NULL;

  X509 *x509=NULL;
  EVP_PKEY *pkey=NULL;
  PKCS12 *p12 = NULL;

  SSL_library_init();
  OpenSSL_add_all_algorithms();
  SSL_load_error_strings();

  method = SSLv3_client_method();
  ctx = SSL_CTX_new(method);
  ERROR_CHECK(!ctx, "SSL_CTX_new() failed", ERROR, end);

  SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);

#if 0
  ret = SSL_CTX_load_verify_locations(ctx, CAfile, CApath);
  ERROR_CHECK(ret != 1, "SSL_CTX_load_verify_locations() failed", ERROR, end);
#else
  fp = fopen(CAfile, "r");
  ERROR_CHECK(!fp, "fopen() failed", ERROR, end);

  p12 = d2i_PKCS12_fp(fp, NULL);
  ERROR_CHECK(!p12, "d2i_PKCS12_fp() failed", ERROR, end);

  ret = PKCS12_parse(p12, DEFAULT_AUTH, &pkey, &x509, NULL);
  ERROR_CHECK(!ret || !pkey || !x509, "PKCS12_parse() failed", ERROR, end);

  ret = SSL_CTX_use_certificate(ctx, x509);
  ERROR_CHECK(ret != 1, "SSL_CTX_use_certificate() failed", ERROR, end);
  ret = SSL_CTX_use_PrivateKey(ctx, pkey);
  ERROR_CHECK(ret != 1, "SSL_CTX_use_PrivateKey() failed", ERROR, end);

  ret = X509_STORE_add_cert(SSL_CTX_get_cert_store(ctx), x509);
  ERROR_CHECK(ret != 1, "X509_STORE_add_cert() failed", ERROR, end);

  ret = OK;
#endif

end:
  *p_ctx = ctx;
  if(p12) PKCS12_free(p12);
  if(x509) X509_free(x509);
  if(pkey) EVP_PKEY_free(pkey);
  return ret;
}

int isRetryNeeded(int soc, SSL* ssl, int err, int signal_fd, int *p_isExitFlag) {
  int ret = OK, result, len;
  fd_set write_fds, read_fds;
  struct timeval timeout;

  FD_ZERO(&write_fds);
  FD_ZERO(&read_fds);
  err = SSL_get_error(ssl, err);
#if 1
  switch(err)
  {
    case SSL_ERROR_NONE:
      LOG("SUCCESS SSL_connect()");
      ret = OK;
      goto end;
      break;
    case SSL_ERROR_WANT_READ:
      LOG_ERROR("SSL_ERROR_WANT_READ");
      FD_SET(soc, &read_fds);
      break;
    case SSL_ERROR_WANT_WRITE:
      LOG_ERROR("SSL_ERROR_WANT_WRITE");
      FD_SET(soc, &write_fds);
      break;
    case SSL_ERROR_ZERO_RETURN:
      LOG_ERROR("SSL_ERROR_ZERO_RETURN");
      ret = ERROR;
      goto end;
      break;
    default:
      //LOG("FAILED %d",err);
      //ERR_print_errors_fp(stderr);
      LOG_ERROR("%d, %s",err, ERR_error_string(ERR_get_error(), NULL));
      ret = ERROR;
      goto end;
      break;
  }
#else
  if(err != SSL_ERROR_WANT_READ && err != SSL_ERROR_WANT_WRITE) {
    ret = ERROR;
    goto end;
  }
#endif

  if(signal_fd) FD_SET(signal_fd, &read_fds);

  //soc = SSL_get_fd(ssl);
  timeout.tv_sec = 10;
  timeout.tv_usec = 0;

  len = (soc > signal_fd)?soc:signal_fd;

  result = select(len+1, &read_fds, &write_fds, NULL, &timeout);
  ERROR_CHECK(result == 0, "select() timed-out", ERROR, end);

  if(signal_fd && FD_ISSET(signal_fd, &read_fds)) {
    ret = checkForSignal(signal_fd, &result);
    ERROR_CHECK(ret == ERROR, "checkForSignal() failed", ERROR, end);
    *p_isExitFlag = result;
    ERROR_CHECK(result, "SIGTERM", ERROR, end);
  }

  ERROR_CHECK((!FD_ISSET(soc, &write_fds) && !FD_ISSET(soc, &read_fds)), "FD_ISSET() not true", ERROR, end);
  len = sizeof(result);
  err = getsockopt(soc, SOL_SOCKET, SO_ERROR, &result, &len);
  ERROR_CHECK(err != 0 || result, "getsockopt() or socket failed", ERROR, end);

end:
  return ret;
}

int sslAccept(int soc, SSL_CTX *ctx, SSL **p_ssl, int signal_fd, int *p_isExitFlag) {
  int ret = OK;
  SSL *ssl = NULL;

  ssl = SSL_new(ctx);
  SSL_set_fd(ssl, soc);

  do {
    ret = SSL_accept(ssl);
    if(ret != 1 && isRetryNeeded(soc, ssl, ret, signal_fd, p_isExitFlag) != OK) {
      ERROR_CHECK(*p_isExitFlag, "Exit flag set", ERROR, end);
      break;
    }
    ERROR_CHECK(*p_isExitFlag, "Exit flag set", ERROR, end);
  } while(ret != 1);

  ERROR_CHECK(ret != 1, "SSL_accept() failed", ERROR, end);

  *p_ssl = ssl;
  return ret;

end:
  if(ssl) {
    SSL_shutdown(ssl);
    SSL_free(ssl);
  }
  *p_ssl = NULL;
  return ERROR;
}

int sslConnect(int soc, SSL_CTX *ctx, SSL **p_ssl, int signal_fd, int *p_isExitFlag) {
  int ret = OK;
  SSL *ssl = NULL;
  int i;
  char buf[1024];

  ssl = SSL_new(ctx);
  SSL_set_fd(ssl, soc);
  do {
    ret = SSL_connect(ssl);
    if(ret != 1 && isRetryNeeded(soc, ssl, ret, signal_fd, p_isExitFlag) != OK) {
      ERROR_CHECK(*p_isExitFlag, "Exit flag set", ERROR, end);
      break;
    }
    ERROR_CHECK(*p_isExitFlag, "Exit flag set", ERROR, end);
  } while(ret != 1);
  ERROR_CHECK(ret != 1, "SSL_connect() failed", ERROR, end);

  X509 *cert;
  cert = SSL_get_peer_certificate(ssl);
  ERROR_CHECK(!cert, "SSL_get_peer_certificate() failed", ERROR, end);
  X509_free(cert);

  ret = SSL_get_verify_result(ssl);
  LOG("SSL_get_verify_result() returned %d, %s",ret,crt_strerror(ret));
  ERROR_CHECK(ret != X509_V_OK, "SSL_get_verify_result() failed", ERROR, end);

  *p_ssl = ssl;
  return ret;

end:
  if(ssl) {
    SSL_shutdown(ssl);
    SSL_free(ssl);
  }
  *p_ssl = NULL;
  return ERROR;
}

char const* crt_strerror(int err){
  switch(err)
  {
    case X509_V_ERR_UNABLE_TO_DECRYPT_CERT_SIGNATURE:
      return "UNABLE_TO_DECRYPT_CERT_SIGNATURE";

    case X509_V_ERR_UNABLE_TO_DECRYPT_CRL_SIGNATURE:
      return "UNABLE_TO_DECRYPT_CRL_SIGNATURE";

    case X509_V_ERR_UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY:
      return "UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY";

    case X509_V_ERR_CERT_SIGNATURE_FAILURE:
      return "CERT_SIGNATURE_FAILURE";

    case X509_V_ERR_CRL_SIGNATURE_FAILURE:
      return "CRL_SIGNATURE_FAILURE";

    case X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD:
      return "ERROR_IN_CERT_NOT_BEFORE_FIELD";

    case X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD:
      return "ERROR_IN_CERT_NOT_AFTER_FIELD";

    case X509_V_ERR_ERROR_IN_CRL_LAST_UPDATE_FIELD:
      return "ERROR_IN_CRL_LAST_UPDATE_FIELD";

    case X509_V_ERR_ERROR_IN_CRL_NEXT_UPDATE_FIELD:
      return "ERROR_IN_CRL_NEXT_UPDATE_FIELD";

    case X509_V_ERR_CERT_NOT_YET_VALID:
      return "CERT_NOT_YET_VALID";

    case X509_V_ERR_CERT_HAS_EXPIRED:
      return "CERT_HAS_EXPIRED";

    case X509_V_ERR_OUT_OF_MEM:
      return "OUT_OF_MEM";

    case X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT:
      return "UNABLE_TO_GET_ISSUER_CERT";

    case X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY:
      return "UNABLE_TO_GET_ISSUER_CERT_LOCALLY";

    case X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE:
      return "UNABLE_TO_VERIFY_LEAF_SIGNATURE";

    case X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT:
      return "DEPTH_ZERO_SELF_SIGNED_CERT";

    case X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN:
      return "SELF_SIGNED_CERT_IN_CHAIN";

    case X509_V_ERR_CERT_CHAIN_TOO_LONG:
      return "CERT_CHAIN_TOO_LONG";

    case X509_V_ERR_CERT_REVOKED:
      return "CERT_REVOKED";

    case X509_V_ERR_INVALID_CA:
      return "INVALID_CA";

    case X509_V_ERR_PATH_LENGTH_EXCEEDED:
      return "PATH_LENGTH_EXCEEDED";

    case X509_V_ERR_INVALID_PURPOSE:
      return "INVALID_PURPOSE";

    case X509_V_ERR_CERT_UNTRUSTED:
      return "CERT_UNTRUSTED";

    case X509_V_ERR_CERT_REJECTED:
      return "CERT_REJECTED";

    case X509_V_ERR_UNABLE_TO_GET_CRL:
      return "UNABLE_TO_GET_CRL";

    case X509_V_ERR_CRL_NOT_YET_VALID:
      return "CRL_NOT_YET_VALID";

    case X509_V_ERR_CRL_HAS_EXPIRED:
      return "CRL_HAS_EXPIRED";
  }

  return "Unknown verify error";
}
