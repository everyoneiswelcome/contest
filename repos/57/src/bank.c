#include "defs.h"

static int do_getopt(int argc, char *argv[], int *p_port, char **p_authFile);
static int createSelfSignedCert(const char *authFile, X509 **p_x509, EVP_PKEY **p_pkey);
static int parseJSON(char *JSONstring, int *p_cardDetails, char *p_acntName, int *p_action, double *pamount);
static int processRequest(char *buffer, char *authFile, char **p_responseToSend, int *p_nbytes);

#ifdef ATM_THREADS
void freeThreadSpecificData(void *value) {
  return;
}
void *processConnection(void *_args);
#else
int processConnection(void *_args);
#endif

pthread_mutex_t mut;
pthread_key_t portKey = 0;

int client_port = 0;

struct thread_args {
  int *p_clnt_soc;
  char *p_authFile;
  SSL_CTX *ctx;
  int signal_fd;
  int *p_isExitFlag;
  int client_port;
  X509 *x509;
  EVP_PKEY *pkey;
};

int main(int argc, char *argv[]) {
  int ret = OK, port = 3000, srvr_soc = 0, clnt_soc = 0;
  int ssl_thread_setup_flag = 0;

  socklen_t soc_len;
  struct sockaddr_in clnt_addr;
  struct linger so_linger;

  char *authFile = DEFAULT_AUTH;

  X509 *x509 = NULL;
  EVP_PKEY *pkey = NULL;
  SSL_CTX *ctx = NULL;

  int signal_fd = 0;
  sigset_t mask;
  int isExitFlag = 0;

  pthread_t tid;
  struct thread_args *st_thread_args;

  LOG_ENTRY();

#ifdef ATM_THREADS
  pthread_mutex_init(&mut, NULL);
  ret = pthread_key_create(&portKey, freeThreadSpecificData);
#endif
  if(ret != 0) {
    LOG("pthread_key_create() failed");
    ret = ERROR;
    goto end;
  }

  so_linger.l_onoff = 1;
  so_linger.l_linger = 0;

  ret = createSignalFD(&signal_fd, &mask);
  ERROR_CHECK(ret == ERROR, "createSignalFD() failed", ERROR, end);

  ret = do_getopt(argc, argv, &port, &authFile);
  ERROR_CHECK(ret == ERROR, "do_getopt() failed", ERROR, end);

#ifdef ATM_THREADS
  thread_setup();
  ssl_thread_setup_flag = 1;
#endif
  ret = createSelfSignedCert(authFile, &x509, &pkey);
  ERROR_CHECK(ret == ERROR, "createSelfSignedCert() failed", ERROR, end);
  printf("created\n");
  fflush(stdout);

  ret = tcpListen(LOCAL_ADDR, port, &srvr_soc);
  ERROR_CHECK(ret == ERROR, "tcpListen() failed", PROTOCOL_ERROR, end);

  ret = checkForSignal(signal_fd, &isExitFlag);
  ERROR_CHECK(ret == ERROR, "checkSignal() failed", PROTOCOL_ERROR, end);
  ERROR_CHECK(isExitFlag, "SIGTERM", ERROR, end);

  fcntl(srvr_soc, F_SETFL, O_NONBLOCK);

  while(1) {
    ret = tcpAccept(&clnt_soc, srvr_soc, &clnt_addr, signal_fd, &isExitFlag);
    ERROR_CHECK(isExitFlag, "Exit flag set", ERROR, clnt_end);
    ERROR_CHECK(ret == ERROR, "tcpAccept() failed", PROTOCOL_ERROR, clnt_end);

    st_thread_args = (struct thread_args *) malloc(sizeof(*st_thread_args));
    st_thread_args->p_clnt_soc = &clnt_soc;
    st_thread_args->p_authFile = authFile;
    st_thread_args->ctx = ctx;
    st_thread_args->x509 = x509;
    st_thread_args->pkey = pkey;
    st_thread_args->signal_fd = signal_fd;
    st_thread_args->p_isExitFlag = &isExitFlag;
    st_thread_args->client_port = ntohs(clnt_addr.sin_port);
#ifdef ATM_THREADS
    ret = pthread_create(&tid, NULL, processConnection, st_thread_args);
    ERROR_CHECK(ret != 0, "pthread_create() failed", ERROR, clnt_end);

    continue;

    // Sequential access with threads i.e, waits for thread to exit before proceeeding with the next request
    //ret = pthread_join(tid, NULL);
    //ERROR_CHECK(ret != 0, "pthread_join() failed", ERROR, clnt_end);
#else
    ret = processConnection((void *) st_thread_args);
    ERROR_CHECK(ret != OK, "processConnection() failed", ERROR, clnt_end);
#endif


clnt_end:
    if(ret == PROTOCOL_ERROR) {
      printf("protocol_error\n");
      fflush(stdout);
      port = setsockopt(clnt_soc, SOL_SOCKET, SO_LINGER, &so_linger, sizeof so_linger);
      LOG("setsockopt() returned %d, %s", port, (port)?"FAILED":"SUCCESS");
    }
    if(isExitFlag) {
      if(clnt_soc) {
        port = setsockopt(clnt_soc, SOL_SOCKET, SO_LINGER, &so_linger, sizeof so_linger);
        LOG("setsockopt() returned %d, %s", port, (port)?"FAILED":"SUCCESS");
        close(clnt_soc);
      }
      break;
    }
    if(clnt_soc) close(clnt_soc);
    clnt_soc = 0;
    ret = OK;
  }

end:
#ifdef ATM_THREADS
  if(ssl_thread_setup_flag) thread_cleanup();
  pthread_key_delete(portKey);
#endif
  if(signal_fd) close(signal_fd);
  if(x509) X509_free(x509);
  if(pkey) EVP_PKEY_free(pkey);
  if(ctx) SSL_CTX_free(ctx);
  if(srvr_soc) {
    //nbytes = setsockopt(srvr_soc, SOL_SOCKET, SO_LINGER, &so_linger, sizeof so_linger);
    //LOG("setsockopt() returned %d, %s", nbytes, (nbytes)?"FAILED":"SUCCESS");
    port = fcntl(srvr_soc, F_GETFL);
    fcntl(srvr_soc, F_SETFL, port & (~O_NONBLOCK));
    close(srvr_soc);
  }
  if(ret == PROTOCOL_ERROR) {
    printf("protocol_error\n");
    fflush(stdout);
  }
  ERR_free_strings();
  LOG_EXIT();
  return ret;
}

#ifdef ATM_THREADS
void *processConnection(void *_args)
#else
int processConnection(void *_args)
#endif
{
  struct thread_args *args = (struct thread_args *)_args;
  char *authFile    = args->p_authFile;
  SSL_CTX *ctx      = args->ctx;
  X509 *x509        = args->x509;
  EVP_PKEY *pkey    = args->pkey;
  int signal_fd     = args->signal_fd;
  int *p_isExitFlag = args->p_isExitFlag;
  int *p_clnt_soc   = args->p_clnt_soc;

  int ret = OK, clnt_soc;
  char buffer[1024];
  int nbytes = 0;
  struct linger so_linger;
  char *responseToSend = NULL;
  SSL *ssl = NULL;

#ifdef ATM_THREADS
  pthread_setspecific(portKey, (void *)args->client_port);
  LOG_ERROR("Thread starting, %d", (unsigned int)pthread_self());
#else
  client_port = args->client_port;
#endif

  clnt_soc = *p_clnt_soc;
  so_linger.l_onoff = 1;
  so_linger.l_linger = 0;

  fcntl(clnt_soc, F_SETFL, O_NONBLOCK);

  ret = sslServerInitializeContext(&ctx, x509, pkey);
  ERROR_CHECK(ret == ERROR, "sslServerInitializeContext() failed", ERROR, end);

  ret = sslAccept(clnt_soc, ctx, &ssl, signal_fd, p_isExitFlag);
  ERROR_CHECK(*p_isExitFlag, "Exit flag set", ERROR, clnt_end);
  ERROR_CHECK(ret == ERROR, "sslAccept() failed", PROTOCOL_ERROR, clnt_end);

  do{
    nbytes = SSL_read(ssl, buffer, 1024);
    if(nbytes <=0 && isRetryNeeded(clnt_soc, ssl, nbytes, signal_fd, p_isExitFlag) != OK) {
      ERROR_CHECK(*p_isExitFlag, "Exit flag set", ERROR, clnt_end);
      break;
    }
    ERROR_CHECK(*p_isExitFlag, "Exit flag set", ERROR, clnt_end);
  } while(nbytes <= 0);
  ERROR_CHECK(nbytes <= 0, "SSL_read() failed", PROTOCOL_ERROR, clnt_end);
  buffer[nbytes] = '\0';
  LOG("JSONstring\n%s",buffer);

  ret = processRequest(buffer, authFile, &responseToSend, &nbytes);
  ERROR_CHECK(ret == ERROR, "processRequest() failed", ERROR, clnt_end);

  do{
    nbytes = SSL_write(ssl, responseToSend, nbytes);
    if(nbytes <=0 && isRetryNeeded(clnt_soc, ssl, nbytes, signal_fd, p_isExitFlag) != OK) {
      ERROR_CHECK(*p_isExitFlag, "Exit flag set", ERROR, clnt_end);
      break;
    }
    ERROR_CHECK(*p_isExitFlag, "Exit flag set", ERROR, clnt_end);
  } while(nbytes <= 0);
  ERROR_CHECK(nbytes <= 0, "SSL_write() failed", PROTOCOL_ERROR, clnt_end);

clnt_end:
  if(ret == PROTOCOL_ERROR) {
    printf("protocol_error\n");
    fflush(stdout);
    nbytes = setsockopt(clnt_soc, SOL_SOCKET, SO_LINGER, &so_linger, sizeof so_linger);
    LOG("setsockopt() returned %d, %s", nbytes, (nbytes)?"FAILED":"SUCCESS");
    ret = ERROR;
  }
  //FREE_STRING(responseToSend);
  if(ssl) {
    SSL_shutdown(ssl);
    SSL_free(ssl);
  }
  if(*p_isExitFlag && clnt_soc) {
    nbytes = setsockopt(clnt_soc, SOL_SOCKET, SO_LINGER, &so_linger, sizeof so_linger);
    LOG("setsockopt() returned %d, %s", nbytes, (nbytes)?"FAILED":"SUCCESS");
  }
  close(clnt_soc);
  *p_clnt_soc = 0;

  free(args);
end:
  if(ctx) SSL_CTX_free(ctx);
#ifdef ATM_THREADS
  LOG("Exiting thread 0x%ux\n", (unsigned int)pthread_self());
  LOG_ERROR("Thread exiting, %d\n", (unsigned int)pthread_self());
  pthread_setspecific(portKey, NULL);
  pthread_exit(0);
#else
  return ret;
#endif
}

static int do_getopt(int argc, char *argv[], int *p_port, char **p_authFile) {
  int ret = OK, c;
  LOG_ENTRY();

  while((c = getopt(argc, argv, "p:s:")) != -1) {
    switch(c) {
      case 'p':
        ret = isNumericValid(optarg, 0);
        ERROR_CHECK(ret == ERROR, "isNumericValid() failed", ERROR, end);
        *p_port = atoi(optarg);
        break;
      case 's':
        *p_authFile = optarg;
        break;
      case '?':
        LOG_ERROR("Unknown option -%c",optopt);
        ret = ERROR;
        break;
    }
  }

  ret = isFileNameValid(*p_authFile);
  ERROR_CHECK(ret == ERROR, "isFileNameValid() failed", ERROR, end);

  LOG("port = %d, auth file=%s", *p_port, *p_authFile);
  LOG_EXIT();

end:
  return ret;
}

static int createSelfSignedCert(const char *authFile, X509 **p_x509, EVP_PKEY **p_pkey) {
  int ret = OK;
  FILE *fp=NULL;

  X509 *x509=NULL;
  EVP_PKEY *pkey=NULL;
  PKCS12 *p12 = NULL;

#if 1
  // autFile shouln't exist
  fp = fopen(authFile, "r");
  if(fp) {
    LOG_ERROR("%s exists",authFile);
    ret = ERROR;
    goto end;
  }
#endif

  fp = fopen(authFile, "w");
  if(!fp) {
    LOG_ERROR("%s unable to create, %d",authFile, errno);
    ret = ERROR;
    goto end;
  }

  ret = mkcert(&x509,&pkey,1024,0,365);
  ERROR_CHECK(ret == ERROR, "mkcert() failed", ERROR, end);
#if 0
  PEM_write_X509(fp,x509);
#else
  SSL_library_init();
  OpenSSL_add_all_algorithms();
  SSL_load_error_strings();

  p12 = PKCS12_create(DEFAULT_AUTH, "ATM-PROTOCOL", pkey, x509, NULL,0,0,0,0,0);
  //LOG("error %s\n", ERR_error_string(ERR_get_error(), NULL));
  ERROR_CHECK(!p12, "PKCS12_create() failed", ERROR, end);

  ret = i2d_PKCS12_fp(fp, p12);
  ERROR_CHECK(ret <= 0, "i2d_PKCS12() failed", ERROR, end);

  ret = OK;
#endif

  *p_pkey = pkey;
  *p_x509 = x509;

end:
  if(fp) fclose(fp);
  if(p12) PKCS12_free(p12);
  return ret;
}

static int parseJSON(char *JSONstring, int *p_cardDetails, char *p_acntName, int *p_action, double *p_amount) {
  int ret = OK;
  cJSON *json = NULL;

  json = cJSON_Parse(JSONstring);
  ERROR_CHECK(!json, "cJSON_Parse() failed", ERROR, end);

  *p_cardDetails = cJSON_GetObjectItem(json, CARD_TAG)->valueint;
  // TODO: possible overflow in snprintf() on p_acntName when isAccountNameValid() on client(atm.c:do_optget()) is bypassed
  snprintf(p_acntName, MAX_ACCOUNT_NAME+1, "%s", cJSON_GetObjectItem(json, ACCOUNT_TAG)->valuestring);
  *p_action = cJSON_GetObjectItem(json, ACTION_TAG)->valueint;
  *p_amount = cJSON_GetObjectItem(json, AMOUNT_TAG)->valuedouble;

  LOG("Arguments:\naccount name\t%s\ncard details\t%d\naction\t%d\namount\t%f", p_acntName, *p_cardDetails, *p_action, *p_amount);

end:
  if(json) cJSON_Delete(json);
  return ret;
}

static int performAction(int action, int cardDetails, char *acntName, double *p_amount, int *p_key) {
  int ret = OK;
  double amount;

  NodeType *node;
  ValType val;
  KeyType key;

  strNodeType *strNode;
  strValType strVal;
  strKeyType strKey;

  amount = *p_amount;

#ifdef ATM_THREADS
  pthread_mutex_lock(&mut);
#endif

  if(action == CREATE) {
    ERROR_CHECK(amount < INITIAL_AMOUNT, "initial ammount cannot be less than INITIAL_AMOUNT", ERROR, end);
    strcpy(strKey.name, acntName);
    strNode = strrbtFind(strKey);
    ERROR_CHECK(strNode, "Account already exists", ERROR, end);

    strcpy(val.name, acntName);
    val.amount = amount;
    while(1) {
      key = rand();
      ret = rbtInsert(key, val, &node);
      if(ret != RBT_STATUS_OK)
        continue;
      else {
        strVal.node = node;
        ret = strrbtInsert(strKey, strVal, &strNode);
        ERROR_CHECK(ret != RBT_STATUS_OK, "strrbtInsert() failed", ERROR, end);
        break;
      }
    }
    *p_key = key;
  } else {
    node = rbtFind(cardDetails);
    ERROR_CHECK(!node, "rbtFind() failed; unable to find key", ERROR, end);

    ret = strcmp(acntName, node->val.name);
    ERROR_CHECK(ret != 0, "Names not same", ERROR, end);

    if(action == DEPOSIT) {
      node->val.amount += amount;
    } else if(action == WITHDRAW) {
      ERROR_CHECK(node->val.amount - amount <= -0.01, "Cannot withdraw more than available amount", ERROR, end);
      node->val.amount -= amount;
    } else {
      *p_amount = node->val.amount;
    }
  }

end:
#ifdef ATM_THREADS
  pthread_mutex_unlock(&mut);
#endif
  return ret;
}

// TODO: what happens if the request has garbage characters
static int processRequest(char *buffer, char *authFile, char **p_responseToSend, int *p_nbytes) {
  int ret = OK, nbytes = OK, key = 0;

  int cardDetails = -1;
  char acntName[MAX_ACCOUNT_NAME+1];
  int action = NONE;
  double amount = -1;

  char *responseToPrint = NULL;
  char *responseToPrint2 = NULL;
  int responseToPrintMaxLen = sizeof(char)*(MAX_FILE_NAME + MAX_INTEGER_STORAGE + MAX_DOUBLE_STORAGE + 100);
  char *responseToSend = NULL;

  acntName[MAX_ACCOUNT_NAME] = '\0';

#if 0
  ret = parseJSON(buffer, &cardDetails, acntName, &action, &amount);
#else
  ret = parseRequest(buffer, &cardDetails, acntName, &action, &amount);
#endif
  ERROR_CHECK(ret == ERROR, "parseJSON() failed", ERROR, send_err);

  ret = performAction(action, cardDetails, acntName, &amount, &key);
  ERROR_CHECK(ret == ERROR, "performAction() failed", ERROR, send_err);

#if 0
  if(ret == OK) {
    responseToPrint = (char *)malloc(responseToPrintMaxLen);
    nbytes = snprintf(responseToPrint, responseToPrintMaxLen, RESPONSE_TO_PRINT_FORMAT, responseNames[NONE], acntName, responseNames[action], amount);
    ERROR_CHECK(nbytes < 0, "snprintf() failed", ERROR, end);
    responseToPrint2 = (char *)malloc(responseToPrintMaxLen);
    nbytes = snprintf(responseToPrint2, responseToPrintMaxLen, RESPONSE_TO_PRINT2_FORMAT, responseNames[NONE], acntName, responseNames[action], amount);
    ERROR_CHECK(nbytes < 0, "snprintf() failed", ERROR, end);

    if(action != CREATE) {
      responseToSend = (char *)malloc(responseToPrintMaxLen+100);
      nbytes = snprintf(responseToSend, responseToPrintMaxLen+100, RESPONSE_TO_SEND_FORMAT, STATUS_TAG, OK, RESPONSE_TAG, responseToPrint);
      ERROR_CHECK(nbytes < 0, "snprintf() failed", ERROR, end);
    } else {
      responseToSend = (char *)malloc(responseToPrintMaxLen+100);
      nbytes = snprintf(responseToSend, responseToPrintMaxLen+100, RESPONSE_CREATE_TO_SEND_FORMAT, STATUS_TAG, OK, RESPONSE_TAG, responseToPrint, CARD_TAG, key);
      ERROR_CHECK(nbytes < 0, "snprintf() failed", ERROR, end);
    }

    printf("%s",responseToPrint2);
    fflush(stdout);
  } else {
send_err:
    responseToSend = (char *)malloc(responseToPrintMaxLen+100);
    nbytes = snprintf(responseToSend, responseToPrintMaxLen+100, RESPONSE_TO_SEND_FORMAT, STATUS_TAG, ERROR, RESPONSE_TAG, "");
    ERROR_CHECK(nbytes < 0, "snprintf() failed", ERROR, end);
    ret = OK;
  }
#else
  printf(RESPONSE_TO_PRINT2_FORMAT, responseNames[NONE], acntName, responseNames[action], amount);
  fflush(stdout);
send_err:
  createResponse(&responseToSend, ret, amount, key);
  ret = OK;
#endif

end:
  *p_nbytes = strlen(responseToSend);
  *p_responseToSend = responseToSend;
  FREE_STRING(responseToPrint);
  FREE_STRING(responseToPrint2);
  return ret;
}
