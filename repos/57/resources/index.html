<html><head>
<meta http-equiv="content-type" content="text/html; charset=windows-1252"></head><body><h1>Programming Problem: ATM Protocol</h1>
<h2>Summary</h2>
<p>Players will implement an ATM communication protocol.
 There will be two programs.
 One program, called <code>atm</code>, will allow bank customers to withdraw and
deposit money from their account.
 The other program, called <code>bank</code>, will run as a server that keeps
track of customer balances.</p>
<h2>Security Model</h2>
<p><code>atm</code> and <code>bank</code> must be implemented such that only a customer with a
correct <em>card file</em> can learn or modify the balance of their account,
and only in an appropriate way (e.g., they may not withdraw more money
than they have).
 In addition, an <code>atm</code> may only communicate with a <code>bank</code> if it and
the bank agree on an <em>auth file</em>, which they use to mutually
authenticate. 
The <em>auth file</em> will be shared between the <code>bank</code> and <code>atm</code> via a 
trusted channel unavailable to the attacker, and is used to set up
secure communications.</p>
<p>Since the ATM client is communicating with the bank server over the
network, it is possible that there is a "man in the middle" that can
observe and change the messages, or insert new messages.
 A "man in the middle" attacker can view all traffic transmitted 
between the <code>atm</code> and the <code>bank</code>. 
The "man in the middle" may send messages to either the atm
or the bank. </p>
<p>The source code for <code>atm</code> and <code>bank</code> will be available to attackers,
but not the auth file. The card file may be available in some cases,
depending on the kind of attack.</p>
<h2>Requirements</h2>
<p>The specification details for each program are linked below.</p>
<ul>
<li><a href="https://coursera.builditbreakit.org/static/doc/fall2015coursera/spec/bank.html">Bank Server</a></li>
<li><a href="https://coursera.builditbreakit.org/static/doc/fall2015coursera/spec/atm.html">ATM Client</a></li>
</ul>
<p>Here are some general requirements that apply to both <code>atm</code> and <code>bank</code>
programs.</p>
<h3>Valid Inputs</h3>
<p>Any command-line input that is not valid according to the rules below
should result with a return value of 255 from the invoked program and
nothing should be output to <strong><em>stdout</em></strong> .</p>
<ul>
<li>
<p>Command line arguments must be <a href="http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap12.html">POSIX
  compliant</a>
  and each argument cannot exceed 4096 characters (with additional
  restrictions below).
  In particular, this allows command arguments specified
  as "-i 4000" to be provided without the space as "-i4000" or with
  extra spaces as in "-i &nbsp;&nbsp;&nbsp; 4000".
  Arguments may appear in any order. You should not implement <code>--</code>, 
  which is optional for POSIX compliance.</p>
</li>
<li>
<p>Numeric inputs are positive and provided in decimal without any
 leading 0's (should match <strong>/(0|[1-9][0-9]*)/</strong>).
 Thus "42" is a valid input number but the octal "052" or hexadecimal
  "0x2a" are not.
 Any reference to "<strong>number</strong>" below refers to this input specification.</p>
</li>
<li>
<p>Balances and currency amounts are specified as a <strong>number</strong> indicating a
  whole amount and a fractional input separated by a period.
  The fractional input is in decimal and is always two digits and thus
  <em>can include a leading 0</em> (should match <strong>/[0-9]{2}/</strong>).
 The interpretation of the fractional amount <em>v</em> is that of having
  value equal to <em>v</em>/100 of a whole amount (akin to cents and dollars
  in US currency). Balances are bounded from 0.00 to 4294967295.99. 
<!---  The whole part should be representable as a 32-bit unsigned integer
  (0 though 4,294,967,295).---></p>
</li>
</ul>
<!---
- Command-line integer inputs are non-negative, should be storable in
  an unsigned 32 bit integer, and may have leading zeros [pm: this is
  bad as a leading zero usually means octal, can we get rid of this
  possibility?]. The length of an integer on the command line is
  between 1 and 4096 characters. **/[0-9]{1,4096}/**
  --->

<ul>
<li>
<p>File names are restricted to underscores, hyphens, dots, digits,
  and lowercase alphabetical characters (each character should match
  <strong>/[_\-\.0-9a-z]/</strong>).
  File names are to be between 1 and 255 characters long. The special
  file names "." and ".." are not allowed.</p>
</li>
<li>
<p>Account names are restricted to same characters as file names but
  they cannot exceed 250 characters of length, and "." and ".." are
  valid account names. </p>
</li>
<li>
<p>IP addresses are restricted to IPv4 32-bit addresses and are
  provided on the command line in dotted decimal notation, i.e., four
  <strong>numbers</strong> between 0 and 255 separated by periods.</p>
</li>
<li>
<p>Ports are specified as <strong>numbers</strong> between 1024 and 65535
  inclusively.</p>
</li>
</ul>
<h3>Outputs</h3>
<ul>
<li>
<p>Anything printed to <strong>stderr</strong> will be ignored (e.g., so detailed error
  messages could be printed there, if desired).</p>
</li>
<li>
<p>All JSON output is printed on a single line and is followed by a
  newline.</p>
</li>
<li>
<p>JSON numbers require full precision. </p>
</li>
<li>
<p>Newlines are '\n' -- the ASCII character with code decimal 10.</p>
</li>
<li>
<p>Both programs should <strong>explicitly flush stdout</strong> after every line
  printed.</p>
</li>
<li>
<p>Successful exits should return exit code 0.</p>
</li>
</ul>
<h3>Errors</h3>
<h4>Protocol Error</h4>
<ul>
<li>
<p>If an error is detected in the protocol's communication, <code>atm</code>
  should exit with return code <code>63</code>, while <code>bank</code> should print
  "<code>protocol_error</code>" to stdout (followed by a newline) and roll back
  (i.e., undo any changes made by) the current transaction.</p>
</li>
<li>
<p>A timeout occurs if the other program does not respond within 10
  seconds.
  If the atm observes the timeout, it should exit with return code
  <code>63</code>, while if the bank observes it, it should print
  "<code>protocol_error</code>" to stdout (followed by a newline) and rollback
  the current transaction.
  The non-observing party need not do anything in particular.</p>
</li>
</ul>
<h4>Other Errors</h4>
<ul>
<li>All other errors, specified throughout this document or
  unrecoverable errors not explicitly discussed, should prompt the
  program to exit with return code <code>255</code>.</li>
</ul>
<h2>Oracle</h2>
<p>We provide an <em>oracle</em>, or reference implementation. This
implementation will be use to adjudicate correctness-related error
reports submitted during the Break-it round -- in particular, if a
submitted test fails on a particular submission, it must not fail on
the oracle. (Bugs in the oracle are discussed below.)</p>
<p>Contestants may query the oracle during the build-it round to clarify
the expected behavior of the <code>atm</code> and <code>bank</code> programs. Queries are
specified as a sequence of <code>atm</code> commands, submitted via the "Oracle
submissions" link on the team participation page of the contest site.
Here is an example query (this will make more sense if you read the
specifications for the <code>atm</code> and <code>bank</code> programs first):</p>
<pre><code>[ 
    {"input":["-p", "%PORT%", "-i", "%IP%", "-a", "ted", "-n", "10.30"],"base64":false}, 
    {"input":["-p", "%PORT%", "-i", "%IP%", "-a", "ted", "-d", "5.00"]}, 
    {"input":["-p", "%PORT%", "-i", "%IP%", "-a", "ted", "-g"]},
    {"input":["LXA=", "JVBPUlQl", "LWk=", "JUlQJQ==", "LWE=", "dGVk", "LWc="],"base64":true} 
]
</code></pre>
<p>The oracle will provide outputs from the atm and bank. Here is an example for the previous query:</p>
<pre><code>[
    {
        "bank": {
            "output": {"initial_balance": 10.3,"account": "ted"}
        },
        "atm": {
            "exit": 0,
            "output": {"initial_balance": 10.3,"account": "ted"}
        }
    },
    {
        "bank": {
            "output": {"deposit": 5,"account": "ted"
            }
        },
        "atm": {
            "exit": 0,
            "output": {"deposit": 5,"account": "ted"}
        }
    },
    {
        "bank": {
            "output": {"balance": 15.3,"account": "ted"}
        },
        "atm": {
            "exit": 0,
            "output": {"balance": 15.3,"account": "ted"}
        }
    },
    {
        "bank": {
            "output": {"balance": 15.3,"account": "ted"}
        },
        "atm": {
            "exit": 0,
            "output": {"balance": 15.3,"account": "ted"}
        }
    }
]
</code></pre>
<p>In general, a test must begin with account creation, so that the atm
program creates a card file, which can be used on subsequent
interactions. Though we have not shown it, you can also pass the
cardfile explicitly during a test, i.e., using the <code>-c</code> option.</p>
<p>The <code>bank</code> will run at a dynamically chosen IP address, listening on a
dynamically chosen port. Since this information cannot be known when
writing the query, you instead specify these values using the
following variables:</p>
<ul>
<li><code>%IP%</code> - IP address of the <code>bank</code> server</li>
<li><code>%PORT%</code> - TCP port of the <code>bank</code> server</li>
</ul>
<p>Each "input" is the argument list passed to <code>atm</code>. 
Queries may specify inputs using base64-encoded values by by providing
the optional boolean field <code>"base64"</code>. </p>
<h2>Changes and Oracle Updates</h2>
<p>There will inevitably be changes to the specification and oracle
implementation during the contest as unclear assumptions and mistakes
on our part are uncovered. We apologize in advance!</p>
<p>All changes will be summarized on this page. Breaker teams will
receive (<a href="">M/2</a>) points for identifying bugs in the oracle implementation. 
Points will only be awarded to the first
breaker team that reports the bug. To report a bug in the oracle
implementation, email us at  with a description of the bug, links to relevant oracle submissions, your team
name, and team id. 
All changes will be summarized at the top of this page. Breaker teams will
receive (<a href="https://coursera.builditbreakit.org/details#scoring">M/2</a>) points for identifying bugs in the oracle implementation. 
Points will only be awarded to the first
breaker team that reports a particular bug. To report a bug in the oracle
implementation, email us at
<a href="mailto:coursera@builditbreakit.org">coursera@builditbreakit.org</a> with a
description of the bug, links to relevant oracle submissions on the
contest website, your team name, and team id. </p>
<h2>Build-it Round Submission</h2>
<p>Each build-it team should
initialize a git repository on either <a href="https://github.com/">github</a> or
<a href="https://bitbucket.org/">bitbucket</a> or <a href="https://gitlab.com/">gitlab</a> and share it 
with the <code>bibifi</code> user on those services. Create a directory 
named <code>build</code> in the top-level directory of this repository and commit your code into that folder. Your 
submission will be scored after every push to the repository. (Beware making your
repository public, or other contestants might be able to see it!)</p>
<p>To score a submission, an automated system will first invoke <code>make</code> in the <code>build</code>
directory of your submission. The only requirement on <code>make</code> is that it 
must function without internet connectivity, and that it must return within 
ten minutes. Moreover, it must be the case that your software is
actually built, through initiation of make, from source (not including
libraries you might use). Submitting binaries (only) is not acceptable.</p>
<p>Once make finishes, <code>atm</code> and <code>bank</code> should be executable 
files within the <code>build</code> directory. An automated system will invoke them with a 
variety of options and measure their responses. 
The executables must be able to be run from any working directory. 
If your executables are bash scripts, you may find the following <a href="http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in">resource</a> helpful. </p>
<h2>Examples</h2>
<p>Here is an example of how to use <code>atm</code> and <code>bank</code>. 
First, do some setup and run <code>bank</code>.</p>
<pre><code>$ mkdir bankdir; mv bank bankdir/; cd bankdir/; ./bank -s bank.auth &amp;; cd ..
created
</code></pre>
<p>Now set up the atm. </p>
<pre><code>$ mkdir atmdir; cp bankdir/bank.auth atmdir/; mv atm atmdir/; cd atmdir
</code></pre>
<p>Create an account 'bob' with balance $1000.00 (There are two outputs because one is from the <code>bank</code> which is running in the same shell).</p>
<pre><code>$ ./atm -s bank.auth -c bob.card -a bob -n 1000.00
{"account":"bob","initial_balance":1000}
{"account":"bob","initial_balance":1000}
</code></pre>
<p>Deposit $100.</p>
<pre><code>$ ./atm -c bob.card -a bob -d 100.00
{"account":"bob","deposit":100}
{"account":"bob","deposit":100}
</code></pre>
<p>Withdraw $63.10.</p>
<pre><code>$ ./atm -c bob.card -a bob -w 63.10
{"account":"bob","withdraw":63.1}
{"account":"bob","withdraw":63.1}
</code></pre>
<p>Attempt to withdraw $2000, which fails since 'bob' does not have a sufficient balance.</p>
<pre><code>$ ./atm -c bob.card -a bob -w 2000.00
$ echo $?
255
</code></pre>
<p>Attempt to create another account 'bob', which fails since the account 'bob' already exists.</p>
<pre><code>$ ./atm -a bob -n 2000.00
$ echo $?
255
</code></pre>
<p>Create an account 'alice' with balance $1500.</p>
<pre><code>$ ./atm -a alice -n 1500.00
{"account":"alice","initial_balance":1500}
{"account":"alice","initial_balance":1500}
</code></pre>
<p>Bob attempts to access alice's balance with his card, which fails.</p>
<pre><code>$ ./atm -a alice -c bob.card -g
$ echo $?
255
</code></pre></body></html>