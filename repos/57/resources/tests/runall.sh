#!/bin/bash

find . -name "*.json" -exec bash -c './run_test.py {}' \; > log.txt

tests_run=`grep "test file" log.txt | wc -l`
tests_passed=`grep "test passed" log.txt | wc -l`

if [ $tests_run -eq $tests_passed ]; then
  echo "All tests passed"
else
  echo "Tests failed, please check the log"
fi
