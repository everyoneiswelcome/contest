#!/bin/bash
apt-add-repository ppa:staticfloat/juliareleases 
apt-get update
apt-get install julia
apt-get install python-gevent
apt-get install python3-pip
pip3 install simplejson
apt-get install clang
