#!/bin/bash
apt-get update
apt-get dist-upgrade
apt-get install python3-requests libsodium-dev python-virtualenv python-pytest python-cryptography python-pycrypto python3-openssl build-essential gnupg elixir
pip install pynacl
pip install pyopenssl
pip install pyzmq
