#!/bin/bash
apt-get update
apt-get dist-upgrade
which rustc || curl -sSf https://static.rust-lang.org/rustup.sh | sh -s -- -y
apt-get install ruby
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -sSL https://get.rvm.io | bash -s stable --ruby=jruby --gems=rails,puma
apt-get install erlang
apt-get install rebar
apt-get install python-protobuf libprotobuf-c0-dev libprotobuf-java libprotobuf-dev libprotoc-dev protobuf-compiler
apt-get install libssl-dev
apt-get build-dep firefox thunderbird
