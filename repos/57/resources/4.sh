#!/bin/bash
apt-get update
apt-get dist-upgrade
apt-get install golang-go cmake libssl-dev zlib1g-dev libcppunit-dev llvm python-demjson python3-crypto python3-cryptography python-zmq libzmq-dev

wget https://red.libssh.org/attachments/download/177/libssh-0.7.2.tar.xz
tar -Jxpf libssh-0.7.2.tar.xz
mkdir libssh-0.7.2/build
cd libssh-0.7.2/build
cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Release ..
make

make install
cd

apt-get install python-pycrypto
pip install cryptography
