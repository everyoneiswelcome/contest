import java.io.*;
import java.util.Random;


public class CommonUtils {

    static class RC4 {
        private final byte[] S = new byte[256];
        private final byte[] T = new byte[256];
        private final int keylen;

        public RC4(final byte[] key) {
            if (key.length < 1 || key.length > 256) {
                throw new IllegalArgumentException(
                        "key must be between 1 and 256 bytes");
            } else {
                keylen = key.length;
                for (int i = 0; i < 256; i++) {
                    S[i] = (byte) i;
                    T[i] = key[i % keylen];
                }
                int j = 0;
                for (int i = 0; i < 256; i++) {
                    j = (j + S[i] + T[i]) & 0xFF;
                    byte temp = S[i];
                    S[i] = S[j];
                    S[j] = temp;
                }
            }
        }

        public byte[] encrypt(final byte[] plaintext) {
            final byte[] ciphertext = new byte[plaintext.length];
            int i = 0, j = 0, k, t;
            for (int counter = 0; counter < plaintext.length; counter++) {
                i = (i + 1) & 0xFF;
                j = (j + S[i]) & 0xFF;
                byte temp = S[i];
                S[i] = S[j];
                S[j] = temp;
                t = (S[i] + S[j]) & 0xFF;
                k = S[t];
                ciphertext[counter] = (byte) (plaintext[counter] ^ k);
            }
            return ciphertext;
        }

        public byte[] decrypt(final byte[] ciphertext) {
            return encrypt(ciphertext);
        }
    }

    public static boolean validIP(String ip) {
        try {
            if (ip == null || ip.isEmpty()) {
                return false;
            }

            String[] parts = ip.split("\\.");
            if (parts.length != 4) {
                return false;
            }

            for (String s : parts) {
                if (s.length() > 3)
                    return false;
                int i = Integer.parseInt(s);
                if ((i < 0) || (i > 255) || !(s.equals(Integer.toString(i)))) {
                    return false;
                }
            }
            if (ip.endsWith(".")) {
                return false;
            }

            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public static String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
        return sb.toString();
    }

    public static void writeFile(String fileName, String data) throws IOException {
        File file = new File(fileName);
        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(data);
        fileWriter.flush();
        fileWriter.close();
    }

    public static void deleteFile(String fileName){
        File file = new File(fileName);
        if (file.exists())
            file.delete();
    }

    public static String generateRandomText(int len){
        char[] text = new char[len];
        Random random = new Random();
        for (int i = 0; i < len; i++)
        {
            text[i] = (char) (36 + random.nextInt(91));
        }
        return new String(text);
    }

    public static void validName(String name, boolean is_account) {

        boolean result = true;

        if (!is_account) {
            if (name.equals(".") || name.equals(".."))
                    result = false;
            if (!(name.length() >= 1 && name.length() <= 255))
                System.exit(255);
        } else if (!(name.length() >= 1 && name.length() <= 250)) {
            System.exit(255);
        }

        for (int i = 0; i < name.length(); i++){
            char c = name.charAt(i);
            if(!((c >= 0x30 && c <= 0x39) || (c >= 0x61 && c <= 0x7a) || c == 0x2d || c == 0x2e || c == 0x5f)){
                System.exit(255);
            }
        }

        if (!result)
            System.exit(255);
    }

    public static byte[] encrypt(String toEncrypt, byte[] bkey) throws Exception {

        RC4 rc4 = new RC4(bkey);

        byte [] cipherText = rc4.encrypt(toEncrypt.getBytes("ASCII"));

        return cipherText;
    }

    public static String decrypt(byte[] toDecrypt, byte[] bkey) throws Exception {

        RC4 rc4 = new RC4(bkey);

        byte [] clearText = rc4.decrypt(toDecrypt);

        return new String(clearText, "ASCII");
    }
}