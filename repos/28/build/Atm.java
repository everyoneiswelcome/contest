import java.net.*;
import java.io.*;
import java.util.Random;

public class Atm {

    public static void main(String[] args) throws IOException {

        new AtmClient().init(args).communicate();

    }

}

class AtmClient {

    private String ipAddress = "127.0.0.1";
    private int portNumber = 3000;
    private String authFileName = "bank.auth";

    private byte[] secret = new byte[16];

    private String accountName;
    private String operation;
    private String cardFileName = "";
    private String cardString = null;
    private long value;


    public AtmClient init(String[] args){

        //reading commandline parameters
        boolean pcount = false;
        boolean scount = false;
        boolean icount = false;
        boolean ccount = false;
        boolean acount = false;
        boolean ocount = false;

        for (int i = 0; i < args.length; i++) {

            String arg = args[i];
            String argValue = "";

            if (arg.length() < 2 || arg.charAt(0) != '-')
                System.exit(255);
            if (!(arg.charAt(1) == 'g')) {
                if (arg.length() == 2) {
                    if (i + 1 >= args.length)
                        System.exit(255);
                    argValue = args[++i];
                }else argValue = arg.replace("-"+arg.charAt(1), "");
            }else if (arg.length() != 2)
                System.exit(255);
            if (arg.charAt(1) == 'p') {
                if (pcount)
                    System.exit(255);
                try {
                    pcount = true;
                    portNumber = Integer.parseInt(argValue);
                    if (portNumber < 1024 || portNumber > 65535 || !(argValue.equals(Integer.toString(portNumber))))
                        System.exit(255);
                } catch (NumberFormatException e) {
                    System.exit(255);
                }
            } else if (arg.charAt(1) == 's') {
                if (scount)
                    System.exit(255);
                scount = true;
                authFileName = argValue;
                CommonUtils.validName(authFileName, false);
            } else if (arg.charAt(1) == 'i') {
                if (icount)
                    System.exit(255);
                icount = true;
                ipAddress = argValue;
                if (!CommonUtils.validIP(ipAddress))
                    System.exit(255);
            } else if (arg.charAt(1) == 'c') {
                if (ccount)
                    System.exit(255);
                ccount = true;
                cardFileName = argValue;
                CommonUtils.validName(cardFileName, false);
            } else if (arg.charAt(1) == 'a') {
                if (acount)
                    System.exit(255);
                acount = true;
                accountName = argValue;
            } else if (arg.charAt(1) == 'n' || arg.charAt(1) == 'd' || arg.charAt(1) == 'w' || arg.charAt(1) == 'g') {
                if (ocount)
                    System.exit(255);
                ocount = true;
                operation = "" + arg.charAt(1);
                if (!(arg.charAt(1) == 'g')) {
                    int integerPlaces = argValue.indexOf('.');
                    if (integerPlaces > -1) {
                        int decimalPlaces = argValue.length() - integerPlaces - 1;
                        if (decimalPlaces != 2)
                            System.exit(255);
                        value = Long.parseLong(argValue.substring(0,integerPlaces)) * 100l + Integer.parseInt(argValue.substring(integerPlaces+1));
                    }else System.exit(255);
                    if (value <= 0 || value > 429496729599l)
                        System.exit(255);
                    if (argValue.charAt(0) == '0' && value > 1)
                        System.exit(255);
                }
            }else System.exit(255);
        }

        if (!acount || !ocount)
            System.exit(255);

        CommonUtils.validName(accountName, true);

        if (accountName.isEmpty())
            System.exit(255);

        File authFile = new File(authFileName);
        if (!authFile.exists() || authFile.isDirectory())
            System.exit(255);

        try {
            FileInputStream fis = new FileInputStream(authFileName);
            fis.read(secret, 0, 16);
            fis.close();
        }catch (Exception e){
            System.exit(255);
        }

        if (cardFileName.isEmpty())
            cardFileName = accountName + ".card";

        File cardFile = new File(cardFileName);

        if (operation.equals("n")) {
            if (cardFile.exists())
                System.exit(255);
            else {
                cardString = CommonUtils.generateRandomText(8);
            }
        } else if (!cardFile.exists() || cardFile.isDirectory())
            System.exit(255);
        else
            try {
                cardString = CommonUtils.readFile(cardFileName);
            } catch (IOException e) {
                System.exit(255);
            }

        return this;

    }

    public void communicate(){

        byte[] outputdata = new byte[0];
        try {
            outputdata = createBankQuerry();
        } catch (Exception e) {
            System.exit(255);
        }

        try (
                Socket socket = new Socket(ipAddress, portNumber);

                OutputStream out = socket.getOutputStream();
                DataOutputStream dos = new DataOutputStream(out);

                InputStream is = socket.getInputStream();
                DataInputStream dis = new DataInputStream(is);
        ) {
            socket.setSoTimeout(10000);

            if (operation.equals("n"))
                CommonUtils.writeFile(cardFileName, cardString);

            if (outputdata.length > 0) {
                dos.writeInt(outputdata.length);
                dos.write(outputdata, 0, outputdata.length);
            }else throw new Exception();

            byte[] inputdata = null;
            int len = dis.readInt();
            if (len > 0 && len < 1024) {
                inputdata = new byte[len];
                dis.readFully(inputdata);
            }else throw new Exception();

            String answer = CommonUtils.decrypt(inputdata, secret).trim();
            if (answer.contains("##error##")) {
                if (operation.equals("n"))
                    CommonUtils.deleteFile(cardFileName);
                System.exit(255);
            }

            socket.shutdownOutput();

            socket.close();

            System.out.println(answer);

        } catch (Exception e) {
            //Error while creating new acc: delete card file
            if (operation.equals("n"))
                CommonUtils.deleteFile(cardFileName);
            System.exit(63);
        }

    }

    private byte[] createBankQuerry() throws Exception{

        String outString = accountName + "#" + cardString + "#" + operation + "#" +Long.toString(value);

        Random random = new Random();
        int ml = outString.length();

        byte[] output = CommonUtils.encrypt(new String(new byte[random.nextInt(ml/2)]) + outString + new String(new byte[random.nextInt(ml/2)]), secret);

        return  output;

    }

}

