import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

public class Bank {

    public static void main(String[] args) {

        BankServer bankServer = new BankServer().init(args);

        bankServer.start();

    }

}

class BankServer{

    private int portNumber = 3000;
    private String authFileName = "bank.auth";

    private byte[] secret = new byte[16];

    private HashMap<String, Account> accounts = new HashMap<>();
    private HashSet<String> cards = new HashSet<>();

    public BankServer init(String[] args){

        //reading commandline parameters
        int pcount = 0;
        int scount = 0;
        for (int i = 0; i < args.length; i++) {
            String argValue = "";
            String arg = args[i];
            if (arg.length() < 2 || arg.charAt(0) != '-')
                System.exit(255);
            if (arg.length() == 2) {
                if (i + 1 >= args.length)
                    System.exit(255);
                argValue = args[++i];
            }else argValue = arg.replace("-"+arg.charAt(1), "");
            if (arg.charAt(1) == 'p') {
                if (++pcount > 1)
                    System.exit(255);
                try {
                    portNumber = Integer.parseInt(argValue);
                    if (portNumber < 1024 || portNumber > 65535 || !(argValue.equals(Integer.toString(portNumber))))
                        System.exit(255);
                } catch (NumberFormatException e) {
                    System.exit(255);
                }
            } else if (arg.charAt(1) == 's') {
                if (++scount > 1)
                    System.exit(255);
                authFileName = argValue;
                CommonUtils.validName(authFileName, false);
            } else System.exit(255);
        }

        File authFile = new File(authFileName);
        if (authFile.exists() || authFile.isDirectory())
            System.exit(255);
        try {
            if (authFile.getParentFile() != null)
                authFile.getParentFile().mkdirs();
            if (!authFile.createNewFile())
                System.exit(255);
        } catch (IOException e) {
            System.exit(255);
        }

        try {
            SecureRandom secureRandom = new SecureRandom();
            secureRandom.nextBytes(secret);
            FileOutputStream fos = new FileOutputStream(authFileName);
            fos.write(secret);
            fos.close();

            System.out.println("created");

        } catch (Exception e) {
            System.exit(255);
        }

        return this;
    }

    public void start(){

        try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
            while (true) new BankServerThread(serverSocket.accept()).start();
        } catch (IOException e) {
            System.exit(255);
        }
    }

    public static String longToStr(long val){
        int decels = (int) (val-(val/100)*100);
        return (val < 100 ? "0": val/100) + "." + (decels < 10 ? "0" : "") + decels;
    }

    class Transaction{
        private String operation;
        private String accountName;
        private String card;
        private long value;
        private String json = "##error##";
        private boolean valid = false;
        public Transaction(){

        }

        public String getJsonString() {
            return json;
        }

        public void setTransaction(String operation, String accountName, String card, long value) throws Exception{
            this.operation = operation;
            this.accountName = accountName;
            this.card = card;
            this.value = value;

            switch (operation){
                case "n":
                    if (!accounts.containsKey(accountName) && value >= 1000 && !cards.contains(card)){
                        accounts.put(accountName, new Account("", 0));
                        json = "{\"account\":\"" + accountName + "\",\"initial_balance\":" + longToStr(value) + "}";
                        valid = true;
                    }
                    break;
                case "d":
                    if (accounts.containsKey(accountName) && value > 0){
                        Account account = accounts.get(accountName);
                        if (account.card.equals(card)) {
                            json = "{\"account\":\"" + accountName + "\",\"deposit\":" + longToStr(value) + "}";
                            valid = true;
                        }
                    }
                    break;
                case "w":
                    if (accounts.containsKey(accountName) && value > 0){
                        Account account = accounts.get(accountName);
                        if (account.card.equals(card)){
                            if (account.getBalance() >= value){
                                json = "{\"account\":\"" + accountName + "\",\"withdraw\":" + longToStr(value) + "}";
                                valid = true;
                            }
                        }
                    }
                    break;
                case "g":
                    if (accounts.containsKey(accountName)){
                        Account account = accounts.get(accountName);
                        if (account.card.equals(card)){
                            json = "{\"account\":\"" + accountName + "\",\"balance\":" + longToStr(account.getBalance()) + "}";
                            valid = true;
                        }
                    }
                    break;
            }

        }

        public void writeToDatabase() throws Exception{
            if (valid) {
                Account account = null;
                switch (operation) {
                    case "n":
                        account = new Account(card, value);
                        accounts.put(accountName, account);
                        break;
                    case "d":
                        account = accounts.get(accountName);
                        account.deposit(value);
                        break;
                    case "w":
                        account = accounts.get(accountName);
                        if (!account.withdraw(value))
                            throw new Exception();
                        break;
                }
                System.out.println(json.toString().trim());
            }
        }

        public void cancel(){

            if (operation != null && operation.equals("n") && accounts.containsKey(accountName))
                accounts.remove(accountName);

        }
    }

    class Account{
        private String card;
        private long balance;

        public Account(String card, long balance) {
            this.card = card;
            this.balance = balance;
            cards.add(card);
        }

        public long getBalance() {
            return balance;
        }

        public void deposit(long amount){
            balance = balance + amount;
        }

        public boolean withdraw(long amount){
            if (balance < amount)
                return false;
            balance = balance - amount;
            return true;
        }
    }

    class BankServerThread {

        private Socket socket = null;

        public BankServerThread(Socket socket) {
            this.socket = socket;
        }

        Transaction transaction = new Transaction();

        public void start() {

            try (
                    OutputStream out = socket.getOutputStream();
                    DataOutputStream dos = new DataOutputStream(out);

                    InputStream is = socket.getInputStream();
                    DataInputStream dis = new DataInputStream(is);
            ) {

                socket.setSoTimeout(10000);

                byte[] inputdata;
                int len = dis.readInt();
                if (len > 0 && len < 1024) {
                    inputdata = new byte[len];
                    dis.readFully(inputdata);
                }else throw new Exception();

                byte[] outputdata = processInput(inputdata, transaction);

                if (outputdata != null && outputdata.length > 0) {
                    dos.writeInt(outputdata.length);
                    dos.write(outputdata, 0, outputdata.length);
                } else {
                    throw new Exception();
                }

                socket.close();

                //All done, fixing transaction to secured database
                transaction.writeToDatabase();
                
            } catch (Exception e) {
                transaction.cancel();
                System.out.println("protocol_error");
            }

        }
    }

    byte[] processInput(byte[] theInput, Transaction transaction) throws Exception{

        String inputString = CommonUtils.decrypt(theInput, secret).trim();

        String[] strs = inputString.trim().split("#");

        transaction.setTransaction(strs[2], strs[0], strs[1], Long.parseLong(strs[3]));

        String outString = transaction.getJsonString();

        Random random = new Random();
        int ml = outString.length();

        byte[] out = CommonUtils.encrypt(new String(new byte[random.nextInt(ml/2)]) + outString + new String(new byte[random.nextInt(ml/2)]), secret);

        return out;
    }

}
