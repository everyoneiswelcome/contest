#include <openssl/evp.h>                // for EVP_CIPHER_CTX_ctrl, etc
#include <openssl/ossl_typ.h>           // for EVP_CIPHER_CTX
#include <openssl/rand.h>               // for RAND_bytes
#include <pcre.h>                       // for pcre_compile, pcre_exec, etc
#include <stdint.h>                     // for uint8_t
#include <stdio.h>                      // for fprintf, stderr, fflush, etc
#include <stdlib.h>                     // for free, malloc
#include <string.h>                     // for NULL, memset, memcpy, etc
#include <sys/socket.h>                 // for recv, send
#include <time.h>                       // for time_t, time, difftime

#define INVALID 255
#define PROTOCOLERR 63
#define PORTMAX 65535
#define MAXPENDING 2
#define CARDSIZE 32
#define KEYSIZE 32
#define TIMEOUT 10
#define BUFSIZE 1024
#define FILENAMELEN 255
#define ACCOUNTNAMELEN 250
#define MAXBALANCE 4294967296
#define IVLEN 16

#ifdef DEBUG
#define DEBUG_ 1
#else
#define DEBUG_ 0
#endif

#define debug_printf(...) \
    do { \
        if (DEBUG_) { \
            fprintf(stderr, "%s:%d:%s: ", __FILE__, __LINE__, __func__); \
            fprintf(__VA_ARGS__); \
         } \
    } while (0)

typedef struct account {
    char name[256];
    unsigned char card[CARDSIZE];
    long double balance;
} __attribute__((packed)) account;

typedef struct net_msg {
    char operation;
    struct account account;
} __attribute__((packed)) net_msg;

int check_match(char *p, char *a_regex) {
    pcre *re_compiled;
    pcre_extra *pcre_extra = NULL;
    int pcre_ret;
    int sub_str_vec[30];
    const char *pcre_error;
    int pcre_error_offset;
    uint8_t ret_val;

    debug_printf(stderr, "Using regex: %s\n", a_regex);
    re_compiled = pcre_compile(a_regex, 0, &pcre_error, &pcre_error_offset, NULL);

    if (re_compiled == NULL) {
        debug_printf(stderr, "Error: could not compile %s: %s\n", a_regex, pcre_error);
        ret_val = INVALID;
        goto End;
    }

    pcre_extra = pcre_study(re_compiled, 0, &pcre_error);

    if (pcre_error != NULL) {
        debug_printf(stderr, "Error: could not study %s: %s\n", a_regex, pcre_error);
        ret_val = INVALID;
        goto End;
    }

    debug_printf(stderr, "Checking match with : %s\n", p);
    pcre_ret = pcre_exec(re_compiled, pcre_extra, p, strlen(p), 0, 0, sub_str_vec, 30);
    if (pcre_ret > 0) {
        debug_printf(stderr, "match found\n");
        ret_val = 0;
    }
    else if (pcre_ret == PCRE_ERROR_NOMATCH) {
        debug_printf(stderr, "No match\n");
        ret_val = INVALID;
    }
    else {
        debug_printf(stderr, "Error: PCRE something went wrong\n");
        ret_val = INVALID;
    }

End:
    if (re_compiled) {
        pcre_free(re_compiled);
    }
    if (pcre_extra) {
        pcre_free(pcre_extra);
    }

    return ret_val;
}

void print_json(char* account_name, long double balance, char print_type) {
    if (print_type == 'c') {
        printf("{\"account\":\"%s\", \"initial_balance\":%.2Lf}\n",
                account_name, balance);
        fflush(stdout);
    }
    else if (print_type == 'b') {
        printf("{\"account\":\"%s\", \"balance\":%.2Lf}\n",
                account_name, balance);
        fflush(stdout);
    }
    else if (print_type == 'w') {
        printf("{\"account\":\"%s\", \"withdraw\":%.2Lf}\n",
                account_name, balance);
        fflush(stdout);
    }
    else if (print_type == 'd') {
        printf("{\"account\":\"%s\", \"deposit\":%.2Lf}\n",
                account_name, balance);
        fflush(stdout);
    }
}

int send_encrypted(int sock, net_msg *new_msg, unsigned char *private_key) {
    int status;
    EVP_CIPHER_CTX *ctx = NULL;
    int len;
    int ciphertext_len;
    time_t rawtime;
    unsigned char magic = '\x42';
    unsigned char *ciphertext = NULL;
    unsigned char iv[IVLEN];
    unsigned char tag[KEYSIZE];

    ciphertext = malloc(BUFSIZE);
    if (ciphertext == NULL) {
        status = INVALID;
        goto End;
    }

    memset(tag, 0, KEYSIZE);
    memset(ciphertext, 0, BUFSIZE);

    debug_printf(stderr, "Preparing to encrypt msg\n");
    time(&rawtime);
    status = RAND_bytes(iv, IVLEN);
    if (status != 1) {
        status = INVALID;
        goto End;
    }
    debug_printf(stderr, "Initializing context\n");
    ctx = EVP_CIPHER_CTX_new();
    if (!ctx) {
        status = INVALID;
        goto End;
    }
    debug_printf(stderr, "Initializing ciphers\n");
    status = EVP_EncryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL);
    if (status != 1) {
        status = INVALID;
        goto End;
    }
    debug_printf(stderr, "Setting IV length to %d\n", IVLEN);
    status = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, IVLEN, NULL);
    if (status != 1) {
        status = INVALID;
        goto End;
    }
    debug_printf(stderr, "Initializing encryption with private key and iv\n");
    status = EVP_EncryptInit_ex(ctx, NULL, NULL, private_key, iv);
    if (status != 1) {
        status = INVALID;
        goto End;
    }
    debug_printf(stderr, "Encrypting msg\n");
    status = EVP_EncryptUpdate(ctx, ciphertext, &len, (unsigned char*)new_msg, sizeof(net_msg));
    if (status != 1) {
        status = INVALID;
        goto End;
    }
    ciphertext_len = len;
    debug_printf(stderr, "Encrypt msg len: %d\n", ciphertext_len);
    status = EVP_EncryptFinal_ex(ctx, ciphertext+len, &len);
    if (status != 1) {
        status = INVALID;
        goto End;
    }
    ciphertext_len += len;
    status = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_GET_TAG, 16, tag);
    if (status != 1) {
        status = INVALID;
        goto End;
    }

    status = send(sock, &magic, 1, 0);
    status += send(sock, iv, IVLEN, 0);
    status += send(sock, &rawtime, sizeof(time_t), 0);
    status += send(sock, ciphertext, ciphertext_len, 0);
    status += send(sock, tag, KEYSIZE, 0);

    if (status != (1 + IVLEN + sizeof(time_t) + ciphertext_len + KEYSIZE)) {
        debug_printf(stderr, "failed to send ciphertext\n");
        status = INVALID;
        goto End;
    }
    status = 0;
End:
    if (ciphertext) {
        free(ciphertext);
    }
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
    }
    return status;
}

int recv_encrypted(int sock, void* data, unsigned char *private_key) {
    EVP_CIPHER_CTX *ctx;
    int len;
    int plaintext_len;
    int status;
    time_t rawtime;
    time_t msgtime;
    double seconds;
    unsigned char magic;
    unsigned char iv[IVLEN];
    unsigned char recv_buf[BUFSIZE];
    unsigned char plaintext[BUFSIZE];
    net_msg *new_msg;
    unsigned char tag[KEYSIZE];

    debug_printf(stderr, "Attempting to decrypt msg\n");
    time(&rawtime);
    ctx = EVP_CIPHER_CTX_new();
    if(!ctx) {
        status = INVALID;
        goto End;
    }

    debug_printf(stderr, "Initializing decryption GCM\n");
    status = EVP_DecryptInit_ex(ctx, EVP_aes_256_gcm(), NULL, NULL, NULL);
    if (!status) {
        status = INVALID;
        goto End;
    }

    debug_printf(stderr, "Setting IV length to %d\n", IVLEN);
    status = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_IVLEN, IVLEN, NULL);
    if (!status) {
        status = INVALID;
        goto End;
    }

    debug_printf(stderr, "Receiving from buffer\n");
    len = recv(sock, &magic, 1, 0);
    if (len <= 0) {
        status = INVALID;
        goto End;
    }
    debug_printf(stderr, "Received %d bytes from buffer\n", len);

    if ('\x42' != magic) {
        status = INVALID;
        goto End;
    }

    debug_printf(stderr, "Magic matched\n");
    len = recv(sock, iv, IVLEN, 0);
    if (len <= 0) {
        status = INVALID;
        goto End;
    }
    debug_printf(stderr, "Received %d bytes from buffer\n", len);
    len = recv(sock, &msgtime, sizeof(time_t), 0);
    if (len <= 0) {
        status = INVALID;
        goto End;
    }
    debug_printf(stderr, "Received %d bytes from buffer\n", len);
    seconds = difftime(rawtime, msgtime);
    debug_printf(stderr, "Seconds are %.f apart\n", seconds);
    if (seconds > 10) {
        status = INVALID;
        goto End;
    }
    len = recv(sock, recv_buf, sizeof(net_msg), 0);
    if (len <= 0) {
        status = INVALID;
        goto End;
    }
    debug_printf(stderr, "Received %d bytes from buffer\n", len);
    len = recv(sock, &tag, KEYSIZE, 0);
    if (len <= 0) {
        debug_printf(stderr, "Failed to receive magic\n");
        status = INVALID;
        goto End;
    }
    debug_printf(stderr, "Received %d bytes from buffer\n", len);

    status = EVP_DecryptInit_ex(ctx, NULL, NULL, private_key, iv);
    if (!status) {
        debug_printf(stderr, "Failed to Initialize with private key and iv\n");
        status = INVALID;
        goto End;
    }

    status = EVP_DecryptUpdate(ctx, plaintext, &len, recv_buf, sizeof(net_msg));
    if (!status) {
        debug_printf(stderr, "Failed to decrypt the message\n");
        status = INVALID;
        goto End;
    }
    plaintext_len = len;

    status = EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_GCM_SET_TAG, 16, tag);
    if (!status) {
        debug_printf(stderr, "Failed to set tag\n");
        status = INVALID;
        goto End;
    }

    status = EVP_DecryptFinal_ex(ctx, plaintext + len, &len);
    if (status <= 0) {
        debug_printf(stderr, "Failed to match tag and finalize decryption\n");
        status = INVALID;
        goto End;
    }

    plaintext_len += len;
#if DEBUG
    new_msg = (net_msg*)plaintext;
    debug_printf(stderr, "Read from buffer operation mode: %c\n", new_msg->operation);
#endif
    memcpy(data, plaintext, plaintext_len);
    status = 0;

End:
    if (ctx) {
        EVP_CIPHER_CTX_free(ctx);
    }
    return status;
}
