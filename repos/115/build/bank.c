#include <arpa/inet.h>                  // for inet_ntoa
#include <errno.h>                      // for errno, ENOENT
#include <getopt.h>                     // for optarg, getopt
#include <netinet/in.h>                 // for sockaddr_in, htonl, htons, etc
#include <openssl/rand.h>               // for RAND_bytes
#include <search.h>                     // for ENTRY, hsearch, hcreate, etc
#include <signal.h>                     // for sigaction, SIGTERM, etc
#include <stddef.h>                     // for size_t
#include <stdint.h>                     // for uint16_t, uint8_t, uint32_t
#include <stdio.h>                      // for stderr, NULL, fflush, etc
#include <stdlib.h>                     // for free, exit, malloc, strtol
#include <string.h>                     // for memcmp, memcpy, memset, etc
#include <sys/select.h>                 // for select, FD_ISSET, FD_SET, etc
#include <sys/socket.h>                 // for setsockopt, SOL_SOCKET, etc
#include <sys/stat.h>                   // for stat
#include <sys/time.h>                   // for timeval
#include <unistd.h>                     // for close
#include "bibifi.h"                     // for debug_printf, account, etc

char auth_file[FILENAMELEN+1] = "bank.auth";
int serv_sock = -1;
volatile sig_atomic_t done = 0;

int get_account(char *account_name, struct account **ret_account) {
    ENTRY e = {account_name, NULL};
    ENTRY *p;
    p = hsearch(e, FIND);
    if (p) {
        debug_printf(stderr, "found account\n");
        *ret_account = (account*)p->data;
        debug_printf(stderr, "got account name: %s balance: %.2Lf\n", (*ret_account)->name, (*ret_account)->balance);
        return 0;
    } else {
        debug_printf(stderr, "did not find account\n");
        return INVALID;
    }
}

struct account* create_account(char *account_name, long double balance) {
    ENTRY e = {account_name, NULL};
    ENTRY *p;
    account *new_account;
    int ret_val;

    new_account = malloc(sizeof(account));
    debug_printf(stderr, "creating account sizeof %lu\n", sizeof(account));
    if (new_account == NULL) {
        debug_printf(stderr, "account malloc failed\n");
        return NULL;
    }
    debug_printf(stderr, "Searching for account: %s\n", account_name);
    ret_val = get_account(account_name, &new_account);
    if (ret_val != INVALID) {
        debug_printf(stderr, "account already exists\n");
        return NULL;
    }
    strncpy(new_account->name, account_name, ACCOUNTNAMELEN);
    debug_printf(stderr, "account name set as %s\n", new_account->name);
    new_account->balance = balance;
    debug_printf(stderr, "account balance: %.2Lf\n", new_account->balance);
    ret_val = RAND_bytes(new_account->card, CARDSIZE);
    if (ret_val != 1) {
        debug_printf(stderr, "account card generation failed\n");
        free(new_account);
        return NULL;
    }

    p = hsearch(e, ENTER);
    if (p == NULL) {
        debug_printf(stderr, "account failed to insert\n");
        free(new_account);
        return NULL;
    }
    p->data = (void *)new_account;
    return new_account;
}

int get_balance(struct account *msg_account) {
    int ret_val;
    struct account* ret_account;

    ret_val = get_account(msg_account->name, &ret_account);
    if (ret_val == INVALID) {
        debug_printf(stderr, "account doesn't exist\n");
        goto End;
    }

    debug_printf(stderr, "got account name: %s balance: %.2Lf\n", ret_account->name, ret_account->balance);
    ret_val = memcmp(ret_account->card, msg_account->card, CARDSIZE);

    if (ret_val != 0) {
        ret_val = INVALID;
        goto End;
    }

    memcpy(msg_account, ret_account, sizeof(account));
    debug_printf(stderr, "copied to account name: %s balance: %.2Lf\n", msg_account->name, msg_account->balance);
    ret_val = 0;
End:
    return ret_val;
}

int deposit_account(struct account *msg_account) {
    int ret_val;
    struct account* ret_account;

    ret_val = get_account(msg_account->name, &ret_account);
    if (ret_val == INVALID) {
        debug_printf(stderr, "account doesn't exist\n");
        goto End;
    }

    debug_printf(stderr, "got account name: %s balance: %.2Lf\n", ret_account->name, ret_account->balance);
    ret_val = memcmp(ret_account->card, msg_account->card, CARDSIZE);

    if (ret_val != 0) {
        ret_val = INVALID;
        goto End;
    }

    if (msg_account->balance > 0) {
       ret_account->balance += msg_account->balance;
    } else {
        ret_val = INVALID;
    }

End:
    return ret_val;
}

int withdraw_account(struct account *msg_account) {
    int ret_val;
    struct account* ret_account;

    ret_val = get_account(msg_account->name, &ret_account);
    if (ret_val == INVALID) {
        debug_printf(stderr, "account doesn't exist\n");
        goto End;
    }

    debug_printf(stderr, "got account name: %s balance: %.2Lf\n", ret_account->name, ret_account->balance);
    ret_val = memcmp(ret_account->card, msg_account->card, CARDSIZE);

    if (ret_val != 0) {
        ret_val = INVALID;
        goto End;
    }

    if (ret_account->balance >= msg_account->balance) {
       ret_account->balance -= msg_account->balance;
    } else {
        ret_val = INVALID;
    }

End:
    return ret_val;
}

int create_authfile(char *auth_file, unsigned char *key) {
    int ret_val;
    size_t written;
    FILE *fp;

    fp = fopen(auth_file, "w");
    if (fp == NULL) {
        debug_printf(stderr, "Failed to open auth_file for writing\n");
        ret_val = INVALID;
        goto End;
    }
    ret_val = RAND_bytes(key, KEYSIZE);
    if (ret_val == 1) {
        written = fwrite(key, KEYSIZE, 1, fp);
        if (written != 1) {
            debug_printf(stderr, "Failed to write key to auth_file. Wrote %zu\n", written);
            ret_val = INVALID;
        }
    }
    printf("created\n");
    fflush(stdout);
End:
    if (fp) {
        fclose(fp);
    }
    return ret_val;
}

int create_tcp_serv(uint16_t port) {
    int sock = -1;
    int optval = 1;
    struct timeval tv;
    struct sockaddr_in srv_addr;
    tv.tv_sec = TIMEOUT;
    tv.tv_usec = 0;

    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        debug_printf(stderr,"failed to create sock\n");
        goto End;
    }

    setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval));
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

    memset(&srv_addr, 0, sizeof(srv_addr));
    srv_addr.sin_family = AF_INET;
    srv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    srv_addr.sin_port = htons(port);

    if (bind(sock, (struct sockaddr *) &srv_addr, sizeof(srv_addr)) < 0) {
        debug_printf(stderr,"failed to bind sock\n");
        close(sock);
        sock = -1;
        goto End;
    }

    if (listen(sock, MAXPENDING) < 0) {
        debug_printf(stderr,"failed to listen\n");
        close(sock);
        sock = -1;
        goto End;
    }
End:
    return sock;
}

void handle_tcp_client(int client, unsigned char *private_key) {
    char recv_buf[BUFSIZE];
    struct net_msg *new_msg;
    struct account *ret_account;
    struct timeval tv;
    int recv_size = 0;
    int optval = 1;

    tv.tv_sec = TIMEOUT;
    tv.tv_usec = 0;

    if (client < 0) {
        debug_printf(stderr, "protocol_error\n");
        goto End;
    }

    setsockopt(client, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval));
    setsockopt(client, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

    recv_size = recv_encrypted(client, recv_buf, private_key);
    if (recv_size == INVALID) {
        debug_printf(stderr, "Error decrypting recv buffer\n");
        goto End;
    }

    new_msg = (struct net_msg*) recv_buf;

    switch(new_msg->operation) {
        case 'c':
        {
            debug_printf(stderr, "bank: got message create account %s\n", new_msg->account.name);
            ret_account = create_account(new_msg->account.name, new_msg->account.balance);
            if (ret_account != NULL) {
                print_json(ret_account->name, ret_account->balance, 'c');
                memcpy(&(new_msg->account), ret_account, sizeof(account));
                debug_printf(stderr, "created account: %s\n", new_msg->account.name);
            } else {
                debug_printf(stderr, "bank: create account failed\n");
                new_msg->operation = 'f';
            }
            break;
        }
        case 'b':
        {
            debug_printf(stderr, "bank: got message get balance %s\n", new_msg->account.name);
            //get_balance(new_msg);
            recv_size = get_balance(&(new_msg->account));
            if (recv_size != 0){
                // fail
                debug_printf(stderr, "bank: get account balance failed\n");
                new_msg->operation = 'f';
            } else {
                debug_printf(stderr, "get balance returned account name: %s balance: %.2Lf\n", new_msg->account.name, new_msg->account.balance);
                print_json(new_msg->account.name, new_msg->account.balance, 'b');
            }
            break;
        }
        case 'd':
        {
            //deposit_account(new_msg);
            debug_printf(stderr, "bank: got message deposit %s\n", new_msg->account.name);
            recv_size = deposit_account(&(new_msg->account));
            if (recv_size != 0){
                // fail
                debug_printf(stderr, "bank: deposit failed\n");
                new_msg->operation = 'f';
            } else {
                debug_printf(stderr, "deposit returned account name: %s balance: %.2Lf\n", new_msg->account.name, new_msg->account.balance);
                print_json(new_msg->account.name, new_msg->account.balance, 'd');
            }
            break;
        }
        case 'w':
        {
            //withdraw_account(new_msg);
            debug_printf(stderr, "bank: got message withdraw %s\n", new_msg->account.name);
            recv_size = withdraw_account(&(new_msg->account));
            if (recv_size != 0){
                // fail
                debug_printf(stderr, "bank: withdraw failed\n");
                new_msg->operation = 'f';
            } else {
                debug_printf(stderr, "withdraw returned account name: %s balance: %.2Lf\n", new_msg->account.name, new_msg->account.balance);
                print_json(new_msg->account.name, new_msg->account.balance, 'w');
            }
            break;
        }
        default:
        {
            //send empty
            debug_printf(stderr, "protocol_error\n");
            new_msg->operation = 'f';
            break;
        }
    }

    debug_printf(stderr,"bank: finished processing. sending to ATM\n");
    recv_size = send_encrypted(client, new_msg, private_key);
    if (recv_size == INVALID){
        debug_printf(stderr,"failed to send data\n");
        printf("protocol_error\n");
        fflush(stdout);
        goto End;
    }

End:
    debug_printf(stderr,"done processing client\n");
    if (recv_size == INVALID) {
        printf("protocol_error\n");
        fflush(stdout);
    }
    if (client >= 0) {
        close(client);
    }
}

int accept_tcp_connection(int serv_sock) {
    int client;
    struct sockaddr_in client_addr;
    unsigned int len;

    len = sizeof(client_addr);
    client = accept(serv_sock, (struct sockaddr *) &client_addr, &len);
    if (client < 0) {
        debug_printf(stderr, "protocol_error socket: %d errno: %d\n", serv_sock, errno);
    }

    debug_printf(stderr, "handling client %s\n", inet_ntoa(client_addr.sin_addr));

    return client;
}

void sig_handler(int signo) {
    struct stat sts;
    int status;
    debug_printf(stderr, "caught signal %d\n", signo);
    if (signo == SIGTERM) {
        status = stat(auth_file, &sts);
        if (status == -1 && errno == ENOENT) {
            debug_printf(stderr,"auth_file does not exist\n");
        } else {
            remove(auth_file);
        }
        if (serv_sock != -1) {
            close(serv_sock);
        }
    }
    hdestroy();
    exit(0);
}

int start_server(uint16_t port, unsigned char *private_key) {
    fd_set sock_set;
    struct timeval sel_timeout;
    struct sigaction action;
    memset(&action, 0, sizeof(struct sigaction));
    action.sa_handler = sig_handler;
    sigaction(SIGTERM, &action, NULL);

    serv_sock = create_tcp_serv(port);
    if (serv_sock < 0) {
        return INVALID;
    }
    debug_printf(stderr, "Starting server on port %hu\n", port);

    while(1) {
        FD_ZERO(&sock_set);
        FD_SET(serv_sock, &sock_set);

        sel_timeout.tv_sec = TIMEOUT;
        sel_timeout.tv_usec = 0;

        if (select(serv_sock+1, &sock_set, NULL, NULL, &sel_timeout) == 0) {
            continue;
        }
        else
        {
            if (FD_ISSET(serv_sock, &sock_set)) {
                debug_printf(stderr, "Got request on port %hu\n", port);
                handle_tcp_client(accept_tcp_connection(serv_sock), private_key);
            }
        }
    }
    if (serv_sock != -1) {
        close(serv_sock);
    }
    return 0;
}

int main (int argc, char *argv[]) {
    char opt = 0;
    uint32_t len = 0;
    int status = 0;
    uint16_t port = 3000;
    char *stop_ptr = NULL;
    struct stat sts;
    unsigned char private_key[KEYSIZE];
    uint8_t unique_p = 0;
    uint8_t unique_s = 0;

    while ((opt = getopt(argc, argv, "s:p:")) != -1) {
        switch(opt) {
            case 'p':
            {
                if (unique_p) {
                    debug_printf(stderr,"Multiple arguments p\n");
                    status = INVALID;
                    goto End;
                }
                unique_p = 1;
                status = check_match(optarg, "^0$|^[1-9][0-9]*$");
                if (status != 0) {
                    debug_printf(stderr,"Invalid port\n");
                    status = INVALID;
                    goto End;
                }
                port = (int)strtol(optarg, &stop_ptr, 10);
                if (strlen(stop_ptr) > 0) {
                    debug_printf(stderr,"Invalid port\n");
                    status = INVALID;
                    goto End;
                }
                if (port < 1024 || port > PORTMAX) {
                    debug_printf(stderr,"Invalid port\n");
                    status = INVALID;
                    goto End;
                }
                debug_printf(stderr,"port: %d\n", port);
                break;
            }
            case 's':
            {
                if (unique_s) {
                    debug_printf(stderr,"Multiple arguments s\n");
                    status = INVALID;
                    goto End;
                }
                unique_s = 1;
                status = check_match(optarg, "^[_\\-\\.0-9a-z]+$");
                if (status == 0) {
                    strncpy(auth_file, optarg, FILENAMELEN);
                    len = strlen(auth_file);
                    if (len < 1 || len > FILENAMELEN) {
                        debug_printf(stderr,"Invalid auth_file\n");
                        status = INVALID;
                        goto End;
                    }
                    if (    strcmp(auth_file, ".") == 0 ||
                            strcmp(auth_file, "..") == 0)
                    {
                        debug_printf(stderr,"Invalid auth_file\n");
                        status = INVALID;
                        goto End;
                    }
                    debug_printf(stderr,"auth_file: %s\n", auth_file);
                    break;
                }
            }
            default:
            {
                debug_printf(stderr,"\nInvalid argument\n");
                status = INVALID;
                goto End;
            }
        }
    }

    if (optind < argc) {
        debug_printf(stderr, "Non option argument\n");
        status = INVALID;
        goto End;
    }

    if (hcreate(PORTMAX) == 0) {
        debug_printf(stderr,"failed to create hash table\n");
        status = INVALID;
        goto End;
    }

    status = stat(auth_file, &sts);
    if (!(status == -1 && errno == ENOENT)) {
        debug_printf(stderr,"auth_file must not exist\n");
        status = INVALID;
        goto End;
    }

    status = create_authfile(auth_file, private_key);
    if (status == INVALID) {
        debug_printf(stderr, "failed to generate auth file\n");
        goto End;
    }

    status = start_server(port, private_key);
    if (status == INVALID) {
        debug_printf(stderr, "failed to start server\n");
        goto End;
    }

End:
    return status;
}
