#include <arpa/inet.h>                  // for inet_addr, inet_pton
#include <errno.h>                      // for errno, ENOENT
#include <getopt.h>                     // for optarg, getopt, optind
#include <netinet/in.h>                 // for sockaddr_in, htons, etc
#include <stdint.h>                     // for uint8_t, uint16_t, uint32_t
#include <stdio.h>                      // for stderr, fopen, NULL, fclose, etc
#include <stdlib.h>                     // for strtold, free, malloc, etc
#include <string.h>                     // for strlen, strncpy, strcmp, etc
#include <sys/socket.h>                 // for setsockopt, AF_INET, etc
#include <sys/stat.h>                   // for stat
#include <sys/time.h>                   // for timeval
#include <unistd.h>                     // for close
#include "bibifi.h"                     // for debug_printf, INVALID, etc

int check_ipaddress(char *ip_address) {
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, ip_address, &(sa.sin_addr));
    debug_printf(stderr, "inet_pton returned %d\n", result);
    return result ? 0 : INVALID;
}

int send_msg(char* account_name, long double balance, char* card_file, char* ip_address, uint16_t port, char operation_mode, unsigned char *private_key) {
    int sock = 0;
    int optval = 1;
    int status;
    struct timeval tv;
    struct sockaddr_in s_addr;
    struct net_msg *new_msg = NULL;
    FILE *card = NULL;

    new_msg = malloc(sizeof(net_msg));
    if (!new_msg) {
        debug_printf(stderr, "failed to allocate net msg\n");
        status = INVALID;
        goto End;
    }

    memset(new_msg, 0, sizeof(net_msg));
    new_msg->operation = operation_mode;
    debug_printf(stderr, "atm: got account name: %s\n", account_name);
    strncpy(new_msg->account.name, account_name, ACCOUNTNAMELEN);
    debug_printf(stderr, "sending request for account %s\n", new_msg->account.name);
    new_msg->account.balance = balance;
    debug_printf(stderr, "sending request balance %.2Lf\n", new_msg->account.balance);
    if (operation_mode != 'c') {
        card = fopen (card_file, "r");
        if (!card) {
            debug_printf(stderr, "failed to open card_file\n");
            status = INVALID;
            goto End;
        }
        status = fread(new_msg->account.card, 1, CARDSIZE, card);
        if (status != CARDSIZE) {
            debug_printf(stderr, "failed to read card_file\n");
            status = INVALID;
            goto End;
        }
    }

    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        debug_printf(stderr,"failed to create sock\n");
        status = INVALID;
        goto End;
    }

    tv.tv_sec = TIMEOUT;
    tv.tv_usec = 0;

    setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv, sizeof(struct timeval));
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

    memset(&s_addr, 0, sizeof(s_addr));
    s_addr.sin_family = AF_INET;
    s_addr.sin_addr.s_addr = inet_addr(ip_address);
    s_addr.sin_port = htons(port);

    debug_printf(stderr,"ATM: Connecting to server\n");
    if (connect(sock, (struct sockaddr *) &s_addr, sizeof(s_addr)) < 0) {
        debug_printf(stderr,"failed to connect sock %d\n", errno);
        status = INVALID;
        goto End;
    }

    debug_printf(stderr,"ATM: Sending to server\n");
    status = send_encrypted(sock, new_msg, private_key);

    if (status == INVALID) {
        debug_printf(stderr,"failed to send data\n");
        goto End;
    }

    debug_printf(stderr,"ATM: Receiving from server\n");
    status = recv_encrypted(sock, new_msg, private_key);
    if (status == INVALID) {
        debug_printf(stderr,"failed to recv data\n");
        status = PROTOCOLERR;
        goto End;
    }
    if (new_msg->operation == 'f') {
        debug_printf(stderr, "bank failed command\n");
        status = INVALID;
        goto End;
    } else if (new_msg->operation == 'c') {
        card = fopen(card_file, "w");
        if (card == NULL) {
            debug_printf(stderr, "Failed to open card_file for writing\n");
            status = INVALID;
            goto End;
        }
        status = fwrite(new_msg->account.card, 1, KEYSIZE, card);
        if (status != KEYSIZE) {
            debug_printf(stderr, "Failed to write key to card_file. Wrote %u\n", status);
            status = INVALID;
            goto End;
        }
        status = 0;
    }

    print_json(new_msg->account.name, new_msg->account.balance, new_msg->operation);
End:
    if (new_msg) {
        free(new_msg);
    }
    if (card) {
        fclose(card);
    }
    if (sock) {
        close(sock);
    }
    return status;
}

int main (int argc, char *argv[]) {
    char opt = 0;
    uint32_t len = 0;
    int status = 0;
    uint16_t port = 3000;
    long double balance = 0.0;
    char auth_file[FILENAMELEN+1] = "bank.auth";
    char ip_address[16] = "127.0.0.1";
    char card_file[FILENAMELEN+sizeof(".card")] = "";
    char account_name[ACCOUNTNAMELEN+1] = "";
    char operation_mode = 0;
    char *stop_ptr = NULL;
    struct stat sts;
    FILE *auth = 0;
    unsigned char auth_data[KEYSIZE];
    uint8_t unique_s = 0;
    uint8_t unique_i = 0;
    uint8_t unique_p = 0;
    uint8_t unique_c = 0;
    uint8_t unique_a = 0;
    uint8_t unique_n = 0;
    uint8_t unique_d = 0;
    uint8_t unique_w = 0;
    uint8_t unique_g = 0;

    while ((opt = getopt(argc, argv, "s:i:p:c:a:n:d:w:g")) != -1) {
        switch(opt) {
            case 'p':
            {
                if (unique_p) {
                    debug_printf(stderr, "Duplicate argument\n");
                    status = INVALID;
                    goto End;
                }
                unique_p = 1;
                status = check_match(optarg, "^0$|^[1-9][0-9]*$");
                if (status == 0) {
                    port = (uint16_t)strtol(optarg, &stop_ptr, 10);
                    if (strlen(stop_ptr) > 0) {
                        debug_printf(stderr,"Invalid port\n");
                        status = INVALID;
                        goto End;
                    }
                    if (port < 1024 || port > 65535) {
                        debug_printf(stderr,"Invalid port\n");
                        status = INVALID;
                        goto End;
                    }
                    debug_printf(stderr,"port: %d\n", port);
                    break;
                }
            }
            case 'i':
            {
                if (unique_i) {
                    debug_printf(stderr, "Duplicate argument\n");
                    status = INVALID;
                    goto End;
                }
                unique_i = 1;
                status = check_match(optarg, "^((25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)\\.){3}(25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]?\\d)$");
                if (status != 0) {
                    debug_printf(stderr,"Invalid ip address\n");
                    status = INVALID;
                    goto End;
                }
                status = check_ipaddress(optarg);
                if (status != 0) {
                    debug_printf(stderr,"Invalid ip address\n");
                    status = INVALID;
                    goto End;
                }
                strncpy(ip_address, optarg, 16);
                ip_address[15] = '\0';
                debug_printf(stderr,"port: %d\n", port);
                break;
            }
            case 's':
            {
                if (unique_s) {
                    debug_printf(stderr, "Duplicate argument\n");
                    status = INVALID;
                    goto End;
                }
                unique_s = 1;
                status = check_match(optarg, "^[_\\-\\.0-9a-z]+$");
                if (status != 0) {
                    debug_printf(stderr,"Invalid auth_file\n");
                    status = INVALID;
                    goto End;
                }
                len = strlen(optarg);
                if (len < 1 || len > FILENAMELEN) {
                    debug_printf(stderr,"Invalid auth_file\n");
                    status = INVALID;
                    goto End;
                }
                if (    strcmp(optarg, ".") == 0 ||
                        strcmp(optarg, "..") == 0)
                {
                    debug_printf(stderr,"Invalid auth_file\n");
                    status = INVALID;
                    goto End;
                }
                strncpy(auth_file, optarg, FILENAMELEN);
                auth_file[FILENAMELEN] = '\0';
                debug_printf(stderr,"auth_file: %s\n", auth_file);
                break;
            }
            case 'c':
            {
                if (unique_c) {
                    debug_printf(stderr, "Duplicate argument\n");
                    status = INVALID;
                    goto End;
                }
                unique_c = 1;
                status = check_match(optarg, "^[_\\-\\.0-9a-z]+$");
                if (status != 0) {
                    debug_printf(stderr,"Invalid card_file\n");
                    status = INVALID;
                    goto End;
                }
                len = strlen(optarg);
                if (len < 1 || len > FILENAMELEN) {
                    debug_printf(stderr,"Invalid card_file\n");
                    status = INVALID;
                    goto End;
                }
                if (    strcmp(optarg, ".") == 0 ||
                        strcmp(optarg, "..") == 0)
                {
                    debug_printf(stderr,"Invalid card_file\n");
                    status = INVALID;
                    goto End;
                }
                strncpy(card_file, optarg, FILENAMELEN);
                card_file[FILENAMELEN] = '\0';
                debug_printf(stderr,"card_file: %s\n", card_file);
                break;
            }
            case 'a':
            {
                if (unique_a) {
                    debug_printf(stderr, "Duplicate argument\n");
                    status = INVALID;
                    goto End;
                }
                unique_a = 1;
                status = check_match(optarg, "^[_\\-\\.0-9a-z]+$");
                if (status != 0) {
                    debug_printf(stderr,"Invalid account name\n");
                    status = INVALID;
                    goto End;
                }
                len = strlen(optarg);
                if (len < 1 || len > ACCOUNTNAMELEN) {
                    debug_printf(stderr,"Invalid account name\n");
                    status = INVALID;
                    goto End;
                }
                strncpy(account_name, optarg, ACCOUNTNAMELEN);
                debug_printf(stderr,"account_name: %s\n", account_name);
                break;
            }
            case 'n':
            {
                if (unique_n) {
                    debug_printf(stderr, "Duplicate argument\n");
                    status = INVALID;
                    goto End;
                }
                unique_n = 1;
                if ( operation_mode != 0 ) {
                    debug_printf(stderr,"Only one operation mode at a time\n");
                    status = INVALID;
                    goto End;
                }
                operation_mode = 'c';
                status = check_match(optarg, "^(0|^[1-9][0-9]*)\\.[0-9]{2}$");
                if (status != 0) {
                    debug_printf(stderr,"Invalid balance regex\n");
                    status = INVALID;
                    goto End;
                }
                balance = strtold(optarg, &stop_ptr);
                if (strlen(stop_ptr) > 0) {
                    debug_printf(stderr,"Invalid balance strtol\n");
                    status = INVALID;
                    goto End;
                }
                if (balance < 10 || balance >= MAXBALANCE) {
                    debug_printf(stderr,"Invalid balance bounds\n");
                    status = INVALID;
                    goto End;
                }
                break;
            }
            case 'd':
            {
                if (unique_d) {
                    debug_printf(stderr, "Duplicate argument\n");
                    status = INVALID;
                    goto End;
                }
                unique_d = 1;
                if ( operation_mode != 0 ) {
                    debug_printf(stderr,"Only one operation mode at a time\n");
                    status = INVALID;
                    goto End;
                }
                operation_mode = 'd';
                status = check_match(optarg, "^(0|^[1-9][0-9]*)\\.[0-9]{2}$");
                if (status != 0) {
                    debug_printf(stderr,"Invalid deposit regex\n");
                    status = INVALID;
                    goto End;
                }
                balance = strtold(optarg, &stop_ptr);
                if (strlen(stop_ptr) > 0) {
                    debug_printf(stderr,"Invalid balance strol\n");
                    status = INVALID;
                    goto End;
                }
                if (balance <= 0 || balance >= MAXBALANCE) {
                    debug_printf(stderr,"Invalid balance bounds\n");
                    status = INVALID;
                    goto End;
                }
                break;
            }
            case 'w':
            {
                if (unique_w) {
                    debug_printf(stderr, "Duplicate argument\n");
                    status = INVALID;
                    goto End;
                }
                unique_w = 1;
                if ( operation_mode != 0 ) {
                    debug_printf(stderr,"Only one operation mode at a time\n");
                    status = INVALID;
                    goto End;
                }
                operation_mode = 'w';
                status = check_match(optarg, "^(0|^[1-9][0-9]*)\\.[0-9]{2}$");
                if (status != 0) {
                    debug_printf(stderr,"Invalid withdraw regex\n");
                    status = INVALID;
                    goto End;
                }
                balance = strtold(optarg, &stop_ptr);
                if (strlen(stop_ptr) > 0) {
                    debug_printf(stderr,"Invalid withdraw strtol\n");
                    status = INVALID;
                    goto End;
                }
                if (balance <= 0 || balance >= MAXBALANCE) {
                    debug_printf(stderr,"Invalid withdraw bounds\n");
                    status = INVALID;
                    goto End;
                }
                break;
            }
            case 'g':
            {
                if (unique_g) {
                    debug_printf(stderr, "Duplicate argument\n");
                    status = INVALID;
                    goto End;
                }
                unique_g = 1;
                if ( operation_mode != 0 ) {
                    debug_printf(stderr,"Only one operation mode at a time\n");
                    status = INVALID;
                    goto End;
                }
                operation_mode = 'b';
                break;
            }
            default:
            {
                debug_printf(stderr,"\nInvalid argument\n");
                status = INVALID;
                goto End;
            }
        }
    }

    if (optind < argc) {
        debug_printf(stderr, "Non option argument\n");
        status = INVALID;
        goto End;
    }

    if (!unique_a) {
        debug_printf(stderr, "account name required\n");
        status = INVALID;
        goto End;
    }
    if (operation_mode == 0) {
        debug_printf(stderr, "Operation mode required\n");
        status = INVALID;
        goto End;
    }
    if (!unique_c && card_file[0] == '\0') {
        strncpy(card_file, account_name, ACCOUNTNAMELEN);
        strcat(card_file, ".card");
    }
    status = stat(auth_file, &sts);
    if (status == -1 && errno == ENOENT) {
        debug_printf(stderr,"auth_file does not exist\n");
        status = INVALID;
        goto End;
    }

    status = stat(card_file, &sts);
    if (status == -1 && errno == ENOENT) {
        if (operation_mode != 'c') {
            debug_printf(stderr,"card_file does not exist\n");
            status = INVALID;
            goto End;
        }
    } else {
        if (operation_mode == 'c') {
            debug_printf(stderr,"card_file must not exist\n");
            status = INVALID;
            goto End;
        }
    }
    auth = fopen (auth_file, "r");
    if (!auth) {
        debug_printf(stderr, "failed to open auth_file\n");
        status = INVALID;
        goto End;
    }
    status = fread(auth_data, 1, KEYSIZE, auth);
    if (status != KEYSIZE) {
        debug_printf(stderr, "failed to read auth_file\n");
        status = INVALID;
        goto End;
    }

    status = send_msg(account_name, balance, card_file, ip_address, port, operation_mode, auth_data);
    if (status != 0) {
        debug_printf(stderr, "error receiving data %d\n", status);
        goto End;
    }
End:
    if (auth) {
        fclose(auth);
    }
    return status;
}
