//
//  messages.h
//
//  Created by Jeff Abbott on 10/5/15.
//  Cybersecurity Capstone, Team AI
//  Build 0.7, 10/14/15
//
//  Defines message structure, plus function to package and extract the messages
#ifndef messages_h
#define messages_h

#include <stdio.h>

struct seqmessage {
    const int msgid;
    unsigned long seqnum;
};

struct transmessage {
    unsigned long seqnum;
    char account[251];
    char action;
    double amount;
};

struct respmessage {
    unsigned long seqnum;
    char passed;
    double amount;
};

int encryptBuffer(const unsigned char* inBuff, unsigned char* outBuff, const unsigned char* enckey, int len);

int decryptBuffer(const unsigned char* inBuff, unsigned char* outBuff, const unsigned char* enckey, int len);

int packageMessage(unsigned char* input, unsigned char* output, int length, unsigned char* key);

int extractMessage(unsigned char* input, unsigned char* output, int length, unsigned char* key);

int messageLength(int payloadLength);

#endif /* messages_h */
