#include "messages.h"
#include <string.h>
#include <stdio.h>

void main()
{
    unsigned char key[16];
    unsigned char inbuf[1024];
    unsigned char outbuf[1024];
    memset(key, 5, 16);

    char* tmessage = "Test message";
    memcpy(inbuf, tmessage, strlen(tmessage));

    int result = encryptBuffer(inbuf, outbuf, key, strlen(tmessage));
    if (result < 0)
	fputs("Encrypt error\n", stderr);
    else
        fprintf(stderr, "Returned %i bytes\n", result);

    memset(inbuf, 0, sizeof(inbuf));
    result = decryptBuffer(outbuf, inbuf, key, result);
    if (result < 0)
        fputs("Decrypt error\n", stderr);
    else
        fprintf(stderr, "Decrypted %i bytes\n", result);

    char* outstring;
    outstring = (char*) inbuf;
    outstring[result] = 0;

    fprintf(stderr, "%s\n", outstring);
}
