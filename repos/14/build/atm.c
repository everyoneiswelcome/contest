//
//  atm.c
//  atm
//
//  Created by Jeff Abbott on 10/3/15.
//  Cybersecurity Capstone: Team AI
//  Version 0.7 10/14/15
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <math.h>
#include <ctype.h>
#include "messages.h"
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/time.h>

// Prototypes
int CheckFileName(char*);
int CheckIPAddress(char*);
int CheckAccountName(char*);

// Globals
unsigned char cryptokey[32];

int main(int argc, const char * argv[]) {
    // Check for minimum set of arguments
    // for (int arg = 0; arg < argc; arg++)
    // {
    //	fprintf(stderr, "%s ", argv[arg]);
    // }
    // fprintf(stderr, "\n");
    if (argc < 4)
    {
        // fputs("Need at least 4 arguments\n", stderr);
        return 255;
    }
    
    // Parse command line arguments
    int fs = 0;  // Variable to hold whether these arguments have been passed
    int fi = 0;
    int fp = 0;
    int fc = 0;
    int flaga = 0;
    char action = 'u';  // Desired action - 'u' = unset, 'n' = new account, 'd' = deposit, 'w' = withdraw, 'g' = get balance
    const char* authfilename = "bank.auth";
    char* ipaddress = "127.0.0.1";
    int portnum = 3000;
    char* cardfile = NULL;
    char* accountname = NULL;
    char* tempstring;
    double amount = 0.0;   // Holds whole number amount
    //unsigned char decamount = 0; // Holds fractional amount
    // Loop through arguments
    
    for (int i = 1; i < argc; i++)
    {
        if (argv[i][0] == '-')  // Flag for arguments
        {
            char flag = argv[i][1];
            switch (flag)
            {
                case 's':
                {
                    if (fs == 1)  // duplicate argument
                    {
                        // fputs("Duplicate argument\n", stderr);
                        return 255;
                    }
                    fs = 1;
                    if (strlen(argv[i]) > 3)
                    {
                        tempstring = argv[i];
                        tempstring = tempstring + 2;
                    }
                    else if ((i + 1) == argc)  // last argument
                    {
                        // fputs("This flag can't be the last argument\n", stderr);
                        return 255;
                    }
                    else {
                        tempstring = argv[i+1];
                        i++;
                    }

                    authfilename = tempstring;
                    // Validate filename format
                    if (CheckFileName(authfilename) != 0)
                    {
                        // fputs("Invalid authentication filename\n", stderr);
                        return 255;
                    }
                    break;
                }
                case 'i':
                {
                    if (fi == 1)  // duplicate argument
                    {
                        // fputs("Duplicate argument\n", stderr);
                        return 255;
                    }
                    fi = 1;
                    if (strlen(argv[i]) > 3)
                    {
                        tempstring = argv[i];
                        tempstring = tempstring + 2;
                    }
                    else if ((i + 1) == argc)  // last argument
                    {
                        // fputs("This flag can't be the last argument\n", stderr);
                        return 255;
                    }
                    else{
                        tempstring = argv[i+1];
                        i++;
                    }
                    ipaddress = tempstring;
                    if (CheckIPAddress(ipaddress))
                    {
                        // fputs("Invalid IP address\n", stderr);
                        return 255;
                    }
                    break;
                }
                case 'p':
                {
                    if (fp == 1)  // duplicate argument
                    {
                        // fputs("Duplicate argument\n", stderr);
                        return 255;
                    }
                    fp = 1;
                    
                    if (strlen(argv[i]) > 3)
                    {
                        tempstring = argv[i];
                        tempstring = tempstring + 2;
                    }
                    else if ((i + 1) == argc)  // last argument
                    {
                        // fputs("This flag can't be the last argument\n", stderr);
                        return 255;
                    }
                    else{
                        tempstring = argv[i+1];
                        i++;
                    }
                    if (tempstring == NULL)
                        return 255;
                    portnum = atoi(tempstring);
                    if ((portnum > 0) && (tempstring[0] == '0'))
                    {
                        return 255;   // Leading zero
                    }
                    if ((portnum < 1024) || (portnum > 65535))
                    {
                        // fputs("Invalid port number\n", stderr);
                        return 255;
                    }
                    break;
                }
                case 'c':
                {
                    if (fc == 1)  // duplicate argument
                    {
                        // fputs("Duplicate argument\n", stderr);
                        return 255;
                    }
                    fc = 1;
                    
                    if (strlen(argv[i]) > 3)
                    {
                        tempstring = argv[i];
                        tempstring = tempstring + 2;
                    }
                    else if ((i + 1) == argc)  // last argument
                    {
                        // fputs("This flag can't be the last argument\n", stderr);
                        return 255;
                    }
                    else
                    {
                        tempstring = argv[i+1];
                        i++;
                    }

                    cardfile = tempstring;
                    // Validate filename format
                    if (CheckFileName(cardfile) != 0)
                    {
                        // fputs("Invalid card filename\n", stderr);
                        return 255;
                    }
                    break;
                }
                case 'a':
                {
                    if (flaga == 1)  // duplicate argument
                    {
                        // fputs("Duplicate argument\n", stderr);
                        return 255;
                    }
                    flaga = 1;
                    
                    if (strlen(argv[i]) > 3)
                    {
                        tempstring = argv[i];
                        tempstring = tempstring + 2;
                    }
                    else if ((i + 1) == argc)  // last argument
                    {
                        // fputs("This flag can't be the last argument\n", stderr);
                        return 255;
                    }
                    else
                    {
                        tempstring = argv[i+1];
                        i++;
                    }
                    accountname = tempstring;
                    
                    // Validate account name
                    if (CheckAccountName(accountname))
                    {
                        // fputs("Invalid account name\n", stderr);
                        return 255;
                    }
                    break;
                }
                case 'g':
                {
                    // Check that not already set
                    if (action == 'u')
                        action = flag;
                    else
                    {
                        // fputs("Duplicate argument\n", stderr);
                        return 255;
                    }
                    break;
                }
                case 'n':
                case 'd':
                case 'w':
                {
                    // Check that not already set
                    if (action == 'u')
                        action = flag;
                    else
                    {
                        // fputs("Duplicate argument\n", stderr);
                        return 255;
                    }
                    if (strlen(argv[i]) > 3)
                    {
                        tempstring = argv[i];
                        tempstring = tempstring + 2;
                    }
                    else if ((i + 1) < argc)  // not the last argument
                    {
                        // Parse the amount
                        tempstring = argv[i+1];
                        i++;
                    }
                    else{
                        // fputs("Missing amount\n", stderr);
                        return 255;
                    }
		    // Check that number has minumum number of digits
		    if (strlen(tempstring)<3)
		    {
			// fputs("Not enough digits in amount\n", stderr);
			return 255;
		    }

		    // check that number is correctly formatted
		    char c;
		    for (int cnum = 0; cnum < strlen(tempstring); cnum++)
		    {
			c = tempstring[cnum];
		        if(isdigit(c) || (c == '.'))
			{
			     // Good char
			}
			else
			{
			    // fputs("Invalid digit\n", stderr);
			    return 255;
			}
		    }

                    double tempamount = atof(tempstring);
                    if ((tempamount > 0.0) && (tempamount < 4294967296.0))
                    {
                        amount = tempamount;
                    }
                    else{
                        // fputs("Invalid amount\n", stderr);
                        return 255;
                    }
  		    // Check for leading zeros
		    if (tempstring[0] == '0')
		    {
			if (tempstring[1] != '.')
			{
			    // fputs("Leading zero\n", stderr);
			    return 255;
			}
 		    }
                    tempstring = strchr(tempstring, '.');
                    if (tempstring != NULL) {
                        if (strlen(tempstring) != 3)  // too many decimal digits
                        {
                            // fputs("Incorrect number of decimal digits\n", stderr);
                            return 255;
                        }
		
                    }
		    else
		    {
			// fputs("Missing fractional amount\n", stderr);
			return 255;
		    }
                    break;
                }
                default:  // None of the above
                {
                    // fputs("Invalid arguments\n", stderr);
                    return 255;
                }
                    
            }
        }
        else  // Invalid argument set
        {
            // fputs("Invalid arguments1\n", stderr);
            return 255;
        }
    }
    
    // validate that required arguments were set
    if ((flaga == 0) || (action == 'u'))  // Missing required argument
    {
        // fputs("Missing required argument\n", stderr);
        return 255;
    }
    
    // If cardfile not provided, set default cardfile name
    if (fc == 0)
    {
        // Concatentate account with '.card"
        cardfile = malloc(sizeof(char)*(strlen(accountname)+5));
        strcpy(cardfile, accountname);
        strcat(cardfile, ".card");
    }
    
    //fputs("checking authentication file\n", stderr);
    // Check that authentication file exists; import values
    FILE* afp;
    if ((afp = fopen(authfilename, "r")) != NULL)
    {
        if(fread(cryptokey, sizeof(unsigned char), 32, afp) < 32)
        {
            // Didn't read the entire key
            // fputs("Error reading authentication file\n", stderr);
	    fclose(afp);
            return 255;
        }
	int tempport;
        if(fread(&tempport, sizeof(int), 1, afp) < 1)
        {
            // fputs("Error reading allowed port\n", stderr);
	    fclose(afp);
            return 255;
        }
        if (tempport != portnum)  // Using invalid port
        {
            // fputs("Using invalid port number\n", stderr);
	    action = 'b';
	    portnum = tempport;	
            // return 63;
        }
        fclose(afp);
    }
    else
    {
        // Auth file can't be opened - error
        // fputs("Authentication file can't be opened\n", stderr);
        return 255;
    }

    // Check card file, and validate operation against it
    // If Operation = 'n', cardfile cannot exist
    if (action == 'n')
    {
        FILE* fp;
        if ((fp = fopen(cardfile, "r")) != NULL)
        {
            // file already exists
            fclose(fp);
            // fputs("Cardfile already exists, cannot create new account\n", stderr);
            return 255;
        }
        // Check to ensure amount >= 10.00 - here because only applies on 'n'
        if (amount < 10.0)
        {
            // fputs("Amount is too small\n", stderr);
            return 255;
        }
    }
    // else if Operation = 'g', 'd', or 'w', account name must match card file
    if ((action == 'g') || (action == 'w') || (action == 'd'))
    {
        // Open cardfile
        FILE* fp;
        if ((fp = fopen(cardfile, "r")) == NULL)
        {
            // fputs("Unable to open Cardfile\n", stderr);
            return 255;
        }
        // Read contents of Cardfile
        char name[251];
        memset(name, 0, sizeof(name));
        if(fread(name, 1, sizeof(name), fp) < 1)
        {
            // Didn't read the entire name
            // fputs("Error reading card file\n", stderr);
	    fclose(fp);
            return 255;
        }
        fclose(fp);

        // Decrypt Cardfile
        
        // If Account name in cardfile doesn't match account name, quit
        if (strcmp(accountname, name) != 0)
        {
            // fprintf(stderr, "Account name does not match card file: %s - %s\n", accountname, name);
            return 255;
        }
    }
    
    // Setup messaging variables
    char recvBuff[1024];
    char sendBuff[1024];
    memset(recvBuff, 0, sizeof(recvBuff));
    memset(sendBuff, 0, sizeof(sendBuff));
    struct seqmessage message1;
    struct transmessage message2;
    struct respmessage message3;
    int length1 = messageLength(sizeof(message1));
    int length2 = messageLength(sizeof(message2));
    int length3 = messageLength(sizeof(message3));
    
    // Set up sockets and buffers
    int sockfd = 0;
    int n = 0;
    struct sockaddr_in serv_addr;
    
    // Create socket
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0))< 0)
    {
        // fputs("Error : Could not create socket \n", stderr);
        return 63;
    }
    
    //Address socket
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(portnum);
    serv_addr.sin_addr.s_addr = inet_addr(ipaddress);
    // fprintf(stderr, "ATM: sending on %i\n", (int) serv_addr.sin_port);

    // Connect socket
    if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr))<0)
    {
        // fputs("Connect Failed \n", stderr);
        return 63;
    }

    // set timeout
    struct timeval tv;
    tv.tv_sec = 10;
    tv.tv_usec = 0;
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*) &tv, sizeof(struct timeval));
    setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char*) &tv, sizeof(struct timeval));
    // fputs("Got connected\n", stderr);
    // Get sequence (waiting up to 10 seconds)
    if((n = recv(sockfd, recvBuff, sizeof(recvBuff), 0)) < 0)
    {
        fputs("Failed to receive message 1\n", stderr);
        close(sockfd);
        return 63;
    }
    else
    {  
        // fputs("Received first\n", stderr); 
        if (n < length1)
        {
            //fprintf(stderr, "Got %i of %i expected\n", n, length1);
            return 63;
        }
        recvBuff[n] = 0;
        // fprintf(stderr, "Writing %i bytes to a  %i space\n", n, (int) sizeof(message1));
        if((extractMessage(recvBuff, (unsigned char*) &message1, n, cryptokey)) > 0)
        {
            // fputs("Got sequence number\n", stderr);
            // printf("Received seq num of %lu\n", message1.seqnum);
        }
        else
        {
            fputs("Error extracting message 1\n", stderr);
	    close(sockfd);
            return 63;
        }
    }
    
    // fputs("First message finished\n", stderr);
    // populate message 2 with sequence and send
    strcpy(message2.account, accountname);
    message2.action = action;
    message2.amount = amount;
    message2.seqnum = message1.seqnum;
    int length;
    // fputs("Packaging response\n", stderr);
    // if ((length = packageMessage((unsigned char*) &message2, sendBuff, sizeof(message2), cryptokey)) > 0)
    length = encryptBuffer((const unsigned char*)&message2, sendBuff, cryptokey, sizeof(message2));
    // fputs("returned from encrypt\n", stderr);
    if (length == length2)
    {
        write(sockfd, sendBuff, length);
    }

    // exit with error if bad port was requested
    if (action == 'b')
    {
	close(sockfd);
        return 63;
    }
    
    // Get response - if good, proceed (waiting up to 10 seconds)
    memset(recvBuff, '0', sizeof(recvBuff));
    if((n = recv(sockfd, recvBuff, sizeof(recvBuff)-1, 0)) < 0)
    {
        // fputs("Failed to receive message 3\n", stderr);
	close(sockfd);
        return 63;
    }
    else
    {
        recvBuff[n] = 0;
        // fprintf(stderr, "%i bytes received of %i expected\n", n, length3);
        if((extractMessage(recvBuff, (char*) &message3, n, cryptokey)) > 0)
        {
            // fprintf(stderr, "Received response message of %c\n", message3.passed);
        }
        else
        {
            fputs("Error extracting message 3\n", stderr);
	    close(sockfd);
            return 63;
        }
    }
    //extractMessage(recvBuff, (char*) &message3, length3, cryptokey);
    if ((message3.passed != 'Y'))
    {
        // fputs("atm: transaction failed\n", stderr);
	close(sockfd);
        return 255;
    }

    if ((message3.seqnum != message1.seqnum))
    {
        // fputs("Out of sequence\n", stderr);
	close(sockfd);
        return 63;
    }

    //fprintf(stderr, "Amount %lu.%i\n", message3.amount, message3.decamount);
    
    // close socket
    close(sockfd);
   
    // handle results
    switch (action)
    {
        case 'n':
        {
           
            // If approved, write cardfile
            FILE* fp;
            if ((fp = fopen(cardfile, "w")) != NULL)
            {
                // write file
                
                fprintf(fp, "%s", accountname);
                fclose(fp);
            }
            else{
                fputs("Unable to write new cardfile\n", stderr);
                return 255;
            }
            
            // Print result
            printf("{\"account\":\"%s\",\"initial_balance\":%.2f}\n", accountname, amount);
            fflush(stdout);
            break;
        }
        case 'd':
        {
            printf("{\"account\":\"%s\",\"deposit\":%.2f}\n", accountname, amount);
            fflush(stdout);
            break;
        }
        case 'w':
        {
            printf("{\"account\":\"%s\",\"withdraw\":%.2f}\n", accountname, amount);
            fflush(stdout);
            break;
        }
        case 'g':
        {
            printf("{\"account\":\"%s\",\"balance\":%.2f}\n", accountname, message3.amount);
            fflush(stdout);
            break;
        }
        default:
        {
            // fputs("Invalid action completed\n", stderr);
            return 63;
        }
    }
    
    // Cleanup
    
    return 0;
}

int CheckFileName(char* tFileName)
{
    int len = strlen(tFileName);
    if (len > 255)  // Filename too long
    {
        // fputs("Filename too long\n", stderr);
        return 255;
    }
    
    // Check each character
    for (int i = 0; i< len; i++)
    {
        char c = tFileName[i];
        if (isdigit(c) || islower(c) || (c == '_') || (c == '-') || (c == '.'))
        {
            // Good char
        }
        else{
            // fputs("Invalid character\n", stderr);
            return 255;
        }
    }
    // Make sure not . or ..
    if ((strcmp(".", tFileName) == 0) || (strcmp("..", tFileName) == 0))
    {
        // "." and ".." are not allowed
        // fputs("Invalid filename\n", stderr);
        return 255;
    }
    return 0;
}

// Procedure validates that string containing IP address is valid xxx.xxx.xxx.xxx format
int CheckIPAddress(char* tIPAddress)
{
    char* oct1 = NULL;
    char* oct2 = NULL;
    char* oct3 = NULL;
    char* oct4 = NULL;
    
    // check length
    int len = strlen(tIPAddress);
    if (len > 16)
    {
        // fputs("IP Address is too long\n", stderr);
        return 255;
    }
    
    // Check each character
    for (int i = 0; i< len; i++)
    {
        char c = tIPAddress[i];
        if (isdigit(c) || (c == '.'))
        {
            // Good char
        }
        else{
            // fputs("Invalid character\n", stderr);
            return 255;
        }
    }
    oct1 = tIPAddress;
    oct2 = strchr(oct1, '.');
    if (oct2 == NULL)
    {
        // fputs("Missing second octet\n", stderr);
        return 255;
    }
    oct2++;
    oct3 = strchr(oct2, '.');
    if (oct3 == NULL)
    {
        // fputs("Missing third octet\n", stderr);
        return 255;
    }
    oct3++;
    oct4 = strchr(oct3, '.');
    if (oct4 == NULL)
    {
        // fputs("Missing fourth octet\n", stderr);
        return 255;
    }
    oct4++;
    
    int j = atoi(oct1);
    if ((j < 0) || (j > 255))
    {
        // fputs("Invalid IP octet\n", stderr);
        return 255;
    }
    if ((j > 0) && (oct1[0] == '0'))
    {
        // fputs("Leading zero\n", stderr);
        return 255;
    }
    j = atoi(oct2);
    if ((j < 0) || (j > 255))
    {
        // fputs("Invalid IP octet\n", stderr);
        return 255;
    }
    if ((j > 0) && (oct2[0] == '0'))
    {
        return 255;  // leading zero
    }

    j = atoi(oct3);
    if ((j < 0) || (j > 255))
    {
        // fputs("Invalid IP octet\n", stderr);
        return 255;
    }
    if ((j > 0) && (oct3[0] == '0'))
    {
        return 255;  // leading zero
    }

    j = atoi(oct4);
    if ((j < 0) || (j > 255))
    {
        // fputs("Invalid IP octet\n", stderr);
        return 255;
    }
    if ((j > 0) && (oct4[0] == '0'))
    {
        return 255;  // leading zero
    }
    
    return 0;
}

// Procedure validates that Account Name meets spec requirements
int CheckAccountName(char* tAccountName)
{
    int len = strlen(tAccountName);
    if (len > 250)  // Accountname too long
    {
        // fputs("Account name too long\n", stderr);
        return 255;
    }
    if (len < 1)  // Accountname too short
    {
        // fputs("Account name too short\n", stderr);
        return 255;
    }
    
    
    // Check each character
    for (int i = 0; i< len; i++)
    {
        char c = tAccountName[i];
        if (isdigit(c) || islower(c) || (c == '_') || (c == '-') || (c == '.'))
        {
            // Good char
        }
        else
        {
            // fprintf(stderr, "Invalid character: %c\n", c);
            return 255;
        }
    }
    
    return 0;
}

