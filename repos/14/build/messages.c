//
//  messages.c
//
//  Created by Jeff Abbott on 10/5/15.
//  Cybersecurity Capstone, Team AI
//  Build 0.7, 10/14/15
//
//  Encrypt/decrypt functions based in part on code from http://stackoverflow.com/questions/3141860/aes-ctr-encryption-mode-of-operation-on-openssl


#include "messages.h"
#include <string.h>
#include <openssl/aes.h>
#include <openssl/rand.h>
#include <openssl/hmac.h>
#include <openssl/buffer.h>

static const size_t BUF_SIZE = 4096;

struct ctr_state
{
    unsigned char ivec[AES_BLOCK_SIZE];
    unsigned int num;
    unsigned char ecount[AES_BLOCK_SIZE];
};

static void init_ctr(struct ctr_state* state, const unsigned char iv[16])
{
    // Initialize ctr_state structure, including first 8 bytes of IV
    state->num = 0;
    memset(state->ecount, 0, AES_BLOCK_SIZE);
    memset(state->ivec, 0, 16);
    memcpy(state->ivec, iv, 8);
}

int encryptBuffer(const unsigned char* inBuff, unsigned char* outBuff, const unsigned char* enckey, int len)
{
    unsigned char*  outptr = outBuff;
    const unsigned char* inptr = inBuff;
    // Generate and insert randome bytes
    unsigned char buf[1024];
    if (!RAND_bytes(buf, AES_BLOCK_SIZE))
    {
	fputs("Could not write random bytes\n", stderr);
        return -1;
    }
    memcpy(outptr, buf, 8);
    outptr = outptr + 8;
    memset(outptr, 0, 8);
    outptr = outptr + 8;

    // Initialize Encryption
    AES_KEY key;
    if (AES_set_encrypt_key(enckey, 128, &key) < 0)
    {
        fputs("Unable to set encryption key\n", stderr);
        return -2;
    }
    struct ctr_state state;
    init_ctr(&state, buf);

    size_t len_write = 0;
    // Encrypt blocks and write to outBuff
        if (BUF_SIZE > len)
            len_write = len;
        else
            len_write = BUF_SIZE;
        // fprintf(stderr, "len_write = %i\n", (int)len_write);
        AES_ctr128_encrypt(inptr, buf, len_write, &key, state.ivec, state.ecount, &state.num);
        memcpy(outptr, buf, len_write);
        // fputs("Copied into outbuf\n", stderr);
        // outptr = outptr + len_write;   
    // int tint = len_write + AES_BLOCK_SIZE;
    // fprintf(stderr, "Total length = %i\n", (int)(len_write + AES_BLOCK_SIZE));    
    return len_write + AES_BLOCK_SIZE;
}

int decryptBuffer(const unsigned char* inBuff, unsigned char* outBuff, const unsigned char* enckey, int len)
{
    const unsigned char* inptr = inBuff;
    unsigned char* outptr = outBuff;
    // decrypt and write to outbuff

    // Initialize decryption
    AES_KEY key;
    if (AES_set_encrypt_key(enckey, 128, &key) < 0)
    {
        fputs("Unable to set decryption key\n", stderr);
        return -1;
    }
    unsigned char buf[BUF_SIZE];
    struct ctr_state state;
    memcpy(buf, inptr, AES_BLOCK_SIZE);
    inptr = inptr + AES_BLOCK_SIZE;
    init_ctr(&state, buf);
    // fprintf(stderr, "decrypting %i bytes\n", (len - AES_BLOCK_SIZE));
    AES_ctr128_encrypt(inptr, outptr, (len - AES_BLOCK_SIZE), &key, state.ivec, state.ecount, &state.num); 
    // fprintf(stderr, "Managed to fit it\n");
    return (len - AES_BLOCK_SIZE);
}

int packageMessage(unsigned char* input, unsigned char* output, int length, unsigned char* key)
{
    // Wrapper around encrypt function
    // Returns total length of encrypted, MAC'd buffer
    
    // memcpy(output, input, length);
    // fprintf(stderr, "Packaging %i message\n", length);
    int len = encryptBuffer(input, output, key, length);
    // fputs("returned\n", stderr);
    // fprintf(stderr, "Encrypted %i bytes, of %i message\n", len, length);
    return len;
}

int extractMessage(unsigned char* input, unsigned char* output, int length, unsigned char* key)
{
    // Wrapper around decrypt function
    // Output will be the decrypted payload, will return the length of content of the output buffer
    // memcpy(output, input, length);
 
    int len = decryptBuffer(input, output, key, length);
    // fprintf(stderr, "Decrypted %i of %i byte message\n", len, length);
    return len;
}

int messageLength(int payloadLength)
{
    // Eventually, this will include length of HMAC, IV, and any padding
    return payloadLength + AES_BLOCK_SIZE;
}

