//
//  messages.c
//
//  Created by Jeff Abbott on 10/5/15.
//  Cybersecurity Capstone, Team AI
//

#include "messages.h"
#include <string.h>

int packageMessage(char* input, char* output, int length, unsigned char* key)
{
    // ***** Fix - this will pad the input buffer (if required), encrypt it, and concat the HMAC on the end
    // Returns total length of encrypted, MAC'd buffer
    
    memcpy(output, input, length);
    
    return length;
}

int extractMessage(char* input, char* output, int length, unsigned char* key)
{
    // ***** FIX - this will pull off the MAC, HMAC the rest, compare the MACs, and then decrypt the rest
    // Output will be the decrypted payload, will return the length of content of the output buffer
    memcpy(output, input, length);
    return length;
}

int messageLength(int payloadLength)
{
    // Eventually, this will include length of HMAC, IV, and any padding
    return payloadLength;
}

