//
//  bank.c
//  bank
//
//  Created by Jeff Abbott on 10/3/15.
//  Cybersecurity Capstone: Team AI
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <math.h>
#include <ctype.h>
#include "messages.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/time.h>

//#include <openssl/rand.h>

// Prototypes
int CheckFileName(char*);

// void handler(int signal);

struct bankaccount {
    char* accountname;
    double balance;
    struct bankaccount* next;
};

struct bankaccount* findAccount(char* accountname);

struct bankaccount* addAccount(char* accountname, double amount);

// Globals
char* authfilename = NULL;
int portnum = 3000;
unsigned char cryptokey[32];
struct bankaccount* rootaccount = NULL;

int main(int argc, const char * argv[]) {
    // Validate and parse command line arguments

    int ip = 0;
    int is = 0;
    char* tempstring;
    if (argc > 5) // Incorrect number of arguments
    {
        fputs("Too many arguments\n", stderr);
        return 255;
    }
    // Set default authfilename
    authfilename = "bank.auth";
    //printf("Original Port num: %i\n", portnum);
    //printf("Authentication file: %s\n", authfilename);
    // Parse arguments
    
    for (int i = 1; i < argc; i++)
    {
        if (argv[i][0] == '-')  // Flag for arguments
        {
            char flag = argv[i][1];
            switch (flag)
            {
                case 's':
                {
                    if (is == 1)  // duplicate argument
                    {
                        fputs("Duplicate argument\n", stderr);
                        return 255;
                    }
                    is = 1;
                    if (strlen(argv[i]) > 3)
                    {
                        tempstring = argv[i];
                        tempstring = tempstring + 2;
                    }
                    else if ((i + 1) == argc)  // last argument
                    {
                        fputs("This flag can't be the last argument\n", stderr);
                        return 255;
                    }
                    else {
                        tempstring = argv[i+1];
                        i++;
                    }
                    
                    authfilename = tempstring;
                    // Validate filename format
                    if (CheckFileName(authfilename) != 0)
                    {
                        fputs("Invalid filename\n", stderr);
                        return 255;
                    }
                    break;
                }
                case 'p':
                {
                    if (ip == 1)  // duplicate argument
                    {
                        fputs("Duplicate argument\n", stderr);
                        return 255;
                    }
                    ip = 1;
                    
                    if (strlen(argv[i]) > 3)
                    {
                        tempstring = argv[i];
                        tempstring = tempstring + 2;
                    }
                    else if ((i + 1) == argc)  // last argument
                    {
                        fputs("This flag can't be the last argument\n", stderr);
                        return 255;
                    }
                    else{
                        tempstring = argv[i+1];
                        i++;
                    }
                    if (tempstring == NULL)
                        return 255;
                    portnum = atoi(tempstring);
                    if ((portnum < 1024) || (portnum > 65535))
                    {
                        fputs("Invalid port number\n", stderr);
                        return 255;
                    }
                    break;
                }
                default:  // None of the above
                {
                    fputs("Invalid arguments\n", stderr);
                    return 255;
                }
                    
            }
        }
        else  // Invalid argument set
        {
            fputs("Invalid arguments1\n", stderr);
            return 255;
        }
    }
    
    //fprintf(stderr, "Num args: %i\n", argc);
    fprintf(stderr, "New port: %i\n", portnum);
    //fprintf(stderr, "Authentication file: %s\n", authfilename);

    // Check to make sure the file doesn't already exist
    FILE *fp;
    if ((fp = fopen(authfilename, "r")) != NULL)
    {
        // file already exists
        fclose(fp);
        fputs("Authentication file already exists\n", stderr);
        return 255;
    }
    
    // Open file
    if ((fp = fopen(authfilename, "w")) == NULL)
    {
        // Unable to open auth file for writing
        fputs("Unable to open auth file for writing\n", stderr);
        return 255;
    }
    
    // Create key - random 256 bit value
    // ***** FIX - replace with Random function from SSL *****
    for (int i = 0; i < 32; i++)
    {
        cryptokey[i] = (rand() % 256);
    }
    
    // Write authentication key
    if (fwrite(cryptokey, sizeof(unsigned char), 32, fp) != 32)
    {
        fputs("Unable to write key\n", stderr);
        return 255;
    }
    
    // Close file
    fclose(fp);

    // Setup is done
    fputs("created\n", stdout);
    fflush(stdout);
    
    // signal(SIGTERM, handler);
    // Create socket
    int listenfd = 0;
    int sockfd = 0;
    struct sockaddr_in serv_addr;
    int n = 0;
    char sendBuff[1024];
    char recvBuff[1024];
    struct seqmessage message1;
    struct transmessage message2;
    struct respmessage message3;
    int length1 = sizeof(message1);
    int length2 = sizeof(message2);
    int length3 = sizeof(message3);
    unsigned long seqnum = 1;
    int length;
    int state = 0;
    
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(portnum);
    
    // Bind listener socket
    bind(listenfd, (struct sockaddr*)&serv_addr,sizeof(serv_addr));
    
    fprintf(stderr, "Listening on %i\n", (int) serv_addr.sin_port);
    if(listen(listenfd, 10) == -1){   // Maybe a protocol_error?
        fputs("Failed to listen\n", stderr);
        return 255;
    }
    
    struct timeval tv;
    tv.tv_sec = 10;
    tv.tv_usec = 0;
    //setsockopt(listenfd, SOL_SOCKET, SO_RCVTIMEO, (char*) &tv, sizeof(struct timeval));
    setsockopt(sockfd, SOL_SOCKET, SO_SNDTIMEO, (char*) &tv, sizeof(struct timeval));

    fputs("Listening\n", stderr);

    // Endless loop of listening
    while(1)
    {
	sockfd = 0;

        // Listen for connection
        sockfd = accept(listenfd, (struct sockaddr*) NULL, NULL); // accept awaiting request
	if (sockfd < 0)
	{
	    fputs("Unable to establish connection\n", stderr);
	    printf("protocol_error\n");
	    fflush(stdout);
	    break;
	}
	fputs("Got socket\n", stderr);
	fprintf(stderr, "On socket %i\n", (int)sockfd);

        // Construct and send seqmessage
        message1.seqnum = seqnum;
        seqnum++;
        fprintf(stderr, "Bank: Seq %lu\n", seqnum);
        state = 1;
        memset(sendBuff, 0, sizeof(sendBuff));
        
        if ((length = packageMessage((char*) &message1, sendBuff, length1, cryptokey)) > 0)
        {
            write(sockfd, sendBuff, length);
        }
        else
        {
            // Protocol error
            printf("protocol_error\n");
            fflush(stdout);
            state = 0;
            break;
        }
        fprintf(stderr, "Sent %i bytes, for number %lu, Waiting for response\n", length, (seqnum-1));
	// Set timeout for receive
	fputs("resetting timeout\n", stderr);
        tv.tv_sec = 10;
        setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char*) &tv, sizeof(struct timeval));

        // Listen for transmessage
        memset(recvBuff, 0, sizeof(recvBuff));
        if ((n = recv(sockfd, recvBuff, sizeof(recvBuff), 0)) < length2)
        {
            fputs("Failed to receive full trans message\n", stderr);
            printf("protocol_error\n");
            fflush(stdout);
            state = 0;
            break;
        }
        else
        {
            recvBuff[n] = 0;
            fprintf(stderr, "bank: %i bytes received of %i expected\n", n, length2);
            if(((extractMessage(recvBuff, (char*) &message2, n, cryptokey)) > 0) && (length2 == n))
            {
                // printf("Extracted transaction method with action %c\n", message2.action);
            }
            else
            {
                printf("protocol_error\n");
		fflush(stdout);
                state = 0;
		break;
            }
        }
        
        // Check seqnum
        if(message2.seqnum != message1.seqnum)
        {
            printf("protocol_error\n");
            state = 0;
	    fflush(stdout);
            break;
        }
        
        // Process transaction message
        struct bankaccount *thisaccount = NULL;
        switch (message2.action)
        {
            case 'n':
            {
                // fprintf(stderr, "New account: %s\n", message2.account);
                if (findAccount(message2.account) == NULL)
                {
                    thisaccount = addAccount(message2.account, message2.amount);
                    message3.passed = 'Y';
                    message3.amount = message2.amount;
                    //message3.decamount = message2.decamount;
                    // Print result
                    printf("{\"account\":\"%s\",\"initial_balance\":%.2f}\n", thisaccount->accountname, thisaccount->balance);
		    fflush(stdout);
                }
                else
                {
                    message3.passed = 'N';
                    message3.amount = 0;
                }
                break;
            }
            case 'd':
            {
                // fputs("Deposit\n", stderr);
                if ((thisaccount =findAccount(message2.account)) == NULL)
                {
                    message3.passed = 'N';
                    message3.amount = 0;
                }
                else
                {
                    message3.passed = 'Y';
                    thisaccount->balance = thisaccount->balance + message2.amount;
                    message3.amount = thisaccount->balance;
                    // Print result
                    printf("{\"account\":\"%s\",\"deposit\":%.2f}\n", thisaccount->accountname, message2.amount);
		    fflush(stdout);
                }
                break;
            }
            case 'w':
            {
                // fputs("Withdrawal\n", stderr);
                if ((thisaccount =findAccount(message2.account)) == NULL)
                {
                    message3.passed = 'N';
                    message3.amount = 0;
                    //message3.decamount = 0;
                }
                else
                {
                    if ((thisaccount->balance + 0.001) >= message2.amount)
                    {
                        message3.passed = 'Y';
                        thisaccount->balance = thisaccount->balance - message2.amount;
                        message3.amount = thisaccount->balance;
                        //message3.decamount = thisaccount->decbalance;
                        // Print result
                        printf("{\"account\":\"%s\",\"withdraw\":%.2f}\n", thisaccount->accountname, message2.amount);
			fflush(stdout);
                    }
                    else
                    {
                        fputs("Not enough money in account\n", stderr);
                        message3.passed = 'N';
                        message3.amount = 0;
                        //message3.decamount = 0;
                    }
                }
                break;
            }
            case 'g':
            {
                // fputs("Get balance\n", stderr);
                if ((thisaccount =findAccount(message2.account)) == NULL)
                {
                    message3.passed = 'N';
                    message3.amount = 0;
                    //message3.decamount = 0;
                }
                else
                {
                    message3.passed = 'Y';
                    message3.amount = thisaccount->balance;
                    //message3.decamount = thisaccount->decbalance;
                    //fprintf(stderr, "Balance: %lu.%i\n", thisaccount->balance, thisaccount->decbalance);
                    // Print result
                    printf("{\"account\":\"%s\",\"balance\":%.2f}\n", thisaccount->accountname, message3.amount);
		    fflush(stdout);
                }
                break;
            }
            default:  // None of the above
            {
                printf("protocol_error\n");
                state = 0;
		fflush(stdout);
                break;
            }
        }
        
        // Send response
        if (state == 1)
        {
            message3.seqnum = message2.seqnum;
            memset(sendBuff, 0, sizeof(sendBuff));
            if ((length = packageMessage((char*) &message3, sendBuff, length3, cryptokey)) > 0)
            {
                write(sockfd, sendBuff, length);
            }
            else
            {
                // Protocol error
                printf("protocol_error\n");
		fflush(stdout);
            }
            //printf("Sent %i bytes, for number %lu, confirming tranaction\n", length, (seqnum-1));
        }
        state = 0;
        
    }
    
    return 0;
}



int CheckFileName(char* FileName)
{
    int len = strlen(FileName);
    if (len > 255)  // Filename too long
    {
        fputs("Filename too long\n", stderr);
        return 255;
    }
    
    // Check each character
    for (int i = 0; i< len; i++)
    {
        char c = FileName[i];
        if (isdigit(c) || islower(c) || (c == '_') || (c == '-') || (c == '.'))
        {
            // Good char
        }
        else{
            fputs("Invalid character\n", stderr);
            return 255;
        }
    }
    // Make sure not . or ..
    if ((strcmp(".", FileName) == 0) || (strcmp("..", FileName) == 0))
    {
        // "." and ".." are not allowed
        fputs("Invalid filename\n", stderr);
        return 255;
    }
    return 0;
}
/*
void handler(int signal)
{
    exit(0);
}
*/
struct bankaccount* findAccount(char* accountname)
{
    struct bankaccount* current = rootaccount;
    while (current != NULL)
    {
        if (strcmp(accountname, current->accountname) == 0)
            break;
        current = current->next;
    }
    return current;
}

struct bankaccount* addAccount(char* accountname, double amount)
{
    struct bankaccount* newAccount;
    newAccount = malloc(sizeof(struct bankaccount));
    newAccount->accountname = malloc(strlen(accountname));
    strcpy(newAccount->accountname, accountname);
    newAccount->balance = amount;
    newAccount->next = rootaccount;
    rootaccount = newAccount;
    return newAccount;
}

