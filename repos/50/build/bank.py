#!/usr/bin/env python

import os, sys, signal, getopt, re, ntpath, socket, json, time
from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Hash import HMAC, SHA256

def signal_handler(signal, frame):
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

opts, args = getopt.getopt(sys.argv[1:],'p:s:')

IP = '127.0.0.1'
port = '3000'
auth = 'bank.auth'

CREATE = 1
DEPOSIT = 2
WITHDRAW = 3
CHECK = 4

db = {}

for opt, arg in opts:
    if opt == '-p':
        port = arg
    elif opt == '-s':
        auth = arg

##########################################################
# Input Validation
# Validate port number
portRegex = re.compile('^[0-9]+$')
match = portRegex.match(port)

if not match:
    sys.exit(255)

port = int(port)

if port < 1024 or port > 65535:
    sys.exit(255)

# Validate authentication file name
if auth == '.' or auth == '..':
    sys.exit(255)

authRegex = re.compile('^[_\-\.0-9a-z]+$')
match = authRegex.match(auth)

if not match:
    sys.exit(255)

##########################################################
# Create auth file
auth_fh = open(auth, 'w')
keyEnc = os.urandom(16)
keyHash = os.urandom(16)

auth_fh.write(keyEnc + keyHash)
auth_fh.close()
print('created')
sys.stdout.flush()

##########################################################
# Start listening
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
while True:
    try:
        s.bind((IP, port))
        break
    except:
        continue
socket.setdefaulttimeout(10)
s.listen(1)

while True:
    result = '{}'
    timeout = 0
    msg = ''
    conn = None

    try:
        (conn, addr) = s.accept()
        msg = conn.recv(1024)
    except socket.timeout:
        timeout = 1
        result = 'protocol_error'

    if result != 'protocol_error':
        if len(msg) < 80:
            result = 'protocol_error'
        else:
            tag = msg[:64]
            ciphertext = msg[64:]

            # Decrypt
            iv = ciphertext[:16]

            if len(iv) < 16:
                result = 'protocol_error'
            else:
                cipher = AES.new(keyEnc, AES.MODE_CBC, iv)
                padded = cipher.decrypt(ciphertext)[16:]
                pbytes = ord(padded[-1])
                plaintext = padded[:-pbytes]

                h = HMAC.new(keyHash, msg[64:], SHA256.new())

                if h.hexdigest() != tag:
                    result = 'protocol_error'

    if result != 'protocol_error':
        ##########################################################
        # Process transaction
        data = json.loads(plaintext)
        ts = data[0]
        account = data[1]
        pin = data[2]
        mode = data[3]
        amount = data[4]
        now = time.time()

        if now - ts > 10:
            result = 'protocol_error'
        else:
            #####################
            # MODE: Create
            if mode == CREATE and not account in db and amount >= 10:
                db[account] = (amount, ts)
                result = '{"account":"%s","initial_balance":%.2f}' % (account, amount)

            # All other modes of operation
            elif db.has_key(account) and ts > db[account][1]:
                h = SHA256.new()
                h.update(account)

                if h.hexdigest() == pin:
                    #####################
                    # MODE: Deposit
                    if mode == DEPOSIT and amount > 0:
                        db[account] = (db[account][0] + amount, ts)
                        result = '{"account":"%s","deposit":%.2f}' % (account, amount)

                    #####################
                    # MODE: Withdraw
                    elif mode == WITHDRAW and amount > 0 and (amount < db[account][0] or abs(amount - db[account][0]) < 0.00001):
                        db[account] = (abs(db[account][0] - amount), ts)
                        result = '{"account":"%s","withdraw":%.2f}' % (account, amount)

                    #####################
                    # MODE: Check Balance
                    elif mode == CHECK:
                        db[account] = (db[account][0], ts)
                        result = '{"account":"%s","balance":%.2f}' % (account, db[account][0])

    print(result)
    sys.stdout.flush()

    if conn:
        # Pad data
        l = 16 - (len(result) % 16)
        result += chr(l) * l

        # Encrypt
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(keyEnc, AES.MODE_CBC, iv)
        c = iv + cipher.encrypt(result)

        # Hash
        h = HMAC.new(keyHash, c, SHA256.new())

        payload =  h.hexdigest() + c

        conn.send(payload)
        conn.close()

