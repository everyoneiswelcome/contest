#!/usr/bin/env python

import os, sys, getopt, re, ntpath, socket, json, time
from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Hash import HMAC, SHA256

opts, args = getopt.getopt(sys.argv[1:],"a:s:i:p:c:n:d:w:g")

NONE = 0
CREATE = 1
DEPOSIT = 2
WITHDRAW = 3
CHECK = 4


account = mode = initial = deposit = withdraw = card = NONE
auth = 'bank.auth'
ip = '127.0.0.1'
port = '3000'

used = {'-a': 0, '-s': 0,'-i': 0,'-p': 0,'-c': 0,'-n': 0,'-d': 0,'-w': 0,'-g': 0}
modeExists = 0

for opt, arg in opts:
    if used[opt]:
        sys.exit(255)
    if opt == '-a':
        account = arg
    elif opt == '-s':
        auth = arg
    elif opt == '-i':
        ip = arg
    elif opt == '-p':
        port = arg
    elif opt == '-c':
        card = arg
    else:
        if modeExists:
            sys.exit(255)
        if opt == '-n':
            mode = CREATE
            initial = arg
        elif opt == '-d':
            mode = DEPOSIT
            deposit = arg
        elif opt == '-w':
            mode = WITHDRAW
            withdraw = arg
        elif opt == '-g':
            if (arg != None and len(arg) != 0):
                sys.exit(255)
            mode = CHECK
        else:
            continue
        modeExists = 1

    used[opt] = 1

##########################################################
# Input Validation
if mode == NONE or (mode == CHECK and args):
    sys.exit(255)
    
# Validate account name
if account == NONE:
    sys.exit(255)

accNameLen = len(account)

if accNameLen > 250:
    sys.exit(255)
    
accNameRegex = re.compile('^[_\-\.0-9a-z]+$')

match = accNameRegex.match(account)
if not match:
    sys.exit(255)

# Validate authentication file name
if not os.path.isfile(auth):
    sys.exit(255)

authBasename = ntpath.basename(auth)
authLen = len(authBasename)

if authLen > 255:
    sys.exit(255)

if authBasename == '.' or authBasename == '..':
    sys.exit(255)

match = accNameRegex.match(authBasename)

if not match:
    sys.exit(255)

# Validate IP address
ipRegex = re.compile('^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$')
match = ipRegex.match(ip)

if not match:
    sys.exit(255)

# Validate port number
portRegex = re.compile('^[1-9]{1}[0-9]*$')
match = portRegex.match(port)

if not match:
    sys.exit(255)

port = int(port)
if port < 1024 or port > 65535:
    sys.exit(255)

##########################################################
# Retrieve keys
auth_fh = open(auth, 'r')
keys = auth_fh.read()
auth_fh.close()

if len(keys) != 32:
    sys.exit(255)

keyEnc = keys[:16]
keyHash = keys[16:]

##########################################################
# Process Mode of Operation
pin = None
dollarRegex = re.compile('^(0(?=\.)|[1-9]){1}[0-9]*\.[0-9]{2}$')

# MODE: Create new account with initial balance.  
if mode == CREATE:  #(0(?=\.)|[1-9]){1}[0-9]*\.[0-9]{2}
    match = dollarRegex.match(initial)

    if match:
        initial = float(initial)

        if initial < 10 or initial > 4294967295.99:
            sys.exit(255)
    else:
        sys.exit(255)

    amount = initial
 
    if card == NONE:
        card = account + '.card'
    else:
        match = accNameRegex.match(card)

        if not match:
            sys.exit(255)

    if os.path.isfile(card) or card == '.' or card == '..' or len(card) > 255:
        sys.exit(255)

    # All other modes of operation
else:
    # Validate card file name
    if card == NONE:
        card = cardBasename = account + '.card'

        if not os.path.isfile(card):
            sys.exit(255)

    elif not os.path.isfile(card):
        sys.exit(255)
    else:
        cardBasename = ntpath.basename(card)

    match = accNameRegex.match(cardBasename)

    if not match:
        sys.exit(255)

    # Verify the card file belongs to the user
    card_fh = open(card, 'r')
    pin = card_fh.read()
    h = SHA256.new()
    h.update(account)

    if h.hexdigest() != pin:
        sys.exit(255)

    # MODE: Make a deposit
    if mode == DEPOSIT:
        match = dollarRegex.match(deposit)

        if match:
            deposit = float(deposit)
            
            if deposit <= 0 or deposit > 4294967295.99:
                sys.exit(255)
        else:
            sys.exit(255)

        amount = deposit
 
    # MODE: Withdraw an amount
    elif mode == WITHDRAW:
        match = dollarRegex.match(withdraw)

        if match:
            withdraw = float(withdraw)

            if withdraw <= 0 or withdraw > 4294967295.99:
                sys.exit(255)
        else:
            sys.exit(255)
 
        amount = withdraw

    # MODE: Check balance
    elif mode == CHECK:
        amount = 0
    else:
        sys.exit(255)

##########################################################
# Process transaction
ts = time.time()
data = json.dumps([ts, account, pin, mode, amount])

# Pad data
l = 16 - (len(data) % 16)
data += chr(l) * l

# Encrypt
iv = Random.new().read(AES.block_size)
cipher = AES.new(keyEnc, AES.MODE_CBC, iv)
c = iv + cipher.encrypt(data)

# Hash
h = HMAC.new(keyHash, c, SHA256.new())

payload =  h.hexdigest() + c

##########################################################
# Send payload
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True:
    try:
        s.connect((ip, port))
        break
    except:
        continue
s.settimeout(10)
try:
    s.send(payload)
    response = s.recv(1024)

except socket.timeout:
    sys.exit(63)

s.close()

if len(response) < 80:
    sys.exit(63)

tag = response[:64]
ciphertext = response[64:]

# Decrypt
iv = ciphertext[:16]

if len(iv) < 16:
    sys.exit(63)

cipher = AES.new(keyEnc, AES.MODE_CBC, iv)
padded = cipher.decrypt(ciphertext)[16:]
pbytes = ord(padded[-1])
plaintext = padded[:-pbytes]

# Verify tag
h = HMAC.new(keyHash, response[64:], SHA256.new())

if h.hexdigest() != tag:
    sys.exit(63)

if plaintext == '{}':
    sys.exit(255)
elif plaintext == 'protocol_error':
    sys.exit(63)

if mode == CREATE:
    card_fh = open(card, 'w')
    h = SHA256.new()
    h.update(account)
    card_fh.write(h.hexdigest())
    card_fh.close()

print(plaintext)
sys.stdout.flush()
sys.exit(0)
