// everything about card files

#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <openssl/rand.h>
#include "global.h"
#include "atm.h"
#include "card.h"


char accname[MAXACCNAMELEN+1];  // account name
char cardname[MAXFILENAMELEN+1];  // card file name


extern int Cardindex;   
extern int Accountindex;  



inline int initcard(){
	struct stat st;
	int result;
	FILE *fp;
	

	//create Card record file if not exist

	if (stat(CARDRECORDNAME, &st)!=0){

	    fp=fopen(CARDRECORDNAME, "w");
    	    if (fp==NULL) return ERROR;
   
	    fclose(fp);

	}

	return SUCCESS;
}



// find indice of Account name and Card name from record file 
inline int findindice(){

	FILE *fp;

	fp = fopen(CARDRECORDNAME,"r");
 	if (fp==NULL) return ERROR;

	char line_buffer[BUFSIZ]; /* BUFSIZ is defined in stdio.h */ 
	int i = -1;
 
	while (fgets(line_buffer, sizeof(line_buffer), fp)) { 
		i++;
		sscanf(line_buffer,"%s %s", accname, cardname);	

    		if ( ( strlen(accname) == strlen(Accountname) ) && ( strcmp(accname,Accountname) ==0 ) ) 
			Accountindex=i;

    		if ( ( strlen(cardname) == strlen(Cardfile) ) && ( strcmp(cardname,Cardfile) ==0 ) ) 
			Cardindex=i;
	
		if ((Accountindex!=-1)&&(Cardindex!=-1)) {fclose(fp); return SUCCESS;}
	} 	

	fclose(fp);
	return ERROR;
}




// create a card file 
inline int create_cardfile(char *filename){


    //write to cardfile
    FILE *fp;


    //fprintf(stderr,"write to: %s\n",filename);

    fp=fopen(filename, "w");
    if (fp==NULL) return ERROR;

    fclose(fp);
    return SUCCESS;

}



// create the correct file name for a card file
inline int createCardname(){

  if (Cardfile[0]==0){

     strcat(Cardfile,Accountname);
     strcat(Cardfile,".card");
  }
  return SUCCESS;
}



// write an entry to the record file. An entry contains account name, card file name
inline int createcardrecord(){
  int index;

 
  FILE *fp;

  fp = fopen(CARDRECORDNAME,"a");
  if (fp==NULL) {fclose(fp); return ERROR;}

  if (create_cardfile(Cardfile)!=SUCCESS) {fclose(fp); return ERROR;}

  if (fprintf(fp,"%s %s\n",Accountname,Cardfile)<0) {fclose(fp); return ERROR;}

  fclose(fp);
  return SUCCESS;
}













