#include "global.h"


#define OPTION "ps"
#define NUMOPTION  2
#define MAXNUMARGC  5 //max argc
#define MINNUMARGC  1 //min argc

#define PORTNUM 0
#define AUTHFILE 1

char Authfile[MAXFILENAMELEN+1];
int Portnum;

unsigned char Auth_id[AUTH_IDBYTESIZE];



// messsage string for sending to/from bank and atm
char Msgstr[PROTOCOLMSGLEN];



// parsed command line options and parameters are stored here
char BParameter[NUMOPTION][MAXLEN];
char BParameterpresent[NUMOPTION];
char BOptionpresent[NUMOPTION];



// records for all customers
char recordname[MAXCUSTOMER][MAXFILENAMELEN+1];
long int recorddollar[MAXCUSTOMER];
int recordcent[MAXCUSTOMER];
long int recorddollarbackup[MAXCUSTOMER];
int recordcentbackup[MAXCUSTOMER];



struct money {
  long int dollar;
  int cent; // -1 is error
};



// one transaction containing operation and $ amount
struct banktransdata {
  char dowhat;  // operation: n - newaccount, w - withdraw, d - deposit, g - getbalance
  char account[MAXACCNAMELEN+1];  //account name
  long int dollar;
  int cent;  
  char valid;  // 0 is valid 
};
  


struct banktransdata Trans;

