To compile:

	make


To run:

  Bank Examples (run in background):

	bank &
	bank -p 3000 -a certificate_name &
 
  ATM Examples: 

	atm -a name1 -n 100.00   // new account
	atm -a name1 -d 20.50    // deposit
	atm -a name1 -g	         // get balance



After each run, must remove these files:

	rm bank.auth
	rm *.card
	rm Card-file
	rm ..card
	rm ...card
	rm *.auth


To debug:

  Do this before compile:

  	sed -i 's/gcc /gcc -g /g' Makefile
	sed -i 's/\/\/fprintf(stderr/fprintf(stderr/g' *.c	


To turn off debug:

  Do this before compile:

  	sed -i 's/gcc -g /gcc /g' Makefile
	sed -i 's/fprintf(stderr/\/\/fprintf(stderr/g' *.c	
