// openssl server program

#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include "openssl/ssl.h"
#include "openssl/err.h"
#include <stdio.h>
#include <stdlib.h>
#include "bank.h"

 
#define FAIL    -1
 

extern char* handlerequest(char *str1);




//misc functions for server

int OpenListener(int port)
{   int sd;
    struct sockaddr_in addr;
 
    sd = socket(PF_INET, SOCK_STREAM, 0);
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;


   //set socket timtout
    struct timeval tv;

    tv.tv_sec = 10;  
    tv.tv_usec = 0;  // Not init'ing this can cause strange errors

    if (setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval))<0)
	return ERROR;
  	//fprintf(stderr,"server set timeout to 10 sec \n");


    //allow reuse socket
    int yes=1;  // or int yes='1'; 
    if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) 
 	return ERROR;
	//fprintf(stderr,"server set to re-use \n");



    //try to bind until success
    int tmp=0;
    while ( bind(sd, (struct sockaddr*)&addr, sizeof(addr)) != 0 )
    {

	tmp++;
	//fprintf(stderr,"Server retry bind %d\n",tmp);

	sleep(1);

    }

    while ( listen(sd, 10) != 0 )
    {

	tmp++;
	//fprintf(stderr,"Server retry listen %d\n",tmp);

    }
    return sd;
}
 



//misc functions for server

SSL_CTX* InitServerCTX(void)
{   const SSL_METHOD *method;
    SSL_CTX *ctx;
 
    OpenSSL_add_all_algorithms();  /* load & register all cryptos, etc. */
    SSL_load_error_strings();   /* load all error messages */
    method = SSLv3_server_method();  /* create new server-method instance */
    ctx = SSL_CTX_new(method);   /* create new context from method */
    if ( ctx == NULL )
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    return ctx;
}
 





//misc functions for server

void LoadCertificates(SSL_CTX* ctx, char* CertFile, char* KeyFile)
{
    /* set the local certificate from CertFile */
    if ( SSL_CTX_use_certificate_file(ctx, CertFile, SSL_FILETYPE_PEM) <= 0 )
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    /* set the private key from KeyFile (may be the same as CertFile) */
    if ( SSL_CTX_use_PrivateKey_file(ctx, KeyFile, SSL_FILETYPE_PEM) <= 0 )
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    /* verify private key */
    if ( !SSL_CTX_check_private_key(ctx) )
    {
        //fprintf(stderr, "Private key does not match the public certificate\n");
        abort();
    }
}
 





//misc functions for server
//one set of things to do: receive a message, send a result.

int Servlet(SSL* ssl) /* Serve the connection -- threadable */
{   

  	     char buffer[PROTOCOLMSGLEN];


    	     int sd, n;



    	     if ( SSL_accept(ssl) == FAIL )   
             	 { sd = SSL_get_fd(ssl); SSL_free(ssl);  close(sd); return PROTOCOL_ERROR;}

	     //fprintf(stderr,"Servlet receiving\n");

	     // read command. timeout after 10 sec
	     bzero(buffer,PROTOCOLMSGLEN);
	     n = SSL_read(ssl, buffer,PROTOCOLMSGLEN);

	     if (n < 0) {  
		sd = SSL_get_fd(ssl); SSL_free(ssl);  close(sd); return PROTOCOL_ERROR;
	     }

	     buffer[n] = 0;

	     //fprintf(stderr,"Server Received command: %s\n",buffer);
 
	     // handle request. copy result string to Mststr
	     strncpy(Msgstr,handlerequest(buffer),PROTOCOLMSGLEN);

	     //fprintf(stderr,"Server prepared Msgstr: %s\n",Msgstr);

	     // if only still alive msg. done. quit
	     if (!strcmp(Msgstr,STILLALIVE)) {
		sd = SSL_get_fd(ssl); SSL_free(ssl);  close(sd); return SUCCESS;
	     }


	     // if parse error
	     if (!strcmp(Msgstr,PARSE_ERROR)) {
		//fprintf(stderr,"Server says cannot parse msg\n");
		sd = SSL_get_fd(ssl); SSL_free(ssl);  close(sd); return PROTOCOL_ERROR;
	     }


	     // send msg to atm
	     n = SSL_write(ssl, Msgstr,PROTOCOLMSGLEN);  

	     if (n < 0) {  
		sd = SSL_get_fd(ssl); SSL_free(ssl);  close(sd); return PROTOCOL_ERROR;
	     }

		//fprintf(stderr,"Server sent result: %s\n",Msgstr);


	     // if bad transaction, quit

	     if (!strcmp(Msgstr,TRANSACTION_FAIL)) {

		//fprintf(stderr,"Server says bad operation\n"); 

	     	sd = SSL_get_fd(ssl); SSL_free(ssl);  close(sd); return TRANSACTION_ERROR;
	     }




	    // success
	    printf("%s",Msgstr); fflush(stdout);





	    // done. quit
	    sd = SSL_get_fd(ssl);       
	    SSL_free(ssl);         
	    close(sd);      

	    return SUCCESS;   
}
 



// server starts here. loops forever. starts a servlet for each set of things to do

int startserver(int portnum){

    SSL_CTX *ctx;
    int server;
  
    int result;

    SSL_library_init();
 
    ctx = InitServerCTX();        

    LoadCertificates(ctx, DEFAULT_CERTFILE, DEFAULT_CERTFILE); 

    //fprintf(stderr,"Server loaded Certificate\n");


    SSL_CTX_set_timeout(ctx, 10);

    server = OpenListener(portnum);    

    //fprintf(stderr,"Server opened listener\n");

    while (1){   

	struct sockaddr_in addr;
        socklen_t len = sizeof(addr);
        SSL *ssl;
 
        int client = accept(server, (struct sockaddr*)&addr, &len);  /* accept connection as usual */

   	//fprintf(stderr,"Server accepting: %s:%d\n",inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));

        ssl = SSL_new(ctx);              /* get new SSL state with context */
        SSL_set_fd(ssl, client);      /* set connection socket to SSL state */
        result = Servlet(ssl);         /* service connection */

	if (result==PROTOCOL_ERROR){
	    printf("protocol_error\n"); fflush(stdout); 
	}


	//fprintf(stderr,"Servlet exit result: %d\n",result);

    }



    close(server);          /* close server socket */
    SSL_CTX_free(ctx);         /* release context */


    return SUCCESS;
}







