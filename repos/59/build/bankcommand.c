// this file is for the bank to parse the command received from atm

#include <stdio.h>
#include <string.h>
#include "bank.h"



extern struct money getbalance(char* str1);
extern char* printoutput(char *name, long int dollar, int cent, int operation);




// the command sent from atm looks like this:  #peter#n#10.10# - open new account for Peter with $10.10
// parse and check this command for error. return a structure with what operation, dollar and cent.
// if parsing error, the return structure contains cent = -1 
inline struct banktransdata parsebankcommand(char *str1){

  struct banktransdata trans;
  memset(&trans,0,sizeof(trans));
  trans.valid = PROTOCOL_ERROR;

  // if message is "still alive", ignore
  if (!strcmp(str1,STILLALIVE)){
	trans.valid=STILLALIVEMODE;  //still alive code
	return trans;
  }

  //fprintf(stderr,"server about to check auth key\n");

  // extract and check auth key
  unsigned char auth_id[AUTH_IDBYTESIZE];
  memcpy(auth_id,str1,AUTH_IDBYTESIZE);

  int i=0;
  while (i<AUTH_IDBYTESIZE){
	if (auth_id[i]!=Auth_id[i]) {  //wrong auth id
	    trans.valid=STILLALIVEMODE;  //just ignore it like still alive 
	    return trans;
  	}
	i++;
  }

  //fprintf(stderr,"server checked auth key ok\n");

  // extract and check other fields
  char end=1;
  sscanf(str1+AUTH_IDBYTESIZE,"#%s %c %ld %d%c", trans.account, &trans.dowhat, &trans.dollar, &trans.cent, &end);
  if (end!='#') return trans;

  //fprintf(stderr,"server scanned %s %c %ld %d %s\n",trans.account, trans.dowhat, trans.dollar, trans.cent, auth_id);




  if ((trans.dowhat!='n')&&(trans.dowhat!='d')&&(trans.dowhat!='w')&&(trans.dowhat!='g')) return trans;
  if ((trans.dollar>MAXDOLLAR)||(trans.dollar<0)) return trans;
  if ((trans.cent>99)||(trans.cent<0)) return trans;

//  if (checkaccname(trans.account)!=SUCCESS) return trans;


  //check account name ok
  char c;
  int nlen = strlen(trans.account);
  if ((nlen<1)||(nlen>MAXACCNAMELEN)) return trans;
  i=0;
  while ( (c=trans.account[i]) !=0 ){ 
		if (c<'-') return trans;
		if ((c>'.')&&(c<'0')) return trans;
		if ((c>'9')&&(c<'_')) return trans;
		if ((c>'_')&&(c<'a')) return trans;
		if (c>'z') return trans;
		i++;
  }

  trans.valid = SUCCESS;

  return trans;

}



// starting with a message string. parse into a structure containing operaion and $ amoung.
// do the operation. return a message string containing the result
// final message is in global var Msgstr. it may say error. it may be correct result.
inline char* handlerequest(char *str1){

  //start parsing
  ////fprintf(stderr,"Server start parsing \n");
  Trans = parsebankcommand(str1);

  if (Trans.valid==STILLALIVEMODE) return STILLALIVE;

  if (Trans.valid!=SUCCESS) return PARSE_ERROR;



  // no parsing error. do the operation
  struct money amount;
  int result;

  if (Trans.dowhat=='g'){
	amount = getbalance(Trans.account); 
	if (amount.cent!=-1)
	   return printoutput(Trans.account, amount.dollar, amount.cent, OP_GETBALANCE);
	else return TRANSACTION_FAIL;
  }

  if (Trans.dowhat == 'n'){
	if (newaccount(Trans.account,Trans.dollar,Trans.cent)==SUCCESS)	
	   return printoutput(Trans.account, Trans.dollar, Trans.cent, OP_NEWACCOUNT);
	else return TRANSACTION_FAIL;
  }

  if (Trans.dowhat == 'd'){
	if (deposit(Trans.account,Trans.dollar,Trans.cent)==SUCCESS)	
	   return printoutput(Trans.account, Trans.dollar, Trans.cent, OP_DEPOSIT);
	else return TRANSACTION_FAIL;
  }

  if (Trans.dowhat == 'w'){
	if (withdraw(Trans.account,Trans.dollar,Trans.cent)==SUCCESS)	
	   return printoutput(Trans.account, Trans.dollar, Trans.cent, OP_WITHDRAW);
	else return TRANSACTION_FAIL;
  }
}



// not used.
// if something goes wrong, roll back transaction.
inline int handlerollback(){

	return rollback(Trans);

}


