// main bank program

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include "bank.h"





// main program
int main(int argc, char* argv[]){

//  bankinit();  
	int i;

	for (i=0;i<MAXCUSTOMER;i++){
		memset(recordname[i],0,MAXFILENAMELEN+1);
		recorddollar[i]=0;
		recordcent[i]=0;
		recorddollarbackup[i]=0;
		recordcentbackup[i]=0;
	}

  	memset(Authfile,0,sizeof(Authfile));
	strncpy(Authfile,DEFAULT_AUTHFILE,sizeof(Authfile));
	Portnum=3000;





  int nlen,result;
  char c;

  //parse options
//  result = parse_bank_option(argc,argv);
//  if (result!=SUCCESS) return ERROR;


  if (argc>1) {

    // special parser will fill the arrays BParameter etc
    if (special_parser(argc,argv, OPTION,NUMOPTION,MAXNUMARGC, MINNUMARGC, BParameter,BParameterpresent, BOptionpresent) !=SUCCESS) return ERROR;



    //check and set auth file
    if (BOptionpresent[AUTHFILE]==1){
	  if (BParameterpresent[AUTHFILE]==0) return ERROR;
	  if (BParameter[AUTHFILE][0]==0) return ERROR;

	  nlen = strlen(BParameter[AUTHFILE]);
	  if ((nlen<1)||(nlen>MAXFILENAMELEN)) return ERROR;
	  i=0;
  	  while ( (c=BParameter[AUTHFILE][i]) !=0 ){ 
		if (c<'-') return ERROR;
		if ((c>'.')&&(c<'0')) return ERROR;
		if ((c>'9')&&(c<'_')) return ERROR;
		if ((c>'_')&&(c<'a')) return ERROR;
		if (c>'z') return ERROR;
		i++;
	  }

	  if (BParameter[AUTHFILE][0]=='.'){
		if (BParameter[AUTHFILE][1]==0) return ERROR;
		if ((BParameter[AUTHFILE][1]=='.')&&(BParameter[AUTHFILE][2]==0)) return ERROR;
	  }

  	  strcpy(Authfile,BParameter[AUTHFILE]);
    }  




    // check and set port num
    if (BOptionpresent[PORTNUM]==1){
	  if (BParameterpresent[PORTNUM]==0) return ERROR;
	  if (BParameter[PORTNUM][0]==0) return ERROR;
	  if (BParameter[PORTNUM][0]=='0') return ERROR;
	  i=0;
	  while ( (c=BParameter[PORTNUM][i]) !=0 ){ 
		if ( (c<'0') || (c>'9') ) return ERROR;
		i++;
	  }
	  int portnum = atoi(BParameter[PORTNUM]);
	  if ((portnum<1024)||(portnum>65535)) return ERROR;
	  Portnum = portnum;
    }

    //fprintf(stderr,"server parsed Option:  %s %d\n",Authfile,Portnum);

  }





  //create certificate file - openssl certificate
  if (create_certfile(DEFAULT_CERTFILE)!=SUCCESS) return ERROR;

  //create authentication file 
  if (create_authfile(Authfile,Auth_id)!=SUCCESS) return ERROR;
  printf("created\n");fflush(stdout);
  //fprintf(stderr,"server created auth file\n");

  //start server
  result = startserver(Portnum);
  //fprintf(stderr,"server exit %d\n",result);

  return result;
}





