#include <stdio.h>
#include <string.h>
#include "atm.h"


// main file for parsing the atm program command line options.



// parse the atm program command line options. check for errors. 
inline int parse_atm_command(int num1, char *str1[]){


  // special parser will fill the arrays Parameter etc
  special_parser(num1, str1,OPTION,NUMOPTION,MAXNUMARGC,MINNUMARGC,Parameter,Parameterpresent,Optionpresent);


  // check that only one operation is present, set global var Operation
  int j=0;
  int i;
  for (i=0;i<4;i++){  //the first 4 available options are the possible operations
	if (Optionpresent[i]==1) {
	  Operation = OPTION[i];
	  j++;
	}
  }
  if (j!=1) return ERROR;



  // check operation parameter is/not present
  if ( (Operation=='n') && (Parameterpresent[NEWACCOUNT]==0) ) return ERROR;
  else if ( (Operation=='d') && (Parameterpresent[DEPOSIT]==0) ) return ERROR;
  else if ( (Operation=='w') && (Parameterpresent[WITHDRAW]==0) ) return ERROR;
  else if ( Parameterpresent[GETBALANCE]==1)  return ERROR;



  char c; unsigned char end = 0; int portnum=3000;

  // check  Portnum. don't set yet.
  if (Optionpresent[PORTNUM]==1)  {
	  if (Parameterpresent[PORTNUM]==0) return ERROR;
	  if (Parameter[PORTNUM][0]==0) return ERROR;
	  if (Parameter[PORTNUM][0]=='0') return ERROR;
	  i=0;
	  while ( (c=Parameter[PORTNUM][i]) !=0 ){ 
		if ( (c<'0') || (c>'9') ) return ERROR;
		i++;
	  }
	  portnum = atoi(Parameter[PORTNUM]);
	  if ((portnum<1024)||(portnum>65535)) return ERROR;
//	  Portnum = portnum;
  }


  // check ip address. don't set yet.
  if (Optionpresent[IPADDRESS]==1)  {
	  if (Parameterpresent[IPADDRESS]==0) return ERROR;
	  if (Parameter[IPADDRESS][0]==0) return ERROR;
	  if (Parameter[IPADDRESS][0]=='0') return ERROR;

	  int n1=-1; int n2=-1; int n3=-1; int n4=-1; 

	  sscanf( Parameter[IPADDRESS], "%d.%d.%d.%d.%c", &n1,&n2,&n3,&n4,&end);
	  if (end!=0) return ERROR;
	  if ( (n1<0) || (n1>255) ) return ERROR;
	  if ( (n2<0) || (n2>255) ) return ERROR;
	  if ( (n3<0) || (n3>255) ) return ERROR;
	  if ( (n4<0) || (n4>255) ) return ERROR;

	  int d1=1, d2=1, d3=1, d4=1;
	  if (n1>100) d1=3;
	  else if (n1>10) d1=2;
	  if (n2>100) d2=3;
	  else if (n2>10) d2=2;
	  if (n3>100) d3=3;
	  else if (n3>10) d3=2;
	  if (n4>100) d4=3;
	  else if (n4>10) d4=2;

	  if ( (d1+d2+d3+d4+3) != strlen( Parameter[IPADDRESS]) ) return ERROR;
//	  strcpy(Ipaddress,Parameter[IPADDRESS]);
  }



  // check and set account name
  if ((Optionpresent[ACCOUNTNAME]==0)||(Parameterpresent[ACCOUNTNAME]==0)) return ERROR;
  if (Parameter[ACCOUNTNAME][0]==0) return ERROR;
  int nlen = strlen(Parameter[ACCOUNTNAME]);
  if ((nlen<1)||(nlen>MAXACCNAMELEN)) return ERROR;
  i=0;
  while ( (c=Parameter[ACCOUNTNAME][i]) !=0 ){ 
	if (c<'-') return ERROR;
	if ((c>'.')&&(c<'0')) return ERROR;
	if ((c>'9')&&(c<'_')) return ERROR;
	if ((c>'_')&&(c<'a')) return ERROR;
	if (c>'z') return ERROR;
	i++;
  }

  strcpy(Accountname,Parameter[ACCOUNTNAME]);



  // check and set card file
  if (Optionpresent[CARDFILE]==1)  {
	  if (Parameterpresent[CARDFILE]==0) return ERROR;
	  if (Parameter[CARDFILE][0]==0) return ERROR;
	  nlen = strlen(Parameter[CARDFILE]);
	  if ((nlen<1)||(nlen>MAXFILENAMELEN)) return ERROR;
	  i=0;
  	  while ( (c=Parameter[CARDFILE][i]) !=0 ){ 
		if (c<'-') return ERROR;
		if ((c>'.')&&(c<'0')) return ERROR;
		if ((c>'9')&&(c<'_')) return ERROR;
		if ((c>'_')&&(c<'a')) return ERROR;
		if (c>'z') return ERROR;
		i++;
	  }

	  if (Parameter[CARDFILE][0]=='.'){
		if (Parameter[CARDFILE][1]==0) return ERROR;
		if ((Parameter[CARDFILE][1]=='.')&&(Parameter[CARDFILE][2]==0)) return ERROR;
	  }

  	  strcpy(Cardfile,Parameter[CARDFILE]);
  }



  // check and set auth file
  if (Optionpresent[AUTHFILE]==1)  {
	  if (Parameterpresent[AUTHFILE]==0) return ERROR;
	  if (Parameter[AUTHFILE][0]==0) return ERROR;
	  nlen = strlen(Parameter[AUTHFILE]);
	  if ((nlen<1)||(nlen>MAXFILENAMELEN)) return ERROR;
	  i=0;
  	  while ( (c=Parameter[AUTHFILE][i]) !=0 ){ 
		if (c<'-') return ERROR;
		if ((c>'.')&&(c<'0')) return ERROR;
		if ((c>'9')&&(c<'_')) return ERROR;
		if ((c>'_')&&(c<'a')) return ERROR;
		if (c>'z') return ERROR;
		i++;
	  }

	  if (Parameter[AUTHFILE][0]=='.'){
		if (Parameter[AUTHFILE][1]==0) return ERROR;
		if ((Parameter[AUTHFILE][1]=='.')&&(Parameter[AUTHFILE][2]==0)) return ERROR;
	  }

  	  strcpy(Authfile,Parameter[AUTHFILE]);
  }



  // check and set amount
  char *pptr;
  if (Operation!='g'){
	  if (Operation=='n') pptr=Parameter[NEWACCOUNT]; 
	  if (Operation=='d') pptr=Parameter[DEPOSIT]; 
	  if (Operation=='w') pptr=Parameter[WITHDRAW]; 

	  if (pptr[0]==0) return ERROR;
	  if ((pptr[0]=='0')&&(pptr[1]!='.')) return ERROR;
	  nlen = strlen(pptr);
	  if ((nlen<4)||(nlen>13)) return ERROR;
	  if (pptr[nlen-3]!='.') return ERROR;

	  Dollar = -1; Cent = -1; end = 0;
	  sscanf(pptr,"%ld.%d%c",&Dollar,&Cent,&end);
	  if (end!=0) return ERROR;
	  if ((Cent>99)||(Cent<0)) return ERROR;
	  if ((Dollar>MAXDOLLAR)||(Dollar<0)) return ERROR;
	  if ((Cent<10)&&(pptr[nlen-2]!='0')) return ERROR;
	  if ((Dollar<10)&&(nlen>4)) return ERROR;
	  if ((Dollar<100)&&(nlen>5)) return ERROR;
	  if ((Dollar<1000)&&(nlen>6)) return ERROR;
	  if ((Dollar<10000)&&(nlen>7)) return ERROR;
	  if ((Dollar<100000)&&(nlen>8)) return ERROR;
	  if ((Dollar<1000000)&&(nlen>9)) return ERROR;
	  if ((Dollar<10000000)&&(nlen>10)) return ERROR;
	  if ((Dollar<100000000)&&(nlen>11)) return ERROR;
	  if ((Dollar<1000000000)&&(nlen>12)) return ERROR;

  }
  else Cent=0;


  // if nothing goes wrong, set port num and ip address
  Portnum = portnum;
  strcpy(Ipaddress,Parameter[IPADDRESS]);




//fprintf(stderr,"atm parsed Cmd: %s %s %d %s %s %c %ld %d\n",Authfile,Ipaddress,Portnum,Cardfile,Accountname,Operation,Dollar,Cent);




  return SUCCESS;
}










