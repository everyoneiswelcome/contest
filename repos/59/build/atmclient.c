// Openssl client 

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <sys/socket.h>
#include <resolv.h>
#include <netdb.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <stdlib.h>
#include <sys/types.h>
#include <netinet/in.h>
#include "atm.h"
#include "global.h"



 
#define FAIL    -1
 



// misc functions needed by client

int OpenConnection(const char *hostname, int port, int mode)
{   int sd;
    struct hostent *host;
    struct sockaddr_in addr;
 
    if ( (host = gethostbyname(hostname)) == NULL )
    {
        perror(hostname);
        abort();
    }
    sd = socket(PF_INET, SOCK_STREAM, 0);
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = *(long*)(host->h_addr);



   //set socket timtout to 10 sec
    struct timeval tv;

    tv.tv_sec = 10;  
    tv.tv_usec = 0;  // Not init'ing this can cause strange errors

    if (setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,sizeof(struct timeval))<0)
	return ERROR;
    //fprintf(stderr,"client set timeout to 10 sec \n");



   //allow reuse socket
    int yes=1;  // or int yes='1'; 
    if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) 
 	return ERROR;
    //fprintf(stderr,"client set to re-use \n");


    // try to connect until success
    int tmp=0;
    while ( connect(sd, (struct sockaddr*)&addr, sizeof(addr)) != 0 )
    {

	tmp++;
	if (mode==STILLALIVEMODE) break;
	//if (tmp>10) break;
 	//fprintf(stderr,"client retry bind %d\n",tmp);

	sleep(1);

    }

    return sd;
}
 



// misc functions needed by client

SSL_CTX* InitCTX(void)
{   const SSL_METHOD *method;
    SSL_CTX *ctx;
 
    OpenSSL_add_all_algorithms();  /* Load cryptos, et.al. */
    SSL_load_error_strings();   /* Bring in and register error messages */
    method = SSLv3_client_method();  /* Create new client-method instance */
    ctx = SSL_CTX_new(method);   /* Create new context */
    if ( ctx == NULL )
    {
        ERR_print_errors_fp(stderr);
        abort();
    }
    return ctx;
}
 




// client starts here. It opens connection. send a message to bank. receive a result from bank. 

int startclient(char* hostname, int portnum, char* cmdstr, int mode)
{


	    // check msg length
	    int clen = strlen(cmdstr+AUTH_IDBYTESIZE) + AUTH_IDBYTESIZE;
	    if (clen>PROTOCOLMSGLEN) return ERROR;

	    //fprintf(stderr,"client cmdstr %s\n",cmdstr);


	    SSL_CTX *ctx;
	    int client;
	    SSL *ssl;

    	    char buffer[PROTOCOLMSGLEN];
	    int n;
	 

	    SSL_library_init();
	    ctx = InitCTX();

	    SSL_CTX_set_timeout(ctx, 10);
	    
	    client = OpenConnection(hostname, portnum, mode);

	    ssl = SSL_new(ctx);      /* create new SSL connection state */
	    SSL_set_fd(ssl, client);    /* attach the socket descriptor */



	    if ( SSL_connect(ssl) == FAIL )   /* perform the connection */
		{close(client); SSL_CTX_free(ctx); return PROTOCOL_ERROR;}

	    //fprintf(stderr,"client connected\n");


	    //fprintf(stderr,"Client Connected with %s encryption\n", SSL_get_cipher(ssl));
 


	    //send cmdstr to bank
	    //fprintf(stderr,"client Sending: %s\n",cmdstr);

	    n = SSL_write(ssl, cmdstr,clen);

	    if (n < 0) {
		SSL_free(ssl); close(client); SSL_CTX_free(ctx); return PROTOCOL_ERROR;
	    }

	    //fprintf(stderr,"client cndstr Sent to bank \n");


	    //if only sending still alive msg, done. quit
	    if (mode==STILLALIVEMODE){
		SSL_free(ssl); close(client); SSL_CTX_free(ctx); return SUCCESS;
	    }


	    //receive result
	    bzero(buffer,PROTOCOLMSGLEN);
	    n = SSL_read(ssl, buffer,PROTOCOLMSGLEN);

	    if (n < 0) {
		SSL_free(ssl); close(client); SSL_CTX_free(ctx); return PROTOCOL_ERROR;
	    }

	    buffer[n] = 0;

	    //fprintf(stderr,"client Received result: %d(length) %s\n",(int)strlen(buffer),buffer);


	    // if bank says transaction failed, quit
	    if (!strcmp(buffer,TRANSACTION_FAIL)) {
		SSL_free(ssl); close(client); SSL_CTX_free(ctx); return ERROR;
	    }


	    // save bank's message to Msgstr to be printed later
	    memset(Msgstr,0,PROTOCOLMSGLEN);
	    strncpy(Msgstr,buffer,PROTOCOLMSGLEN);

	    printf("%s",Msgstr); fflush(stdout);



	    //done. quit

	    SSL_free(ssl);
	    close(client);         /* close socket */
	    SSL_CTX_free(ctx);        /* release context */
	    return 0;
}





