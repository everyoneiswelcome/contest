// all bank operations. read/write to customer record. All records in memory only. no file created.

#include <stdio.h>
#include <string.h>
#include "bank.h"


inline int newaccount(char *name, long int dollar, int cent){

  if (dollar<MIN_NEWBALANCE)
	return ERROR;


  int i; 
  int nlen = strlen(name);

  // see if it's in the record. if not add new acc
  for (i=0;i<MAXCUSTOMER;i++){ 

    if ( ( nlen == strlen(recordname[i]) ) && ( strcmp(name,recordname[i]) ==0 ) ) 
	return ERROR;

    if ( recordname[i][0]==0) {
	strcpy(recordname[i],name);
	recorddollar[i]=dollar;
	recordcent[i]=cent;

	return SUCCESS;
    }
  }

  return ERROR;
}






inline struct money getbalance(char *name){

  struct money amount;
  amount.cent=-1;

  int nlen = strlen(name);
  int i;

  // see if it's in the record. if yes, return amount
  for (i=0;i<MAXCUSTOMER;i++){ 

    if ( ( nlen == strlen(recordname[i]) ) && ( strcmp(name,recordname[i]) ==0 ) ) 
    {
      amount.dollar=recorddollar[i];
      amount.cent=recordcent[i];
   
      return amount;
    }
  }
  return amount;
}



inline int withdraw(char *name, long int dollar, int cent){

  if ((dollar<0)||(dollar>MAXDOLLAR))	return ERROR;
  if ((cent<0)||(cent>99))  return ERROR;


  int i;
  long int tmpdollar;
  int tmpcent;

  int nlen = strlen(name);


  // see if it's in the record. if yes, do withdraw
  for (i=0;i<MAXCUSTOMER;i++){ 

    if ( ( nlen == strlen(recordname[i]) ) && ( strcmp(name,recordname[i]) ==0 ) ) 
    {

	if (recorddollar[i]<dollar) return ERROR;

	tmpdollar = recorddollar[i] - (long int)dollar;
	tmpcent = recordcent[i] - cent;

	if (tmpcent<0){
		if (tmpdollar==0) return ERROR;
		tmpdollar -= 1;
		tmpcent +=100;
	}

//  	recorddollarbackup[i]=recorddollar[i];
//  	recordcentbackup[i]=recordcent[i];
	recorddollar[i]=tmpdollar;
	recordcent[i]=tmpcent;

	return SUCCESS;
    }
  }
  return ERROR;

}



inline int deposit(char *name, long int dollar, int cent){

  if ((dollar<0)||(dollar>MAXDOLLAR))	return ERROR;
  if ((cent<0)||(cent>99))  return ERROR;


  int i;
  long int tmpdollar;
  int tmpcent;

  int nlen = strlen(name);


  // see if it's in the record. if yes, do deposit
  for (i=0;i<MAXCUSTOMER;i++){ 

    if ( ( nlen == strlen(recordname[i]) ) && ( strcmp(name,recordname[i]) ==0 ) ) 
    {

	tmpdollar = recorddollar[i] + (long int)dollar;
	tmpcent = recordcent[i] + cent;

	if (tmpcent>99){
		tmpdollar += 1;
		tmpcent -=100;
	}

//  	recorddollarbackup[i]=recorddollar[i];
//  	recordcentbackup[i]=recordcent[i];
	recorddollar[i]=tmpdollar;
	recordcent[i]=tmpcent;

	return SUCCESS;
    }
  }
  return ERROR;

}





inline int rollback(struct banktransdata trans){

  int i;

  if (trans.dowhat == 'g') return SUCCESS;  

  if ((trans.dowhat == 'w')||(trans.dowhat == 'd')){
    for (i=0;i<MAXCUSTOMER;i++){ 

    if ( ( strlen(trans.account) == strlen(recordname[i]) ) && ( strcmp(trans.account,recordname[i]) ==0 ) )  {
    
  	recorddollar[i]=recorddollarbackup[i];
  	recordcent[i]=recordcentbackup[i];
	return SUCCESS;
      }
    }
    if (i==MAXCUSTOMER) return ERROR;
  }

  if (trans.dowhat == 'n'){
    for (i=0;i<MAXCUSTOMER;i++){ 

    if ( ( strlen(trans.account) == strlen(recordname[i]) ) && ( strcmp(trans.account,recordname[i]) ==0 ) )  {

	memset(recordname[i],0,MAXFILENAMELEN+1);	
	return SUCCESS;
      }
    }
  }
 return ERROR;

}









