// a special parser for parsing command line options 
// plus a special string compare function

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#define S_MAXNUMOPTION 26  

#define S_MAXLEN 4096
#define S_ERROR 255
#define S_SUCCESS 0
 
#define S_MIN(X, Y) (((X) < (Y)) ? (X) : (Y))



// Parse options. options must be single lowercase letters. no duplicates. otherwise report error.
// formats of -p 2000 and -p2000 are both accepted
// all available options should be in *options  eg. "ndwgasipca", total of 9 options
// parsed Parameters will be put into S_Parameter[][]. 
// Also, S_Optionpresent[] and S_Parameterpresent[] are set to 1's if options and parameters are prsent

inline int special_parser(int num1, char *str1[], char *options, int numoption, int maxnumargc, int minnumargc, char S_Parameter[][S_MAXLEN], char *S_Parameterpresent, char *S_Optionpresent){

  // too many or too few options
  if ((num1<minnumargc)||(num1>maxnumargc)) return S_ERROR;

  // initialize tables, space for all 26 letters
  char allpresent[S_MAXNUMOPTION];
  char allavailable[S_MAXNUMOPTION];
  char allparameterpresent[S_MAXNUMOPTION];
  char allparameter[S_MAXNUMOPTION][S_MAXLEN];

  memset(allpresent,0,S_MAXNUMOPTION);
  memset(allavailable,0,S_MAXNUMOPTION);
  memset(allparameterpresent, 0, S_MAXNUMOPTION);
  memset(allparameter, 0, S_MAXNUMOPTION*S_MAXLEN);

  // if option is in available *options list, mark it as 1 in allavailble[]
  int i=0; 
  char *cptr=options;

  while ( *cptr !=0 ){
	allavailable[ *cptr - 'a' ] = 1;
	cptr++;
  }


  // parse options. -avalue or -a value. cannot have duplicates. must be in available *options list

  i = 1;

  int k = 0;
  int j = 0;
  int ttt = 0;

  // first fill the option tables with spaces for all possible 26 letters. 
  // if a parsed option is not in available *options list, return error.
  while (i<num1) { //more to parse
 
	cptr = str1[i];

	//first char must be -
	if ( *cptr !='-') return S_ERROR;	
	cptr++;

	//second char must be lowercase letter
	k = *cptr - 'a';
	if ( (k<0) || (k>25) ) return S_ERROR;

	//if not in options list or already present, error
	if (!allavailable[k]) return S_ERROR;
	if (allpresent[k]) return S_ERROR;
   	allpresent[k]=1;
	cptr++;

	//format -a value. copy S_Parameter
	if (*cptr ==0) {
		i++;
		if (i<num1){  //not the end
		    cptr=str1[i];
		    if (*cptr=='-') continue;  //no S_Parameter, move on

		    allparameterpresent[k]=1;  //there is S_Parameter

		    strncpy(allparameter[k],cptr,S_MIN(S_MAXLEN,ttt=strlen(cptr)));
		    cptr=cptr+ttt+1;

		    i++;
		}
	}

	// format -avlue. copy S_Parameter
	else {
		allparameterpresent[k]=1;  //there is S_Parameter


		    strncpy(allparameter[k],cptr,S_MIN(S_MAXLEN,ttt=strlen(cptr)));
		    cptr=cptr+ttt+1;

		i++;
	}

  }


  // copy options to final S_Parameter etc
  cptr = options;
  j = 0;

  while (j<numoption){
	  k = *cptr - 'a';

	  if (allpresent[k]){
		S_Optionpresent[j]=1;
		if (allparameterpresent[k]){
		    S_Parameterpresent[j]=1;
		    strcpy(S_Parameter[j], allparameter[k]);
		}
	  }
	  j++; 
	  cptr++;
  }
  
  return 0;
}







