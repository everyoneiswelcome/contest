#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include "atm.h"


// this file contains the main atm program


extern int parse_atm_command(int num1, char *str1[]);



// main atm program
int main(int argc, char* argv[]){

//     initatm();
	  memset(Authfile,0,MAXFILENAMELEN);
	  memset(Cardfile,0,MAXFILENAMELEN);
	  memset(Accountname,0,MAXACCNAMELEN);
	  memset(Ipaddress,0,MAXIPHOSTLEN);

	  strcpy(Authfile,"bank.auth");
	  strcpy(Ipaddress , "127.0.0.1");
	  Portnum=3000;
	  strcpy(Accountname,"");
	  Dollar=0;
	  Cent=-1;
	  Operation='-';



     //parse options, write to global var
     int result = parse_atm_command(argc,argv) ;

     //fprintf(stderr,"atm parse result %d\n",result);
     if (result!=SUCCESS) {
     	//fprintf(stderr,"atm about to send SA %s %d \n",Ipaddress,Portnum);
	startclient(Ipaddress,Portnum,STILLALIVE,STILLALIVEMODE);
	return ERROR;
     }

     //set card name
     if (createCardname()!=SUCCESS) {
	//fprintf(stderr,"atm about to send SA %s %d \n",Ipaddress,Portnum);		
	startclient(Ipaddress,Portnum,STILLALIVE,STILLALIVEMODE);
	return ERROR;
     }

     //init card record
     initcard();

     //verify operation. if error, send a dummy msg to bank to keep it alive
     if (Operation=='n') {

	if (verifynewaccount()!=SUCCESS) {
		//fprintf(stderr,"atm about to send SA %s %d \n",Ipaddress,Portnum);	
		startclient(Ipaddress,Portnum,STILLALIVE,STILLALIVEMODE);
		return ERROR;
        }
     }
     else if (Operation=='w') {
	if (verifywithdraw()!=SUCCESS) {
		//fprintf(stderr,"atm about to send SA %s %d \n",Ipaddress,Portnum);	
		startclient(Ipaddress,Portnum,STILLALIVE,STILLALIVEMODE);
		return ERROR;
        }
     }
     else if (Operation=='d') {
	if (verifydeposit()!=SUCCESS) {
		//fprintf(stderr,"atm about to send SA %s %d \n",Ipaddress,Portnum);
		startclient(Ipaddress,Portnum,STILLALIVE,STILLALIVEMODE);
		return ERROR;
        }
     }
     else if (Operation=='g') {
	if (verifygetbalance()!=SUCCESS) {
		//fprintf(stderr,"atm about to send SA %s %d \n",Ipaddress,Portnum);
		startclient(Ipaddress,Portnum,STILLALIVE,STILLALIVEMODE);
		return ERROR;
        }
     }


     //fprintf(stderr,"atm cmd ok\n" );


     //fprintf(stderr,"atm about to check auth key \n");
     // does auth file exist?
     struct stat st;
     if ((Optionpresent[AUTHFILE]==1)&&(stat(Authfile, &st)!=0)) return ERROR;

     //fprintf(stderr,"atm about to open auth file \n");
     // read Auth key from file
     memset(Auth_id,0,AUTH_IDBYTESIZE);
     FILE *fp1;
     fp1 = fopen(Authfile,"r");
     if (fp1==NULL) {
	//fprintf(stderr,"atm cannot open auth file\n");
	return ERROR;
     }
     //fprintf(stderr,"atm about to read auth key from auth file \n");
     if (fread(Auth_id, 1, AUTH_IDBYTESIZE, fp1) != AUTH_IDBYTESIZE) {
	fclose(fp1); 
	//fprintf(stderr,"atm cannot read auth file\n");
     	return ERROR;
     }
     fclose(fp1);
     
     //fprintf(stderr,"atm read auth key %s \n",Auth_id);

     //craft msg to bank
     char tobankstr[PROTOCOLMSGLEN];
     memset(tobankstr,0,PROTOCOLMSGLEN);
     memcpy(tobankstr,Auth_id,AUTH_IDBYTESIZE);
     snprintf(tobankstr+AUTH_IDBYTESIZE,PROTOCOLMSGLEN,"#%s %c %ld %d#",Accountname,Operation,Dollar,Cent);

     //fprintf(stderr,"atm about to send to bank %s %d \n",Ipaddress,Portnum);

     //talk to bank
     result = startclient(Ipaddress,Portnum,tobankstr,NORMALMODE);

     //fprintf(stderr,"client exit result %d\n", result );


     //for new account, create card file and card record
     if (result==SUCCESS){
	    if (Operation=='n'){
		if (createcardrecord()!=SUCCESS){
		  //fprintf(stderr,"client card create failed\n");	  
		  return ERROR;
		}	
	    }
     }

     //fprintf(stderr,"client exit result %d\n",result);
     return result;



}


