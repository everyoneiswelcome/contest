#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <openssl/rand.h>
#include "global.h"

//#define AUTH_IDSIZE 256


// this file is for creating am authentication file - openssl certificate


int create_certfile(char *filename){

    // craft system command
    char c[MAXFILENAMELEN+100];
    memset(c,0,MAXFILENAMELEN+100);

    strcpy( c, "openssl req -x509 -nodes -batch -days 365 -newkey rsa:1024 -keyout ");
    strcat( c, filename);
    strcat( c, " -out ");
    strcat( c, filename);
    strcat( c, " >2 ");

    int result = system(c);

    //fprintf(stderr,"create certfile result: %d\n", result);

    if (result==0) return SUCCESS;
  
    return ERROR;

}


int create_authfile(char *filename, unsigned char *auth_id){

    // check file exist
    struct stat st;
    if (stat(filename, &st)==0)
	 return ERROR;


    //generate random bits

    if (!RAND_bytes(auth_id, AUTH_IDBYTESIZE)) {
	return ERROR;
    }

    //write to authfile
    FILE *fp;

    //fprintf(stderr,"write to: %s\n",filename);

    fp=fopen(filename, "w");
    if (fp==NULL) return ERROR;

   
    if (fwrite(auth_id, 1, AUTH_IDBYTESIZE, fp) != AUTH_IDBYTESIZE) return ERROR; 

    //fprintf(stderr,"Bank Created Auth file\n");

    fclose(fp);
    return SUCCESS;

}




























