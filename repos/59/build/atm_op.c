#include <stdio.h>
#include <sys/stat.h>
#include "atm.h"
#include "card.h"


// these functions verifies an atm operation. check for errors



extern int findindice();

int Cardindex=-1;
int Accountindex=-1;



inline int verifynewaccount(){

  //for new account, check acc not exist, card not exist, min balance

    if (Dollar<MIN_NEWBALANCE) return ERROR;


//    if (create_new_ok()!=SUCCESS) return ERROR;
    findindice();
    if ((Accountindex!=-1)||(Cardindex!=-1)) return ERROR;


//    if (fileexist(Cardfile)==SUCCESS) return ERROR;
    struct stat st;
    if (!stat(Cardfile, &st)) return ERROR;


    return SUCCESS;
}


inline int verifydeposit(){

  //for withdraw/deposit, check amount not zero, account exist, card belongs to account

    if((Dollar==0)&&(Cent==0)) return ERROR;


//    if (match_card_acc()!=SUCCESS) return ERROR;
    findindice();
    if (Accountindex!=Cardindex) return ERROR;
    if ((Accountindex==-1)||(Cardindex==-1)) return ERROR;



//    if (fileexist(Cardfile)==ERROR) return ERROR;
    struct stat st;
    if (stat(Cardfile, &st)!=0) return ERROR;



    return SUCCESS;
}


inline int verifywithdraw(){

  //for withdraw/deposit, check amount not zero, account exist, card belongs to account

    if((Dollar==0)&&(Cent==0)) return ERROR;


//    if (match_card_acc()!=SUCCESS) return ERROR;
    findindice();
    if (Accountindex!=Cardindex) return ERROR;
    if ((Accountindex==-1)||(Cardindex==-1)) return ERROR;


//    if (fileexist(Cardfile)==ERROR) return ERROR;
    struct stat st;
    if (stat(Cardfile, &st)!=0) return ERROR;

    return SUCCESS;
}


inline int verifygetbalance(){

//    if (match_card_acc()!=SUCCESS) return ERROR;
    findindice();
    if (Accountindex!=Cardindex) return ERROR;
    if ((Accountindex==-1)||(Cardindex==-1)) return ERROR;


//    if (fileexist(Cardfile)==ERROR) return ERROR;
    struct stat st;
    if (stat(Cardfile, &st)!=0) return ERROR;
    
    return SUCCESS;
}












