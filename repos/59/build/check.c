// this file contains the function that crafts the message string after a successful bank operation


#include <stdio.h>
#include <string.h>
#include "bank.h"
#include "global.h"





inline char* printoutput(char *name, long int dollar, int cent, int operation){

	memset(Msgstr,0,sizeof(PROTOCOLMSGLEN));

 	char centstr[4];

	if (cent==0) centstr[0]=0;
	else centstr[0]='.';

	// 0 is 48 ... 9 is 57
	if (cent<10)  centstr[1]=48;
	else   centstr[1]=(cent/10)+48;

	int t=cent%10;
	if (t==0)  centstr[2]=0;
	else   centstr[2]=t+48;

	centstr[3]=0;

	
	if (operation == OP_NEWACCOUNT){
	  if (centstr[0]==0) 
  	    snprintf(Msgstr,PROTOCOLMSGLEN,"{\"%s\": %ld, \"account\": \"%s\"}\n", STR_NEWACCOUNT, dollar, name);
	  else
  	    snprintf(Msgstr,PROTOCOLMSGLEN,"{\"%s\": %ld%s, \"account\": \"%s\"}\n", STR_NEWACCOUNT, dollar, centstr,name);

       	}

	else  if (operation == OP_WITHDRAW){
	  if (centstr[0]==0) 
  	    snprintf(Msgstr,PROTOCOLMSGLEN,"{\"account\": \"%s\", \"%s\": %ld}\n", name, STR_WITHDRAW, dollar);
	  else
  	    snprintf(Msgstr,PROTOCOLMSGLEN,"{\"account\": \"%s\", \"%s\": %ld%s}\n", name, STR_WITHDRAW, dollar, centstr);
	}

	else  if (operation == OP_DEPOSIT){
	  if (centstr[0]==0) 
  	    snprintf(Msgstr,PROTOCOLMSGLEN,"{\"account\": \"%s\", \"%s\": %ld}\n", name, STR_DEPOSIT, dollar);
	  else
  	    snprintf(Msgstr,PROTOCOLMSGLEN,"{\"account\": \"%s\", \"%s\": %ld%s}\n", name, STR_DEPOSIT, dollar, centstr);
	}


	else  if (operation == OP_GETBALANCE){
	  if (centstr[0]==0) 
  	    snprintf(Msgstr,PROTOCOLMSGLEN,"{\"account\": \"%s\", \"%s\": %ld}\n", name, STR_BALANCE, dollar);
	  else
  	    snprintf(Msgstr,PROTOCOLMSGLEN,"{\"account\": \"%s\", \"%s\": %ld%s}\n", name, STR_BALANCE, dollar, centstr);
	}




	//fprintf(stderr,"prepared msg: %s",Msgstr);


	return (char*)Msgstr;
}










