#include "global.h"

#define OPTION "ndwgsipca"  //command line options
#define NUMOPTION  9     //9 possible options
#define MAXNUMARGC  13  //max argc
#define MINNUMARGC  3   //min argc


// numbers based on the order in OPTION
#define NEWACCOUNT 0
#define DEPOSIT 1
#define WITHDRAW 2
#define GETBALANCE 3
#define AUTHFILE 4
#define IPADDRESS 5
#define PORTNUM 6
#define CARDFILE 7
#define ACCOUNTNAME 8


// values extracted from parsed parameters will be put in these
int Portnum;
char Authfile[MAXFILENAMELEN+1];
char Cardfile[MAXFILENAMELEN+1];
char Ipaddress[MAXIPHOSTLEN+1];
char Accountname[MAXACCNAMELEN+1];
long int Dollar;
int Cent;
char Operation;




// string for sending to/from atm and bank 
char Msgstr[PROTOCOLMSGLEN];


// parsed options will be put in these
char Parameter[NUMOPTION][MAXLEN];
char Parameterpresent[NUMOPTION];
char Optionpresent[NUMOPTION];


// for auth key
unsigned char Auth_id[AUTH_IDBYTESIZE];




// a single transaction based on values extraced from parsed parameters.
struct banktransdata {
  char dowhat;  // operation: n - newaccount, w - withdraw, d - deposit, g - getbalance
  char account[MAXACCNAMELEN+1];  // account name
  long int dollar;  
  int cent;  // -1 is error
};
  
