#ifndef RAND_FUNC_H
#define RAND_FUNC_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/rand.h>

unsigned int random_uint();
void gen_key(int size, unsigned char *key);

#endif
