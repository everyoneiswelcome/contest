#include <stdio.h>
#include <openssl/rand.h>
#include <string.h>
#include <errno.h>
#include "auth_file.h"
#include "file_functions.h"

unsigned int random_uint() {
    union {
        unsigned int i;
        unsigned char c[sizeof(unsigned int)];
    } u;

        if (!RAND_bytes(u.c, sizeof(u.c))) {
            fprintf(stderr, "Can't get random bytes!\n");
            exit(1);
        }
    return u.i ;
}
void gen_key(int size, char *key) {
        int n=0;
        while(n<size) {
                key[n]=(char)(random_uint()%94+32);
		fflush(stdout);
                n++;
        }
	key[n]='\0';
}

int save_key(char *path, char *iv, char *key)
{
	char iv_key[IV_SIZE+KEY_SIZE];

	memset(iv_key, '\0', sizeof(iv_key));
	strncpy(iv_key, iv, strlen(iv));
	strncat(iv_key, key, strlen(key));	
	fprintf(stderr, "OUT IV_KEY: %s\n", iv_key);

	if (file_create(path, iv_key, KEY_SIZE+IV_SIZE) <0)
	{
		return 255;
	} 
	return 0;
}	

int get_key(char *path, char *iv, char *key)
{
	int counter;
	char new_iv[IV_SIZE+1];
	char new_key[KEY_SIZE+1];
	char iv_key[IV_SIZE+KEY_SIZE];

	memset(new_iv, '\0', sizeof(new_iv));
	memset(new_key, '\0', sizeof(new_key));
	memset(iv_key, '\0', sizeof(iv_key));
	if (file_read(path, iv_key, IV_SIZE+KEY_SIZE+1) <0)
	{
		return 255;
	}

	fprintf(stderr, "IN IV_KEY:  %s\n", iv_key);
		
	for(counter=0; counter<IV_SIZE+KEY_SIZE; counter++)
	{
		if (counter < 16)
		{
			fprintf(stderr, "new_iv[%d] = iv_key[%d] : %c\n", counter, counter, iv_key[counter]);
			new_iv[counter]=iv_key[counter];
		} else {
			fprintf(stderr, "new_key[%d] = iv_key[%d] : %c\n", counter-IV_SIZE, counter, iv_key[counter]);
			new_key[counter-16]=iv_key[counter];
		}
	}
	new_iv[IV_SIZE]='\0';
	new_key[KEY_SIZE]='\0';
	strncpy(key, new_key, strlen(new_key)+1);
	strncpy(iv, new_iv, strlen(new_iv)+1);
	fprintf(stdout, "#New Key(%zu) => %s\n", strlen(new_key), new_key);
        fprintf(stdout, "#New IV (%zu) => %s\n", strlen(new_iv), new_iv);
	return 0;
}
