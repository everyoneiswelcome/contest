#ifndef FF_H
#define FF_H

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

int file_create(char *path, char *buf, int size);
int file_delete(char *path);
int file_read(char *path, char *buf, int n);
int file_exists(char *path);

#endif
