#include "rand_funcs.h"

unsigned int random_uint() {
    union {
        unsigned int i;
        unsigned char c[sizeof(unsigned int)];
    } u;

        if (!RAND_bytes(u.c, sizeof(u.c))) {
            fprintf(stderr, "Can't get random bytes!\n");
            exit(1);
        }
    return u.i ;
}

void gen_key(int size, unsigned char *key) {
        int n=0;
        while(n<size) {
                key[n]=(char)random_uint();
                n++;
        }
}

