#include <string.h>
#include "auth_file.h"

int main()
{

	char text_key[KEY_SIZE+1];
	gen_key(KEY_SIZE, text_key);

	char text_iv[IV_SIZE+1];
	gen_key(IV_SIZE, text_iv);


	fprintf(stdout, "Text Key(%zu) => %s\n", strlen(text_key), text_key);
	fprintf(stdout, "Text IV (%zu) => %s\n", strlen(text_iv), text_iv);	
/*	
	char output_text[KEY_SIZE+IV_SIZE];
	memset(output_text, '\0', sizeof(output_text));
	strncpy(output_text, text_iv, strlen(text_iv));
	strncat(output_text, text_key, strlen(text_key));
	
	fprintf(stdout, "Output Text (%zu) => %s\n", strlen(output_text), output_text);	

	
	file_create("test.output", output_text, KEY_SIZE+IV_SIZE); 	
*/
	if (save_key("test.output", text_iv, text_key)>0) {
		fprintf(stdout, "ERROR WILL ROBINSON!\n");
		return 255;
	}
/*
	char input_text[KEY_SIZE+IV_SIZE];
	file_read("test.output", input_text, IV_SIZE+KEY_SIZE+1);

	fprintf(stdout, "Input Text (%zu) => %s\n", strlen(input_text), input_text);	
*/
//	int counter=0;
	char new_iv[IV_SIZE+1];
	memset(new_iv, '\0', sizeof(new_iv));
	char new_key[KEY_SIZE+1];
	memset(new_key, '\0', sizeof(new_key));
/*
	fprintf(stdout, "KEY SIZE: %d\n", KEY_SIZE);
	for(counter=0; counter<IV_SIZE+KEY_SIZE; counter++)
	{
		if (counter < 16) {
			fprintf(stdout, "new_iv[%d] = input_text[%d] : %c\n", counter, counter, input_text[counter]);
			new_iv[counter] = input_text[counter];
		} else {
			fprintf(stdout, "new_key[%d] = input_text[%d] : %c\n", counter-16, counter, input_text[counter]);
			new_key[counter-16] = input_text[counter];
		}
	}
	new_iv[16]='\0';
	new_key[32]='\0';
*/

	get_key("test.output", new_iv, new_key);
	fprintf(stdout, "New Key(%zu) => %s\n", strlen(new_key), new_key);
	fprintf(stdout, "New IV (%zu) => %s\n", strlen(new_iv), new_iv);	
		
	return 1;
}
