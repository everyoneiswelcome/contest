#include "rand_funcs.h"

int main() {
    unsigned char tkey[32];
    gen_key(32, tkey);

    printf("Key: %s\n", tkey);
    return 0;
}
