
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

//Given a path name and a buffer as strings, create a file
int file_create(char *path, char *buf, int size)
{
	int file_descriptor = open(path, O_CREAT | O_RDWR, 
				   S_IRUSR|S_IWUSR);
	if (file_descriptor < 0)
		return file_descriptor;

	write(file_descriptor,buf,size);
	
	close(file_descriptor);
	
	return 0;
}

//Delete a file.  Used for cleanup at termination of bank.
int file_delete(char *path)
{
	return unlink(path);
}

//Open a file & read n bytes
int file_read(char *path, char *buf, int n)
{
	int file_descriptor = open(path, O_RDONLY);
	if (file_descriptor < 0 || n <= 0)
		return -1;
	read(file_descriptor,buf,n);
	buf[n-1] = '\0';

	return 0;
}

//Returns 0 if a file exists, -1 if not
int file_exists(char *path)
{
//	fprintf(stderr,"checking for file %s\n",path);
	int retval = access(path,F_OK);
//	fprintf(stderr,"return value from access was %d\n",retval);
	if(errno == ENOENT)
	{
		/* file doesn't exist errno = ENOENT */
//		fprintf(stderr,"auth file %s doesn't exist\n",path);
		return -1;
	}
	else
	{
//		fprintf(stderr,"auth file %s exists\n",path);
		return 0;
	}
}

