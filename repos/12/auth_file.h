#ifndef AUTH_FILE_H
#define AUTH_FILE_H

#include <stdio.h>
#include <openssl/rand.h>
#include <string.h>
#include <errno.h>

#define KEY_SIZE 32
#define IV_SIZE 16

typedef struct KeyStuff
        {
        unsigned char iv[16];
        unsigned char key[32];
      } iv_key;

unsigned int random_uint();
void gen_key(int size, char *key);
int save_key(char *path, char *iv, char *key);
int get_key(char *path, char *iv, char *key);

#endif
