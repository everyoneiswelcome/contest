#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <stdio.h>

#include <stdlib.h>

#include <string.h>


#include <sys/types.h>
#include <openssl/md5.h>
#include "helper_funcs.h"


typedef struct Account {
        char Name[MNL];
	char md5sum[32];
        long long Balance;
	char card[32];
        struct Account *next;
} Account_T;

Account_T *create_account(long long balance, char *name);
Account_T *add_account(Account_T *account, long long balance, char *name);
Account_T *find_account(Account_T *account, char *name);
Account_T *modify_account(Account_T *account, int type, long long amount, char *name);
/* MD5 STUFF
void md5_build(char *str, char *md5s);
*/
void gen_md5(Account_T *account);
int md5_check(Account_T *account);

/* NEVER USED OUTSIDE OF TEST */
void print_account(Account_T *account);

#endif
