#list of bugs we know about

FIXED:getopt optstring is incorrect (s:: should be s: - for one)
FIXED:the -g option should not have a value (atm should return 255)

Card file contents to be generated, read in (atm) and compared (bank)
iv and key to be read in and used (both atm and bank)

CARD and AUTH files... need to check if the exist before we create a new one. 

Bank.auth does not get cleaned up after receipt of SIGTERM.  (Attempted fix of remove() instead of unlink() failed).

#other Notes:
passed tests:
core/createaccount.json
core/getbalance.json
core/invalid1.json
core/withdraw.json 
core/timeout1.json
core/core1.json
core/core2.json
core/core3.json
core/core4.json
core/core5.json
core/core6.json

performance/performance1.json
performance/performance2.json
performance/performance3.json
performance/performance4.json
performance/performance5.json
performance/performance6.json
performance/performance7.json
performance/performance8.json
performance/performance9.json
performance/performance10.json

failed tests:

Touch!
