#include "helper_funcs.h"
#include "linked_list.h"

Account_T *create_account(long long amount, char *name)
{
	/* check to see if name is to long */
	if (strlen(name) > (MNL-1)) return NULL; /* null character */

	/* check if amount is larger or smaller then minimum allowable balance (10.00) or in out case (1000) */
	if ((amount > MAV) || (amount < 1000)) {
		printf("unacceptable balance amount\n");
		return NULL;
	}	

        Account_T *account;

        if (!(account=malloc(sizeof(Account_T)))) return NULL;
        account->Balance=amount;
        memset(account->Name, '\0', sizeof(account->Name));
        strncpy(account->Name,name,strlen(name));
	gen_md5(account);
        account->next=NULL;
        return account;
}

/* Inserts new account at the begining of the list, returns newlist */
/* RETURNS: account list - if create_account failed  
                         - if NAME already exists
            new account list - if NAME:BALANCE added
*/
Account_T *add_account(Account_T *account, long long balance, char *name)
{

	/* check if account name already in use */
	if (find_account(account, name) != NULL) 
	{
		return account; 
	}

	Account_T *newaccount;

	newaccount=create_account(balance, name);

	/* return account if newaccount failed to be created */
	if (newaccount==NULL)  return account;
	newaccount->next=account;
	return newaccount;
}

/* FINDS AN ACCOUNT BY NAME */
/* RETURNS: account - if a matching account is found
            NULL - if no account is found
*/
Account_T *find_account(Account_T *account, char *name)
{
	while(account) {
		if(strcmp(name,account->Name)==0) return account;
		account=account->next;
	}
	return NULL;
}

/*
	type = Change Type
	     = 0 deposit
	     = 1 withdrawl
	returns: account - only if the account is modified
		 NULL - if account doesn't exist or balance can't be increase or reduced out of bounds.
*/
Account_T *modify_account(Account_T *account, int type, long long amount, char *name)
{
	Account_T *search;
	long long tmp;
	search=find_account(account, name);
	if (search == NULL) {
//		fprintf(stderr, "Account %s does not exist\n", name);
		return NULL;
	}
	if (type == 0) {
	     tmp = search->Balance+amount;
		/*check to see if new balance will exceed largest limit*/
		if (tmp<=MAB) { 
			search->Balance=tmp;
		} else {
			/*perhaps I should return something else??*/
			return NULL;
		}
	}
	if (type == 1) {
		/*check to see if new balance will drop balance below 0*/
		tmp = search->Balance-amount;
		if (tmp>=0) {
			search->Balance=tmp;
		} else {
			/*perhaps I should return something else??*/
			return NULL;
		}
	}
	gen_md5(account);
	return account;
}	


void gen_md5(Account_T *account)
{
	char str[270];
	char tmp[21];
	char md5s[32];

	memset(str, '\0', sizeof(str));
	strcpy(str, account->Name);
	sprintf(tmp, "%llu", account->Balance);
	strcat(str, tmp);
	
	md5_build(str, md5s);
	memset(account->md5sum, '\0', 32);
	strncpy(account->md5sum, md5s,32);

}


/* Compares MD5 Stored against a calulated value */
/* Returns 0 if md5's match */
/* Returns 1 if md5's don't */
int md5_check(Account_T *account)
{
	char str[270];
	char md5tmp[32];
	char tmp[21];

	memset(str, '\0', sizeof(str));
	strcpy(str, account->Name);
	sprintf(tmp, "%llu", account->Balance);
	strcat(str, tmp);

	md5_build(str, md5tmp);

	if(strcmp(md5tmp, account->md5sum)==0) {
		return(0);
	} else {
		return(1);
	}
}


void print_account(Account_T *account)
{
	while(account) {
        	fprintf(stderr, "ACCOUNT: %s : %lli : %s\n", account->Name, account->Balance, account->md5sum);
                account=account->next;
        }
	return;
}
