#ifndef FILE_FUNCTIONS_H
#define FILE_FUNCTIONS_H

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

//Given a path name and a buffer as strings, create a file
int file_create(char *path, char *buf, int size);
//Delete a file.  Used for cleanup at termination of bank.
int file_delete(char *path);

//Open a file & read n bytes
int file_read(char *path, char *buf, int n);

//Returns 0 if a file exists, -1 if not
int file_exists(char *path);

#endif
