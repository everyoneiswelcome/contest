//  atm.c
#define _GNU_SOURCE  // for strchrnul

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <errno.h>
#include "unp.h"
#include "enc_network.h"
#include "helper_funcs.h"
#include "file_functions.h"


int main (int argc, char **argv) {
    // Declaration of variables.
    char *authFile;
    char *ip; /* max IPv4 is 16 char + null */
    char *cardFile;
    char *account;
    int port;
    int transaction;
    int optoken;
    long long amount;
    struct sockaddr_in sa_addr;
    int hasTransaction;
	int sockfd=0;

    // Initialization to default values.
	//fprintf(stderr,"initializing variables\n");
    account = NULL;
    authFile = strndup("bank.auth",9);
    cardFile = NULL;
    ip = strndup("127.0.0.1",17);
    port = 3000;
    transaction = 0; // 1 = create, 2 = deposit, 3 = withdraw, 4 = status
    optoken = 0;
    amount = 0;
    hasTransaction = 0;
    char usedTokens[9];

	memset(usedTokens,'\0',9);
//	fprintf(stderr,"starting atm code\n");
    
    while ((optoken = getopt(argc, argv, "s:i:p:c:a:n:d:w:g")) != -1) {
//				fprintf(stderr,"usedTokens is %s\n", usedTokens);
        switch (optoken) {
            case 's':
                // Find if token is already used.
//					fprintf(stderr,"s token\n");
                if(strchr (usedTokens, 's')) {
                    //fprintf (stderr,"%s\n", "Used argument");
                    return 255;
                }
                // Parse auth filename.
                if (!isValidFileName(optarg)) {
		    //fprintf(stderr,"invalid auth name \n");
                    return 255;
                }
                authFile = strdup(optarg); /* must end in null */
                // Add to used tokens.
                strcat (usedTokens, "s");
                break;
                
            case 'i':
					//fprintf(stderr,"i token\n");
                if(strchr (usedTokens, 'i')) {
                    //fprintf (stderr,"%s\n", "Used argument");
                    return 255;
                }
                strcat (usedTokens, "i");
                // Parse IP address.
                ip = strndup (optarg, 17);

						//fprintf(stderr,"IP is %s\n",ip);
    				int result = inet_pton(AF_INET, ip, &(sa_addr.sin_addr));
                if (result != 1) {
						//fprintf(stderr, "invalid IP address\n");
                    return 255;
                }
                break;
            case 'p':
					//fprintf(stderr,"p token\n");
                // Parse port.
                if(strchr (usedTokens, 'p')) {
                    //fprintf (stderr,"%s\n", "Used argument");
                    return 255;
                }
                strcat (usedTokens, "p");
////						fprintf(stderr,"usedTokens is now %s\n",usedTokens);
//						fprintf(stderr,"optarg is %s\n",optarg);
						if(optarg[0] == '0')
							return 255;
                		port = atoi (optarg);

						//fprintf(stderr,"port is %d\n",port);
					
                if (port < 1024 || port > 65535) {
		    //fprintf(stderr, "invalid port\n");
                    return 255;
                }
                break;
            case 'c':
					//fprintf(stderr,"c token\n");
                if(strchr (usedTokens, 'c')) {
                    //fprintf (stderr,"%s\n", "Used argument");
                    return 255;
                }
		
                // Parse card filename.
					char *tmp_cardfile;
						tmp_cardfile = optarg;
	
                if (!isValidFileName (tmp_cardfile)) {
		    //fprintf(stderr, "Invalid card filename\n");
                    return 255;
                }
                cardFile = strdup(tmp_cardfile); /* must end in null */
////				fprintf(stderr,"Card filename is: \"%s\"\n",cardFile);
                strcat (usedTokens, "c");
                break;
            case 'a':
					//fprintf(stderr,"a token\n");
                if(strchr (usedTokens, 'a')) {
                    //fprintf (stderr,"%s\n", "Used argument");
                    return 255;
                }
                // Parse account name.
					char *tmp_accountname;
						tmp_accountname = optarg;

                if (!isValidAccount (tmp_accountname)) {
		    //fprintf(stderr,"invalid filename\n");
                    return 255;
                }
                account = strdup (tmp_accountname); /* must end in null */
                strcat (usedTokens, "a");
                break;
            case 'n':
					//fprintf(stderr,"n token\n");
						//fprintf(stderr,"case n: usedTokens is %s\n",usedTokens);
                if(strchr (usedTokens, 'n')) {
                    //fprintf (stderr,"%s\n", "Used argument");
                    return 255;
                }
                // Check if existing transaction occurs
                if (hasTransaction == 0) {
                    hasTransaction = 1;
                    transaction = NEW_ACCT;
							// Check for leading 0 - failure
							if(optarg[0] =='0')
								return 255;
							// Check for 2 and only 2 digits past the decimal
							char *teststring;
							teststring = strchrnul(optarg,'.');
							if (strlen(teststring) != 3)	//Includes the .
								return 255;
                    amount = str_to_ll (optarg);
                    // Bounds check for amount per spec.
                    if (amount < 1000 || amount > 429496729599)	
                        return 255;
                } else {
                    return 255;
                }
                strcat (usedTokens, "n");
                break;
            case 'd':
					//fprintf(stderr,"d token\n");
                if(strchr (usedTokens, 'd')) {
                   //fprintf (stderr,"%s\n", "Used argument");
                    return 255;
                }
                if (hasTransaction == 0) {
                    hasTransaction = 1;
                    transaction = DEPOSIT;
                    amount = str_to_ll (optarg);
							char *teststring;
							teststring = strchrnul(optarg,'.');
							if (strlen(teststring) != 3)	//Includes the .
								return 255;
                    if (amount <= 0 || amount > 429496729599)
                        return 255;
                } else {
                    return 255;
                }
                strcat (usedTokens, "d");
                break;
            case 'w':
					//fprintf(stderr,"w token\n");
                if(strchr (usedTokens, 'w')) {
                    //fprintf (stderr,"%s\n", "Used argument");
                    return 255;
                }
                if (hasTransaction == 0) {
                    hasTransaction = 1;
                    transaction = WITHDRAW;
							char *teststring;
							teststring = strchrnul(optarg,'.');
							if (strlen(teststring) != 3)	//Includes the .
								return 255;
                    amount = str_to_ll (optarg);
                    if (amount <= 0 || amount > 429496729599)
                        return 255;
                } else {
                    return 255;
                }
                strcat (usedTokens, "w");
                break;
            case 'g':
       //         if (optind == (argc-1)){
        ////            fprintf (stderr, "g is at end and has an argument at end, abort...\n");
         //           return 255;
          //      }
					//fprintf(stderr,"g token\n");
                if(strchr (usedTokens, 'g')) {
                    //fprintf (stderr,"%s\n", "Used argument");
                    return 255;
                }
				
                if (hasTransaction == 0) {
                    hasTransaction = 1;
                    transaction = BALANCE;
                } else {
                    return 255;
                }
                strcat (usedTokens, "g");
                break;
            default:
					/* no clue */
					//fprintf(stderr,"End of getopt - unknown option %c\n",optoken);
					return 255;
                break;
        }
    }
	if (hasTransaction == 0) {
		/* there was no transaction */
		return 255;
	}
	if (argv[optind]) {
		/* there were extra arguments */
		//fprintf(stderr,"extra arguments\n");
		return 255;
	}
    
    // Check for mandatory account name.
    if (account == NULL) {
		//fprintf(stderr,"No account name given\n");
		return 255;
	}
	//fprintf(stderr,"account name is %s\n",account);
    
    // Assign default card file if card filename is not given.
    if (cardFile == NULL) {
			int to_allocate = sizeof(char) * (strlen(account)+6);
        cardFile = malloc (to_allocate);
        strncpy (cardFile, account, to_allocate);
        strncat (cardFile, ".card", 5); /* strncat includes the \0 after 5 chars */
    }
	//fprintf(stderr,"cardfilename = %s\n",cardFile);

	/* set up networking */    
	if((sockfd = socket(AF_INET, SOCK_STREAM, 0))< 0)
   {
      /* Couldn't create socket */
		//fprintf(stderr, "Couldn't create sockfd socket\n");
      return 255;
   }
	struct timeval timeout;
	timeout.tv_sec = 10;
	timeout.tv_usec = 0;

	if(setsockopt(sockfd, SOL_SOCKET,SO_RCVTIMEO, (char *)&timeout,sizeof(timeout)) < 0) {
		/* failed to set socket options */
		//fprintf(stderr,"failed to set RCVTIMEO on sockfd\n");
		return 255;
	}
	if(setsockopt(sockfd, SOL_SOCKET,SO_SNDTIMEO, (char *)&timeout,sizeof(timeout)) < 0) {
		//fprintf(stderr,"failed to set SNDTIMEO on sockfd\n");
		/* failed to set socket options */
		return 255;
	}

	sa_addr.sin_family = AF_INET;
   sa_addr.sin_port = htons(port);
  	inet_pton(AF_INET, ip, &sa_addr.sin_addr); /* redo to account for not having -i */

	/* TEST CODE - REMOVE/ALTER BEFORE USING! */	
  /* Set up test encryption stuff */

   char text_key[KEY_SIZE+1];
   memset(text_key, '\0', sizeof(text_key));
   char text_iv[IV_SIZE+1];
   memset(text_iv, '\0', sizeof(text_iv));

   if (get_key(authFile, text_iv, text_key) > 0) {
		return 255;
   } 


//   unsigned char *key = (unsigned char *)"01234567890123456789012345678901";
   unsigned char *key = (unsigned char *)text_key;
   /* A 128 bit IV */
//  unsigned char *iv = (unsigned char *)"01234567890123456";
   unsigned char *iv = (unsigned char *)text_iv;

//fprintf(stderr, "we have the key and iv\n");


    //16 bit IV + 1 null in auth file
    static int size_of_auth_data = 17;
    char auth_data[size_of_auth_data];
    file_read(authFile,auth_data,size_of_auth_data-1);
    //Forcing null termination
    auth_data[size_of_auth_data-1] = '\0';

/* ADDING MD5 */

    static int size_of_card_data = 33; 
    char card_data[size_of_card_data];
    memset(card_data, '\0', size_of_card_data);

//   card_data[size_of_card_data] = '\0';
	/* Set up binary to send to bank */
	NProtocol transmitbin;

	strncpy(transmitbin.Name,account, MNL);
	transmitbin.amount = amount;
	//fprintf(stderr,"amount is: %lld\n",amount);


    // Transaction execution
    if (transaction == NEW_ACCT) {// Create account
		if (file_exists(cardFile)==0) {
			file_read(cardFile,card_data,size_of_card_data);
			//fprintf(stderr, "R:card_data (%lu) - %s\n", strlen(card_data), card_data);
			if (md5_compare(account, card_data)==1) {
				//fprintf(stderr,"md5compare failed\n");
				return 255;
			}
		} else {
       			md5_build(account, card_data); 
			//fprintf(stderr, "W:card_data (%lu) - %s\n", strlen(card_data), card_data);
   			file_create(cardFile,card_data,size_of_card_data); 
		}
		strncpy(transmitbin.md5sum, card_data ,size_of_card_data-1);
		transmitbin.operation = NEW_ACCT;
    } else if (transaction == DEPOSIT) {// Deposit account
		if (file_exists(cardFile)!=0) {
			//fprintf(stderr,"cardfile exists\n");
			return 255;
		}	
		file_read(cardFile,card_data,size_of_card_data);
		if (md5_compare(account, card_data)!=0) {
				//fprintf(stderr,"md5compare failed\n");
			return 255;
		}
		strncpy(transmitbin.md5sum, card_data ,size_of_card_data-1);
		transmitbin.operation = DEPOSIT;
    } else if (transaction == WITHDRAW) {// Withdraw
		if (file_exists(cardFile)!=0) {
		//fprintf(stderr,"cardfile exists\n");
			return 255;
		}	
		file_read(cardFile,card_data,size_of_card_data);
		if (md5_compare(account, card_data)!=0) {
				//fprintf(stderr,"md5compare failed\n");
			return 255;
		}
		strncpy(transmitbin.md5sum, card_data ,size_of_card_data-1);
		transmitbin.operation = WITHDRAW;
    } else if (transaction == BALANCE) {// Status
		if (file_exists(cardFile)!=0) {
			//fprintf(stderr,"cardfile exists\n");
			return 255;
		}	
		file_read(cardFile,card_data,size_of_card_data-1);
			//fprintf(stderr, "R:card_data (%lu) - %s\n", strlen(card_data), card_data);
		if (md5_compare(account, card_data)!=0) {
				//fprintf(stderr,"md5compare failed\n");
			return 255;
		}
		strncpy(transmitbin.md5sum, card_data ,size_of_card_data-1);
		transmitbin.operation = BALANCE;
	} else
		return 255;


	/* open network socket and send stuff over to the bank */
   if(connect(sockfd, (struct sockaddr *)&sa_addr, sizeof(sa_addr))<0)
   {
      fprintf(stderr,"\n Error : Connect Failed \n");
      return 255;
   }

	int ret;
	ret = write_enc_socket(sockfd, (unsigned char *)&transmitbin, sizeof(NProtocol), key, iv);
	if (ret < 0 && errno == EWOULDBLOCK) {
		//fprintf(stderr,"network failure on write: %d\n",ret);
		/* network failure */
		return 63;
	}

	long long *balance;
	/* read the bank's response */
	if(readable_timeo(sockfd) == 0) {
		//fprintf(stderr,"atm: detect socket timeout\n");
		return 63;
	}
	ret = read_enc_socket(sockfd, key, iv, (unsigned char **)&balance);
	if(ret < 0) {
		//fprintf(stderr,"network failure on read: %d\n",ret);
		/*network failure - could be crypto, could be timeout */
		if(errno == EWOULDBLOCK)
			return 63;
		else
			//fprintf(stderr,"error reading on network read\n");
			return 255;
	}
	/* print to our stderr */
	//fprintf(stderr, "Account name: %s, New balance: %lld\n", account, *balance);

	if(*balance < 0) return 255; /* using a negative number to return "error" from the bank */

	char value_to_print[2048];

	/* print to stdout */
	if (transaction == NEW_ACCT) {	
		printll(*balance, (char *)&value_to_print);	
		fprintf(stdout,"{\"account\":\"%s\",\"initial_balance\":%s}\n",account,value_to_print);
		fflush(stdout);
	}
	if (transaction == DEPOSIT) {
		printll(amount, (char *)&value_to_print);	
		fprintf(stdout,"{\"account\":\"%s\",\"deposit\":%s}\n",account,value_to_print);
		fflush(stdout);
	}
	if (transaction == WITHDRAW) {
		printll(amount, (char *)&value_to_print);
		fprintf(stdout,"{\"account\":\"%s\",\"withdraw\":%s}\n",account,value_to_print);
		fflush(stdout);
	}
	if (transaction == BALANCE) {
		printll(*balance, (char *)&value_to_print);	
		fprintf(stdout,"{\"account\":\"%s\",\"balance\":%s}\n",account,value_to_print);
		fflush(stdout);
	}

return 0;
}
