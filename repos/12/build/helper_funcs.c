#include <string.h>
#include <stdio.h>
#include <openssl/rand.h>
#include <openssl/md5.h>
#include "helper_funcs.h"
#include "file_functions.h"

int isValidFileName (char* filename) {
    const char *valid_chars = "_-.0123456789abcdefghijklmnopqrstuvwxyz";

	/* check for . and .. as invalid filenames */
	if(strcmp(filename,".") == 0)
			return 0;
	if(strcmp(filename,"..")==0)
			return 0;
    while (*filename) {
        if (!strchr (valid_chars, *filename)) {
            return 0;
        }
        filename++;
    }
    return 1;
}

int isValidAccount (char* account) {
    int isValid = 0;
	if(strcmp(account,".") == 0)
		return 1;
	if(strcmp(account,"..") == 0)
		return 1;
    if (strlen(account) <= 250 && strlen(account) > 1) {
        if (isValidFileName (account) == 1) {
            isValid = 1;
        }
    }
    return isValid;
}

double lltod(long long input) {
		/* should probably think about bounds checking here */
		return ((double) input)/100.0;
}

long long dtoll(double input) {
		/* should probably think about bounds checking here */
		return (long long) (input*100);	
}	

void printll(long long input, char * output) {
	if ((input % 100) == 0) {
		/*whole number, dont include the decipmal point */
		sprintf(output,"%lld",input/100);
	} else if ((input % 10) == 0) {
		/* only include one decimal place */
		sprintf(output,"%.1f",(double) input/100.0);
	} else {
		/* full value */
		sprintf(output,"%.2f",(double) input/100.0);
	}

}

long long str_to_ll(char *input) {
	/* reads in the string, removes the decimal and returns the value as a long long */
	long long tmp1,tmp2;
	char *endptr;

	tmp1 = strtoll(input,&endptr,10);
	tmp2 = strtoll(endptr+sizeof(char),NULL,10);
//	fprintf(stderr,"tmp1 = %lld tmp2 = %lld, total = %lld\n",tmp1,tmp2, (tmp1*100)+tmp2);

	return (tmp1*100)+tmp2;
	
}

unsigned int random_uint() {
    union {
        unsigned int i;
        unsigned char c[sizeof(unsigned int)];
    } u;

        if (!RAND_bytes(u.c, sizeof(u.c))) {
            //fprintf(stderr, "Can't get random bytes!\n");
            exit(1);
        }
    return u.i ;
}

void md5_build(char *str, char *md5s)
{
        unsigned char digest[16];
        int i;
        char th[3];
        char hex[MD5_SIZE+1];

        MD5_CTX ctx;
        MD5_Init(&ctx);
        MD5_Update(&ctx, str, strlen(str));

        MD5_Final(digest, &ctx);

     //   fprintf(stderr, "MD5 -> ");
        for(i=0; i<16; i++){
      //           fprintf(stderr, "%02x", digest[i]);
                 memset(th, '\0', sizeof(th));
                 sprintf(th, "%02x", digest[i]);
                 if (i==0) {
                        memset(hex, '\0', sizeof(hex));
                        strncpy(hex,th,2);
                 } else {
                        strncat(hex, th, 2);
                }
        }
      //  fprintf(stderr, "\n");
       // fprintf(stderr, "MD5 STRING - %s\n", hex);
//	fprintf(stderr, "BUILD HEX (%lu)- %s\n", strlen(hex), hex);
	memset(md5s, '\0', MD5_SIZE+1);
        strncpy(md5s, hex, 32);

//	fprintf(stderr, "BUILD MD5S(%lu)- %s\n", strlen(md5s), md5s);

}

int md5_compare(char *name, char *md5s)
{
	char md5temp[MD5_SIZE+1];
	memset(md5temp, '\0', strlen(md5temp));
//	fprintf(stderr, "SEND TO MD5_BUILD!\n");
	md5_build(name, md5temp);
//	fprintf(stderr, "MD5-1(%lu): %s\nMD5-2(%lu): %s\n",strlen(md5s), md5s, strlen(md5temp), md5temp);
	if (strncmp(md5temp, md5s, strlen(md5temp))==0) {
	/*MD5 is ACCOUNT*/
		return 0;
	} 
	return 1;
}

void gen_key(int size, char *key) {
        int n=0;
        while(n<size) {
                key[n]=(char)(random_uint()%94+32);
              //  fflush(stdout);
                n++;
        }
        key[n]='\0';
}

int save_key(char *path, char *iv, char *key)
{
        char iv_key[IV_SIZE+KEY_SIZE+1];

        memset(iv_key, '\0', sizeof(iv_key));
        strncpy(iv_key, iv, IV_SIZE*sizeof(char));
        strncat(iv_key, key, KEY_SIZE*sizeof(char));
        //fprintf(stderr, "OUT IV_KEY: %s\n", iv_key);

        if (file_create(path, iv_key, KEY_SIZE+IV_SIZE+1) <0)
        {
                return 255;
        } 
        return 0;
}

int get_key(char *path, char *iv, char *key)
{
        int counter;
        char new_iv[IV_SIZE+1];
        char new_key[KEY_SIZE+1];
        char iv_key[IV_SIZE+KEY_SIZE];

        memset(new_iv, '\0', sizeof(new_iv));
        memset(new_key, '\0', sizeof(new_key));
        memset(iv_key, '\0', sizeof(iv_key));
        if (file_read(path, iv_key, IV_SIZE+KEY_SIZE+1) <0)
        {
                return 255;
        }

//       fprintf(stderr, "IN IV_KEY:  %s\n", iv_key);

        for(counter=0; counter<IV_SIZE+KEY_SIZE; counter++)
        {
                if (counter < 16)
                {
//                        fprintf(stderr, "new_iv[%d] = iv_key[%d] : %c\n", counter, counter, iv_key[counter]);
                        new_iv[counter]=iv_key[counter];
                } else {
//                        fprintf(stderr, "new_key[%d] = iv_key[%d] : %c\n", counter-IV_SIZE, counter, iv_key [counter]);
                        new_key[counter-16]=iv_key[counter];
                }
        }
   //     new_iv[IV_SIZE]='\0';
    //    new_key[KEY_SIZE]='\0';
        strncpy(key, new_key, strlen(new_key));
        strncpy(iv, new_iv, strlen(new_iv));
//        fprintf(stdout, "#New Key(%zu) => %s\n", strlen(new_key), new_key);
//        fprintf(stdout, "#New IV (%zu) => %s\n", strlen(new_iv), new_iv);
        return 0;
}	
