//  bank.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include "unp.h"
#include "enc_network.h"
#include "linked_list.h"
#include "helper_funcs.h"
#include "file_functions.h"

volatile sig_atomic_t run = 1;

void sigterm_handler (int signal)
{
	run = 0;
}


int main (int argc, char **argv) {
    char *authFile;
    int port;
    int optoken;
    optoken = 0;
    port = 3000;

    authFile = "bank.auth"; 

	/* setup signal handling */
	struct sigaction sa_bank;
	memset (&sa_bank, '\0', sizeof(sa_bank));
	sa_bank.sa_handler = sigterm_handler;
	sigaction(SIGTERM, &sa_bank, NULL); 	

	
	/* set up networking information */
	int listenfd=0, connfd=0;
	struct sockaddr_in serv_addr;
	

	/* open the socket to listen on */
   listenfd = socket(AF_INET, SOCK_STREAM, 0);


	char usedTokens[3];    
	memset(usedTokens,'\0',3);
    while ((optoken = getopt (argc, argv, "p:s:")) != -1) {
//   fprintf(stderr,"usedTokens is %s\n", usedTokens);
	fflush(stderr);
        switch (optoken) {
            case 'p':
//		fprintf(stderr, "p token\n");
	  				// Find if token is already used.
                if(strchr (usedTokens, 'p')) {
 //                   fprintf (stderr,"%s\n", "Used argument");
                    return 255;
                }
                // Parse port.
					if(optarg[0] == '0')
						return 255;
               port = atoi (optarg);

                if (port < 1024 || port > 65535) {
                    return 255;
                }

					strcat(usedTokens,"p");
                break;
            case 's':
					if(strchr (usedTokens, 's')) {
                    //fprintf (stderr,"%s\n", "Used argument");
                    return 255;
                }

                // Parse auth filename.
                if (!isValidFileName(optarg)) {
                    return 255;
                }
                authFile = strdup(optarg);

						strcat(usedTokens,"s");
                break;
            default:
                break;
        }
    }

    //Create the bank.auth file...we need the IV before the run loop.
    char text_key[KEY_SIZE+1];
    gen_key(KEY_SIZE, text_key);

    char text_iv[IV_SIZE+1];
    gen_key(IV_SIZE, text_iv);

    if(file_exists(authFile) == 0) {
		/* bank file exists */
//		fprintf(stderr,"Bank file \"%s\"exists\n", authFile);
		return 255;
	}
//    else if (file_create(authFile,temporary_IV,sizeof(temporary_IV)) < 0) /* create after we have final filename */ {
      else if (save_key(authFile, text_iv, text_key) > 0) {
		/* couldn't create the bank auth file */
//		fprintf(stderr,"couldn't create the bank file \n");
		return 255;
	}
    else {
		fprintf(stdout,"created\n");
		fflush(stdout);
	}
	/* Initialize the server address to zero */
   memset(&serv_addr, '0', sizeof(serv_addr));

   serv_addr.sin_family = AF_INET;
   inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr);
   serv_addr.sin_port = htons(port);

	int optval;
	if(setsockopt(listenfd, SOL_SOCKET,SO_REUSEADDR, &optval,sizeof(optval)) < 0) {
      /* failed to set socket options */
      return 255;
   }
   bind(listenfd, (struct sockaddr*)&serv_addr,sizeof(serv_addr));

   if(listen(listenfd, 10) == -1){
      /* Failed to get a socket to listen on */
//		fprintf(stderr,"Failed to get listening socket\n");
      return 255; /* not explicitly in the spec */
   }

	/* We're doing this - let's create the initial account database */
	Account_T *accountdb=NULL;

	//fprintf(stderr,"Listening on port %d\n",port);
	//fprintf(stderr,"Using Authfile %s\n",authFile);


		   /* Set up test encryption stuff */
   unsigned char *key = (unsigned char *)text_key;
  unsigned char *iv = (unsigned char *)text_iv;

    
    // Start listening here.
  while(run) 
   {
  
      connfd = accept(listenfd, (struct sockaddr*)NULL ,NULL); // accept awaiting request

      unsigned char *decryptedtext;
		int decryptedtext_len;

		struct timeval timeout;
		timeout.tv_sec = 10;
		timeout.tv_usec = 0;

		if(setsockopt(connfd, SOL_SOCKET,SO_RCVTIMEO, (char *)&timeout,sizeof(timeout)) < 0) {
      /* failed to set socket options */
      	return 255;
   	}
		if(setsockopt(connfd, SOL_SOCKET,SO_SNDTIMEO, (char *)&timeout,sizeof(timeout)) < 0) {
      /* failed to set socket options */
      	return 255;
   	}

			if(readable_timeo(connfd) == 0) {
				/* socket timeout */
				//fprintf(stderr,"bank: detected timeout\n");
				fprintf(stdout,"protocol_error\n");
				fflush(stdout);
				continue;
			}
	
			errno=0;			
         decryptedtext_len = read_enc_socket(connfd, key, iv, &decryptedtext);
         if (decryptedtext_len < 0 )
         {
				if(errno == EWOULDBLOCK) {
					//fprintf(stderr,"Socket timeout WOULDBLOCK\n");
					fprintf(stdout,"protocol_error\n");
					fflush(stdout);
					continue;
				}
            /*failure*/
            //fprintf(stderr, "bank: failure in decryption\n");
				fprintf(stdout,"protocol_error\n");
				fflush(stdout);
           	close(connfd);
           	continue;
         }
         decryptedtext = (unsigned char *)realloc(decryptedtext, decryptedtext_len+1 * sizeof(unsigned char));

			long long balance = 0;

			NProtocol *transmitbin;
			transmitbin = (NProtocol *)decryptedtext;
//			fprintf(stderr,"bank: transmitbin->Name: %s\n",transmitbin->Name);
//			fprintf(stderr,"bank: transmitbin->amount: %lld\n",transmitbin->amount);
//			fprintf(stderr,"bank: transmitbin->operation: %d\n",transmitbin->operation);
//			fprintf(stderr,"bank: transmitbin->md5sum: %s\n",transmitbin->md5sum);
		
			/* do stuff with the values */
   		if (transmitbin->operation == NEW_ACCT) {
				/* add account to linked list */
//				fprintf(stderr,"Creating new account %s with %lld\n",transmitbin->Name, transmitbin->amount);
				if(transmitbin->amount <1000) {
					/* new balance must be >= 10 */
					balance = -1;
					write_enc_socket(connfd, (unsigned char *) &balance, sizeof(double), key, iv);
					continue;
				}	
				Account_T *newaccountdb;
				newaccountdb = add_account(accountdb,transmitbin->amount,transmitbin->Name);
				if(newaccountdb == accountdb) {
					/* failure to create account */
					balance = -1;
					write_enc_socket(connfd, (unsigned char *) &balance, sizeof(double), key, iv);
					continue;
				}
				accountdb = newaccountdb;
				balance = transmitbin->amount;
		   }
		   if (transmitbin->operation == DEPOSIT) {
//				fprintf(stderr,"depositing %lld to account %s\n",transmitbin->amount, transmitbin->Name);
				if(transmitbin->amount <=0) {
					/* deposit must be > 0 */
					balance = -1;
					write_enc_socket(connfd, (unsigned char *) &balance, sizeof(double), key, iv);
					continue;
				}	
				/* modify the entry */
				Account_T *newaccountdb;
				newaccountdb = modify_account(accountdb,0,transmitbin->amount,transmitbin->Name);
				if (!newaccountdb) {
					/* Failure to modify account */
					balance = -1;
					write_enc_socket(connfd, (unsigned char *) &balance, sizeof(double), key, iv);
					continue;
				}

				accountdb = newaccountdb;
				
		   }
		   if (transmitbin->operation == WITHDRAW) {
//				fprintf(stderr,"withdrawing %lld from account %s\n",transmitbin->amount, transmitbin->Name);
				/* modify the entry */
				if(transmitbin->amount <=0) {
					/* withdrawal must be > 0 */
					balance = -1;
					write_enc_socket(connfd, (unsigned char *) &balance, sizeof(double), key, iv);
					continue;
				}	
				Account_T *newaccountdb;
				newaccountdb = modify_account(accountdb,1,transmitbin->amount,transmitbin->Name);
				if (!newaccountdb) {
					/* Failure to modify account */
					balance = -1;
					write_enc_socket(connfd, (unsigned char *) &balance, sizeof(double), key, iv);
					continue;
				}
				accountdb = newaccountdb;
		   }
 		  if (transmitbin->operation == BALANCE) {
				/* just return the balance information */
//				fprintf(stderr,"printing status for account %s\n", transmitbin->Name);
				Account_T *account;
				account = find_account(accountdb, transmitbin->Name);
				if(!account) {
					/* Failure to find account */
					balance = -1;
					write_enc_socket(connfd, (unsigned char *) &balance, sizeof(double), key, iv);
					continue;
				
				}
				balance = account->Balance;
		   }
			


			/* send results back to the client */
			/* Assuming we are successful here and haven't errored out */
			write_enc_socket(connfd, (unsigned char *) &balance, sizeof(double),key, iv);
		//	fprintf(stderr,"Balance is %lld\n",balance);
			   /* print to stdout */
			char value_to_print[2048]; /* is this long enough?? */
			memset(value_to_print,'\0',sizeof(char) * 2048);
   		if (transmitbin->operation == NEW_ACCT) {
				printll(balance, (char *)&value_to_print);	
      		fprintf(stdout,"{\"account\":\"%s\",\"initial_balance\":%s}\n",transmitbin->Name,value_to_print);
		      fflush(stdout);
		   }
		   if (transmitbin->operation == DEPOSIT) {
				printll(transmitbin->amount, (char *)&value_to_print);	
		      fprintf(stdout,"{\"account\":\"%s\",\"deposit\":%s}\n",transmitbin->Name,value_to_print);
		      fflush(stdout);
		   }
		   if (transmitbin->operation == WITHDRAW) {
				printll(transmitbin->amount, (char *)&value_to_print);	
		      fprintf(stdout,"{\"account\":\"%s\",\"withdraw\":%s}\n",transmitbin->Name,value_to_print);
		      fflush(stdout);
		   }
 		  if (transmitbin->operation == BALANCE) {
				printll(balance, (char *)&value_to_print);	
		      fprintf(stdout,"{\"account\":\"%s\",\"balance\":%s}\n",transmitbin->Name,value_to_print);
		      fflush(stdout);
		   }
			
			if(decryptedtext)
				free(decryptedtext);

         close(connfd);    /* done with this client */
   }
	close(connfd);
 
   file_delete(authFile);
   return 0; /* we have failure of some kind */  
	
}

