/* From Richard Stevens: Unix Network Programming */
#ifndef UNP_H
#define UNP_H


#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>

#define MAXREADLINE 4096

/* read n bytes from a descriptor */
ssize_t readn(int fd, void *vptr, size_t n);

/* Write n bytes to the descriptor */
ssize_t writen(int fd, const void *vptr, size_t n);

/* Read until the newline (like fgets() ) */
ssize_t readline(int fd, void *vptr, size_t maxlen);

/* check for timeout */
int readable_timeo(int fd);

#endif
