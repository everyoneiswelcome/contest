/* From Richard Stevens: Unix Network Programming */
#include "unp.h"
#include <openssl/bio.h>

/* read n bytes from a descriptor */
ssize_t readn(int fd, void *vptr, size_t n)
{
	size_t nleft;
	ssize_t nread;
	char *ptr;

	ptr = vptr;
	nleft = n;
	while (nleft >0) {
		if ( (nread = read(fd, ptr, nleft)) < 0) {
			if (errno == EINTR) 
				nread = 0;
			else
				return (-1);
		} else if (nread == 0)
			break;  /* EOF */

		nleft -= nread;
		ptr += nread;
	}
//	fprintf(stderr, "Read from network:\n");
//	BIO_dump_fp (stderr, (const char *)ptr, n-nleft);
	
	return (n - nleft);  /* return >= 0 */
}

/* Write n bytes to the descriptor */
ssize_t writen(int fd, const void *vptr, size_t n)
{
	size_t nleft;
	ssize_t nwritten;
	const char *ptr;
	
	ptr = vptr;
	nleft = n;
//	fprintf(stderr,"Writing to the network:\n");
//	BIO_dump_fp (stderr, (const char *)vptr, n);
	while (nleft > 0) {
		if ( (nwritten = write(fd, ptr, nleft)) <= 0) {
			if (errno == EINTR)
				nwritten = 0;  /* And call write again */
			else
				return (-1);  /* error */
		}
		nleft -= nwritten;
		ptr += nwritten;
	}
	return (n);
}

static ssize_t my_read (int fd, char *ptr)
{
	static int read_cnt = 0;
	static char *read_ptr;
	static char read_buf[MAXREADLINE];

	if (read_cnt <= 0) {
		again:
			if ((read_cnt = read(fd, read_buf, sizeof(read_buf))) < 0 ) {
				if (errno == EINTR)
					goto again;
				return (-1);
			} else if (read_cnt == 0)
				return (0);
			read_ptr = read_buf;
	}
	read_cnt--;
	*ptr = *read_ptr++;
	return (1);
}

ssize_t readline(int fd, void *vptr, size_t maxlen)
{
	ssize_t n, rc;
	char c, *ptr;
	
	ptr = vptr;
	for (n = 1; n < maxlen; n++) {
		if ((rc=my_read(fd,&c)) == 1) {
			*ptr++ = c;
			if (c == '\n')
				break;	/* Newline is stored, like fgets() */
		} else if (rc == 0) {
			if (n == 1)
				return (0);  /* EOF, no data read */
			else
				break;		/* EOF, some data read */
		} else
			return (-1);	/* error, errno set by read() */
	}
	
	*ptr = 0;	/* null terminate like fgets() */
	return (n);
}

int readable_timeo(int fd)
{
	fd_set rset;
	struct timeval tv;
	
	FD_ZERO(&rset);
	FD_SET(fd,&rset);

	tv.tv_sec = 10;
	tv.tv_usec = 0;

	return (select(fd+1,&rset, NULL,NULL, &tv));
		/* > 0 if descriptor is readable */
}
