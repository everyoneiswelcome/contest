#ifndef HELPER_FUNCS_H
#define HELPER_FUNCS_H

#define NEW_ACCT 1
#define DEPOSIT 2
#define WITHDRAW 3
#define BALANCE 4

#define KEY_SIZE 32
#define IV_SIZE 16

/* MAB = MAX ACCOUNT BALANCE as defined by the spec is max of long long int (9223372036854775807) */
#define MAB 9223372036854775807

/* MAX ACCOUNT VALUE */
#define MAV 429496729599

/* MNL = MAX NAME LENGTH as defined by the spec 250 characters */
#define MNL 251 /* added one for the null character*/

#define MD5_SIZE 32

typedef struct NetAccount {
	char Name[MNL];
	char md5sum[33];  
	long long amount;
	int operation;
} NProtocol;

int isValidFileName (char* filename); 
int isValidAccount (char* account);
double lltod(long long input); 
long long dtoll(double input);
void printll(long long input, char * output);
long long str_to_ll(char *input);
unsigned int random_uint();
/* MD5 STUFF */
void md5_build(char *str, char *md5s);
int md5_compare(char *name, char *md5s);
/* key stuff */
void gen_key(int size, char *key);
int save_key(char *path, char *iv, char *key);
int get_key(char *path, char *iv, char *key);

#endif
