#ifndef ENC_NETWORK_H
#define ENC_NETWORK_H

#include <openssl/evp.h>
#include "unp.h"

void handleErrors(void);

/* Write to the encrypted network socket */
/* returns the length of the cipher text */
int write_enc_socket(int writesocket, unsigned char *plaintext, int plaintext_len, unsigned char *key, unsigned char *iv);

/* Read from the encrypted network socket */
/* returns a void pointer to the read data (allocated by the function) */
int read_enc_socket(int readsocket, unsigned char *key, unsigned char *iv, unsigned char **plaintext); 

#endif
