#include <openssl/evp.h>
#include <openssl/err.h>
#include "unp.h"

void handleErrors(void)
{
  ERR_print_errors_fp(stderr);
  abort();
}

/* Write to the encrypted network socket */
/* returns the length of the cipher text */
int write_enc_socket(int writesocket, unsigned char *plaintext, int plaintext_len, unsigned char *key, unsigned char *iv)	{

	EVP_CIPHER_CTX *ctx;

	int len;
	
	int ciphertext_len;
	
	unsigned char *ciphertext;
	unsigned char tag[15];
	
	ciphertext = (unsigned char *)malloc(plaintext_len+16);
	if (ciphertext == NULL) {
		/*error */
		return -1;
	}

	/* Create and initialise the context */
	if(!(ctx = EVP_CIPHER_CTX_new())) {
		/* error */
		handleErrors();
	}

	/* Initialize the encryption operation. */
	if(1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_ccm(), NULL, NULL, NULL)) {
		/* error in OpenSSL */
		handleErrors();
	}

	/* Setting IV len to 7. Not strictly necessary as this is the default */

	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_SET_IVLEN, 7, NULL)) {
		/* error */
		handleErrors();
	}

	/* Set tag length */
	EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_SET_TAG, 14, NULL);

	/* Initialise key and IV */
	if(1 != EVP_EncryptInit_ex(ctx, NULL, NULL, key, iv)) {
		/* error */
		handleErrors();
	}

	/* Provide the total plaintext length
	 */
	if(1 != EVP_EncryptUpdate(ctx, NULL, &len, NULL, plaintext_len)) {
		/* error */
		handleErrors();
	}


	/* Provide the message to be encrypted, and obtain the encrypted output.
	 * EVP_EncryptUpdate can only be called once for this
	 */
	if(1 != EVP_EncryptUpdate(ctx, ciphertext, &len, plaintext, plaintext_len)) {
		/* error */
		handleErrors();
	}
	ciphertext_len = len;

	/* Finalise the encryption. Normally ciphertext bytes may be written at
	 * this stage, but this does not occur in CCM mode
	 */
	if(1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))  {
		/*error */
		handleErrors();
	}
	ciphertext_len += len;

	/* Get the tag */
	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_GET_TAG, 14, tag)) {
		/* error */
		handleErrors();
	}

	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);

	//fprintf(stderr,"Ciphertext length is %d\n", ciphertext_len);
 	//fprintf(stderr,"Ciphertext is:\n");
  	//BIO_dump_fp (stderr, (const char *)ciphertext, ciphertext_len);
	//fprintf(stderr,"Tag is:\n");
 	//BIO_dump_fp (stderr, (const char *)tag, 15);
	

	/* we have the 14 character tag, and the ciphertext and it's length */
	/* Write the length first */
	writen(writesocket, &ciphertext_len, sizeof(int));
	writen(writesocket, tag, 14); /* write the tag next */	
	writen(writesocket, ciphertext, ciphertext_len); /* Then write the ciphertext */
	return ciphertext_len;

}


/* Read from the encrypted network socket */
/* returns plaintext length */
int read_enc_socket(int readsocket, unsigned char *key, unsigned char *iv, unsigned char **plaintext)
{
	EVP_CIPHER_CTX *ctx;
	int ciphertext_len;
	int plaintext_len;
	unsigned char tag[15];
	int ret;
	int len; /* plain text length */
	char recvBuff[MAXREADLINE];
	unsigned char *ciphertext;
	int n; /* counter */

	/* Create and initialise the context */
	if(!(ctx = EVP_CIPHER_CTX_new()))  {
		/*error */
		handleErrors();
	}

	/* Initialise the decryption operation. */
	if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_ccm(), NULL, NULL, NULL)) {
		/* error */
		handleErrors();
	}

	/* Setting IV len to 7. Not strictly necessary as this is the default
	 * but shown here for the purposes of this example */
	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_SET_IVLEN, 7, NULL)) {
		/*error */	
		handleErrors();
	}	



	/* Read in network data and get length and ciphertext */
	 	n = readn(readsocket, &ciphertext_len, sizeof(int));
	 	if (n == 0)
			return -2;  /* connection closed by client */
		if (ciphertext_len <= 0) {
			/* failure to read length */
			return -1;
		}
		
		/* read in the tag */
		bzero(recvBuff,MAXREADLINE);
		n = readn(readsocket, &tag, 14); /* all tags are 14 */
		if (n == 0)
			return -2; /*connection closed by client */
	

	//fprintf(stderr,"Ciphertext length is %d\n", ciphertext_len);
	//fprintf(stderr,"Tag is:\n");
 	//BIO_dump_fp (stderr, (const char *)tag, 14);

	/* Set expected tag value. */
	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_SET_TAG, 14, tag)) {
		/*error */
		handleErrors();
	}

	/* Initialise key and IV */
	if(1 != EVP_DecryptInit_ex(ctx, NULL, NULL, key, iv)) {
		/* error */
		handleErrors();
	}

		/* read in from the socket MAXREADLINE at a time */
		int nread = 0;
		int toreadtotal = ciphertext_len;
		int toread;
		ciphertext = NULL;
		while (toreadtotal > 0) {
			/* keep reading in MAXREADLINE at a time until we've reached the ciphertext_len */
			bzero(recvBuff,MAXREADLINE);
			/* how much to read this time? */
			if (toreadtotal > MAXREADLINE)
				toread = MAXREADLINE;	
			else
				toread = toreadtotal;
//			fprintf(stderr, "expecting to read %d\n", toread);
			if(toread == 0) {
				return -1;
			}

			n = readn(readsocket, recvBuff, toread);
			if (n <= 0) 
				return -2;
//			fprintf(stderr, "Read in n = %d bytes\n",n);
		
			/* allocate n bytes of memory for the ciphertext */	
			ciphertext = (unsigned char *)realloc(ciphertext, sizeof(unsigned char)*n);
			if (ciphertext == NULL) {
				/*failure */
				//fprintf(stderr, "Failure to allocate memory for ciphertext\n");
				return -1;
			}	
			memcpy(ciphertext,recvBuff,toread);
			nread += n;
//			fprintf(stderr,"nread = %d\n",nread);
			toreadtotal -= n;
//			fprintf(stderr,"totaltoread = %d\n",toreadtotal);
			
		}
		ciphertext_len = nread;
		
 //	fprintf(stderr,"Ciphertext is:\n");
 // 	BIO_dump_fp (stderr, (const char *)ciphertext, ciphertext_len);

	/* Provide the total ciphertext length
	 */
	if(1 != EVP_DecryptUpdate(ctx, NULL, &len, NULL, ciphertext_len)) {
		/* error */
		handleErrors();
	}
	
	*plaintext = (unsigned char *)malloc(sizeof(unsigned char)*ciphertext_len);

	/* Provide the message to be decrypted, and obtain the plaintext output.
	 * EVP_DecryptUpdate can be called multiple times if necessary
	 */
	ret = EVP_DecryptUpdate(ctx, *plaintext, &len, ciphertext, ciphertext_len);

	plaintext_len = len;

	/* Clean up */
	EVP_CIPHER_CTX_free(ctx);
	if (ciphertext != NULL)
		free(ciphertext);

	if(ret > 0)
	{
		/* Success */
//		fprintf(stderr,"Success decrypting\n");
		
		return plaintext_len;
	}
	else
	{
		/* Verify failed */
		//fprintf(stderr, "Verification failure\n");
		return -1;
	}
}

