#include <openssl/evp.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <openssl/bio.h>


#define MAX_BUF_LEN  (128)


int main(int argc, char *argv[])
{
  BIO *b=NULL;
  EVP_MD_CTX c;
  const EVP_MD *md;
  unsigned char buf[MAX_BUF_LEN];
  int fd=0;
  int len,i=0;


  unsigned char hash[EVP_MAX_MD_SIZE];


  /* File name is required */
  if( argc < 2 ) {
     fprintf(stderr,"Usage: %s <filename>\n",argv[0]);
      return 1;
  }


  b=BIO_new_file(argv[1], "r");
  if( b == NULL ){
      fprintf(stderr,"ERROR: Unable to open file '%s'\n",argv[1]);
      return 1;
  }


  OpenSSL_add_all_digests();


  md = EVP_get_digestbyname("md5");


  EVP_MD_CTX_init(&c);
  EVP_DigestInit_ex(&c, md, NULL);


  /* Read file in MAX_BUF_LEN chunks
     and pass it to update functions
   */
  while ( len=BIO_read(b,buf,MAX_BUF_LEN) )
      EVP_DigestUpdate(&c,buf,len);


  EVP_DigestFinal_ex(&c, hash, &len);
  EVP_MD_CTX_cleanup(&c);
  BIO_free(b);


  fprintf(stdout,"MD5(%s)= ",argv[1]);
  while ( i<len) {
     fprintf(stdout,"%02x",(unsigned int)hash[i]);
     i++;
  }


  printf("\n");
  close(fd);
}
