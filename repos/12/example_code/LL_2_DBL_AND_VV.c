#include <stdlib.h>
#include <stdio.h>

int main(){
    unsigned long long int Balance = 129768523;
    double Balance_D = ((double) Balance)/100;

     printf("Double: %f\n", Balance_D);

     double Balance_DB = 1297685.23;
     long long int Balance_New = (long long) (Balance_DB * 100);
    
     printf("Long Long: %lli\n", Balance_New);
}