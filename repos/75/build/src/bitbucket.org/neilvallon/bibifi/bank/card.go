package bank

import "crypto/rand"

type CardStrip [16]byte

func NewCard() (c CardStrip, err error) {
	_, err = rand.Read(c[:])
	return c, err
}
