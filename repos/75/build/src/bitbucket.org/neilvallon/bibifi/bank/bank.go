package bank

import "sync"

type Bank struct {
	sync.RWMutex
	accounts map[string]*Account
}

func NewBank() *Bank {
	return &Bank{
		accounts: make(map[string]*Account),
	}
}

func (b *Bank) makeAccount(name string, cardData CardStrip, ballance uint64) (*Account, error) {
	if _, ok := b.accounts[name]; ok {
		return nil, AccountExists
	}

	a := NewAccount(cardData, ballance)
	b.accounts[name] = a

	return a, nil
}

func (b *Bank) rollbackMakeAccount(name string) {
	delete(b.accounts, name)
}

func (b *Bank) GetAccount(name string, cardData CardStrip) (*Account, error) {
	b.RLock()
	defer b.RUnlock()

	a, ok := b.accounts[name]
	if !ok {
		return nil, NoAccount
	}

	if cardData != a.cardData {
		return nil, BadCard
	}

	return a, nil
}

func (b *Bank) NewAccountTransaction(name string, card CardStrip, val uint64) Transaction {
	return createTx{
		b,
		name, card, val,
	}
}
