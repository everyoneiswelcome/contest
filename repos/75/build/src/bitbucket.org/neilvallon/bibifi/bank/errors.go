package bank

type bankError int

const (
	BalanceLimitReached bankError = iota
	InsufficientFunds

	AccountExists
	NoAccount
	BadCard
)

func (err bankError) Error() string {
	switch err {
	case BalanceLimitReached:
		return "account: cannot deposit funds. balance limit reached"
	case InsufficientFunds:
		return "account: insufficient funds"

	case AccountExists:
		return "bank: cannot create account that already exists"
	case NoAccount:
		return "bank: could not find account"
	case BadCard:
		return "bank: incorrect card data"

	default:
		return "unknown error"
	}
}
