package bank

import "sync"

const (
	MaxBalance       = 429496729599
	MinCreateBalance = 10 * 100
)

type Account struct {
	cardData CardStrip

	sync.RWMutex
	balance uint64
}

func NewAccount(data CardStrip, val uint64) *Account {
	return &Account{
		cardData: data,
		balance:  val,
	}
}

func (a *Account) deposit(val uint64) error {
	if a.balance+val < a.balance {
		return BalanceLimitReached
	}

	a.balance += val
	return nil
}

func (a *Account) rollbackDeposit(val uint64) {
	a.balance -= val
}

func (a *Account) withdraw(val uint64) error {
	if a.balance < val {
		return InsufficientFunds
	}

	a.balance -= val
	return nil
}

func (a *Account) rollbackWithdraw(val uint64) {
	a.balance += val
}

func (a *Account) getBalance() uint64 {
	return a.balance
}

func (a *Account) NewDepositTransaction(val uint64) Transaction {
	return depositTx{a, val}
}

func (a *Account) NewWithdrawTransaction(val uint64) Transaction {
	return withdrawTx{a, val}
}

func (a *Account) NewBalanceTransaction() Transaction {
	return balanceTx{a}
}
