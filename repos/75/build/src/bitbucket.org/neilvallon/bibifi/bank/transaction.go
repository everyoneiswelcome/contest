package bank

type Transaction interface {
	Exec() (uint64, error)
	Confirm()
	Rollback()
}

// Create
type createTx struct {
	bank *Bank

	name string
	card CardStrip
	val  uint64
}

func (tx createTx) Exec() (uint64, error) {
	tx.bank.Lock()

	_, err := tx.bank.makeAccount(tx.name, tx.card, tx.val)
	if err != nil {
		tx.bank.Unlock()
		return 0, err
	}

	return tx.val, nil
}

func (tx createTx) Confirm() {
	tx.bank.Unlock()
}

func (tx createTx) Rollback() {
	tx.bank.rollbackMakeAccount(tx.name)
	tx.bank.Unlock()
}

///// Deposit
type depositTx struct {
	acc *Account
	val uint64
}

func (tx depositTx) Exec() (uint64, error) {
	tx.acc.Lock()

	if err := tx.acc.deposit(tx.val); err != nil {
		tx.acc.Unlock()
		return 0, err
	}

	return tx.val, nil
}

func (tx depositTx) Confirm() {
	tx.acc.Unlock()
}

func (tx depositTx) Rollback() {
	tx.acc.rollbackDeposit(tx.val)
	tx.acc.Unlock()
}

///// Withdraw
type withdrawTx struct {
	acc *Account
	val uint64
}

func (tx withdrawTx) Exec() (uint64, error) {
	tx.acc.Lock()

	if err := tx.acc.withdraw(tx.val); err != nil {
		tx.acc.Unlock()
		return 0, err
	}

	return tx.val, nil
}

func (tx withdrawTx) Confirm() {
	tx.acc.Unlock()
}

func (tx withdrawTx) Rollback() {
	tx.acc.rollbackWithdraw(tx.val)
	tx.acc.Unlock()
}

///// Balance
type balanceTx struct {
	acc *Account
}

func (tx balanceTx) Exec() (uint64, error) {
	tx.acc.RLock()
	return tx.acc.getBalance(), nil
}

func (tx balanceTx) Confirm() {
	tx.acc.RUnlock()
}

func (tx balanceTx) Rollback() {
	tx.acc.RUnlock()
}
