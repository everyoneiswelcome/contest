package parse

import "strconv"

// ParseNumber parses numbers matching (0)|([1-9][0-9]*)
func Number(num string) (int, error) {
	l := len(num)
	if l < 1 {
		return 0, NumberParseError // no number
	}

	// non-numeric or leading 0's
	if f := num[0]; (f < '1' || '9' < f) && (f != '0' || l != 1) {
		return 0, NumberParseError
	}

	return strconv.Atoi(num)
}
