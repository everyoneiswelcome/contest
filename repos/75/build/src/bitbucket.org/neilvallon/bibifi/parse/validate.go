package parse

func ValidateCharSet(name string) error {
	for _, c := range name {
		if !(('a' <= c && c <= 'z') || ('0' <= c && c <= '9') || c == '_' || c == '-' || c == '.') {
			return CharSetError
		}
	}

	return nil
}

func Account(name string) error {
	if len(name) < 1 || 250 < len(name) {
		return AccountLengthError
	}

	return ValidateCharSet(name)
}

func File(name string) error {
	if len(name) < 1 || 255 < len(name) {
		return FileLengthError
	}

	if name == "." || name == ".." {
		return CharSetError
	}

	return ValidateCharSet(name)
}
