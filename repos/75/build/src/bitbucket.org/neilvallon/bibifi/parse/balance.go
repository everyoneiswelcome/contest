package parse

import "strconv"

// TODO: cleanup
func Balance(bal string) (uint64, error) {
	// quick check length
	l := len(bal)
	if l < 4 || 13 < l { // len("0.00"), len("4294967294.50")
		return 0, BalanceParseError
	}

	// verify 'whole.fract' split
	split := l - 3 // dot position
	if bal[split] != '.' {
		return 0, BalanceParseError
	}

	// parse
	whole, err := Number(bal[:split])
	if err != nil {
		return 0, err
	}

	/// TODO: find an easier way to do this
	fractStr := bal[split+1:]
	if fractStr[0] == '+' || fractStr[0] == '-' {
		return 0, BalanceParseError
	}

	fract, err := strconv.Atoi(fractStr)
	if err != nil {
		return 0, err
	}
	////

	// validate range
	if 4294967295 < whole {
		return 0, BalanceRangeError
	}

	// cents range is implicitly valid by only containing 2 characters

	return uint64(whole)*100 + uint64(fract), nil
}
