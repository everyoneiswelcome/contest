package parse

type parseError int

const (
	BalanceParseError parseError = iota
	BalanceRangeError

	NumberParseError
	PortRangeError
	BadIPError

	AccountLengthError
	FileLengthError

	FileReservedError

	CharSetError
)

func (err parseError) Error() string {
	switch err {
	case BalanceParseError:
		return "balances must match ([0-9]+).[0-9]{2}"
	case BalanceRangeError:
		return "balances are bounded from 0.00 to 4294967295.99"
	case NumberParseError:
		return "numeric inputs should match (0|[1-9][0-9]*)"
	case PortRangeError:
		return "ports are specified as numbers between 1024 and 65535 inclusively"
	case BadIPError:
		return "ip parse error"
	case AccountLengthError:
		return "account names are between 1 and 250 characters inclusively"
	case FileLengthError:
		return "file names are between 1 and 255 characters inclusively"
	case FileReservedError:
		return "file names are between 1 and 255 characters inclusively"
	case CharSetError:
		return "special file names '.' and '..' are not allowed"
	}
	return "unknown error"
}
