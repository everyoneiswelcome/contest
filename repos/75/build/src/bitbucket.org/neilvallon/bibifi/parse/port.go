package parse

func Port(p string) (int, error) {
	n, err := Number(p)
	if err != nil {
		return 0, err
	}

	if n < 1024 || 65535 < n {
		return 0, PortRangeError
	}

	return n, nil
}

func IP(ip string) error {
	var segs [5]int
	segs[4] = len(ip) + 1

	pos := 1
	for i := 0; i < len(ip) && pos < 4; i++ {
		if ip[i] == '.' {
			segs[pos] = i + 1
			pos++
		}
	}

	for i := 1; i < len(segs); i++ {
		if segs[i] == 0 {
			return BadIPError
		}

		start, end := segs[i-1], segs[i]
		if n, err := Number(ip[start : end-1]); err != nil || 255 < n {
			return BadIPError
		}
	}

	return nil
}
