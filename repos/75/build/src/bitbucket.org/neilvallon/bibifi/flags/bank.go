package flags

import "bitbucket.org/neilvallon/bibifi/parse"

type BankFlags struct {
	AuthFile string
	Port     string
}

func ParseBank(args []string) (f BankFlags, err error) {
	var i int
	for i < len(args) {
		var flag, val string
		switch a := args[i]; len(a) {
		case 0, 1:
			return BankFlags{}, BadFlag
		case 2:
			if len(args) <= i+1 {
				return BankFlags{}, MissingArgumentValue
			}

			flag = a
			val = args[i+1]
			i += 2
		default:
			flag = a[:2]
			val = a[2:]
			i++
		}

		if val == "" {
			return BankFlags{}, MissingArgumentValue
		}

		switch flag {
		default:
			return BankFlags{}, BadFlag
		case "-s":
			if f.AuthFile != "" {
				return BankFlags{}, DuplicateFlag
			}
			f.AuthFile = val
		case "-p":
			if f.Port != "" {
				return BankFlags{}, DuplicateFlag
			}
			f.Port = val
		}
	}

	return f, nil
}

type BankOptions struct {
	AuthFile string
	Port     int
}

func ParseBankOptions(f BankFlags) (BankOptions, error) {
	opt := BankOptions{
		AuthFile: "bank.auth",
		Port:     3000,
	}

	if f.Port != "" {
		var err error
		if opt.Port, err = parse.Port(f.Port); err != nil {
			return BankOptions{}, err
		}
	}

	if f.AuthFile != "" {
		if err := parse.File(f.AuthFile); err != nil {
			return BankOptions{}, err
		}
		opt.AuthFile = f.AuthFile
	}

	return opt, nil
}
