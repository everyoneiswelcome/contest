package flags

import (
	"bitbucket.org/neilvallon/bibifi/bank"
	"bitbucket.org/neilvallon/bibifi/parse"
	"bitbucket.org/neilvallon/bibifi/protocol"
)

type ATMFlags struct {
	AuthFile string

	IP   string
	Port string

	CardFile string

	Account string

	Action protocol.Action
	Val    string
}

func ParseATM(args []string) (f ATMFlags, err error) {
	var i int
	for i < len(args) {
		var flag, val string
		switch a := args[i]; len(a) {
		case 0, 1:
			return ATMFlags{}, BadFlag
		case 2:
			if a == "-g" { // special case
				flag = a
				i++
			} else if i+1 < len(args) {
				flag = a
				val = args[i+1]
				i += 2
			} else {
				return ATMFlags{}, MissingArgumentValue
			}
		default:
			flag = a[:2]
			val = a[2:]
			i++

			if flag == "-g" {
				return ATMFlags{}, BadFlag
			}
		}

		if val == "" && flag != "-g" {
			return ATMFlags{}, MissingArgumentValue
		}

		switch flag {
		default:
			return ATMFlags{}, BadFlag
		case "-s":
			if f.AuthFile != "" {
				return ATMFlags{}, DuplicateFlag
			}
			f.AuthFile = val
		case "-i":
			if f.IP != "" {
				return ATMFlags{}, DuplicateFlag
			}
			f.IP = val
		case "-p":
			if f.Port != "" {
				return ATMFlags{}, DuplicateFlag
			}
			f.Port = val
		case "-c":
			if f.CardFile != "" {
				return ATMFlags{}, DuplicateFlag
			}
			f.CardFile = val
		case "-a":
			if f.Account != "" {
				return ATMFlags{}, DuplicateFlag
			}
			f.Account = val

		case "-n":
			if f.Action != protocol.NoAction {
				return ATMFlags{}, DuplicateAction
			}
			f.Action, f.Val = protocol.ActionCreate, val
		case "-d":
			if f.Action != protocol.NoAction {
				return ATMFlags{}, DuplicateAction
			}
			f.Action, f.Val = protocol.ActionDeposit, val
		case "-w":
			if f.Action != protocol.NoAction {
				return ATMFlags{}, DuplicateAction
			}
			f.Action, f.Val = protocol.ActionWithdraw, val
		case "-g":
			if f.Action != protocol.NoAction {
				return ATMFlags{}, DuplicateAction
			}
			f.Action = protocol.ActionGet
		}
	}

	return f, nil
}

type ATMOptions struct {
	AuthFile string

	IP   string
	Port int

	CardFile string

	Account string

	Action protocol.Action
	Val    uint64
}

func ParseATMOptions(f ATMFlags) (ATMOptions, error) {
	if f.Action == protocol.NoAction {
		return ATMOptions{}, NoActionErr
	}

	if err := parse.Account(f.Account); err != nil {
		return ATMOptions{}, err
	}

	opt := ATMOptions{
		Action: f.Action,

		Account:  f.Account,
		AuthFile: "bank.auth",
		IP:       "127.0.0.1",
		Port:     3000,
	}

	if f.Port != "" {
		var err error
		if opt.Port, err = parse.Port(f.Port); err != nil {
			return ATMOptions{}, err
		}
	}

	if f.IP != "" {
		if err := parse.IP(f.IP); err != nil {
			return ATMOptions{}, err
		}
		opt.IP = f.IP
	}

	if f.Action != protocol.ActionGet {
		var err error
		if opt.Val, err = parse.Balance(f.Val); err != nil {
			return ATMOptions{}, err
		}

		if opt.Val == 0 || (f.Action == protocol.ActionCreate && opt.Val < bank.MinCreateBalance) {
			return ATMOptions{}, MinBalanceError
		}
	}

	if f.AuthFile != "" {
		if err := parse.File(f.AuthFile); err != nil {
			return ATMOptions{}, err
		}
		opt.AuthFile = f.AuthFile
	}

	if f.CardFile != "" {
		if err := parse.File(f.CardFile); err != nil {
			return ATMOptions{}, err
		}
		opt.CardFile = f.CardFile
	} else {
		opt.CardFile = f.Account + ".card"
	}

	return opt, nil
}
