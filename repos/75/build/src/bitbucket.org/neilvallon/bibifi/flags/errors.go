package flags

type parseError int

const (
	BadFlag parseError = iota

	MissingArgumentValue

	DuplicateFlag
	DuplicateAction

	NoActionErr

	MinBalanceError
)

func (err parseError) Error() string {
	switch err {
	case BadFlag:
		return "flag length error"

	case MissingArgumentValue:
		return "no argument value"

	case DuplicateFlag:
		return "duplicate argument flag"
	case DuplicateAction:
		return "duplicate action flags"

	case NoActionErr:
		return "no action error"

	case MinBalanceError:
		return "balance range error"
	}
	return "unknown error"
}
