package main

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"

	"bitbucket.org/neilvallon/bibifi/bank"
	"bitbucket.org/neilvallon/bibifi/flags"
	"bitbucket.org/neilvallon/bibifi/protocol"
)

func init() {
	log.SetOutput(ioutil.Discard)
}

func main() {
	f, err := flags.ParseATM(os.Args[1:])
	if err != nil {
		log.Println(err)
		os.Exit(255)
	}
	opt, err := flags.ParseATMOptions(f)
	if err != nil {
		log.Println(err)
		os.Exit(255)
	}

	// get card
	var card bank.CardStrip
	if opt.Action != protocol.ActionCreate {
		card, err = readCard(opt.CardFile)
	} else {
		if _, ferr := os.Stat(opt.CardFile); !os.IsNotExist(ferr) {
			err = errors.New("card exists")
		}
	}

	if err != nil {
		log.Println(err)
		os.Exit(255)
	}

	//
	caPool := x509.NewCertPool()
	cert, err := ioutil.ReadFile(opt.AuthFile)
	if err != nil {
		log.Println(err)
		os.Exit(255)
	}
	caPool.AppendCertsFromPEM(cert)
	//

	addr := fmt.Sprintf("%s:%d", opt.IP, opt.Port)

	dialer := net.Dialer{Timeout: protocol.Timeout}
	conn, err := tls.DialWithDialer(&dialer, "tcp", addr, &tls.Config{
		RootCAs:      caPool,
		CipherSuites: []uint16{tls.TLS_RSA_WITH_RC4_128_SHA},
	})
	if err != nil {
		log.Println(err)
		os.Exit(63)
	}

	c := protocol.NewClient(conn)

	switch opt.Action {
	case protocol.ActionCreate:
		card, err = bank.NewCard()
		err = c.MakeAccount(opt.Account, card, opt.Val)
		if err == nil {
			err = ioutil.WriteFile(opt.CardFile, card[:], 0644)
		}
	case protocol.ActionDeposit:
		err = c.Deposit(opt.Account, card, opt.Val)
	case protocol.ActionWithdraw:
		err = c.Withdraw(opt.Account, card, opt.Val)
	case protocol.ActionGet:
		err = c.Balance(opt.Account, card)
	default:
		err = errors.New("no action specified")
	}

	if err != nil {
		log.Println(err)
		os.Exit(255)
	}
}

func readCard(name string) (card bank.CardStrip, err error) {
	f, err := os.Open(name)
	if err != nil {
		return bank.CardStrip{}, err
	}
	defer f.Close()

	n, err := f.Read(card[:])

	if err != nil {
		return bank.CardStrip{}, err
	}

	if n != 16 {
		return bank.CardStrip{}, errors.New("bad card file")
	}

	return card, nil
}
