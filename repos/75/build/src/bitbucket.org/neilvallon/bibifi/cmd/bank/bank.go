package main

import (
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"bitbucket.org/neilvallon/bibifi/flags"
	"bitbucket.org/neilvallon/bibifi/protocol"
	"bitbucket.org/neilvallon/bibifi/protocol/auth"
)

func init() {
	log.SetOutput(ioutil.Discard)
}

func main() {
	f, err := flags.ParseBank(os.Args[1:])
	if err != nil {
		log.Println(err)
		os.Exit(255)
	}
	opt, err := flags.ParseBankOptions(f)
	if err != nil {
		log.Println(err)
		os.Exit(255)
	}

	//
	pemCert, pemKey, err := auth.MakeCert()
	if err != nil {
		log.Println(err)
		os.Exit(255)
	}

	if err := ioutil.WriteFile(opt.AuthFile, pemCert, 0644); err != nil {
		log.Println(err)
		os.Exit(255)
	}
	fmt.Println("created")

	///
	cert, err := tls.X509KeyPair(pemCert, pemKey)
	if err != nil {
		log.Println(err)
		os.Exit(255)
	}

	///////
	addr := fmt.Sprintf(":%d", opt.Port)

	ln, err := tls.Listen("tcp", addr, &tls.Config{
		Certificates: []tls.Certificate{cert},
		CipherSuites: []uint16{tls.TLS_RSA_WITH_RC4_128_SHA},
	})
	if err != nil {
		log.Println(err)
		os.Exit(255)
	}
	defer ln.Close()

	log.Printf("bank listening for connections at %q", addr)
	rpc := protocol.NewRPC()
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println(err)
		}

		go rpc.Handle(conn)

		conn.SetDeadline(time.Now().Add(protocol.Timeout))
	}
}
