package protocol

type protocolError int

const (
	InitBalanceRangeError protocolError = iota
	BalanceRangeError

	ZeroBalanceError
)

func (err protocolError) Error() string {
	switch err {
	case InitBalanceRangeError:
		return "initial balance must be greater than or equal to 10.00"
	case BalanceRangeError:
		return "balances are bounded from 0.00 to 4294967295.99"

	case ZeroBalanceError:
		return "deposit/withdraw amount must be greater than 0.00"
	}
	return "unknown error"
}
