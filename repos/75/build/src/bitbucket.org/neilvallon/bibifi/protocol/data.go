package protocol

import (
	"time"

	"bitbucket.org/neilvallon/bibifi/bank"
)

const Timeout = 10 * time.Second

type Request struct {
	Account string
	Card    bank.CardStrip

	Action Action
	Val    uint64
}

type Response struct {
	Val      uint64
	HasError bool
}
