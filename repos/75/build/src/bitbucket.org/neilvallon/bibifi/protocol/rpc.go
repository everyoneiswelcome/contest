package protocol

import (
	"fmt"
	"io"
	"log"
	"net"

	"bitbucket.org/neilvallon/bibifi/bank"
	"bitbucket.org/neilvallon/bibifi/parse"
)

type BankRPC struct {
	bank *bank.Bank
}

func NewRPC() BankRPC {
	return BankRPC{bank.NewBank()}
}

func (rpc BankRPC) Handle(conn net.Conn) {
	defer conn.Close()

	//// protobuf
	var req Request
	{
		var data [512]byte
		n, err := conn.Read(data[:])
		if err != nil && err != io.EOF {
			log.Println(err)
			fmt.Println("protocol_error")
			return
		}

		req, err = UnmarshalRequest(data[:n])
		if err != nil {
			log.Println(err)
			fmt.Println("protocol_error")
			return
		}
	}
	/////

	var tx bank.Transaction
	var txErr error
	switch req.Action {
	default:
		log.Println("bad action")
		fmt.Println("protocol_error")
		return
	case ActionCreate:
		tx, txErr = rpc.MakeAccount(req)
	case ActionDeposit:
		tx, txErr = rpc.Deposit(req)
	case ActionWithdraw:
		tx, txErr = rpc.Withdraw(req)
	case ActionGet:
		tx, txErr = rpc.Balance(req)
	}

	var res Response
	{
		var err error
		if txErr == nil {
			res.Val, err = tx.Exec()
		}
		res.HasError = txErr != nil || err != nil
	}

	if err := rpcReply(conn, res); err != nil {
		if !res.HasError {
			tx.Rollback()
		}

		log.Println(err)
		fmt.Println("protocol_error")
		return
	}

	if !res.HasError {
		printLog(req.Action, req.Account, res.Val)
		tx.Confirm()
	}
}

func rpcReply(conn net.Conn, res Response) error {
	data, err := MarshalResponse(res)
	if err != nil {
		return err
	}

	n, err := conn.Write(data)
	if err != nil || n != len(data) {
		return err
	}

	return nil
}

// MakeAccount creates a new bank account or returns an error if it can't
func (rpc BankRPC) MakeAccount(req Request) (bank.Transaction, error) {
	if req.Val < bank.MinCreateBalance {
		return nil, InitBalanceRangeError
	}
	if bank.MaxBalance < req.Val {
		return nil, BalanceRangeError
	}

	if err := parse.Account(req.Account); err != nil {
		return nil, err
	}

	return rpc.bank.NewAccountTransaction(req.Account, req.Card, req.Val), nil
}

func (rpc BankRPC) Deposit(req Request) (bank.Transaction, error) {
	if req.Val <= 0 {
		return nil, ZeroBalanceError
	}
	if bank.MaxBalance < req.Val {
		return nil, BalanceRangeError
	}

	a, err := rpc.bank.GetAccount(req.Account, req.Card)
	if err != nil {
		return nil, err
	}

	return a.NewDepositTransaction(req.Val), nil
}

func (rpc BankRPC) Withdraw(req Request) (bank.Transaction, error) {
	if req.Val <= 0 {
		return nil, ZeroBalanceError
	}

	a, err := rpc.bank.GetAccount(req.Account, req.Card)
	if err != nil {
		return nil, err
	}

	return a.NewWithdrawTransaction(req.Val), nil
}

func (rpc BankRPC) Balance(req Request) (bank.Transaction, error) {
	a, err := rpc.bank.GetAccount(req.Account, req.Card)
	if err != nil {
		return nil, err
	}

	return a.NewBalanceTransaction(), nil
}
