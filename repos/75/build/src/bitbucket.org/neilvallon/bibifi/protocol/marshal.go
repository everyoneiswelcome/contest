package protocol

import (
	"bitbucket.org/neilvallon/bibifi/bank"
	"bitbucket.org/neilvallon/bibifi/protocol/serial"
	"github.com/gogo/protobuf/proto"
)

func MarshalRequest(req Request) ([]byte, error) {
	a := serial.Request_Action(req.Action)

	return proto.Marshal(&serial.Request{
		Account: req.Account,
		Card:    req.Card[:],

		Action: a,

		Val: req.Val,
	})
}

func UnmarshalRequest(data []byte) (Request, error) {
	var r serial.Request
	if err := proto.Unmarshal(data, &r); err != nil {
		return Request{}, err
	}

	var card bank.CardStrip
	copy(card[:], r.Card)

	return Request{
		Account: r.Account,
		Card:    card,

		Action: Action(r.Action),

		Val: r.Val,
	}, nil
}

func MarshalResponse(res Response) ([]byte, error) {
	return proto.Marshal(&serial.Response{
		Val:      res.Val,
		HasError: res.HasError,
	})
}

func UnmarshalResponse(data []byte) (Response, error) {
	var r serial.Response
	if err := proto.Unmarshal(data, &r); err != nil {
		return Response{}, err
	}

	return Response{
		Val:      r.Val,
		HasError: r.HasError,
	}, nil
}
