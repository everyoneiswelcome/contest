package protocol

import (
	"errors"
	"io"
	"net"

	"bitbucket.org/neilvallon/bibifi/bank"
)

type Client struct {
	conn net.Conn
}

func NewClient(c net.Conn) Client {
	return Client{c}
}

func (c Client) Call(req Request) (Response, error) {
	////// protobuf
	{
		data, err := MarshalRequest(req)
		if err != nil {
			return Response{}, err
		}

		n, err := c.conn.Write(data)
		if err != nil {
			return Response{}, err
		}
		_ = n
	}

	var res Response
	{
		var data [32]byte
		n, err := c.conn.Read(data[:])
		if err != nil && err != io.EOF {
			return Response{}, err
		}

		res, err = UnmarshalResponse(data[:n])
		if err != nil {
			return Response{}, err
		}
	}
	///

	if res.HasError {
		return Response{}, errors.New("unknown server error")
	}

	return res, nil
}

func (c Client) MakeAccount(name string, card bank.CardStrip, val uint64) error {
	res, err := c.Call(Request{name, card, ActionCreate, val})
	if err == nil {
		printMake(name, res.Val)
	}

	return err
}

func (c Client) Deposit(name string, card bank.CardStrip, val uint64) error {
	res, err := c.Call(Request{name, card, ActionDeposit, val})
	if err == nil {
		printDeposit(name, res.Val)
	}

	return err
}

func (c Client) Withdraw(name string, card bank.CardStrip, val uint64) error {
	res, err := c.Call(Request{name, card, ActionWithdraw, val})
	if err == nil {
		printWithdraw(name, res.Val)
	}

	return err
}

func (c Client) Balance(name string, card bank.CardStrip) error {
	res, err := c.Call(Request{name, card, ActionGet, 0})
	if err == nil {
		printBalance(name, res.Val)
	}

	return err
}
