package protocol

import "fmt"

func printLog(act Action, account string, val uint64) {
	switch act {
	case ActionCreate:
		printMake(account, val)
	case ActionDeposit:
		printDeposit(account, val)
	case ActionWithdraw:
		printWithdraw(account, val)
	case ActionGet:
		printBalance(account, val)
	}
}

func printMake(account string, val uint64) {
	fmt.Printf("{\"account\":%q,\"initial_balance\":%g}\n", account, float64(val)/100)
}

func printDeposit(account string, val uint64) {
	fmt.Printf("{\"account\":%q,\"deposit\":%g}\n", account, float64(val)/100)
}

func printWithdraw(account string, val uint64) {
	fmt.Printf("{\"account\":%q,\"withdraw\":%g}\n", account, float64(val)/100)
}

func printBalance(account string, val uint64) {
	fmt.Printf("{\"account\":%q,\"balance\":%g}\n", account, float64(val)/100)
}
