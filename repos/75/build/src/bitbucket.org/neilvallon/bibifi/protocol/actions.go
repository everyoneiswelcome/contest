package protocol

type Action int

const (
	NoAction Action = iota
	ActionCreate
	ActionDeposit
	ActionWithdraw
	ActionGet
)
