// +build bank

package main

import (
  "crypto/rand"
  "errors"
  "fmt"
  "net"
  "os"
  "os/signal"
  "syscall"
  "time"
)


func main() {
  initSignalsHandler()

  config := parseBankConfig()
  if config == nil { os.Exit(FAILURE) }

  ensure(generateAuthFile(config.authFile))
  initAccounts()

  listener, err := net.Listen("tcp", "0.0.0.0:" + config.port)
  ensure(err)
  defer listener.Close()

  for {
    conn, err := listener.Accept()
    bankEnsure(err)
    if DEBUG { fmt.Fprintf(os.Stderr, "[DEBUG] accepted connection\n") }
    handleRequest(conn)
  }
}


func initSignalsHandler() {
  c := make(chan os.Signal, 1)
  go func() {
        _ = <- c
        os.Exit(SUCCESS)
    }()
  signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
}


func handleRequest(conn net.Conn) {
  defer func() { conn.Close(); recover() }()

  bankEnsure(conn.SetReadDeadline(time.Now().Add(TIMEOUT)))

  var msg ClientMsg
  bankEnsure(readClientMsg(&conn, &msg))
  if DEBUG { fmt.Fprintf(os.Stderr, "[DEBUG] Received msg: %+v\n", msg) }

  if validClientMsg(&msg) {
    status, balance := handleAccountRequest(msg.Name, msg.Card, msg.RequestType,
                                           msg.Amount)
    bankEnsure(sendServerMsg(&conn, &ServerMsg{Balance: balance, StatusCode: status}))
  } else {
    bankEnsure(errors.New("Invalid client message"))
  }
}


func generateAuthFile(path string) (err error) {
  _, err = rand.Read(sharedSecret[:])
  if err != nil { return }
  err = writeFile(path, sharedSecret[:])
  if err != nil { return }
  fmt.Println("created")
  return
}
