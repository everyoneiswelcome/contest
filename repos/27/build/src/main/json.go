package main

import (
  "fmt"
  "math/big"
)


func atmPrint(request byte, name string, amount *big.Int) {
  switch request {
  case RequestNew:      accountNewPrint(name, amount)
  case RequestDeposit:  accountDepositPrint(name, amount)
  case RequestWithdraw: accountWithdrawPrint(name, amount)
  case RequestGet:      accountGetPrint(name, amount)
  }
}


func accountNewPrint(name string, amount *big.Int) {
  accountPrint(name, "initial_balance", amount)
}


func accountDepositPrint(name string, amount *big.Int) {
  accountPrint(name, "deposit", amount)
}


func accountWithdrawPrint(name string, amount *big.Int) {
  accountPrint(name, "withdraw", amount)
}


func accountGetPrint(name string, amount *big.Int) {
  accountPrint(name, "balance", amount)
}


func accountPrint(name string, key string, balance *big.Int) {
  fmt.Printf(`{"account":"%v", "%v":%v}%v`,
    name, key, moneyToString(balance), "\n")
}


var HUNDRED = big.NewInt(100)

func moneyToString(amount *big.Int) string {
  i, f := big.NewInt(0), big.NewInt(0)
  i.DivMod(amount, HUNDRED, f)

  return fmt.Sprintf("%v.%02d", i, f)
}
