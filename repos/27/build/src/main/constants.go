package main

import (
  "time"
)

// Debug mode
const DEBUG = false //true

const (
  ATM = iota
  BANK
)

// Request codes
const (
  RequestNew = iota
  RequestDeposit
  RequestWithdraw
  RequestGet
)

// Status codes
const SUCCESS = 0
const PROTOCOL_FAILURE = 63
const FAILURE = 255

const (
  NAME_MIN_LEN = 1
  NAME_MAX_LEN = 250

  PATH_MIN_LEN = 1
  PATH_MAX_LEN = 255

  NEW_ACCOUNT_MIN_BALANCE = 1000

  AMOUNT_MIN = 1
  AMOUNT_MAX = 429496729599

  CARD_LEN = 16

  SERVER_NAME = "ATM"

  TIMEOUT = 10 * time.Second

  MAX_PACKET_SIZE = 16384
)
