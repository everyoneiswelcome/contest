package main

import (
  "bytes"
  "math/big"
)


type Account struct {
  name    string
  card    []byte
  balance *big.Int
}


var bankAccounts map[string]*Account


func initAccounts() {
  bankAccounts = make(map[string]*Account)
}


func handleAccountRequest(
  name string, card []byte, request byte, amount uint64) (byte, *big.Int) {
  var status byte = FAILURE
  var balance *big.Int
  account, _ := bankAccounts[name]

  if isAuthorized(request, account, card) {
    switch request {
    case RequestNew:
      status = accountNew(name, card, amount, account)
    case RequestDeposit:
      status = accountDeposit(amount, account)
    case RequestWithdraw:
      status = accountWithdraw(amount, account)
    case RequestGet:
      status, balance = accountGet(account)
    }
  }

  return status, balance
}


func isAuthorized(request byte, account *Account, card []byte) bool {
  return request == RequestNew ||
         (account != nil && bytes.Equal(account.card, card))
}


func accountNew(
  name string, card []byte, amount uint64, account *Account) byte {
  if account == nil && amount >= NEW_ACCOUNT_MIN_BALANCE {
    balance := big.NewInt(0).SetUint64(amount)
    bankAccounts[name] = &Account{name: name, card: card, balance: balance }
    accountNewPrint(name, balance)
    return SUCCESS
  } else {
    return FAILURE
  }
}


func accountDeposit(amount uint64, account *Account) byte {
  if account != nil {
    v := big.NewInt(0).SetUint64(amount)
    account.balance.Add(account.balance, v)
    accountDepositPrint(account.name, v)
    return SUCCESS
  } else {
    return FAILURE
  }
}


func accountWithdraw(amount uint64, account *Account) byte {
  v := big.NewInt(0).SetUint64(amount)
  if account != nil && Lte(v, account.balance) {
    account.balance.Sub(account.balance, v)
    accountWithdrawPrint(account.name, v)
    return SUCCESS
  } else {
    return FAILURE
  }
}


func accountGet(account *Account) (byte, *big.Int) {
  if account != nil {
    accountGetPrint(account.name, account.balance)
    return SUCCESS, account.balance
  } else {
    return FAILURE, nil
  }
}
