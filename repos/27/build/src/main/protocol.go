package main

import (
  "bytes"
  "encoding/binary"
  "errors"
  "math/big"
  "golang.org/x/crypto/nacl/secretbox"
  "io"
  "net"
  "unsafe"
)


type Packet struct {
  Nonce  uint64
  Size   uint64
}


type ClientMsg struct {
  Amount      uint64
  Card        []byte
  Name        string
  RequestType byte
}


type ServerMsg struct {
  StatusCode  byte
  Balance     *big.Int
}


var nextNonce uint64 = 1337

var sharedSecret [32]byte

var byteOrder = binary.LittleEndian


func readClientMsg(conn *net.Conn, msg *ClientMsg) (err error) {
  raw, err := readMessage(conn)
  if err != nil { return }
  reader := bytes.NewReader(raw)

  msg.Amount, err = readUint64(reader)

  msg.Card = make([]byte, CARD_LEN)
  _, err = io.ReadFull(reader, msg.Card)
  if err != nil { return }

  nameLen, err := reader.ReadByte()
  if err != nil { return }
  var name = make([]byte, nameLen)
  _, err = io.ReadFull(reader, name[:])
  if err != nil { return }
  msg.Name = string(name)

  msg.RequestType, err = reader.ReadByte()
  return
}


func readServerMsg(conn *net.Conn, msg *ServerMsg) (err error) {
  raw, err := readMessage(conn)
  if err != nil { return }
  reader := bytes.NewReader(raw)

  // StatusCode
  msg.StatusCode, err = reader.ReadByte()

  // Balance
  balanceLen, err := readUint64(reader)
  if err != nil { return }
  var balance = make([]big.Word, balanceLen)
  err = readWords(reader, balance)
  if err != nil { return }
  msg.Balance = big.NewInt(0)
  msg.Balance.SetBits(balance)

  return
}


func sendClientMsg(conn *net.Conn, msg *ClientMsg) (err error) {
  size := unsafe.Sizeof(msg.Amount) + 2 * unsafe.Sizeof(msg.RequestType) +
          uintptr(len(msg.Card) + len(msg.Name))

  writer := bytes.NewBuffer(make([]byte, 0, size))
  writeUint64(writer, msg.Amount)
  writer.Write(msg.Card)
  writer.WriteByte(byte(len(msg.Name)))
  writer.WriteString(msg.Name)
  writer.WriteByte(msg.RequestType)
  return sendMessage(conn, writer.Bytes())
}


func sendServerMsg(conn *net.Conn, msg *ServerMsg) (err error) {
  writer := bytes.NewBuffer([]byte{})

  // StatusCode
  writer.WriteByte(msg.StatusCode)

  // Balance
  var balance []big.Word
  if msg.Balance != nil {
    balance = msg.Balance.Bits()
  }
  writeUint64(writer, uint64(len(balance)))
  writeWords(writer, balance)
  return sendMessage(conn, writer.Bytes())
}


func readUint64(reader *bytes.Reader) (x uint64, err error) {
  var buf [8]byte
  _, err = reader.Read(buf[:])
  if err != nil { return }
  x = byteOrder.Uint64(buf[:])
  return
}


func writeUint64(writer *bytes.Buffer, x uint64) {
  var buf [8]byte
  byteOrder.PutUint64(buf[:], x)
  writer.Write(buf[:])
}


func readWords(reader *bytes.Reader, words []big.Word) (err error) {
  for i := 0; i < len(words); i++ {
    var x uint64
    x, err = readUint64(reader)
    if err != nil { return }
    words[i] = big.Word(x)
  }
  return
}


func writeWords(writer *bytes.Buffer, words []big.Word) {
  for i := 0; i < len(words); i++ {
    writeUint64(writer, uint64(words[i]))
  }
}


func readMessage(conn *net.Conn) (msg []byte, err error) {
  var packet Packet
  reader := *conn
  err = binary.Read(reader, byteOrder, &packet)
  if err != nil { return }
  if packet.Size > MAX_PACKET_SIZE {
    err = errors.New("Invalid packet size")
    return
  }
  var box = make([]byte, packet.Size, packet.Size)
  _, err = io.ReadFull(reader, box)
  if err != nil { return }
  var nonce [24]byte
  byteOrder.PutUint64(nonce[:], packet.Nonce)
  msg, valid := secretbox.Open(msg, box, &nonce, &sharedSecret)
  if !valid {
    err = errors.New("Invalid message received")
  }
  return
}


func sendMessage(conn *net.Conn, message []byte) (err error) {
  var box []byte
  var nonce [24]byte

  byteOrder.PutUint64(nonce[:], nextNonce)
  box = secretbox.Seal(box, message, &nonce, &sharedSecret)
  var packet = Packet{Size: uint64(len(box)), Nonce: nextNonce}
  nextNonce++
  writer := *conn
  err = binary.Write(writer, byteOrder, packet)
  if err != nil { return }
  _, err = writer.Write(box)
  if err != nil { return }
  return
}


func validClientMsg(msg *ClientMsg) bool {
  return validName(msg.Name) &&
         (msg.RequestType == RequestGet ||
          msg.RequestType < RequestGet &&
          AMOUNT_MIN <= msg.Amount && msg.Amount <= AMOUNT_MAX)
}


func validServerMsg(msg *ServerMsg) bool {
  status := msg.StatusCode
  return (status == SUCCESS || status == PROTOCOL_FAILURE || status == FAILURE)
}
