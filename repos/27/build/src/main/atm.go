// +build atm

package main

import (
  "crypto/rand"
  "errors"
  "fmt"
  "io/ioutil"
  "math/big"
  "os"
  "time"
  "net"
)


func main() {
  config := parseATMConfig()
  if config == nil { os.Exit(FAILURE) }

  card, err := getOrCreateCard(config.requestType, config.cardFile)
  ensure(err)

  data, err := readAuthFile(config.authFile)
  ensure(err)
  copy(sharedSecret[:], data)

  conn, err := net.Dial("tcp", config.ipAddress + ":" + config.port)
  atmEnsure(err)

  defer conn.Close()

  atmEnsure(conn.SetReadDeadline(time.Now().Add(TIMEOUT)))

  var clientMsg = ClientMsg{Amount: config.amount, Card: card,
                            RequestType: config.requestType, Name: config.account}
  var serverMsg ServerMsg

  atmEnsure(sendClientMsg(&conn, &clientMsg))
  atmEnsure(readServerMsg(&conn, &serverMsg))
  if DEBUG { fmt.Fprintf(os.Stderr, "[DEBUG] Received msg: %+v\n", serverMsg) }

  if !validServerMsg(&serverMsg) { os.Exit(PROTOCOL_FAILURE) }
  if serverMsg.StatusCode == SUCCESS {
    if config.requestType == RequestGet {
      atmPrint(config.requestType, config.account, serverMsg.Balance)
    } else {
      if config.requestType == RequestNew {
        ensure(writeFile(config.cardFile, card))
      }
      amount := big.NewInt(0).SetUint64(config.amount)
      atmPrint(config.requestType, config.account, amount)
    }
  }
  os.Exit(int(serverMsg.StatusCode))
}


func readAuthFile(path string) (bytes []byte, err error) {
  bytes, err = ioutil.ReadFile(path)
  if err != nil { return }
  if len(bytes) != len(sharedSecret) { err = errors.New("Invalid auth file") }
  return
}


func getOrCreateCard(requestType byte, path string) (card []byte, err error) {
  if requestType == RequestNew {
    card = make([]byte, CARD_LEN)
    _, err = rand.Read(card)
    if err != nil { return }

    var exists bool
    exists, err = fileExists(path)
    if err != nil { return }
    if exists { err = errors.New(path + ": file exists") }
  } else {
    card, err = ioutil.ReadFile(path)
    if len(card) != CARD_LEN { err = errors.New("Invalid card file") }
  }
  return
}
