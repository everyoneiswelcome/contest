package main

import (
  "errors"
  "os"
  "regexp"
  "strconv"
  "strings"
)


type Config struct {
  authFile    string
  ipAddress   string
  port        string
  cardFile    string
  account     string
  amount      uint64
  requestType byte
}


var (
  NAME_REGEXP = regexp.MustCompile(`^[_\-\.0-9a-z]+$`)
  NUMBER_REGEXP = regexp.MustCompile(`^(0|[1-9][0-9]*)$`)
  AMOUNT_REGEXP = regexp.MustCompile(`^(0|[1-9][0-9]*)\.(\d\d)$`)
)


func parseATMConfig() *Config {
  config := Config{}
  args, ok := parseArgs(ATM)
  if !ok { return nil }

  ok = setAuthFile(&config, args) && setIpAddress(&config, args) &&
       setPort(&config, args) && setAccount(&config, args) &&
       setCardFile(&config, args) && setRequestType(&config, args)

  if ok { return &config } else { return nil }
}


func parseBankConfig() *Config {
  config := Config{}
  args, ok := parseArgs(BANK)
  if !ok { return nil }

  ok = setAuthFile(&config, args) && setPort(&config, args)

  if ok { return &config } else { return nil }
}


func setAuthFile(config *Config, args map[byte]string) bool {
  value, exists := args['s']

  if exists {
    if !validPath(value) { return false }
    config.authFile = value
  } else {
    config.authFile = "bank.auth"
  }

  return true
}


func setIpAddress(config *Config, args map[byte]string) bool {
  value, exists := args['i']

  if exists {
    if !validIp(value) { return false }
    config.ipAddress = value
  } else {
    config.ipAddress = "127.0.0.1"
  }

  return true
}


func setPort(config *Config, args map[byte]string) bool {
  value, exists := args['p']

  if exists {
    if !validPort(value) { return false }
    config.port = value
  } else {
    config.port = "3000"
  }

  return true
}


func setCardFile(config *Config, args map[byte]string) bool {
  value, exists := args['c']

  if exists {
    if !validPath(value) { return false }
    config.cardFile = value
  } else {
    config.cardFile = config.account + ".card"
  }

  return true
}


func setAccount(config *Config, args map[byte]string) bool {
  value, exists := args['a']

  if !(exists && validName(value)) { return false }
  config.account = value
  return true
}


func setRequestType(config *Config, args map[byte]string) bool {
  count := 0

  for i, c := range "ndwg" {
    value, exists := args[byte(c)]

    if !exists { continue }

    count++
    config.requestType = byte(i)

    if c != 'g' {
      amount, err := parseAmount(value)
      if err != nil || (c == 'n' && amount < NEW_ACCOUNT_MIN_BALANCE) {
        return false
      }
      config.amount = amount
    }
  }

  return count == 1
}


func parseAmount(str string) (amount uint64, err error) {
  matched := AMOUNT_REGEXP.Match([]byte(str))
  if !matched { return 0, errors.New("Invalid amount: " + str) }
  amount, err = strconv.ParseUint(strings.Replace(str, ".", "", 1), 10, 64)
  if err != nil { return }
  if !(AMOUNT_MIN <= amount && amount <= AMOUNT_MAX) {
    err = errors.New("Invalid amount: " + str)
  }
  return
}


func validPath(str string) bool {
  return NAME_REGEXP.Match([]byte(str)) && str != "." && str != ".." &&
         PATH_MIN_LEN <= len(str) && len(str) <= PATH_MAX_LEN
}


func validName(str string) bool {
  return NAME_REGEXP.Match([]byte(str)) && NAME_MIN_LEN <= len(str) &&
         len(str) <= NAME_MAX_LEN
}


func validPort(str string) bool {
  if !NUMBER_REGEXP.Match([]byte(str)) { return false }
  v, err := strconv.ParseUint(str, 10, 16)
  if err != nil { return false }
  if !(1024 <= v && v <= 65535) { return false }
  return true
}


func validIp(str string) bool {
  parts := strings.Split(str, ".")
  if len(parts) != 4 { return false }

  for _, part := range parts {
    if !NUMBER_REGEXP.Match([]byte(part)) { return false }
    v, err := strconv.ParseUint(part, 10, 8)
    if err != nil { return false }
    if !(0 <= v && v <= 255) { return false }
  }

  return true
}


func parseArgs(kind byte) (map[byte]string, bool) {
  result := make(map[byte]string)
  args := os.Args[1:]

  for len(args) > 0 {
    var ok bool
    args, ok = parseNextOption(args, result, kind)
    if !ok { return result, false }
  }

  return result, true
}


func parseNextOption(
  args []string, result map[byte]string, kind byte) ([]string, bool) {

  if len(args) == 0 { return args, true }
  arg := args[0]

  if len(arg) < 2 || arg[0] != '-' || !validOptionLetter(arg[1], kind) {
    return args, false
  } else if arg[1] == 'g' && kind == ATM && len(arg) == 2 {
    return args[1:], argsAddNew(result, 'g', "")
  } else if len(arg) > 2 {
    return args[1:], argsAddNew(result, arg[1], arg[2:])
  } else if len(args) > 1 {
    return args[2:], argsAddNew(result, arg[1], args[1])
  } else {
    return args[1:], false
  }
}


func argsAddNew(result map[byte]string, name byte, value string) bool {
  _, exists := result[name]

  if exists {
    return false
  } else {
    result[name] = value
    return true
  }
}


func validOptionLetter(char byte, kind byte) bool {
  if kind == BANK {
    return strings.ContainsRune("ps", rune(char))
  } else {
    return strings.ContainsRune("acdginpsw", rune(char))
  }
}


func fileExists(path string) (bool, error) {
  _, err := os.Stat(path)
  if err == nil { return true, nil }
  if os.IsNotExist(err) { return false, nil }
  return true, err
}
