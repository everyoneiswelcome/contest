package main

import (
  "fmt"
  "math/big"
  "os"
)


func Lte(x *big.Int, y *big.Int) bool {
  return x.Cmp(y) < 1
}


func bankEnsure(err error) {
  if err != nil {
     if DEBUG { fmt.Fprintf(os.Stderr, "[DEBUG] Bank protocol error: %v\n", err.Error()) }
     fmt.Println("protocol_error")
     panic(err)
  }
}


func atmEnsure(err error) {
  if err != nil {
    if DEBUG { fmt.Fprintf(os.Stderr, "[DEBUG] ATM protocol error: %v\n", err.Error()) }
    os.Exit(PROTOCOL_FAILURE)
  }
}


func ensure(err error) {
  if err != nil {
     if DEBUG { fmt.Fprintf(os.Stderr, "[DEBUG] Error: %v\n", err.Error()) }
     os.Exit(FAILURE)
  }
}


func writeFile(path string, content []byte) (err error) {
  var perm os.FileMode = 0600
  flags := os.O_CREATE | os.O_EXCL | os.O_WRONLY
  file, err := os.OpenFile(path, flags, perm)
  if err != nil { return }
  defer func() { err = file.Close() }()
  _, err = file.Write(content)
  return
}
