﻿using System;
using NUnit.Framework;
using GenericName.Transactions;

namespace GenericName.Common
{
	[TestFixture]
	public class TransactionTests
	{
		static void AddAccountTransaction (AccountContainer container, string accountName, Currency balance)
		{
			var result = container.OpenTransaction (TransactionType.GenerateAccount, accountName, null, balance);
			Assert.IsTrue (result.IsValid);
		}

		[Test]
		public void TestGenerateAccount()
		{
			var container = new AccountContainer ();

			AddAccountTransaction (container, "Casey", new Currency (100M));
			var tran = container.CommitTransaction ("Casey");

			Assert.AreEqual (100M, tran.Account.Balance.Total);
			Assert.AreEqual ("Casey", tran.AccountName);
			Assert.AreEqual (TransactionType.GenerateAccount, tran.TransactionType);
		}

		[Test]
		public void TestAccountRollback()
		{
			var container = new AccountContainer ();
			AddAccountTransaction (container, "Casey", new Currency (100M));

			container.RollbackTransaction ("Casey");

			Assert.DoesNotThrow (() => {
				AddAccountTransaction(container, "Casey", new Currency(110M));
			});

			var tran = container.CommitTransaction ("Casey");
			Assert.AreEqual (110M, tran.Account.Balance.Total);
 		}

		[Test]
		public void TestAccountDeposit()
		{
			var container = new AccountContainer ();
			AddAccountTransaction (container, "Casey", new Currency (100M));
			var card = container.CommitTransaction ("Casey").Account.Card;

			var res = container.OpenTransaction (TransactionType.Deposit, "Casey", card, new Currency (100M));

			Assert.IsTrue (res.IsValid);

			var tran = container.CommitTransaction ("Casey");
			Assert.AreEqual (200M, tran.Account.Balance.Total);
			Assert.AreEqual ("Casey", tran.AccountName);
			Assert.AreEqual (TransactionType.Deposit, tran.TransactionType);
		}

		[Test]
		public void TestAccountWithdrawal()
		{
			var container = new AccountContainer ();
			AddAccountTransaction (container, "Casey", new Currency (100M));
			var card = container.CommitTransaction ("Casey").Account.Card;

			var res = container.OpenTransaction (TransactionType.Withdrawal, "Casey", card, new Currency (50M));

			Assert.IsTrue (res.IsValid);

			var tran = container.CommitTransaction ("Casey");
			Assert.AreEqual (50M, tran.Account.Balance.Total);
			Assert.AreEqual ("Casey", tran.AccountName);
			Assert.AreEqual (TransactionType.Withdrawal, tran.TransactionType);
		}

		[Test]
		public void TestAccountWithdrawalFailure()
		{
			var container = new AccountContainer ();
			AddAccountTransaction (container, "Casey", new Currency (100M));
			var card = container.CommitTransaction ("Casey").Account.Card;

			var res = container.OpenTransaction (TransactionType.Withdrawal, "Casey", card, new Currency (110M));

			Assert.IsFalse (res.IsValid);
		}

		[Test]
		public void TestMultipleGetBalances()
		{
			var container = new AccountContainer ();
			AddAccountTransaction (container, "Casey", new Currency (100M));
			var card = container.CommitTransaction ("Casey").Account.Card;

			container.OpenTransaction (TransactionType.GetBalance, "Casey", card, Currency.Zero);
			container.CommitTransaction ("Casey");

			var transactionResult = container.OpenTransaction (TransactionType.GetBalance, "Casey", card, Currency.Zero);
			Assert.IsTrue (transactionResult.IsValid);
		}

		[Test]
		public void TestWithdrawZeroDollarsShouldFail()
		{
			var container = new AccountContainer ();
			AddAccountTransaction (container, "Casey", new Currency (100M));
			var card = container.CommitTransaction ("Casey").Account.Card;

			var transactionResult = container.OpenTransaction (TransactionType.Withdrawal, "Casey", card, Currency.Zero);

			Assert.IsFalse (transactionResult.IsValid);

		}

		[Test]
		public void TestDepositZeroDollarsShouldFail()
		{
			var container = new AccountContainer ();
			AddAccountTransaction (container, "Casey", new Currency (100M));
			var card = container.CommitTransaction ("Casey").Account.Card;

			var transactionResult = container.OpenTransaction (TransactionType.Deposit, "Casey", card, Currency.Zero);

			Assert.IsFalse (transactionResult.IsValid);

		}
	}
}

