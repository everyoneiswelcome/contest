﻿using System;
using NUnit.Framework;

namespace GenericName.Tests
{
	[TestFixture]
	public class AppUtilityTests
	{
		#if DEBUG
		[Test]
		[TestCase(".")]
		[TestCase("")]
		[TestCase("..")]
		[TestCase(" ..")]
		[TestCase(@"\0..")]
		[TestCase("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")]
		[TestCase("monkey.$")]
		[TestCase("panda@card.com")]
		[TestCase(@"\network\share\file.txt")]
		[TestCase("http://www.google.com")]
		public void CheckInvalidFileNames(string fileName)
		{
			try 
			{
				AppUtility.AssertValidFilename(fileName);
			} catch (RequirementViolationException ex) {
				Assert.AreEqual (255, ex.ExitCode);
				Assert.Pass ();
			}
			Assert.Fail ();
		}

		[Test]
		[TestCase(@"123456789")]
		[TestCase(@"1")]
		[TestCase(@"1.1")]
		[TestCase(@"1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwfj1k0wuomwf.auth")]
		[TestCase(@"auth.file")]
		public void CheckValidFileNames(string fileName)
		{
			try 
			{
				AppUtility.AssertValidFilename(fileName);
			} catch (RequirementViolationException ex) {
				Assert.Fail ();
			}
				
			Assert.Pass ();
		}

		[Test]
		[TestCase("127.0.0.1")]
		public void CheckValidIpAddresses(string ip)
		{
			Assert.DoesNotThrow (() => {
				AppUtility.AssertValidIpAddress (ip);
			});
		}

		[Test]
		[TestCase("0127.0.0.1")]
		public void CheckInvalidIpAddresses(string ip)
		{


			try {
				AppUtility.AssertValidIpAddress(ip);
			} catch (RequirementViolationException ex) {
				Assert.AreEqual (255, ex.ExitCode);
				Assert.Pass ();
			}

			Assert.Fail ();
		}


		[Test]
		[TestCase(TransactionType.GetBalance, 0.00)]
		[TestCase(TransactionType.GenerateAccount, 10.00)]
		[TestCase(TransactionType.GenerateAccount, 99.01)]
		[TestCase(TransactionType.GenerateAccount, 10.01)]
		[TestCase(TransactionType.Withdrawal, 1.00)]
		[TestCase(TransactionType.GenerateAccount, 4294967295.99)]
		public void TestValidTransactionAmounts(TransactionType type, decimal curr)
		{
			Currency amount = new Currency (curr);

			Assert.DoesNotThrow (() => {
				AppUtility.IsValidTransactionRange(type, amount);
			});
		}

		[Test]
		[TestCase(TransactionType.Deposit, 0.00)]
		[TestCase(TransactionType.Withdrawal, 0.00)]
		[TestCase(TransactionType.Deposit, 4294967296.00)]
		[TestCase(TransactionType.Withdrawal, 4294967296.00)]
		[TestCase(TransactionType.GenerateAccount, 0.01)]
		[TestCase(TransactionType.GenerateAccount, 9.99)]
		public void TestInvalidTransactionAmount(TransactionType type, decimal curr)
		{
			Currency amount = new Currency (curr);

			Assert.IsFalse (AppUtility.IsValidTransactionRange (type, amount));
		}

		#endif


		private string[] args1 = new string[] {"-p","3000","-s","100","-dabc","-l","-k"};
		//[{"input":["-p", "%PORT%", "-i", "%IP%", "-a", "homepath", "-c", "~/test.card", "-n", "50.00"],"base64":false},{"input":["-p", "%PORT%", "-i", "%IP%", "-a", "rootpath", "-c", "/test.card", "-n", "50.00"], "base64":false}, {"input":["-p", "%PORT%", "-i", "%IP%", "-a", "http", "-c", "http://www.google.com/test.card", "-n", "60.00"], "base64":false}]
		[Test]
		[TestCase()]
		public void TestValidArgumentParsing(){

			string[] args = new string[] {"-p","3000","-s","100","-dabc","-l","-k"};
			int i = 0;
			while (i < args.Length) {
				string a = AppUtility.GetArgOrExit (args, i++);
				Console.WriteLine(String.Format("Arg[{0}] : {1}", i, a));
				Assert.IsFalse(String.IsNullOrEmpty(a));
			}
		}

		[Test]
		[TestCase("-lakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasdsdsdfsgsdfgsdfgdfgertrrtrrttrt")]
		[TestCase("--l akighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasghndfpcoasmdpfocihasodihmcaoiusghnpoahsnpofihamsopicfhoashidfcnpoashdfncopashnmfopaishdfcnoihasndcfophamsdofihaspohdgcfnpaoshdmfcoasmihdfcoihmaspoidfcnpoasihfposaihncfopiashfpmoiahmsdcflakighsjabscoiANOSIDXUGOiugcioagfcboaiysgcfblifcgboiagbfcaisjugndfcpmagpcougqawpuglliuasdnoifucnaspodiufgmcpiausmgdfpcuapsdocfpmaosihmdfcpoiahsdifpocaspdiofhjcmpoasihdmfcoaishdfmcipoashcasfcpashdmcfpoishamfpcoiahsfmnopcuaghsnpcoasghnfpcouasdsdsdfsgsdfgsdfgdfgertrrtrrttrt")]
		public void TestInvalidArgumentParsing(string longArg){
			/*
			 * The longarg3 that was here was technically valid. (-l <billion spaces> 1)
			 * In POSIX ALL spaces are truncated to a single
			 * space before they are considered arguments.
			 * Thus, length validation only applies to the trimmed
			 * arguments.
			 */
			string[] args = new string[] { longArg };
			try {
				AppUtility.AssertArgumentLength (args);
				//Assert.IsNull(a);  // [Casey] Why would this be null?
			} catch (RequirementViolationException ex) {
				Assert.AreEqual (255, ex.ExitCode);
				Assert.Pass ();
			}
			Assert.Fail ();
		}
	}
}

