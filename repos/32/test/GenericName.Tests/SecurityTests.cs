﻿using NUnit.Framework;
using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace GenericName.Common
{
	[TestFixture ()]
	public class SecurityTests
	{
		public static readonly string sampleString = "Encrypt me please!";
		public static readonly string sampleStringEncrypted = "EB-84-8C-FE-CF-EB-D3-2D-BB-DA-DB-52-28-3F-70-AD-C2-16-BA-8D-CF-5E-A5-8F-BB-00-92-BF-CF-30-D2-05";
		public static readonly string sampleStringSigned = "5Xw+ye+D0f60eQn4mexS8RNusny9fdGRJaPd+dW2Wxk=";
		public static readonly string sampleStringSignedHash = "xy9mYv6JtMuCn9tivx1ybHvpyUDDUQgIwfxFBYunjn8=";
		public static readonly string sampleSecurityToken = @"{""EncodedEncryptDecryptKey"":""RW5jcnlwdCBtZSBwbGVhc2Uh"",""EncodedIV"":""AAAAAAAAAAAAAAAAAAAAAA=="",""EncodedServerSignature"":""5Xw+ye+D0f60eQn4mexS8RNusny9fdGRJaPd+dW2Wxk="",""EncodedSigningKey"":""RW5jcnlwdCBtZSBwbGVhc2Uh""}";
		public static readonly string sampleLongString = "Encrypt me please, I beg of you, OH PLEASE encrypt me with AES so that I can be used to test the allowable lenghts of files, and input parameters. Validation will most likely take care of things, but just in case, maybe we can spice it up and make sure!!";
		public static readonly string sampleLongStringEncrypted = 
			"EB-84-8C-FE-CF-EB-D3-2D-BB-DA-DB-52-28-3F-70-AD-E8-E6-8D-82-88-EB-10-65-87-C8-96-AF-7F-5D-C6-8F-65-CF-87-7F-8A-AF-B9"+
			"-BD-AF-CC-45-85-40-61-55-93-08-4D-94-49-E5-60-01-A9-1C-DD-60-6C-26-37-7D-C1-6E-7A-7F-1F-55-DB-68-22-EB-9A-5F-A0-DF-BA"+
			"-08-A6-98-79-3B-92-BA-39-38-CC-1B-49-AB-55-12-94-60-A0-74-4D-A6-18-89-1A-D2-BE-BA-F4-D6-1F-6F-A7-95-31-27-59-3D-DA-28"+
			"-66-51-89-0F-B5-AF-09-98-C4-59-93-E8-EA-F4-1F-A8-33-22-0D-5A-3B-3A-92-95-59-2A-F4-45-74-83-47-B8-16-43-FB-D5-7C-22-6A"+
			"-0F-CA-DF-65-AC-DC-1C-3D-E5-8A-93-91-DC-0D-3E-33-43-C8-FC-C3-DD-62-6D-F0-9F-DF-CD-54-10-22-E1-E7-C1-70-3F-7A-64-5D-3D"+
			"-98-43-58-9D-B4-99-CE-B1-B1-DE-BD-56-94-6C-E9-4D-88-42-99-2A-9F-AA-C1-A2-7F-2E-E8-8C-B0-51-27-3B-C6-0B-E3-14-60-73-02"+
			"-0D-41-A5-E0-D4-EE-83-22-F9-0F-C4-54-58-2A-9B-6D-4F-66-4F-A2-9E-5B";

		public static readonly byte [] AllZeroes256 = new byte[] { 
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
		};

		public static readonly byte [] AllZeroes128 = new byte[] {
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
		};

		/// <summary>
		/// This test cases just uses AES in CBC mode with Zero padding and IV and key set to all 0's.
		/// Expected encryption of the bytes in string: "Encrypt me please!" (not newline-terminated)
		/// Result in EB-84-8C-FE-CF-EB-D3-2D-BB-DA-DB-52-28-3F-70-AD-C2-16-BA-8D-CF-5E-A5-8F-BB-00-92-BF-CF-30-D2-05
		/// Vetted using an online AES calculator
		/// </summary>
		[TestCase ()]
		public void TestAesEncryptDecryptAllZerosKeyAndIV ()
		{
			Console.WriteLine ("Enter TestAesEncryptDecryptAllZerosKeyAndIV");
			Console.WriteLine (String.Format("Encrypting : {0}", sampleString));
			byte[] sampleStringBytes = System.Text.Encoding.UTF8.GetBytes (sampleString);
			Console.WriteLine (String.Format("Source Bytes : {0}", BitConverter.ToString (sampleStringBytes)));
			byte[] encrypted = null;
			using (var aesCrypt = Aes.Create ()) {
				aesCrypt.BlockSize = 128;
				aesCrypt.Padding = PaddingMode.Zeros;
				aesCrypt.KeySize = 256;
				aesCrypt.Key = AllZeroes256;
				aesCrypt.IV = AllZeroes128;
				encrypted = aesCrypt.CreateEncryptor ().TransformFinalBlock (sampleStringBytes, 0, sampleStringBytes.Length);
				Assert.IsNotNull (encrypted);
				Assert.IsTrue (encrypted.Length > 0);
				Console.WriteLine (String.Format ("Encrypted: {1} bytes ==>  {0}", BitConverter.ToString (encrypted), encrypted.Length));
				Assert.AreEqual (sampleStringEncrypted, BitConverter.ToString (encrypted), "Encryption mismatched expected result");
			}
			using (var aesCrypt = Aes.Create ()) {
				Assert.IsNotNull (encrypted);
				aesCrypt.BlockSize = 128;
				aesCrypt.Padding = PaddingMode.Zeros;
				aesCrypt.KeySize = 256;
				aesCrypt.Key = AllZeroes256;
				aesCrypt.IV = AllZeroes128;
				byte[] decrypted = aesCrypt.CreateDecryptor ().TransformFinalBlock (encrypted, 0, encrypted.Length);

				Console.WriteLine (String.Format("Decrypted: {1} bytes ==>  {0}", BitConverter.ToString (decrypted), decrypted.Length));
				Console.WriteLine (String.Format("Decrypted text : {0}", System.Text.Encoding.UTF8.GetString (decrypted)));
				Assert.AreEqual (sampleString, System.Text.Encoding.UTF8.GetString (decrypted).TrimEnd('\0'));
			}
			Console.WriteLine ("Exit TestAesEncryptDecryptAllZerosKeyAndIV");
		}

		/// <summary>
		/// This test cases just uses AES in CBC mode with Zeroes padding and IV and key set to 0x00's
		/// Expectation is to be able to retrieve all messages from 1 char to 255 characters.
		/// </summary>
		[TestCase ()]
		public void TestAesEncryptDecryptMinMaxLengths ()
		{
			Console.WriteLine ("Enter TestAesEncryptDecryptMinMaxLengths");
			Console.WriteLine (String.Format("Encrypting : {0}", sampleLongString));
			byte[] sampleLongStringBytes = System.Text.Encoding.UTF8.GetBytes (sampleLongString);
			Console.WriteLine (String.Format("Source Bytes : {0}", BitConverter.ToString (sampleLongStringBytes)));
			byte[] encrypted = null;
			using (var aesCrypt = Aes.Create ()) {
				aesCrypt.BlockSize = 128;
				aesCrypt.Padding = PaddingMode.Zeros;
				aesCrypt.KeySize = 256;
				aesCrypt.Key = AllZeroes256;
				aesCrypt.IV = AllZeroes128;
				encrypted = aesCrypt.CreateEncryptor ().TransformFinalBlock (sampleLongStringBytes, 0, sampleLongStringBytes.Length);
				Assert.IsNotNull (encrypted);
				Assert.IsTrue (encrypted.Length > 0);
				Console.WriteLine (String.Format ("Encrypted: {1} bytes ==>  {0}", BitConverter.ToString (encrypted), encrypted.Length));
				Assert.AreEqual (sampleLongStringEncrypted, BitConverter.ToString (encrypted), "Encryption mismatched expected result");
			}
			using (var aesCrypt = Aes.Create ()) {
				Assert.IsNotNull (encrypted);
				aesCrypt.BlockSize = 128;
				aesCrypt.Padding = PaddingMode.Zeros;
				aesCrypt.KeySize = 256;
				aesCrypt.Key = AllZeroes256;
				aesCrypt.IV = AllZeroes128;
				byte[] decrypted = aesCrypt.CreateDecryptor ().TransformFinalBlock (encrypted, 0, encrypted.Length);

				Console.WriteLine (String.Format("Decrypted: {1} bytes ==>  {0}", BitConverter.ToString (decrypted), decrypted.Length));
				Console.WriteLine (String.Format("Decrypted text : {0}", System.Text.Encoding.UTF8.GetString (decrypted)));
				Assert.AreEqual (sampleLongString, System.Text.Encoding.UTF8.GetString (decrypted).TrimEnd('\0'));
			}
			Console.WriteLine ("Exit TestAesEncryptDecryptMinMaxLengths");
		}


		[TestCase ()]
		public void TestRng()
		{
			Console.WriteLine ("Enter TestRng");
			byte[] rand = new byte[32];
			for(int i=0; i<32; i++){
				rand[i] = 0x00;
			}
			rand = CryptoUtils.GetRandom (32);
			Assert.IsNotNull (rand);
			Console.WriteLine (BitConverter.ToString (rand));
			Console.WriteLine ("Exit TestRng");
		}

		[TestCase ()]
		public void TestHash()
		{
			Console.WriteLine ("Enter TestHash");
			byte[] hash = CryptoUtils.HashData (Encoding.UTF8.GetBytes (sampleString));
			Assert.IsNotNull (hash);
			Console.WriteLine (BitConverter.ToString (hash));
			Assert.IsTrue (BitConverter.ToString (hash).Contains ("5B-91-BA-81-79-AF"));
			Console.WriteLine ("Exit TestHash");
		}

		[TestCase ()]
		public void TestHMACSHA256()
		{
			Console.WriteLine ("Enter TestHMACSHA256");
			using (HMACSHA256 provHmac = (HMACSHA256) HMACSHA256.Create ("HMACSHA256")) {
				provHmac.Key = CryptoUtils.GetRandom (32);
				provHmac.Initialize ();
				byte[] tag = provHmac.ComputeHash (System.Text.UTF8Encoding.UTF8.GetBytes ("Hello World!"));
				Assert.IsNotNull (tag);
				Console.WriteLine (String.Format("Tag Length: {0}",tag.Length));
				Console.WriteLine (BitConverter.ToString (tag));
			}
			Console.WriteLine ("Exit TestHMACSHA256");
		}

		[TestCase ()]
		public void TestEncryptDecryptFunctions()
		{
			Console.WriteLine ("Enter TestEncryptDecryptFunctions");
			Console.WriteLine (String.Format("Source Bytes : {0}", BitConverter.ToString (Encoding.UTF8.GetBytes(sampleString))));
			byte[] encrypted = CryptoUtils.Encrypt (GetAesProvAllZerosPadding().CreateEncryptor(), Encoding.UTF8.GetBytes(sampleString));
			Assert.IsNotNull (encrypted);
			Console.WriteLine (String.Format("Encrypted [ {{Size={0}, Data={1} }}", encrypted.Length, BitConverter.ToString (encrypted)));
			Assert.AreEqual(sampleStringEncrypted, BitConverter.ToString(encrypted));
			byte[] decrypted = CryptoUtils.Decrypt (GetAesProvAllZerosPadding().CreateDecryptor(), encrypted);
			Assert.IsNotNull (decrypted);
			string decryptedString = Encoding.UTF8.GetString (decrypted);
			Console.WriteLine (String.Format("Decrypted [ {{Size={0}, Data={1} }}", decrypted.Length, BitConverter.ToString (decrypted)));
			Console.WriteLine (String.Format ("Decrypted String: {0} \n", decryptedString));
			Assert.AreEqual (sampleString, decryptedString.TrimEnd('\0')); //TODO: Need to find out why the CryptoStream will not remove the Zeroes padding, but will remove random.
			Console.WriteLine ("Exit TestEncryptDecryptFunctions");
		}

		[TestCase ()]
		public void TestEncryptDecryptFunctionsRandomPadding()
		{
			Console.WriteLine ("Enter TestEncryptDecryptFunctionsRandomPadding");
			SymmetricAlgorithm provider = CryptoUtils.GetAesProvider (AllZeroes256,AllZeroes128); //Need the same provider in order to keep keys, otherwise decrypt wont work.
			Console.WriteLine (String.Format("Source Bytes : {0}", BitConverter.ToString (Encoding.UTF8.GetBytes(sampleString))));
			byte[] encrypted = CryptoUtils.Encrypt (provider.CreateEncryptor(), Encoding.UTF8.GetBytes(sampleString));
			Assert.IsNotNull (encrypted);
			Assert.AreNotEqual(sampleStringEncrypted, BitConverter.ToString(encrypted)); //Using random padding, the encrypted result should not match.
			Console.WriteLine (String.Format("Encrypted [ {{Size={0}, Data={1} }}", encrypted.Length, BitConverter.ToString (encrypted)));
			byte[] decrypted = CryptoUtils.Decrypt (provider.CreateDecryptor(), encrypted);
			Assert.IsNotNull (decrypted);
			string decryptedString = Encoding.UTF8.GetString (decrypted);
			Console.WriteLine (String.Format("Decrypted [ {{Size={0}, Data={1} }}", decrypted.Length, BitConverter.ToString (decrypted)));
			Console.WriteLine (String.Format ("Decrypted String: {0} \n", decryptedString));
			Assert.AreEqual (sampleString, decryptedString); //The outcome should be the same though.
			Console.WriteLine ("Exit TestEncryptDecryptFunctionsRandomPadding");
		}

		[TestCase ()]
		[Ignore]
		public void TestSerializeDeserializeSecurityToken()
		{
			Console.WriteLine ("Enter TestSerializeDeserializeSecurityToken");
			TestSecurityManager man = new TestSecurityManager(AllZeroes128);
			Assert.IsTrue (man.GenerateToken ());
			Console.WriteLine (man.ToString ());
			Assert.AreEqual (sampleStringSigned, man.InspectToken.EncodedServerSignature, "Signatures dont match!");
			byte [] serToken = man.SerializeToken ();
			Assert.IsNotNull (serToken);
			Console.WriteLine (String.Format("JSON Object: \n\n{0}", Encoding.UTF8.GetString(serToken)));

			Assert.IsTrue (man.DeserializeToken (sampleSecurityToken));
			Assert.IsNotNull (man.InspectToken);

			Assert.AreEqual (sampleString, Encoding.UTF8.GetString (Convert.FromBase64String (man.InspectToken.EncodedEncryptDecryptKey)));
			Assert.AreEqual (sampleString, Encoding.UTF8.GetString (Convert.FromBase64String (man.InspectToken.EncodedSigningKey)));
			Assert.IsTrue (man.TestVerifyToken(man.InspectToken, AllZeroes128));
			Assert.IsTrue (man.IsValid());

			man.InspectToken.EncodedEncryptDecryptKey = sampleStringSignedHash;
			Console.WriteLine (String.Format("\nBAD Token: \n{0}", Encoding.UTF8.GetString(man.SerializeToken ())));
			Assert.IsFalse (man.IsValid ());
			Console.WriteLine ("Exit TestSerializeDeserializeSecurityToken");
		}

		[TestCase ()]
		public void TestCardDataVerification()
		{
			Console.WriteLine ("Enter TestCardDataVerification");

			CardData card = new CardData (Encoding.UTF8.GetBytes (sampleString), Convert.FromBase64String(sampleStringSignedHash));
			Console.WriteLine (String.Format("Source Bytes : {0}", BitConverter.ToString (Encoding.UTF8.GetBytes(sampleString))));
			Console.WriteLine (card.ToString ());
			Console.WriteLine (String.Format("Hashed Bytes : {0}", CryptoUtils.HashData(sampleString)));
			byte[] signedHash = CryptoUtils.SignData (CryptoUtils.HashData (sampleString), AllZeroes128);
			Console.WriteLine (String.Format("Signed Bytes : {0}", Convert.ToBase64String(signedHash)));
			TestSecurityManager man = new TestSecurityManager(AllZeroes128);

			//Need to remake the card?
			Assert.IsTrue (man.VerifyCard (sampleString, card));

			Console.WriteLine ("Exit TestCardDataVerification");
		}

		[TestCase ()]
		[Ignore]
		public void TestSerializeDeserializeCardData()
		{
			string goodcardJson1 = @"{""CardNumber"":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],""Signature"":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}";
			string badcardJson2 = @"{""CardNumber"":[0,255,0,0,0,0,0,0,0,123,0,0x23,0,0,-1,123876],""Signature"":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}";
			string badcardJson3 = @"{""CardNumber"":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],""Signature"":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],""Error"":1}}";
			string badcardJson4 = @"{""CardNumber"":[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]";
				

			Console.WriteLine ("Enter TestSerializeDeserializeSecurityToken");
			CardData card = new CardData (AllZeroes128, AllZeroes128);
			byte[] serializedCard = Utilities.Serialize<CardData> (card);
			Assert.AreEqual (goodcardJson1, Encoding.UTF8.GetString (serializedCard));
			Console.WriteLine (String.Format("Test Card Serialized : {0}", Encoding.UTF8.GetString(serializedCard)));

			Console.WriteLine ("<======>");

			try{
				Utilities.Deserialize<CardData> (badcardJson2);
			}catch(Exception e) {
				Assert.AreEqual (e.GetType (), typeof(System.Runtime.Serialization.SerializationException));
				Console.WriteLine (e.GetType ());
				Console.WriteLine (e.Message);
			}
			Console.WriteLine ("<======>");
			try{
				CardData c = Utilities.Deserialize<CardData> (badcardJson3);
				Assert.IsNotNull(c);
				Console.WriteLine (c.ToString());
			}catch(Exception e) {
				Assert.AreEqual (e.GetType (), typeof(System.Runtime.Serialization.SerializationException));
				Console.WriteLine (e.GetType ());
				Console.WriteLine (e.Message);
			}

			Console.WriteLine ("<======>");
			try{
				CardData c = Utilities.Deserialize<CardData> (badcardJson4); //Interesting use case, adding extra data to the serialization JSON doesnt throw a deserialization exception...
				Assert.IsNotNull(c);
				Assert.IsNotNull(c.CardNumber);
				Assert.IsNull(c.Signature);
				CryptoUtils.CheckSignature(c.CardNumber, AllZeroes128, c.Signature);
			}catch(Exception e) {
				Assert.AreEqual (e.GetType (), typeof(InvalidOperationException));
				Console.WriteLine (e.GetType ());
				Console.WriteLine (e.Message);
			}
			Console.WriteLine ("Exit TestSerializeDeserializeSecurityToken");
		}

		/// <summary>
		/// This is a test class used to get the security manager in a desired state.
		/// </summary>
		internal class TestSecurityManager : SecurityManager{

			public TestSecurityManager(byte [] key)
				:base(key)
			{
			}

			public new bool GenerateToken()
			{
				base.GenerateToken ();
				Token.EncodedEncryptDecryptKey=Convert.ToBase64String(Encoding.UTF8.GetBytes(SecurityTests.sampleString));
				Token.EncodedSigningKey = Convert.ToBase64String(Encoding.UTF8.GetBytes(SecurityTests.sampleString));
				Token.EncodedIV = Convert.ToBase64String(AllZeroes128);
				return TestSignToken (Token, SecurityTests.AllZeroes128);
			}

			public bool TestSignToken(SecurityToken token, byte [] key)
			{
				return SignToken(token, key);
			}

			public bool TestVerifyToken(SecurityToken token, byte [] key)
			{
				return VerifyToken(token, key);
			}

			public SecurityToken InspectToken{
				get{
					return Token;
				}
			}
		}

		/// <summary>
		/// Get an AES-256/CBC Provider with all zero for keys, and IV. Padding mode set to zero. 
		/// </summary>
		/// <returns>The aes prov all zeros padding.</returns>
		public static SymmetricAlgorithm GetAesProvAllZerosPadding()
		{
			return CryptoUtils.GetAesProvider (AllZeroes256, AllZeroes128, PaddingMode.Zeros);
		}
	}
}


