﻿using System;
using NUnit.Framework;
using System.Text;
using System.Net.Sockets;
using System.Net;
using GenericName.Common;
using System.IO;
using System.Linq;
using GenericName.Transactions;
using System.Collections.Generic;

namespace GenericName.Tests
{
	[TestFixture()]
	public class CommunicationsTests
	{

		public static string SignedAllZeroes=@"ebRX6ozLeHVg4d8xQwrEWUCEflnz6a72ZcVKy0Q5xkg=";
		private TestSecurityManager man=null;
		public CommunicationsTests ()
		{
		}

		[SetUp]
		public void Setup()
		{
			man = new TestSecurityManager(SecurityTests.AllZeroes128);
			Assert.IsTrue (man.GenerateToken ());
			Console.WriteLine (man.ToString ());
			Assert.AreEqual (SignedAllZeroes, man.InspectToken.EncodedServerSignature, "Signatures dont match!");
			Assert.AreEqual (man.SerializeToken (), TestSecurityManager.Current.SerializeToken ());
		}

		#region TestSecurityManager
		private static TestSecurityManager manager = new TestSecurityManager(SecurityTests.AllZeroes128);
		/// <summary>
		/// This is a test class used to get the security manager in a desired state.
		/// </summary>
		private class TestSecurityManager : SecurityManager{

			public TestSecurityManager(byte [] key)
				:base(key)
			{
				Current = this;
			}

			public new bool GenerateToken()
			{
				base.GenerateToken ();
				Token.EncodedEncryptDecryptKey=Convert.ToBase64String(SecurityTests.AllZeroes256);
				Token.EncodedSigningKey = Convert.ToBase64String(SecurityTests.AllZeroes256);
				Token.EncodedIV = Convert.ToBase64String(SecurityTests.AllZeroes128);
				return TestSignToken (Token, SecurityTests.AllZeroes128); 
			}

			public bool TestSignToken(SecurityToken token, byte [] key)
			{
				return SignToken(token, key);
			}

			public bool TestVerifyToken(SecurityToken token, byte [] key)
			{
				return VerifyToken(token, key);
			}

			public SecurityToken InspectToken{
				get{
					return Token;
				}
			}
		}
		#endregion
	}
}

