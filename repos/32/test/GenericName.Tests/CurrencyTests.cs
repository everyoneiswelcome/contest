﻿using System;
using NUnit.Framework;

namespace GenericName.Common
{
	[TestFixture()]
	public class CurrencyTests
	{
		[Test]
		[TestCase("555.55", 555.55, 55555, 555, 55)]
		[TestCase("0.00", 0.00, 0, 0, 0)]
		[TestCase("0.01", 0.01, 1, 0, 1)]
		[TestCase("-1000.00", -1000.00, -100000, -1000, 0)]
		[TestCase("1.00", 1.00, 100, 1, 0)]
		public void CurrencyParsesString(string str, decimal decVal, long cTotalVal, long dollarVal, byte centVal)
		{
			var c = Currency.Parse (str);

			Assert.AreEqual (decVal, c.Total);
			Assert.AreEqual (cTotalVal, c.TotalCents);
			Assert.AreEqual (dollarVal, c.Dollars);
			Assert.AreEqual (centVal, c.Cents);
		}

		[Test]
		public void CurrencyParseHatesLeadingZeros()
		{
			Assert.Throws<ArgumentException> (() => {
				Currency.Parse ("01.00");
			});
		}

		[Test]
		public void CurrencyParseHatesExcessiveDecimals()
		{
			Assert.Throws<ArgumentException> (() => {
				Currency.Parse("10.000");
			});
		}

		[Test]
		[TestCase(55.00, 55.00, 110.00)]
		[TestCase(-500.00, 100.00, -400.00)]
		public void CurrencyPlusOperatorPasses(decimal val1, decimal val2, decimal eq)
		{
			var c1 = new Currency (val1);
			var c2 = new Currency (val2);

			var expected = new Currency (eq);

			Assert.AreEqual (expected, c1 + c2);
		}

		[Test]
		[TestCase(55.00, "55.00")]
		[TestCase(303046.03, "303046.03")]
		public void CurrencyToStringProvidesCorrectOutput(decimal val1, string str)
		{
			var c1 = new Currency (val1);

			Assert.AreEqual (str, c1.ToString ());
		}

		[Test]
		public void CurrencyComparesToDecimalsGT()
		{
			var currency = new Currency (500.00M);

			Assert.IsTrue (currency > 499.99M);
		}

		[Test]
		public void CurrencyComparesToDecimalsLT()
		{
			var currency = new Currency (500.00M);

			Assert.IsTrue (currency < 500.01M);
		}

		[Test]
		public void CurrencyComparesToDecimalLTE()
		{
			var currency = new Currency (500.00M);

			Assert.IsTrue (currency <= 500.01M);
			Assert.IsTrue (currency <= 500.00M);
		}

		[Test]
		public void CurrencyComparesToDecimalGTE()
		{
			var currency = new Currency (500.00M);

			Assert.IsTrue (currency >= 499.99M);
			Assert.IsTrue (currency >= 500.00M);
		}
	}
}

