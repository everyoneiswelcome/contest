﻿using System;
using NUnit.Framework;
using GenericName.Common;

namespace GenericName.Tests
{
	[TestFixture]
	public class PerformanceTests
	{
		private readonly System.Diagnostics.Stopwatch _stopWatch = new System.Diagnostics.Stopwatch();

		public void SetupWatch()
		{
			_stopWatch.Reset ();
			_stopWatch.Start ();
		}
			

		[Test]
		public void RunIsValidIP()
		{
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch ();
			sw.Start ();
			for (int i = 0; i < 1000000; i++) {
				AppUtility.AssertValidIpAddress ("192.168.0.1");
			}
			sw.Stop ();

			Console.WriteLine (sw.Elapsed);
		}

		[Test]
		public void RunIsValidAlphaNumericV1()
		{
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch ();
			sw.Start ();
			for (int i = 0; i < 1000000; i++) {
				AppUtility.IsValidAlphaNumericString ("thisissoveryvalid111222333444555-_-------______.....___99999");
			}
			sw.Stop ();

			Console.WriteLine (sw.Elapsed);
		}



		[Test]
		public void RunIsValidAlphaNumericV2()
		{
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch ();
			sw.Start ();
			for (int i = 0; i < 1000000; i++) {
				AppUtility.IsValidAlphaNumericStringV2 ("thisissoveryvalid111222333444555-_-------______.....___99999");
			}
			sw.Stop ();

			Console.WriteLine (sw.Elapsed);
		}

		/*
		 * There is a bug in NUnit under
		 * mono that causes serialization
		 * to fail.  I've had a lot of 
		 * fun looking at the mono class
		 * library source to figure this out.
		 */
		[Test]
		[Ignore]
		public void RunSerialization()
		{
			SecurityManager manager = SecurityManager.Current;
			manager.GenerateToken ();
			var card = manager.GenerateCardData ("George");

			SetupWatch ();
			var packet = new CommunicationPacket (PacketTypeEnum.StartTransact) {
				//Session = Guid.NewGuid (),
				EncodedNonce = CryptoUtils.GetRandom (8),
				AccountName = "George",
				TransactionType = TransactionType.Deposit,
				TransactionAmount = Currency.MaxValue,
				AtmCard = card,
			};

			for (int i = 0; i < 100; i++) { 
				Console.WriteLine (i);
				byte[] theBits = Utilities.Serialize<CommunicationPacket> (packet);
				Console.WriteLine (theBits.Length);
			}

			StopTheWatch ("Packet Serialization");
			Assert.Pass ();
		}

		public void StopTheWatch(string testName)
		{
			_stopWatch.Stop ();
			Console.WriteLine ("Test: {0}; Elapsed: {1}", testName, _stopWatch.ElapsedMilliseconds);
		}

	}
}

