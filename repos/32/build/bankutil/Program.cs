﻿using System;
#if (LINUX)
using Mono.Unix;
#endif
using System.Threading;
using GenericName;
using System.IO;

namespace bank
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//Setting up an unhandled exception handlers, to guarantee we exit with code 255 on unhandled exceptions.
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(UnhandledExceptionHandler);

			//Defaults
			string bankFile = AppUtility.DEFAULT_AUTH_FILE;
			int port = AppUtility.DEFAULT_PORT_NUMBER;

			int i = 0;

			while (i < args.Length) {

				string a = AppUtility.GetArgOrExit (args, i++);

				if (a.Length < 2)
					AppUtility.ExitCommandLineError ();

				/*
				 * Get only the switch portion of the command line argument.
				 * e.g. -s or -p
				 */
				string cmdSwitch = a.Substring (0, 2);

				/*
				 * Detecting the case where the command line
				 * argument has no space.
				 * e.g. - "-p3001" rather than "-p 3001"
				 */
				bool switchOnly = a.Length <= 2;

				switch (cmdSwitch) {

				/*
				 * -s is for the bank authorization file.
				 * Format: [_\-\.0-9a-z]+ 
				 * Note: . and .. are specifically disallowed.
				 */
				case "-s":
					if (switchOnly)
						bankFile = AppUtility.GetArgOrExit (args, i++);
					else
						bankFile = AppUtility.GetUnspacedArg (args, i);
					break;
				case "-p":
					/*
					 * -p is for the port number.
					 * Format: Number
					 * Range: (1024..65535)
					 */
					string portStr = null;
					if (switchOnly)
						portStr = AppUtility.GetArgOrExit (args, i++);
					else
						portStr = AppUtility.GetUnspacedArg (args, i);

					if(portStr.StartsWith("0"))
						AppUtility.ExitCommandLineError ();

					if (!Int32.TryParse (portStr, out port))
						AppUtility.ExitCommandLineError ();
					
					break;
				default:
					/*
					 * No unsupport arguments.  Goodbye!
					 */
					AppUtility.ExitCommandLineError ();
					break;
				}
			}

			AppUtility.AssertValidFilename (bankFile);
			AppUtility.AssertValidPortNumber (port);
		
			AppUtility.WriteLog ("[DEBUG] AuthFile: {0}{1}Port:{2}", bankFile, Environment.NewLine, port);
			BankServer server;
			try{
				server = new BankServer (bankFile, port);
				/*
				 * Using the conditional compilation symbol so that
				 * testing on windows works properly.  Attempting to wait
				 * on a Unix Signal seems to crash the program
				 * on windows.  So in the windows case we just sit and 
				 * wait for someone to hit the 'q' key.  On linux,
				 * we sit and wait for a SIGTERM.
				 */

				#if (LINUX)
				var signal = new UnixSignal (Mono.Unix.Native.Signum.SIGTERM);
				var signals = new UnixSignal[] { signal };

				#endif 

				server.Start ();

				#if (!LINUX)

				while (Console.ReadKey().KeyChar != 'q') {
					Thread.Sleep (100);
				}

				AppUtility.WriteLog("Shutting down...");

				#else

				UnixSignal.WaitAny (signals);

				#endif

				server.Stop ();
			}catch(RequirementViolationException re) {
				/***
				 * From spec: Existing auth files are not valid for new runs of bank -- if the specified file already exists, bank should exit with return code 255
				 * */
				AppUtility.WriteError(String.Format("Exception Message: {0}",re.Message));
				AppUtility.ExitServerAuthFileError ();

			}
		}

		static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs args) 
		{
			Exception e = (Exception) args.ExceptionObject;
			AppUtility.WriteError("Runtime terminating: {0}", args.IsTerminating);
			AppUtility.WriteError (e);
			//At this point using AppUtility may not work anyway, so this is the last hail mary attempt to handle exception.
			Environment.Exit (255);
		}
	}
}
