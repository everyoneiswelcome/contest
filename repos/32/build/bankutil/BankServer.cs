﻿using System;
using System.Threading;
using System.IO;
using GenericName.Common;
using GenericName;
using System.Net.Sockets;
using System.Net;
using GenericName.Transactions;
using System.ServiceModel.Channels;

namespace bank
{
	public class BankServer
	{
		private Thread _runThread;
		private volatile bool _running = false;
		private SecurityManager _securityManager;
		private AccountContainer _accountContainer;
		private readonly BufferManager _bufferManager;
		private TcpListener _tcpListener = null;
		private FileInfo _authFileInfo;
		private int _port;

		public BankServer (string authFile, int port)
		{
			/**
			 * Existing auth files are not valid for new runs of bank -- if the specified file already exists, bank should exit with return code 255.
			 */
			_authFileInfo = new FileInfo (authFile);
			if (_authFileInfo.Exists ||
				!InitializeSecurityParameters() ||
				!CreateAuthFile()
			) {
				throw new RequirementViolationException ("Failed initializing the server instance.");
			}

			_port = port;
			_accountContainer = new AccountContainer ();
			_bufferManager = Utilities.CurrentBufferManager;
		}

		private bool CreateAuthFile()
		{
			Stream writer = null;
			try{
				writer = _authFileInfo.Open (FileMode.CreateNew,FileAccess.Write,FileShare.None); //CreateNew will fail if file exists, which is desired in this case.
				byte[] serializedToken = SecurityManager.Current.SerializeToken ();
				writer.Write (serializedToken, 0, serializedToken.Length);
				//From spec: Once the auth file is written completely, bank prints "created" (followed by a newline) to stdout.
				AppUtility.PrintLine("created");
				return true;
			}catch(IOException ioe) {
				AppUtility.WriteError ("Failed creating file for auth file");
				AppUtility.WriteError (String.Format ("Message: {0}\nStackTrace:{1}", ioe.Message, ioe.StackTrace));
				throw new RequirementViolationException ("Could not create auth file.");
			}catch(Exception e){
				AppUtility.WriteError ("Exception during auth file processing");
				AppUtility.WriteError (String.Format ("Message: {0}\nStackTrace:{1}", e.Message, e.StackTrace));
			}finally{
				if(writer != null)
				{
					try{
						writer.Close();
					}finally{
						writer = null;
					}
				}
			}
			return false;
		}

		private bool InitializeSecurityParameters()
		{
			//Init manager
			_securityManager = SecurityManager.Current;
			return _securityManager.GenerateToken();
		}

		/// <summary>
		/// Start server. Opens listener and runs server on a separate thread.
		/// </summary>
		public void Start()
		{
			if (_runThread != null)
				return;

			_tcpListener = new TcpListener (IPAddress.Any, _port);
			try {
				_tcpListener.Start ();
			} catch (SocketException) {
				throw new RequirementViolationException (255, "Socket is already bound.");
			}
			_runThread = new Thread (new ThreadStart (RunServer));
			_runThread.Start ();
		}

		/// <summary>
		/// Stop server.
		/// </summary>
		public void Stop()
		{
			_running = false;
			_runThread.Join ();
		}

		/// <summary>
		/// Runs the server thread, this is the function executed by the running thread.
		/// </summary>
		protected void RunServer()
		{
			_running = true;
			while (_running) {

				if (!_tcpListener.Pending ()) {
					Thread.Sleep (5);
				}

				TcpClient client = _tcpListener.AcceptTcpClient ();
				AppUtility.WriteLog("Received connection!");

				MessageHandler handler = new MessageHandler (client, _securityManager, _bufferManager, true);
				Instrumentation.StartTimer ("Thread Schedule Delay");
				ThreadPool.QueueUserWorkItem (new WaitCallback (HandleAccountTransaction), handler);
			}

			_tcpListener.Stop ();
		}

		private void HandleAccountTransaction(object state)
		{
			Instrumentation.StopAndReportTimer ("Thread Schedule Delay");
			Instrumentation.StartTimer ("Bank Transaction");
			var handler = state as MessageHandler;
			string accountName = null;
			try {

				//var helloPacket = new CommunicationPacket (PacketTypeEnum.Hello);
				/*
				 * REFACTOR-ALERT: Experimenting with removing the "hello"
				 * and session identifiers.
				 */
				/*
				 * Initiate connection with "hello"
				 */
				//handler.SendMessage (helloPacket);

				/*
				 * Receive the start transaction message,
				 * validate some simple junk and send
				 */
				var startPacket = handler.ReceiveMessage ();

				if (startPacket.PacketType != PacketTypeEnum.StartTransact)
					throw new ProtocolException ();

				if (!AppUtility.IsValidAlphaNumericString (startPacket.AccountName))
					throw new ProtocolException ();

				TransactionResult result = _accountContainer.OpenTransaction (
					startPacket.TransactionType, 
					startPacket.AccountName, 
					startPacket.AtmCard, 
					startPacket.TransactionAmount);

				if (!result.IsValid) {

					/*
					 * Transaction service didn't allow it.  Protocol
					 * says we abort at this point and move on.
					 * 
					 * Hitting continue (now return) should fire the finally { } 
					 * clause and dispose of the handler.
					 */

					handler.SendAbort ();
					return;

				} else {

					accountName = result.Transaction.AccountName;

					var confirmPacket = new CommunicationPacket (PacketTypeEnum.ConfirmTransact) {
						TransactionType = result.Transaction.TransactionType,
						TransactionAmount = result.Transaction.Amount,
						AccountName = result.Transaction.AccountName,
					};

					/*
					 * Solely in the generate case, we send the card data directly
					 * to the individual rather than receiving it.  This is us "issuing"
					 * the card.  
					 */
					if (result.Transaction.TransactionType == TransactionType.GenerateAccount)
						confirmPacket.AtmCard = result.Transaction.Account.Card;

					handler.SendMessage (confirmPacket);
				}

				/*
				 * Receive the transaction ok or abort.
				 * Send back what is needed.
				 */

				var verifyPacket = handler.ReceiveMessage ();

				if (verifyPacket.PacketType == PacketTypeEnum.OK) {
					
					var transaction = _accountContainer.CommitTransaction (accountName);

					/*
					 * Per the spec, have to write out the transaction to the console.
					 */
					AppUtility.PrintLine(String.Format("{0}", transaction));

				} else if (verifyPacket.PacketType == PacketTypeEnum.ServerAbort) { //TODO: Look at this, because it's fishy.
					_accountContainer.RollbackTransaction (accountName);
				} else {
					throw new ProtocolException ();
				}
			} catch (GenericName.TimeoutException) {
				/*
				 * Timeouts get identified and result in protocol errors.
				 */
				ProtocolUtility.ProtocolError ();
				//handler.SendProtocolCorrupt (); // Do NOT send a message after timeout.  It will crash the program.

				/*
				 * Rollback transaction.
				 */
				if (!String.IsNullOrEmpty (accountName))
					_accountContainer.RollbackTransaction (accountName);
			}
			catch (ProtocolException) {
				/*
				 * Spec specifies that Console.Out prints "protocol_error" when errors have been identified.
				 * This exception will get thrown whenever protocol issues develop. 
				 */
				ProtocolUtility.ProtocolError ();


				/*
				 * Rollback transaction.
				 */
				if (!String.IsNullOrEmpty (accountName))
					_accountContainer.RollbackTransaction (accountName);

				try {
					/**
					 * BUG: a man in the middle attack can refuse/drop packets and sending a protocol corrupt message
					 * could in fact throw a number of exceptions, which would be unhandled, and the thread would die. 
					 * In turn the server would slowly die as well, why, is still unwknown to me.
					*/
					handler.SendProtocolCorrupt ();
				} catch (Exception) {
					AppUtility.WriteLog ("Attempted to send ProtocolCorrupt, but failed. Continuing.");
				}
			} catch (ArgumentException ae) {
				//TODO: We may need to look at this method, it is a copy of the ProtocolException handling 
				/**
				 * ArgumentException, another one of those methods that fails when someone modifies the signature of encrypted packets.
				*/
				AppUtility.WriteLog ("Argument exception, probably a malformed packet.");
				/*
				 * Spec specifies that Console.Out prints "protocol_error" when errors have been identified.
				 * This exception will get thrown whenever protocol issues develop. 
				 */
				ProtocolUtility.ProtocolError ();

				/*
				 * Rollback transaction.
				 */
				if (!String.IsNullOrEmpty (accountName))
					_accountContainer.RollbackTransaction (accountName);

				try {
					/**
					 * BUG: a man in the middle attack can refuse/drop packets and sending a protocol corrupt message
					 * could in fact throw a number of exceptions, which would be unhandled, and the thread would die. 
					 * In turn the server would slowly die as well, why, is still unwknown to me.
					*/
					handler.SendProtocolCorrupt ();
				} catch (Exception) {
					AppUtility.WriteLog ("Attempted to send ProtocolCorrupt, but failed. Continuing.");
				}
			} catch (InvalidOperationException ioe) {
				/*
				 * Spec specifies that Console.Out prints "protocol_error" when errors have been identified.
				 * This exception will get thrown whenever protocol issues develop. 
				 */
				ProtocolUtility.ProtocolError ();


				/*
				 * Rollback transaction.
				 */
				if (!String.IsNullOrEmpty (accountName))
					_accountContainer.RollbackTransaction (accountName);

				try {
					/**
					 * BUG: a man in the middle attack can refuse/drop packets and sending a protocol corrupt message
					 * could in fact throw a number of exceptions, which would be unhandled, and the thread would die. 
					 * In turn the server would slowly die as well, why, is still unwknown to me.
					*/
					handler.SendProtocolCorrupt ();
				} catch (Exception) {
					AppUtility.WriteLog ("Attempted to send ProtocolCorrupt, but failed. Continuing.");
				}
			}catch (Exception e) {
				AppUtility.WriteLog ("Caught an unexpected exception. Transactions may not be rolled back! Ouch!");
				AppUtility.WriteLog ((e.Message == null) ? "NULL Message" : e.Message);
				AppUtility.WriteLog ((e.StackTrace == null) ? "NULL StackTrace" : e.StackTrace);
				#if DEBUG
				throw e;
				#endif
				/*
				 * Rollback transaction.
				 */
				if (!String.IsNullOrEmpty (accountName))
					_accountContainer.RollbackTransaction (accountName);
			} finally {
				AppUtility.WriteLog ("Running finally clause.  Handler: {0}", handler);
				handler.Dispose ();
				Instrumentation.StopAndReportTimer ("Bank Transaction");
			}
		}
	}
}

