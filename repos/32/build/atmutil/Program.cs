﻿using System;
using GenericName;
using System.Runtime.Serialization;
using System.IO;

namespace atm
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			//Don't ask
			Configuration.Stupid.ConfigurationFixes.Install ();
			//No seriously, stop asking
			System.Runtime.GCSettings.LatencyMode = System.Runtime.GCLatencyMode.SustainedLowLatency;

			//Setting up an unhandled exception handlers, to guarantee we exit with code 255 on unhandled exceptions.
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler (UnhandledExceptionHandler);

			BootstrapHelper.Bootstrap ();

			//OK, now you can ask, because these are pretty cool.
			Instrumentation.StartTimer ("ATM Bootstrap");
			Instrumentation.StartTimer ("ATM Runtime");
			AppUtility.AssertArgumentLength (args);
	
			TransactionType? transType = null;
			Currency transactionAmount = Currency.Zero; 
			string accountName = null;
			string bankFile = AppUtility.DEFAULT_AUTH_FILE;
			string cardFile = null;
			int port = AppUtility.DEFAULT_PORT_NUMBER;
			string ipString = AppUtility.DEFAULT_IP;

			int i = 0;

			while (i < args.Length) {
				string a = AppUtility.GetArgOrExit (args, i++);

				if (a.Length < 2)
					AppUtility.ExitCommandLineError ();

				/*
				 * Get only the switch portion of the command line argument.
				 * e.g. -s or -p
				 */
				string cmdSwitch = a.Substring (0, 2);

				/*
				 * Detecting the case where the command line
				 * argument has no space.
				 * e.g. - "-p3001" rather than "-p 3001"
				 */
				bool switchOnly = a.Length <= 2;

				switch (cmdSwitch) {
				case "-s":
					if (switchOnly)
						bankFile = AppUtility.GetArgOrExit (args, i++);
					else
						bankFile = AppUtility.GetUnspacedArg (args, i);

					AppUtility.AssertValidFilename (bankFile);
					break;
				case "-p":
					string portStr = null;
					if (switchOnly)
						portStr = AppUtility.GetArgOrExit (args, i++);
					else
						portStr = AppUtility.GetUnspacedArg (args, i);

					//Parameters cannot start 0
					if (portStr.StartsWith ("0"))
						AppUtility.ExitCommandLineError ();

					if (!Int32.TryParse (portStr, out port))
						AppUtility.ExitCommandLineError ();
					break;
				case "-i":
					ipString = AppUtility.GetArgOrExit (args, i++);
					break;
				case "-a":
					accountName = AppUtility.GetArgOrExit (args, i++);
					break;
				case "-c":
					cardFile = AppUtility.GetArgOrExit (args, i++);
					break;
				case "-n":
					if (transType.HasValue)
						AppUtility.ExitCommandLineError ();
					transType = TransactionType.GenerateAccount;
					transactionAmount = AppUtility.GetCurrencyOrExit (args, i++);
					break;
				case "-d":
					if (transType.HasValue)
						AppUtility.ExitCommandLineError ();
					transType = TransactionType.Deposit;
					transactionAmount = AppUtility.GetCurrencyOrExit (args, i++);
					break;
				case "-w":
					if (transType.HasValue)
						AppUtility.ExitCommandLineError ();
					transType = TransactionType.Withdrawal;
					transactionAmount = AppUtility.GetCurrencyOrExit (args, i++);
					break;
				case "-g":
					if (transType.HasValue)
						AppUtility.ExitCommandLineError ();
					transType = TransactionType.GetBalance;
					break;
				default:
					AppUtility.ExitCommandLineError ();
					break;
				}
			}

			if (cardFile == null)
				cardFile = String.Format ("{0}.card", accountName);

			/*
			 * Blob of validate-or-exit lines.
			 * Ick
			 */
			var ip = AppUtility.AssertValidIpAddress (ipString);

			/*
			 * Spec: All filenames must adhere to format.
			 */
			AppUtility.AssertValidFilename (bankFile);

			/*
			 * Spec: All port numbers between 1024 and 65535
			 */
			AppUtility.AssertValidPortNumber (port);

			/*
			 * Spec: Account names must be alphanumeric with _, -, or .
			 */
			if (!AppUtility.IsValidAlphaNumericString (accountName))
				AppUtility.ExitCommandLineError ();

			/*
			 * Spec: All filenames must adhere to format.
			 */
			AppUtility.AssertValidFilename (cardFile);

			/*
			 * Spec: transaction arguments must be between 0.00 and 4294967295.99
			 */
			AppUtility.WriteLog ("[Transaction Type: {0}] [Volume: {1}]", transType.Value, transactionAmount);
			if (!AppUtility.IsValidTransactionRange (transType.Value, transactionAmount))
				AppUtility.ExitCommandLineError ();

			if (!transType.HasValue)
				AppUtility.ExitCommandLineError ();

			AppUtility.WriteLog ("[DEBUG] atm AuthFile: {0} Ip: {1}, Port: {2}, Type: {3}, Amount: {4}", bankFile, ip, port, transType, transactionAmount);
			Instrumentation.StopAndReportTimer ("ATM Bootstrap");
			try {
				AtmClient atmClient = new AtmClient (bankFile, ip, port);

				Instrumentation.StartTimer ("ATM Transaction");
				if (atmClient.RunTransaction (transType.Value, accountName, transactionAmount, cardFile)) {
					Instrumentation.StopAndReportTimer ("ATM Transaction");
					Instrumentation.StopAndReportTimer ("ATM Runtime");
					AppUtility.ExitSuccess ();
				} else {
					Instrumentation.StopAndReportTimer ("ATM Transaction");
					Instrumentation.StopAndReportTimer ("ATM Runtime");
					AppUtility.ExitAtmAccountError ();
				}

			} catch (RequirementViolationException re) {
				AppUtility.WriteError (re.Message);
				AppUtility.WriteError (re.StackTrace);
				AppUtility.ExitAtmAccountError ();
			} catch (SerializationException re) {
				AppUtility.WriteError (re.Message);
				AppUtility.WriteError (re.StackTrace);
				AppUtility.ExitAtmAccountError ();
			} catch (FileNotFoundException fnf) {
				//Bug caught this morning, where passing a non-existing file would thrown an exception that was not caught by a specific handler.
				AppUtility.WriteError (fnf.Message);
				AppUtility.WriteError (fnf.StackTrace);
				AppUtility.ExitAtmAccountError ();
			} catch (Exception e) {
				AppUtility.WriteError ("An otherwise uncaught exception in the client!");
				AppUtility.WriteError (e.Message);
				AppUtility.WriteError (e.StackTrace);
				#if DEBUG
				throw e; //throw if in debug mode.
				#endif
				AppUtility.ExitAtmAccountError ();
			}
		}

		static void UnhandledExceptionHandler (object sender, UnhandledExceptionEventArgs args)
		{
			Exception e = (Exception)args.ExceptionObject;
			AppUtility.WriteError ("Runtime terminating: {0}", args.IsTerminating);
			AppUtility.WriteError (e);
			//At this point using AppUtility may not work anyway, so this is the last hail mary attempt to handle exception.
			Environment.Exit (255);
		}
	}
}
