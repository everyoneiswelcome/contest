﻿using System;
using GenericName.Common;
using System.IO;
using GenericName;
using System.Net.Sockets;
using System.Net;
using GenericName.Transactions;
using System.ServiceModel.Channels;

namespace atm
{
	public class AtmClient
	{
		private readonly SecurityManager _securityManager;
		private readonly IPAddress _ip;
		private readonly int _port;
		private readonly BufferManager _bufferManager;

		public AtmClient (string authFileName, IPAddress ip, int port)
		{
			_ip = ip;
			_port = port;
			_securityManager = SecurityManager.Client;
			_bufferManager = Utilities.CurrentBufferManager;

			byte[] authBytes = File.ReadAllBytes (authFileName);

			if (!_securityManager.DeserializeToken (authBytes, false))
				throw new RequirementViolationException ();
		}

		public bool RunTransaction(TransactionType type, string accountName, Currency transactionAmount, string cardFile)
		{
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch ();
			sw.Start ();
			CardData cardData = null;
			MessageHandler handler = null;
			if (type != TransactionType.GenerateAccount) {
				if (!File.Exists (cardFile))
					return false;
				/*
				 * We're not generating an account, so load
				 * the card file.
				 */
                try
                {
                    /**
                     * Throws System.Runtime.Serialization.SerializationException - which cannot be left unhandled or it kills the client atm.
                     */
                    cardData = GetCardData(cardFile);
                }
                catch (System.Runtime.Serialization.SerializationException se)
                {
                    AppUtility.WriteLog("Invalid card detected.");
                    AppUtility.ExitAtmAccountError();
                    return false;
                }
				// FIXED - NullReferenceException not being caught and killing client.
				catch (NullReferenceException)
				{
					AppUtility.WriteLog("Invalid card detected.");
					AppUtility.ExitAtmAccountError();
					return false;
				}
			}
			else {
				/*
				 * We're generating an account, just verify that
				 * the card file is not already there.
				 */
				if (File.Exists (cardFile))
					return false;
			}

			try {
				
				using (var client = new TcpClient ()) {
					
					/*
					 * Connect, pass to handler,
					 * and wait for hello.
					 */
					client.Connect (_ip, _port);

					handler = new MessageHandler (client, _securityManager, _bufferManager, false);

					/*
					 * REFACTOR-ALERT: Experimenting with removing the "hello"
					 * and session identifiers.
					 */
					//var helloPacket = handler.ReceiveMessage ();

					//if(helloPacket.PacketType != PacketTypeEnum.Hello)
					//{
					//	handler.SendAbort();
					//	return false;
					//}

					/*
					 * Hello is just for establishing a session and
					 * starting the cycle of nonce exchanges.  Thus,
					 * there's no real processing in this method 
					 * that happens after hello.  We just send 
					 * the transaction immediately now.
					 */

					var startPacket = new CommunicationPacket (PacketTypeEnum.StartTransact) {
						AccountName = accountName,
						TransactionAmount = transactionAmount,
						TransactionType = type, 
						AtmCard = cardData,
					};

					handler.SendMessage (startPacket);

					/*
					 * Now wait for transaction confirmation
					 * (assuming there was no abort due to bad
					 * parameters) and validate all the parameters 
					 * match what we want to do.
					 */

					var confirmPacket = handler.ReceiveMessage ();
				
					if(confirmPacket.PacketType == PacketTypeEnum.ServerAbort)
						return false;

					if(confirmPacket.AccountName.Equals(accountName)
						&& confirmPacket.TransactionType.Equals(type)
						&& (confirmPacket.TransactionType == TransactionType.GetBalance || confirmPacket.TransactionAmount.Equals(transactionAmount)))
					{
						if(type == TransactionType.GenerateAccount)
						{
							WriteCardData(cardFile, confirmPacket.AtmCard);
						}

						if(type == TransactionType.GetBalance)
							transactionAmount = confirmPacket.TransactionAmount;

						handler.SendOK();
						Console.Out.WriteLine(Transaction.GetTransactionOutput(type, transactionAmount, accountName));
						Console.Out.Flush();
						return true;
					} else
					{
						/*
						 * Something didn't match, we send the client abort code.
						 */
						handler.SendAbort();
						return false;
					}

					/*
					 * Shortening protocol to eliminate the final "OK" call
					 * Will run under profiling to see difference.
					 */

//					var okPacket = handler.ReceiveMessage();
//
//					if(okPacket.PacketType == PacketTypeEnum.OK) {
//						Console.Out.WriteLine(Transaction.GetTransactionOutput(type, transactionAmount, accountName));
//						Console.Out.Flush();
//						return true;
//					} else {
//						return false;
//					}
				}
			} catch (GenericName.TimeoutException) {
				AppUtility.ExitAtmError (); //Timeouts specifically should be error 63, not 255.
				return false;
			}
			catch (CommunicationCorruptedException) {
				AppUtility.ExitAtmError ();
				return false;
			}
			catch (ProtocolException) {
				AppUtility.ExitAtmError ();
				return false;
			}
			finally {
				if(handler != null)
					handler.Dispose();

				sw.Stop ();
				AppUtility.WriteLog ("Transaction Runtime: {0}", sw.Elapsed);
			}
		}

		private CardData GetCardData(string cardFile)
		{
			byte[] cardFileBytes = File.ReadAllBytes (cardFile);
			return Utilities.Deserialize<CardData> (cardFileBytes);
		}

		private void WriteCardData(string cardFile, CardData cardData)
		{
			byte[] cardFileBytes = Utilities.Serialize (cardData);
			File.WriteAllBytes (cardFile, cardFileBytes);
		}
	}
}

