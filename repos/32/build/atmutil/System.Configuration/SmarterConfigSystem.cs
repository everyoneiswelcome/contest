﻿using System;

namespace Configuration.Stupid
{
	/// <summary>
	/// This is the smarter config system that helps the mono socket implementation not be stupid.
	/// We install this at the top of the ATM program, and then mono stops reading the whole
	/// machine.config file and adding 150ms of execution time to our program.
	/// 
	/// Note: I actually don't think the mono implementors are stupid, I just wish there
	/// was a way to turn off this dependency on the machine.config file <3.
	/// </summary>
	public class SmarterConfigSystem : System.Configuration.Internal.IInternalConfigSystem
	{
		public SmarterConfigSystem ()
		{
		}

		#region IInternalConfigSystem implementation

		public object GetSection (string configKey)
		{
			var section = new System.Net.Configuration.SettingsSection ();
			section.Ipv6.Enabled = false;
			return section;
		}

		public void RefreshConfig (string sectionName)
		{
			
		}

		public bool SupportsUserConfig {
			get {
				return false;
			}
		}

		#endregion
	}
}

