﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Configuration.Stupid
{
	public static class ConfigurationFixes
	{
		public static void Install()
		{
			try 
			{
				FieldInfo fiSystem = typeof(System.Configuration.ConfigurationManager)
					.GetField("configSystem", BindingFlags.NonPublic | BindingFlags.Static);
				fiSystem.SetValue(null, new SmarterConfigSystem());


				FieldInfo giSystem = typeof(System.Guid)
					.GetField("_rng", BindingFlags.NonPublic | BindingFlags.Static);
				giSystem.SetValue(null, new System.Security.Cryptography.RNGCryptoServiceProvider());


				FieldInfo cgSystem = typeof(System.Security.Cryptography.CryptoConfig)
					.GetField("algorithms", BindingFlags.NonPublic | BindingFlags.Static);
				var algos = new Dictionary<string, Type>();
				algos.Add("System.Security.Cryptography.HMACSHA256", typeof(HMACSHA256));
				algos.Add("HMACSHA256", typeof(HMACSHA256));
				algos.Add("SHA256", typeof(System.Security.Cryptography.SHA256Managed));
				cgSystem.SetValue(null, algos);

			} catch (Exception) {
			}
		}
	}
}

