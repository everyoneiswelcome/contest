﻿using System;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using GenericName.Common;
using System.Collections.Generic;
using System.ServiceModel.Channels;
using System.Xml;
using System.Collections.Concurrent;

namespace GenericName
{
	public static class Utilities
	{
		private static BufferManager _bufferManager;

		public const int MAX_BUFFER_BLOCK = 2048;

		public static BufferManager CurrentBufferManager {
			get {
				
				if (_bufferManager == null)
					_bufferManager = BufferManager.CreateBufferManager (MAX_BUFFER_BLOCK * 256, MAX_BUFFER_BLOCK);

				return _bufferManager;
			}
		}
			
		/// <summary>
		/// Deserialize the specified jsonToken.
		/// </summary>
		/// <param name="jsonToken">Json token.</param>
		/// <typeparam name="T">The serializable class.</typeparam>
		public static T Deserialize<T>(string jsonToken) where T:class
		{
			if (String.IsNullOrWhiteSpace(jsonToken)) {
				throw new NullReferenceException ("JSON string cannot be null or empty/whitespace!");
			}
			return Deserialize<T> (Encoding.UTF8.GetBytes (jsonToken));
		}

		private static ConcurrentDictionary<Type, XmlObjectSerializer> _serializerCache = new ConcurrentDictionary<Type, XmlObjectSerializer>(2, 10);

		public static XmlObjectSerializer GetSerializer<T> ()
		{
			Type t = typeof(T);
			if (_serializerCache.ContainsKey (t))
				return _serializerCache [t];
			XmlObjectSerializer serializer = new DataContractSerializer (typeof(T));
			_serializerCache [t] = serializer;
			return serializer;
		}

		/// <summary>
		/// Deserialize the specified jsonToken.
		/// </summary>
		/// <param name="jsonToken">Json token.</param>
		/// <typeparam name="T">The serializable class.</typeparam>
		public static T Deserialize<T>(byte[] jsonToken) where T:class
		{
			if (!isValidData(jsonToken)) {
				throw new NullReferenceException ("JSON string cannot be null or empty/whitespace!");
			}

			var serializer = GetSerializer<T> ();
			using (MemoryStream stream = new MemoryStream(jsonToken)) {
				var reader = XmlDictionaryReader.CreateBinaryReader (stream, new XmlDictionaryReaderQuotas () {  });
				try{
					T obj = (T) serializer.ReadObject(reader);
					return obj;
				}catch(InvalidDataContractException e) {
					Console.Error.WriteLine (e.Message);
					Console.Error.WriteLine (e.StackTrace);
					throw e;
				}
			}
		}

		public static T Deserialize<T>(byte[] data, int length)
		{
			var serializer = GetSerializer<T> ();
			var reader = XmlDictionaryReader.CreateBinaryReader (data, 0, length, new XmlDictionaryReaderQuotas ());
			try {
				T obj = (T)serializer.ReadObject (reader);
				return obj;
			} catch (InvalidDataContractException e) {
				Console.Error.WriteLine (e.Message);
				Console.Error.WriteLine (e.StackTrace);
				throw e;
			}
		}

		/// <summary>
		/// Serialize the specified object to JSON
		/// </summary>
		/// <param name="obj">Object to serialize</param>
		/// <typeparam name="T">The 1st type parameter.</typeparam>
		public static byte [] Serialize<T>(T obj) where T:class
		{
			if (obj == null) {
				throw new NullReferenceException ("Object cannot be null!");
			}

			var serializer = GetSerializer<T> ();

			using (MemoryStream stream = new MemoryStream ()) {
                using (var writer = XmlDictionaryWriter.CreateBinaryWriter(stream))
                {
                    serializer.WriteObject(writer, obj);
					writer.Flush();
					byte[] array = stream.ToArray ();
#if DEBUG
					AppUtility.WriteLog(Encoding.UTF8.GetString(array));
#endif
					return array;
                }
			}

		}

		/// <summary>
		/// A simple verification routine, checks if data is valid. Valid is non null arrays with 1 to MAX_DATA_SIZE elements.
		/// </summary>
		/// <returns><c>true</c>, if valid data was used, <c>false</c> otherwise.</returns>
		/// <param name="data">Byte array to be examined</param>
		public static bool isValidData(byte[] data)
		{
			return (data != null &&
				data.Length > 0 &&
				data.LongLength <= CryptoUtils.MAX_DATA_SIZE
			);
		}
	}
}

