﻿using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace GenericName
{
	/// <summary>
	/// Helper to aid in finding bottlenecks in code.
	/// </summary>
	public static class Instrumentation
	{
		private static Dictionary<string, Stopwatch> _timers = new Dictionary<string, Stopwatch>();

		/// <summary>
		/// Starts the timer and tracks it during program execution.
		/// </summary>
		/// <param name="name">Name of the timer.</param>
		[Conditional("DEBUG")]
		public static void StartTimer(string name)
		{
			var sw = new Stopwatch ();
			_timers [name] = sw;
			sw.Start ();
		}

		/// <summary>
		/// Stops the identified timer and reports the results.
		/// </summary>
		/// <param name="name">Name of the timer.</param>
		[Conditional("DEBUG")]
		public static void StopAndReportTimer(string name)
		{
			var sw = _timers [name];
			sw.Stop ();
			AppUtility.WriteLog ("{0}: {1}", name, sw.Elapsed);
			_timers [name] = null;
		}
	}
}

