﻿using System;

namespace GenericName
{
	public class RequirementViolationException : Exception
	{
		public RequirementViolationException ():base() {}

		public RequirementViolationException(string message) : base(message) {}

		public RequirementViolationException(int exitCode, string message) : base(message) {
			ExitCode = exitCode;
		}

		public int? ExitCode {get;set;}
	}
}

