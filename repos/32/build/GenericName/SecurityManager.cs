﻿using System;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Threading;
using System.Linq;

namespace GenericName.Common
{
	/// <summary>
	/// Security manager - handles auth file creation and loading.
	/// </summary>
	public class SecurityManager
	{
		private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim ();
		protected readonly byte[] securityOfficerKey;
		private SecurityToken token = null;
		protected SecurityToken Token{
			get{
				if (token == null) {
					throw new NullReferenceException ("Token is uninitialized, please generate or load one.");
				} else {
					return token;
				}
			}
		}

		protected SecurityManager(byte [] key)
		{
			try{
				_lock.EnterWriteLock();
				securityOfficerKey = key;
			}finally{
				_lock.ExitWriteLock ();
			}
		}

		private SecurityManager ()
			:this(CryptoUtils.GetRandom (CryptoUtils.SIGN_KEY_SIZE_BYTES))
		{
		}

		#region Singleton pattern
		private static SecurityManager _instance;// = new SecurityManager ();
		public static SecurityManager Current{
			get{
				if (_instance == null)
					_instance = new SecurityManager ();
				return _instance;
			}
			protected set {
				_instance = value;
			}
		}

		private static SecurityManager _client;// = new SecurityManager(new byte[CryptoUtils.SIGN_KEY_SIZE_BYTES]);
		public static SecurityManager Client {
			get {
				if (_client == null)
					_client = new SecurityManager (new byte[CryptoUtils.SIGN_KEY_SIZE_BYTES]);
				return _client;
			}
		}
		#endregion

		/// <summary>
		/// Generate a security token and sign it with the security officer key.
		/// </summary>
		/// <returns><c>true</c>, if token was generated, <c>false</c> otherwise.</returns>
		public bool GenerateToken()
		{
			try{
				_lock.EnterWriteLock();
				//Make a new token and then sign it
				token = new SecurityToken ();
				token.EncodedEncryptDecryptKey = Convert.ToBase64String (CryptoUtils.GetRandom (CryptoUtils.SIGN_KEY_SIZE_BYTES));
				token.EncodedSigningKey = Convert.ToBase64String (CryptoUtils.GetRandom (CryptoUtils.SIGN_KEY_SIZE_BYTES));
				token.EncodedIV = Convert.ToBase64String (CryptoUtils.GetRandom (CryptoUtils.IV_SIZE_BYTES));
				#if DEBUG
				AppUtility.WriteLog("Spilling the beans!");
				AppUtility.WriteLog(String.Format("Encryption Key: {0}",token.EncodedEncryptDecryptKey));
				AppUtility.WriteLog(String.Format("Encryption IV : {0}",token.EncodedIV));
				AppUtility.WriteLog(String.Format("Signing Key   : {0}",token.EncodedSigningKey));
				#endif
				return SignToken (token, this.securityOfficerKey);
			}finally{
				_lock.ExitWriteLock ();
			}
		}

		/// <summary>
		/// Deserializes a token string in JSON format
		/// </summary>
		/// <returns><c>true</c>, if token was deserialized, <c>false</c> otherwise.</returns>
		/// <param name="jsonToken">JSON serialized token.</param>
		public bool DeserializeToken(string jsonToken, bool enforceSignatureCheck = true)
		{
			return DeserializeToken (Encoding.UTF8.GetBytes (jsonToken), enforceSignatureCheck);
		}

		public bool DeserializeToken(byte[] jsonToken, bool enforceSignatureCheck)
		{
			if (jsonToken == null || jsonToken.Length == 0) {
                //Throwing an exception is cumbersome, just return a false.
                //throw new NullReferenceException ("Authentication file info cannot be null!");             
                return false;
			}

			try{
				string[] tokenStr = Encoding.ASCII.GetString(jsonToken).Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
				SecurityToken token = new SecurityToken();
				token.EncodedEncryptDecryptKey = tokenStr[0];
				token.EncodedIV = tokenStr[1];
				token.EncodedServerSignature = tokenStr[2];
				token.EncodedSigningKey = tokenStr[3];

				//SecurityToken token = Utilities.Deserialize<SecurityToken>(jsonToken);

				//TODO: reading the SO key may conflict if multiple writers want to update the SO key, luckily, SO key is generated once.
				if(//If the signature check is not enforced, or if it is and it passes, proceed
					!enforceSignatureCheck ||
					(enforceSignatureCheck && VerifyToken(token, this.securityOfficerKey))
				)
				{
					_lock.EnterWriteLock();
					try{
						this.token = token;
						return true;
					}finally{
						_lock.ExitWriteLock ();
					}
				}
				System.Diagnostics.Trace.TraceError("Token was deserialized, but invalid!");
				return false;
			}catch(InvalidDataContractException e) {
				AppUtility.WriteError (e.Message);
				AppUtility.WriteError (e.StackTrace);
				return false;
			}catch(ArgumentException ae) {
				AppUtility.WriteError (ae.Message);
				AppUtility.WriteError (ae.StackTrace);
				return false;
			}catch (SerializationException re) {
				AppUtility.WriteError (re.Message);
				AppUtility.WriteError (re.StackTrace);
				return false;
			}catch(Exception e) {
				AppUtility.WriteError ("An otherwise uncaught exception in SecurityManager!");
				AppUtility.WriteError (e.Message);
				AppUtility.WriteError (e.StackTrace);
				#if DEBUG
				throw e; //throw if in debug mode.
				#endif
				return false;
			}
		}


		/// <summary>
		/// Serializes the token within the session.
		/// </summary>
		/// <returns>The token.</returns>
		public byte [] SerializeToken()
		{
			return SerializeToken (this.token);
		}

		/// <summary>
		/// Serializes a SecurityToken object to a JSON string.
		/// </summary>
		/// <returns>Serialized token in a JSON string</returns>
		/// <param name="token">Token to be serialized.</param>
		private byte [] SerializeToken(SecurityToken token)
		{
			if (token == null) {
				throw new NullReferenceException ("Authentication file info cannot be null!");
			}

			_lock.EnterReadLock ();
			try{
				StringBuilder str = new StringBuilder();
				str.AppendLine(token.EncodedEncryptDecryptKey);
				str.AppendLine(token.EncodedIV);
				str.AppendLine(token.EncodedServerSignature);
				str.AppendLine(token.EncodedSigningKey);
				return Encoding.ASCII.GetBytes(str.ToString());
				return Utilities.Serialize<SecurityToken>(token);
			}finally{
				_lock.ExitReadLock ();
			}
		}

		/// <summary>
		/// Signs a security token.
		/// </summary>
		/// <returns><c>true</c>, if token was signed, <c>false</c> otherwise.</returns>
		/// <param name="token">Token to be signed</param>
		/// <param name="securityOfficerKey">Security officer key.</param>
		protected bool SignToken(SecurityToken token, byte[] securityOfficerKey){
			if (token == null ||
				String.IsNullOrWhiteSpace (token.EncodedEncryptDecryptKey) ||
				String.IsNullOrWhiteSpace (token.EncodedSigningKey) ||
				!Utilities.isValidData(securityOfficerKey)
			) {
				return false;
			}
			try{
				byte[] signature = CryptoUtils.SignData(
					Encoding.UTF8.GetBytes(String.Concat(token.EncodedEncryptDecryptKey,token.EncodedSigningKey,token.EncodedIV)), 
					securityOfficerKey);
				token.EncodedServerSignature = Convert.ToBase64String(signature);
				return true;
			}catch(Exception e) {
				Console.Error.WriteLine (e.Message);
				Console.Error.WriteLine (e.StackTrace);
				return false;
			}
		}
			
		/// <summary>
		/// Verifies a token's signature.
		/// </summary>
		/// <returns><c>true</c>, if token was verifyed, <c>false</c> otherwise.</returns>
		/// <param name="token">Token to be verified</param>
		/// <param name="securityOfficerKey">Security officer key.</param>
		protected bool VerifyToken(SecurityToken token, byte[] securityOfficerKey){
			if (token == null ||
				String.IsNullOrWhiteSpace (token.EncodedEncryptDecryptKey) ||
				String.IsNullOrWhiteSpace (token.EncodedSigningKey) ||
				String.IsNullOrWhiteSpace (token.EncodedIV) ||
				String.IsNullOrWhiteSpace (token.EncodedServerSignature) ||
				!Utilities.isValidData(securityOfficerKey)
			) {
				return false;
			}
			_lock.EnterReadLock();
			try{
				KeyedHashAlgorithm signer = CryptoUtils.GetHmacSigner(securityOfficerKey);
				byte[] signature = signer.ComputeHash(Encoding.UTF8.GetBytes(
					String.Concat(token.EncodedEncryptDecryptKey,token.EncodedSigningKey,
						token.EncodedIV)));
				return Enumerable.SequenceEqual(signature, Convert.FromBase64String(token.EncodedServerSignature));
			}catch(Exception e) {
				Console.Error.WriteLine (e.Message);
				Console.Error.WriteLine (e.StackTrace);
				return false;
			}finally{
				_lock.ExitReadLock();
			}
		}

		/// <summary>
		/// Check whether the security manager's token is valid.
		/// </summary>
		/// <returns><c>true</c> if this instance is valid; otherwise, <c>false</c>.</returns>
		public bool IsValid()
		{
			return VerifyToken (this.token, this.securityOfficerKey);
		}

		/// <summary>
		/// Generates the card data
		/// </summary>
		/// <returns>The card data.</returns>
		/// <param name="accountName">Account name.</param>
		public CardData GenerateCardData(string accountName)
		{
			AppUtility.WriteLog (String.Format("Generating card for account {0}", accountName));
			if(String.IsNullOrWhiteSpace(accountName))
				throw new NullReferenceException("Null or invalida account name");
			//account name hash should be the same all the time, byte by byte!
			byte [] accountNameHash = CryptoUtils.HashData (accountName);
			byte [] signedCardData = CryptoUtils.SignData (accountNameHash, this.securityOfficerKey);
			CardData card = new CardData (accountNameHash, signedCardData);
			return card;
		}

		/// <summary>
		/// Verifies the card structure
		/// </summary>
		/// <returns><c>true</c>, if card was verifyed, <c>false</c> otherwise.</returns>
		/// <param name="AccountName">Account name.</param>
		/// <param name="card">Card.</param>
		public bool VerifyCard(string AccountName, CardData card)
		{
			if (null == card)
				throw new NullReferenceException ("Invalid card data");
			_lock.EnterReadLock ();
			try{
				return CryptoUtils.CheckSignature (CryptoUtils.HashData(AccountName), this.securityOfficerKey, card.Signature);
			}finally{
				_lock.ExitReadLock ();
			}
		}
			
		/// <summary>
		/// Protects (encrypts) the data.
		/// </summary>
		/// <returns>The data.</returns>
		/// <param name="data">Data.</param>
		public byte [] ProtectData(byte[] data){
			if (data == null ||
				!Utilities.isValidData(data)
			) {
				throw new InvalidDataException ("Cannot protect provided data");
			}
			try{
				SymmetricAlgorithm provider = CryptoUtils.GetAesProvider(Convert.FromBase64String(token.EncodedEncryptDecryptKey),Convert.FromBase64String(token.EncodedIV));
				byte[] ciphertext = CryptoUtils.Encrypt(provider.CreateEncryptor(), data);
				return ciphertext;
			}catch(Exception e) {
				Console.Error.WriteLine (e.Message);
				Console.Error.WriteLine (e.StackTrace);
				throw e;
			}
		}

		/// <summary>
		/// Reveals (decrypts) the data.
		/// </summary>
		/// <returns>The data.</returns>
		/// <param name="ciphertext">Ciphertext.</param>
		public byte [] RevealData(byte[] ciphertext){
			if (ciphertext == null ||
				!Utilities.isValidData(ciphertext)
			) {
				throw new InvalidDataException ("Cannot reveal provided data");
			}
			try{
				SymmetricAlgorithm provider = CryptoUtils.GetAesProvider(Convert.FromBase64String(token.EncodedEncryptDecryptKey),Convert.FromBase64String(token.EncodedIV));
				byte[] data = CryptoUtils.Decrypt(provider.CreateDecryptor(), ciphertext);
				return data;
			}catch(Exception e) {
				Console.Error.WriteLine (e.Message);
				Console.Error.WriteLine (e.StackTrace);
				throw e;
			}
		}

		/// <summary>
		/// Signs the data with the communications signing key.
		/// </summary>
		/// <returns>The data.</returns>
		/// <param name="data">Data.</param>
		public byte[] SignData(byte[] data){
			if (data == null ||
				!Utilities.isValidData(data)
			) {
				throw new InvalidDataException ("Cannot sign provided data");
			}
			try{
				byte[] signature = CryptoUtils.SignData(data, Convert.FromBase64String(token.EncodedSigningKey));
				return signature;
			}catch(Exception e) {
				Console.Error.WriteLine (e.Message);
				Console.Error.WriteLine (e.StackTrace);
				throw e;
			}
		}

		public bool VerifySignature(byte[] data, byte[] signature){
			if (data == null ||
				!Utilities.isValidData(data)
			) {
				throw new InvalidDataException ("Cannot sign provided data");
			}
			try{
				byte[] calcSignature = CryptoUtils.SignData(data, Convert.FromBase64String(token.EncodedSigningKey));
				return Enumerable.SequenceEqual(calcSignature, signature);
			}catch(Exception e) {
				Console.Error.WriteLine (e.Message);
				Console.Error.WriteLine (e.StackTrace);
				throw e;
			}
		}

		public ProtectedCommunicationPacket ProtectPackage(CommunicationPacket packet)
		{
			if (null == packet)
				throw new ArgumentNullException ("packet");
			byte [] encryptedPacket = ProtectData (Utilities.Serialize<CommunicationPacket>(packet));
			byte [] packetSignature = SignData(encryptedPacket);
			return new ProtectedCommunicationPacket (encryptedPacket, packetSignature);
		}

		public CommunicationPacket RevealPackage(ProtectedCommunicationPacket packet)
		{
			if (null == packet)
				throw new ArgumentNullException ("packet");
			if (VerifySignature (packet.EncryptedCommunicationPacket, packet.PacketSignature)) {
				byte[] commData = RevealData (packet.EncryptedCommunicationPacket);
				return Utilities.Deserialize<CommunicationPacket> (commData);
			} else
				throw new ApplicationException ("Invalid packet received!");
		}

		public override string ToString ()
		{
			return string.Format ("[SecurityManager]\n{0}", token.ToString());
		}
	}
}

