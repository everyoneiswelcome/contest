﻿using System;
using System.Security.Cryptography;
using GenericName.Common;
using System.Runtime.Serialization;

namespace GenericName
{
	/// <summary>
	/// Initial proposed structure for card data.  As long as the private key used for the signature
	/// stays specifically with the bank (not part of the auth file) then the entirety of the data
	/// should not be forgeable.  
	/// </summary>
	[DataContract]
	public class CardData
	{
		//Had to remove readonly so that I can deserialize a card object into this object type.
		private byte[] _hash;
		private byte[] _sig;

		public CardData(byte[] accountHash, byte[] signature)
		{
			/*
			 * Replace with cryptographic hash of account name.
			 */
			_hash = accountHash;

			/*
			 * Replace with digital signature of hash + private key. (Maybe)
			 */
			_sig = signature;
		}

		/// <summary>
		/// Gets the card number, which is just a hash of the account name.
		/// </summary>
		/// <value>The card number.</value>
		[DataMember]
		public byte[] CardNumber 
		{
			get { return _hash; } 
			set { _hash = value; } 
		}

		/// <summary>
		/// Gets the signature.  Which is a signature of the hash with the bank's private key.
		/// </summary>
		/// <value>The signature.</value>
		[DataMember]
		public byte[] Signature { 
			get { return _sig; } 
			set { _sig = value; } 
		}

		public override string ToString ()
		{
			return string.Format ("[CardData:\n\tCardNumber={0}\n\tSignature={1}\n]", BitConverter.ToString(CardNumber), BitConverter.ToString(Signature));
		}
	}
}

