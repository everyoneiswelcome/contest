﻿using System;

namespace GenericName
{
	public enum TransactionType : byte
	{
		GetBalance = 0,
		GenerateAccount = 1,
		Deposit = 2,
		Withdrawal = 3,
	}
}

