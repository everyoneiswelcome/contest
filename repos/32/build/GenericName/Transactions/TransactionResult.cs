﻿using System;

namespace GenericName.Transactions
{	
	public struct TransactionResult
	{
		private readonly bool _isValid;
		private readonly Transaction _transaction;

		public TransactionResult (bool isValid, Transaction transaction)
		{
			_isValid = isValid;
			_transaction = transaction;
		}

		public bool IsValid { get { return _isValid; } }

		public Transaction Transaction { get { return _transaction; } }

		public static TransactionResult False()
		{
			return new TransactionResult (false, null); 
		}

		public static TransactionResult True(Transaction transaction)
		{
			return new TransactionResult (true, transaction);
		}
	}
}

