﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using GenericName.Common;

namespace GenericName.Transactions
{
	/// <summary>
	/// A container class meant to be instantiated to keep track of account state.  Transactions are controlled
	/// via the OpenTransaction(), CommitTransaction(), RollbackTransaction() api.  
	/// </summary>
	public sealed class AccountContainer : IDisposable
	{
		private readonly ReaderWriterLockSlim _lock;
		private readonly Dictionary<string, Account> _accounts;
		private readonly Dictionary<string, Transaction> _transactions;

		public AccountContainer ()
		{
			_lock = new ReaderWriterLockSlim ();
			_accounts = new Dictionary<string, Account> ();
			_transactions = new Dictionary<string, Transaction> ();
		}

		/// <summary>
		/// Opens a transaction for the specific account name.  This transaction
		/// will not take place until a CommitTransaction() is run.
		/// </summary>
		/// <returns><c>true</c>, if transaction was opened, <c>false</c> otherwise.</returns>
		/// <param name="type">Type of transaction</param>
		/// <param name="accountName">The account name to operate upon.  This account will lock until complete.</param>
		/// <param name="cardData">Card data.</param>
		/// <param name="amount">Amount.</param>
		public TransactionResult OpenTransaction(
			TransactionType type, 
			string accountName, 
			CardData cardData, 
			Currency amount)
		{
			_lock.EnterReadLock ();
			try {
				
				if (_transactions.ContainsKey (accountName))
					return TransactionResult.False();

				if (type == TransactionType.GenerateAccount) {
					return HandleGenerateAccountOpen (type, accountName, amount);
				} else 
				{
					/*
					 * Validate card data here.  
					 * Then validate the transaction parameters.
					 * Then add an object to the transaction store.
					 */

					if(!SecurityManager.Current.VerifyCard(accountName, cardData))
						return TransactionResult.False();

					/*
					 * You can't withdraw more than you have in the account.
					 */
					if(type == TransactionType.Withdrawal && amount > _accounts[accountName].Balance)
						return TransactionResult.False();

					/*
					 * You can't withdraw or deposit zero dollars in the account.
					 */
					if((type == TransactionType.Withdrawal || type == TransactionType.Deposit)
						&& amount == Currency.Zero)
						return TransactionResult.False();

					var tranAmount = Currency.Zero;

					if(type == TransactionType.GetBalance)
						tranAmount = _accounts[accountName].Balance;
					else
						tranAmount = amount;

					var transaction = new Transaction(type, _accounts[accountName], tranAmount);
					_transactions[accountName] = transaction;
					return TransactionResult.True(transaction);
				}
					
			} finally {
				_lock.ExitReadLock ();
			}
		}
			

		/// <summary>
		/// Commits the transaction for the current username.
		/// </summary>
		/// <returns>The transaction that was committed so that it can be printed to the console.</returns>
		/// <param name="accountName">Account name to check for/commit a pending transaction.</param>
		public Transaction CommitTransaction(string accountName)
		{
			Transaction transaction = null;
			_lock.EnterUpgradeableReadLock ();

			try {
				
				if (!_transactions.ContainsKey (accountName))
					throw new InvalidOperationException ("This account has no pending transaction.");

				transaction = _transactions [accountName];

				if (transaction.TransactionType == TransactionType.GetBalance) {
					_transactions.Remove(accountName);
					return transaction;
				} else {
					/*
					 * All other transaction types will make a write to memory controlled by the dictionaries, so
					 * the lock gets upgraded to a write lock.  
					 */
					_lock.EnterWriteLock ();

					try {
						switch (transaction.TransactionType) {
						case TransactionType.GenerateAccount:
							_accounts [accountName] = transaction.Account;
							break;
						case TransactionType.Deposit:
							_accounts [accountName].Deposit (transaction.Amount);
							break;
						case TransactionType.Withdrawal:
							_accounts [accountName].Withdraw (transaction.Amount);
							break;
						}

						_transactions.Remove(accountName);
					} finally {
						_lock.ExitWriteLock ();
					}
				}

			} finally {
				_lock.ExitUpgradeableReadLock ();
			}

			return transaction;
		}

		/// <summary>
		/// Rollback any current transaction on accountName.  This will prevent any pending transaction on the account from being committed.
		/// </summary>
		/// <param name="accountName">Account name for which to rollback any pending transaction.</param>
		public void RollbackTransaction(string accountName)
		{
			_lock.EnterWriteLock ();

			if (_transactions.ContainsKey (accountName))
				_transactions.Remove (accountName);

			_lock.ExitWriteLock ();
		}

		private TransactionResult HandleGenerateAccountOpen (TransactionType type, string accountName, Currency amount)
		{
			if (_accounts.ContainsKey (accountName))
				return TransactionResult.False ();

			if (amount < 10M)
				return TransactionResult.False ();

			var transaction = new Transaction (type, Account.Create (accountName, amount), amount);
			_transactions [accountName] = transaction;
			return TransactionResult.True (transaction);
		}

		#region IDisposable implementation
		public void Dispose ()
		{
			if (_lock != null)
				_lock.Dispose ();
		}
		#endregion
	}
}

