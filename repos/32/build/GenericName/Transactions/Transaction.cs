﻿using System;

namespace GenericName.Transactions
{
	public class Transaction
	{
		private readonly Account _account;
		private readonly TransactionType _type;
		private readonly Currency _amount;

		internal Transaction (TransactionType type, Account account, Currency amount)
		{
			_type = type;
			_account = account;
			_amount = amount;
		}

		public string AccountName { get { return _account.AccountName; } }

		public TransactionType TransactionType { get { return _type; } }

		public Currency Amount { get { return _amount; } }

		public Account Account { get { return _account; } }

		public override string ToString ()
		{
			//var amt = this.TransactionType == TransactionType.GetBalance ? this.Account.Balance : this.Amount;
			var str = GetTransactionOutput (this.TransactionType, this.Amount, this.AccountName);

			#if DEBUG
			AppUtility.WriteLog(str);
			#endif

			return str;
		}

		private static string GetTransactionName(TransactionType type)
		{
			string name = null;
			switch (type) {
			case TransactionType.Deposit:
				name = "deposit";
				break;
			case TransactionType.GetBalance:
				name = "balance";
				break;
			case TransactionType.GenerateAccount:
				name = "initial_balance";
				break;
			case TransactionType.Withdrawal:
				name = "withdraw";
				break;
			}

			return name;
		}

		public static string GetTransactionOutput (TransactionType type, Currency amount, string accountName)
		{
			string str = String.Format ("{{ \"account\":\"{2}\", \"{0}\":{1} }}", GetTransactionName (type), amount, accountName);
			return str;
		}
	}
}
