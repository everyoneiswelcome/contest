﻿using System;
using System.Numerics;
using GenericName.Common;

namespace GenericName.Transactions
{
	public class Account
	{
		private string _accountName; 
		private Currency _balance;
		private CardData _cardData;

		internal Account() 
		{
		}

		public string AccountName { get { return _accountName; } private set { _accountName = value; } }

		public Currency Balance { get { return _balance; } private set { _balance = value; } }

		public void Deposit(Currency amount)
		{
			_balance += amount;
		}

		public void Withdraw(Currency amount)
		{
			_balance -= amount;
		}

		public CardData Card { get { return _cardData; } } 

		public static Account Create(string accountName, Currency startingBalance)
		{
			var account = new Account () {
				_accountName = accountName,
				_balance = startingBalance,
				_cardData = SecurityManager.Current.GenerateCardData(accountName)
			};

			return account;
		}
	}
}

