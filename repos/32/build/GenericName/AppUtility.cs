﻿using System;
using System.Text.RegularExpressions;
using System.Net;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;

namespace GenericName
{
	public static class AppUtility
	{
		public const int MAX_ARG_LENGTH = 4096;
		public const string DEFAULT_AUTH_FILE = "bank.auth";
		public const int DEFAULT_PORT_NUMBER = 3000;
		public const int MIN_PORT = 1024;
		public const int MAX_PORT = 65535;
		public const string DEFAULT_IP = "127.0.0.1";


		public const decimal MAX_TRANSACTION_CURRENCY = 4294967295.99M;
		public const decimal MIN_TRANSACTION_CURRENCY = 0.00M;
		public const decimal MIN_DEPOSIT_CURRENCY = 10.00M;

		/// <summary>
		/// Print a message to StdOut and flushes output stream.
		/// </summary>
		/// <param name="message">Message to write</param>
		public static void PrintLine(string message)
		{
			Console.Out.WriteLine(String.Format ("{0}", message));
			Console.Out.Flush ();
		}

		/// <summary>
		/// Asserts the length of the arguments array are less than or equal to MAX_ARG_LENGTH
		/// </summary>
		/// <param name="args">Argument string array</param>
		public static void AssertArgumentLength(string[] args)
		{
			foreach (var a in args) {
				if (a.Length > MAX_ARG_LENGTH) {
					AppUtility.WriteError ("Argument too long.");
					AppUtility.ExitCommandLineError ();
				}
			}
		}

		public static string GetArgOrExit(string[] args, int index)
		{
			//I understand that there is an initial check on the start of main for argument length, just figured it doesnt hurt to keep checking it... anyway very optional
			if (index >= args.Length ||
				args[index].Length > MAX_ARG_LENGTH) {
				WriteError ("Invalid argument...");
				ExitCommandLineError ();
			}
				
			return args [index];
		}

		public static Currency GetCurrencyOrExit(string[] args, int index)
		{
			string s = GetArgOrExit (args, index);

			Currency curr;
			/***
			 * REQUIREMENTS
			 * Numeric inputs are positive and provided in decimal without any leading 0's (should match /(0|[1-9][0-9]*)/). 
			 * Thus "42" is a valid input number but the octal "052" or hexadecimal "0x2a" are not. 
			 * Any reference to **number** below refers to this input specification.
			 * 
			 * Balances and currency amounts are specified as a **number** indicating a whole amount and a fractional input separated by a period. 
			 * The fractional input is in decimal and is always two digits and thus can include a leading 0 (should match /[0-9]{2}/).
			 * The interpretation of the fractional amount v is that of having value equal to v/100 of a whole amount (akin to cents and dollars in US currency). 
			 * Balances are bounded from 0.00 to 4294967295.99.
			 * */
			//Narrowing the case where amounts start with 0

			// [Casey] This is handled in Currency.TryParse.  Also, StartsWith("s") rejects 0.01, which is a valid deposit/withdraw.
			//if(s.StartsWith("0"))
			//	AppUtility.ExitCommandLineError ();
			
			if (!Currency.TryParse (s, out  curr))
				AppUtility.ExitCommandLineError ();

			return curr;
		}

		public static string GetUnspacedArg(string[] args, int index)
		{
			string a = args [index];

			if (a.Length <= 2)
				AppUtility.ExitCommandLineError ();

			return a.Substring (2, a.Length - 3);
		}

		/// <summary>
		/// Exits the error code 0;
		/// </summary>
		public static void ExitSuccess()
		{
			AppUtility.ExitOrThrow (0, "Everything is awesome!");
		}

		/// <summary>
		/// Exit with error code 255
		/// </summary>
		public static void ExitCommandLineError()
		{
			AppUtility.ExitOrThrow (255, "Argument Error");
		}

		public static void ExitServerAuthFileError()
		{
			AppUtility.ExitOrThrow (255, "Server auth file error");
		}
	
		public static void ExitAtmError()
		{
			AppUtility.ExitOrThrow (63, "Protocol Error");
		}

		public static void ExitAtmAccountError()
		{
			AppUtility.ExitOrThrow (255, "Account Error");
		}

		public static void ExitOrThrow(int exitCode, string reason = null)
		{
			#if DEBUG
			AppUtility.WriteLog ("Exiting with status '{0}'.  Reason: {1}", exitCode, reason ?? "None Given");
			//NOTE: [Eduardo] Learned this the hard way, for testing an exception must be thrown, otherwise test aborts before reporting results.
			throw new RequirementViolationException (exitCode, reason ?? "No Reason Given");
			#endif
			Environment.Exit (exitCode);
		}

		/// <summary>
		/// Writes a string to StdError
		/// </summary>
		/// <param name="error">The string to write.</param>
		public static void WriteError(string error)
		{
			Console.Error.WriteLine (error);
		}

		/// <summary>
		/// Writes a formatted string to StdError
		/// </summary>
		/// <param name="error">The string to write.</param>
		/// <param name="args">The formatting arguments.</param>
		public static void WriteError(string error, params object[] args)
		{
			Console.Error.WriteLine (error, args);
		}

		/// <summary>
		/// Writes an exception to StdError
		/// </summary>
		/// <param name="e">The exception to write.</param>
		public static void WriteError(Exception e)
		{
			Exception ex = e;
			while (ex != null) {
				Console.Error.WriteLine ("Exception: {0}{1}{2}{1}{3}", 
					ex.GetType ().Name,
					Environment.NewLine,
					ex.Message,
					ex.StackTrace);

				if (ex.InnerException != null) {
					Console.Error.WriteLine ("BEGIN INNER EXCEPTION FOR {0}:", ex.GetType ().Name);
				}

				ex = ex.InnerException;
			}
		}

		/// <summary>
		/// Asserts the filename is valid according to spec.
		/// </summary>
		/// <param name="fileName">File name.</param>
		public static void AssertValidFilename(string fileName)
		{
			/***
			 * File names are restricted to underscores, hyphens, dots, digits, and lowercase alphabetical characters (each character should match /[_\-\.0-9a-z]/). 
			 * File names are to be between 1 and 255 characters long. 
			 * The special file names "." and ".." are not allowed.
			 */
			if (String.IsNullOrWhiteSpace (fileName)
			    || fileName.Equals (".")
			    || fileName.Equals ("..")
			    || fileName.Length > 255
			    || fileName.Length <= 0)
				AppUtility.ExitCommandLineError ();

			if (!AppUtility.IsValidAlphaNumericStringV2 (fileName))
				AppUtility.ExitCommandLineError ();
		}

		public static bool IsValidTransactionRange(TransactionType type, Currency curr)
		{
			switch (type) {
			case TransactionType.GetBalance:
				return true;
			case TransactionType.GenerateAccount:
				return curr <= MAX_TRANSACTION_CURRENCY && curr >= MIN_DEPOSIT_CURRENCY;
			default:
				return curr <= MAX_TRANSACTION_CURRENCY && curr > MIN_TRANSACTION_CURRENCY;
			}
		}

		public static Regex _alpaNumRx = new Regex(@"^[-_.0-9a-z]+$", RegexOptions.Singleline|RegexOptions.ExplicitCapture);

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsValidAlphaNumericString(string str)
		{
			if (String.IsNullOrEmpty (str))
				return false;
			
			return _alpaNumRx.IsMatch (str);
		}
			
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool IsValidAlphaNumericStringV2(string str)
		{
			if (String.IsNullOrEmpty (str))
				return false;

			for (int i = 0; i < str.Length; i++) {
				char c = str [i];
				if (!Char.IsNumber (c)
				   && !Char.IsLower (c)
				   && c != '-'
				   && c != '_'
				   && c != '.')
					return false;
			}

			return true;
		}

		private static Regex _ipRx = new Regex (@"^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$", RegexOptions.Singleline|RegexOptions.ExplicitCapture);

		public static System.Net.IPAddress AssertValidIpAddress(string ipAddress)
		{
			IPAddress addr = null;

			if (ipAddress == null || ipAddress.Equals (DEFAULT_IP))
				return IPAddress.Loopback;

			if (!_ipRx.IsMatch (ipAddress))
				AppUtility.ExitCommandLineError ();

			if (!System.Net.IPAddress.TryParse (ipAddress, out addr))
				AppUtility.ExitCommandLineError ();

			return addr;
		}

		public static void AssertValidPortNumber(int portNum)
		{
			if (portNum < MIN_PORT || portNum > MAX_PORT)
				AppUtility.ExitCommandLineError ();
		}

		[Conditional("DEBUG")]
		public static void WriteLog(string log)
		{
			WriteError (log);
		}

		[Conditional("DEBUG")]
		public static void WriteLog(string log, params object[] args)
		{
			WriteError (log, args);
		}
	}
}

