﻿using System;
using System.Runtime.Serialization;
using GenericName.Transactions;
using System.Net;

namespace GenericName
{
	[DataContract(Name = "CP", Namespace = "http://builditbreakit.org")]
	public class CommunicationPacket
	{
		protected CommunicationPacket() { }

		public CommunicationPacket (PacketTypeEnum packetType)
		{
			PacketType = packetType;
		}

		[DataMember(Name = "PT", EmitDefaultValue = true)]
		public PacketTypeEnum PacketType;

		[DataMember(Name = "EN", EmitDefaultValue = false)]
		public byte[] EncodedNonce { get; set; }

		/*
		 * REFACTOR-ALERT: Experimenting with removing the "hello"
		 * and session identifiers.
		 */
		//[DataMember(Name = "SE", EmitDefaultValue = false)]
		//public Guid Session { get; set; }

		[DataMember(Name = "AC", EmitDefaultValue = false)]
		public CardData AtmCard { get; set; }

		[DataMember(Name = "AN", EmitDefaultValue = false)]
		public string AccountName { get; set; }

		[DataMember(Name = "TT", EmitDefaultValue = false)]
		public TransactionType TransactionType { get; set; }

		[DataMember(Name = "TA", EmitDefaultValue = false)]
		public Currency TransactionAmount { get; set; }
	}

	[DataContract(Name = "PTE", Namespace = "http://builditbreakit.org")]
	public enum PacketTypeEnum : byte
	{
		[EnumMember(Value = "H")]
		Hello = 1,

		[EnumMember(Value = "S")]
		StartTransact = 2,

		[EnumMember(Value = "C")]
		ConfirmTransact = 3,

		[EnumMember(Value = "O")]
		OK = 5,

		[EnumMember(Value = "SA")]
		ServerAbort = 100,

		[EnumMember(Value = "CA")]
		ClientAbort = 200,

		[EnumMember(Value = "CO")]
		ProtocolCorrupt = 255,
	}
}

