﻿using System;
using System.Net.Sockets;
using GenericName.Common;
using System.IO;
using System.Linq;
using System.Threading;
using System.ServiceModel.Channels;
using System.Security.Cryptography;

namespace GenericName
{
	public class MessageHandler : IDisposable
	{
		private TcpClient _client;
		private Stream _stream;
		private const int TIMEOUT_MS = 10000;
		private readonly SecurityManager _securityManager;
		private readonly bool _isServer;
		private byte[] _nonce;
		//private Guid? _session;
		private TimeSpan _timeout;
		private readonly BufferManager _bufferManager;

		public MessageHandler (TcpClient client, SecurityManager securityManager, BufferManager bufferManager, bool isServer)
		{
			_client = client;
			_client.SendBufferSize = 2048;
			_client.ReceiveBufferSize = 2048;
			//_stream = _client.GetStream ();

			var netStream = _client.GetStream();
			#if !DEBUG
			netStream.ReadTimeout = TIMEOUT_MS;
			netStream.WriteTimeout = TIMEOUT_MS;
			#else
			netStream.ReadTimeout = System.Threading.Timeout.Infinite;
			netStream.WriteTimeout = System.Threading.Timeout.Infinite;
			#endif
			_stream = netStream;
			_securityManager = securityManager;
			_isServer = isServer;
			_timeout = TimeSpan.FromMilliseconds (TIMEOUT_MS);
			_bufferManager = bufferManager;

			/*
			 * Assign a session.  This just has to remain
			 * consistent for all information received on 
			 * this socket.  This would prevent an attacker
			 * who can hijack the socket connection from
			 * pre-empting the traffic.
			 */
			/*
			* REFACTOR-ALERT: Experimenting with removing the "hello"
			* and session identifiers.
			*/
			if (_isServer) {
				_nonce = new byte[0];
				//_session = Guid.NewGuid ();
			} else {
				_nonce = new byte[0];
			}
		}

		public void SendAbort()
		{
			PacketTypeEnum packetType = _isServer ? PacketTypeEnum.ServerAbort : PacketTypeEnum.ClientAbort;
			CommunicationPacket packet = new CommunicationPacket (packetType);
			SendMessage (packet);
		}

		public void SendOK()
		{
			CommunicationPacket packet = new CommunicationPacket (PacketTypeEnum.OK);
			SendMessage (packet);
		}

		public void SendProtocolCorrupt()
		{
			CommunicationPacket packet = new CommunicationPacket (PacketTypeEnum.ProtocolCorrupt);
			SendMessage (packet);
		}

		public void SendMessage(CommunicationPacket packet)
		{
			AppUtility.WriteLog ("Sending {0} Message.", packet.PacketType);
			if (_isServer) {
				packet.EncodedNonce = CryptoUtils.GetRandom (8);
				_nonce = packet.EncodedNonce;
			} else {
				packet.EncodedNonce = _nonce;
			}
			/*
			 * REFACTOR-ALERT: Experimenting with removing the "hello"
			 * and session identifiers.
			 */
			//packet.Session = _session.Value;
			byte[] protectedBytes;
			byte[] dataLength;

			try{
				ProtectedCommunicationPacket protectedPacket = new ProtectedCommunicationPacket (_securityManager, packet);
				protectedBytes = Utilities.Serialize (protectedPacket);
				dataLength = BitConverter.GetBytes (protectedBytes.Length);
				AppUtility.WriteLog ("Sending packet of length: {0}", protectedBytes.Length);
			}catch(CryptographicException ce){
				AppUtility.WriteError(ce.Message);
				throw new CommunicationCorruptedException ();
			}catch(CommunicationCorruptedException e) {
				throw e;
			}catch(Exception) {
				throw new CommunicationCorruptedException ();
			}
			try {
				_stream.Write(dataLength.Concat(protectedBytes).ToArray(), 0, dataLength.Length + protectedBytes.Length);
				//_stream.Write (dataLength, 0, dataLength.Length);
				//_stream.Write (protectedBytes, 0, protectedBytes.Length);
				_stream.Flush();
			} catch (IOException) {
				throw new GenericName.TimeoutException ();
			} catch (Exception) {
				throw new CommunicationCorruptedException ();
			}
		}

		public int ReadBlock(byte[] bytes, int length)
		{
			DateTime start = DateTime.Now;

			int i = 0;
			while(DateTime.Now - start < _timeout) {

				i += _stream.Read (bytes, i, length - i);
				if (i >= length)
					return i;
				Thread.Sleep (1);
			}

			throw new GenericName.TimeoutException ();
		}

		public CommunicationPacket ReceiveMessage()
		{
			byte[] dataLength = null;
			byte[] dataBytes = null;
			int intLength = 0;
			try {
				
				dataLength = new byte[sizeof(int)];
				//_stream.Read (dataLength, 0, dataLength.Length);
				ReadBlock(dataLength, dataLength.Length);
				intLength = BitConverter.ToInt32(dataLength, 0);

				if(intLength > Utilities.MAX_BUFFER_BLOCK || intLength < 0)
					throw new ProtocolException();

				//dataBytes = new byte[intLength];
				dataBytes = _bufferManager.TakeBuffer(Utilities.MAX_BUFFER_BLOCK);

				ReadBlock(dataBytes, intLength);
				//_stream.Read (dataBytes, 0, intLength);

			} catch (IOException) {
                if (null != dataBytes) { 
                    //FIXED: Using a corrupted packet would kill remote server. In turn, dataBytes would be null and the below instruction would cause a NullReference exception
				    _bufferManager.ReturnBuffer (dataBytes);
                }
				AppUtility.WriteError ("Error receiving message.");
				throw new ProtocolException ();
			} 

			ProtectedCommunicationPacket protectedPacket = Utilities.Deserialize<ProtectedCommunicationPacket> (dataBytes, intLength);
			_bufferManager.ReturnBuffer (dataBytes);
			//dataBytes = null;

			if (!_securityManager.VerifySignature (protectedPacket.EncryptedCommunicationPacket, protectedPacket.PacketSignature))
				throw new ProtocolException ();
			byte[] packetBytes = null;
            try{
			    packetBytes = _securityManager.RevealData (protectedPacket.EncryptedCommunicationPacket);
				CommunicationPacket packet = Utilities.Deserialize<CommunicationPacket> (packetBytes);

				AppUtility.WriteLog ("Receiving {0} Message.", packet.PacketType);

				if (packet.PacketType == PacketTypeEnum.ProtocolCorrupt)
					throw new CommunicationCorruptedException ();

				/*
				 * REFACTOR-ALERT: Experimenting with removing the "hello"
				 * and session identifiers.
				 */
				if (_isServer) {
					if (packet.EncodedNonce == null
						|| !packet.EncodedNonce.SequenceEqual (_nonce)) {
						//|| packet.Session != _session) {
						throw new ProtocolException ();
					}
				} else {
					_nonce = packet.EncodedNonce;
					//_session = packet.Session;
				}

				return packet;
            }catch(CryptographicException ce){
                AppUtility.WriteError(ce.Message);
				throw new ProtocolException ();
			}
		}

		public bool IsClosed { get { return !_client.Connected; } }

		//public Guid? SessionKey { get { return _session; } }

		#region IDisposable implementation

		public void Dispose ()
		{
			Dispose (true);
			GC.SuppressFinalize (this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing) {
				if (_client != null) {
					_client.Close ();
					_client = null;
				}

				if (_stream != null)
					_stream = null;

				_nonce = null;
				//_session = null;
			}
		}
		#endregion

		public override string ToString ()
		{
			return string.Format ("[MessageHandler: IsClosed={0}]", IsClosed );//SessionKey);
		}
	}
}

