﻿using System;
using System.Runtime.Serialization;
using GenericName.Common;
using System.IO;

namespace GenericName
{
	[DataContract(Name = "ECP", Namespace = "http://builditbreakit.org")]
	public class ProtectedCommunicationPacket
	{
		[DataMember(Name = "DATA")]
		public byte [] EncryptedCommunicationPacket;

		[DataMember(Name = "SIGN")]
		public byte [] PacketSignature;

		public ProtectedCommunicationPacket(){
		}

		public ProtectedCommunicationPacket (byte[] encryptedPacket, byte[] signature)
		{
			this.EncryptedCommunicationPacket = encryptedPacket;
			this.PacketSignature = signature;
		}

		public ProtectedCommunicationPacket (SecurityManager man, CommunicationPacket packet)
		{
			byte[] packetBytes = Utilities.Serialize<CommunicationPacket> (packet);
			//this.EncryptedCommunicationPacket = man.ProtectData (Utilities.Serialize<CommunicationPacket>(packet));
			//this.PacketSignature = man.SignData(Utilities.Serialize<CommunicationPacket>(packet));
			this.EncryptedCommunicationPacket = man.ProtectData (packetBytes);
			this.PacketSignature = man.SignData (this.EncryptedCommunicationPacket);
		}
	}
}

