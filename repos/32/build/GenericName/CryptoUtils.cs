﻿using System;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Runtime.Serialization;
using System.Linq;

namespace GenericName.Common
{
	public class CryptoUtils
	{
		/// <summary>
		/// Size of encryption keys in bytes.
		/// </summary>
		public static int ENCRYPT_KEY_SIZE_BYTES = 32;

		/// <summary>
		/// Size of signing keys in bytes
		/// </summary>
		public static int SIGN_KEY_SIZE_BYTES = 32;

		/// <summary>
		/// Size of init vector in bytes
		/// </summary>
		public static int IV_SIZE_BYTES = 16;

		/// <summary>
		/// The maximum allowable data object size in elements.
		/// </summary>
		public static int MAX_DATA_SIZE = 4096;

		/// <summary>
		/// The nonce size in bytes.
		/// </summary>
		public static int NONCE_SIZE = 128;

		/// <summary>
		/// Encrypt the specified plaintext.
		/// </summary>
		/// <param name="encryptor">Symmetric algorithm transform used for operation</param>
		/// <param name="plaintext">Plaintext bytes to be encrypted.</param>
		public static byte[] Encrypt(ICryptoTransform encryptor, byte[] plaintext)
		{
			if (null == plaintext || plaintext.Length < 1) {
				throw new ArgumentNullException ("plaintext", "Parameter must be non-null and non-empty.");
			} else if(null == encryptor){
				throw new ArgumentNullException ("encryptor", "ICryptoTransform must be non-null and non-empty.");
			} else if (plaintext.LongLength >= Int32.MaxValue){
				throw new IndexOutOfRangeException ("Length exceeds Int32.Max.");
			} else {
				byte[] ciphertext = new byte[plaintext.Length];
				using (MemoryStream ms = new MemoryStream ()) {
					using (CryptoStream cs = new CryptoStream (ms, encryptor, CryptoStreamMode.Write)) {
						cs.Write (plaintext, 0, plaintext.Length);
						cs.FlushFinalBlock ();
						ciphertext = ms.ToArray ();
						cs.Close ();
					}
					ms.Close ();
				}
				return ciphertext;
			}
		}

		/// <summary>
		/// Decrypt the specified ciphertext.
		/// </summary>
		/// <param name="decryptor">Symmetric algorithm transform used for operation</param>
		/// <param name="ciphertext">Ciphertext to be decrypted.</param>
		public static byte [] Decrypt(ICryptoTransform decryptor, byte[] ciphertext)
		{
			if (null == ciphertext || ciphertext.Length < 1) {
				throw new ArgumentNullException ("ciphertext", "Parameter must be non-null and non-empty.");
			} else if (ciphertext.LongLength >= Int32.MaxValue) {
				throw new IndexOutOfRangeException ("Length exceeds Int32.Max.");
			}else if(null == decryptor){
				throw new ArgumentNullException ("decryptor", "ICryptoTransform must be non-null and non-empty.");
			} else {
				byte[] result = new byte[ciphertext.Length];
				byte[] plaintext = null;
				using (MemoryStream ms = new MemoryStream (ciphertext)) {
					using (CryptoStream cs = new CryptoStream (ms, decryptor, CryptoStreamMode.Read)) {
						int readBytes = cs.Read (result, 0, ciphertext.Length);
						cs.Close ();
						plaintext = new byte[readBytes];
						Array.Copy (result, plaintext, readBytes);
					}
					ms.Close ();
				}
				return plaintext;
			}
		}

		/// <summary>
		/// Gets a random byte array using the RNG crypto provider.
		/// </summary>
		/// <returns>The random.</returns>
		/// <param name="length">Length of bytes to return from randome. Range 1 to int32.max</param>
		public static byte[] GetRandom (int length)
		{
			if (length > 0 && length < Int32.MaxValue) {
				byte[] rand = new byte[length];
				using (var provRng = new RNGCryptoServiceProvider()) {
				//using (var provRng = RandomNumberGenerator.Create ()) {
					provRng.GetBytes (rand);
				}
				AppUtility.WriteLog (String.Format ("RNG Size[{0}]: {1}", length, BitConverter.ToString (rand)));
				return rand;
			} else {
				throw new ArgumentOutOfRangeException ("length", "Argument value exceeds allowed values: from 1 to Int32.Max.");
			}
		}

		/// <summary>
		/// Gets an instance of the HMAC-SHA256 signer with a random 256-bit key.
		/// </summary>
		/// <returns>The hmac signer implementation.</returns>
		public static HMAC GetHmacSigner()
		{
			return GetHmacSigner (GetRandom (32));
		}

		/// <summary>
		/// Gets an instance of the HMAC-SHA256 signer with a random key.
		/// </summary>
		/// <returns>The hmac signer implementation.</returns>
		public static HMAC GetHmacSigner(byte [] key)
		{
			if (key == null)
				throw new NullReferenceException ("Key is null!");

			//HMAC provHmac = new HMACSHA512 (key);
			HMACSHA256 provHmac = new HMACSHA256 (key);
			//HMACSHA256 provHmac = (HMACSHA256)HMACSHA256.Create ("HMACSHA256");
			if (key.Length <= 32 && key.LongLength > 4096) {
				//Key for HMAC SHA256 is 32 bytes on most cases, can be longer, but it doesnt matter after that since key=HASH(key) when key > block size (256 bits) -  
				//4k is exaggerated and this upper limit
				throw new ArgumentException ("Key is too small or too large (32 to 4096 bytes accepted", "key");
			}
			/*
			 * According to mono source,
			 * the key property is set by using the parameterized 
			 * constructor above ^^^.  This avoids a machine.config
			 * load and should give some extra performance
			 */
			//provHmac.Key = key;
			provHmac.Initialize ();
			return provHmac;
		}


		/// <summary>
		/// Get an AES-256/CBC Provider with provided keys, IV and padding.
		/// </summary>
		/// <returns>The SymmetricAlgorithm provider instance.</returns>
		public static SymmetricAlgorithm GetAesProvider(byte[] key, byte[] iv, PaddingMode paddingMode)
		{
			SymmetricAlgorithm aesCrypt = new AesCryptoServiceProvider ();
			//SymmetricAlgorithm aesCrypt = Aes.Create ();
			aesCrypt.BlockSize = 128;
			aesCrypt.Padding = paddingMode;
			aesCrypt.KeySize = 256;
			aesCrypt.Mode = CipherMode.CBC;
			aesCrypt.Key = key;
			aesCrypt.IV = iv;
			return aesCrypt;
		}

		/// <summary>
		/// Get an AES-256/CBC Provider with provided keys, IV and ISO-10126 padding.
		/// </summary>
		/// <returns>The SymmetricAlgorithm provider instance.</returns>
		public static SymmetricAlgorithm GetAesProvider(byte[] key, byte[] iv)
		{
			return GetAesProvider (key, iv, PaddingMode.PKCS7);
			//return GetAesProvider (key, iv, PaddingMode.ISO10126);
		}

		/// <summary>
		/// Generates a signature using the corresponding key and source data. Uses HMAC-SHA256.
		/// </summary>
		/// <returns>The HMAC-SHA256 result of the operation HMAC(SHA256(data),key)</returns>
		/// <param name="data">Data to be signed</param>
		/// <param name="key">Key to be used</param>
		public static byte [] SignData(byte [] data, byte [] key)
		{
			if (!Utilities.isValidData(data) ||
				!Utilities.isValidData(key)
			) {
				throw new InvalidOperationException ("SignData operation cannot be evaluated with provided parameters."); //Purposely obscure.
			}
			KeyedHashAlgorithm signer = CryptoUtils.GetHmacSigner(key);
			return signer.ComputeHash(data);
		}

		/// <summary>
		/// Apply a SHA-256 transform on provided input.
		/// </summary>
		/// <returns>The SHA-256 hash of input</returns>
		/// <param name="data">Data to be hashed</param>
		public static byte [] HashData (string data)
		{
			return HashData (Encoding.UTF8.GetBytes (data));
		}

		/// <summary>
		/// Apply a SHA-256 transform on provided input.
		/// </summary>
		/// <returns>The SHA-256 hash of input</returns>
		/// <param name="data">Data to be hashed</param>
		public static byte [] HashData(byte [] data)
		{
			if (!Utilities.isValidData(data)
			) {
				throw new InvalidOperationException ("SignData operation cannot be evaluated with provided parameters."); //Purposely obscure.
			}
			HashAlgorithm hashAlg = HashAlgorithm.Create("SHA-256");
			hashAlg.Initialize ();
			return hashAlg.ComputeHash(data);
		}

		/// <summary>
		/// Checks a signature is valid using the corresponding key, source data and expected outcome. Uses HMAC-SHA256.
		/// </summary>
		/// <returns><c>true</c>, if signature was checked, <c>false</c> otherwise.</returns>
		/// <param name="data">Source data</param>
		/// <param name="key">Key to be used to HMAC.</param>
		/// <param name="expectedSignature">Expected signature.</param>
		public static bool CheckSignature(byte [] data, byte [] key, byte [] expectedSignature)
		{
			if (!Utilities.isValidData(data) ||
				!Utilities.isValidData(key) ||
				!Utilities.isValidData(expectedSignature)
			) {
				throw new InvalidOperationException ("SignData operation cannot be evaluated with provided parameters."); //Purposely obscure.
			}

			byte[] calculatedSignature = SignData (data, key);
			return Enumerable.SequenceEqual(calculatedSignature,expectedSignature);
		}
	}
}

