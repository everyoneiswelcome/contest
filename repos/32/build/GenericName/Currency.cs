﻿using System;
using System.Numerics;
using System.Runtime.Serialization;

namespace GenericName 
{
	/// <summary>
	/// A structure representing a currency object.  Is comparable to itself and the <see cref="System.Decimal"/> type.
	/// </summary>
	[DataContract]
	public struct Currency : IEquatable<Currency>, IEquatable<decimal>
	{
		/*
		 * Long is appropriate for all amounts given our first reading of the spec.
		 * However, then I saw this forum thread:
		 * https://class.coursera.org/cybersecuritycapstone-002/forum/thread?thread_id=85
		 * If tests prove the idea that balances are unbounded to
		 * be the case, then we may have to switch
		 * to an object backed by BigInteger.  
		 */
		[DataMember]
		private long _amount; //FIXME: Maybe set to 0 anyway, just in case parameterless kicks in?

		public static Currency Zero { get { return new Currency (0); } }

		public static Currency MaxValue { get { return new Currency (Int64.MaxValue); } }

		public static Currency MinValue { get { return new Currency (Int64.MinValue); } }

		// NOTE: This compiles on MONO but is not part of the .NET spec.  Bad.  Bad Casey
		//public Currency()
		//{
		//	_amount = 0;
		//}

		/// <summary>
		/// Use this to instantiate from a long value.  The long represents the total number of cents in the value.
		/// </summary>
		/// <param name="amount"><see cref="System.Int64"/> representing total number of cents in the value.</param>
		public Currency(long amount)
		{
			_amount = amount;
		}

		public Currency(decimal amount)
		{
			_amount = (long)(amount * 100);
		}

		public Currency(long dollars, byte cents)
		{
			_amount = dollars * 100 + cents;
		}

		public long TotalCents { get { return _amount; } }

		public decimal Total { get { return ((decimal)_amount) / 100; } }

		public long Dollars { get { return _amount / 100; } }

		public byte Cents { get { return (byte)(_amount % 100); } }

		public override string ToString()
		{
			return String.Format ("{0}.{1:00}", this.Dollars, this.Cents);
		}

		/// <summary>
		/// Formats the currency object from a string.
		/// </summary>
		/// <param name="str">The string of the format "#.##" to parse.</param>
		public static Currency Parse(string str)
		{
			if (str == null)
				throw new ArgumentNullException ("str", "Argument cannot be null.");

			str = str.Trim ();

			if (str.Length < 4)
				throw new ArgumentException ("Invalid currency string.");

			if (str [0] == '0' && str [1] != '.')
				throw new ArgumentException ("Leading zeros are invalid.");

			int periodIndex = str.Length - 3;

			if (str [periodIndex] != '.')
				throw new ArgumentException ("Invalid currency string.");
			
			long dollars = long.Parse (str.Substring (0, periodIndex));
			byte cents = byte.Parse (str.Substring (periodIndex + 1, 2));

			return new Currency (dollars, cents);
		}

		public static bool TryParse(string str, out Currency curr)
		{
			curr = Currency.Zero;

			try {
				curr = Currency.Parse (str);
				return true;
			} catch (Exception) {
				return false;
			}
		}

		#region Operators on Currency
		public static Currency operator +(Currency c1, Currency c2)
		{
			return new Currency (c1.TotalCents + c2.TotalCents);
		}

		public static Currency operator -(Currency c1, Currency c2)
		{
			return new Currency (c1.TotalCents - c2.TotalCents);
		}

		public static bool operator == (Currency c1, Currency c2)
		{
			return c1._amount == c2._amount;
		}

		public static bool operator != (Currency c1, Currency c2)
		{
			return c1._amount != c2._amount;
		}

		public static bool operator > (Currency c1, Currency c2)
		{
			return c1._amount > c2._amount;
		}

		public static bool operator < (Currency c1, Currency c2)
		{
			return c1._amount < c2._amount;
		}

		public static bool operator >= (Currency c1, Currency c2)
		{
			return c1._amount >= c2._amount;
		}

		public static bool operator <= (Currency c1, Currency c2)
		{
			return c1._amount <= c2._amount;
		}
		#endregion

		#region Operators on Decimal
		public static Currency operator +(Currency c1, decimal c2)
		{
			return c1 + new Currency (c2);
		}

		public static Currency operator -(Currency c1, decimal c2)
		{
			return c1 - new Currency (c2);
		}

		public static bool operator == (Currency c1, decimal c2)
		{
			return c1 == new Currency (c2);
		}

		public static bool operator != (Currency c1, decimal c2)
		{
			return c1 != new Currency (c2);
		}

		public static bool operator > (Currency c1, decimal c2)
		{
			return c1 > new Currency (c2);
		}

		public static bool operator < (Currency c1, decimal c2)
		{
			return c1 < new Currency (c2);
		}

		public static bool operator >= (Currency c1, decimal c2)
		{
			return c1 >= new Currency (c2);
		}

		public static bool operator <= (Currency c1, decimal c2)
		{
			return c1 <= new Currency (c2);
		}
		#endregion

		public override int GetHashCode ()
		{
			return _amount.GetHashCode ();
		}

		public override bool Equals (object obj)
		{
			if (obj is Currency)
				return this.Equals ((Currency)obj);

			return object.Equals(this, obj);
		}
			
		#region IEquatable implementation
		public bool Equals (Currency other)
		{
			return this._amount == other._amount;
		}

		public bool Equals (decimal other)
		{
			return this == other;
		}
		#endregion
	}
}
