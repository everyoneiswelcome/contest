﻿using System;
using System.Runtime.Serialization;

namespace GenericName.Common
{
	//[DataContract]
	public class SecurityToken{

		public SecurityToken() {}

		/// <summary>
		/// Base64 encoded key used for encrypt/decrypt
		/// </summary>
		//[DataMember]
		public string EncodedEncryptDecryptKey;

		/// <summary>
		/// Base64 encoded key used for signing messages
		/// </summary>
		//[DataMember]
		public string EncodedSigningKey;

		/// <summary>
		/// Base64 encoded IV used for encrypting messages
		/// </summary>
		//[DataMember]
		public string EncodedIV;

		/// <summary>
		/// Base64 encoded signature of the authorization material by the security officer key.
		/// </summary>
		//[DataMember]
		public string EncodedServerSignature;

		public override string ToString ()
		{
			return string.Format ("[AuthenticationTokens] \n\t[EncDecKey: {0}]\n\t[EncSignKey: {1}]\n\t[EncSignature: {2}]", EncodedEncryptDecryptKey, EncodedSigningKey, EncodedServerSignature);
		}
	}
}

