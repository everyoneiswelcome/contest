package Security.Helpers;

import Main.Exceptions.FailedException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class RSA {
    
    private PublicKey publicKey;
    private PrivateKey privateKey;
    
    public RSA() { }
    
    public RSA(PrivateKey pvt) {
        privateKey = pvt;
    }
    
    public RSA(PublicKey pub) {
        publicKey = pub;
    }
    
    public RSA(PrivateKey pvt, PublicKey pub) {
        privateKey = pvt;
        publicKey = pub;
    }
    
    public void generateKeyPair() throws FailedException {
        KeyPairGenerator kpg;
        try {
            kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(1024);
            KeyPair kp = kpg.genKeyPair();
            publicKey = kp.getPublic();
            privateKey = kp.getPrivate();
        } catch (Exception ex) { 
            throw new FailedException();
        }
    }

    public void savePublicKey(File file) throws FailedException {
        try {
            ObjectOutputStream oout = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
            oout.writeObject(publicKey);
            oout.close();
        } catch (Exception e) {
            throw new FailedException();
        }
    }

    public void savePrivateKey(File file) throws FailedException {
        try {
            ObjectOutputStream oout = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
            oout.writeObject(privateKey);
            oout.close();
        } catch (Exception e) {
            throw new FailedException();
        }
    }
   
    public void readPubKeyFromFile(File file) throws IOException, FailedException {
        try {
            InputStream in = new FileInputStream(file);
            ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(in));
            publicKey = (PublicKey) oin.readObject();
            oin.close();
        } catch (Exception e) {
            throw new FailedException();
        }
    }

    public void readPrivateKeyFromFile(File file) throws IOException, FailedException {
        try {
            InputStream in = new FileInputStream(file);
            ObjectInputStream oin = new ObjectInputStream(new BufferedInputStream(in));
            privateKey = (PrivateKey) oin.readObject();
            oin.close();
        } catch (Exception e) {
            throw new FailedException();
        }
    }

    public byte[] encrypt(byte[] data) throws FailedException {
        byte[] encrypted = null;
        try {
            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            encrypted = cipher.doFinal(data);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            throw new FailedException();
        }
        return encrypted;
    }

    public byte[] decrypt(byte[] encrypted) throws FailedException {
        byte[] data = null;
        try {
            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWithSHA-256AndMGF1Padding");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            data = cipher.doFinal(encrypted);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException e) {
            throw new FailedException();
        }
        return data;
    }

    public byte[] sign(byte[] data) throws FailedException {
        byte[] signature = null;
        try {
            Signature signer = Signature.getInstance("SHA256withRSA");
            signer.initSign(privateKey);
            signer.update(data);
            signature = signer.sign();
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            throw new FailedException();
        }
        return signature;
    }

    public boolean verify(byte[] data, byte[] signature) throws FailedException {
        boolean out = false;
        try {
            Signature signer = Signature.getInstance("SHA256withRSA");
            signer.initVerify(publicKey);
            signer.update(data);
            out = signer.verify(signature);
        } catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
            throw new FailedException();
        }
        return out;
    }

}
