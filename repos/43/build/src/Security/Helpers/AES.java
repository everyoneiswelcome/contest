package Security.Helpers;

import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES {
       
    public static byte[] encrypt(byte[] data, byte[] iv, byte[] key) {
        byte[] enc = null;
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec keyspec = new SecretKeySpec(key, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, keyspec, new IvParameterSpec(iv));
            enc = cipher.doFinal(data);
        } catch (Exception ex) { }
        
        return enc;
    }

    public static byte[] decrypt(byte[] data, byte[] iv, byte[] key) {
        byte[] dec = null;
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec keyspec = new SecretKeySpec(key, "AES");
            cipher.init(Cipher.DECRYPT_MODE, keyspec, new IvParameterSpec(iv));
            dec = cipher.doFinal(data);
        } catch (Exception ex) { }
        
        return dec;
    }

    public static byte[] generateIV() {
        byte[] iv = null;
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            iv = new byte[cipher.getBlockSize()];
            SecureRandom random = new SecureRandom();
            random.nextBytes(iv);        
        } catch (Exception ex) { }
        return iv;
    }

    public static byte[] generateKey() {
        return generateIV();
    }
}
