package Security;

import Security.Helpers.RSA;
import Main.Exceptions.FailedException;
import java.io.File;
import java.io.IOException;
import java.security.PrivateKey;

public class CardBankSecurity {

    private CardBankSecurity() {}
    
    public static RSA init(File cf, boolean generate) throws FailedException {
        RSA helper = new RSA();
        try {
            if(generate) {
                if(cf.exists()) throw new FailedException();
                cf.createNewFile();
                if(!cf.canWrite()) throw new FailedException();
            } else {
                if(!cf.exists()) throw new FailedException();
                if(!cf.canRead()) throw new FailedException();
                helper.readPrivateKeyFromFile(cf);                
            }
        } catch (IOException | SecurityException ex) { throw new FailedException(); }
        
        return helper;
    }

    public static void savePrivateKey(File cf, PrivateKey pk) throws FailedException {
        RSA helper = new RSA(pk);
        helper.savePrivateKey(cf);
        cf.setReadOnly();
    }
    
}
