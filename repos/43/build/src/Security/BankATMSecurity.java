package Security;

import Security.Helpers.RSA;
import Main.Exceptions.FailedException;
import java.io.File;
import java.io.IOException;

public class BankATMSecurity {

    private BankATMSecurity() {};
    
    public static RSA init(File af) throws FailedException {
        try {
            if(af.exists()) throw new FailedException();
            af.createNewFile();
            if(!af.canWrite()) throw new FailedException();
        } catch (IOException | SecurityException ex) { throw new FailedException(); }
                
        RSA helper = new RSA();
        helper.generateKeyPair();
        helper.savePublicKey(af);
        
        return helper;
    }
    
}
