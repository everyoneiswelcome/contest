package Security;

import Security.Helpers.RSA;
import Main.Exceptions.FailedException;
import java.io.File;
import java.io.IOException;

public class ATMBankSecurity {

    private ATMBankSecurity() {}
    
    public static RSA init(File af) throws FailedException {
        RSA helper = new RSA();
        try {
            if(!af.exists()) throw new FailedException();
            if(!af.canRead()) throw new FailedException();
            helper.readPubKeyFromFile(af);
        } catch (IOException ex) {
        } catch (SecurityException ex) { throw new FailedException(); }
        
        return helper;
    }

}
