package Security;

import Security.Helpers.RSA;
import Main.Exceptions.FailedException;
import java.security.PublicKey;

public class BankCardSecurity {
    
    private BankCardSecurity() {}
    
    public static RSA init(PublicKey pk) throws FailedException {
        RSA helper = new RSA(pk);
        return helper;
    }

}
