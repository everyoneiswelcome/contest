package Regions;

import Regions.Data.Account;
import Regions.Exceptions.*;
import java.math.BigInteger;
import java.util.HashMap;

public class AccountManager implements IAccountManager{

    HashMap<String, Account> accounts;
    
    public AccountManager() {
        accounts = new HashMap<>();
    }
    
    @Override
    public synchronized void newAccount(String name, long balance) throws CollisionException, InvalidAmountException {
        if(accounts.containsKey(name)) throw new CollisionException();
        Account acc = new Account(balance);
        accounts.put(name, acc);
    }

    @Override
    public void deposit(String account, long amount) throws NoAccountException, InvalidAmountException {
        if(!accounts.containsKey(account)) throw new NoAccountException();
        Account acc = accounts.get(account);
        acc.deposit(amount);
    }

    @Override
    public void withdraw(String account, long amount) throws NoFundsException, NoAccountException, InvalidAmountException {
        if(!accounts.containsKey(account)) throw new NoAccountException();
        Account acc = accounts.get(account);
        acc.withdraw(amount);
    }

    @Override
    public BigInteger getBalance(String account) throws NoAccountException {
        if(!accounts.containsKey(account)) throw new NoAccountException();
        Account acc = accounts.get(account);
        return acc.getBalance();
    }

    @Override
    public synchronized void rbNewAccount(String account) {
        if(!accounts.containsKey(account)) return;
        accounts.remove(account);
    }

    @Override
    public void rbDeposit(String account, long amount) {
        if(!accounts.containsKey(account)) return;
        Account acc = accounts.get(account);
        try { acc.withdraw(amount); } catch (Exception e) {}
    }

    @Override
    public void rbWithdraw(String account, long amount) {
        if(!accounts.containsKey(account)) return;
        Account acc = accounts.get(account);
        try { acc.deposit(amount); } catch (Exception e) {}
    }
    
}
