package Regions;

import java.util.HashSet;
import java.util.UUID;

public class ConnLog {

    public final HashSet<UUID> connlog;
    
    public ConnLog() {
        connlog = new HashSet<>();
    }
    
    public synchronized boolean accept(UUID id) {
        if(connlog.contains(id)) return false;
        connlog.add(id);
        return true;
    }

}
