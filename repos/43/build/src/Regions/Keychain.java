package Regions;

import Regions.Exceptions.CollisionException;
import Regions.Exceptions.UUIDNotFoundException;
import java.security.PublicKey;
import java.util.HashMap;

public class Keychain {
    private final HashMap<String, PublicKey> keychain;
    
    public Keychain() {
        keychain = new HashMap<>();
    }

    public PublicKey getPublicKey(String account) throws UUIDNotFoundException {
        if(!keychain.containsKey(account)) throw new UUIDNotFoundException();
        return keychain.get(account);
    }
    
    public synchronized void insert(String account, PublicKey pk) throws CollisionException {
        if(keychain.containsKey(account)) throw new CollisionException();
        keychain.put(account, pk);
    }

}
