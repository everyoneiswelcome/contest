package Regions.Data;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

public class Card {
    private final KeyPair pair;
        
    public Card(KeyPair pair) {
        this.pair = pair;
    }
    
    public PrivateKey getPrivateKey() {
        return pair.getPrivate();
    }
    
    public PublicKey getPublicKey() {
        return pair.getPublic();
    }
    
    public KeyPair getKeyPair() {
        return pair;
    }
}
