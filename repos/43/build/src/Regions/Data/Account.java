package Regions.Data;

import Regions.Exceptions.*;
import java.math.BigInteger;

public class Account {
    
    private BigInteger balance;
    
    public Account(long value) throws InvalidAmountException {
        if( value <= 0L || 429496729599L < value) throw new InvalidAmountException();
        this.balance = new BigInteger(String.valueOf(value));
    }
    
    public synchronized BigInteger getBalance() {
        return balance;
    }
    
    public synchronized void withdraw(long value) throws InvalidAmountException, NoFundsException {
        if( value <= 0L || 429496729599L < value) throw new InvalidAmountException();
        BigInteger val = new BigInteger(String.valueOf(value));
        if( balance.compareTo(val) < 0 ) throw new NoFundsException();
        balance = balance.subtract(val);
    }

    public synchronized void deposit(long value) throws InvalidAmountException {
        if( value <= 0L || 429496729599L < value) throw new InvalidAmountException();
        BigInteger val = new BigInteger(String.valueOf(value));
        balance = balance.add(val);
    }
    
}
