package Regions.Data;

import java.io.Serializable;
import java.math.BigInteger;

public class Exponents implements Serializable {
    
    private final BigInteger m;
    private final BigInteger e;
    
    public Exponents(BigInteger m, BigInteger e) {
        this.m = m;
        this.e = e;
    }
    
    public BigInteger getM() {
        return m;
    }

    public BigInteger getE() {
        return e;
    }

}
