package Regions;

import Regions.Data.Card;
import java.util.ArrayDeque;


public class CardPool {

    private final ArrayDeque<Card> pool;
    private final int limit;
    
    public CardPool(int limit) {
        pool = new ArrayDeque<>();
        this.limit = limit;
    }
    
    public synchronized Card getCard() {
        while(pool.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException ex) { }
        }      
        Card c = pool.removeFirst();
        notifyAll();
        return c;
    }
    
    public synchronized void insertCard(Card c) {
        pool.addLast(c);
        notifyAll();
        while(pool.size() > limit) {
            try {
                wait();
            } catch (InterruptedException ex) { }
        }
    }
       
}
