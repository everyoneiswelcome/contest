package Regions;


import java.util.ArrayDeque;

public class IVPool {
    
    private final ArrayDeque<byte[]> pool;
    private final int limit;
    
    public IVPool(int limit) {
        pool = new ArrayDeque<>();
        this.limit = limit;
    }

    public synchronized byte[] getIV() {
        while(pool.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException ex) { }
        }      
        byte[] iv = pool.removeFirst();
        notifyAll();
        return iv;
    }
    
    public synchronized void insertIV(byte[] iv) {
        pool.addLast(iv);
        notifyAll();
        while(pool.size() > limit) {
            try {
                wait();
            } catch (InterruptedException ex) { }
        }
    }

}
