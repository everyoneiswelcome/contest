package Regions;

import Regions.Exceptions.*;
import java.math.BigInteger;

public interface IAccountManager {

    public void newAccount(String name, long balance) throws CollisionException, InvalidAmountException;
    public void rbNewAccount(String name);
    public void deposit(String account, long amount) throws NoAccountException, InvalidAmountException;
    public void rbDeposit(String account, long amount);
    public void withdraw(String account, long amount) throws NoFundsException, NoAccountException, InvalidAmountException;
    public void rbWithdraw(String account, long amount);
    public BigInteger getBalance(String account) throws NoAccountException;

}
