package Main.Helpers;

import java.math.BigInteger;

public class Amount {

    public static long toLong(String amount) {
        String[] temp = amount.split("[.]");
        
        long value = Long.parseLong(temp[0]) * 100L;
        long mod = Long.parseLong(temp[1]);
        
        return value + mod; 
    }
    
    public static String parse(String amount) {
        String[] temp = amount.split("[.]");
        
        //long value = Long.parseLong(temp[0]);
        long mod = Long.parseLong(temp[1]);
        
        String rem;
        if(mod % 10L == 0L) { rem = Long.toString(mod / 10L); }
        else if(mod < 10L) { rem = "0" + Long.toString(mod); }
        else rem = Long.toString(mod);
        
        return (mod == 0L) ? temp[0] : temp[0] + "." + rem; 
    }

    public static String toString(long amount) {
        long value = amount / 100L;
        long mod = amount % 100L;
        
        String rem;
        if(mod % 10L == 0L) { rem = Long.toString(mod / 10L); }
        else if(mod < 10L) { rem = "0" + Long.toString(mod); }
        else rem = Long.toString(mod);
        
        return (mod == 0L) ? Long.toString(value) : Long.toString(value) + "." + rem; 
    }
    
    public static String toString(BigInteger amount) {
        BigInteger cem = new BigInteger("100");
        BigInteger value = amount.divide(cem);
        int mod = amount.mod(cem).intValue();
        
        String rem;
        if(mod % 10 == 0) { rem = Integer.toString(mod / 10); }
        else if(mod < 10) { rem = "0" + Integer.toString(mod); }
        else rem = Integer.toString(mod);
        return (mod == 0) ? value.toString() : value.toString() + "." + rem; 
    }

}
