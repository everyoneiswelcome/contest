package Main.Helpers;

public class JSON {
        
    public static void printNewAccount(String name, String balance) {
        System.out.println("{\"initial_balance\":" + balance + ",\"account\":\"" + name + "\"}");
    }

    public static void printDeposit(String name, String amount) {
        System.out.println("{\"deposit\":" + amount + ",\"account\":\"" + name + "\"}");
    }

    public static void printWithdraw(String name, String amount) {
        System.out.println("{\"withdraw\":" + amount + ",\"account\":\"" + name + "\"}");
    }

    public static void printBalance(String name, String balance) {
        System.out.println("{\"balance\":" + balance + ",\"account\":\"" + name + "\"}");
    }

}
