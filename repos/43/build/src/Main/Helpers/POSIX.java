package Main.Helpers;

import Main.Exceptions.FailedException;
import java.util.ArrayList;

public class POSIX {

    public static String[] parse(String[] args) throws FailedException {
        ArrayList<String> params = new ArrayList<>();
        for(String arg : args) {
            if(arg == null || arg.compareTo("") == 0) throw new FailedException();
            if(arg.startsWith("-")) {
                String[] xx = arg.split("-.{1}");
                if(xx.length > 1) { params.add(arg.substring(0, 2)); params.add(xx[1]); }
                else params.add(arg);
            } else {
                params.add(arg);
            }
        }
        return params.toArray(new String[params.size()]);
    }

}
