package Main;

import Security.BankATMSecurity;
import Security.Helpers.RSA;
import Main.Exceptions.FailedException;
import Main.Exceptions.ProtocolErrorException;
import Main.Helpers.POSIX;
import Main.Parameters.BankParameter;
import Regions.AccountManager;
import Regions.CardPool;
import Regions.ConnLog;
import Regions.IVPool;
import Regions.Keychain;
import Server.Proxy.ATMProxy;
import Server.ServerCom;
import Server.Utils.CardGenerator;
import Server.Utils.IVGenerator;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import java.io.File;
import static java.lang.Thread.MIN_PRIORITY;

public class BankMain {
    
    public static void main(String[] args) {
        
        JCommander cmd = new JCommander();
        BankParameter param = new BankParameter();
        
        try {
            String[] posix = POSIX.parse(args);
            cmd.addObject(param);
            cmd.parse(posix);
        } catch (ParameterException | FailedException e) { 
            System.exit(255);
        }
        
        File af = new File(param.auth_file);
        RSA rsa_atm;
        try {
            rsa_atm = BankATMSecurity.init(af);
        } catch (FailedException ex) {
            System.exit(255);
            return;
        }
        
        System.out.println("created");
        
        int port = Integer.parseInt(param.port);              
        ServerCom scon = null;
        
        boolean started = false;
        while(!started) {
            try {    
                scon = new ServerCom(port);
                scon.start();
                started = true;
            } catch (ProtocolErrorException ex) { }
        }
        
        AccountManager accounts = new AccountManager();
        ConnLog connlog = new ConnLog();
        Keychain keychain = new Keychain();
        
        CardPool cardpool = new CardPool(10);
        IVPool ivpool = new IVPool(15);
        int cores = Runtime.getRuntime().availableProcessors();

        CardGenerator[] cgs = new CardGenerator[cores > 1 ? cores-1 : 1];
        for(CardGenerator cg : cgs) {
            cg = new CardGenerator(cardpool);
            //cg.setPriority(MIN_PRIORITY);
            cg.start();
        }

        // One is more than enough...
        IVGenerator ivgen = new IVGenerator(ivpool);
        //ivgen.setPriority(MIN_PRIORITY);
        ivgen.start();
        

        while(true) {
            try {
                ServerCom sconi = scon.accept();
                ATMProxy atm = new ATMProxy(sconi, rsa_atm, connlog, keychain, accounts, cardpool, ivpool);
                atm.start();
            } catch (ProtocolErrorException ex) {
                System.out.println("protocol_error");
            }
        }
        
        //System.exit(255);
    }
}
