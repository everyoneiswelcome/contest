package Main.Parameters.Op;

import Main.Parameters.ATMCommonParameters;

public class MainATMOp {
    private final EMainATMOps op;
    private final String amount;
    private final ATMCommonParameters cp;
    
    public MainATMOp(EMainATMOps op, ATMCommonParameters cp, String amount) {
        this.op = op;
        this.amount = amount;
        this.cp = cp;
    }
    
    public MainATMOp(EMainATMOps op, ATMCommonParameters cp) {
        this.op = op;
        this.amount = null;
        this.cp = cp;
    }
    
    public EMainATMOps getOP() {
        return op;
    }
    
    public String getAmount() {
        return amount;
    }
    
    public ATMCommonParameters getCP() {
        return cp;
    }

}
