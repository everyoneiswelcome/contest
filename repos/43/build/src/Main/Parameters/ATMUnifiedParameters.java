package Main.Parameters;

import Main.Parameters.Validators.AmountValidator;
import Main.Parameters.Validators.BalanceValidator;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParametersDelegate;

public class ATMUnifiedParameters {
    
    @Parameter(names = "-n", required = false, validateWith = BalanceValidator.class)
    public String new_balance;

    @Parameter(names = "-d", required = false, validateWith = AmountValidator.class)
    public String deposit_amount;

    @Parameter(names = "-w", required = false, validateWith = AmountValidator.class)
    public String withdraw_amount;

    @Parameter(names = "-g", required = false)
    public boolean get_balance;
        
    @ParametersDelegate
    public ATMCommonParameters cp = new ATMCommonParameters();

}
