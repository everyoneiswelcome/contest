package Main.Parameters;

import Main.Parameters.Validators.FilenameValidator;
import Main.Parameters.Validators.PortValidator;
import com.beust.jcommander.Parameter;

public class BankParameter {
    @Parameter(names = "-s", required = false, validateWith = FilenameValidator.class)
    public String auth_file = "bank.auth";

    @Parameter(names = "-p", required = false, validateWith = PortValidator.class)
    public String port = "3000";
}
