package Main.Parameters;

import Main.Parameters.Validators.*;
import com.beust.jcommander.Parameter;

public class ATMCommonParameters {

    @Parameter(names = "-s", required = false, validateWith = FilenameValidator.class)
    public String auth_file = "bank.auth";

    @Parameter(names = "-i", required = false, validateWith = IPv4Validator.class)
    public String ipv4 = "127.0.0.1";

    @Parameter(names = "-p", required = false, validateWith = PortValidator.class)
    public String port = "3000";

    @Parameter(names = "-c", required = false, validateWith = FilenameValidator.class)
    public String card_file;

    @Parameter(names = "-a", required = true, validateWith = AccountValidator.class)
    public String account;

}