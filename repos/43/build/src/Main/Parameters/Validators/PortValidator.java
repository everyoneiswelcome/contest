package Main.Parameters.Validators;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class PortValidator  implements IParameterValidator {

    @Override
    public void validate(String name, String value) throws ParameterException {
        if(!value.matches("[1-9][0-9]{3,4}"))
            throw new ParameterException("");
        
        int v = Integer.parseInt(value);
        if( (v < 1024) || (v > 65535) ) throw new ParameterException("");           
    }
}
