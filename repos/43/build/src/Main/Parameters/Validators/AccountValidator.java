package Main.Parameters.Validators;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class AccountValidator implements IParameterValidator {

    @Override
    public void validate(String name, String value) throws ParameterException {
        if(!value.matches("[_\\-\\.0-9a-z]{1,250}"))
            throw new ParameterException("");
    }
}
