package Main.Parameters.Validators;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class IPv4Validator implements IParameterValidator {

    @Override
    public void validate(String name, String value) throws ParameterException {
        if(!value.matches("((0|([1-9][0-9]{0,2}))[.]){3}(0|([1-9][0-9]{0,2}))"))
            throw new ParameterException("");
        
        String[] ipv4 = value.split(".");        
        for(String octet : ipv4) {
            int v = Integer.parseInt(octet);
            if( (v < 0) || (v > 255) ) throw new ParameterException("");
        }    
    }
}
