package Main.Parameters.Validators;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class AmountValidator implements IParameterValidator {

    @Override
    public void validate(String name, String value) throws ParameterException {
        if(!value.matches("(0|[1-9][0-9]*)[.][0-9]{2}"))
            throw new ParameterException("");
    }
    
}
