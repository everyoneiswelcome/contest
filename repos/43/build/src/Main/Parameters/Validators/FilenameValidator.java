package Main.Parameters.Validators;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

public class FilenameValidator implements IParameterValidator {

    @Override
    public void validate(String name, String value) throws ParameterException {
        if(!value.matches("[_\\-\\.0-9a-z]{1,255}"))
            throw new ParameterException("");
        
        if(value.equalsIgnoreCase(".") || value.equalsIgnoreCase(".."))
            throw new ParameterException("");           
    }
}
