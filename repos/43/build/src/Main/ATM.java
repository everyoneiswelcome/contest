package Main;

import Client.Stubs.StubAccountManager;
import Main.Helpers.POSIX;
import Main.Exceptions.*;
import Main.Helpers.Amount;
import Main.Helpers.JSON;
import Main.Parameters.*;
import Main.Parameters.Op.EMainATMOps;
import Main.Parameters.Op.MainATMOp;
import com.beust.jcommander.JCommander;
import java.io.File;
import java.math.BigInteger;

public class ATM {
    
    private static MainATMOp parseParameters(String[] args) throws FailedException {
        
        String[] posix_params = POSIX.parse(args);
        JCommander cmd = new JCommander();       
        ATMUnifiedParameters uparam = new ATMUnifiedParameters();        
        try {
            cmd.addObject(uparam);
            cmd.parse(posix_params);
        } catch (Exception e) { throw new FailedException(); }
        
        // Fix card_file defaults
        if(uparam.cp.card_file == null) {
            uparam.cp.card_file = uparam.cp.account + ".card";
        }
        
        short mutex_count = 0;
        EMainATMOps op = null;
        if(uparam.new_balance != null) { mutex_count++; op = EMainATMOps.NEW; }
        if(uparam.deposit_amount != null) { mutex_count++; op = EMainATMOps.DEPOSIT; }
        if(uparam.withdraw_amount != null) { mutex_count++; op = EMainATMOps.WITHDRAW; }
        if(uparam.get_balance) { mutex_count++; op = EMainATMOps.GETBALANCE; }
        
        if(mutex_count != 1) throw new FailedException();
        
        switch(op) {
            case NEW:
                return new MainATMOp(op, uparam.cp, uparam.new_balance);
            case DEPOSIT:
                return new MainATMOp(op, uparam.cp, uparam.deposit_amount);
            case WITHDRAW:
                return new MainATMOp(op, uparam.cp, uparam.withdraw_amount);
            case GETBALANCE:
                return new MainATMOp(op, uparam.cp);
            default:
                throw new FailedException(); // Shouldn't get here ever
        }
        
    }
    
    private static void newAccount(MainATMOp op) throws FailedException, ProtocolErrorException {     
        File cf = new File(op.getCP().card_file);
        File af = new File(op.getCP().auth_file);
        int port = Integer.parseInt(op.getCP().port);               
        StubAccountManager am = new StubAccountManager(op.getCP().ipv4, port, af, cf);
        
        try {
            am.newAccount(op.getCP().account, Amount.toLong(op.getAmount()));
        } catch (Exception ex) {
            throw ex;
        }
        
        // Final stages
        JSON.printNewAccount(op.getCP().account, Amount.parse(op.getAmount()));        
    }
    
    private static void deposit(MainATMOp op) throws FailedException, ProtocolErrorException {
        File cf = new File(op.getCP().card_file);
        File af = new File(op.getCP().auth_file);
        int port = Integer.parseInt(op.getCP().port);               
        StubAccountManager am = new StubAccountManager(op.getCP().ipv4, port, af, cf);
        
        try {
            am.deposit(op.getCP().account, Amount.toLong(op.getAmount()));
        } catch (Exception ex) {
            throw ex;
        }
        
        // Final stages
        JSON.printDeposit(op.getCP().account, Amount.parse(op.getAmount()));        
    }

    private static void withdraw(MainATMOp op) throws FailedException, ProtocolErrorException {

        File cf = new File(op.getCP().card_file);
        File af = new File(op.getCP().auth_file);
        int port = Integer.parseInt(op.getCP().port);               
        StubAccountManager am = new StubAccountManager(op.getCP().ipv4, port, af, cf);
        
        try {
            am.withdraw(op.getCP().account, Amount.toLong(op.getAmount()));
        } catch (Exception ex) {
            throw ex;
        }
        
        // Final stages
        JSON.printWithdraw(op.getCP().account, Amount.parse(op.getAmount()));        
    }

    private static void getBalance(MainATMOp op) throws FailedException, ProtocolErrorException {
        File cf = new File(op.getCP().card_file);
        File af = new File(op.getCP().auth_file);
        int port = Integer.parseInt(op.getCP().port);               
        StubAccountManager am = new StubAccountManager(op.getCP().ipv4, port, af, cf);
        
        BigInteger balance;
        try {
            balance = am.getBalance(op.getCP().account);
        } catch (Exception ex) {
            throw ex;
        }
        
        // Final stages
        JSON.printBalance(op.getCP().account, Amount.toString(balance));   
    }

    public static void main(String[] args) {
               
        try {            
            MainATMOp op = parseParameters(args);
            switch(op.getOP()) {
                case NEW:
                    newAccount(op); // 1009
                    break;
                case WITHDRAW:
                    withdraw(op);   // 373
                    break;
                case DEPOSIT:
                    deposit(op);    // 326
                    break;
                case GETBALANCE:
                    getBalance(op); // 292
                    break;
                default:
                    throw new FailedException();
            }
        
        } catch (FailedException ex) {
            System.exit(255);
        } catch (ProtocolErrorException ex) {
            System.exit(63);
        }  catch (Exception ex) {
            System.exit(255);
        }
        
        System.exit(0);
    }
}
