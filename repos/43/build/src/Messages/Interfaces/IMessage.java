package Messages.Interfaces;

import Messages.Enumerates.MessageCode;
import java.io.Serializable;

public interface IMessage extends Serializable {
    MessageCode getCode();
}
