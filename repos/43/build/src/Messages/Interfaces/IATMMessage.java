
package Messages.Interfaces;

import java.util.UUID;

public interface IATMMessage extends IMessage {
    public UUID getUUID();
}
