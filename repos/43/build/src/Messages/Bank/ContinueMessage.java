package Messages.Bank;

import Messages.Enumerates.MessageCode;
import Messages.Interfaces.IMessage;

public class ContinueMessage implements IMessage {

    private final MessageCode code = MessageCode.CONTINUE;
    
    @Override
    public MessageCode getCode() {
        return code;
    }
    
}
