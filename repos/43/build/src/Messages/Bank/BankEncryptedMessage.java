package Messages.Bank;

import Main.Exceptions.FailedException;
import Main.Exceptions.ProtocolErrorException;
import Messages.Interfaces.IMessage;
import Regions.IVPool;
import Security.Helpers.AES;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class BankEncryptedMessage implements Serializable {
    private final byte[] enc;
    private final byte[] iv;

    public BankEncryptedMessage(IMessage msg, byte[] real_key) throws FailedException {
        this(msg, real_key, AES.generateIV());        
    }

    public BankEncryptedMessage(IMessage msg, IVPool pool) throws FailedException {
        this(msg, AES.generateKey(), pool.getIV());
    }

    public BankEncryptedMessage(IMessage msg, byte[] real_key, IVPool pool) throws FailedException {
        this(msg, real_key, pool.getIV());
    }

    public BankEncryptedMessage(IMessage msg, byte[] real_key, byte[] iv) throws FailedException {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec keyspec = new SecretKeySpec(real_key, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, keyspec, new IvParameterSpec(iv));

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            CipherOutputStream cout = new CipherOutputStream(out, cipher);
            ObjectOutputStream os = new ObjectOutputStream(cout);
            os.writeObject(msg);
            os.close();

            this.enc = out.toByteArray();
            this.iv = iv;
        } catch (Exception e) { throw new FailedException(); }   
    }
    
    public IMessage getIMessage(byte[] real_key) throws ProtocolErrorException {
        IMessage msg;
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec keyspec = new SecretKeySpec(real_key, "AES");
            cipher.init(Cipher.DECRYPT_MODE, keyspec, new IvParameterSpec(iv));

            ByteArrayInputStream ins = new ByteArrayInputStream(enc);
            CipherInputStream cins = new CipherInputStream(ins, cipher);
            ObjectInputStream is = new ObjectInputStream(cins);
            msg = (IMessage) is.readObject();
            is.close();
        } catch (Exception e) { throw new ProtocolErrorException(); }
        return msg;
    }
}
