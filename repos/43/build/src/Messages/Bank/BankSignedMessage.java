package Messages.Bank;

import Main.Exceptions.FailedException;
import Main.Exceptions.ProtocolErrorException;
import Messages.Interfaces.IMessage;
import Regions.IVPool;
import Security.Helpers.RSA;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class BankSignedMessage implements Serializable {
    
    private final BankEncryptedMessage msg;
    private final byte[] signature; 
    
    public BankSignedMessage(IMessage msg, RSA bank, byte[] key, IVPool ivpool) throws FailedException {
        this(msg, bank, key, ivpool.getIV());
    }
    
    public BankSignedMessage(IMessage msg, RSA bank, byte[] key, byte[] iv) throws FailedException {
        this.msg = new BankEncryptedMessage(msg, key, iv);
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(this.msg);
            os.close();
            
            byte[] data = out.toByteArray();
            signature = bank.sign(data);
        } catch(Exception e) { throw new FailedException(); }
    }
    
    public IMessage getIMessage(RSA atm, byte[] key) throws ProtocolErrorException {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(msg);
            os.close();
            
            byte[] data = out.toByteArray();        
            if(!atm.verify(data, signature)) throw new ProtocolErrorException();
        } catch(Exception e) { throw new ProtocolErrorException(); }        
        return msg.getIMessage(key);
    }

}
