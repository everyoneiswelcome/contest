package Messages.Bank;

import Messages.Enumerates.MessageCode;
import Messages.Interfaces.IMessage;

public class NACKMessage implements IMessage {

    private final MessageCode code = MessageCode.NACK;
    
    @Override
    public MessageCode getCode() {
        return code;
    }
}
