package Messages.Bank;

import Messages.Enumerates.MessageCode;
import Messages.Interfaces.IMessage;
import java.math.BigInteger;

public class BalanceMessage implements IMessage {

    private final MessageCode code = MessageCode.BALANCE;
    private final BigInteger balance;
    
    public BalanceMessage(BigInteger balance) {
        this.balance = balance;
    }
    
    public BigInteger getBalance() {
        return balance;
    }
    
    @Override
    public MessageCode getCode() {
        return code;
    }
}
