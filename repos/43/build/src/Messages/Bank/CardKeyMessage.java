package Messages.Bank;

import Messages.Enumerates.MessageCode;
import Messages.Interfaces.IMessage;
import java.security.KeyPair;
import java.security.PrivateKey;

public class CardKeyMessage implements IMessage {

    private final MessageCode code = MessageCode.CARD;
    private final PrivateKey pvt;
    private final byte[][] keys;

    public CardKeyMessage(KeyPair kp, byte[][] keys) {
        pvt = kp.getPrivate();
        this.keys = keys;
    }
    
    public PrivateKey getPrivateKey() {
        return pvt;
    }
    
    @Override
    public MessageCode getCode() {
        return code;
    }
     
    public byte[][] getNextKeys() {
        return keys;
    }
    
}
