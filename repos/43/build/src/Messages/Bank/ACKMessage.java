package Messages.Bank;

import Messages.Enumerates.MessageCode;
import Messages.Interfaces.IMessage;

public class ACKMessage implements IMessage {

    private final MessageCode code = MessageCode.ACK;
    
    @Override
    public MessageCode getCode() {
        return code;
    }
}
