package Messages.Bank;

import Messages.Enumerates.MessageCode;
import Messages.Interfaces.IMessage;

public class CollisionMessage implements IMessage {

    private final MessageCode code = MessageCode.COLLISION;
    
    @Override
    public MessageCode getCode() {
        return code;
    }
    
}
