package Messages.Enumerates;

public enum MessageCode {
    HELLO,
    CONTINUE,
    COLLISION,
    DEPOSIT,
    WITHDRAW,
    NEW,
    CARD,
    GETBALANCE,
    BALANCE,
    ACK,
    NACK
}
