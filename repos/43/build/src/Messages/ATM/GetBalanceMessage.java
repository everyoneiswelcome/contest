package Messages.ATM;

import Messages.Enumerates.MessageCode;
import Messages.Interfaces.IMessage;

public class GetBalanceMessage implements IMessage {

    private final MessageCode code = MessageCode.GETBALANCE;
    private final String account;

    public GetBalanceMessage(String account) {
        this.account = account;
    }
    
    public String getAccount() {
        return account;
    }
       
    @Override
    public MessageCode getCode() {
        return code;
    }
    
}
