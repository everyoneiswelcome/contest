package Messages.ATM;

import Security.Helpers.AES;
import Security.Helpers.RSA;
import Main.Exceptions.FailedException;
import Main.Exceptions.ProtocolErrorException;
import Messages.Interfaces.IMessage;
import Regions.IVPool;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.MessageDigest;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class ATMRSAEncryptedMessage implements Serializable {
    
    private final byte[] enc;
    private final byte[] iv;
    private final byte[] key;
    private final byte[] hash;

    public ATMRSAEncryptedMessage(IMessage msg, RSA atm) throws FailedException {
        this(msg, atm, AES.generateKey(), AES.generateIV());
    }

    public ATMRSAEncryptedMessage(IMessage msg, RSA atm, byte[] real_key) throws FailedException {
        this(msg, atm, real_key, AES.generateIV());
    }

    public ATMRSAEncryptedMessage(IMessage msg, RSA atm, byte[] real_key, IVPool pool) throws FailedException {
        this(msg, atm, real_key, pool.getIV());
    }

    public ATMRSAEncryptedMessage(IMessage msg, RSA atm, byte[] real_key, byte[] iv) throws FailedException {
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec keyspec = new SecretKeySpec(real_key, "AES");
            cipher.init(Cipher.ENCRYPT_MODE, keyspec, new IvParameterSpec(iv));

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            CipherOutputStream cout = new CipherOutputStream(out, cipher);
            ObjectOutputStream os = new ObjectOutputStream(cout);
            os.writeObject(msg);
            os.close();

            this.enc = out.toByteArray();
            this.iv = iv;
            this.key = atm.encrypt(real_key);
            
            // ----
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] v = new byte[enc.length + this.iv.length + key.length];
            System.arraycopy(enc, 0, v, 0, enc.length);
            System.arraycopy(this.iv, 0, v, enc.length, this.iv.length);
            System.arraycopy(key, 0, v, enc.length + this.iv.length, key.length);
            this.hash = atm.encrypt(digest.digest(v));
        } catch (Exception e) { throw new FailedException(); }   
    }
    
    public IMessage getIMessage(RSA bank) throws ProtocolErrorException {
        IMessage msg;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] v = new byte[enc.length + iv.length + key.length];
            System.arraycopy(enc, 0, v, 0, enc.length);
            System.arraycopy(iv, 0, v, enc.length, iv.length);
            System.arraycopy(key, 0, v, enc.length + iv.length, key.length);
            if(!Arrays.equals(bank.decrypt(this.hash), digest.digest(v))) throw new ProtocolErrorException();
            // ----
            
            byte[] dec_key = bank.decrypt(key);
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec keyspec = new SecretKeySpec(dec_key, "AES");
            cipher.init(Cipher.DECRYPT_MODE, keyspec, new IvParameterSpec(iv));

            ByteArrayInputStream ins = new ByteArrayInputStream(enc);
            CipherInputStream cins = new CipherInputStream(ins, cipher);
            ObjectInputStream is = new ObjectInputStream(cins);
            msg = (IMessage) is.readObject();
            is.close();
        } catch (Exception e) { throw new ProtocolErrorException(); }
        return msg;
    }

}
