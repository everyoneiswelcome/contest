package Messages.ATM;

import Main.Exceptions.FailedException;
import Messages.Enumerates.MessageCode;
import Messages.Interfaces.IMessage;
import Security.Helpers.RSA;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;

public class CardSignedMessage implements IMessage {

    private final IMessage msg;
    private final byte[] signature;
    
    public CardSignedMessage(IMessage msg, RSA card) throws FailedException {
        this.msg = msg;
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(msg);
            os.close();
            
            byte[] data = out.toByteArray();
            signature = card.sign(data);
        } catch(Exception e) { throw new FailedException(); }
    }
    
    public IMessage getMessage(RSA bank) throws FailedException {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(msg);
            os.close();
            
            byte[] data = out.toByteArray();        
            if(!bank.verify(data, signature)) throw new FailedException();
        } catch(Exception e) { throw new FailedException(); }        
        return msg;
    }
       
    public IMessage getMessage() {       
        return msg;
    }
    
    public boolean verifySignature(RSA bank) throws FailedException {
        boolean valid = false;
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(msg);
            os.close();
         
            byte[] data = out.toByteArray();
            valid = bank.verify(data, signature);
        } catch (Exception e) { throw new FailedException(); }
        return valid;        
    }

    @Override
    public MessageCode getCode() {
        return msg.getCode();
    }

}
