package Messages.ATM;

import Messages.Enumerates.MessageCode;
import Messages.Interfaces.IATMMessage;
import java.util.UUID;

public class HelloMessage implements IATMMessage {
    private final MessageCode code;
    private final UUID id;
    private final byte[][] key;

    public HelloMessage(UUID id, byte[][] keys) {
        this.code = MessageCode.HELLO;
        this.id = id;
        this.key = keys;
    }

    @Override
    public MessageCode getCode() {
        return code;
    }
    
    @Override
    public UUID getUUID() {
        return id;
    }
    
    public byte[][] getKeys() {
        return key;
    }
    
}
