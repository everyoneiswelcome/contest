package Messages.ATM;

import Messages.Enumerates.MessageCode;
import Messages.Interfaces.IMessage;

public class WithdrawMessage implements IMessage {

    private final MessageCode code = MessageCode.WITHDRAW;
    private final String account;
    private final long amount;

    public WithdrawMessage(String account, long amount) {
        this.account = account;
        this.amount = amount;
    }
       
    public String getAccount() {
        return account;
    }
    
    public long getAmount() {
        return amount;
    }
    
    @Override
    public MessageCode getCode() {
        return code;
    }
}
