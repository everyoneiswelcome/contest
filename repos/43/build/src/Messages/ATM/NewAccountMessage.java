package Messages.ATM;

import Messages.Enumerates.MessageCode;
import Messages.Interfaces.IMessage;

public class NewAccountMessage implements IMessage {
    
    private final MessageCode code = MessageCode.NEW;
    private final String account;
    private final long balance;
    
    public NewAccountMessage(String account, long balance) { //, Exponents pub) {
        this.account = account;
        this.balance = balance;
    }
      
    public String getAccount() {
        return account;
    }
    
    public long getBalance() {
        return balance;
    }
    
    @Override
    public MessageCode getCode() {
        return code;
    }
    
}
