package Server.Utils;

import Regions.CardPool;
import Regions.Data.Card;
import java.security.KeyPair;
import java.security.KeyPairGenerator;

public class CardGenerator extends Thread {

    private final CardPool pool;
    
    public CardGenerator(CardPool pool) {
        this.pool = pool;
    }
    
    @Override
    public void run() {
        while(true) {
            try {
                KeyPairGenerator kpg;
                kpg = KeyPairGenerator.getInstance("RSA");
                kpg.initialize(1024);
                KeyPair kp = kpg.genKeyPair();
                pool.insertCard(new Card(kp));
            } catch (Exception ex) { }
        }
    }
}
