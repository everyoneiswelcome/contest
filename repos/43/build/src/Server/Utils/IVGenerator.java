package Server.Utils;

import Regions.IVPool;
import Security.Helpers.AES;

public class IVGenerator extends Thread {
    private final IVPool pool;
    
    public IVGenerator(IVPool pool) {
        this.pool = pool;
    }

    @Override
    public void run() {
        while(true) {
            byte[] iv = AES.generateIV();
            if( iv != null) pool.insertIV(iv);
        }
    }

}
