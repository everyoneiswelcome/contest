package Server;

import Main.Exceptions.ProtocolErrorException;
import java.io.*;
import java.net.*;

public class ServerCom {

   private ServerSocket listeningSocket = null;
   private Socket commSocket = null;
   private int serverPortNumb;

   private ObjectInputStream in = null;
   private ObjectOutputStream out = null;

   public ServerCom(int portNumb) {
      serverPortNumb = portNumb;
   }

   public ServerCom(int portNumb, ServerSocket lSocket) {
      serverPortNumb = portNumb;
      listeningSocket = lSocket;
   }

   public void start() throws ProtocolErrorException {
      try {
        listeningSocket = new ServerSocket(serverPortNumb);
        listeningSocket.setPerformancePreferences(0, Integer.MAX_VALUE, Integer.MIN_VALUE);  // <--- Nice placebo!
      } catch (BindException e) { 
        throw new ProtocolErrorException();
      } catch (IOException e) { 
        throw new ProtocolErrorException();
      }
   }

   public void end() {
      try {
       listeningSocket.close();
      } catch (IOException e) { }
   }

   public ServerCom accept() throws ProtocolErrorException {
      ServerCom scon;

      scon = new ServerCom(serverPortNumb, listeningSocket);
      try {
        scon.commSocket = listeningSocket.accept();
        scon.commSocket.setSoTimeout(10000);
        scon.commSocket.setTcpNoDelay(true);
        scon.commSocket.setPerformancePreferences(0, Integer.MAX_VALUE, Integer.MIN_VALUE);  // <--- Nice placebo!
      } catch (SocketException e) { throw new ProtocolErrorException();
      } catch (IOException e) { throw new ProtocolErrorException(); }

      try {
          scon.in = new ObjectInputStream(scon.commSocket.getInputStream());
          scon.out = new ObjectOutputStream(scon.commSocket.getOutputStream());
      } catch (IOException e) { 
        throw new ProtocolErrorException();
      }

      return scon;
   }

   public void close() {
      try { 
        in.close();
        out.close();
        commSocket.close();
      } catch (IOException e) { }
   }

   public Object readObject() throws ProtocolErrorException {
      Object fromClient = null;

      try { 
        fromClient = in.readObject ();
      } catch (InvalidClassException e) { 
        throw new ProtocolErrorException();
      } catch (IOException e) { 
        throw new ProtocolErrorException();
      } catch (ClassNotFoundException e) { 
        throw new ProtocolErrorException();
      }

      return fromClient;
   }

   public void writeObject(Object toClient) throws ProtocolErrorException {
      try {
        out.writeObject(toClient);
        out.flush();
      } catch (InvalidClassException e) { 
        throw new ProtocolErrorException();
      } catch (NotSerializableException e) { 
        throw new ProtocolErrorException();
      } catch (IOException e) { 
        throw new ProtocolErrorException();
      }
   }
}
