package Server.Proxy;

import Main.Exceptions.FailedException;
import Main.Exceptions.ProtocolErrorException;
import Main.Helpers.Amount;
import Main.Helpers.JSON;
import Messages.ATM.*;
import Messages.Bank.*;
import Messages.Enumerates.MessageCode;
import Messages.Interfaces.*;
import Regions.AccountManager;
import Regions.CardPool;
import Regions.ConnLog;
import Regions.Exceptions.UUIDNotFoundException;
import Regions.IVPool;
import Regions.Keychain;
import Security.BankCardSecurity;
import Security.Helpers.RSA;
import Server.ServerCom;
import java.math.BigInteger;
import java.security.PublicKey;
import java.util.ArrayDeque;

public class ATMProxy extends Thread {
    private final ServerCom sconi;
    private final RSA rsa_atm;
    private final ConnLog connlog;
    private final Keychain keychain;
    private final AccountManager accounts;
    private final CardPool cardpool;
    private final IVPool ivpool;
   
    public ATMProxy(ServerCom sconi, RSA rsa_atm, ConnLog connlog, Keychain keychain, AccountManager accounts, CardPool cardpool, IVPool ivpool) {
        this.sconi = sconi;
        this.rsa_atm = rsa_atm;
        this.connlog = connlog;
        this.keychain = keychain;
        this.accounts = accounts;
        this.cardpool = cardpool;
        this.ivpool = ivpool;
    }
    
    public void verifySignature(ServerCom sconi, RSA rsa_atm, ArrayDeque<byte[]> keys, CardSignedMessage smsg, IVPool ivpool, String account) throws FailedException, ProtocolErrorException {
        boolean failed = false;
        RSA card = null;
        try {
            card = BankCardSecurity.init(keychain.getPublicKey(account));
        } catch (UUIDNotFoundException ex) { failed = true; }
        if(failed || !smsg.verifySignature(card)) {
            BankSignedMessage out = new BankSignedMessage(new NACKMessage(), rsa_atm, keys.removeFirst(), ivpool);
            sconi.writeObject(out);               
            sconi.close();
            throw new FailedException();
        }
    }
    
    
    @Override
    public void run() {
 
        try {
            ATMRSAEncryptedMessage enc = (ATMRSAEncryptedMessage) sconi.readObject();       
            IMessage msg = (IMessage) enc.getIMessage(rsa_atm);
            if(msg.getCode() != MessageCode.HELLO) throw new ProtocolErrorException();
    
            HelloMessage hello = (HelloMessage) msg;           
            ArrayDeque<byte[]> keys = new ArrayDeque<>();
            byte[][] hello_keys = hello.getKeys();
            if(hello_keys.length != 2) throw new ProtocolErrorException();
            for(byte[] key : hello_keys) {
                keys.addLast(key);
            }

            if(!connlog.accept(hello.getUUID())) {
                BankSignedMessage out = new BankSignedMessage(new CollisionMessage(), rsa_atm, keys.removeFirst(), ivpool);
                sconi.writeObject(out);
                throw new FailedException();
            }

            BankSignedMessage out = new BankSignedMessage(new ContinueMessage(), rsa_atm, keys.removeFirst(), ivpool);
            sconi.writeObject(out);

            ATMRSAEncryptedMessage enc_op = (ATMRSAEncryptedMessage) sconi.readObject();
            IMessage op = (IMessage) enc_op.getIMessage(rsa_atm);

            CIAccountManager am = new CIAccountManager();
            switch(op.getCode()) {
                case DEPOSIT:
                    CardSignedMessage sdmsg = (CardSignedMessage) op;
                    DepositMessage dmsg = (DepositMessage) sdmsg.getMessage();
                    verifySignature(sconi, rsa_atm, keys, sdmsg, ivpool, dmsg.getAccount());
                    am.deposit(sconi, rsa_atm, keys, accounts, dmsg.getAccount(), dmsg.getAmount(), ivpool);
                    JSON.printDeposit(dmsg.getAccount(), Amount.toString(dmsg.getAmount()));
                    break;
                case WITHDRAW:
                    CardSignedMessage swmsg = (CardSignedMessage) op;
                    WithdrawMessage wmsg = (WithdrawMessage) swmsg.getMessage();
                    verifySignature(sconi, rsa_atm, keys, swmsg, ivpool, wmsg.getAccount());
                    am.withdraw(sconi, rsa_atm, keys, accounts, wmsg.getAccount(), wmsg.getAmount(), ivpool);
                    JSON.printWithdraw(wmsg.getAccount(), Amount.toString(wmsg.getAmount()));                    
                    break;
                case NEW:
                    NewAccountMessage nmsg = (NewAccountMessage) op;
                    PublicKey pk = am.newAccount(sconi, rsa_atm, keys, cardpool, accounts, nmsg.getAccount(), nmsg.getBalance(), ivpool);
                    keychain.insert(nmsg.getAccount(), pk);
                    JSON.printNewAccount(nmsg.getAccount(), Amount.toString(nmsg.getBalance()));
                    break;
                case GETBALANCE:
                    CardSignedMessage snmsg = (CardSignedMessage) op;
                    GetBalanceMessage gmsg = (GetBalanceMessage) snmsg.getMessage();
                    verifySignature(sconi, rsa_atm, keys, snmsg, ivpool, gmsg.getAccount());
                    BigInteger balance = am.getBalance(sconi, rsa_atm, keys, accounts, gmsg.getAccount(), ivpool);
                    JSON.printBalance(gmsg.getAccount(), Amount.toString(balance));
                    break;
                default:
                    throw new ProtocolErrorException();
            }
        } catch(ProtocolErrorException | ClassCastException e) {
            System.out.println("protocol_error");
        } catch(FailedException e) { 
        } catch(Exception e) { 
        } finally { try { sconi.close(); } catch (Exception e) {} }

    }
}
