package Server.Proxy;

import Main.Exceptions.FailedException;
import Main.Exceptions.ProtocolErrorException;
import Messages.ATM.ATMRSAEncryptedMessage;
import Messages.Bank.*;
import Messages.Enumerates.MessageCode;
import Messages.Interfaces.IMessage;
import Regions.AccountManager;
import Regions.CardPool;
import Regions.Data.Card;
import Regions.Exceptions.*;
import Regions.IVPool;
import Security.Helpers.AES;
import Security.Helpers.RSA;
import Server.ServerCom;
import java.math.BigInteger;
import java.security.PublicKey;
import java.util.ArrayDeque;

public class CIAccountManager {
    public PublicKey newAccount(ServerCom sconi, RSA rsa_atm, ArrayDeque<byte[]> keys, CardPool pool, AccountManager am, String name, long balance, IVPool ivpool) throws FailedException, ProtocolErrorException {
        BankSignedMessage out;
        Card card = null;
        
        try {
            am.newAccount(name, balance);
            byte[][] bkeys = new byte[1][];  // <--- One more for extra handshake
            for(int i = 0; i < bkeys.length; i++) {
                bkeys[i] = AES.generateKey();
                keys.addLast(bkeys[i]);
            }
            card = pool.getCard();
            out = new BankSignedMessage(new CardKeyMessage(card.getKeyPair(), bkeys), rsa_atm, keys.removeFirst(), ivpool);
        } catch (CollisionException | InvalidAmountException ex) {
            out = new BankSignedMessage(new NACKMessage(), rsa_atm, keys.removeFirst(), ivpool);
            try { sconi.writeObject(out); } catch (Exception e) { /* Silence a time-out here */ }
            throw new FailedException();       
        }
                
        try {
            sconi.writeObject(out);
            
            IMessage reply;
            ATMRSAEncryptedMessage in = (ATMRSAEncryptedMessage) sconi.readObject();
            reply = in.getIMessage(rsa_atm);
            if(reply.getCode() != MessageCode.ACK) throw new ProtocolErrorException();

            out = new BankSignedMessage(new ACKMessage(), rsa_atm, keys.removeFirst(), ivpool);
            sconi.writeObject(out);
        } catch(ClassCastException e) {
            am.rbNewAccount(name);
            throw new ProtocolErrorException();
        } catch(ProtocolErrorException e) {
            //pool.rbGetCard(card); <--- Very serious bug! MITM could have the same card re-issued for another account!
            am.rbNewAccount(name);
            throw e;
        }
    
        return card.getPublicKey(); 
    }
    
    public void deposit(ServerCom sconi, RSA rsa_atm, ArrayDeque<byte[]> keys, AccountManager am, String account, long amount, IVPool ivpool) throws FailedException, ProtocolErrorException {        
        BankSignedMessage out;
        try {
            am.deposit(account, amount);
            out = new BankSignedMessage(new ACKMessage(), rsa_atm, keys.removeFirst(), ivpool);
        } catch (NoAccountException | InvalidAmountException ex) {
            out = new BankSignedMessage(new NACKMessage(), rsa_atm, keys.removeFirst(), ivpool);
            try { sconi.writeObject(out); } catch (Exception e) { /* Silence a time-out here */ }
            throw new FailedException();       
        }
        
        try {
            sconi.writeObject(out);
        } catch(ProtocolErrorException e) {
            am.rbDeposit(account, amount);
            throw e;
        }
    }
    
    public void withdraw(ServerCom sconi, RSA rsa_atm, ArrayDeque<byte[]> keys, AccountManager am, String account, long amount, IVPool ivpool) throws FailedException, ProtocolErrorException {
        BankSignedMessage out;
        try {
            am.withdraw(account, amount);
            out = new BankSignedMessage(new ACKMessage(), rsa_atm, keys.removeFirst(), ivpool);
        } catch (NoAccountException | InvalidAmountException | NoFundsException ex) {
            out = new BankSignedMessage(new NACKMessage(), rsa_atm, keys.removeFirst(), ivpool);
            try { sconi.writeObject(out); } catch (Exception e) { /* Silence a time-out here */ }
            throw new FailedException();         
        }
        
        try {
            sconi.writeObject(out);
        } catch(ProtocolErrorException e) {
            am.rbWithdraw(account, amount);
            throw e;
        }        
    }
    
    public BigInteger getBalance(ServerCom sconi, RSA rsa_atm, ArrayDeque<byte[]> keys, AccountManager am, String account, IVPool ivpool) throws FailedException, ProtocolErrorException {
        BigInteger balance;
        BankSignedMessage out;
        try {
            balance = am.getBalance(account);
            out = new BankSignedMessage(new BalanceMessage(balance), rsa_atm, keys.removeFirst(), ivpool);
        } catch (NoAccountException ex) {
            out = new BankSignedMessage(new NACKMessage(), rsa_atm, keys.removeFirst(), ivpool);
            try { sconi.writeObject(out); } catch (Exception e) { /* Silence a time-out here */ }
            throw new FailedException();         
        }
        
        sconi.writeObject(out);       
        return balance;
    }
}
