package Client;

import Main.Exceptions.ProtocolErrorException;
import java.io.*;
import java.net.*;

public class ClientCom {

   private Socket commSocket = null;
   private String serverHostName = null;
   private int serverPortNumb;

   private ObjectInputStream in = null;
   private ObjectOutputStream out = null;

   public ClientCom(String hostName, int portNumb) {
      serverHostName = hostName;
      serverPortNumb = portNumb;
   }

   public boolean open() throws ProtocolErrorException {
      boolean success = true;
      SocketAddress serverAddress = new InetSocketAddress (serverHostName, serverPortNumb);

      try { 
        commSocket = new Socket();
        commSocket.setTcpNoDelay(true);
        commSocket.setPerformancePreferences(0, Integer.MAX_VALUE, Integer.MIN_VALUE);  // <--- Nice placebo!
        commSocket.connect (serverAddress);
        commSocket.setSoTimeout(10000);
      } catch (UnknownHostException e) { 
        throw new ProtocolErrorException();
      } catch (NoRouteToHostException e) { 
        throw new ProtocolErrorException();
      } catch (ConnectException e) { 
        if (e.getMessage().equals ("Connection refused"))
           success = false;
           else { throw new ProtocolErrorException(); }
      } catch (SocketTimeoutException e) { 
        success = false;
      } catch (IOException e) { 
        throw new ProtocolErrorException();
      }

      if (!success) return (success);

      try { 
        out = new ObjectOutputStream (commSocket.getOutputStream());
        in = new ObjectInputStream(commSocket.getInputStream());
      } catch (IOException e) { 
        throw new ProtocolErrorException();
      }

      return (success);
   }

   public void close() {
      try {
        in.close();
        out.close();
        commSocket.close();
      } catch (IOException e) { }
   }

   public Object readObject() throws ProtocolErrorException {
      Object fromServer = null;

      try {
          fromServer = in.readObject();
      } catch (InvalidClassException e) { 
        throw new ProtocolErrorException();
      } catch (IOException e) { 
        throw new ProtocolErrorException(); //System.exit(63);
      } catch (ClassNotFoundException e) { 
        throw new ProtocolErrorException();
      }
      
      return fromServer;
   }

   public void writeObject(Object toServer) throws ProtocolErrorException {
      try { 
        out.writeObject(toServer);
        out.flush();
      } catch (InvalidClassException e) { 
        throw new ProtocolErrorException();
      } catch (NotSerializableException e) { 
        throw new ProtocolErrorException();
      } catch (IOException e) { 
        throw new ProtocolErrorException();
      }
   }
}
