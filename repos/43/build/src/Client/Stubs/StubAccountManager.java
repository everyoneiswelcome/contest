package Client.Stubs;

import Client.ClientCom;
import Main.Exceptions.FailedException;
import Main.Exceptions.ProtocolErrorException;
import Messages.ATM.*;
import Messages.Bank.*;
import Messages.Enumerates.MessageCode;
import Messages.Interfaces.IMessage;
import Security.ATMBankSecurity;
import Security.CardBankSecurity;
import Security.Helpers.AES;
import Security.Helpers.RSA;
import java.io.File;

import java.math.BigInteger;
import java.security.PrivateKey;
import java.util.ArrayDeque;
import java.util.UUID;

public class StubAccountManager {
    private final String host;
    private final int port;
    
    private final File af;
    private final File cf;
    
    private UUID id;
    
    public StubAccountManager(String host, int port, File af, File cf) {
        this.host = host;
        this.port = port;
    
        this.af = af;
        this.cf = cf;
        
        this.id = UUID.randomUUID();    
    }
    
    private ArrayDeque<byte[]> doHello(ClientCom con, RSA bank) throws FailedException, ProtocolErrorException {
        ArrayDeque<byte[]> keys = new ArrayDeque<>();
        IMessage reply;
        do {
            byte[][] bkeys = new byte[2][];
            for(int i = 0; i < bkeys.length; i++) {
                bkeys[i] = AES.generateKey();
                keys.addLast(bkeys[i]);
            }

            HelloMessage hello = new HelloMessage(id, bkeys);
            ATMRSAEncryptedMessage out;
            byte[] key = AES.generateKey();
            out = new ATMRSAEncryptedMessage(hello, bank, key);
            con.writeObject(out);

            try {
                BankSignedMessage in = (BankSignedMessage) con.readObject();
                reply = in.getIMessage(bank, keys.removeFirst());
            } catch(ClassCastException e) {
                throw new ProtocolErrorException();
            }
            if(reply.getCode() == MessageCode.COLLISION) id = UUID.randomUUID();
        } while (reply.getCode() == MessageCode.COLLISION);

        return keys;
    }
    
    public void newAccount(String name, long balance) throws FailedException, ProtocolErrorException {               
        if(balance < 1000L || balance > 429496729599L) throw new FailedException();
        
        RSA card = CardBankSecurity.init(cf, true);
        RSA bank;      
        ArrayDeque<byte[]> keys = null;
        ClientCom con = null;
        try {
            bank = ATMBankSecurity.init(af);
        
            con = new ClientCom(host, port);
            while(!con.open()) { }
        
            try {
                keys = doHello(con, bank);
            } catch (FailedException ex) {
                throw new ProtocolErrorException();
            }
        
            ATMRSAEncryptedMessage out;
            NewAccountMessage nmsg = new NewAccountMessage(name, balance);
            out = new ATMRSAEncryptedMessage(nmsg, bank);
            con.writeObject(out);
        } catch (FailedException | ProtocolErrorException ex) {
            try { con.close(); } catch (Exception e) {}
            cf.delete();
            throw ex;
        } catch (Exception ex) { cf.delete(); throw new FailedException(); }
                
        // ---------
        PrivateKey pk;
        try {
            BankSignedMessage in = (BankSignedMessage) con.readObject();
            IMessage reply = in.getIMessage(bank, keys.removeFirst());
            if(reply.getCode() == MessageCode.NACK) throw new FailedException();
            if(reply.getCode() != MessageCode.CARD) throw new ProtocolErrorException();
            CardKeyMessage ck = (CardKeyMessage) reply;
            pk = ck.getPrivateKey();

            byte[][] nextkeys = ck.getNextKeys();
            for(byte[] k : nextkeys) {
                keys.addLast(k);
            }

            CardBankSecurity.savePrivateKey(cf, pk);

            ATMRSAEncryptedMessage out = new ATMRSAEncryptedMessage(new ACKMessage(), bank);
            con.writeObject(out);                        
        } catch(ClassCastException ex) {
            cf.delete();
            try { con.close(); } catch (Exception e) {}
            throw new ProtocolErrorException();
        } catch (FailedException | ProtocolErrorException ex) {
            cf.delete();
            try { con.close(); } catch (Exception e) {}
            throw ex;
        } catch (Exception ex) {
            cf.delete();
            try { con.close(); } catch (Exception e) {}
            throw new FailedException();
        }

        // ---------
        try {
            BankSignedMessage in = (BankSignedMessage) con.readObject();
            IMessage reply = in.getIMessage(bank, keys.removeFirst());
            if(reply.getCode() != MessageCode.ACK) {
                cf.delete();
                throw new ProtocolErrorException();
            }
        } catch(ClassCastException ex) {
            throw new ProtocolErrorException();
        } catch (ProtocolErrorException ex) {
            throw ex;        
        } catch (Exception ex) { throw new FailedException();
        } finally { try { con.close(); } catch (Exception e) {} }

    }

    public void deposit(String account, long amount) throws FailedException, ProtocolErrorException {        
        if(amount <= 0L || amount > 429496729599L) throw new FailedException();

        RSA card;
        RSA bank;      
        ClientCom con = null;
        ArrayDeque<byte[]> keys = null;
        try {
            card = CardBankSecurity.init(cf, false);
            bank = ATMBankSecurity.init(af);
        
            con = new ClientCom(host, port);
            while(!con.open()) { }
        
            try {
                keys = doHello(con, bank);
            } catch (FailedException ex) {
                throw new ProtocolErrorException();
            }
        
            ATMRSAEncryptedMessage out;
            DepositMessage dmsg = new DepositMessage(account, amount);
            CardSignedMessage csmsg = new CardSignedMessage(dmsg, card);
            out = new ATMRSAEncryptedMessage(csmsg, bank);
            con.writeObject(out);
        } catch (FailedException | ProtocolErrorException ex) {
            try { con.close(); } catch (Exception e) {}
            throw ex;
        } catch (Exception ex) { throw new FailedException(); }
        
        try {
            BankSignedMessage in = (BankSignedMessage) con.readObject();
            IMessage reply = in.getIMessage(bank, keys.removeFirst());
            if(reply.getCode() == MessageCode.NACK) throw new FailedException();
            if(reply.getCode() != MessageCode.ACK) throw new ProtocolErrorException();
        } catch(ClassCastException ex) {
            throw new ProtocolErrorException();
        } catch (FailedException | ProtocolErrorException ex) {
            throw ex;
        } catch (Exception ex) { throw new FailedException();
        } finally { try { con.close(); } catch (Exception e) {} }

    }

    public void withdraw(String account, long amount) throws FailedException, ProtocolErrorException {        
        if(amount <= 0L || amount > 429496729599L) throw new FailedException();

        RSA card;
        RSA bank;      
        ClientCom con = null;
        ArrayDeque<byte[]> keys = null;
        try {
            card = CardBankSecurity.init(cf, false);
            bank = ATMBankSecurity.init(af);
        
            con = new ClientCom(host, port);
            while(!con.open()) { }
        
            try {
                keys = doHello(con, bank);
            } catch (FailedException ex) {
                throw new ProtocolErrorException();
            }
        
            ATMRSAEncryptedMessage out;
            WithdrawMessage wmsg = new WithdrawMessage(account, amount);
            CardSignedMessage csmsg = new CardSignedMessage(wmsg, card);
            out = new ATMRSAEncryptedMessage(csmsg, bank);
            con.writeObject(out);
        } catch (FailedException | ProtocolErrorException ex) {
            try { con.close(); } catch (Exception e) {}
            throw ex;
        } catch (Exception ex) { throw new FailedException(); }
        
        try {
            BankSignedMessage in = (BankSignedMessage) con.readObject();
            IMessage reply = in.getIMessage(bank, keys.removeFirst());
            if(reply.getCode() == MessageCode.NACK) throw new FailedException();
            if(reply.getCode() != MessageCode.ACK) throw new ProtocolErrorException();
        } catch(ClassCastException ex) {
            throw new ProtocolErrorException();
        } catch (FailedException | ProtocolErrorException ex) {
            throw ex;
        } catch (Exception ex) { throw new FailedException();
        } finally { try { con.close(); } catch (Exception e) {} }

    }

    public BigInteger getBalance(String account) throws FailedException, ProtocolErrorException {

        RSA card;
        RSA bank;      
        ClientCom con = null;
        ArrayDeque<byte[]> keys = null;
        try {
            card = CardBankSecurity.init(cf, false);
            bank = ATMBankSecurity.init(af);
        
            con = new ClientCom(host, port);
            while(!con.open()) { }
        
            try {
                keys = doHello(con, bank);
            } catch (FailedException ex) {
                throw new ProtocolErrorException();
            }
        
            ATMRSAEncryptedMessage out;
            GetBalanceMessage gmsg = new GetBalanceMessage(account);
            CardSignedMessage csmsg = new CardSignedMessage(gmsg, card);
            out = new ATMRSAEncryptedMessage(csmsg, bank);
            con.writeObject(out);
        } catch (FailedException | ProtocolErrorException ex) {
            try { con.close(); } catch (Exception e) {}
            throw ex;
        } catch (Exception ex) { throw new FailedException(); }
        
        BigInteger ret;
        try {
            BankSignedMessage in = (BankSignedMessage) con.readObject();
            IMessage reply = in.getIMessage(bank, keys.removeFirst());
            if(reply.getCode() == MessageCode.NACK) throw new FailedException();
            if(reply.getCode() != MessageCode.BALANCE) throw new ProtocolErrorException();
            BalanceMessage bmsg = (BalanceMessage) reply;
            ret = bmsg.getBalance();
        } catch(ClassCastException ex) {
            throw new ProtocolErrorException();
        } catch (FailedException | ProtocolErrorException ex) {
            throw ex;
        } catch (Exception ex) { throw new FailedException();
        } finally { try { con.close(); } catch (Exception e) {} }

        return ret;
    }

}
