#!/bin/sh

rm -f bank
rm -f atm
rm -rf jar
rm -rf class
rm -rf lib/extracted
find src -name "*.java" > sources.txt
mkdir lib/extracted
unzip -q lib/jcommander-1.48.jar -d lib/extracted -x META-INF/*
mkdir class
javac @sources.txt -d class -classpath lib/extracted
rm sources.txt
mv ./lib/extracted/* ./class
cd class
find -name "*.class" > classes.txt
jar cf0m atm ../manifests/manifest_atm.txt @classes.txt
chmod +x atm
mv atm ..
jar cf0m bank ../manifests/manifest_bank.txt @classes.txt
chmod +x bank
mv bank ..
cd ..
rm -rf class

exit 0;
