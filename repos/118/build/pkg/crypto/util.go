package crypto

import (
	"crypto/aes"
	"crypto/cipher"
  "crypto/hmac"
  "crypto/rand"
  "crypto/sha256"
  "errors"
)

const (
  IV_SIZE = 16
  HMAC_SIZE = 32
)

//* Must check length beforehand
func Encrypt(key, mackey, text []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	ciphertext := make([]byte, IV_SIZE+len(text))
	iv := ciphertext[:IV_SIZE]
	if _, err := rand.Read(iv); err != nil {
		return nil, err
	}
	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(ciphertext[IV_SIZE:], text)
  
  mac := hmac.New(sha256.New, mackey)
  mac.Write(ciphertext)
  ciphertext = append(ciphertext, mac.Sum(nil)...)
  return ciphertext, nil
}

//* Must check length beforehand
func Decrypt(key, mackey, text []byte) ([]byte, error) {	
  mac := hmac.New(sha256.New, mackey)
	mac.Write(text[:len(text)-HMAC_SIZE])
	expectedMAC := mac.Sum(nil)
	if !hmac.Equal(text[len(text)-HMAC_SIZE:], expectedMAC) {
    return nil, errors.New("HMACs don't lie/and I'm starting to feel it's wrong")
  }
  text = text[:len(text)-HMAC_SIZE]  
  block, err := aes.NewCipher(key)
  if err != nil {
    return nil, err
  }
  iv := text[:IV_SIZE]
  text = text[IV_SIZE:]
  mode := cipher.NewCBCDecrypter(block, iv)
  mode.CryptBlocks(text, text)
  return text, nil
}
