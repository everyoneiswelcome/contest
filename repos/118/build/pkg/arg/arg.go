package arg

import (
  "strconv"
  "strings"
  "fmt"
  "os"
)

var args map[string]string

func Parse(osargs []string, valid map[byte]bool) bool {
  args = make(map[string]string)
  for _, arg := range osargs {
    if len(arg) > 4096 {
      return false
    }
  }
  var waiting bool
  var last string
  for i := 1; i < len(osargs); i++ {
    arg := strings.Trim(osargs[i], " ")
    if len(arg) == 0 {
      return false
    }
    if waiting {
      args[last] = arg
      waiting = false
    } else {
      if arg[0] == '-' {
        if len(arg) == 1 || !valid[arg[1]] {
          return false
        }
        if arg[1] == 'g' {
          if _, ok := args["g"]; ok {
            return false
          }
          args["g"] = ""
          if len(arg) != 2 {
            if !valid[arg[2]] {
              return false
            }
            arg = "-" + arg[2:]
          } else {
            continue
          }
        }
        command := arg[1:2]
        if _, ok := args[command]; ok {
          return false
        }
        if len(arg) > 2 {
          args[command] = arg[2:]
        } else {
          waiting = true
          last = command
        }
      } else {
        return false
      }
    }
  }
  return true
}

func Int(name string) (int64, bool){
  val, found := args[name]
  fmt.Errorf("Parsing %s which is %s\n", name, val)
  if !found {
    return 0, false
  }
  if strings.Contains(val, "-") || (len(val) > 1 && val[0] == '0') || len(val) == 0 {
    os.Exit(255)
  }
  num, err := strconv.ParseInt(val, 10, 64)
  if err != nil {
    os.Exit(255)
  }
  return num, true
}

func Float(name string) (int64, bool) {
  val, found := args[name]
  if !found {
    return 0, false
  }
  if strings.Contains(val, "-") || len(val) < 4 || val[len(val) - 3] != '.' || (val[0] == '0' && val[1] != '.') {
    os.Exit(255)
  }
  val = val[:len(val)-3] + val[len(val)-2:]
  num, err := strconv.ParseInt(val, 10, 64)
  if err != nil {
    os.Exit(255)
  }
  return num, true
}

func String(name string) (string, bool) {
  val, ok := args[name]
  return val, ok
}