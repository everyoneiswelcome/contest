package arg

import (
  "regexp"
)

const (
  MAX_MONEY = 429496729599
)

var strRegex *regexp.Regexp
var ipRegex *regexp.Regexp
func InitTypeChecking() error {
  var err error
  if strRegex, err = regexp.Compile("^[_\\-\\.0-9a-z]*$"); err != nil {
    return err
  }
  if ipRegex, err = regexp.Compile("^(0|[1-9][0-9]{0,2})\\.(0|[1-9][0-9]{0,2})\\.(0|[1-9][0-9]{0,2})\\.(0|[1-9][0-9]{0,2})$"); err != nil {
    return err
  }
  return nil
}
//* Check to see if deposits larger than this are legal
func IsBalance(b int64) bool {
  return b >= 0 && b <= MAX_MONEY
}

func IsFile(name string) bool {
  return len(name) >= 1 && len(name) <= 255 && 
    name != "." && name != ".." && strRegex.MatchString(name)
}

func IsAccount(name string) bool {
  return len(name) >= 1 && len(name) <= 250 && strRegex.MatchString(name)
}

func IsIp(ip string) bool {
  return ipRegex.MatchString(ip)
}

func IsPort(port int64) bool {
  return port >= 1024 && port <= 65535
}