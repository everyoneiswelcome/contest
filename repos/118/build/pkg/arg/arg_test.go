package arg

import (
  "testing"
)

func getAtmValid() map[byte]bool {
  m := make(map[byte]bool)
  m['s'] = true
  m['i'] = true
  m['p'] = true
  m['c'] = true
  m['a'] = true
  m['n'] = true
  m['d'] = true
  m['w'] = true
  m['g'] = true
  return m
}

func getBankValid() map[byte]bool {
  m := make(map[byte]bool)
  m['s'] = true
  m['p'] = true
  return m
}

func checkStr(t *testing.T, command string, valt string, okt bool) {
  if val, ok := String(command); val != valt || ok != okt {
    t.Errorf("Value for %s was %d, ok was %t\n", command, val, ok)
    t.FailNow()
  }  
}

func checkInt(t *testing.T, command string, valt int64, okt bool) {
  if val, ok := Int(command); val != valt || ok != okt {
    t.Errorf("Value for %s was %d, ok was %t\n", command, val, ok)
    t.FailNow()
  }  
}

func checkFloat(t *testing.T, command string, valt int64, okt bool) {
  if val, ok := Float(command); val != valt || ok != okt {
    t.Errorf("Value for %s was %s, ok was %t\n", command, val, ok)
    t.FailNow()
  }  
}

func TestG(t *testing.T) {
  if !Parse([]string{"___", "-g"}, getAtmValid()) {
    t.Errorf("Failed to parse.\n")
    t.FailNow()
  }
  checkStr(t, "g", "", true)
}

func TestArgWithSpace(t *testing.T) {
  if !Parse([]string{"___", "-a", "cat"}, getAtmValid()) {
    t.Errorf("Failed to parse.\n")
    t.FailNow()
  }
  checkStr(t, "a", "cat", true)
}

func TestArgNoSpace(t *testing.T) {
  if !Parse([]string{"___", "-acat"}, getAtmValid()) {
    t.Errorf("Failed to parse.\n")
    t.FailNow()
  }
  checkStr(t, "a", "cat", true)
}

func TestMultipleArgs(t *testing.T) {
  if !Parse([]string{"___", "-acat", "-c", "rat hat", "-s", "1.10", "-g"}, getAtmValid()) {
    t.Errorf("Failed to parse.\n")
    t.FailNow()
  }
  checkStr(t, "a", "cat", true)
  checkStr(t, "c", "rat hat", true)
  checkStr(t, "s", "1.10", true)
  checkStr(t, "g", "", true)
}

func TestMissingArgs(t *testing.T) {
  if !Parse([]string{"___", "-acat", "-c", "rat hat", "-s", "1.10", "-g"}, getAtmValid()) {
    t.Errorf("Failed to parse.\n")
    t.FailNow()
  }
  checkStr(t, "p", "", false)
  checkStr(t, "b", "", false)
  checkStr(t, "v", "", false)
  checkStr(t, "k", "", false)
}

func TestBlanks(t *testing.T) {
  if !Parse([]string{"___", "-acat", "-c", "", "rat hat", "-s", " ", "   ", "1.10", "-g"}, getAtmValid()) {
    t.Errorf("Failed to parse.\n")
    t.FailNow()
  }
  checkStr(t, "a", "cat", true)
  checkStr(t, "c", "rat hat", true)
  checkStr(t, "s", "1.10", true)
  checkStr(t, "g", "", true)
}

func TestDoubleUp(t *testing.T) {
  if !Parse([]string{"___", "-acat", "-c", "", "rat hat", "-gs", " ", "   ", "1.10"}, getAtmValid()) {
    t.Errorf("Failed to parse.\n")
    t.FailNow()
  }
  checkStr(t, "a", "cat", true)
  checkStr(t, "c", "rat hat", true)
  checkStr(t, "s", "1.10", true)
  checkStr(t, "g", "", true)
}

func TestHyphens(t *testing.T) {
  if !Parse([]string{"___", "-acat", "-c", "", "-rat hat", "-gs", " ", "   ", "1.10"}, getAtmValid()) {
    t.Errorf("Failed to parse.\n")
    t.FailNow()
  }
  checkStr(t, "a", "cat", true)
  checkStr(t, "c", "-rat hat", true)
  checkStr(t, "s", "1.10", true)
  checkStr(t, "g", "", true)
}

func TestInts(t *testing.T) { //*
  if !Parse([]string{"___", "-a10", "-c", "800", "-s", "9999999999"}, getAtmValid()) {
    t.Errorf("Failed to parse.\n")
    t.FailNow()
  }
  checkInt(t, "a", 10, true)
  checkInt(t, "c", 800, true)
  checkInt(t, "s", 9999999999, true)
}

func TestFloats(t *testing.T) {
  if !Parse([]string{"___", "-a10.99", "-c", "0.99", "-s", "00.10", "-p", "99999999999.99"}, getAtmValid()) {
    t.Errorf("Failed to parse.\n")
    t.FailNow()
  }
  checkFloat(t, "a", 1099, true)
  checkFloat(t, "c", 99, true)
  checkFloat(t, "s", 0, false)
  checkFloat(t, "p", 9999999999999, true)
  checkFloat(t, "i", 0, false)
}

// func TestLength(t *testing.T) { //*
  // if !Parse([]string{"___", "-a10", "-c", "", "rat hat", "-gs", " ", "   ", "1.10"}, getAtmValid()) {
    // t.Errorf("Failed to parse.\n")
    // t.FailNow()
  // }
  // checkStr(t, "a", "cat", true)
  // checkStr(t, "c", "rat hat", true)
  // checkStr(t, "s", "1.10", true)
  // checkStr(t, "g", "1", true)
// }

func TestLeadingZeros(t *testing.T) {
  if !Parse([]string{"___", "-a010", "-c", "0800", "-s", "0x9999999999"}, getAtmValid()) {
    t.Errorf("Failed to parse.\n")
    t.FailNow()
  }
  checkInt(t, "a", 0, false)
  checkInt(t, "c", 0, false)
  checkInt(t, "s", 0, false)
}

func TestNonnumeric(t *testing.T) {
  if !Parse([]string{"___", "-acat", "-c", "08r0", "-s", "0x9999999999"}, getAtmValid()) {
    t.Errorf("Failed to parse.\n")
    t.FailNow()
  }
  checkInt(t, "a", 0, false)
  checkInt(t, "c", 0, false)
  checkInt(t, "s", 0, false)
}

func TestDuplicate(t *testing.T) {
  if Parse([]string{"___", "-acat", "-c", "08r0", "-gc", "0x9999999999"}, getAtmValid()) {
    t.Errorf("Failed to fail parse.\n")
    t.FailNow()
  }
}

func TestGDuplicate(t *testing.T) {
  if Parse([]string{"___", "-gacat", "-c", "08r0", "-gs", "0x9999999999"}, getAtmValid()) {
    t.Errorf("Failed to fail parse.\n")
    t.FailNow()
  }
}