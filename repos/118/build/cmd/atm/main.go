//atm
package main

import (
  "../../pkg/arg"
  "fmt"
  "io/ioutil"
  "os"
)

func check(err error) {
  if err != nil {
    //fmt.Printf/**/("%s\n", err.Error())
    os.Exit(255)
  }
}

func main() {
  if !arg.Parse(os.Args, getValidArgs()) || arg.InitTypeChecking() != nil {
    os.Exit(255)
  }
  authFile, authPresent := arg.String("s")
  ipAddress, ipPresent := arg.String("i")
  port, portPresent := arg.Int("p")
  cardFile, cardPresent := arg.String("c")
  account, accountPresent := arg.String("a")
  n, nPresent := arg.Float("n")
  d, dPresent := arg.Float("d")
  w, wPresent := arg.Float("w")
  _, gPresent := arg.String("g")
  
  if !authPresent {
    authFile = "bank.auth"
  }
  if !ipPresent {
    ipAddress = "127.0.0.1"
  }
  if !portPresent {
    port = 3000
  }
  if !cardPresent {
    cardFile = account + ".card"
  }
  if !arg.IsFile(authFile) {
    os.Exit(255)
  }
  if !arg.IsIp(ipAddress) {
    os.Exit(255)
  }
  if !arg.IsPort(port) {
    os.Exit(255)
  }
  if !arg.IsFile(cardFile) {
    os.Exit(255)
  }
  if !arg.IsAccount(account) {
    os.Exit(255)
  }
  numOptions := 0
  if nPresent {
    numOptions++
  }
  if dPresent {
    numOptions++
  }
  if wPresent {
    numOptions++
  }
  if gPresent {
    numOptions++
  }
  if numOptions != 1 {
    //fmt.Printf/**/("Too many options\n")
    os.Exit(255)
  }
  if !accountPresent {
    //fmt.Printf/**/("No account supplied\n")
    os.Exit(255)
  }
  
  //////////////////////////////////////////////////////////////
  
  auth, authErr := ioutil.ReadFile(authFile)
  check(authErr)
  
  a := NewAtm(ipAddress, port, auth)
  if nPresent {
    if !arg.IsBalance(n) {
      os.Exit(255)
    }
    check(a.CreateCardFile(cardFile))
    if a.CreateAccount(account, n) != nil {
      a.DeleteCardFile()
      os.Exit(255)
    }
  } else {
    card, err := ioutil.ReadFile(cardFile)
    check(err)
    if dPresent {
      if !arg.IsBalance(d) {
        os.Exit(255)
      }
      check(a.Deposit(account, d, card))
    } else if wPresent {
      if !arg.IsBalance(w) {
        os.Exit(255)
      }
      check(a.Withdraw(account, w, card))
    } else if gPresent {
      check(a.Balance(account, card))
    } else {
      fmt.Printf/**/("How did I even get here?\n")
      os.Exit(255)
    }
  }
}

func getValidArgs() map[byte]bool {
  m := make(map[byte]bool)
  m['s'] = true
  m['i'] = true
  m['p'] = true
  m['c'] = true
  m['a'] = true
  m['n'] = true
  m['d'] = true
  m['w'] = true
  m['g'] = true
  return m
}