package main

import (
  "crypto/rand"
  "errors"
  "fmt"
  "io/ioutil"
  "os"
  "net"
  "../../pkg/crypto"
  "encoding/binary"
  "strconv"
  "time"
  "bytes"
)

type atm struct {
  ip string
  port int
  card []byte
  auth []byte
  name string
}

const (
  CARDSIZE = 8
)

const (
  NEW = iota
  WITHDRAW
  DEPOSIT
  BALANCE
)

func NewAtm(ipAddress string, port int64, auth []byte) *atm {
  return &atm{
    ip: ipAddress,
    port: int(port),
    auth: auth,
  }
}

func (a *atm) DeleteCardFile() {
  if a.name != "" {
    os.Remove(a.name)
  }
}

func (a *atm) CreateCardFile(name string) error {
  if _, err := os.Stat(name); !os.IsNotExist(err) {
    return errors.New("Card file already exists")
  }
  a.card = make([]byte, CARDSIZE)
  if _, err := rand.Read(a.card); err != nil {
    return err
  }
  a.name = name
  return ioutil.WriteFile(name, a.card, 0644)
}

func (a *atm) CreateAccount(account string, balance int64) error {
  if balance < 1000 {
    return errors.New("Initial balance must be at least $10.00")
  }
  if ok, _ := a.Contact(account, a.card, NEW, balance); !ok {
    return errors.New("Failed contacting the bank")
  }
  fmt.Printf("{\"account\":\"%s\",\"initial_balance\":%d.%.2d}\n", account, balance / 100, balance % 100)
  return nil
}

func (a *atm) Deposit(account string, deposit int64, card []byte) error {
  if deposit <= 0 {
    return errors.New("Can only deposit positive amounts")
  }
  if ok, _ := a.Contact(account, card, DEPOSIT, deposit); !ok {
    return errors.New("Failed contacting the bank")
  }
  fmt.Printf("{\"account\":\"%s\",\"deposit\":%d.%.2d}\n", account, deposit / 100, deposit % 100)
  return nil
}

func (a *atm) Withdraw(account string, withdraw int64, card []byte) error {
  if withdraw <= 0 {
    return errors.New("Can only withdraw positive amounts")
  }
  if ok, _ := a.Contact(account, card, WITHDRAW, withdraw); !ok {
    return errors.New("Failed contacting the bank")
  }
  fmt.Printf("{\"account\":\"%s\",\"withdraw\":%d.%.2d}\n", account, withdraw / 100, withdraw % 100)
  return nil
}

func (a *atm) Balance(account string, card []byte) error {
  ok, balance := a.Contact(account, card, BALANCE, 0)
  if !ok {
    return errors.New("Failed contacting the bank")
  }
  fmt.Printf("{\"account\":\"%s\",\"balance\":%d.%.2d}\n", account, balance / 100, balance % 100)
  return nil
}

func (a *atm) Contact(account string, card []byte, mode byte, balance int64) (bool, int64) {
  conn, err := net.DialTimeout("tcp", a.ip + ":" + strconv.Itoa(a.port), time.Second * 10)
  if err != nil {
    a.DeleteCardFile()
    os.Exit(63)
  }
  cid := make([]byte, 64)
  conn.SetReadDeadline(time.Now().Add(10 * time.Second))
  if n, err2 := conn.Read(cid); n != 64 || err2 != nil {
    a.DeleteCardFile()
    os.Exit(63)
  }
  id, err3 := crypto.Decrypt(a.auth[:32], a.auth[32:], cid)
  if err3 != nil {
    a.DeleteCardFile()
    os.Exit(63)
  }
  msg := make([]byte, 272)
  copy(msg[:250], account)
  copy(msg[250:258], card)
  binary.BigEndian.PutUint64(msg[258:266], uint64(balance))
  msg[266] = mode
  copy(msg[267:272], id[:5])
  
  cipher, err4 := crypto.Encrypt(a.auth[:32], a.auth[32:], msg)
  if err4 != nil || len(cipher) != 320 {
    a.DeleteCardFile()
    os.Exit(63)
  }
  
  conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
  if n, err2 := conn.Write(cipher); n != len(cipher) || err2 != nil {
    a.DeleteCardFile()
    os.Exit(63)
  }
  
  cresult := make([]byte, 64)
  conn.SetReadDeadline(time.Now().Add(10 * time.Second))
  if n, err2 := conn.Read(cresult); n != 64 || err2 != nil {
    a.DeleteCardFile()
    os.Exit(63)
  }
  result, err5 := crypto.Decrypt(a.auth[:32], a.auth[32:], cresult)
  if err5 != nil {
    a.DeleteCardFile()
    os.Exit(63)
  }
  return bytes.Equal(id[:5], result[:5]) && result[5] == 1, int64(binary.BigEndian.Uint64(result[6:14]))
}
