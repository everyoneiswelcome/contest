//bank
package main

import (
  "../../pkg/arg"
  "crypto/rand"
  "errors"
  "fmt"
  "io/ioutil"
  "os"
)

const (
  KEYSIZE = 64
)

func main() {
  if !arg.Parse(os.Args, getValidArgs()) || arg.InitTypeChecking() != nil {
    os.Exit(255)
  }
  port, portPresent := arg.Int("p")
  authFile, authPresent := arg.String("s")
  if !portPresent {
    port = 3000
  }
  if !authPresent {
    authFile = "bank.auth"
  }
  if !arg.IsFile(authFile) {
    os.Exit(255)
  }
  if !arg.IsPort(port) {
    os.Exit(255)
  }
  key, err2 := createKey(authFile)
  if err2 != nil {
    os.Exit(255)
  } else {
    fmt.Println("created")
  }
  b := NewBank(key)
  b.Listen(int(port))
}

func createKey(file string) ([]byte, error) {
  if _, err := os.Stat(file); !os.IsNotExist(err) {
    return nil, errors.New("File already existed")
  }
  bytes := make([]byte, KEYSIZE)
  if _, err := rand.Read(bytes); err != nil {
    return nil, err
  }
  if err := ioutil.WriteFile(file, bytes, 0644); err != nil {
    return nil, err
  }
  return bytes, nil
}

func getValidArgs() map[byte]bool {
  m := make(map[byte]bool)
  m['s'] = true
  m['p'] = true
  return m
}
