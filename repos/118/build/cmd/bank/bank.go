package main

import (
  "fmt"
  "net"
  "time"
  "bytes"
  "errors"
  "strconv"
  "encoding/binary"
  "crypto/rand"
  "../../pkg/crypto"
  "strings"
)

const (
  MAX_MONEY = 429496729599
)

const (
  NEW = iota
  WITHDRAW
  DEPOSIT
  BALANCE
)

type bank struct {
  auth []byte
  accounts map[string]*user
  writers int32
}

type user struct {
  balance int64
  card []byte
}

func NewBank(auth []byte) *bank {
  return &bank{
    auth: auth,
    accounts: make(map[string]*user),
  }
} 

func (b *bank) Listen(port int) error {
  ln, err := net.Listen("tcp", ":" + strconv.Itoa(port))
  if err != nil {
    return errors.New("Couldn't listen on that port")
  }
  for {
    conn, err := ln.Accept()
    if err != nil {
      fmt.Printf("protocol_error\n")
    } else if err2 := b.handleConnection(conn); err2 != nil {
      fmt.Printf("protocol_error\n")
    }
    conn.Close()
  }
}

func (b *bank) handleConnection(conn net.Conn) error {
  id := make([]byte, 16)
  if _, err := rand.Read(id); err != nil {
    return errors.New("Couldn't make a transaction ID")
  }
  cipherid, err3 := crypto.Encrypt(b.auth[:32], b.auth[32:], id)
  if err3 != nil {
    return errors.New("Couldn't encrypt transaction ID")
  }
  conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
  if n, err := conn.Write(cipherid); err != nil || n != len(cipherid) {
    return errors.New("Could not write transaction id")
  }
  
  conn.SetReadDeadline(time.Now().Add(10 * time.Second))
  cipher := make([]byte, 320)
  if n, err := conn.Read(cipher); err != nil || n != 320 {
    //fmt.Printf("Error was %s\n", err.Error())
    return errors.New("Couldn't read full message")
  }
  msg, err := crypto.Decrypt(b.auth[:32], b.auth[32:], cipher)
  if err != nil {
    return err
  }
  account := strings.TrimRight(string(msg[:250]), "\x00")
  auth := msg[250:258]
  balance := int64(binary.BigEndian.Uint64(msg[258:266]))
  mode := msg[266]
  
  if !bytes.Equal(id[:5], msg[267:272]) {
    return errors.New("Invalid transaction ID")
  }
  
  output := ""
  
  if mode == NEW {
    if _, ok := b.accounts[account]; ok {
      id[5] = 0
    } else {
      id[5] = 1
      output = fmt.Sprintf("{\"initial_balance\": %d.%.2d, \"account\": \"%s\"}", balance / 100, balance % 100, account)
    }
  } else {
    user, ok := b.accounts[account]
    if !ok || user == nil || !bytes.Equal(user.card, auth) {
      id[5] = 0
    } else {
      switch mode {
        case BALANCE:
          id[5] = 1
          binary.BigEndian.PutUint64(id[6:14], uint64(user.balance))
          output = fmt.Sprintf("{\"account\":\"%s\",\"balance\":%d.%.2d}", account, user.balance / 100, user.balance % 100)
        case DEPOSIT:
          output = fmt.Sprintf("{\"account\":\"%s\",\"deposit\":%d.%.2d}", account, balance / 100, balance % 100)
          id[5] = 1
        case WITHDRAW:
          if user.balance < balance {
            id[5] = 0
          } else {
            output = fmt.Sprintf("{\"account\":\"%s\",\"withdraw\":%d.%.2d}", account, balance / 100, balance % 100)
            id[5] = 1
          }
        default:
          return errors.New("Unknown mode")
      }
    }
  }
  response, err := crypto.Encrypt(b.auth[:32], b.auth[32:], id)
  if err != nil {
    return errors.New("Could not encrypt")
  }
  conn.SetWriteDeadline(time.Now().Add(10 * time.Second))
  if n, err2 := conn.Write(response); n != len(response) || err2 != nil {
    return errors.New("Timeout")
  }
  if id[5] == 1 {
    switch mode {
      case NEW:
        b.accounts[account] = &user{
          balance: balance,
          card: auth,
        }
      case WITHDRAW:
        b.accounts[account].balance -= balance
      case DEPOSIT:
        b.accounts[account].balance += balance
    }
    fmt.Printf("%s\n", output)
    
  }
  
  return nil
}