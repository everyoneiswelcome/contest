#!/usr/bin/python

import os
import json
import sys, getopt ,  ast
import subprocess
import time


DIR="./"
IP ="127.0.0.1"
PORT = "3000"
EXE = "atm"
VERBOSE = False
ONLYFAILED = True

DRYRUN = 0
COUNT = 0
COUNT_FAILED =0
COUNT_PASSED =0



#
# Just print status of each test case
#
def printTestCaseStatus(status, expected, result ):
    global COUNT
    COUNT = COUNT +1
    if not ONLYFAILED or (ONLYFAILED and status == 'FAILED'):
        print ""
        print "#", COUNT , " - ",  status
        print "test:", result['exe'] 
        print "exit codes:| expected:", expected['exit'], "| result:" ,result['exit']
        print "console message:| expected:",expected['output'],"| result:",result['output']
        print ""

#
# Execute as separate process and capture the output and exit codes
#
def exeCase(cmd):
    if DRYRUN == 1:
        print "DRYRUN is ON ", " ".join(cmd)
    else:
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE,stderr=subprocess.PIPE,universal_newlines=True)
        output, err = p.communicate()
        #print "*** Running ", " ".join(cmd)," command ***\n", output
    return {"exe":" ".join(cmd),"output":output,"exit":p.returncode}

#
# Run a test case with given params
#
def runTest(inparams, expected):
    # inparams = {"input": ["-p","%PORT%","-i","%IP%","-a","ted","-g"]},
    # expected = {"output": "","exit": 255}
    cmd = []
    cmd.append(EXE)
    for i in inparams['input']:
        if i == "%PORT%":
            cmd.append(PORT)
        elif i =="%IP%":
            cmd.append(IP)
        else:
            cmd.append(i)
    cmd.insert (0,"-jar")
    cmd.insert (0,"java")
    print cmd
    result = exeCase(cmd)

    # json strong from the running program is currently a stirng
    # need to convert it into dictionary
    a = ""
    if result['output'] and expected['output']:
        a = ast.literal_eval(result['output'])
        if VERBOSE:
            print >> sys.stderr, "expected json=", expected['output']
            print >> sys.stderr, "resulted json=",a
            print >> sys.stderr, "is above equal? ",len(set(expected['output'].items()) ^ set(a.items())) == 0

    status = 'PASS'
    if expected['exit'] == result['exit'] and expected['exit'] >0:
    	status = 'PASS'
        global COUNT_PASSED
        COUNT_PASSED = COUNT_PASSED +1
    elif expected['exit'] == result['exit'] and (len(set(expected['output'].items()) ^ set(a.items())) == 0) :

        status = 'PASS'
        global COUNT_PASSED
        COUNT_PASSED = COUNT_PASSED +1
    else:
        status = 'FAILED'
        global COUNT_FAILED
        COUNT_FAILED = COUNT_FAILED +1

    printTestCaseStatus(status, expected, result)

#
# Wrapper for executing a test case
#
def runTestCases(testcases):
	# testcase is dictionary
	# each test case has 2 elements
    # "input" : {"input": ["-p","%PORT%","-i","%IP%","-a","ted","-g"]},
    # "output": {"output": "","exit": 255}
    runTest(testcases['input'],testcases['output'])

#
# Parse the test file and extract the test cases
#
def runTestFile(file):
	data = json.load(file)
	#inputs is array of [input ]
	for testcase in data['inputs']:
		runTestCases(testcase)

#
# Print usage 
#
def usage():
    print 'usage: runtest.py -d <./core> -p 3000 -e ../atm -i 127.0.0.1'

def endBank(proc):
    print "Stating bank"
    subprocess.call(["kill", "-9", "%d" % proc.pid])

def startBank():
    print "Stopping bank"
    proc = subprocess.Popen(["./bank.sh"], stdout=subprocess.PIPE,stderr=subprocess.PIPE,universal_newlines=True)
    return proc

#######################################################################
# parsing input parameters

try:
    opts, args = getopt.getopt(sys.argv[1:],"d:p:e:i:v")
except getopt.GetoptError:
    usage()
    sys.exit(2)

for opt, arg in opts:
    if opt == '-d':
        DIR = arg
    elif opt == '-i':
        IP = arg
    elif opt == '-p':
        PORT = arg
    elif opt == '-e':
        EXE = arg
    elif opt == '-v':
        VERBOSE = True
    else:
        usage()
        sys.exit(2)

# end of parsing parameters
#######################################################################
# traverse directory, and build list of json files
#for root, dirs, files in os.walk(DIR):
    #path = root.split('/')
    # filter only .json file
 #   files = [ fi for fi in files if  fi.endswith(".json") ]
    #p = os.path.basename(root)
  #  for file in files:
   #     testfile =  root +"/"+ file
print  "Executing cases from:", DIR, 
with open(DIR) as data_file:
            #proc = startBank()    
    runTestFile(data_file)
            #endBank(proc)

print "End of execution:"
print "# of FAILED cases", COUNT_FAILED
print "# of PASSED cases", COUNT_PASSED

