#!/bin/bash
echo "Executing core"
find ../json/core/ -name "*.json" -exec bash -c 'echo {}; time ./run_test.py {}' \;
echo "Core is done"
echo "Executing peformance"
find ../json/performance/ -name "*.json" -exec bash -c 'echo {}; time ./run_test.py {}' \;
echo "Perf is done"
