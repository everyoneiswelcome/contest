package bibifi.sphericalcow.bank;

import bibifi.sphericalcow.common.SimpleLog;

import java.util.Collection;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.ParseException;

public class BankConfig {

    private static final String DEFAULT_SERVER_PORT = "3000";
    private static final String DEFAULT_AUTH_FILE = "bank.auth";

    private Options cliOptions;
    private int port;
    private String authFile;

    public BankConfig() {
        this.cliOptions = new Options()
                .addOption("p", true, "Port")
                .addOption("s", true, "Auth file");
    }

    public boolean parse(String[] args) {
        CommandLine cliArgs = null;

        try {
            cliArgs = new PosixParser().parse(cliOptions, args, true);
        } catch (ParseException e) {
          if (SimpleLog.isDebug()) {
              SimpleLog.log(e);
          }
          return false;
        }

        // No extra arguments given
        if (cliArgs.getArgList().size() > 0) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Unrecognized argument given");
            }
            return false;
        }

        // No duplicate arguments
        for (Option option : (Collection<Option>) cliOptions.getOptions()) {
            if (cliArgs.getOptionValues(option.getOpt()) != null && cliArgs.getOptionValues(option.getOpt()).length > 1) {
                if (SimpleLog.isDebug()) {
                    SimpleLog.log("Duplicate argument given");
                }
                return false;
            }
        }

        // Check formatting
        if (
            cliArgs.getOptionValue('p') != null && !cliArgs.getOptionValue('p').matches("^\\d{1,4096}$") ||
            cliArgs.getOptionValue('s') != null && !cliArgs.getOptionValue('s').matches("^[_\\-\\.0-9a-z]{1,255}$")
        ) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Incorrectly formatted argument given");
            }
            return false;
        }

        // Invalid filenames . & ..
        if (cliArgs.getOptionValue('s') != null && cliArgs.getOptionValue('s').matches("^\\.{1,2}$")) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Invalid file name");
            }
            return false;
        }

        this.port = Integer.parseInt(cliArgs.getOptionValue('p', DEFAULT_SERVER_PORT));
        this.authFile = cliArgs.getOptionValue('s', DEFAULT_AUTH_FILE);

        if (port < 1024 || port > 65535) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Port must be 1024-65535");
            }
            return false;
        }

        return true;
    }

    public int getPort() {
        return this.port;
    }

    public String getAuthFile() {
        return this.authFile;
    }

}
