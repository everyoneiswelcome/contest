package bibifi.sphericalcow.bank;

public class RequestQueueException extends Exception {

	public RequestQueueException(String string) {
		super(string);
	}

}
