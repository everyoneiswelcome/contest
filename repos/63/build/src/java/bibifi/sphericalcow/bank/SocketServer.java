package bibifi.sphericalcow.bank;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import javax.net.ServerSocketFactory;

import bibifi.sphericalcow.common.SimpleLog;
import bibifi.sphericalcow.common.crypto.BlowfishDataEncryptor;
import bibifi.sphericalcow.common.crypto.DataEncryptor;

public class SocketServer extends Thread {

	private ServerSocket serverSocket;
	private final int port;
	private final String ip = "127.0.0.1";
    private String authFile;
	private boolean running = false;
	private static final int TIMEOUT = 10000;
	// max number of request before rejecting requests
	private static final int MAX_BACKLOG_OF_REQUESRS = 1000;
	private static final int QUEUE_SIZE = 100;
	private static final int MIN_THREAD_POOL_SIZE = 1;
	private static final int MAX_THREAD_POOL_SIZE = 100;

    private DataEncryptor dataEncryptor;
	private RequestQueue requestQueue;

	public SocketServer(int port, String authFile) {
		this.port = port;
        this.authFile = authFile;

	}

	public int startServer() {
		int exitcode = 0;
		try {
			this.dataEncryptor = BlowfishDataEncryptor.getInstance(authFile, true);
            // open socket
			// Create our Server Socket
            ServerSocketFactory ssf = ServerSocketFactory.getDefault();
            serverSocket = ssf.createServerSocket();
            serverSocket.setPerformancePreferences(0,1, 2);
            serverSocket.setReceiveBufferSize(1024);
            serverSocket.bind(new InetSocketAddress(ip, port), MAX_BACKLOG_OF_REQUESRS);
           
			// Create our request queue
	        //this.requestQueue = new RequestQueue( dataEncryptor,QUEUE_SIZE,MIN_THREAD_POOL_SIZE,MAX_THREAD_POOL_SIZE );

			this.start();
		} catch (Exception ex) {
            if (SimpleLog.isDebug())
                SimpleLog.log(ex);
			exitcode = 255;
		}
		return exitcode;
	}

	public void stopServer() {
		running = false;
		this.interrupt();
	}

	@Override
	public void run() {
		running = true;
		while (running) {
			try {
                if (SimpleLog.isDebug())
                    SimpleLog.log("Listening for a connection...");
				// Call accept() to receive the next connection
                Socket socket = serverSocket.accept();
                socket.setSoTimeout(TIMEOUT);
                socket.setTcpNoDelay(true);
                socket.setReceiveBufferSize(1024);
                ServerRequestHandler requestHandler = new ServerRequestHandler( dataEncryptor);
                requestHandler.handleRequest(socket);
                // Add the socket to the new RequestQueue
                //this.requestQueue.add( socket );

			} catch (Exception ex) {
                if (SimpleLog.isDebug())
                    SimpleLog.log(ex);
			}
		}

		//this.requestQueue.shutdown();
	}
}
