package bibifi.sphericalcow.bank;

import java.net.*;

import bibifi.sphericalcow.common.SimpleLog;
import bibifi.sphericalcow.common.crypto.DataEncryptor;


public class RequestThread extends Thread {
	
	private RequestQueue queue;
	private boolean running;
	private boolean processing = false;
	private int threadNumber;
	private ServerRequestHandler requestHandler;

	public RequestThread(RequestQueue queue, int threadNumber, DataEncryptor dataEncryptor) throws Exception {
		this.queue = queue;
		this.threadNumber = threadNumber;
		this.requestHandler = new ServerRequestHandler(dataEncryptor);
		
	}

	public boolean isProcessing() {
		return this.processing;
	}

	public void killThread() {
		if (SimpleLog.isDebug()) 
			SimpleLog.log("[" + threadNumber + "]: Attempting to kill thread...");
		this.running = false;
	}

	public void run() {
		this.running = true;
		while (running) {
			try {
				// Obtain the next pending socket from the queue; only process
				// requests if
				// we are still running. The shutdown mechanism will wake up our
				// threads at this
				// point, so our state could have changed to not running here.
				Socket socket = queue.getNextObject();
				if (running) {

					// Mark ourselves as processing a request
					this.processing = true;
					if (SimpleLog.isDebug()) 
						SimpleLog.log("[" + threadNumber + "]: Processing request...");

					// Handle the request
					this.requestHandler.handleRequest(socket);

					// We’ve finished processing, so make ourselves available
					// for the next request
					this.processing = false;
					if (SimpleLog.isDebug()) 
						SimpleLog.log("[" + threadNumber + "]: Finished Processing request...");
				}
			} catch (Exception e) {
				SimpleLog.log(e);
			}
		}
		if (SimpleLog.isDebug()) 
			SimpleLog.log("[" + threadNumber + "]: Thread shutting down...");
	}
}