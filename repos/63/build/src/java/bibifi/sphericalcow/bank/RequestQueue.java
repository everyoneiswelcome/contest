package bibifi.sphericalcow.bank;

import java.net.Socket;
import java.util.*;

import bibifi.sphericalcow.common.SimpleLog;
import bibifi.sphericalcow.common.crypto.DataEncryptor;


public class RequestQueue {
	
	private LinkedList<Socket> queue = new LinkedList<Socket>();
	private int maxQueueLength;
	private int minThreads;
	private int maxThreads;
	private int currentThreads = 0;
	private DataEncryptor dataEncryptor;

	private List<RequestThread> threadPool = new ArrayList<RequestThread>();

	private boolean running = true;

	/**
	 * Creates a new RequestQueue
	 * @param dataEncryptor 
	 * @throws Exception 
	 */
	public RequestQueue( DataEncryptor dataEncryptor, int maxQueueLength, int minThreads, int maxThreads) throws Exception {
		// Initialize our parameters
		
		this.maxQueueLength = maxQueueLength;
		this.minThreads = minThreads;
		this.maxThreads = maxThreads;
		this.currentThreads = this.minThreads;
		this.dataEncryptor = dataEncryptor;

		// Create the minimum number of threads
		for (int i = 0; i < this.minThreads; i++) {
			RequestThread thread = new RequestThread(this, i, dataEncryptor);
			thread.start();
			this.threadPool.add(thread);
		}
	}


	public synchronized void add(Socket o) throws Exception {
		// Validate that we have room of the object before we add it to the
		// queue
		if (queue.size() > this.maxQueueLength) {
			throw new RequestQueueException("The Request Queue is full. Max size = " + this.maxQueueLength);
		}
		
		if (SimpleLog.isDebug()) 
			SimpleLog.log("Adding request socket to the queue. " + o);
		// Add the new object to the end of the queue
		queue.addLast(o);

		// See if we have an available thread to process the request
		boolean availableThread = false;
		for (Iterator<RequestThread> i = this.threadPool.iterator(); i.hasNext();) {
			RequestThread rt = i.next();
			if (!rt.isProcessing()) {
				if (SimpleLog.isDebug()) 
					SimpleLog.log("Found an available thread");
				availableThread = true;
				break;
			}
			  if (SimpleLog.isDebug()) 
				  SimpleLog.log("Thread is busy");
		}

		// See if we have an available thread
		if (!availableThread) {
			if (this.currentThreads < this.maxThreads) {
				SimpleLog.log("Creating a new thread to satisfy the incoming request");
				RequestThread thread = new RequestThread(this, currentThreads++, this.dataEncryptor);
				thread.start();
				this.threadPool.add(thread);
			} else {
				 if (SimpleLog.isDebug()) 
					 SimpleLog.log("Whoops, can’t grow the thread pool, guess you have to wait");
			}
		}

		// Wake someone up
		notifyAll();
	}

	/**
	 * Returns the first object in the queue
	 */
	public synchronized Socket getNextObject() {
		// Setup waiting on the Request Queue
		while (queue.isEmpty()) {
			try {
				if (!running) {
					// Exit criteria for stopping threads
					return null;
				}
				wait();
			} catch (InterruptedException ie) {
				 if (SimpleLog.isDebug()) 
					 	SimpleLog.log(ie);
			}
			
		}

		// Return the item at the head of the queue
		return queue.removeFirst();
	}

	
	public synchronized void shutdown() {
		 if (SimpleLog.isDebug()) 
			 SimpleLog.log("Shutting down request threads...");

		// Mark the queue as not running so that we will free up our request
		// threads
		this.running = false;

		// Tell each thread to kill itself
		for (Iterator<RequestThread> i = this.threadPool.iterator(); i.hasNext();) {
			RequestThread rt =  i.next();
			rt.killThread();
		}

		// Wake up all threads and let them die
		notifyAll();
	}
}