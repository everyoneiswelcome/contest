package bibifi.sphericalcow.bank;

import java.io.*;
import java.net.Socket;

import bibifi.sphericalcow.common.ATMRequest;
import bibifi.sphericalcow.common.BankResponse;
import bibifi.sphericalcow.common.SimpleLog;
import bibifi.sphericalcow.common.crypto.DataEncryptor;

public class ServerRequestHandler extends Thread {

    private DataEncryptor dataEncryptor;
	private Clearing clearing;
	
	ServerRequestHandler(DataEncryptor dataEncryptor) throws Exception  {
        this.dataEncryptor = dataEncryptor;
		this.clearing = Clearing.getInstance();
	}
	
	public void handleRequest( Socket socket ){
		
		try {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Received a connection");
            }

			// Get input and output streams
            DataInputStream in = new DataInputStream(socket.getInputStream());
            ByteArrayOutputStream baos = new ByteArrayOutputStream(256);
            
            byte[] buffer = new byte[256];
            int bytesRead;
            while ((bytesRead = in.read(buffer)) > 0) {
                baos.write(buffer, 0, bytesRead);
            }
            byte[] encryptedRequest = baos.toByteArray();
            byte[] decryptedRequest = dataEncryptor.decrypt(encryptedRequest);
            ATMRequest request = ATMRequest.fromByteArray(decryptedRequest);
            BankResponse response = clearing.executeTransaction(request);

        	DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			byte[] data = dataEncryptor.encrypt(response.asByteArray());
			out.write(data);
			out.flush();
			socket.shutdownOutput();


		} catch (Exception e) {
			SimpleLog.stdout("protocol_error");
            if (SimpleLog.isDebug()) 
                SimpleLog.log(e);
            
		} finally {
			try {
				if (SimpleLog.isDebug()) 
	                SimpleLog.log("Closing socket");
				socket.close();
			} catch (IOException e) {
                if (SimpleLog.isDebug()) {
                    SimpleLog.log(e);
                }
			}
		}
	
	}
}
