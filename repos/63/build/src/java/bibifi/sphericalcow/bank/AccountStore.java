package bibifi.sphericalcow.bank;

import java.util.HashMap;
import java.util.Map;

public final class AccountStore {

    private Map<String, Account> accountMap;

    private AccountStore() {
        this.accountMap = new HashMap<String, Account>();
    }

    private static volatile AccountStore instance = null;

    public static AccountStore getInstance() {
        if (instance == null) {
            synchronized (AccountStore.class) {
                if (instance == null) {
                    instance = new AccountStore();
                }
            }
        }

        return instance;
    }

    public synchronized void insertNewAccount(Account account) {
        if (this.accountMap.containsKey(account.getName())) {
            throw new IllegalArgumentException("Account name already exists");
        }

        this.accountMap.put(account.getName(), account);
    }

    public Account retrieveAccount(String name) {
        return this.accountMap.get(name);
    }

}