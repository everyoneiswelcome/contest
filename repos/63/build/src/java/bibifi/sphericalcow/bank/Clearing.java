package bibifi.sphericalcow.bank;

import bibifi.sphericalcow.common.ATMRequest;
import bibifi.sphericalcow.common.BankResponse;
import bibifi.sphericalcow.common.BankingTransaction;
import bibifi.sphericalcow.common.BankingTransactionType;
import bibifi.sphericalcow.common.SimpleLog;
import bibifi.sphericalcow.common.crypto.DataSigner;
import bibifi.sphericalcow.common.crypto.HMACDataSigner;

public final class Clearing {

    private AccountStore accountStore;
    private DataSigner dataSigner;

    private Clearing(AccountStore accountStore, DataSigner dataSigner) {
        this.accountStore = accountStore;
        this.dataSigner = dataSigner;
    }

    private static volatile Clearing instance = null;

    public static Clearing getInstance() {
        if (instance == null) {
            synchronized (Clearing.class) {
                if (instance == null) {
                    AccountStore accountStore = AccountStore.getInstance();
                    DataSigner dataSigner = HMACDataSigner.getInstance();
                    instance = new Clearing(accountStore, dataSigner);
                }
            }
        }

        return instance;
    }

    public BankResponse executeTransaction(ATMRequest request) {
        BankResponse response;

        try {
            BankingTransaction trans = request.getTransaction();
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Executing transaction: " + trans.toString());
            }

            Account account;
            if (trans.getTransactionType() == BankingTransactionType.CREATE_ACCOUNT) {
                account = new Account(trans.getAccountName(), trans.getAmount());
            } else {
                account = accountStore.retrieveAccount(trans.getAccountName());

                if (account == null) {
                    throw new IllegalArgumentException("Account does not exist");
                }

                if (!dataSigner.verify(trans, account.getCardSecret(), request.getSignature())) {
                    throw new IllegalArgumentException("Request signature failed verification");
                }

                if (!account.validateTimestamp(request.getTimestamp())) {
                    throw new IllegalArgumentException("Bad timestamp");
                }
            }

            switch (trans.getTransactionType()) {
                case CREATE_ACCOUNT:
                    accountStore.insertNewAccount(account);
                    response = new BankResponse(0, account.getCardSecret());
                    SimpleLog.stdout("{\"account\":\"" + account.getName() + "\",\"initial_balance\":" + trans.getAmount() + "}");
                    break;
                case DEPOSIT:
                    account.deposit(trans.getAmount());
                    response = new BankResponse(0);
                    SimpleLog.stdout("{\"account\":\"" + account.getName() + "\",\"deposit\":" + trans.getAmount() + "}");
                    break;
                case WITHDRAW:
                    account.withdraw(trans.getAmount());
                    response = new BankResponse(0);
                    SimpleLog.stdout("{\"account\":\"" + account.getName() + "\",\"withdraw\":" + trans.getAmount() + "}");
                    break;
                case BALANCE:
                    response = new BankResponse(0, account.getBalance());
                    SimpleLog.stdout("{\"account\":\"" + account.getName() + "\",\"balance\":" + account.getBalance() + "}");
                    break;
                default:
                    throw new IllegalArgumentException("Invalid transaction type");
            }
        } catch (IllegalArgumentException e) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log(e.getMessage());
            }
            response = new BankResponse(255);
        } catch (Exception e) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log(e);
            }
            response = new BankResponse(255);
        }

        return response;
    }

    public boolean isValidInstance() {
        return (accountStore != null) && (dataSigner != null);
    }

}
