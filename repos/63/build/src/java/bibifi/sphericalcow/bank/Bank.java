package bibifi.sphericalcow.bank;

import bibifi.sphericalcow.common.SimpleLog;

import java.io.File;

public class Bank {

    public static void main(String[] args) {

        BankConfig config = new BankConfig();
        if (!config.parse(args)) {
            System.exit(255);
        }

        File authFile = new File(config.getAuthFile());
        if (authFile.exists()) {
            System.exit(255);
        }

        if (SimpleLog.isDebug()) {
            SimpleLog.log("Starting server on port: " + config.getPort());
        }
        final SocketServer server = new SocketServer(config.getPort(), config.getAuthFile());

        if (server.startServer() != 0) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Server did not start, exiting");
            }
            System.exit(255);
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                if (SimpleLog.isDebug()) {
                    SimpleLog.log("Recived shutdown event, closing the bank.");
                }
                server.stopServer();
            }
        });
        // the message is required by the bank spec
        // do not change it to logging
        // https://coursera.builditbreakit.org/static/doc/fall2015coursera/spec/bank.html
        SimpleLog.stdout("created");
    }

}
