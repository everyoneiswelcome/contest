package bibifi.sphericalcow.bank;

import bibifi.sphericalcow.common.crypto.DataSigner;
import bibifi.sphericalcow.common.crypto.HMACDataSigner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.regex.Pattern;

public final class Account {

    private static final BigDecimal MINIMUM_INITIAL_BALANCE = new BigDecimal("10.00");
    private static final BigDecimal MAXIMUM_TRANSACTION_AMOUNT = new BigDecimal("4294967295.99");
    private static final long MILLI_TIME_WINDOW = 60000L;
    private static final Pattern ACCOUNT_NAME_PATTERN = Pattern.compile("^[_\\-\\.0-9a-z]{1,250}$");

    private String name;
    private BigDecimal balance;
    private byte[] cardSecret;
    private HashSet<Long> timestamps;

    public Account(String name, BigDecimal balance) throws Exception {
        if (name == null || balance == null) {
            throw new IllegalArgumentException("Null input when creating new account");
        }

        if (!ACCOUNT_NAME_PATTERN.matcher(name).matches()) {
            throw new IllegalArgumentException("Account name cannot be longer than 250 character");
        }

        if (balance.compareTo(MINIMUM_INITIAL_BALANCE) < 0) {
            throw new IllegalArgumentException("Minimum balance for new account is " + MINIMUM_INITIAL_BALANCE);
        }

        if (balance.compareTo(MAXIMUM_TRANSACTION_AMOUNT) > 0) {
            throw new IllegalArgumentException("Maximum balance for new account is " + MAXIMUM_TRANSACTION_AMOUNT);
        }

        this.name = name;
        this.balance = balance;

        DataSigner dataSigner = HMACDataSigner.getInstance();
        this.cardSecret = dataSigner.generateKey();

        this.timestamps = new HashSet();
    }

    public String getName() {
        return this.name;
    }

    public BigDecimal getBalance() {
        return this.balance;
    }

    public byte[] getCardSecret() {
        return Arrays.copyOf(this.cardSecret, this.cardSecret.length);
    }

    public synchronized BigDecimal deposit(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) != 1) {
            throw new IllegalArgumentException("Cannot deposit zero or a negative amount");
        }

        if (amount.compareTo(MAXIMUM_TRANSACTION_AMOUNT) > 0) {
            throw new IllegalArgumentException("Maximum transaction amount is " + MAXIMUM_TRANSACTION_AMOUNT);
        }

        this.balance = this.balance.add(amount);
        return this.balance;
    }

    public synchronized BigDecimal withdraw(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.ZERO) != 1) {
            throw new IllegalArgumentException("Cannot withdraw zero or a negative amount");
        }

        if (amount.compareTo(MAXIMUM_TRANSACTION_AMOUNT) > 0) {
            throw new IllegalArgumentException("Maximum transaction amount is " + MAXIMUM_TRANSACTION_AMOUNT);
        }

        if (balance.compareTo(amount) < 0) {
            throw new IllegalArgumentException("Account balance insufficient for withdrawal");
        }

        this.balance = this.balance.subtract(amount);
        return this.balance;
    }

    public synchronized boolean validateTimestamp(long timestamp) {
        if (timestamp < new Date().getTime() - MILLI_TIME_WINDOW) {
            return false;
        }
        else if (this.timestamps.contains(timestamp)) {
            return false;
        } else {
            return true;
        }
    }
}
