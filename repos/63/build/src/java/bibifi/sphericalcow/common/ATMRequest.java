package bibifi.sphericalcow.common;

import bibifi.sphericalcow.common.crypto.DataSigner;
import bibifi.sphericalcow.common.crypto.HMACDataSigner;
import org.bouncycastle.util.encoders.Base64;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Date;

public class ATMRequest {

    private static final String STRING_ENCODING = "US-ASCII";

    private BankingTransaction transaction;
    private byte[] signature;
    private Date timestamp;

    private DataSigner transactionSigner;
    private String string = null;

    public ATMRequest(BankingTransaction transaction, String cardFile) {
        this.transaction = transaction;
        this.transactionSigner = HMACDataSigner.getInstance();
        if (cardFile != null) {
            this.signature = transactionSigner.sign(transaction, cardFile);
        }
        this.timestamp = new Date();
    }

    private ATMRequest(BankingTransaction transaction, byte[] signature, Date timestamp) {
        this.transaction = transaction;
        if (signature == null) {
            this.signature = null;
        } else {
            this.signature = Arrays.copyOf(signature, signature.length);
        }
        this.timestamp = timestamp;
        this.transactionSigner = HMACDataSigner.getInstance();
    }

    public BankingTransaction getTransaction() {
        return transaction;
    }

    public byte[] getSignature() {
        return Arrays.copyOf(this.signature, this.signature.length);
    }

    public long getTimestamp() {
        return this.timestamp.getTime();
    }

    public static ATMRequest fromByteArray(byte[] array) throws Exception {
        String[] tokens = new String(array).split("\\|");
        String[] tokens1 = tokens[0].split("\\;");
        byte[] signature;
        if ("null".equals(tokens1[0])) {
            signature = null;
        } else {
            signature = Base64.decode(tokens1[0]);
        }
        Date timestamp = new Date(Long.parseLong(tokens1[1]));
        BankingTransaction transaction = BankingTransaction.fromString(tokens[1]);
        return new ATMRequest(transaction, signature, timestamp);
    }

    public byte[] asByteArray() throws UnsupportedEncodingException {
        if (this.string == null) {
            StringBuilder sb = new StringBuilder();
            if (signature != null) {
                sb.append(new String(Base64.encode(this.signature), STRING_ENCODING));
            } else {
                sb.append("null");
            }
            sb.append(";");
            sb.append(String.valueOf(this.timestamp.getTime()));
            sb.append("|");
            sb.append(this.transaction.asString());
            this.string = sb.toString();
        }

        return this.string.getBytes(STRING_ENCODING);
    }

}
