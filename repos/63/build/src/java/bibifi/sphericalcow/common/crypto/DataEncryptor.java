package bibifi.sphericalcow.common.crypto;

public interface DataEncryptor {

    byte[] encrypt(byte[] data);

    byte[] decrypt(byte[] data);
}
