package bibifi.sphericalcow.common.crypto;

import java.io.File;
import java.io.IOException;

public class DummyDataEncryptor implements DataEncryptor {

    private static volatile DummyDataEncryptor instance;

    private DummyDataEncryptor() {}

    public static DummyDataEncryptor getInstance(String keyFile, boolean generateKey) {
        if (instance == null) {
            synchronized (DummyDataEncryptor.class) {
                if (instance == null) {
                    if (generateKey) {
                        File dummyAuthFile = new File(keyFile);
                        try {
                            dummyAuthFile.createNewFile();
                        } catch (IOException ex) {}
                    }
                    instance = new DummyDataEncryptor();
                }
            }
        }

        return instance;
    }

    public byte[] encrypt(byte[] data) {
        return data;
    }

    public byte[] decrypt(byte[] data) {
        return data;
    }
}
