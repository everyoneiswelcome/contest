package bibifi.sphericalcow.common;

import java.io.*;

public final class FileHandler {

    private FileHandler() {}

	public static byte[] loadBinaryFile(String fileName, int maxFileSize) {

		File file = new File(fileName);
        InputStream is = null;
		byte[] fileContent = null;

		if (file.length() > maxFileSize) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log(fileName + " is larger than expected");
            }
			return null;
		}

		try {
            is = new FileInputStream(file);
            int bytesToRead = (int) file.length();
            fileContent = new byte[bytesToRead];
            is.read(fileContent);
        } catch (IOException ex) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Cannot read " + fileName);
            }
            return null;
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (IOException ex) {}
		}

		return fileContent;
	}

	public static void saveBinaryFile(String fileName, byte[] content) {

        File file = new File(fileName);
		OutputStream os = null;

		try {
            os = new FileOutputStream(file);
            os.write(content);
            os.flush();
        } catch (IOException ex) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Cannot save " + fileName);
            }
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException ex) {}
			}
		}
	}

}
