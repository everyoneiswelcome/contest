package bibifi.sphericalcow.common.crypto;

import bibifi.sphericalcow.common.BankingTransaction;

public interface DataSigner {

    byte[] generateKey();

    byte[] sign(BankingTransaction transaction, String cardFile);

    boolean verify(BankingTransaction transaction, byte[] key, byte[] signature);

}
