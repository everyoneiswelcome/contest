package bibifi.sphericalcow.common;

public final class SimpleLog {
	
	private static final boolean DEBUG = false;

    private SimpleLog() {}

	public static boolean isDebug() {
		return DEBUG;
	}
	
	public static void log(Throwable trace) {
        System.err.println("[SIMPLE LOG]");
        trace.printStackTrace(System.err);
	}

	public static void log(String msg) {
        System.err.println("[SIMPLE LOG] " + msg);
	}

	public static void stdout(String msg) {
		System.out.println(msg);
		System.out.flush();
	}

}
