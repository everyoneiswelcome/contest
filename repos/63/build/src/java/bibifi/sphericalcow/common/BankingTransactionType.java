package bibifi.sphericalcow.common;

public enum BankingTransactionType {

    CREATE_ACCOUNT (1),
    DEPOSIT (2),
    WITHDRAW (3),
    BALANCE (4);
    
    private final int type;
    
    BankingTransactionType(int t) {
    	this.type = t;
    }

    public int getType() {
    	return this.type;
    }

}
