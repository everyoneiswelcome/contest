package bibifi.sphericalcow.common;

import org.bouncycastle.util.encoders.Base64;

import java.math.BigDecimal;
import java.util.Arrays;

public class BankResponse {

    private static final String STRING_ENCODING = "US-ASCII";

    private int errorCode;
    private byte[] accountKey;
    private BigDecimal balance;

    private String string = null;

    public BankResponse(int errorCode) {
        this(errorCode, null, null);
    }

    public BankResponse(int errorCode, byte[] accountKey) {
        this(errorCode, accountKey, null);
    }

    public BankResponse(int errorCode, BigDecimal balance) {
        this(errorCode, null, balance);
    }

    private BankResponse(int errorCode, byte[] accountKey, BigDecimal balance) {
        this.errorCode = errorCode;
        if (accountKey != null) {
            this.accountKey = Arrays.copyOf(accountKey, accountKey.length);
        }
        this.balance = balance;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public byte[] getAccountKey() {
        return Arrays.copyOf(this.accountKey, this.accountKey.length);
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public static BankResponse fromByteArray(byte[] array) {
        if (array == null || array.length == 0) {
            return new BankResponse(63, null, null);
        }

        String[] tokens = new String(array).split("\\;");
        int errorCode = Integer.parseInt(tokens[0]);
        byte[] accountKey;
        if ("null".equals(tokens[1])) {
            accountKey = null;
        } else {
            accountKey = Base64.decode(tokens[1]);
        }
        BigDecimal balance;
        if ("null".equals(tokens[2])) {
            balance = null;
        } else {
            balance = new BigDecimal(tokens[2]);
        }
        return new BankResponse(errorCode, accountKey, balance);
    }

    public byte[] asByteArray() throws Exception {
        if (this.string == null) {
            StringBuilder sb = new StringBuilder();
            sb.append(String.valueOf(this.errorCode));
            sb.append(";");
            if (this.accountKey == null) {
                sb.append("null");
            } else {
                sb.append(new String(Base64.encode(this.accountKey), STRING_ENCODING));
            }
            sb.append(";");
            if (this.balance != null) {
                sb.append(this.balance);
            } else {
                sb.append("null");
            }
            this.string = sb.toString();
        }

        return this.string.getBytes(STRING_ENCODING);
    }

}
