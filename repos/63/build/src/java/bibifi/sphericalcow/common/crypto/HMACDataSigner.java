package bibifi.sphericalcow.common.crypto;

import bibifi.sphericalcow.common.BankingTransaction;
import bibifi.sphericalcow.common.FileHandler;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * Signs and validates data represented as byte array using HMACSHA256.
 * Secret key is taken from the given file.
 */
public final class HMACDataSigner implements DataSigner {

    private static final String MAC_ALGO = "HmacSHA256";
    private static final int KEY_LENGTH = 256;
    private static final String STRING_CHARSET = "US-ASCII";

    private static volatile HMACDataSigner instance = null;

    private Mac mac;

    private HMACDataSigner(Mac mac) {
        this.mac = mac;
    }

    public static HMACDataSigner getInstance() {
        if (instance == null) {
            synchronized (HMACDataSigner.class) {
                if (instance == null) {
                    try {
                        Mac mac = Mac.getInstance(MAC_ALGO);
                        instance = new HMACDataSigner(mac);
                    } catch (NoSuchAlgorithmException ex) {
                        instance = new HMACDataSigner(null);
                    }
                }
            }
        }

        return instance;
    }

    public byte[] generateKey() {
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance(MAC_ALGO);
            keyGen.init(KEY_LENGTH);
            return keyGen.generateKey().getEncoded();
        } catch (NoSuchAlgorithmException ex) {
            return null;
        }
    }

    public byte[] sign(BankingTransaction transaction, String cardFile) {
        byte[] key = FileHandler.loadBinaryFile(cardFile, KEY_LENGTH / 8);
        return sign(transaction, key);
    }

    private byte[] sign(BankingTransaction transaction, byte[] key) {
        try {
            mac.init(new SecretKeySpec(key, MAC_ALGO));
            return mac.doFinal(transaction.asString().getBytes(STRING_CHARSET));
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean verify(BankingTransaction transaction, byte[] key, byte[] signature) {
        byte[] realSignature = sign(transaction, key);
        return Arrays.equals(realSignature, signature);
    }

    public boolean isValidInstance() {
        return this.mac != null;
    }

}
