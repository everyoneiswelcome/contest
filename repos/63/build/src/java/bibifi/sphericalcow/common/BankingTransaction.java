package bibifi.sphericalcow.common;

import java.math.BigDecimal;

public final class BankingTransaction {

    private static final String ATTRIBUTE_INITIAL_BALANCE = "initial_balance";
    private static final String ATTRIBUTE_DEPOSIT = "deposit";
    private static final String ATTRIBUTE_WITHDRAW = "withdraw";
    private static final String ATTRIBUTE_BALANCE = "balance";

    private String accountName;
    private BankingTransactionType transactionType;
    private BigDecimal amount;

    public BankingTransaction(String accountName, BankingTransactionType transactionType, String amount) {
        this.accountName = accountName;
        this.transactionType = transactionType;
        if (amount == null) {
            this.amount = null;
        } else {
            this.amount = new BigDecimal(amount);
        }
    }

    public String getAccountName() {
        return accountName;
    }

    public BankingTransactionType getTransactionType() {
        return transactionType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String asString() {
        StringBuilder sb = new StringBuilder();
        sb.append(accountName);
        sb.append(";");
        if (BankingTransactionType.CREATE_ACCOUNT.equals(transactionType)) {
            sb.append(ATTRIBUTE_INITIAL_BALANCE);
            sb.append(";");
            sb.append(amount);
        } else if (BankingTransactionType.DEPOSIT.equals(transactionType)) {
            sb.append(ATTRIBUTE_DEPOSIT);
            sb.append(";");
            sb.append(amount);
        } else if (BankingTransactionType.WITHDRAW.equals(transactionType)) {
            sb.append(ATTRIBUTE_WITHDRAW);
            sb.append(";");
            sb.append(amount);
        } else if (BankingTransactionType.BALANCE.equals(transactionType)) {
            sb.append(ATTRIBUTE_BALANCE);
            sb.append(";");
            sb.append("null");
        }

        return sb.toString();
    }

    public static BankingTransaction fromString(String string) {
        String[] tokens = string.split("\\;");
        String accountName = tokens[0];
        BankingTransactionType transactionType = null;
        String transTypeStr = tokens[1];
        String amount = null;
        if (ATTRIBUTE_INITIAL_BALANCE.equals(transTypeStr)) {
            transactionType = BankingTransactionType.CREATE_ACCOUNT;
            amount = tokens[2];
        } else if (ATTRIBUTE_DEPOSIT.equals(transTypeStr)) {
            transactionType = BankingTransactionType.DEPOSIT;
            amount = tokens[2];
        } else if (ATTRIBUTE_WITHDRAW.equals(transTypeStr)) {
            transactionType = BankingTransactionType.WITHDRAW;
            amount = tokens[2];
        } else if (ATTRIBUTE_BALANCE.equals(transTypeStr)) {
            transactionType = BankingTransactionType.BALANCE;
        }

        return new BankingTransaction(accountName, transactionType, amount);
    }

}
