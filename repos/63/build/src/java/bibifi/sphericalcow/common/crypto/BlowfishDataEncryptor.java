package bibifi.sphericalcow.common.crypto;

import bibifi.sphericalcow.common.FileHandler;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;

/**
 * Encrypts and decrypts data represented as byte array using Blowfish.
 * Secret key is taken from the given file.
 */
public final class BlowfishDataEncryptor implements DataEncryptor {

    private static final String CIPHER_ALGO = "Blowfish";
    private static final int KEY_LENGTH = 128;

    private static volatile BlowfishDataEncryptor instance;

    private Cipher cipher;
    private SecretKey secretKey;

    private BlowfishDataEncryptor(Cipher cipher, SecretKey secretKey) {
        this.cipher = cipher;
        this.secretKey = secretKey;
    }

    public static BlowfishDataEncryptor getInstance(String keyFile, boolean generateKey) {
        if (instance == null) {
            synchronized (BlowfishDataEncryptor.class) {
                if (instance == null) {
                    try {
                        Cipher cipher = Cipher.getInstance(CIPHER_ALGO);

                        SecretKey secretKey;
                        if (generateKey) {
                            secretKey = generateKey();
                            FileHandler.saveBinaryFile(keyFile, secretKey.getEncoded());
                        } else {
                            byte[] secretKeyBytes = FileHandler.loadBinaryFile(keyFile, KEY_LENGTH / 8);
                            secretKey = new SecretKeySpec(secretKeyBytes, CIPHER_ALGO);
                        }

                        instance = new BlowfishDataEncryptor(cipher, secretKey);
                    } catch (Exception ex) {
                        instance = new BlowfishDataEncryptor(null, null);
                    }
                }
            }
        }

        return instance;
    }
    
    private static SecretKey generateKey() {
        try {
            KeyGenerator keyGen = KeyGenerator.getInstance(CIPHER_ALGO);
            keyGen.init(KEY_LENGTH);
            return keyGen.generateKey();
        } catch (NoSuchAlgorithmException ex) {
            return null;
        }
    }

    public byte[] encrypt(byte[] data) {
        try {
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return cipher.doFinal(data);
        } catch (Exception ex) {
            return null;
        }
    }

    public byte[] decrypt(byte[] data) {
        try {
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return cipher.doFinal(data);
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean isValidInstance() {
        return (this.cipher != null) && (this.secretKey != null);
    }

}
