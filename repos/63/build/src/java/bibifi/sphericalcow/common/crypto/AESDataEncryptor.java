package bibifi.sphericalcow.common.crypto;

import bibifi.sphericalcow.common.FileHandler;
import bibifi.sphericalcow.common.SimpleLog;

import org.bouncycastle.util.Arrays;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.SecureRandom;

/**
 * Encrypts and decrypts data represented as byte array using AES128.
 * Secret key is taken from the given file.
 */
public final class AESDataEncryptor implements DataEncryptor {

    // private static final String PRNG_ALGO = "SHA1PRNG";
    // private static final String PRNG_PROVIDER = "";

    private static final String CIPHER_ALGO = "AES";
    private static final String BLOCK_MODE = "CBC";
    private static final String PADDING = "PKCS5Padding";
    private static final int KEY_LENGTH = 128;

    private static volatile AESDataEncryptor instance;

    private Cipher cipher;
    private SecretKey secretKey;

    private AESDataEncryptor(Cipher cipher, SecretKey secretKey) {
        this.cipher = cipher;
        this.secretKey = secretKey;
    }

    public static AESDataEncryptor getInstance(String keyFile, boolean generateKey) throws Exception {
        if (instance == null) {
            synchronized (AESDataEncryptor.class) {
                if (instance == null) {
                    Cipher cipher = Cipher.getInstance(CIPHER_ALGO + "/" + BLOCK_MODE + "/" + PADDING);

                    SecretKey secretKey;
                    if (generateKey) {
                        secretKey = generateKey();
                        FileHandler.saveBinaryFile(keyFile, secretKey.getEncoded());
                    } else {
                        byte[] secretKeyBytes = FileHandler.loadBinaryFile(keyFile, KEY_LENGTH / 8);
                        secretKey = new SecretKeySpec(secretKeyBytes, CIPHER_ALGO);
                    }

                    instance = new AESDataEncryptor(cipher, secretKey);
                }
            }
        }

        return instance;
    }
    
    private static SecretKey generateKey() throws Exception {
        KeyGenerator keyGen = KeyGenerator.getInstance(CIPHER_ALGO);
        keyGen.init(KEY_LENGTH);
        return keyGen.generateKey();
    }

    private byte[] generateIv(Cipher cipher) throws Exception {
        byte[] iv = new byte[cipher.getBlockSize()];
        SecureRandom secureRandom = new SecureRandom();
        secureRandom.nextBytes(iv);
        return iv;
    }

    public byte[] encrypt(byte[] data)  {

        byte[] encrypted;
		try {
			byte[] iv = generateIv(cipher);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv));
			encrypted = cipher.doFinal(data);
			return Arrays.concatenate(iv, encrypted);
		} catch (Exception e) {
			SimpleLog.log(e);
		}
        return null;
    }

    public byte[] decrypt(byte[] data)  {
        byte[] iv = Arrays.copyOf(data, cipher.getBlockSize());
        try {
			cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(iv));
			byte[] encryptedData = Arrays.copyOfRange(data, cipher.getBlockSize(), data.length);
			return cipher.doFinal(encryptedData);
		} catch (Exception e) {
			SimpleLog.log(e);
		}
       return null;
    }

}
