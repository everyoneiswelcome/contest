package bibifi.sphericalcow.common.crypto;

import bibifi.sphericalcow.common.BankingTransaction;
import bibifi.sphericalcow.common.FileHandler;

import java.security.SecureRandom;
import java.util.Arrays;

public class DummyDataSigner implements DataSigner {

    private static final int KEY_LENGTH = 128;

    private static volatile DummyDataSigner instance = null;

    private DummyDataSigner() {}

    public static DummyDataSigner getInstance() {
        if (instance == null) {
            synchronized (DummyDataSigner.class) {
                if (instance == null) {
                    instance = new DummyDataSigner();
                }
            }
        }

        return instance;
    }

    public byte[] generateKey() {
        SecureRandom secureRandom = new SecureRandom();
        byte key[] = new byte[KEY_LENGTH / 8];
        secureRandom.nextBytes(key);
        return key;
    }

    public byte[] sign(BankingTransaction transaction, String cardFile) {
        byte[] key = FileHandler.loadBinaryFile(cardFile, KEY_LENGTH / 8);
        return key;
    }

    public boolean verify(BankingTransaction transaction, byte[] key, byte[] signature) {
        return Arrays.equals(key, signature);
    }

}
