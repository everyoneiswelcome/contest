package bibifi.sphericalcow.atm;

import bibifi.sphericalcow.common.BankingTransactionType;
import bibifi.sphericalcow.common.SimpleLog;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.ParseException;

public class ATMConfig {

    private static final String DEFAULT_SERVER_PORT = "3000";
    private static final String DEFAULT_IP_ADDRESS = "127.0.0.1";
    private static final String DEFAULT_AUTH_FILE = "bank.auth";
    private static final Pattern FILENAME_PATTERN = Pattern.compile("^[_\\-\\.0-9a-z]{1,255}$");
    private static final Pattern MONEY_PATTERN = Pattern.compile("^((0|[1-9]\\d*)\\.\\d{2}){1,4096}$");
    private static final Pattern DOT_NAME_PATTERN = Pattern.compile("^\\.{1,2}$");

    private Options cliOptions;
    private int port;
    private String ipAddress;
    private String authFile;
    private String account;
    private String cardFile;
    private BankingTransactionType transactionType = null;
    private String transactionAmount = null;

    public ATMConfig() {
        this.cliOptions = new Options()
                .addOption("i", true, "IP Address")
                .addOption("p", true, "Port")
                .addOption("s", true, "Auth file")
                .addOption("c", true, "Card file")
                .addOption("a", true, "Account (required)")
                .addOption("n", true, "New account balance")
                .addOption("d", true, "Deposit amount")
                .addOption("w", true, "Withdraw amount")
                .addOption("g", false, "Current balance");
    }

    public boolean parse(String[] args) {
        CommandLine cliArgs = null;

        try {
            cliArgs = new PosixParser().parse(cliOptions, args, true);
        } catch (ParseException e) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log(e);
            }
            return false;
        }

        // No extra arguments given
        if (cliArgs.getArgList().size() > 0) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Unrecognized argument given");
            }
            return false;
        }

        // No duplicate arguments
        for (Option option : (Collection<Option>) cliOptions.getOptions()) {
            if (cliArgs.getOptionValues(option.getOpt()) != null && cliArgs.getOptionValues(option.getOpt()).length > 1) {
                if (SimpleLog.isDebug()) {
                    SimpleLog.log("Duplicate argument given");
                }
                return false;
            }
        }

        // No duplicate -g flags, commons cli is too smart for previous check to catch it...
        boolean g = false;
        for (String arg : args) {
            if (arg.equals("-g")) {
                if (g) {
                    if (SimpleLog.isDebug()) {
                        SimpleLog.log("Duplicate argument given");
                    }
                    return false;
                }
                g = true;
            }
        }

        // No negative numbers. Looks like commons cli sometimes ignores - sign. Then it stores an invalid negative number as valid positive value.
        for (String arg : args) {
            if (arg.matches("^\\-[^ipscandwg]+.*$")) {
                if (SimpleLog.isDebug()) {
                    SimpleLog.log("Negative number or unknown option found");
                }
                return false;
            }
        }

        // Required params
        if (!cliArgs.hasOption('a')) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Account is required");
            }
            return false;
        }

        // Check formatting
        if (
            cliArgs.getOptionValue('p') != null && !cliArgs.getOptionValue('p').matches("^([1-9]\\d*){1,4096}$") ||
            cliArgs.getOptionValue('s') != null && !FILENAME_PATTERN.matcher(cliArgs.getOptionValue('s')).matches() ||
            cliArgs.getOptionValue('i') != null && !cliArgs.getOptionValue('i').matches("^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$") ||
            cliArgs.getOptionValue('c') != null && !FILENAME_PATTERN.matcher(cliArgs.getOptionValue('c')).matches() ||
            cliArgs.getOptionValue('a') != null && !FILENAME_PATTERN.matcher(cliArgs.getOptionValue('a')).matches() && cliArgs.getOptionValue('a').length() <= 250 ||
            cliArgs.getOptionValue('n') != null && !MONEY_PATTERN.matcher(cliArgs.getOptionValue('n')).matches() ||
            cliArgs.getOptionValue('d') != null && !MONEY_PATTERN.matcher(cliArgs.getOptionValue('d')).matches() ||
            cliArgs.getOptionValue('w') != null && !MONEY_PATTERN.matcher(cliArgs.getOptionValue('w')).matches()
        ) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Incorrectly formatted argument given");
            }
            return false;
        }

        // Invalid filenames . & ..
        if (cliArgs.getOptionValue('s') != null && DOT_NAME_PATTERN.matcher(cliArgs.getOptionValue('s')).matches() ||
                cliArgs.getOptionValue('c') != null && DOT_NAME_PATTERN.matcher(cliArgs.getOptionValue('c')).matches()) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Invalid file name");
            }
            return false;
        }

        this.port = Integer.parseInt(cliArgs.getOptionValue('p', DEFAULT_SERVER_PORT));
        this.ipAddress = cliArgs.getOptionValue('i', DEFAULT_IP_ADDRESS);
        this.authFile = cliArgs.getOptionValue('s', DEFAULT_AUTH_FILE);
        this.account = cliArgs.getOptionValue('a');
        this.cardFile = cliArgs.getOptionValue('c', account + ".card");

        if (port < 1024 || port > 65535) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Port must be 1024-65535");
            }
            return false;
        }

        // Validate IP, already have checked that is is in the format ###.###.###.###
        String[] parts = this.ipAddress.split("\\.");
        for (String part : parts) {
            int value = Integer.parseInt(part);
            if (value < 0 || value > 255) {
                if (SimpleLog.isDebug()) {
                    SimpleLog.log("Invalid IP address");
                }
                return false;
            }
        }

        // Determine transaction type and amount
        char[] transactionFlags = {'n', 'd', 'w', 'g'};
        for (char op : transactionFlags) {
            if (cliArgs.hasOption(op)) {
                // Validate only one transaction flag given
                if (this.transactionType != null) {
                    if (SimpleLog.isDebug()) {
                        SimpleLog.log("Only one transaction per invocation");
                    }
                    return false;
                } else {
                    switch (op) {
                        case 'n':
                            this.transactionType = BankingTransactionType.CREATE_ACCOUNT;
                            this.transactionAmount = cliArgs.getOptionValue(op);
                            break;
                        case 'd':
                            this.transactionType = BankingTransactionType.DEPOSIT;
                            this.transactionAmount = cliArgs.getOptionValue(op);
                            break;
                        case 'w':
                            this.transactionType = BankingTransactionType.WITHDRAW;
                            this.transactionAmount = cliArgs.getOptionValue(op);
                            break;
                        case 'g':
                            this.transactionType = BankingTransactionType.BALANCE;
                            break;
                    }

                    if (this.transactionAmount != null) {
                        try {
                            BigDecimal amount = new BigDecimal(this.transactionAmount);

                            if (amount.compareTo(BigDecimal.ZERO) <= 0 || amount.compareTo(new BigDecimal(4294967296.00)) >= 0) {
                                if (SimpleLog.isDebug()) {
                                    SimpleLog.log("Transaction amount must be greater than 0 or less than 4294967296");
                                }
                                return false;
                            }
                        } catch (NumberFormatException ex) {
                            if (SimpleLog.isDebug()) {
                                SimpleLog.log("Transaction amount must be greater than 0 or less than 4294967296");
                            }
                            return false;
                        }
                    }
                }
            }
        }

        // Check transaction was specified
        if (this.transactionType == null) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("No transaction flag given");
            }
            return false;
        }

        return true;
    }

    public int getPort() {
        return this.port;
    }

    public String getIPAddress() {
        return this.ipAddress;
    }

    public String getAuthFile() {
        return this.authFile;
    }

    public String getCardFile() {
        return this.cardFile;
    }

    public String getAccount() {
        return this.account;
    }

    public BankingTransactionType getTransactionType() {
        return this.transactionType;
    }

    public String getTransactionAmount() {
        return this.transactionAmount;
    }
}
