package bibifi.sphericalcow.atm;

import bibifi.sphericalcow.common.*;

import java.io.File;

public class ATM {

    public static void main(String[] args) {

        ATMConfig config = new ATMConfig();
        if (!config.parse(args)) {
            System.exit(255);
        }

        File authFile = new File(config.getAuthFile());
        if (!authFile.exists() || !authFile.canRead()) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log("Cannot load auth file: " + config.getAuthFile());
            }
            System.exit(255);
        }

        if (config.getTransactionType() == BankingTransactionType.CREATE_ACCOUNT) {
            // card should not exist for existing account
            File cardFile = new File(config.getCardFile());
            if (cardFile.exists()) {
                System.exit(255);
            }
        } else {
            // load card if the operation is not to create new account
            File cardFile = new File(config.getCardFile());
            if (!cardFile.exists() || !cardFile.canRead()) {
                if (SimpleLog.isDebug()) {
                    SimpleLog.log("Cannot load card file: " + config.getCardFile());
                }
                System.exit(255);
            }
        }

        SocketATM socket = null;
        try {
            socket = new SocketATM(config.getIPAddress(), config.getPort(), config.getAuthFile());
        } catch (Exception ex) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log(ex);
            }
            System.exit(255);
        }

        int exitcode = 0;
        switch (config.getTransactionType()) {
            case CREATE_ACCOUNT:
                exitcode = doCreateAccount(socket, config);
                break;
            case DEPOSIT:
                exitcode = doDeposit(socket, config, config.getCardFile());
                break;
            case WITHDRAW:
                exitcode = doWithdraw(socket, config, config.getCardFile());
                break;
            case BALANCE:
                exitcode = doBalanceCheck(socket, config, config.getCardFile());
                break;
            default:
                if (SimpleLog.isDebug()) {
                    SimpleLog.log("Unknown operation");
                }
                exitcode = 255;
        }

        System.exit(exitcode);
    }

    /**
     * -n <balance> Create a new account with the given balance. The account
     * must be unique (ie, the account must not already exist). The balance must
     * be greater than or equal to 10.00. The given card file must not already
     * exist. If any of these conditions do not hold, atm exits with a return
     * code of 255.
     *
     * On success, both atm and bank print the account and initial
     * balance to standard output, encoded as JSON. The account name is a JSON
     * string with key "account", and the initial balance is a JSON number with
     * key "initial_balance" (Example: {"account":"55555","initial_balance":10.00}).
     * In addition, atm creates the card file for the new account (think of this as like an auto-generated pin).
     */
    private static int doCreateAccount(SocketATM socket, ATMConfig config) {
        int exitCode = 0;

        try {
            BankingTransaction transaction = new BankingTransaction(config.getAccount(), config.getTransactionType(), config.getTransactionAmount());
            ATMRequest request = new ATMRequest(transaction, null);
            BankResponse response = socket.sendAndReceive(request);
            if (response != null) {
                exitCode = response.getErrorCode();
            } else {
                exitCode = 63; // if we get no response from server it either a timeout or other protocol error
            }

            if (exitCode == 0) {
                FileHandler.saveBinaryFile(config.getCardFile(), response.getAccountKey());
                SimpleLog.stdout("{\"account\":\"" + config.getAccount() + "\",\"initial_balance\":" + config.getTransactionAmount() + "}");
            }
        } catch (Exception ex) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log(ex);
            }
            exitCode = 255;
        }

        return exitCode;
    }

    /**
     * -d <amount> Deposit the amount of money specified. The amount must be greater than 0.00.
     * The specified account must exist, and the card file must be associated with the given account
     * (i.e., it must be the same file produced by atm when the account was created).
     * If any of these conditions do not hold, atm exits with a return code of 255.
     * On success, both atm and bank print the account and deposit amount to standard output,
     * encoded as JSON. The account name is a JSON string with key "account", and the deposit
     * amount is a JSON number with key "deposit" (Example: {"account":"55555","deposit":20.00}).
     */

    private static int doDeposit(SocketATM socket, ATMConfig config, String cardFile) {
        int exitCode = 0;

        try {
            BankingTransaction transaction = new BankingTransaction(config.getAccount(), config.getTransactionType(), config.getTransactionAmount());
            ATMRequest request = new ATMRequest(transaction, cardFile);
            BankResponse response = socket.sendAndReceive(request);

            if (response != null) {
                exitCode = response.getErrorCode();
            } else {
                exitCode = 63; // if we get no response from server it either a timeout or other protocol error
            }

            if (exitCode == 0) {
                SimpleLog.stdout("{\"account\":\"" + config.getAccount() + "\",\"deposit\":" + config.getTransactionAmount() + "}");
            }
        } catch (Exception ex) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log(ex);
            }
            exitCode = 255;
        }

        return exitCode;
    }

    /**
     * -w <amount> Withdraw the amount of money specified. The amount must be greater than 0.00,
     * and the remaining balance must be nonnegative. The card file must be associated with the
     * specified account (i.e., it must be the same file produced by atm when the account was created).
     * The ATM exits with a return code of 255 if any of these conditions are not true.
     * On success, both atm and bank print the account and withdraw amount to standard output, encoded as JSON.
     * The account name is a JSON string with key "account", and the withdraw amount is a JSON number
     * with key "withdraw" (Example: {"account":"55555","withdraw":15.00}).
     */

    private static int doWithdraw(SocketATM socket, ATMConfig config,  String cardFile) {
        int exitCode = 0;

        try {
            BankingTransaction transaction = new BankingTransaction(config.getAccount(), config.getTransactionType(), config.getTransactionAmount());
            ATMRequest request = new ATMRequest(transaction, cardFile);
            BankResponse response = socket.sendAndReceive(request);

            if (response != null) {
                exitCode = response.getErrorCode();
            } else {
                exitCode = 63; // if we get no response from server it either a timeout or other protocol error
            }

            if (exitCode == 0) {
                SimpleLog.stdout("{\"account\":\"" + config.getAccount() + "\",\"withdraw\":" + config.getTransactionAmount() + "}");
            }
        } catch (Exception ex) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log(ex);
            }
            exitCode = 255;
        }

        return exitCode;
    }

    /**
     * -g Get the current balance of the account. The specified account must exist, and the card
     *  file must be associated with the account. Otherwise, atm exits with a return code of 255.
     *  On success, both atm and bank print the account and balance to stdout, encoded as JSON.
     *  The account name is a JSON string with key "account", and the balance is a JSON number
     *  with key "balance" (Example: {"account":"55555","balance":43.63}).
     */

    private static int doBalanceCheck(SocketATM socket, ATMConfig config,  String cardFile) {
        int exitCode = 0;

        try {
            BankingTransaction transaction = new BankingTransaction(config.getAccount(), config.getTransactionType(), config.getTransactionAmount());
            ATMRequest request = new ATMRequest(transaction, cardFile);
            BankResponse response = socket.sendAndReceive(request);
            if (response != null) {
                exitCode = response.getErrorCode();
            } else {
                exitCode = 63; // if we get no response from server it either a timeout or other protocol error
            }

            if (exitCode == 0) {
                SimpleLog.stdout("{\"account\":\"" + config.getAccount() + "\",\"balance\":" + response.getBalance() + "}");
            }
        } catch (Exception ex) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log(ex);
            }
            exitCode = 255;
        }

        return exitCode;
    }

}
