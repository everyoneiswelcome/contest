package bibifi.sphericalcow.atm;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;

import bibifi.sphericalcow.common.ATMRequest;
import bibifi.sphericalcow.common.BankResponse;
import bibifi.sphericalcow.common.SimpleLog;
import bibifi.sphericalcow.common.crypto.BlowfishDataEncryptor;

public class SocketATM {

    private static final int TIMEOUT = 10000; // 10 seconds timeout for a socket as per spec

	private String ip;
	private int port;
	private BlowfishDataEncryptor dataEncryptor;

	public SocketATM(String ip, int port, String authFile) throws Exception {
		this.port = port;
		this.ip = ip;
        this.dataEncryptor = BlowfishDataEncryptor.getInstance(authFile, false);
	}

	/**
	 * Sends and receives message to and from the bank
	 */
	public BankResponse sendAndReceive(ATMRequest request) {
		BankResponse result = null;
		Socket socket = null;

		try {

			socket = new Socket();
			socket.setPerformancePreferences(0, 1, 2);
			socket.setTcpNoDelay(true);
			socket.setReceiveBufferSize(1024);
			socket.connect(new InetSocketAddress(ip, port), TIMEOUT);
			
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			byte[] data = dataEncryptor.encrypt(request.asByteArray());
			
			out.write(data);
			out.flush();
			socket.shutdownOutput();
			
			DataInputStream in = new DataInputStream(socket.getInputStream());
            ByteArrayOutputStream baos = new ByteArrayOutputStream(256);

			byte[] buffer = new byte[256];
            int bytesRead;
            while ((bytesRead = in.read(buffer)) > 0) {
				baos.write(buffer, 0, bytesRead);
			}
            byte[] encryptedResponse = baos.toByteArray();
            byte[] decryptedResponse = dataEncryptor.decrypt(encryptedResponse);
			result = BankResponse.fromByteArray(decryptedResponse);
			
 		} catch (Exception ex) {
            if (SimpleLog.isDebug()) {
                SimpleLog.log(ex);
            }
		} finally {
			if (socket != null) {
				try {
					socket.close();
				} catch (IOException ex) {
                    if (SimpleLog.isDebug()) {
                        SimpleLog.log(ex);
                    }
				}
			}
		}

		return result;
	}

}
