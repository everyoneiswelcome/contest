package bank;

import java.math.BigInteger;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import atm.Atm;

import utils.Money;
import utils.Parsers;

/***
 * Holds account data. Only valid key will permit access.
 * Thread safe?
 * @author wiselion
 *
 */
public class Account {
	public final String name; //Account name
	public final byte [] key;//Immutable
	private Money balance; //Account balance
	private Lock lock;
	
	private Account(String name, byte [] key, Money initialValue){
		this.name = name;
		this.key = key;
		this.balance = new Money(initialValue);
		lock = new ReentrantLock();
	}
	
	public static Account from(String name, Money initialValue){
		//Validate
		if(Parsers.validAccountName(name)&&initialValue.greaterThanOrEqual(Money.MIN_ACCOUNT)){
			Random r = new Random(); //TODO: speed?
			byte [] accKey = new byte[Atm.CARDLENGTH];
			r.nextBytes(accKey);
			return new Account(name, accKey,initialValue);
		}
		
		return null;
	}
	
	public Money deposit(Money value){
		Money m = null;
		lock.lock();
			balance = balance.add(value);
			m = balance;
		lock.unlock();
		return m;
	}
	
	public Money withdraw(Money value){
		Money m = null;
		lock.lock();
			if(balance.greaterThanOrEqual(value)){
				balance = balance.subtract(value);
				m = balance;
			}
		lock.unlock();
		return m;
	}
	
	public Money getMoney(){
		Money m = null;
		lock.lock();
			m = balance;
		lock.unlock();
		return m;
	}
	
}
