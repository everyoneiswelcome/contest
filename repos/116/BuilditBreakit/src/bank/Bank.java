package bank;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import utils.Money;
import utils.Parsers;
import utils.Parsers.BankInput;


public class Bank {
	
	public synchronized void println(String s){
		System.out.println(s);
		System.out.flush();
	}
		
	private Map<String, Account> accounts = new HashMap<String, Account>();
	private final Lock lock = new ReentrantLock();
	
	public Account createAccount(String accountName, Money money){
		//Lock on map
		Account acc = null;
		lock.lock();
			if(!accounts.containsKey(accountName)){
				acc = Account.from(accountName,money);
				if(acc != null){
					accounts.put(accountName,acc);
				}
			}
		lock.unlock();
		return acc;
	}
	
	public Account getAccount(String accountName){
		//Lock on map
		Account acc = null;
		lock.lock();
			acc = accounts.get(accountName);
		lock.unlock();
		return acc;
	}
	
	public Bank(BankInput input){
		//Create Private & Public Keys
		KeyPairGenerator keyGen = null;
		SecretKey key = null;
		
		
		byte [] enc = null;;
		try {
			KeyGenerator kgen = KeyGenerator.getInstance("AES");
		    kgen.init(BankSocket.SYMETRIC_KEY_SIZE);
		    key = kgen.generateKey();
		    enc = key.getEncoded();
		}catch(Exception e){
//			System.err.println("Cant load RSA instance");
			System.exit(255);
		}
		
		
		
		//Existing auth files are not valid for new runs of bank -- if the specified file already exists, bank should exit with return code 255.
		if(new File(input.authfile).getAbsoluteFile().exists()){
//			System.err.println("File exists");
			System.exit(255);
		}else{
			try{
				//Output public key to file
		        FileOutputStream out = new FileOutputStream(input.authfile); //By default overwrites. TODO: Check doc
		        out.write(enc);
		        out.flush();
		        out.close(); //File written with public key
		        println("created");
			}catch(IOException e){
//				System.out.println("IO problems");
				System.exit(255);
			}
		
		
			try{
				ServerSocket bankSocket = new ServerSocket(input.port);
				while(true){
					Socket client = bankSocket.accept();
					Cipher rsaDecrpyt = Cipher.getInstance("AES/ECB/PKCS5Padding");
				    SecretKeySpec aeskeySpec = new SecretKeySpec(key.getEncoded(), "AES");
					Cipher aesCipherDecrypt = Cipher.getInstance("AES/ECB/PKCS5Padding");
					Cipher aesCipherEncrypt = Cipher.getInstance("AES/ECB/PKCS5Padding");
					aesCipherEncrypt.init(Cipher.ENCRYPT_MODE, aeskeySpec);
					aesCipherDecrypt.init(Cipher.DECRYPT_MODE, aeskeySpec);
					
					BankSocket bs = new BankSocket(client, aesCipherDecrypt,aesCipherEncrypt,this);
					Thread th = new Thread(bs);
					th.setDaemon(true);
					//Set thread timeout;
					th.start();
				}
			
			}catch(Exception e){
				System.exit(255);
			}
		}
	}
	
	
	
	
	
	
	
	
	
	
	static class WrapInt{
	    int value;
	}
	
	//// Execution ////
	public static void main(String [] args){
		//Register SIGTERM 
		final WrapInt wi = new WrapInt();
		wi.value = 255;
	
		
		final BankInput input = Parsers.parseBankVariables(args);
		final Thread th2 = new Thread() {
			@Override
			public void run() {
				Bank b = new Bank(input);
			}
		};
		th2.setDaemon(true);
		
		
		Thread th = new Thread() {
			@Override
			public void run() {
//				System.out.println("I'm called");
				th2.interrupt(); //Interrupt bank thread. Should terminate with proper exit as well!
			}
		};
//		th.setDaemon(true);
		
		Runtime.getRuntime().addShutdownHook(th);
		
		
		if(input==null){
//			System.out.println("Bad input");
			System.exit(255);
		}else{
			wi.value = 0;
			th2.start();
			try {
				th2.join();
			} catch (InterruptedException e) {}
//			System.out.println("Complete termination...");
			System.exit(0);
		}
		
	}
	
	
}
