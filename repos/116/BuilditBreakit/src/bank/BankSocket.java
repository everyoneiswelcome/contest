package bank;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.json.simple.JSONObject;

import utils.ByteUtils;
import utils.Protocol;
import utils.exceptions.NetworkException;

public class BankSocket implements Runnable{
	public static final byte[] HAND_SHAKE =new byte[]{127,68,91,14,100,34,75,-1,6,-127,81,0,124,99,42,65};
	public static final int NEW_ACC = 126;
	public static final int DEPOSIT = 148;
	public static final int WITHDRAW = 132;
	public static final int GET_BALANCE = 96;
	public static final int SYMETRIC_KEY_SIZE = 128;
	public static final int MS_TIMEOUT = 10*1000;
	
	
	Socket sock;
	Cipher decrypt;
	Cipher encrypt;	
	Bank bank;
	public BankSocket(Socket s, Cipher decryptKey,Cipher encryptKey, Bank theBank){
		this.sock = s;
		this.bank = theBank;
		this.decrypt = decryptKey;
		this.encrypt = encryptKey;
	}

	public void run() {
		try {
			sock.setSoTimeout(MS_TIMEOUT);
		} catch (SocketException e1) {}
		//handshake read key.
		//TODO: add a timeout?
		byte [] symetricKey;
		byte [] encryptedSymetricKey = new byte[64];
		try {
			InputStream in = sock.getInputStream();			
			//Further communication is done using symmetric key:
			Cipher aesCipherDecrypt = decrypt;
			Cipher aesCipherEncrypt = encrypt;
			
			
			//Wrap input and output streams
			byte [] data = new byte[32];
			in.read(data);
			data = aesCipherDecrypt.doFinal(data);
//			System.err.println("BANK> Got hand shake");
			if(!Arrays.equals(data, HAND_SHAKE)){
				//Bad Hand Shake
				throw new NetworkException();
			}
			
			//Read encrypted message:
			data = new byte[4];
			sock.getInputStream().read(data);
			int s = ByteUtils.readInt(data, 0);
			data = new byte[s];
			sock.getInputStream().read(data);
			
			ByteArrayInputStream input = new ByteArrayInputStream(aesCipherDecrypt.doFinal(data));
			ByteArrayOutputStream out = new ByteArrayOutputStream();


			
//			System.err.println("BANK> Reading code");
			JSONObject jo = null;
			int code = input.read();
			if (code == NEW_ACC) {
//				System.out.println("New Account");
				jo = Protocol.createAccountSever(input, out, bank);
			} else if (code == GET_BALANCE) {
				jo = Protocol.recieveBalanceRequst(input,out, bank);
			} else if (code == DEPOSIT) {
				jo = Protocol.recieveDepositRequest(input, out, bank);
			} else if (code == WITHDRAW) {
				jo = Protocol.recieveWithdrawRequst(input, out, bank);
			} else {
				// Network error.
				throw new NetworkException();
			}
			
			out.flush();
			out.close();
			//Push response
//			System.out.println("Packing output: "+out.size());
			data = aesCipherEncrypt.doFinal(out.toByteArray());
//			System.out.println("Packed output: "+data.length);
			byte [] size = new byte[4];
			ByteUtils.writeInt(data.length, size, 0);
			sock.getOutputStream().write(size);
			sock.getOutputStream().write(data);
//			System.out.println(Arrays.toString(data));

//			System.err.println("BANK>Closing socket");
			
			
			in.close();
			sock.close();
			
			if(jo != null){
				bank.println(jo.toJSONString());
			}
			
		} catch (Exception e) {
			bank.println("protocol_error");
		}
		
	}
	
	
}
