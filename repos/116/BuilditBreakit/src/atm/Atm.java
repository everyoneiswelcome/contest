package atm;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.json.simple.JSONObject;

import utils.ByteUtils;
import utils.Money;
import utils.Parsers;
import utils.Parsers.AtmInput;
import utils.Protocol;
import utils.TransactionType;
import utils.exceptions.NetworkException;
import bank.BankSocket;

public class Atm {
	public static final int KEYLENGTH = 16;
	public static final int CARDLENGTH = 16;
	private static String fromT(TransactionType t){
		if(t==TransactionType.DEPOSIT){
			return "-d";
		}else if(t==TransactionType.GET_BALANCE){
			return "-g";
		}else if(t==TransactionType.WITHDRAW){
			return "-w";
		}else if(t==TransactionType.NEW_ACCOUNT){
			return "-n";
		}
		return "";
	}
	
	private static String inputToString(AtmInput at){
		if(at == null){
			return "";
		}
		return "["+at.account+" "+fromT(at.transaction)+" "+(at.amount==null?"":at.amount.toString())+"]";
	}
	
	private static byte[] readCardData(String cardFile){
		 byte cardData[] = new byte[CARDLENGTH];
		 boolean card = false;
			try{
				File f = new File(cardFile).getAbsoluteFile();
				FileInputStream fin = new FileInputStream(f);
				if(f.length() == CARDLENGTH){
					fin.read(cardData);
					card= true;
				}
				fin.close();
				
			}catch(Exception e){}
		if(!card){
			return null;
		}
		return cardData;
	}
	
	private static byte[] readPublicKey(String authFile){
		byte[] b = null;
		try{
			RandomAccessFile f = new RandomAccessFile(authFile, "r");
			if(f.length() == KEYLENGTH){
				b = new byte[KEYLENGTH];
				f.read(b);
			}else{
				throw new RuntimeException("Bad Key Length");
			}
			f.close();
		}catch(Exception e){}
		return b;
	}
	private static String getStr(String ... args){
		String full ="";
		for(String s : args){
			full+=s+" ";
		}
		return full;
	}

	public static void main(String ... args){
		//Parse and validate input:
		AtmInput input= Parsers.parseAtmVariables(args);
		if(input == null){
//			System.err.println("Bad input - \""+getStr(args)+"\"");
			System.exit(255);
			return;
		}
		
		//read public key:
		byte [] publicKey = readPublicKey(input.authfile);
		if(publicKey == null){
//			System.err.println("Cannot Read Authfile!");
			System.exit(255);
			return;
		}

	
	
		//Read card (if needed)
		byte [] card = null;
		if(input.transaction != TransactionType.NEW_ACCOUNT){
			card = readCardData(input.cardfile);
			if(card == null){
				File f = new File(input.cardfile).getAbsoluteFile();
//				System.err.println("Can't Read Card File - \""+getStr(args)+"\" Len:" + f.length());
				if(f.length() != 0)
					card = readCardData(input.cardfile);
				if(card == null)
					System.exit(255);
//				return;
			}
		}else{
			//Card file must not already exist!
			File f = new File(input.cardfile);
			if(f.getAbsoluteFile().exists()){
//				System.err.println("Card File Already Exists - \""+getStr(args)+"\"");
				System.exit(255);
			}
		}
		
		
		//Send Request
		JSONObject response = null;
		try{
			InetAddress addr = InetAddress.getByName(input.ipaddr);
			if(addr == null){
//				System.err.println("Bad IpAddr - \""+getStr(args)+"\"");
				throw new NetworkException();
			}
			
			
			
			//Generate session key:
			Cipher rsaEncrpyt = null;
			Cipher aesCipherEncrypt= null ;
			Cipher aesCipherDecrypt = null;
			byte [] aesKey = null;
			
			try{
			    SecretKeySpec aeskeySpec = new SecretKeySpec(publicKey, "AES");
				aesCipherDecrypt = Cipher.getInstance("AES/ECB/PKCS5Padding");
				aesCipherEncrypt = Cipher.getInstance("AES/ECB/PKCS5Padding");
				aesCipherEncrypt.init(Cipher.ENCRYPT_MODE, aeskeySpec);
				aesCipherDecrypt.init(Cipher.DECRYPT_MODE, aeskeySpec);
			}catch(Exception e){
				System.exit(255);
			}
			
			//Encrypt Session Key:
//			byte [] encryptedSessionKey = rsaEncrpyt.doFinal(aesKey);
			
			//Open Connection
			Socket s =  null;
			try{
				s = new Socket(addr, input.port);
			}catch(Exception e){
				System.exit(255);
			}
			//Connection Established. 
			try{
				s.setSoTimeout(BankSocket.MS_TIMEOUT);
			}catch(Exception e){}
			//Write session key (encrypted with public key)
//			s.getOutputStream().write(encryptedSessionKey);
//		    s.getOutputStream().flush();
			

//		    System.out.println("Writting secret key: "+encryptedSessionKey.length);
//		    System.out.println("Wrote secret key");
//			System.out.println(Arrays.toString(aesKey));
		    

//		    //Switch to session key
			byte [] data = aesCipherEncrypt.doFinal(BankSocket.HAND_SHAKE);
			s.getOutputStream().write(data);
//			System.err.println("ATM> Wrote Hand Shake "+inputToString(input));
			 
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			
//		    //Start protocol:
		    if(input.transaction == TransactionType.NEW_ACCOUNT){
		    	Protocol.requestNewAccount(out, input.account,input.amount);
		    }else if(input.transaction == TransactionType.GET_BALANCE){
		    	Protocol.requestBalance(out, input.account, card);
		    }else if(input.transaction == TransactionType.WITHDRAW){
		    	Protocol.requestWithdraw(out, input.account, card, input.amount);
		    }else if(input.transaction == TransactionType.DEPOSIT){
		    	Protocol.requestDeposit(out, input.account, card, input.amount);
		    }
		    
		    //Write Request.
		    out.flush();
		    out.close();
		    
		    data = aesCipherEncrypt.doFinal(out.toByteArray());
		    byte [] size = new byte[4];
		    ByteUtils.writeInt(data.length, size, 0);
		    s.getOutputStream().write(size);
		    s.getOutputStream().write(data);
		    
//		    System.err.println("ATM> Sent Output");
		    s.getInputStream().read(size);
		    int dataSize = ByteUtils.readInt(size, 0);
		    data = new byte[dataSize];
		    s.getInputStream().read(data);
		    s.close();
//		    System.err.println("ATM> Got Input! Socket Now Closed");
//		    System.out.println("Final Data (encrypted): "+dataSize);
//		    System.out.println(Arrays.toString(data));
		    data = aesCipherDecrypt.doFinal(data);
//		    System.out.println("Final Data: "+dataSize);
		    ByteArrayInputStream in = new ByteArrayInputStream(data);
		    
		    
		    //Read Reponse.
		    if(input.transaction == TransactionType.NEW_ACCOUNT){
		    	byte []  bi = Protocol.readNewAccountResponse(in,input.account,input.amount);
		    	if(bi != null){
		    		response = new JSONObject();
		    		response.put("account",input.account);
		    		response.put("initial_balance", input.amount.toBD());//TODO not this.
					//Write to card file
					File f = new File(input.cardfile);
					if(f.exists()){ //TODO: ??
						System.exit(255);
					}
					try{
						//Output public key to file
				        FileOutputStream file = new FileOutputStream(input.cardfile); //By default overwrites. TODO: Check doc
				        file.write(bi);
				        file.flush();
				        file.close();
					}catch(IOException e){
//						System.err.println("Cant write file");
//						System.err.println("Can't write Card File - \""+getStr(args)+"\"");
						System.exit(255);
					}
		    	}
		    	
		    	
		    }else if(input.transaction == TransactionType.GET_BALANCE){
		    	Money balance = Protocol.readAccountData(in);
		    	if(balance != null){
		    		response = new JSONObject();
		    		response.put("account",input.account);
		    		response.put("balance",balance.toBD());
		    	}
		    }else if(input.transaction == TransactionType.WITHDRAW){
		    	if(Protocol.positiveRequest(in)){
		    		response = new JSONObject();
		    		response.put("account",input.account);
		    		response.put("withdraw",input.amount.toBD());
		    	}
		    }else if(input.transaction == TransactionType.DEPOSIT){
		    	if(Protocol.positiveRequest(in)){
		    		response = new JSONObject();
		    		response.put("account",input.account);
		    		response.put("deposit",input.amount.toBD());
		    	}
		    }
		  
		    
		    
		    
			
		}catch(IOException | BadPaddingException | IllegalBlockSizeException e){
			//All of these are networking errors.
//			System.err.println("IO Error");
			System.exit(63); //Bad Request.
		}
		
		//Output response:
	    if(response == null){
//	    	System.err.println("Server Says Bad Request\n\n");
	    	System.exit(255); //Bad Request.
	    }else{
//	    	System.err.println("Success " +inputToString(input)+" \n\n");
	    	System.out.println(response);
	    	System.exit(0);
	    }

		//Exit:
	    System.exit(255);
		
		 
		
		
	}
	
	
}
