package utils;

import java.util.regex.Pattern;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

public class Parsers {
	static final int MAX_FILE_NAME = 255;
	static final int MIN_FILE_NAME = 1;
	
	static final int MIN_ACCOUNT_NAME=1;
	static final int MAX_ACCOUNT_NAME=250;
	
	public static class BankInput{
		public int port;
		public String authfile;
		public BankInput(int port, String fileName){
			this.port = port;
			this.authfile = fileName;
		}
	}
	
	public static class AtmInput{
		public int port;
		public String authfile;
		public String ipaddr;
		public String cardfile;
		public String account; // No default value
		public TransactionType transaction;
		public Money amount; // Optional. Based on transactiontype.
		
		public AtmInput(int port, String authName, String ipddr){
			this.port = port;
			this.authfile = authName;
			this.cardfile = null;
			this.ipaddr = ipddr;
		}
	}
	
	public static boolean validFileName(String fileName){
		return fileName.length() >= MIN_FILE_NAME && fileName.length() <= MAX_FILE_NAME && !fileName.equals(".") && !fileName.equals("..") && Pattern.matches("[_\\-\\.0-9a-z]*", fileName);
	}
	
	//Account names are restricted to same characters as file names but they are inclusively between 1 and 250 characters of length, and "." and ".." *are valid* account names.
	public static boolean validAccountName(String accName){
		return accName.length() >= MIN_ACCOUNT_NAME && accName.length() <= MAX_ACCOUNT_NAME  && Pattern.matches("[_\\-\\.0-9a-z]*", accName);
	}
	
	public static boolean validIpFormat(String ip){
		String segments[] = ip.split("\\.");
		//Test # of segment lengths, leading zero's, longer/larger values, invalid characters, hex
		if(segments.length != 4){
			return false;
		}
		
		for(String s : segments){
			if(Pattern.matches("(0|[1-9][0-9]*)", s) && s.length()<=3){
				try{
					Integer i = Integer.valueOf(s); //try very very large numbers to over flow integer to negative.
					if(i>255||i<0){
						return false;
					}
				}catch (Exception e){
					return false;
				}
			}else{
				return false;
			}
		}
		return true;
	}
	
	public static Money validMoneyFromCommand(String s){
		Money m = Money.fromString(s);
		if(m !=null && m.greaterThan(Money.MAX_COMMANDLINE)){
			return null;
		}
		return m;
	}
	
	public static Integer getPort(String portString){
		try {
			//Check for leading zeros:
			if(!Pattern.matches("0|[1-9][0-9]*", portString)){
				return null;
			}

			int port = Integer.parseInt(portString);
			if (port >= 1024 && port <= 65535) {
				return port;
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}
	
	/***
	 * Basically the library i use is very liberal and has 
	 * loads of features i dont want. this is the crap way to remove those
	 * features.
	 * @param g
	 * @param args
	 * @return
	 */
	public static boolean validInput(boolean g, String ... args){
		for(int i=0;i<args.length; i++){
			if(args[i].contains("=")){
				return false;
			}else if(args[i].contains("--")){
				if(g){
					
				}else if(i%2==0){
					return false;
				}
			}
		}
		
		//Stupid.
		if (g) {
			boolean gFound = false;
			for (int i = 0; i < args.length; i++) {

				if (i % 2 == 0 && !gFound) {
					if (args[i].contains("--")) {
						return false;
					} else if (args[i].equals("-g")) {
						gFound = true;
					}
				} else if (i % 2 == 1 && gFound) {
					if (args[i].contains("--")) {
						return false;
					}
				}

			}
		}
		
		
		return true;
	}
	
	
	
	public static BankInput parseBankVariables(String ... args){
		OptionParser parser = new OptionParser( "p:s:" );
		if(!Parsers.validInput(false,args)){
			return null;
		}
		parser.recognizeAlternativeLongOptions(false);
		OptionSet options;
		try{
			options= parser.parse(args);
		}catch(Exception e){
			return null;
		}
		
		BankInput inputArguments = new BankInput(3000,"bank.auth");
		
		//Does any parameters show twice?
		//Are all parameters valid?
		for(OptionSpec<?> k:options.specs()){
			if(k.options().contains("p")||k.options().contains("s")){
				if(options.valuesOf(k).size()>1){
					return null;
				}
			}else{
				return null;
			}
		}

		if (options.has("p")) {
			String value = options.valueOf("p").toString();
			Integer port = getPort(value);
			if(port == null){
				return null;
			}
			inputArguments.port = port;
		}

		if (options.has("s")) {
			String fileName = options.valueOf("s").toString();
			if(!Parsers.validFileName(fileName)){
				return null;
			}
			inputArguments.authfile = fileName;
		}

		return inputArguments;
	}
	
	public static AtmInput parseAtmVariables(String ... args){
		OptionParser parser = new OptionParser("p:s:i:c:a:w:d:n:g::");
		String [] params = new String[]{"p","s","i","c","a","w","d","n","g"};
		if(!Parsers.validInput(true,args)){
			return null;
		}
		//check if -a must have space against oracle.
		//None shall bar my call
		OptionSet options;
		try{
			options= parser.parse(args);
		}catch(Exception e){
			return null;
		}
		
		AtmInput inputArguments = new AtmInput(3000,"bank.auth","127.0.0.1");
		
		int keyParam = 0;
//		//Are all parameters valid?
		for (OptionSpec<?> k : options.specs()) {
			boolean validOption = true;
			for (String s : params) {
				if (k.options().contains(s)) {
					if (s.equals("n") || s.equals("d") || s.equals("w") || s.equals("g")) {
						keyParam += 1;
					}
					break;
				}
			}
			
			if (validOption) {
				//Does valid option only show once?
				if (options.valuesOf(k).size() > 1) {
					return null;
				}
			} else {
				return null;
			}
		}
		
		//Command line input amounts are bounded from 0.00 to 4294967295.99 
		if(keyParam == 1){
			if(options.has("n")){//TODO: test giving it no arg, valid and invalid ranges
				Money balance = validMoneyFromCommand(options.valueOf("n").toString());
				inputArguments.transaction = TransactionType.NEW_ACCOUNT;
				inputArguments.amount = balance;
				if(balance == null || Money.MIN_ACCOUNT.greaterThan(balance)){
					return null;
				}
				
			}else if(options.has("d")){//TODO: test giving it no arg, valid and invalid ranges
				Money amount = validMoneyFromCommand(options.valueOf("d").toString());
				inputArguments.transaction = TransactionType.DEPOSIT;
				inputArguments.amount = amount;
				if(amount == null|| Money.MIN_DEPOSITE.greaterThan(amount)){
					return null;
				}
				
			}else if(options.has("w")){ //TODO: test giving it no arg, valid and invalid ranges
				Money amount = validMoneyFromCommand(options.valueOf("w").toString());
				inputArguments.transaction = TransactionType.WITHDRAW;
				inputArguments.amount = amount;
				if(amount == null|| Money.MIN_WITHDRAW.greaterThan(amount)){
					return null;
				}
			}else if(options.has("g") && options.valueOf("g")==null ||options.valueOf("g").toString()==""){ //TODO: test giving it an arg
				Object o = options.valueOf("g");
				inputArguments.transaction = TransactionType.GET_BALANCE;
				
			}else{
				//Shouldn't be able to reach?!
				return null;
			}
			
		}else{
			//Not key param (n, d, w, g)
			return null;
		}
		
		if(options.has("i")){
			String ip = options.valueOf("i").toString();
			//TODO: ip's with leading zeros 127.00.00.01
			if(validIpFormat(ip)){
				inputArguments.ipaddr = ip;
			}else{
				return null;
			}
		}
		
		if (options.has("p")) {
			String value = options.valueOf("p").toString();
			Integer port = getPort(value);
			if(port == null){
				return null;
			}
			inputArguments.port = port;
		}

		if (options.has("s")) {
			String fileName = options.valueOf("s").toString();
			if(!Parsers.validFileName(fileName)){
				return null;
			}
			inputArguments.authfile = fileName;
		}
		
		//MUST HAVE ACCOUNT NAME
		if (options.has("a")) {
			String accName = options.valueOf("a").toString();
			if(!Parsers.validAccountName(accName)){
				return null;
			}
			inputArguments.account = accName;
		}else{
			return null;
		}
		
		if (options.has("c")) {
			String fileName = options.valueOf("c").toString();
			if(!Parsers.validFileName(fileName)){
				return null;
			}
			inputArguments.cardfile = fileName;
		}else{
			//Construct default value
			String fileName = inputArguments.account+".card";
			if(!Parsers.validFileName(fileName)){
				return null;
			}
			inputArguments.cardfile = fileName;
		}
		
		return inputArguments;
	}

	
}
