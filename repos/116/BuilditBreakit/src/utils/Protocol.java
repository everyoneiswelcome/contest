package utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;

import org.json.simple.JSONObject;

import bank.Account;
import bank.Bank;
import bank.BankSocket;

public class Protocol {

	//ATM Requests:
	public static void requestBalance(ByteArrayOutputStream output, String accountName, byte[] key) throws IOException{
		output.write(BankSocket.GET_BALANCE);
		ByteUtils.writeString(output, accountName);
		ByteUtils.write(output, key);
	}
	
	public static void requestNewAccount(ByteArrayOutputStream output, String accountName, Money initial) throws IOException{
		output.write(BankSocket.NEW_ACC);
		ByteUtils.writeString(output, accountName);
		ByteUtils.writeMoney(output, initial);
	}
	
	public static void requestDeposit(ByteArrayOutputStream output, String accountName, byte[] key, Money value) throws IOException{
		output.write(BankSocket.DEPOSIT);
		ByteUtils.writeString(output, accountName);
		ByteUtils.write(output, key);
		ByteUtils.writeMoney(output, value);
	}
	
	
	public static void requestWithdraw(ByteArrayOutputStream output, String accountName, byte[] key, Money value) throws IOException{
		output.write(BankSocket.WITHDRAW);
		ByteUtils.writeString(output, accountName);
		ByteUtils.write(output, key);
		ByteUtils.writeMoney(output, value);
	}
	
	//ATM-Request Responses:
	public static byte[] readNewAccountResponse(ByteArrayInputStream input, String accountName, Money initial) throws IOException{
		int i = input.read();
		if(i == 127){
			byte[] bi = ByteUtils.read(input);
			return bi;
		}else{
			return null;
		}
	}
	
	public static boolean positiveRequest(ByteArrayInputStream input){
		int i = input.read();
		if(i == 127){
			return true;
		}
		return false;
	}
	
	public static Money readAccountData(ByteArrayInputStream input) throws IOException{
		//Read Response
		int i = input.read();
		if(i == 127){
			//Read balance
			return ByteUtils.readMoney(input);
		}

		return null;
	}
	
	//Bank Responses:
	public static JSONObject createAccountSever(ByteArrayInputStream input, ByteArrayOutputStream output, Bank bank) throws IOException {
		// Read Account name
		String accName = ByteUtils.readString(input);
		Money m = ByteUtils.readMoney(input);
		// Generate a new account.
		Account acc = bank.createAccount(accName, m);
		if (acc == null) {
			output.write(0);
			return null;
		} else {
			// Success
			output.write(127);
			ByteUtils.write(output, acc.key);
			JSONObject jo = new JSONObject();
			jo.put("account", accName);
			jo.put("initial_balance", m.toBD());// TODO no this.
			return jo;
		}
	}
	
	
	public static JSONObject recieveDepositRequest(ByteArrayInputStream input, ByteArrayOutputStream output, Bank bank) throws IOException{
		String accName = ByteUtils.readString(input);
		byte[] key = ByteUtils.read(input);
		Money value = ByteUtils.readMoney(input);
		// Generate a new account.
		Account acc = bank.getAccount(accName);
		Money balance = acc.getMoney();
		
		//Validate Key:
		if (acc == null || !Arrays.equals(acc.key,key)) {
			output.write(0);
			return null;
		} 
		
		balance = acc.deposit(value);
		// Success
		output.write(127);
		JSONObject jo = new JSONObject();
		jo.put("account", accName);
		jo.put("deposit", value.toBD());// TODO not this.
		return jo;
		
	}
	
	public static JSONObject recieveWithdrawRequst(ByteArrayInputStream input, ByteArrayOutputStream output, Bank bank) throws IOException {
		// Read Account name
		String accName = ByteUtils.readString(input);
		byte[] key = ByteUtils.read(input);
		Money value = ByteUtils.readMoney(input);
		// Generate a new account.
		Account acc = bank.getAccount(accName);
		
		
		//Validate Key:
		if (acc == null || !Arrays.equals(acc.key,key)) {
			output.write(0);
			return null;
		} 
		Money balance = acc.getMoney();
		balance = acc.withdraw(value);
		if(balance != null){
			// Success
			output.write(127);
			JSONObject jo = new JSONObject();
			jo.put("account", accName);
			jo.put("withdraw", value.toBD());// TODO not this.
			return jo;
		}else{
			output.write(0);
		}
		return null;
	}
		
	public static JSONObject recieveBalanceRequst(ByteArrayInputStream input, ByteArrayOutputStream output, Bank bank) throws IOException {
		// Read Account name
		String accName = ByteUtils.readString(input);
		byte[] key = ByteUtils.read(input);
		// Generate a new account.
		Account acc = bank.getAccount(accName);
		if (acc == null || !Arrays.equals(acc.key,key)) {
			output.write(0);
			return null;
		}

		Money balance = acc.getMoney();
		// Success
		output.write(127);
		ByteUtils.writeMoney(output,balance);
		JSONObject jo = new JSONObject();
		jo.put("account", accName);
		jo.put("balance", balance.toBD());// TODO not this.
		return jo;

	}
	


}
