package utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.regex.Pattern;

/***
 * Class used to hold money.
 * Resistant to floating point errors.
 * Backed by BigInteger
 * @author wiselion
 */
public class Money {
	static final BigInteger DOLLAR = new BigInteger("100");
	public static final Money MAX_COMMANDLINE = Money.fromString("4294967295.99");
	public static final Money MIN_ACCOUNT = Money.fromString("10.00");
	public static final Money MIN_DEPOSITE = Money.fromString("0.01");
	public static final Money MIN_WITHDRAW = Money.fromString("0.01");
	
	
	
	BigInteger cents;
	public Money(Money m){
		cents = new BigInteger(m.cents.toByteArray());
	}
	public Money(byte [] data){
		cents = new BigInteger(data);
	}
	
	private Money(BigInteger num){
		cents = num;
	}
	
	public static Money fromString(String s){
		//Validate, return null if problem.
		boolean valid = Pattern.matches("^(0|[1-9][0-9]*)\\.[0-9]{2}", s);
		if(valid){
			BigInteger dollars;
			BigInteger cents;
			String[] split = s.split("\\.");
			dollars = new BigInteger(split[0]).multiply(DOLLAR);
			cents = new BigInteger(split[1]);
			return new Money(dollars.add(cents));
		}
		return null;
	}
	
	public String toString(){
		BigInteger dollars = this.cents.divide(DOLLAR);
		BigInteger remainder = this.cents.mod(DOLLAR);
		String centsString = remainder.toString();
		String dollarString = dollars.toString();
		//dollarString must be at least one character long.
		//centsString must be at least 2 characters long.
		if(centsString.length()==1){
			centsString = "0"+centsString;
		}
		
		return dollarString+"."+centsString;
	}
	
	public float toFloat(){
		BigInteger dollars = this.cents.divide(DOLLAR);
		BigInteger remainder = this.cents.mod(DOLLAR);
		return dollars.floatValue()+.01f*remainder.floatValue();
	}
	
//	public double toDouble(){
//		BigInteger dollars = this.cents.divide(DOLLAR);
//		BigInteger remainder = this.cents.mod(DOLLAR);
//		return dollars.doubleValue()+.01*remainder.doubleValue();
//	}
	
	public BigDecimal toBD(){
		BigInteger dollars = this.cents.divide(DOLLAR);
		BigInteger remainder = this.cents.mod(DOLLAR);
		return new BigDecimal(dollars).add(new BigDecimal(remainder).divide(new BigDecimal(DOLLAR)));
	}
	
	/**
	 * Returns a new object money.
	 * @return
	 */
	public Money add(Money m){
		return new Money(cents.add(m.cents));
	}
	
	/**
	 * Returns a new object money.
	 * @return
	 */
	public Money subtract(Money m){
		return new Money(cents.subtract(m.cents));
	}
	
	/**
	 * Returns true if this is greater than or equal to parameter
	 * @param m
	 * @return
	 */
	public boolean greaterThanOrEqual(Money m){
		return cents.compareTo(m.cents)>=0; //1 or zero 
	}
	
	/**
	 * Returns true if this is greater than or equal to parameter
	 * @param m
	 * @return
	 */
	public boolean greaterThan(Money m){
		return cents.compareTo(m.cents)>0; //1 or zero 
	}
	
	
	@Override
	public boolean equals(Object o){
		if(o instanceof Money){
			Money m = (Money) o;
			return m.cents.equals(cents);
		}
		return false;
	}
	
	public byte[] toByteArray(){
		return cents.toByteArray();
	}
	
}
