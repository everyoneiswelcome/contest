package utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class ByteUtils {
	//TODO: over write in.read() , with a function that checks if it returns -1, and throws error if it does.
	public static byte[] toString(String s){
		return s.getBytes();
	}
	
	public static byte[] splice(byte[] arr, int begin, int len){
		byte[] nArr = new byte[len];
		System.arraycopy(arr, begin, nArr, 0, len);
		return nArr;
	}
	
//	public static byte[] read(ByteArrayInputStream in, byte[]data) throws IOException{
//		int i = in.read(data);
//		if(i == -1){
//			throw new NetworkException();
//		}
//		return data;
//	}
//	
//	public static int read(ByteArrayInputStream in) throws IOException{
//		int i = in.read();
//		if(i == -1){
//			throw new NetworkException();
//		}
//		return i;
//	}
	
	public static void write(ByteArrayOutputStream out, byte[] data) throws IOException{
		ByteUtils.writeInt(out, data.length);
		out.write(data);
	}
	
	public static byte[] read(ByteArrayInputStream in) throws IOException{
		int i  = ByteUtils.readInt(in);
		byte [] data =new byte[i];
		in.read(data);
		return data;
	}
	
	
	// Does not handle byte vectors.
	
	public static byte[] toBytaShort(short s){
		return new byte[]{(byte)((s >> 8)&0xFF), (byte)(s & 0xFF)};
	}
	public static byte[] toBytaInt(int i){
		return new byte[]{(byte)((i >> 24)&0xFF), 
				(byte)((i >> 16)&0xFF), 
				(byte)((i >> 8)&0xFF), 
				(byte)(i & 0xFF)};
	}
	public static byte[] toBytaLong(long l){
		return new byte[]{(byte)((l >> 56)&0xFF), 
				(byte)((l >> 48)&0xFF), 
				(byte)((l >> 40)&0xFF), 
				(byte)((l >> 32)&0xFF), 
				(byte)((l >> 24)&0xFF), 
				(byte)((l >> 16)&0xFF), 
				(byte)((l >> 8)&0xFF), 
				(byte)(l & 0xFF)};
	}
	public static byte[] toBytaFloat(float f){
		int n = Float.floatToIntBits(f);
		return new byte[]{(byte)((n >> 24)&0xFF), 
				(byte)((n >> 16)&0xFF), 
				(byte)((n >> 8)&0xFF), 
				(byte)(n & 0xFF)};
	}
	public static byte[] toBytaDouble(double d){
		long n = Double.doubleToLongBits(d);
		return new byte[]{(byte)((n >> 56)&0xFF), 
				(byte)((n >> 48)&0xFF), 
				(byte)((n >> 40)&0xFF), 
				(byte)((n >> 32)&0xFF), 
				(byte)((n >> 24)&0xFF), 
				(byte)((n >> 16)&0xFF), 
				(byte)((n >> 8)&0xFF), 
				(byte)(n & 0xFF)};
	}

	
	public static int writeString(String s, byte[] data, int offset){
		int n = s.length();
		if(n > 65525){
			throw new RuntimeException("String too long. Max 65525 characters.");
		}
		offset = writeShort((short) n, data, offset);
		byte[] str = s.getBytes();
		System.arraycopy(str, 0, data, offset, str.length);
		offset += str.length;
		return offset;
	}
	public static int writeShort(short s, byte[] data, int offset){
		data[offset++] = (byte)((s >> 8)&0xFF);
		data[offset++] = (byte)(s & 0xFF);
		return offset;
	}
	public static int writeInt(int i, byte[] data, int offset){
		data[offset++] = (byte)((i >> 24)&0xFF); 
		data[offset++] = (byte)((i >> 16)&0xFF);
		data[offset++] = (byte)((i >> 8)&0xFF);
		data[offset++] = (byte)(i & 0xFF);
		return offset;
	}
	public static int writeLong(long l, byte[] data, int offset){
		data[offset++] = (byte)((l >> 56)&0xFF);
		data[offset++] = (byte)((l >> 48)&0xFF);
		data[offset++] = (byte)((l >> 40)&0xFF);
		data[offset++] = (byte)((l >> 32)&0xFF);
		data[offset++] = (byte)((l >> 24)&0xFF);
		data[offset++] = (byte)((l >> 16)&0xFF);
		data[offset++] = (byte)((l >> 8)&0xFF);
		data[offset++] = (byte)(l & 0xFF);
		return offset;
	}
	public static int writeFloat(float f, byte[] data, int offset){
		int i = Float.floatToIntBits(f);
		data[offset++] = (byte)((i >> 24)&0xFF); 
		data[offset++] = (byte)((i >> 16)&0xFF);
		data[offset++] = (byte)((i >> 8)&0xFF);
		data[offset++] = (byte)(i & 0xFF);
		return offset;
	}
	public static int writeDouble(double d, byte[] data, int offset){
		long i = Double.doubleToLongBits(d);
		data[offset++] = (byte)((i >> 56)&0xFF);
		data[offset++] = (byte)((i >> 48)&0xFF);
		data[offset++] = (byte)((i >> 40)&0xFF);
		data[offset++] = (byte)((i >> 32)&0xFF);
		data[offset++] = (byte)((i >> 24)&0xFF);
		data[offset++] = (byte)((i >> 16)&0xFF);
		data[offset++] = (byte)((i >> 8)&0xFF);
		data[offset++] = (byte)(i & 0xFF);
		return offset;
	}

	
	
	public static String readString(byte[] data, int offset){
		byte[] str = new byte[readShort(data, offset)];
		if(data.length - offset < str.length+2){
			throw new RuntimeException("Malformed string: Expected length exceeds array.");
		}
		System.arraycopy(data, offset+2, str, 0, str.length);
		return new String(str);
	}
	
	public static short readShort(byte[] data, int offset){
		return (short)((data[offset++]&0xFF) << 8 | 
				(data[offset++]&0xFF));
	}
	
	public static int readInt(byte[] data, int offset){
		return ((data[offset++]&0xFF) << 24) | 
				((data[offset++]&0xFF) << 16) |
				((data[offset++]&0xFF) << 8) | 
				(data[offset++]&0xFF);
	}
	
	public static long readLong(byte[] data, int offset){
		return (((long)data[offset++]&0xFF) << 56) | 
				(((long)data[offset++]&0xFF) << 48) |
				(((long)data[offset++]&0xFF) << 40) | 
				(((long)data[offset++]&0xFF) << 32) |
				(((long)data[offset++]&0xFF) << 24) | 
				(((long)data[offset++]&0xFF) << 16) |
				(((long)data[offset++]&0xFF) << 8) | 
				((long)data[offset++]&0xFF);
	}
	
	public static float readFloat(byte[] data, int offset){
		return Float.intBitsToFloat(((data[offset++]&0xFF) << 24) | 
				((data[offset++]&0xFF) << 16) |
				((data[offset++]&0xFF) << 8) | 
				(data[offset++]&0xFF));
	}
	public static double readDouble(byte[] data, int offset){
		return Double.longBitsToDouble(
				(((long)data[offset++]&0xFF) << 56) | 
				(((long)data[offset++]&0xFF) << 48) |
				(((long)data[offset++]&0xFF) << 40) | 
				(((long)data[offset++]&0xFF) << 32) |
				(((long)data[offset++]&0xFF) << 24) | 
				(((long)data[offset++]&0xFF) << 16) |
				(((long)data[offset++]&0xFF) << 8) | 
				((long)data[offset++]&0xFF));
	}

//	
//	public static BigInteger readBigIntger(ByteArrayInputStream in) throws IOException {
//		int s = readInt(in);
//		byte [] b = new byte[s];
//		in.read(b);
//		return new BigInteger(b);
//	}

//	public static void writeBigIntger(ByteArrayOutputStream out, BigInteger big) throws IOException{
//		byte [] b = big.toByteArray();
//		writeInt(out, b.length);
//		out.write(b);
//	}
	
	
	public static Money readMoney(ByteArrayInputStream in) throws IOException{
		int s = readInt(in);
		byte [] b = new byte[s];
		in.read(b);
		return new Money(b);
	}
//	
	public static void writeMoney(ByteArrayOutputStream out, Money m) throws IOException{
		byte [] b = m.toByteArray();
		writeInt(out, b.length);
		out.write(b);
	}
	
	public static void writeString(ByteArrayOutputStream out, String s) throws IOException{
		int n = s.length();
		if(n > 65525){
			throw new RuntimeException("String too long. Max 65525 characters.");
		}
		writeShort(out, (short) n);
		try {
			out.write(s.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public static void writeShort(ByteArrayOutputStream out, short s) throws IOException{
		out.write(s >> 8);
		out.write(s);
	}
	public static void writeInt(ByteArrayOutputStream out, int i) throws IOException{
		out.write(i >> 24);
		out.write(i >> 16);
		out.write(i >> 8);
		out.write(i);
	}
	public static void writeLong(ByteArrayOutputStream out, long l) throws IOException{
		out.write((int)(l >> 56));
		out.write((int)(l >> 48));
		out.write((int)(l >> 40));
		out.write((int)(l >> 32));
		out.write((int)(l >> 24));
		out.write((int)(l >> 16));
		out.write((int)(l >> 8));
		out.write((int)l);
	}
	public static void writeFloat(ByteArrayOutputStream out, float f) throws IOException{
		int i = Float.floatToIntBits(f);
		out.write(i >> 24);
		out.write(i >> 16);
		out.write(i >> 8);
		out.write(i);
	}
	public static void writeDouble(ByteArrayOutputStream out, double d) throws IOException{
		long l = Double.doubleToLongBits(d);
		out.write((int)(l >> 56));
		out.write((int)(l >> 48));
		out.write((int)(l >> 40));
		out.write((int)(l >> 32));
		out.write((int)(l >> 24));
		out.write((int)(l >> 16));
		out.write((int)(l >> 8));
		out.write((int)l);
	}

	
	public static String readString(ByteArrayInputStream in) throws IOException{
		byte[] str = new byte[readShort(in)];
		try {
			in.read(str);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new String(str);
	}
	public static short readShort(ByteArrayInputStream in) throws IOException{
		return (short)((in.read() << 8) | 
						in.read());
	}
	public static int readInt(ByteArrayInputStream in) throws IOException{
		return (in.read() << 24) | 
				(in.read() << 16) |
				(in.read() << 8) | 
				(in.read());
	}
	public static float readFloat(ByteArrayInputStream in) throws IOException{
		return Float.intBitsToFloat((in.read() << 24) | 
				(in.read() << 16) |
				(in.read() << 8) | 
				(in.read()));
	}
	public static double readDouble(ByteArrayInputStream in) throws IOException{
		return Double.longBitsToDouble(
				(((long)in.read()) << 56) | 
				(((long)in.read()) << 48) |
				(((long)in.read()) << 40) | 
				(((long)in.read()) << 32) |
				(((long)in.read()) << 24) | 
				(((long)in.read()) << 16) |
				(((long)in.read()) << 8) | 
				((long)in.read()));
	}

	
	
}
